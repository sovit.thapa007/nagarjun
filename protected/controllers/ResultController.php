<?php

class ResultController extends RController
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/

	/**
	* @return array action filters
	*/

	/*public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}*/


	 /**
    * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
    * using two-column layout. See 'protected/views/layouts/column2.php'.
    */
    /**
    * @return array action filters
    */
    public function filters()
    {
            return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
    * Specifies the access control rules.
    * This method is used by the 'accessControl' filter.
    * @return array access control rules
    */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
            'actions'=>array('StudentList','gradeSheet','ReportSummery'),
            'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
            'actions'=>array('StudentGradeReport','StudentGradeReportData','ResultPDF','GenerateAverageSchoolReport','AverageSchoolReport','AvgSchlReportData','StudentList','GradeSheet','GradeWiseReport','ReportSummery','SchoolWiseGradeStudent','index','admin'),
            'users'=>array(Yii::app()->params['superadmin']),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
		'model'=>$this->loadModel($id),
		));
	}

	/**
	*student grade report
	*/
	public function actionStudentGradeReport(){
		$model = new Result();
		if(isset($_GET['Result'])){
			$academic_year = isset($_GET['Result']['academic_year']) ? $_GET['Result']['academic_year'] : UtilityFunctions::AcademicYear();
			$grade_point = isset($_GET['Result']['grade']) ? $_GET['Result']['grade'] : null;
			$limit = isset($_GET['Result']['top']) ? $_GET['Result']['top'] : null;
			$school_id = isset($_GET['Result']['school_id']) ? $_GET['Result']['school_id'] : null;
			$studentResultInformation = Result::model()->StudentGradeReport($academic_year, $grade_point, $limit, $school_id);
		if(isset($_GET['Result']['ledger_type']) && $_GET['Result']['ledger_type']=='pdf'){
			$gradePoint = isset($_GET['Result']['grade']) ? $_GET['Result']['grade'] : 0;
			$gardLetter = $gradePoint !=0 ?  UtilityFunctions::GradeLetter($gradePoint) : 'ALL';
			$title = "GRADE ( ".$gardLetter. " ) STUDENT  LIST REPORT";
            # mPDF
            $mPDF1 = Yii::app()->ePdf->mpdf();

            # You can easily override default constructor's params
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
            $mPDF1->WriteHTML($this->renderPartial('_student_report_pdf',array('studentResultInformation'=>$studentResultInformation, 'title'=>$title, 'academic_year'=>$academic_year), true));
			$mPDF1->Output(Yii::app()->function->seoUrl($title).'.pdf', 'I');
		}else{
			$this->layout = '_datatable_layout';
			return $this->render('_student_report_datatable',['academic_year' => $academic_year, 'grade_point' => $grade_point, 'limit' => $limit, 'school_id'=>$school_id,'studentResultInformation'=>$studentResultInformation]);
		}
	}
	return $this->render('_student_report',['model'=>$model]);
}

	/**
	*
	*/
	public function actionStudentGradeReportData(){
		$academic_year = isset($_GET['academic_year']) ? $_GET['academic_year'] : null;
		$grade_point = isset($_GET['grade']) && $_GET['grade'] != 'NULL' ? $_GET['grade'] : null;
		$limit = isset($_GET['top']) && $_GET['top'] != 'NULL' ? $_GET['top'] : null;
		$school_id = isset($_GET['school_id']) && $_GET['school_id'] != 'NULL' ? $_GET['school_id'] : null;
		$studentResultInformation = Result::model()->StudentGradeReport($academic_year, $grade_point, $limit, $school_id);

		$studentData = [];
		if(!empty($studentResultInformation)){
				$sn = 1;
			foreach ($studentResultInformation as $schlInfo) {
				$school = $schlInfo->school ? $schlInfo->school->title : '' ;
				$schooltype = $schlInfo->school ? $schlInfo->school->type : '';
				$name = $schlInfo->student ? ucwords($schlInfo->student->first_name.' '.$schlInfo->student->middle_name.' '.$schlInfo->student->last_name) : '';
				$studentData[] = [$sn++, $school, $schooltype, $name, $schlInfo->grade];
			}
		}
		$studentArrayData = ["data"=>$studentData];
		echo json_encode($studentArrayData);
		exit;
	}
	/**
	* PDF Result Section
	*/

	public function actionResultPDF(){
		$model = new Result();
		$start_time = microtime(true); 
		if(isset($_GET['Result']) ){
			$is_grace_mark = Yii::app()->params->is_grace_mark;
			$terminal_id = 1;//is_numeric($_GET['Result']['terminal_id']) ? $_GET['Result']['terminal_id'] : null;
			$school_id = is_numeric($_GET['Result']['school_id']) ? $_GET['Result']['school_id'] : null;
			$academic_year = is_numeric($_GET['Result']['academic_year']) ? $_GET['Result']['academic_year'] : UtilityFunctions::AcademicYear();
			$academicYear = AcademicYear::model()->findByAttributes(['year'=>$academic_year]);
			$year = $academicYear ?  $academicYear->year.'( '.date('Y', strtotime($academicYear->end_english_date)).' A.D'.' )' : '';
			$schoolInformaiton = BasicInformation::model()->findByPk($school_id);
				$terminal_ = Terminal::model()->findByPk($terminal_id);
				$terminal_title  = !empty($terminal_) ? $terminal_->title : '';
				$InformationSection = StudentInformation::model()->TerminalResultPdF($_GET['Result']);
				$resultInformation = isset($InformationSection['result_information']) ? $InformationSection['result_information'] : array();
				$subject_information = isset($InformationSection['subject_information']) ? $InformationSection['subject_information'] : array();
	            $is_practical_array = $is_cas_array = array();
	            $cas_percent = 0;
	            if($subject_information){
	                foreach ($subject_information as $subject_info) {
	                	$marks_information = $subject_info -> marks;
	                	if(!empty($marks_information)){
		                	foreach ($marks_information as $subject) {
			                    $is_practical_array[] = $subject->is_practical == 1 ? 'true': 'false';
			                    $is_cas_array[] = $subject->is_cas == 1 ? 'true': 'false';
			                    $cas_percent= $subject->cas_percent;
		                	}
	                	}
	                }
	            }
		            $is_cas = in_array('true', $is_cas_array) > 0 ? true : null;
		            $is_practical = in_array('true', $is_practical_array) ? true : false;
	                # mPDF
	                $mPDF1 = Yii::app()->ePdf->mpdf();

	                # You can easily override default constructor's params
	                $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');   

	                # renderPartial (only 'view' of current controller)['RegistrationHistory']
	                if(isset($_GET['Result']['result_type']) && $_GET['Result']['result_type'] == 'grade'){
	                	$mPDF1->WriteHTML($this->renderPartial('terminal_grade_result_nagarjun',array('resultInformation'=>$resultInformation,'subject_information'=>$subject_information, 'is_practical'=>$is_practical, 'cas_percent'=>$cas_percent, 'is_cas'=>$is_cas,'terminal_title'=>$terminal_title,'subject_marks'=>$subject_marks, 'schoolInformaiton'=>$schoolInformaiton, 'year'=>$year, 'academic_year'=>$academic_year), true));
	                }else{
	                	$mPDF1->WriteHTML($this->renderPartial('result_',array('resultInformation'=>$resultInformation,'subject_information'=>$subject_information, 'is_practical'=>$is_practical, 'cas_percent'=>$cas_percent, 'is_cas'=>$is_cas,'terminal_title'=>$terminal_title,'subject_marks'=>$subject_marks), true));
	                }

	                $mPDF1->Output($file);
		}
		$end_time = microtime(true);
		//dividing with 60 will give the execution time in minutes other wise seconds
		$execution_time = ($end_time - $start_time);
		$this->render('result_pdf',['execution_time'=>$execution_time,'model'=>$model]);
	}


	/**
	*/

	public function actionGenerateAverageSchoolReport(){
		$model = new ResultAverageReport();
		$academic_year = UtilityFunctions::AcademicYear();
		$generateAverageResult = Result::model()->SchoolAverageReport($academic_year);
		if($generateAverageResult)
			return $this->redirect(array('averageSchoolReport','academic_year'=>$academic_year));
		$this->render('_generate_average_report',['model'=>$model]);

	}

	/**
	*/
	public function actionAverageSchoolReport(){

		$model = new Result();
		if(isset($_GET['Result'])){
			$academic_year = isset($_GET['Result']['academic_year']) ? $_GET['Result']['academic_year'] : null;
			$grade_point = isset($_GET['Result']['grade']) ? $_GET['Result']['grade'] : null;
			$limit = isset($_GET['Result']['top']) ? $_GET['Result']['top'] : null;
			$averageReportData = ResultAverageReport::model()->AverageReportData($academic_year, $grade_point, $limit);
			if(isset($_GET['Result']['ledger_type']) && $_GET['Result']['ledger_type']=='pdf'){
				$gradePoint = isset($_GET['Result']['grade']) ? $_GET['Result']['grade'] : 0;
				$gardLetter = UtilityFunctions::GradeLetter($gradePoint);
				$title = $gradePoint !=0 ?   "GRADE ( ".$gardLetter. " ) SCHOOL AVERAGE LIST REPORT" : "SCHOOL WISE AVERAGE REPORT";
	            # mPDF
	            $mPDF1 = Yii::app()->ePdf->mpdf();

	            # You can easily override default constructor's params
	            $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
	            $mPDF1->WriteHTML($this->renderPartial('_average_school_report_pdf',array('averageReportData'=>$averageReportData['data'], 'title'=>$title, 'academic_year'=>$academic_year), true));
	            $mPDF1->Output($file);
			}else{
				$this->layout =  '_datatable_layout';
				return $this->render('_average_school_report',['averageReportData'=>$averageReportData['data'], 'academic_year'=>$academic_year]);
			}
		}
		return $this->render('_school_report',['model'=>$model]);
	}


	/**
	*/
	public function actionAvgSchlReportData(){
		$academic_year = isset($_GET['academic_year']) ? $_GET['academic_year'] :  null;
		$grade_point = isset($_GET['grade_point']) ? $_GET['grade_point'] :  null;
		$limit = isset($_GET['limit']) ? $_GET['limit'] :  null;
		$averageReportData = ResultAverageReport::model()->AverageReportData($academic_year, $grade_point, $limit);
		echo json_encode($averageReportData);
		exit;
	}



	/**
	*individual print of grade sheet, 
	*/
	public function actionStudentList(){
	  	$this->layout = '_datatable_layout';
		$model = new Result();
		$resultData = [];
		if(isset($_GET['Result'])){
			$resultData = Result::model()->StudentResultList($_GET['Result']);
		}
		$this->render('_student_list',['model'=>$model, 'resultData'=>$resultData]);
	}

	/**
	* generate individual grade sheet
	*/
	public function actionGradeSheet(){
		if(isset($_GET['student_id']) && is_numeric($_GET['student_id']) && isset($_GET['type']) && in_array($_GET['type'], ['original','copy'])){
			$student_id = $_GET['student_id'];
			$type = $_GET['type'];
			$studentDetail = StudentInformation::model()->findByPk($student_id);
			if(empty($studentDetail))
				throw new CHttpException(404,'The requested student does not exist.');
			if(UtilityFunctions::IsSuperAdmin()){
				$result = Result::model()->find('student_id=:student_id AND status=:status AND academic_year=:academic_year',[':student_id'=>$student_id, ':status'=>1, ':academic_year'=>$studentDetail->academic_year]);
				$markInformation = MarkObtained::model()->findAll('student_id=:student_id AND subject_status=:status AND academic_year=:academic_year',[':student_id'=>$student_id, ':status'=>1, ':academic_year'=>$studentDetail->academic_year]);
			}else{
				$result = Result::model()->find('student_id=:student_id AND status=:status AND academic_year=:academic_year AND school_id=:school_id',[':student_id'=>$student_id, ':status'=>1, ':academic_year'=>$studentDetail->academic_year, ':school_id'=>UtilityFunctions::SchoolID()]);
				$markInformation = MarkObtained::model()->findAll('student_id=:student_id AND subject_status=:status AND academic_year=:academic_year AND school_id=:school_id',[':student_id'=>$student_id, ':status'=>1, ':academic_year'=>$studentDetail->academic_year, ':school_id'=>UtilityFunctions::SchoolID()]);
			}

			if(empty($result))
				throw new CHttpException(404,'Result doesnot publish, please check it again.');
	        # mPDF
	        $mPDF1 = Yii::app()->ePdf->mpdf();

	        # You can easily override default constructor's params
	        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
	       $mPDF1->WriteHTML($stylesheet, 1);
	       $mPDF1->WriteHTML($this->renderPartial('_single_student_grade_result',array('studentDetail'=>$studentDetail, 'result'=>$result, 'academic_year'=>$studentDetail->academic_year, 'type'=>$type, 'markInformation'=>$markInformation), true));
	        $mPDF1->Output($file);
		}else
			$this->redirect(['StudentList']);
	}

	/**
	* grade wise number , 
	*/
	public function actionGradeWiseReport(){
		$this->layout = '_datatable_layout';
		$model = new Result();
		$school = $grade = $data = $lineChart = $years =  [];
		if(isset($_GET['Result'])){
			$dataResult = Result::model()->GradeWiseNumber($_GET['Result']);
			$school = isset($dataResult['schoolArray']) ? $dataResult['schoolArray'] : [];
			$years = isset($dataResult['year']) ? $dataResult['year'] : [];
			$grade = isset($dataResult['grade']) ? $dataResult['grade'] : [];
			$data = isset($dataResult['data']) ? $dataResult['data'] : [];
			$lineChart = isset($dataResult['dataPoints']) ? $dataResult['dataPoints'] : [];
			$this->render('_grade_wise_report',['model'=>$model, 'arrayData'=>json_encode($lineChart),'school' => $school, 'grade'=>$grade, 'data'=>$data, 'lineChart'=>$lineChart,'years'=>$years]);
			exit;
		}
		$this->render('_grade_wise_report',['model'=>$model, 'arrayData'=>json_encode($lineChart),'school' => $school, 'grade'=>$grade, 'data'=>$data, 'lineChart'=>$lineChart,'years'=>$years]);
	}

	/**
	*
	*/


	public function actionReportSummery(){
		$model = new Result();
		$report = [];
		$academic_year = UtilityFunctions::AcademicYear();
		if(isset($_GET['Result'])){
			$academic_year = isset($_GET['Result']['academic_year']) ? $_GET['Result']['academic_year'] : UtilityFunctions::AcademicYear();
			$result_ = isset($_GET['Result']['result_']) ? $_GET['Result']['result_'] : null ;
			$report = Result::model()->ReportSummery($academic_year);
			if($result_ && $result_ == 'pdf'){
	            # mPDF
	            $mPDF1 = Yii::app()->ePdf->mpdf();

	            # You can easily override default constructor's params
	            $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
	            $mPDF1->writeHTMLfooter=true;
	            $mPDF1->defaultfooterline = 0;
	            $mPDF1->SetHTMLFooter('<div style="text-align: center">{PAGENO} / {nbpg}</div>');
	            //$mPDF1->AddPage('L');
	            $mPDF1->WriteHTML($this->renderPartial('_summery_report_pdf',['report'=>$report, 'academic_year'=>$academic_year, 'model'=>$model], true));
				$mPDF1->Output(Yii::app()->function->seoUrl($title).'.pdf', 'I');

			}
		}
		$this->render('_summery_report',['report'=>$report, 'academic_year'=>$academic_year, 'model'=>$model]);
	}


	/**
	* grade wise student number with respected to school,
	* @param int academic year, array school_id, 
	* @return grade wise student number, order by school
	*/
	public function actionSchoolWiseGradeStudent(){
		$this->layout = '_datatable_layout';
		$model = new Result();
		$academic_year = isset($_GET['Result']['academic_year']) ? $_GET['Result']['academic_year'] : UtilityFunctions::AcademicYear();
		$school_id_array = isset($_GET['Result']['school_id']) ? $_GET['Result']['school_id'] : [];
		$reportData = Result::model()->GradeWiseNumberGroupBySchool($academic_year, $school_id_array);
		if(isset($_GET['Result']['result_']) && $_GET['Result']['result_'] == 'pdf'){
            # mPDF
            $mPDF1 = Yii::app()->ePdf->mpdf();

            # You can easily override default constructor's params
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
            $mPDF1->writeHTMLfooter=true;
            $mPDF1->defaultfooterline = 0;
            $mPDF1->SetHTMLFooter('<div style="text-align: center">{PAGENO} / {nbpg}</div>');
            $mPDF1->AddPage('L');
            $mPDF1->WriteHTML($this->renderPartial('_grade_wise_student_pdf',['model'=>$model, 'reportData'=>$reportData, 'academic_year'=>$academic_year], true));
			$mPDF1->Output(Yii::app()->function->seoUrl($title).'.pdf', 'I');

		}

		$this->render('_grade_wise_students',['model'=>$model, 'reportData'=>$reportData, 'academic_year'=>$academic_year]);
	}

		
	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Result');
		$this->render('index',array(
		'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new Result('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Result']))
		$model->attributes=$_GET['Result'];

		$this->render('admin',array(
		'model'=>$model,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=Result::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='result-form')
		{
		echo CActiveForm::validate($model);
		Yii::app()->end();
		}
	}
}
