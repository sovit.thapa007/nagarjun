<?php

class SubjectInformationController extends Controller
{

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
//'accessControl', // perform access control for CRUD operations
		);
	}
	
    /**
    * Specifies the access control rules.
    * This method is used by the 'accessControl' filter.
    * @return array access control rules
    */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
            'actions'=>array('OptionalSubjectList'),
            'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
            'actions'=>array('View','Create','Update','Delete','Index','Admin','OptionalSubjectList'),
            'users'=>array(Yii::app()->params['superadmin']),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
		'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new SubjectInformation;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SubjectInformation']))
		{
		$model->attributes=$_POST['SubjectInformation'];
		if($model->validate() && $model->save())
		$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
		'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SubjectInformation']))
		{
		$model->attributes=$_POST['SubjectInformation'];
		if($model->validate() && $model->save())
		$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
		'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
		// we only allow deletion via POST request
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}


	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$model=new SubjectInformation('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SubjectInformation']))
		$model->attributes=$_GET['SubjectInformation'];

		$this->render('admin',array(
		'model'=>$model,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new SubjectInformation('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SubjectInformation']))
		$model->attributes=$_GET['SubjectInformation'];

		$this->render('admin',array(
		'model'=>$model,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=SubjectInformation::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='subject-information-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


	public function actionOptionalSubjectList(){
		$school_id = isset($_POST['school_id']) ? $_POST['school_id'] : null;
		$school_information = BasicInformation::model()->findByPk($school_id);
		if(!empty($school_information) && $school_information->subject_type == 'general'){
			$compulsary_subject = SubjectInformation::model()->findAll('status=:status AND subject_type=:subject_type',[':status'=>1, ':subject_type'=>'general']);
			$option = "<option value=''>Select Subject </option>";
			if(!empty($compulsary_subject)){
				foreach ($compulsary_subject as $cmp_sub) {
					$option .= CHtml::tag('option', array('value' => $cmp_sub->id), CHtml::encode(ucwords($cmp_sub->title)), true);
				}
			}
		}else{
			$option = "<option value=''>Select Subject </option>";
			$school_wise_subject = SchoolSubject::model()->findAll('school_id=:school_id AND status=:status',[':school_id'=>$school_id,':status'=>1]);
			if(!empty($school_wise_subject)){
				foreach ($school_wise_subject as $subject_) {
					$title = $subject_->subject ? $subject_->subject->title :  '';
					$option .= CHtml::tag('option', array('value' => $subject_->subject_id), CHtml::encode(ucwords($title)), true);
				}
			}
		}
		echo CJSON::encode(['success' => true, 'option' => $option]);

	}

	
}
