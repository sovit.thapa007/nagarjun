<?php

class UsersController extends Controller
{


	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
//'accessControl', // perform access control for CRUD operations
		);
	}
	
    /**
    * Specifies the access control rules.
    * This method is used by the 'accessControl' filter.
    * @return array access control rules
    */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
            'actions'=>array('ChangePassword'),
            'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
            'actions'=>array('View','Create','Update','Delete','Index','Admin','DeactiveUser','ResetPassword','MappingBoard','ChangePassword','sendsms'),
            'users'=>array(Yii::app()->params['superadmin']),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$userInformation = $this->loadModel($id);
		if(empty($userInformation))
			throw new CHttpException('Invalid Request.');
		$boardInformation = SchoolBoardInformation::model()->find('user_id=:user_id',[':user_id'=>$id]);
		$boardMemberList = SchoolBoardInformation::model()->findAll('school_id=:school_id AND status=:status',[':school_id'=>$userInformation->school_id, ':status'=>1]);
		$raw_password =  $userInformation->raw_password;
		$this->render('view',array(
		'model'=>$userInformation, 'boardInformation' => $boardInformation, 'boardMemberList'=>$boardMemberList, 'id'=>$id
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Users;
		//$profile = new Profile;
		if (isset($_POST['Users'])) {
			$role = isset($_POST['Users']['role']) ? $_POST['Users']['role'] : null;
			$this->beginWidget('CHtmlPurifier');
			$model->attributes = $_POST['Users'];
			$model->raw_password = UtilityFunctions::encrypt_decrypt('encrypt',$model->password);
			$model->activkey = $this->encrypting(microtime() . $model->password);
			$model->createtime = time();
			$model->lastvisit = time();
			$model->status = 1;
			if(empty($model->school_id) && !in_array($role, ['dataentry','administrativeofficer']))
				throw new CHttpException('Please Chooose School, Try Again');
			$is_errors = [];
			$transaction = Yii::app()->db->beginTransaction();
				$model->password = $this->encrypting($model->password);
				$this->endWidget();
				if($model->validate() && $model->save())
					$is_errors[] = 'true';
				else{
					$is_errors[] = 'false';
				}

				$auth_model = new AuthAssignment;
				$auth_model -> itemname =  $role;
				$auth_model -> userid = $model->id;//user id
				$auth_model -> data = 'N;';
				if(!$auth_model->validate() || !$auth_model->save()){
					$is_errors[] = 'false';
					$transaction->rollback();
					throw new CHttpException('Something is wrong during user Creation');
				}
			if(!in_array('false', $is_errors)){
				$transaction->commit();
				$this->redirect(array('view', 'id' => $model->id));
			}else{
				$transaction->rollback();
				$this->render('_signup', array(
					'model' => $model,
				));
				//
			}
		} 
		$this->render('_signup', array(
			'model' => $model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{
		$model->attributes=$_POST['Users'];
		if($model->validate() && $model->save())
		$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
		'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
		// we only allow deletion via POST request
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$this->layout = '_datatable_layout';
		$criteria=new CDbCriteria;
		$criteria->order='createtime ASC';
		$userInformation = Users::model()->findAll($criteria);
		$this->render('index', array(
			'userInformation' => $userInformation,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new Users('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Users']))
		$model->attributes=$_GET['Users'];

		$this->render('admin',array(
		'model'=>$model,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=Users::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='users-form')
		{
		echo CActiveForm::validate($model);
		Yii::app()->end();
		}
	}


	public function actionIndexDatatable(){
		$criteria=new CDbCriteria;
		$criteria->order='createtime DESC';
		$userInformation = Users::model()->findAll($criteria);
		$userData = [];
		if(!empty($userInformation)){
			foreach ($userInformation as $user) {
				$username = "<a href='".Yii::app()->baseUrl."/users/".$user->id."'>".$user->username."</a>";
				$school = $user->school ? $user->school->title : Yii::app()->params['municipality'];
				$lastvisit = date("Y-m-d H:i:s", $user->lastvisit);
				$registerDate = date("Y-m-d H:i:s", $user->createtime);
				$status = $user->status == 1 ? 'Active' : 'De-Active';

				$userData[] = [$username, $user->email, $school, $lastvisit, $registerDate, $status];
			}
		}
		$userArrayData = ["data"=>$userData];
		echo json_encode($userArrayData);
		exit;
	}

	/**
	 * @return hash string.
	 */
	private function encrypting($string = "")
	{
		$hash = Yii::app()->getModule('user')->hash;
		if ($hash == "md5")
			return md5($string);
		if ($hash == "sha1")
			return sha1($string);
		else
			return hash($hash, $string);
	}

	/**
	*@param int userid
	*/
	public function actionDeactiveUser($id){
		$userInformation = Users::model()->findByPk($id);
		if(empty($userInformation))
			throw new CHttpException('Invalid User.');
		$personalDetail = Users::model()->findByPk(Yii::app()->user->id);
		if($personalDetail->role == 'dataentry')
			throw new CHttpException('Please donot try this again, You are not authorized.');
		if($personalDetail->role == 'headteacher' && $personalDetail->school_id != $userInformation->school_id)
			throw new CHttpException('Please donot try this again, You are not authorized.');
		$userInformation->status = 0;
		if($userInformation->update())
			$this->redirect(array('view','id'=>$userInformation->id));
		throw new CHttpException('Please try again.');
	}

	/**
	*@param int userid
	*/
	public function actionResetPassword($id){
		$userInformation = Users::model()->findByPk($id);
		if(empty($userInformation))
			throw new CHttpException('Invalid User.');
		$personalDetail = Users::model()->findByPk(Yii::app()->user->id);
		if($personalDetail->role == 'dataentry')
			throw new CHttpException('Please donot try this again, You are not authorized.');
		if($personalDetail->role == 'headteacher' && $personalDetail->school_id != $userInformation->school_id)
			throw new CHttpException('Please donot try this again, You are not authorized.');
		$password = bin2hex(random_bytes(10));
		$userInformation->raw_password = UtilityFunctions::encrypt($password);
		$userInformation->activkey = $this->encrypting(microtime() . $password);
		$userInformation->password = $this->encrypting($password);
		if($userInformation->update())
			$this->redirect(array('view','id'=>$userInformation->id));
		throw new CHttpException('Please try again.');
	}


	/**
	*@param int user id
	*/

	public function actionSendsms($id){
		$user_information = Users::model()->findByPk($id);
		$boardmember_information = SchoolBoardInformation::model()->find('user_id=:user_id AND status=:status',[':user_id'=>$id, ':status'=>1]);
		if(empty($boardmember_information) || empty($user_information))
			throw new CHttpException('Please try again.');
		$user_name  = $user_information->username;
		$password  = UtilityFunctions::encrypt_decrypt('decrypt',$user_information->raw_password);
		$message = 'Respected Sir, web link - http://103.69.125.135, username :'.$user_name.' && password : '.$password.', From Education Branch - '.ucwords(Yii::app()->params['municipality']);
		$mobile_number_1 = $boardmember_information->mobile_number;
		$mobile_number_2 = $boardmember_information->phone_number;
		$receiver_name = $boardmember_information->post.' : '.ucwords($boardmember_information->name);
        $encraftApiData = UtilityFunctions::encraftApi();
        $post_argument['api_key'] = $encraftApiData['key'];
        $post_argument['client_key'] = $encraftApiData['client_key'];
        $post_argument['mobile'] = $mobile_number_1;
        $post_argument['message'] = $message;
        $post_argument['event'] = 'username && password';
        $post_argument['ip_'] = Yii::app()->request->getUserHostAddress();
        $post_argument['message_id'] = '';

        //sms pending section due to unsufficient credit or un-active account status
        $response_code = 400;
        $remarks = '';
        $message_id = $credit_consumed = 0;
        $mobile_number = strlen($mobile_number_1) == 10 && is_numeric($mobile_number_1) ? $mobile_number_1 : $mobile_number_2;
        if(strlen($mobile_number) == 10 && is_numeric($mobile_number)){
            $args = http_build_query($post_argument);
            $url = $encraftApiData['send-message-url'];
            # Make the call using API.
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            // Response
            $response = curl_exec($ch);
            $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            $response_information = json_decode($response);
            $response_code = isset($response_information->response_code) ? $response_information->response_code : 400;
            $credit_consumed = isset($response_information->credit_consumed) ? $response_information->credit_consumed : $credit_consumed;
            $message_id = isset($response_information->message_id) ? $response_information->message_id : null;
            $remarks = isset($response_information->remarks) ? $response_information->remarks : null;
        }
        $status = $response_code == 200 ? 1 : 0;
        $message_log = new MessageReceiver();
        $message_log->ip_ = Yii::app()->request->getUserHostAddress();
        $message_log->mobile = $mobile_number;
        $message_log->name = $receiver_name;
        $message_log->message_code = $response_code;
        $message_log->message = $message;
        $message_log->credit_consumed = $credit_consumed;
        $message_log->message_id = $message_id;
        $message_log->remarks = '';//$remarks;
        $message_log->event='username && password';
        $message_log->status = $status; 
        $message_log->save(false);
		$this->redirect(array('view','id'=>$id));
	}

	/**
	*@param int userId, int boardmemberid
	*/
	public function actionMappingBoard(){
		if(isset($_POST['user_id']) && is_numeric($_POST['user_id']) && isset($_POST['board_member_id']) && is_numeric($_POST['board_member_id'])){
			$id = $_POST['user_id'];
			$board_member_id = $_POST['board_member_id'];
			$userInformation = Users::model()->findByPk($id);
			if(empty($userInformation))
				throw new CHttpException('Invalid User.');
			$personalDetail = Users::model()->findByPk(Yii::app()->user->id);
			if($personalDetail->role == 'dataentry')
				throw new CHttpException('Please donot try this again, You are not authorized.');
			if($personalDetail->role == 'headteacher' && $personalDetail->school_id != $userInformation->school_id)
				throw new CHttpException('Please donot try this again, You are not authorized.');
			$boardmemberInformation = SchoolBoardInformation::model()->find('id=:id AND school_id=:school_id AND status=:status',[':id'=>$board_member_id, ':school_id'=>$userInformation->school_id, ':status'=>1]);
			if(empty($boardmemberInformation))
				throw new CHttpException('Un-authorized Board Member/ Teacher.');
				$boardmemberInformation->user_id = $id;
				if($boardmemberInformation->update())
					$this->redirect(array('view','id'=>$userInformation->id));
				throw new CHttpException('Please try again.');
			}else{
				throw new CHttpException('Please donot try this again, You are not authorized.');
			}
	}


	/**
	*
	*/
	public function actionChangePassword(){
		$id = Yii::app()->user->id;
		$model = Users::model()->findByAttributes(array('id'=>$id));
		$model->setScenario('changePwd');
		if(isset($_POST['Users'])){
			$model->attributes = $_POST['Users'];
			$valid = $model->validate();
			if($valid){
				$model->raw_password = UtilityFunctions::encrypt_decrypt('encrypt',$model->new_password);
			  	$model->password = md5($model->new_password);
			  	if($model->validate() && $model->save())
				 	$this->redirect(array('changepassword','msg'=>'successfully changed password'));
			  	else
				 	$this->redirect(array('changepassword','msg'=>'password not changed'));
			}
		}

		$this->render('_change_password',['model'=>$model]);
	}

}
