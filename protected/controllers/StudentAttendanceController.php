<?php

class StudentAttendanceController extends RController
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}


	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new StudentAttendance;

	// Uncomment the following line if AJAX validation is needed
	// $this->performAjaxValidation($model);

		if(isset($_POST['StudentAttendance']))
		{
			$model->attributes=$_POST['StudentAttendance'];
			if($model->validate() && $model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

	// Uncomment the following line if AJAX validation is needed
	// $this->performAjaxValidation($model);

		if(isset($_POST['StudentAttendance']))
		{
			$model->attributes=$_POST['StudentAttendance'];
			if($model->validate() && $model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
	// we only allow deletion via POST request
			$this->loadModel($id)->delete();

	// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('StudentAttendance');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}



	/**
	*student list out 
	*/

	public function actionSelectClass(){
		$model=  new StudentAttendance;
		$this->render('general_', array('model'=>$model));
		Yii::app()->session->remove('total_schooling_days');
	}

	public function actionStudentAttendanceSec(){
		if(isset($_POST['StudentAttendance'])){
			$user_information = UtilityFunctions::UserInformations();
			$school_id_sec = isset($user_information) ? $user_information['school_id'] : 0;
			$academic_year = UtilityFunctions::AcademicYear();
			$school_id = isset($_POST['StudentAttendance']['school_id']) ? $_POST['StudentAttendance']['school_id'] : $school_id_sec;
			$school_information = BasicInformation::model()->findByPk($school_id);
			$terminal_id = isset($_POST['StudentAttendance']['terminal_id']) ? $_POST['StudentAttendance']['terminal_id'] : 0; 
			$order_by  = isset($_POST['StudentAttendance']['arrange_by']) ? $_POST['StudentAttendance']['arrange_by'] : 'name';
			Yii::app()->session['total_schooling_days'] = isset($_POST['StudentAttendance']['total_attendannce']) ? $_POST['StudentAttendance']['total_attendannce'] : 0;
			$terminal_information = Terminal::model()->findByPk($terminal_id);
			$student_attendance_list = StudentAttendance::model()->StudentAttendanceSection($academic_year, $school_id, $terminal_id, $order_by);
			$student_list = isset($student_attendance_list['student_list']) ? $student_attendance_list['student_list'] :  array();
			$student_attendance = isset($student_attendance_list['student_attendance']) ? $student_attendance_list['student_attendance'] :  array();
			$prev_attendace = isset($student_attendance_list['total_attendannce']) && $student_attendance_list['total_attendannce'] > 0 ? $student_attendance_list['total_attendannce'] : 0;
			$total_attendannce = isset(Yii::app()->session['total_schooling_days']) && Yii::app()->session['total_schooling_days'] !=0 ? Yii::app()->session['total_schooling_days']: $prev_attendace;
			$this->render('student_list',['student_list'=>$student_list, 'student_attendance'=>$student_attendance, 'academic_year'=>$academic_year,'terminal_id'=>$terminal_id, 'total_attendannce'=>$total_attendannce, 'terminal_information'=>$terminal_information,'school_information'=>$school_information,'school_id'=>$school_id]);
		}else
		$this->redirect(['SelectClass']);
	}

	public function actionInsertAttendace(){
		if(isset($_POST['academic_year']) && is_numeric($_POST['academic_year']) && isset($_POST['terminal_id']) && is_numeric($_POST['terminal_id']) && isset($_POST['school_id']) && is_numeric($_POST['school_id']) && isset($_POST['total_number']) && is_numeric($_POST['total_number']) && isset($_POST['total_terminal_class_days']) && is_numeric($_POST['total_terminal_class_days'])){
			$transacation = Yii::app()->db->beginTransaction();
			$error_log = array();
			$academic_year = $_POST['academic_year'];
			$terminal_id = $_POST['terminal_id'];
			$total_number = $_POST['total_number'];
			$school_id = $_POST['school_id'];
			$total_terminal_class_days = $_POST['total_terminal_class_days'];
			for ($i=0; $i < $total_number ; $i++) { 
				if(isset($_POST[$i.'_registration_number']) && is_numeric($_POST[$i.'_registration_number']) ){
					$registration_number = $_POST[$i.'_registration_number']; 
					$student_attendance_ = isset($_POST['student_attendance_'.$i.'_'.$registration_number]) && is_numeric($_POST['student_attendance_'.$i.'_'.$registration_number]) ? $_POST['student_attendance_'.$i.'_'.$registration_number] : 0;
					$student_attendance_checking = StudentAttendance::model()->findByAttributes(['academic_year'=>$academic_year, 'school_id'=>$school_id,'terminal_id'=>$terminal_id, 'student_id'=>$registration_number]);
					if(empty($student_attendance_checking)){
						$model = new StudentAttendance;
						$model -> academic_year = $academic_year;
						$model -> school_id = $school_id;
						$model -> terminal_id = $terminal_id;
						$model -> student_id = $registration_number;
						$model -> attendance = $student_attendance_;
						$model -> total_attendannce = $total_terminal_class_days;
						$model -> created_by = Yii::app()->user->id ;
						$model -> created_date = new CDbExpression('NOW()');
						if(!$model->validate() || !$model->save()){
							$error_log = 'false';
							echo "<pre>";
							echo print_r($model->errors);
							exit;
						}	
					}else{
						$student_attendance_checking -> attendance = $student_attendance_;
						$student_attendance_checking -> total_attendannce = $total_terminal_class_days;
						if(!$student_attendance_checking->validate() && !$student_attendance_checking->save()){
							$error_log = 'false';
							echo "<pre>";
							echo print_r($student_attendance_checking->errors);
							exit;
						}	
					}

				}
			}
			if(!in_array('false', $error_log)){
				$transacation->commit();
				$this->redirect(['SelectClass']);
			}
			else{
				$transacation->rollback();
				$this->redirect(['SelectClass']);
			}

		}
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new StudentAttendance('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['StudentAttendance']))
			$model->attributes=$_GET['StudentAttendance'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=StudentAttendance::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='student-attendance-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
