<?php

class StudentInformationController extends Controller
{/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
			'actions'=>array('index','downloadpdf','allRegisteredStudent'),
			'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
			'actions'=>array('View','Create','Update','Delete','Index','Admin','Downloadpdf','AllRegisteredStudent','ApprovalStudentList','ApprovedList','BelowAgeStudents','SchoolWiseStudentNumber','PrintAdmitCard','AdmitCardSample','AdmitCard','Pdf','AdminDatatable','ApprovedData','SchoolReport','StudentRegistrationReport','SampleList','UploadStudentExcel','InsertStudentList','GenerateAdDOB'),
			'users'=>array(Yii::app()->params['superadmin']),
			),
            array('deny', // deny all users
                'users' => array('*'),
            ),
		);
	}




	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$approvalBox = UtilityFunctions::VerifyAuthentication($id);
		if(isset($_POST['StudentInformation']))
		{
			$approvalData = $_POST['StudentInformation'];
			if($approvalBox && (new StudentInformation())->VerifyStudent($approvalData, $id))
				$this->redirect('approvalStudentList');
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');

		}
		$this->render('view',array(
			'model'=>$this->loadModel($id), 'approvalBox' => $approvalBox
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new StudentInformation;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['StudentInformation']))
		{
			$model->attributes=$_POST['StudentInformation'];
			if($model->validate() && $model->save())
				$this->redirect(array('view','id'=>$model->id));

		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['StudentInformation']))
		{
			$model->attributes=$_POST['StudentInformation'];
			if($model->validate() && $model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
	// we only allow deletion via POST request
			$this->loadModel($id)->delete();

	// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	/**
	* Manages all models.
	*/
	public function actionIndex()
	{
		$model=new StudentInformation('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['StudentInformation'])){
			$model->attributes=$_GET['StudentInformation'];
			if(isset($_GET['StudentInformation']['school_code']))
				$model->setAttribute("school_code",$_GET['StudentInformation']['school_code']);
			Yii::app()->SESSION['params'] = $_GET['StudentInformation'];
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	/**
	* 
	*/
	public function actionDownloadpdf(){
		if(isset(Yii::app()->SESSION['params']) && !empty(Yii::app()->SESSION['params'])){
			$academic_year = UtilityFunctions::AcademicYear();
			$school_id = isset(Yii::app()->SESSION['params']['school_id']) ? Yii::app()->SESSION['params']['school_id'] : null;
			$school_code = isset(Yii::app()->SESSION['params']['school_code']) ? Yii::app()->SESSION['params']['school_code'] : null;
			$sex = isset(Yii::app()->SESSION['params']['sex']) ? Yii::app()->SESSION['params']['sex'] : null;
			$order_by = isset(Yii::app()->SESSION['params']['order_by']) ? Yii::app()->SESSION['params']['order_by'] : null;
			$student_list = StudentInformation::model()->StudentList($academic_year, $school_id, $sex , null, null, $order_by);
			$school_detail = BasicInformation::model()->findByPk($school_id);

			if(empty($school_detail))
				$school_detail = BasicInformation::model()->find('schole_code=:schoolCode',[':schoolCode'=>$school_code]);
			if(empty($school_detail))
        		throw new CHttpException(400,'Invalid request for school. please try again.');
        	$resource_information = BasicInformation::model()->findByPk($school_detail->resource_center_id);
	        $mPDF1 = Yii::app()->ePdf->mpdf();
	    	# You can easily override default constructor's params
	        $mPDF1 = Yii::app()->ePdf->mpdf('UTF-8', 'A4');
	        $mPDF1->writeHTMLfooter=true;

	        //$mPDF1->AddPage('L');
	        $mPDF1->WriteHTML($this->renderPartial('_student_registration_report_pdf',['student_information'=>$student_list, 'school_information'=>$school_detail, 'resource_information'=>$resource_information], true));
	        unset(Yii::app()->session['params']);
        	$mPDF1->Output($file);
	        exit;
		}
		$this->redirect(array('index'));
	}



	/**
	* Manages all models.
	*/
	public function actionAllRegisteredStudent()
	{
		$model=new StudentInformation('allsearch');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['StudentInformation']))
			$model->attributes=$_GET['StudentInformation'];

		$this->render('_over_all_student',array(
			'model'=>$model,
		));
	}
	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new StudentInformation('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['StudentInformation']))
			$model->attributes=$_GET['StudentInformation'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	*student to be approved
	*/
	public function actionApprovalStudentList(){
		$this->layout= '_datatable_layout';
		$studentList = (new StudentInformation())->CanApporvedStudentList(null);
		$this->render('_to_be_approved');
	}



	/**
	*student to be approved
	*/
	public function actionApprovedList(){
		$this->layout= '_datatable_layout';/*
		$studentList = (new StudentInformation())->ApprovedList(null);
		echo "<pre>";
		echo print_r($studentList);
		exit;*/
		$this->render('_approved_list');
	}

	/**
	*  list of student below the age
	*/
	public function actionBelowAgeStudents(){
		$this->layout = '_datatable_layout';
		$model = new StudentInformation();
		$dateInformation = Yii::app()->params['limit'];
		$nepali_date_limit = isset($dateInformation['date']) ? $dateInformation['date'] : '2063-12-32';
		if(isset($_GET['StudentInformation']['dob_nepali']) && !empty($_GET['StudentInformation']['dob_nepali']))
			$nepali_date_limit = $_GET['StudentInformation']['dob_nepali'];
		$ad_date_limit = UtilityFunctions::NepaliToEnglish($nepali_date_limit);
		$student_list = StudentInformation::model()->findAll('don_english>:dob_ad OR don_english=:don_english',[':dob_ad'=>$ad_date_limit, ':don_english'=>'0000-00-00']);
		$this->render('_under_age_student_list',['student_list'=>$student_list, 'nepali_date_limit'=>$nepali_date_limit, 'model'=>$model]);
	}


	/**
	* School wise student report
	*/
	public function actionSchoolWiseStudentNumber(){
		$this->layout= '_datatable_layout';
		$this->render('_school_wise_student');
	}


	public function actionPrintAdmitCard($id){
		$this->layout = 'admitcardlayout';
		$is_superUser = UtilityFunctions::SuperUser();
		if($is_superUser)
			$student_information = $this->loadModel($id);
		else{
			$user_ = UtilityFunctions::UserInformations();
			$school_id = isset($user_['school_id']) ? $user_['school_id'] : null;
			$student_information = StudentInformation::model()->find('id=:id AND school_id=:school_id', [':id'=>$id, ':school_id'=>$school_id]);
		}
		if($student_information===null)
			throw new CHttpException(404,'The requested page does not exist.');
		$academicYear = $student_information->academic_year;
		$municipality = Yii::app()->params['municipality'];
		$address = Yii::app()->params['address'];
		$year_digit_1 = substr($academicYear, 2,1);
		$year_digit_2 = substr($academicYear, 3, 1);
		$studentNumber = $student_information->roll_number;
		$wardNumber = $student_information->school_sec ?  $student_information->school_sec->ward_no : null;
		$schoolCode = $student_information->school_sec ?  $student_information->school_sec->schole_code : null;
		$ward_digit_1 = $ward_digit_2 = $school_digit_1 = $school_digit_2 = $school_digit_3 = $student_digit_1 = $student_digit_2 = $student_digit_3 = 0; 
		if(strlen($wardNumber) > 1){
			$ward_digit_1 = substr($wardNumber, 0,1);
			$ward_digit_2 = substr($wardNumber, 1, 1);
		}else
			$ward_digit_2 = $wardNumber;
		if(strlen($schoolCode) > 2){
			$school_digit_1 = substr($schoolCode, 0,1);
			$school_digit_2 = substr($schoolCode, 1, 1);
			$school_digit_3 = substr($schoolCode, 2, 1);
		}elseif (strlen($schoolCode) == 2) {
			$school_digit_2 = substr($schoolCode, 0, 1);
			$school_digit_3 = substr($schoolCode, 1, 1);
		}else{
			$school_digit_3 = substr($schoolCode, 0, 1);
		}



		if(strlen($studentNumber) > 2){
			$student_digit_1 = substr($studentNumber, 0,1);
			$student_digit_2 = substr($studentNumber, 1, 1);
			$student_digit_3 = substr($studentNumber, 2, 1);
		}elseif (strlen($studentNumber) == 2) {
			$student_digit_2 = substr($studentNumber, 0, 1);
			$student_digit_3 = substr($studentNumber, 1, 1);
		}else{
			$student_digit_3 = substr($studentNumber, 0, 1);
		}
		$subjectArray = [];
		$subjectInformation = MarkObtained::model()->findAll('student_id=:student_id AND academic_year=:academic_year AND subject_status=:subject_status',[':student_id'=>$student_information->id, ':academic_year'=>$academicYear, ':subject_status'=>1]);
		if(!empty($subjectInformation)){
			foreach ($subjectInformation as $subject) {
				if(!in_array($subject->subject_name, $subjectArray))
					$subjectArray[]  = $subject->subject_name;
			}
		}
		if(empty($subjectArray)){
			$basicSubjectInformation = SubjectInformation::model()->findAll('status=:status',[':status'=>1]);
			if(!empty($basicSubjectInformation)){
				foreach ($basicSubjectInformation as $basicSubject) {
					if(!in_array($basicSubject->title, $subjectArray))
						$subjectArray[]  = $basicSubject->title;
				}
			}
		}
		$this->render('_print_admit_card',array('student_information'=>$student_information, 'year_digit_1'=>$year_digit_1, 'year_digit_2'=>$year_digit_2, 'ward_digit_1'=>$ward_digit_1, 'ward_digit_2'=>$ward_digit_2,'school_digit_1'=>$school_digit_1, 'school_digit_2'=>$school_digit_2, 'school_digit_3'=>$school_digit_3,'student_digit_1'=>$student_digit_1, 'student_digit_2'=>$student_digit_2, 'student_digit_3'=>$student_digit_3, 'subjectArray'=>$subjectArray , 'municipality'=>$municipality, 'address'=>$address));
	}

	/**
	* print admit card
	*/
	public function actionAdmitCardSample($id = 1){
		$is_superUser = UtilityFunctions::SuperUser();
		if($is_superUser)
			$student_information = $this->loadModel($id);
		else{
			$user_ = UtilityFunctions::UserInformations();
			$school_id = isset($user_['school_id']) ? $user_['school_id'] : null;
			$student_information = StudentInformation::model()->find('id=:id AND school_id=:school_id', [':id'=>$id, ':school_id'=>$school_id]);
		}
		if($student_information===null)
			throw new CHttpException(404,'The requested page does not exist.');

		$academicYear = $student_information->academic_year;

		$subjectInformation = MarkObtained::model()->findAll('student_id=:student_id AND academic_year=:academic_year AND subject_status=:subject_status',[':student_id'=>$student_information->id, ':academic_year'=>$academicYear, ':subject_status'=>1]);
		$subjectArray = [];
		if(!empty($subjectInformation)){
			foreach ($subjectInformation as $subject) {
				if(!in_array($subject->subject_name, $subjectArray))
					$subjectArray[]  = $subject->subject_name;
			}
		}
		if(empty($subjectArray)){
			$basicSubjectInformation = SubjectInformation::model()->findAll('status=:status',[':status'=>1]);
			if(!empty($basicSubjectInformation)){
				foreach ($basicSubjectInformation as $basicSubject) {
					if(!in_array($basicSubject->title, $subjectArray))
						$subjectArray[]  = $basicSubject->title;
				}
			}
		}
		$this->renderPartial('_admit_card',['student_information'=>$student_information, 'subjectArray'=>$subjectArray, 'academicYear'=>$academicYear]);
	}

	/**
	*/

	public function actionAdmitCard($id){
		$is_superUser = UtilityFunctions::SuperUser();
		if($is_superUser)
			$student_information = $this->loadModel($id);
		else{
			$user_ = UtilityFunctions::UserInformations();
			$school_id = isset($user_['school_id']) ? $user_['school_id'] : null;
			$student_information = StudentInformation::model()->find('id=:id AND school_id=:school_id', [':id'=>$id, ':school_id'=>$school_id]);
		}
		if($student_information===null)
			throw new CHttpException(404,'The requested page does not exist.');
		$academicYear = $student_information->academic_year;
		$municipality = Yii::app()->params['municipality'];
		$address = Yii::app()->params['address'];
		$year_digit_1 = substr($academicYear, 2,1);
		$year_digit_2 = substr($academicYear, 3, 1);
		$studentNumber = $student_information->roll_number;
		$wardNumber = $student_information->school_sec ?  $student_information->school_sec->ward_no : null;
		$schoolCode = $student_information->school_sec ?  $student_information->school_sec->schole_code : null;
		$ward_digit_1 = $ward_digit_2 = $school_digit_1 = $school_digit_2 = $school_digit_3 = $student_digit_1 = $student_digit_2 = $student_digit_3 = 0; 
		if(strlen($wardNumber) > 1){
			$ward_digit_1 = substr($wardNumber, 0,1);
			$ward_digit_2 = substr($wardNumber, 1, 1);
		}else
			$ward_digit_2 = $wardNumber;
		if(strlen($schoolCode) > 2){
			$school_digit_1 = substr($schoolCode, 0,1);
			$school_digit_2 = substr($schoolCode, 1, 1);
			$school_digit_3 = substr($schoolCode, 2, 1);
		}elseif (strlen($schoolCode) == 2) {
			$school_digit_2 = substr($schoolCode, 0, 1);
			$school_digit_3 = substr($schoolCode, 1, 1);
		}else{
			$school_digit_3 = substr($schoolCode, 0, 1);
		}



		if(strlen($studentNumber) > 2){
			$student_digit_1 = substr($studentNumber, 0,1);
			$student_digit_2 = substr($studentNumber, 1, 1);
			$student_digit_3 = substr($studentNumber, 2, 1);
		}elseif (strlen($studentNumber) == 2) {
			$student_digit_2 = substr($studentNumber, 0, 1);
			$student_digit_3 = substr($studentNumber, 1, 1);
		}else{
			$student_digit_3 = substr($studentNumber, 0, 1);
		}
		$subjectArray = [];
		$subjectInformation = MarkObtained::model()->findAll('student_id=:student_id AND academic_year=:academic_year AND subject_status=:subject_status',[':student_id'=>$student_information->id, ':academic_year'=>$academicYear, ':subject_status'=>1]);
		if(!empty($subjectInformation)){
			foreach ($subjectInformation as $subject) {
				if(!in_array($subject->subject_name, $subjectArray))
					$subjectArray[]  = $subject->subject_name;
			}
		}
		if(empty($subjectArray)){
			$basicSubjectInformation = SubjectInformation::model()->findAll('status=:status',[':status'=>1]);
			if(!empty($basicSubjectInformation)){
				foreach ($basicSubjectInformation as $basicSubject) {
					if(!in_array($basicSubject->title, $subjectArray))
						$subjectArray[]  = $basicSubject->title;
				}
			}
		}
        # mPDF
        //$mPDF1 = Yii::app()->ePdf->mpdf();

        # You can easily override default constructor's params
        $mPDF1 = Yii::app()->ePdf->mpdf('utf-8', 'A4');
        
        $mPDF1->WriteHTML($this->renderPartial('_admit_card',array('student_information'=>$student_information, 'year_digit_1'=>$year_digit_1, 'year_digit_2'=>$year_digit_2, 'ward_digit_1'=>$ward_digit_1, 'ward_digit_2'=>$ward_digit_2,'school_digit_1'=>$school_digit_1, 'school_digit_2'=>$school_digit_2, 'school_digit_3'=>$school_digit_3,'student_digit_1'=>$student_digit_1, 'student_digit_2'=>$student_digit_2, 'student_digit_3'=>$student_digit_3, 'subjectArray'=>$subjectArray , 'municipality'=>$municipality, 'address'=>$address), true));
        $mPDF1->Output($file);
	}
	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=StudentInformation::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='student-information-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionPdf(){
		//create a TCPDF instance with the params from tcpdfOptions from config/main.php
		 $pdf=Yii::app()->pdfFactory->getTCPDF(); 

		 //use this instance like explained in [TCPDF examples](http://www.tcpdf.org/examples.php "") 
		 $pdf->SetCreator(PDF_CREATOR);
		 $pdf->SetAuthor('Nicola Asuni');
		 $pdf->addPage();
		 $html =  mb_detect_encoding('नेपाली', 'HTML-ENTITIES', "UTF-8");
		 $pdf->write(0, $html);
		 $pdf->Output();
	}


	public function actionAdminDatatable(){
		$school_id = 1;
		$studentList = (new StudentInformation())->CanApporvedStudentList(null);
		$studentListArrayData = ["data"=>$studentList];
		echo json_encode($studentListArrayData);
		exit;
	}

	public function actionApprovedData(){
		$school_id = 1;
		$studentList = (new StudentInformation())->ApprovedList(null);
		$studentListArrayData = ["data"=>$studentList];
		echo json_encode($studentListArrayData);
		exit;
	}

	/**
	*school wise report
	*/
	public function actionSchoolReport(){
		$schoolReportSection = (new StudentInformation())->SchoolWiseStudent();
		$schoolListArray = ["data"=>$schoolReportSection];
		echo json_encode($schoolListArray);
		exit;
	}


	/**
	*
	*/
	public function actionStudentRegistrationReport(){
		$model = new StudentInformation();
		if(isset($_POST['StudentInformation'])){
			$school_id = isset($_POST['StudentInformation']['school_id']) ? $_POST['StudentInformation']['school_id'] : null;
			$order_by = isset($_POST['StudentInformation']['order_by']) ? $_POST['StudentInformation']['order_by'] : null;
			$academic_year = isset($_POST['StudentInformation']['academic_year']) ? $_POST['StudentInformation']['academic_year'] : null;
			$school_information = BasicInformation::model()->findByPk($school_id);
			if(empty($school_information))
        		throw new CHttpException(400,'Invalid request for school. please try again.');
        	$title = ucwords($school_information->title).' - student registration report.pdf';
        	$resource_information = BasicInformation::model()->findByPk($school_information->resource_center_id	);
			$student_information = StudentInformation::model()->StudentList($academic_year, $school_id, null , null, null, $order_by);
			$mPDF1 = Yii::app()->ePdf->mpdf();
        # You can easily override default constructor's params
            $mPDF1 = Yii::app()->ePdf->mpdf('UTF-8', 'A4');
            $mPDF1->writeHTMLfooter=true;
            $mPDF1->SetHTMLFooter('<div style="text-align: center">{PAGENO} / {nbpg}</div>');

            //$mPDF1->AddPage('L');
            $mPDF1->WriteHTML($this->renderPartial('_student_registration_report_pdf',['student_information'=>$student_information, 'school_information'=>$school_information, 'academic_year'=>$academic_year, 'resource_information'=>$resource_information], true));
            $mPDF1->Output($title, 'I');

		}
		$this->render('_student_registration_report',['model'=>$model]);
	}

	/**
	*same report of s
	*/

	/**
	* download student list sample
	*/
	public function actionSampleList(){
        $column = ['registration_number','symbol_number','first_name','middle_name','last_name','sex','father_name','mother_name','dob_bs','dob_ad','optional_subject','permanent_address','enroll_class','enroll_year'];
        $student_information[0]['registration_number'] = '1231241321';
        $student_information[0]['symbol_number'] = '34533123A';
        $student_information[0]['first_name'] = 'sovit';
        $student_information[0]['middle_name'] = 'bahadur';
        $student_information[0]['last_name'] = 'thapa';
        $student_information[0]['sex'] = 'male';
        $student_information[0]['father_name'] = 'dhan bahadur thapa';
        $student_information[0]['mother_name'] = 'sita kumari thapa';
        $student_information[0]['dob_bs'] = '2060-01-21';
        $student_information[0]['dob_ad'] = '2003-04-04';
        $student_information[0]['optional_subject'] = 'computer';
        $student_information[0]['permanent_address'] = 'Tanahun-5, Bhimad';
        $student_information[0]['enroll_class'] = '1';
        $student_information[0]['enroll_year'] = '2064';
        $this->widget('EExcelView', array(
            'dataProvider'=>new CArrayDataProvider(
                $student_information,array(
                    'keyField'=>'school_name'
                )),
            'grid_mode'=>'export',
            'title'=>'Result Legder',
            'filename'=>'student sample list',
            'stream'=>true,
            'autoWidth'=>false,
            'exportType'=>'Excel2007',
            'columns'=>$column,
        ) ); 

	}

    /**
    *student ledger in data table formate due to error in excel download yii1
    */
    public function actionUploadStudentExcel(){
        $model = new StudentInformation();
        $school_id = '';
        $column = ['registration_number','symbol_number','first_name','middle_name','last_name','sex','father_name','mother_name','dob_bs','dob_ad','optional_subject','permanent_address','enroll_class','enroll_year'];
        $excel_array_data = $school_information = array();
        if(isset($_POST['StudentInformation']) && isset($_POST['StudentInformation']['school_id'])){
            $school_id = $_POST['StudentInformation']['school_id'];
            $school_information = BasicInformation::model()->findByPk($school_id);
            if(empty($school_information))
        		throw new CHttpException(400,'Invalid request. Please Select Valid School.');
			$student_excel_files = CUploadedFile::getInstance($model, 'upload_files');
			if (empty($student_excel_files))
        		throw new CHttpException(400,'Invalid request. Please Select Ecxel File.');
			$nameInformation = $student_excel_files->name;
			$excel_files = 'student_excel_'.time().$nameInformation;
        	$strPath = 'images/upload_files/' . $excel_files;
            $file_upload_section = CUploadedFile::getInstance($model, 'upload_files')->saveAs($strPath, true);
            if(!empty($file_upload_section)){
                Yii::import('ext.PHPExcel.Classes.PHPExcel',true);
                $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                $objPHPExcel = $objReader->load('images/upload_files/'.$excel_files); //$file --> your filepath and filename
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
                $highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5
                
                for ($row = 2; $row <= $highestRow; ++$row) {
                  for ($col = 0; $col < $highestColumnIndex; ++$col) {
                  	$value = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                  	if(in_array($col, [8,9]) && !preg_match('/^(\d{4})-(\d{2})-(\d{2})$/', $value)){
                  		$value = PHPExcel_Style_NumberFormat::toFormattedString($value, "YYYY-M-D");
                  	}
                    $excel_array_data[$row-2][$objWorksheet->getCellByColumnAndRow($col, 1)->getValue()] = $value;
                }
            }
        }else
        throw new CHttpException(400,'Invalid request. Excel file is not saved.');
    }
    $this->render('_upload_school_wise_student',['model'=>$model, 'excel_array_data'=>$excel_array_data, 'school_information'=>$school_information, 'column'=>$column, 'school_id'=>$school_id]);
    }

    public function actionInsertStudentList(){
    	if(isset($_POST['symbol_number']) && isset($_POST['school_id'])){
    		$school_id = $_POST['school_id'];
            $school_information = BasicInformation::model()->findByPk($school_id);
            if(empty($school_information))
        		throw new CHttpException(400,'Invalid request. Please Select Valid School.');
        	$insertStudent = StudentInformation::model()->InsertStudentList($_POST);
        	if($insertStudent)
        		$this->redirect(['/studentInformation/index']);
        	throw new CHttpException(400,'Invalid request. Please insert again.');
    	}
        throw new CHttpException(400,'Invalid request. Excel file is not saved.');
    }


    /**
	* convert nepali date into ad
    */

    public function actionGenerateAdDOB(){
    	$transaction = Yii::app()->db->beginTransaction();
    	$student_list = StudentInformation::model()->findAll();
    	if(!empty($student_list)){
    		foreach ($student_list as $student) {
    			$dob_bs = $student->dob_nepali;
    			$bod_ad = '0000-00-00';
    			if(!empty($dob_bs))
    				$bod_ad = UtilityFunctions::NepaliToEnglish($dob_bs);
    			$student->don_english = $bod_ad;
    			if(!$student -> update()){
    				$transaction->rollback();
    				echo "<pre>";
    				echo print_r($student->errors);
    				exit;
    			}
    		}
    	}
    	$transaction->commit();
    	die('dob AD date');

    }


}
