<?php

class MarkObtainedController extends Controller
{
    /**
    * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
    * using two-column layout. See 'protected/views/layouts/column2.php'.
    */
    /**
    * @return array action filters
    */
    public function filters()
    {
            return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
    * Specifies the access control rules.
    * This method is used by the 'accessControl' filter.
    * @return array access control rules
    */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
            'actions'=>array('chooseSubject','uploadFile','StudentList','InsertMarks','downloadStudentLedger','uploadStudentLedger','previewRawMarks','ledgerAudit','insertBulkLegerMarks','ledger','subjectSchoolWiseGrade'),
            'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
            'actions'=>array('View','Create','Update','Delete','Index','Admin','AuditReport','LedgerAudit','ChooseSubject','StudentList','InsertMarks','PreviewRawMarks','ResultPublishDate','Ledger','PrepareFinalLedger','UploadFile','UploadStudentLedger','InsertBulkLegerMarks','DownloadStudentLedger','ReArrangeGrade','RetakeReport','RetakeStudentSummery','RetakeReportStudentWise','SubjectSchoolWiseGrade','SubjectWiseGradeStudentNumber','SubjectStudentList','SelectStudent','InsertRetakeMark','RetakeSchoolWiseDetail','GraceUpgrade', 'GraceAuditReport','AbsentMarksStudentAudit','RetakeSubjectWiseDetail','RetakeStudentNumber','AbsentStudentsList'),
            'users'=>array(Yii::app()->params['superadmin']),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }



    /**
    *
    */
    public function actionAuditReport(){
        $this->layout = '_datatable_layout';
        $model = new MarkObtained();
        $academic_year = isset($_POST['MarkObtained']['academic_year']) ? $_POST['MarkObtained']['academic_year'] : UtilityFunctions::AcademicYear();
        $auditReportInformation = MarkObtained::model()->StudentAuditDetail($academic_year);
        $auditReport = $auditReportInformation['schoolAuditDetail'];
        $this->render('_general_audit_report',['auditReport'=>$auditReport, 'auditReportInformation'=>$auditReportInformation, 'model'=>$model,'academic_year'=>$academic_year]);
    }

    /**
    * absent mark obtained
    */
    public function actionAbsentMarksStudentAudit(){
        $this->layout = '_datatable_layout';
        $absent_audit_report = MarkObtained::model()->StudentAbsentReport(UtilityFunctions::AcademicYear());
        $student_detail_array = $absent_audit_report['student_detail_array'];
        $subject_header = $absent_audit_report['subject_header'];
        $mark_detail_information = $absent_audit_report['mark_detail_information'];
        $student_id_array = $absent_audit_report['student_id_array'];
        $this->render('_student_absent_audit_report',['student_id_array'=>$student_id_array, 'student_detail_array'=>$student_detail_array, 'subject_header'=>$subject_header, 'mark_detail_information'=>$mark_detail_information]);
    }


    /**
    *
    */
    public function actionAbsentStudentsList(){
        $model = new MarkObtained();
        $student_id_array = $student_detail_array = [];
        $this->layout = '_datatable_layout';
        if(isset($_POST['MarkObtained'])){
            $academic_year = isset($_POST['MarkObtained']['academic_year']) ? $_POST['MarkObtained']['academic_year'] : UtilityFunctions::AcademicYear();
            $absent_audit_report = MarkObtained::model()->AbsentStudentList($academic_year);
            $student_detail_array = $absent_audit_report['absent_student_detail'];
            $student_id_array = $absent_audit_report['student_id_array'];
            if($_POST['MarkObtained']['options'] == 'pdf'){
                $mPDF1 = Yii::app()->ePdf->mpdf();
            # You can easily override default constructor's params
                $mPDF1 = Yii::app()->ePdf->mpdf('UTF-8', 'A4');
                $mPDF1->SetHTMLFooter('<div style="text-align: center">{PAGENO} / {nbpg}</div>');
                $mPDF1->writeHTMLfooter=true;
                $mPDF1->WriteHTML($this->renderPartial('_absent_students_list_pdf',['student_id_array'=>$student_id_array, 'student_detail_array'=>$student_detail_array, 'model'=>$model, 'academic_year'=>$academic_year], true));
                $mPDF1->Output($file);

            }


        }
        $this->render('_absent_students_list',['student_id_array'=>$student_id_array, 'student_detail_array'=>$student_detail_array, 'model'=>$model]);
    }
    /**
    *
    */
    public function actionLedgerAudit($id){
        $academic_year = UtilityFunctions::AcademicYear();
        $subjectInformation = SubjectInformation::model()->SubjectDetail($id);
        $school_information = BasicInformation::model()->findByPk($id);
        $auditReportInformation = MarkObtained::model()->LedgerAudit($id, $academic_year);
        $opt_is_practical = isset($auditReportInformation['opt_is_practical']) ? $auditReportInformation['opt_is_practical'] : null;
        $column = isset($auditReportInformation['columns']) ? $auditReportInformation['columns'] : null;
        $dataArray = isset($auditReportInformation['dataArray']) ? $auditReportInformation['dataArray'] : null;
        $mPDF1 = Yii::app()->ePdf->mpdf();
    # You can easily override default constructor's params
        $mPDF1 = Yii::app()->ePdf->mpdf('UTF-8', 'A4');
        $mPDF1->SetHTMLFooter('<div style="text-align: center">{PAGENO} / {nbpg}</div>');
        $mPDF1->writeHTMLfooter=true;

        $mPDF1->AddPage('L');
        $mPDF1->WriteHTML($this->renderPartial('_ledger_audit_pdf',['dataArray'=>$dataArray, 'column'=>$column, 'school_information'=>$school_information, 'academic_year'=>$academic_year, 'subjectInformation'=>$subjectInformation, 'opt_is_practical'=>$opt_is_practical], true));
        $mPDF1->Output($file);
    }

    /**
    * Choose Subject and Terminal
    */
    public function actionChooseSubject(){
    	$model = new MarkObtained;
        $is_save = $message = null;
        $is_save = isset($_GET['status']) ? $_GET['status'] : null;
        if($is_save && $is_save == 1)
            $message = " Marks Has Been Inserted, Please Insert Next Mark.";
        if($is_save && $is_save == 0)
            $message = " Marks Doesnot Saved, Please Try Again.";
        $this->render('choose_subject', array('model'=>$model, 'is_save'=>$is_save, 'message'=>$message));
    }

    /**
    * Student List Based On School
    */
    public function actionStudentList(){
        $model = new MarkObtained;
        $student_information = $column = array();
        if(isset($_POST['MarkObtained'])){
            /*working on it , error on school id section*/
            $user_information = UtilityFunctions::UserInformations();
            $academic_year = UtilityFunctions::AcademicYear();
            $school_id_sec = isset($user_information) ? $user_information['school_id'] : 0;
            $subject_id = isset($_POST['MarkObtained']['subject_id']) ? $_POST['MarkObtained']['subject_id'] : null;
            $school_id = isset($_POST['MarkObtained']['school_id']) ? $_POST['MarkObtained']['school_id'] : $school_id_sec;
            $order_by = isset($_POST['MarkObtained']['arrange_by']) ? $_POST['MarkObtained']['arrange_by'] : null;
            $insert_type = isset($_POST['MarkObtained']['insert_type']) ? $_POST['MarkObtained']['insert_type'] : 'both';
            $previous_link = isset($_POST['MarkObtained']['prv_link']) ? $_POST['MarkObtained']['prv_link'] : null;
            $terminal_id = 1;
            $subjectDetailData = SubjectInformation::model()->findByPk($subject_id);
            $school_information = BasicInformation::model()->findByPk($school_id);
            if(empty($subjectDetailData) || empty($school_information))
                throw new CHttpException(400,'Invalid request. please try again.');
            $subject_information = MarkObtained::model()->TerminalSubjectDetails($academic_year, $school_id, $terminal_id, $subject_id);
            $is_practical = (isset($subject_information['is_practical']) && $subject_information['is_practical']== 1) || $subjectDetailData->subject_nature == 3 ? 1 : null;
            $subject_information['subject_name'] = empty($subject_information['subject_name']) ? ucwords($subjectDetailData->title) : $subject_information['subject_name'];
            $model = StudentInformation::model()->StudentMarklist($academic_year, $school_id, $order_by, $subject_id);
            if(isset($_POST['MarkObtained']['sep_th_fm'])){
                $optional_subject = explode('-', $_POST['MarkObtained']['sep_th_fm']);
                $theory_full_mark = isset($optional_subject[0]) ? (int) $optional_subject[0] : 0;
                $subject_information['theory_full_mark'] = $theory_full_mark;
                $subject_information['theory_pass_mark'] = $theory_full_mark > 0 ? (int) 40*$theory_full_mark/100 : 0;
                $practical_full_mark = isset($optional_subject[1]) ? (int) $optional_subject[1] : 0;
                $subject_information['practical_full_mark'] = $practical_full_mark;
                $subject_information['practical_pass_mark'] = $practical_full_mark > 0 ? (int) 40*$practical_full_mark/100 : 0;
                $is_practical = 0;
                if($practical_full_mark)
                    $is_practical = 1;
                $subject_information['is_practical'] = $is_practical;
            }

            if(isset($_POST['MarkObtained']['options']) && $_POST['MarkObtained']['options'] == 'download_student_list'){
                $th_marks = $is_practical ? 'TH_Marks' : "Obt_Marks";
                $column[] = 'school_name';
                $column[] = 'school_code';
                $column[] = 'registration_number';
                $column[] = 'name';
                $column[] = 'symbol_number';
                if(in_array($insert_type, ['both','theory']))
                    $column[] = $th_marks;
                if($is_practical && in_array($insert_type, ['both','practical'])){$column[] = 'PRT_Marks';}
                $column[] = 'subject_id';
                $column[] = 'subject_name';
                if(!empty($model)){
                    $sn =0 ;
                    foreach ($model as $mark_infor) {
                        $student_information[$sn]['school_name'] = !empty($school_information)  ? $school_information->title : "null";
                        $student_information[$sn]['school_code'] = !empty($school_information)  ? $school_information->schole_code : "null";
                        $student_information[$sn]['registration_number'] = $mark_infor->id;
                        $student_information[$sn]['name'] = ucwords($mark_infor->first_name." ".$mark_infor->middle_name." ".$mark_infor->last_name);
                        $student_information[$sn]['symbol_number'] = $mark_infor->symbol_number;
                        $marks_information = MarkObtained::model()->findByAttributes(['subject_id'=>$subject_id,'student_id'=>$mark_infor->id,'academic_year'=>$academic_year,'school_id'=>$school_id]);

                        if(in_array($insert_type, ['both','theory']))
                            $student_information[$sn][$th_marks] = !empty($marks_information) ? $marks_information->theory_mark : 0 ;
                        if($is_practical && in_array($insert_type, ['both','practical'])){$student_information[$sn]['PRT_Marks'] = !empty($marks_information) ? $marks_information->practical_mark : 0 ;}
                        $student_information[$sn]['subject_id'] = $subject_id;
                        $student_information[$sn]['subject_name'] = !empty($subjectDetailData) ? $subjectDetailData->title : 'null'; 
                        $sn++;
                    }
                }
                $this->widget('EExcelView', array(
                    'dataProvider'=>new CArrayDataProvider(
                        $student_information,array(
                            'keyField'=>'school_name'
                        )),
                    'grid_mode'=>'export',
                    'title'=>'Result Legder',
                    'filename'=>ucwords($school_information->title.'-'.$subjectDetailData->title)."-".$academic_year,
                    'stream'=>true,
                    'autoWidth'=>false,
                    'exportType'=>'Excel2007',
                    'columns'=>$column,
                ) );   
            }
            if($subjectDetailData && $subjectDetailData->subject_nature == 3 && !isset($_POST['MarkObtained']['sep_th_fm'])){
                $model = new MarkObtained();
                $model->attributes = $_POST['MarkObtained'];
                $order_by = isset($_POST['MarkObtained']['arrange_by']) ? $_POST['MarkObtained']['arrange_by'] : 'name';
                $options = isset($_POST['MarkObtained']['options']) ? $_POST['MarkObtained']['options'] : 'insert_marks';
                $this->render('_assign_optional_full_mark', ['model'=>$model, 'order_by'=>$order_by, 'options'=>$options]);
                exit;
            }
            $this->render('_student_list',array(
                'model'=>$model,
                'school_information' => $school_information,
                'insert_type' => $insert_type,
                'previous_link' => $previous_link,
                'academic_year'=>$academic_year,
                'terminal_id' => $terminal_id,
                'subjectid'=>$subject_id,
                'school_id'=>$school_id,
                'subject_information'=>$subject_information,
            ));
        }
        else
            $this->redirect(['ChooseSubject']);
    }





    /**
     * to insert the terminal marks. problem in school id section;
     */

    // should work on redirection section
    public function actionInsertMarks()
    {
        $mess = '';
        $error_array = $result_updated_student_array = array();
        if(isset($_POST['subject_id']) && isset($_POST['school_id']) && isset($_POST['total_number']) && isset($_POST['academic_year']) && isset($_POST['total_number']) && is_numeric($_POST['total_number']))
        {
            $cas_mark_setting = Yii::app()->params['cas_setting']['combine_mark']; // mark setting section
            $subject_id = isset($_POST['subject_id']) ? $_POST['subject_id'] : null;
            $terminal_id = isset($_POST['terminal_id']) && $_POST['terminal_id'] != 0 ? $_POST['terminal_id'] : 1;
            $previous_link = isset($_POST['previous_link']) ? $_POST['previous_link'] : null;
            $academic_year = isset($_POST['academic_year']) ? $_POST['academic_year'] : UtilityFunctions::AcademicYear();
            $school_id = isset($_POST['school_id']) ? $_POST['school_id'] : null;
            $school_information = BasicInformation::model()->findByPk($school_id);
            $subject_information = SubjectInformation::model()->findByPk($subject_id);
            if(empty($school_information) || empty($subject_information)){
                if($previous_link)
                    $this->redirect(array($previous_link.'?status=0'));
                $this->redirect('ChooseSubject?status=0');
            }
            $total_number_counting = isset($_POST['total_number']) ? $_POST['total_number'] : 0;
            $rules_information = MarkObtained::model()->TerminalSubjectDetails($academic_year, $school_id, $terminal_id, $subject_id);
            $terminal_percent = isset($rules_information['terminal_percent']) ? $rules_information['terminal_percent'] :  0;
            $cas_percent = isset($rules_information['cas_percent']) ? $rules_information['cas_percent'] :  0;
            $percent_for_final = isset($rules_information['percent_for_final']) ? $rules_information['percent_for_final'] :  0;
            $terminal_ = isset($rules_information['terminal_']) ? $rules_information['terminal_'] :  0;
            $subject_name = isset($rules_information['subject_name']) ? $rules_information['subject_name'] :  'null';
            $is_practical = isset($rules_information['is_practical']) ? $rules_information['is_practical'] :  0;
            $theory_full_mark = isset($rules_information['theory_full_mark']) ? $rules_information['theory_full_mark'] :  0;
            $practical_full_mark = isset($rules_information['practical_full_mark']) ? $rules_information['practical_full_mark'] :  0;
            $theory_pass_mark = isset($rules_information['theory_pass_mark']) ? $rules_information['theory_pass_mark'] :  0;
            $practical_pass_mark = isset($rules_information['practical_pass_mark']) ? $rules_information['practical_pass_mark'] :  0;


            if(in_array($subject_information->subject_nature, [2,3])){
                $subject_name = ucwords($subject_information->title);
                $theory_full_mark = isset($_POST['theory_full_mark']) ? $_POST['theory_full_mark'] : 0;
                $practical_full_mark = isset($_POST['practical_full_mark']) ? $_POST['practical_full_mark'] : 0;
                $is_practical = $practical_full_mark != 0  ? 1 : 0;
                $theory_pass_mark = $theory_full_mark != 0 ?  $theory_full_mark*40/100 : 0;
                $practical_pass_mark = $practical_full_mark != 0 ?  $practical_full_mark*40/100 : 0;
            }

            $combine_full_mark = $theory_full_mark + $practical_full_mark;
            $transaction = Yii::app()->db->beginTransaction();
            for ($i=0; $i < $total_number_counting; $i++) {
                if(isset($_POST[$i.'_registration_number']) && is_numeric($_POST[$i.'_registration_number'])){

                    $registration_num = isset($_POST[$i.'_registration_number']) && is_numeric($_POST[$i.'_registration_number']) ? $_POST[$i.'_registration_number'] : 0 ;
                    $MarkObtained = MarkObtained::model()->findByAttributes(['academic_year'=>$academic_year, 'terminal_id'=>$terminal_id, 'student_id'=>$registration_num, 'subject_status'=>1, 'subject_id'=>$subject_id]);
                    $student_information = StudentInformation::model()->findByAttributes(['id'=>$registration_num, 'status'=>1]);
                    if(empty($student_information))
                        continue;
                    $prv_th = $MarkObtained ? $MarkObtained->theory_mark  : 0;
                    $prv_pr = $is_practical && $MarkObtained ? $MarkObtained->practical_mark  : 0;
                    $post_theory = isset($_POST[$registration_num.'_'.$i.'_theory_mark']) && $theory_full_mark >= $_POST[$registration_num.'_'.$i.'_theory_mark'] ? $_POST[$registration_num.'_'.$i.'_theory_mark'] :  0;

                    $post_practical = isset($_POST[$registration_num.'_'.$i.'_practical_mark']) && $theory_full_mark >= $_POST[$registration_num.'_'.$i.'_practical_mark'] ? $_POST[$registration_num.'_'.$i.'_practical_mark'] :  0;

                    $theory_mark = isset($_POST[$registration_num.'_'.$i.'_theory_mark']) && $theory_full_mark >= $_POST[$registration_num.'_'.$i.'_theory_mark'] ? $_POST[$registration_num.'_'.$i.'_theory_mark'] :  $prv_th;
                    $practical_mark = isset($_POST[$registration_num.'_'.$i.'_practical_mark']) && $practical_full_mark >= $_POST[$registration_num.'_'.$i.'_practical_mark'] ? $_POST[$registration_num.'_'.$i.'_practical_mark'] : $prv_pr;
                    $total_number = $theory_mark + $practical_mark;
                    if(empty($MarkObtained)){
                        $MarkObtained__ = new MarkObtained;
                        $MarkObtained__ -> academic_year = $academic_year;
                        $MarkObtained__ -> terminal_id = $terminal_id;
                        $MarkObtained__ -> terminal_ = $terminal_;
                        $MarkObtained__ -> student_id = $registration_num;
                        $MarkObtained__ -> subject_id = $subject_id;
                        $MarkObtained__ -> subject_name = $subject_name ;
                        $MarkObtained__ -> theory_mark = ceil($theory_mark);
                        $MarkObtained__ -> practical_mark = ceil($practical_mark);
                        $MarkObtained__ -> total_mark = ceil($total_number); 
                        $MarkObtained__ -> is_practical = $is_practical;
                        $MarkObtained__ -> cas_mark = 0;
                        $MarkObtained__ -> cas_grade = '';
                        $MarkObtained__ -> cas = '';
                        $MarkObtained__ -> theory_pass_mark = ceil($theory_pass_mark);
                        $MarkObtained__ -> practical_pass_mark = ceil($practical_pass_mark);
                        $MarkObtained__ -> theory_full_mark = ceil($theory_full_mark);
                        $MarkObtained__ -> practical_full_mark = ceil($practical_full_mark);
                        $MarkObtained__ -> terminal_percent = ceil($terminal_percent);
                        $MarkObtained__ -> cas_percent = ceil($cas_percent);
                        $MarkObtained__ -> percent_for_final = ceil($percent_for_final);
                        $MarkObtained__ -> subject_status = 1;
                        $MarkObtained__ -> grace_mark = 0;
                        $MarkObtained__ -> school_id = $school_id;
                        if(!$MarkObtained__->validate() || !$MarkObtained__->save()){
                            echo "<pre>";
                            echo print_r($MarkObtained__->errors);
                            exit;
                            $error_array[] ='false';
                        }
                    }else{
                        $MarkObtained -> theory_mark = ceil($theory_mark);
                        $MarkObtained -> practical_mark = ceil($practical_mark);
                        $MarkObtained -> total_mark = ceil($total_number); 
                        $MarkObtained -> is_practical = $is_practical;
                        $MarkObtained -> theory_pass_mark = ceil($theory_pass_mark);
                        $MarkObtained -> practical_pass_mark = ceil($practical_pass_mark);
                        $MarkObtained -> theory_full_mark = ceil($theory_full_mark);
                        $MarkObtained -> practical_full_mark = ceil($practical_full_mark);
                        $MarkObtained -> terminal_percent = ceil($terminal_percent);
                        $MarkObtained -> cas_percent = ceil($cas_percent);
                        $MarkObtained -> percent_for_final = ceil($percent_for_final);
                        $MarkObtained -> grace_mark = 0;
                        $MarkObtained -> cas_mark = 0;
                        $MarkObtained -> cas_grade = '';
                        $MarkObtained -> cas = '';
                        $MarkObtained -> school_id = $school_id;
                        $MarkObtained -> subject_status = 1;
                        if(!$MarkObtained->validate() || !$MarkObtained->update()){
                            echo "<pre>";
                            echo print_r($MarkObtained->errors);
                            exit;
                            $error_array[] ='false';
                        }
                    }

                    if($prv_th != $post_theory && $prv_pr != $post_practical){
                       $result_update_section = MarkObtained::model()->ResultUpdate($registration_num, $academic_year);
                       //if($result_update_section)
                            $result_updated_student_array[] = $registration_num;
                    }


                }
            }
            if(in_array('false', $error_array)){
                $transaction->rollback();
                if($previous_link)
                    $this->redirect(array($previous_link.'?status=0'));
                $this->redirect('ChooseSubject?status=0');
            }
            $transaction->commit();
            if($previous_link)
                $this->redirect(array($previous_link.'?status=1'));
            $this->redirect('ChooseSubject?status=1');

        }
        else
            $this->redirect(['ChooseSubject']);
    }


    /**
    * Raw Mark Preview
    */

    public function actionPreviewRawMarks(){

        $user_ = UtilityFunctions::UserInformations();
        $school_id = isset($user_['school_id']) ? $user_['school_id'] : null;
        $ledgerHeader = $class_name = $academic_year = $is_practical = $excel_header = $school_information = $subject_information = null ;
        $model = new MarkObtained();
        $start_time = microtime(true); 
        $terminal ="";
        $student_information_new = $column = $student_information = $subject_wise_total = $terminal_information = $cas_terminal_information = $is_practical_ = array();
        if(isset($_GET['MarkObtained'])){
            $information = StudentInformation::model()->StudentRawMarks($_GET['MarkObtained']);
            $academic_year = UtilityFunctions::AcademicYear();
            $terminal_id = 1;
            $user_information = UtilityFunctions::UserInformations();
            $school_id_sec = isset($user_information) ? $user_information['school_id'] : 0;
            $school_id = UtilityFunctions::ShowSchool() && !is_numeric($_GET['MarkObtained']['school_id']) ? $school_id_sec : (int) $_GET['MarkObtained']['school_id'];

            $school_information = BasicInformation::model()->findByPk($school_id);
            if(empty($school_information))
                throw new CHttpException(400,'Invalid request. Excel file is not saved.');
            $excel_header = !empty($school_information) ? ucwords($school_information -> title) : 'null';
            $ledger_type = isset($_GET['MarkObtained']['ledger_type']) ? $_GET['MarkObtained']['ledger_type'] : 'general';
            $subject_information = isset($information['student_subject']) ? $information['student_subject'] : array();
            $student_information_sec = isset($information['student']) ? $information['student'] : array();
            $student_attendance = isset($information['attendance']) ? $information['attendance'] : array();
            $exam_attendance = isset($information['exam_attendance']) ? $information['exam_attendance'] : array();
            $column[] ='school_name';
            $column[] ='school_code';
            $column[] ='name';
            $column[] ='symbol_number';
            if(!empty($subject_information)){
                foreach ($subject_information as $information) {
                    $terminal_percent = (int) $information -> terminal_percent;
                    $cas_percent = (int) $information -> cas_percent;
                    $theory_pass_mark = $information -> theory_pass_mark * $terminal_percent/100;
                    $theory_full_mark = $information -> theory_full_mark * $terminal_percent/100;
                    $practical_full_mark = $information -> practical_full_mark * $terminal_percent/100;
                    $practical_pass_mark = $information -> practical_pass_mark * $terminal_percent/100;
                    $terminal_information[] = $information->subject_name.'-'.$theory_pass_mark.'-'.$theory_full_mark.'-'.$practical_pass_mark.'-'.$practical_full_mark;
                    $theory_title = $information->is_practical && $information->is_practical !=0 ? '_theory' :'';
                    $theory_title .= $information->is_cas && $information->is_cas !=0 ? '_terminal_'.$terminal_percent : ''; 
                    $practical_title = $information->is_cas && $information->is_cas !=0 ? '_practical_terminal_'.$terminal_percent : '_practical'; 
                    $subject_name = UtilityFunctions::ColumnName($information -> subject_name);
                    $column[] = $subject_name.$theory_title;
                    if($information->is_practical && $information->is_practical !=0)
                        $column[] = $subject_name.$practical_title;
                    if($information->is_cas && $information->is_cas !=0)
                        $column[] = $subject_name.'_cas_'.$cas_percent;
                    if($information->is_practical || $information->is_cas)
                        $column[]= $subject_name.'_total';
                    if($information->is_practical){$is_practical_[] = 'true';}else{$is_practical_[] = 'false';}

                }
                $cas_terminal_information['cas'] = $cas_percent;
                $cas_terminal_information['terminal'] = $terminal_percent;
            }
            $is_practical = in_array('true', $is_practical_) ? 1 : null;
            if($ledger_type=='general'){
                $column[] ='Total';
                $column[] ='Full_Mark';
                $column[] ='Percentage';
                /*$column[] ='Result';
                $column[] ='Attendance';*/
            }
            $i=0;
            if(!empty($student_information_sec)){
                foreach ($student_information_sec as $student) {
                    $registration_number = $student->id;

                    $student_information[$i]['school_name'] = isset($student) && isset($student->school_sec) ? $student->school_sec->title : 'null';
                    $student_information[$i]['school_code'] = isset($student) && isset($student->school_sec) ? $student->school_sec->schole_code : 'null';

                    $student_information[$i]['name'] = ucwords($student->first_name." ".$student->middle_name." ".$student->last_name);
                    $student_information[$i]['symbol_number'] = isset($student) ? $student->symbol_number : 'null';
                    $marks_information = $student->marks;
                    $total_obatined_ = $total_full_marks = 0;
                    $subject_wise_pass = array();
                    if(!empty($marks_information)){
                        foreach ($marks_information as $information_) {
                            $theory_mark = $information_->theory_mark;
                            $practical_mark = $information_->practical_mark;
                            $cas_mark = $information_->cas_mark;
                            $student_id = $information_->student_id;
                            $terminal_percent = (int) $information_->terminal_percent;
                            $cas_percent = (int) $information_->cas_percent;
                            $subject_name = UtilityFunctions::ColumnName($information_ -> subject_name);
                            $total_full_marks += $information_->theory_full_mark + $information_->practical_full_mark;
                            $theory_pass_mark = $information_->theory_pass_mark;
                            $practical_pass_mark = $information_ -> practical_pass_mark;
                            $subject_wise_pass[] = $information_ -> status;

                            $total = (int) $theory_mark + (int) $practical_mark ;
                            if($ledger_type=='general'){
                             $therory_mrk = !in_array($information_->theory_grade, ['D','E','Abs<sup>*</sup>']) ? $theory_mark : $theory_mark.'*' ;
                             $practical_mrk =  !in_array($information_->practical_grade, ['D','E','Abs<sup>*</sup>'])  ? $practical_mark : $practical_mark."*";
                             $cas_mrk = $cas_mark*$cas_percent/100;
                             $total_mrk = !is_numeric($therory_mrk) || !is_numeric($practical_mrk) ? $total."*" : $total;
                         }else{
                             $therory_mrk =  strip_tags($information_->theory_grade);
                             $practical_mrk =  strip_tags($information_->practical_grade);
                             $cas_mrk = strip_tags($information_->cas_grade);
                             $total_mrk = strip_tags($information_->total_grade);

                         }
                        if($information_->is_practical && $information_->is_practical !=0){
                            $student_information[$i][$subject_name.'_theory'] = $therory_mrk;
                            $student_information[$i][$subject_name.'_practical'] = $practical_mrk;
                            $student_information[$i][$subject_name.'_total'] = $total_mrk;

                        }else{
                            $student_information[$i][$subject_name] =  $total_mrk;
                        }
                        $subject_wise_total[$student_id][] = (int) $total_mrk;
                        $total_obatined_ += (int) $total_mrk;

                    }
                }
                if($ledger_type=='general'){
                    $student_information[$i]['Total'] = $total_obatined_;
                    $student_information[$i]['Full_Mark'] = $total_full_marks;
                    $student_information[$i]['Percentage'] = $total_full_marks != 0 ? number_format($total_obatined_*100/$total_full_marks, 2) : 0;
                    $student_information[$i]['Result'] = !in_array('fail', $subject_wise_pass) ?  'Pass' : 'fail';
                    //$student_information[$i]["Attendance"] = $student_attendance && isset($student_attendance[$registration_number]) ? $student_attendance[$registration_number] : 'Not Set';
                }

                $i++;
            }
        }
        $student_information_new = new CArrayDataProvider(
            $student_information, 
            array(
                'pagination'=>array(
                    'pageSize'=>40,
                ),
                'keyField'=>'name'
            ));

    }
    $end_time = microtime(true);
        //dividing with 60 will give the execution time in minutes other wise seconds
    $execution_time = ($end_time - $start_time);
    $this->render('_preview_raw_marks',['StudentList'=>$student_information_new, 'column'=>$column,'execution_time'=>$execution_time,'student_information'=>$student_information,'model'=>$model,'ledgerHeader'=>$ledgerHeader,'class_name'=>$class_name,'academic_year'=>$academic_year,'terminal'=>$terminal,'terminal_information'=>$terminal_information, 'cas_terminal_information'=>$cas_terminal_information, 'is_practical'=>$is_practical, 'excel_header'=>$excel_header,'school_id'=>$school_id, 'school_information'=>$school_information, 'subject_information'=>$subject_information]);
    }


    /**
    *verify terminal result section
    */

    public function actionResultPublishDate(){
        $model = new Result;
        $academic_year = UtilityFunctions::AcademicYear();
        if(isset($_POST['Result']) && isset($_POST['Result']['result_publish_date'])){
            $nepali_date = UtilityFunctions::EnglishToNepali(date('Y-m-d'));
            $result_publish_date = isset($_POST['Result']['result_publish_date']) ? $_POST['Result']['result_publish_date'] : $nepali_date;
            $public_information = explode('-', $result_publish_date);
            $year = isset($public_information[0]) ? $public_information[0] : null;
            $month = isset($public_information[1]) ? $public_information[1] : null;
            $day = isset($public_information[2]) ? $public_information[2] : null;
           /* $can_publish_result = UtilityFunctions::CanPublishResult();
            if($academic_year != $year || $month > 12 || $day > 32 || !$can_publish_result)
                throw new CHttpException(400,'Invalid result date. please try again.');
            $result_section = Result::model()->find('academic_year=:academic_year AND status=:status',[':academic_year'=>$academic_year,':status'=>1]);
            if(!empty($result_section)){
                $this->render('_regenerate_result');
            }*/
            $terminal_result = MarkObtained::model()->GenerateTerminalResult(1,  $result_publish_date);
            if($terminal_result)
                $this->redirect(['ledger']);
            throw new CHttpException(400,'Invalid request. Result cannot generate,please try again.');
        }
        else{
            $this->render('_result_publish_date', array('model'=>$model, 'academic_year'=>$academic_year));
        }
    }


    public function actionLedger(){
        $model = new MarkObtained();
        $school_information = $result_date = null;
        $result_information = $subject_information = $raw_result_information = array();
        if(isset($_GET['MarkObtained'])){
            $is_grace_mark = Yii::app()->params->is_grace_mark;
            $model->attributes = $_GET['MarkObtained'];
            $ledger_type = isset($_GET['MarkObtained']['ledger_type']) ?  $_GET['MarkObtained']['ledger_type'] : 'pdf';
            $academic_year = isset($_GET['MarkObtained']['academic_year']) ?  $_GET['MarkObtained']['academic_year'] : UtilityFunctions::AcademicYear();
            $result_ = isset($_GET['MarkObtained']['result_']) ?  $_GET['MarkObtained']['result_'] : 'pdf';
            $user_information = UtilityFunctions::UserInformations();
            $school_id_sec = isset($user_information) ? $user_information['school_id'] : 0;
            $school_id = isset($_GET['MarkObtained']['school_id']) ?  $_GET['MarkObtained']['school_id'] : $school_id_sec;
            $school_information = BasicInformation::model()->findByPk($school_id);
            if(empty($school_information))
                throw new CHttpException(400,'Invalid request. please try again.');
            $subject_information = MarkObtained::model()->SubjectInMarksLedger($_GET['MarkObtained']);
            $result_detail_information = StudentInformation::model()->TerminalResultLedger($_GET['MarkObtained']);
            $result_information = isset($result_detail_information['student_information']) ? $result_detail_information['student_information'] : [];
            $result_date = isset($result_detail_information['result_date']) ? $result_detail_information['result_date'] : date('Y-m-d');
            $raw_result_information = new CArrayDataProvider(
                $result_information, 
                array(
                    'pagination'=>array(
                        'pageSize'=>40,
                    ),
                    'keyField'=>'name'
                ));
            $title = 'Ledger-'.ucwords($school_information->title).'-'.$academic_year;
            if($ledger_type == 'pdf'){
                # mPDF
                $mPDF1 = Yii::app()->ePdf->mpdf();
                # You can easily override default constructor's params
                $mPDF1 = Yii::app()->ePdf->mpdf('UTF-8', 'A3');
                $mPDF1->writeHTMLfooter=true;
                $mPDF1->defaultfooterline = 0;
                $mPDF1->SetHTMLFooter('<div style="text-align: center">{PAGENO} / {nbpg}</div>');
                $mPDF1->AddPage('L');
                $mPDF1->WriteHTML($this->renderPartial('_ledger_pdf',['result_information'=> $result_information, 'raw_result_information'=>$raw_result_information, 'subject_information'=>$subject_information, 'school_information'=>$school_information, 'result_'=>$result_,'result_date'=>$result_date, 'academic_year'=>$academic_year], true));
                $mPDF1->Output($title.'.pdf', 'I');
                    //die('working');
        }
    }
    $this->render('_ledger',['model'=>$model, 'result_information'=> $result_information, 'raw_result_information'=>$raw_result_information, 'subject_information'=>$subject_information, 'school_information'=>$school_information, 'result_date'=>$result_date]);

    }

    public function PrepareFinalLedger($ResultInformation, $status){
        $column = $student_information = $array_section = $attendance_ = $is_practical_ = $terminal_information = $cas_terminal_information = array();
        $is_practical = null;

        $passed_student = $failed_student = $distinction_student = $first_student = $second_student = $third_student = $i = $academic_year = $class_id = 0;
        $is_grace_mark = Yii::app()->params->is_grace_mark;
        if(!empty($ResultInformation)){
            $subject_information = isset($ResultInformation['subject_information']) ? $ResultInformation['subject_information'] : array();
            $student_information_sec = isset($ResultInformation['result_information']) ? $ResultInformation['result_information'] : array();
            $column[] ='name';
            $column[] ='section';
            $column[] ='roll_number';
            if(!empty($subject_information)){
                foreach ($subject_information as $information) {
                    $academic_year = $information -> academic_year;
                    $class_id = $information -> class_id;
                    $first_final_percent =(int) $information->first_final_percent;
                    $second_final_percent = (int) $information->second_term_final_percent;
                    $third_final_percent = (int) $information->third_term_final_percent;
                    $terminal_percent = (int) $information -> terminal_percent;
                    $cas_percent = (int) $information -> cas_percent;
                    $theory_pass_mark = $information -> theory_pass_mark * $terminal_percent/100;
                    $theory_full_mark = $information -> theory_full_mark * $terminal_percent/100;
                    $practical_full_mark = $information -> practical_full_mark * $terminal_percent/100;
                    $practical_pass_mark = $information -> practical_pass_mark* $terminal_percent/100;
                    $terminal_information[] = $information->subject_name.'-'.$theory_pass_mark.'-'.$theory_full_mark.'-'.$practical_pass_mark.'-'.$practical_full_mark;
                    $theory_title = $information->is_practical && $information->is_practical !=0 ? '_theory' :'';
                    $theory_title .= $information->is_cas && $information->is_cas !=0 ? '_terminal_'.$terminal_percent : ''; 
                    $practical_title = $information->is_cas && $information->is_cas !=0 ? '_practical_terminal_'.$terminal_percent : '_practical'; 
                    $subject_name = UtilityFunctions::ColumnName($information -> subject_name);
                    $column[] = $subject_name.$theory_title.'_1st_'.$first_final_percent;
                    if($information->is_practical && $information->is_practical !=0)
                        $column[] = $subject_name.$practical_title.'_1st_'.$first_final_percent;
                    $column[] = $subject_name.$theory_title.'_2nd_'.$second_final_percent;
                    if($information->is_practical && $information->is_practical !=0)
                        $column[] = $subject_name.$practical_title.'_2nd_'.$second_final_percent;
                    $column[] = $subject_name.$theory_title.'_3rd_'.$third_final_percent;
                    if($information->is_practical && $information->is_practical !=0)
                        $column[] = $subject_name.$practical_title.'_3rd_'.$third_final_percent;
                    if($is_grace_mark)
                        $column[]=$subject_name.'_grace_mark';
                    if($information->is_cas && $information->is_cas !=0){
                        $column[] = $subject_name.'_cas_'.$cas_percent;
                        $column[]= $subject_name.'_terminal';
                    }
                    $column[]= $subject_name.'_total';
                    if($information->is_practical){$is_practical_[] = 'true';}else{$is_practical_[] = 'false';}

                }
                $cas_terminal_information['cas'] = $cas_percent;
                $cas_terminal_information['terminal'] = $terminal_percent;
            }
            $is_practical = in_array('true', $is_practical_) ? 1 : null;
            $column[] ='Total';
            $column[] ='Full_Mark';
            $column[] ='Result';
            if($status!='not-approved')
                $column[] ='Ranks';
            $column[] ='Percentage';
            $column[] ='Division';
            $column[] ='Attendance';
            $column[] ='Total_Attendance';

            if($status=='not-approved'){
                $std_criteria = new CDbCriteria;
                $std_criteria -> select = 'registration_number , sum(attendance) as attendance , sum(total_attendannce) as total_attendannce ';
                $std_criteria -> condition = 'academic_year=:academic_year AND class_id=:class_id';
                $std_criteria -> params = [':academic_year'=>$academic_year, ':class_id'=>$class_id];
                $std_criteria -> group = 'registration_number';
                $attendance_terminal_information= StudentAttendance::model()->findAll($std_criteria);
                if(!empty($attendance_terminal_information)){
                    foreach ($attendance_terminal_information as $attendance_info)
                        $attendance_[$attendance_info->registration_number] = $attendance_info->attendance.'/'.$attendance_info->total_attendannce;
                }

            }
            $marks_convertor = MarksConvertor::model()->findAll();
            if(!empty($student_information_sec)){
                foreach ($student_information_sec as $student) {
                    $registration_number = $student->id;
                    $student_information[$i]['name'] = isset($student) && isset($student->registration) ? $student->registration->name : 'null';
                    $student_information[$i]['section'] = isset($student) && isset($student->classSection) ? $student->classSection->title : 'null';
                    $student_information[$i]['roll_number'] = isset($student) ? $student->roll_number : 'null';
                    $marks_information = $student->FinalMarks;
                    $result_information = isset($student->Result) ? $student->Result : null;
                    $total_obatined_ = $total_full_marks = $percentage = $attendance = $total_attendance = $rank = $other_final_percent = 0;
                    $subject_wise_pass = array();
                    if(!empty($marks_information)){
                        foreach ($marks_information as $information_) {
                            $first_theory = number_format($information_->first_theory,2);
                            $first_practical = number_format($information_->first_practical,2);
                            $first_cas = number_format($information_->first_cas,2);
                            $second_theory = number_format($information_->second_theory,2);
                            $second_practical = number_format($information_->second_practical,2);
                            $second_cas = number_format($information_->second_cas,2);
                            $third_theory = number_format($information_->third_theory,2);
                            $third_practical = number_format($information_->third_practical,2);
                            $third_cas = number_format($information_->third_cas,2);
                            $other_mark = number_format($information_->other_mark);
                            $grace_marks = number_format($information_->grace_marks);
                            $first_final_percent = (int) $information_->first_final_percent;
                            $second_final_percent = (int) $information_->second_term_final_percent;
                            $third_final_percent = (int) $information_->third_term_final_percent;
                            $other_final_percent = (int) $information_->other_final_percent;
                            $subject_wise_pass[] = $information_ -> status;
                            $theory_pass_mark = $information_ -> theory_pass_mark != 0 ? $information_ -> theory_pass_mark * $information_ -> terminal_percent/100 : 0;
                            $theory_full_mark = $information_ -> theory_full_mark != 0 ? $information_ -> theory_full_mark * $information_ -> terminal_percent/100 : 0;
                            $practical_full_mark = $information_ -> practical_full_mark != 0 ?  $information_ -> practical_full_mark * $information_ -> terminal_percent/100 : 0;
                            $practical_pass_mark = $information_ -> practical_pass_mark != 0 ?  $information_ -> practical_pass_mark* $information_ -> terminal_percent/100 : 0;
                            $total_full_marks += ($information_ -> theory_full_mark  + $information_ -> practical_full_mark);

                            $total_theory = $first_theory + $second_theory + $third_theory + $other_mark + $grace_marks;
                            $total_practical = $first_practical + $second_practical + $third_practical;
                            $total_cas = $first_cas + $second_cas + $third_cas;

                            $total_obtained_mark = $total_theory + $total_practical + $total_cas ;

                            $raw_cas = $total_cas != 0 ? ($total_cas*100)/$cas_percent : 0;
                            $cas_ = $raw_cas != 0 ? $raw_cas*100/($information_ -> theory_full_mark + $information_ -> practical_full_mark) : 0;

                            $theory_title = $information_->is_practical && $information_->is_practical !=0 ? '_theory' :'';
                            $theory_title .= $information_->is_cas && $information_->is_cas !=0 ? '_terminal_'.(int) $information_->terminal_percent : ''; 
                            $practical_title = $information_->is_cas && $information_->is_cas !=0 ? '_practical_terminal_'.(int) $information_->terminal_percent : '_practical'; 
                            $subject_name = UtilityFunctions::ColumnName($information_ -> subject_name);
                            $student_information[$i][$subject_name.$theory_title.'_1st_'.$first_final_percent] = $first_theory;
                            if($information_->is_practical && $information_->is_practical !=0)
                                $student_information[$i][$subject_name.$practical_title.'_1st_'.$first_final_percent] = $first_practical;
                            $student_information[$i][$subject_name.$theory_title.'_2nd_'.$second_final_percent] = $second_theory;
                            if($information_->is_practical && $information_->is_practical !=0)
                                $student_information[$i][$subject_name.$practical_title.'_2nd_'.$second_final_percent] = $second_practical;
                            $student_information[$i][$subject_name.$theory_title.'_3rd_'.$third_final_percent] = $third_theory;
                            if($information_->is_practical && $information_->is_practical !=0)
                                $student_information[$i][$subject_name.$practical_title.'_3rd_'.$third_final_percent] = $third_practical;
                            if($is_grace_mark)
                                $student_information[$i][$subject_name.'_grace_mark'] = $grace_marks;
                            if($information_->is_cas && $information_->is_cas !=0){
                                $grade = "-";
                                if(!empty($marks_convertor)){
                                    foreach ($marks_convertor as $divgard) {
                                        if($divgard->min_range <= $cas_ && $divgard->max_range >= $cas_ && $divgard->type=='grade')
                                            $grade = $divgard -> code ;
                                    }
                                }
                                $student_information[$i][$subject_name.'_cas_'.(int) $information_->cas_percent]= $grade;
                                $student_information[$i][$subject_name.'_terminal']= $total_practical + $total_theory;
                            }
                            $student_information[$i][$subject_name.'_total'] = $total_theory >= $theory_pass_mark ? $total_obtained_mark :  $total_obtained_mark."*";
                            $total_obatined_ += $total_obtained_mark;
                        }
                    }

                    $division = $grade = $result_status = '';
                    if(!empty($result_information)){
                        foreach ($result_information as $result) {
                            $total_obatined_ = $result->total_obtained_mark;
                            $total_full_marks = $result->total_full_mark;
                            $percentage = $result->percentage;
                            $attendance = $result->attendance;
                            $total_attendance = $result->total_attendance;
                            $rank = $result->rank;
                            $division = $result->division;
                            $grade = $result->grade;
                            $result_status = $result->result_status;
                        }
                    }
                    $percentage = $total_full_marks != 0 ? $total_obatined_*100/$total_full_marks : 0;
                    if($status=='not-approved'){
                        if(!in_array('fail', $subject_wise_pass)){$result_status = 'pass'; }else{$result_status = 'fail'; }
                        if(!empty($marks_convertor)){
                            foreach ($marks_convertor as $divgard) {
                                if($divgard->min_range <= $percentage && $divgard->max_range >= $percentage && $divgard->type=='terminal')
                                    $division = $divgard -> code;
                            }
                        }
                        $attendance_information = isset($attendance_[$registration_number]) ? explode('/',$attendance_[$registration_number]) :array(); 
                        $attendance = isset($attendance_information[0]) ? $attendance_information[0] : 0;
                        $total_attendance = isset($attendance_information[1]) ? $attendance_information[1] : 0;
                    }
                    if($result_status=='pass')
                        $passed_student += 1;
                    if($result_status=='fail')
                        $failed_student += 1;
                    if($division=='Distinction' && $result_status!='fail')
                        $distinction_student += 1;
                    if($division=='First Division' && $result_status!='fail')
                        $first_student += 1;
                    if($division=='Second Division' && $result_status!='fail')
                        $second_student += 1; 
                    if($division=='Third Division' && $result_status!='fail')
                        $third_student += 1; 

                    $student_information[$i]['Total'] = $total_obatined_ ;
                    $student_information[$i]['Full_Mark'] = $total_full_marks;
                    $student_information[$i]['Result'] = $result_status;
                    if($status!='not-approved')
                        $student_information[$i]['Ranks'] = $rank;
                    $student_information[$i]['Percentage'] = number_format($percentage, 2)." %";
                    $student_information[$i]['Division'] = $division;
                    $student_information[$i]['Attendance'] = $attendance;
                    $student_information[$i]['Total_Attendance'] = $total_attendance;
                    $i++;
                }
            }
        }
        $result_sec['passed_student'] = $passed_student;
        $result_sec['failed_student'] = $failed_student;
        $result_sec['distinction_student'] = $distinction_student;
        $result_sec['first_student'] = $first_student;
        $result_sec['second_student'] = $second_student;
        $result_sec['third_student'] = $third_student;
        $final_result['first_final_percent'] = isset($first_final_percent) ? $first_final_percent : 0;
        $final_result['second_final_percent'] = isset($second_final_percent) ? $second_final_percent : 0;
        $final_result['third_final_percent'] = isset($third_final_percent) ? $third_final_percent : 0;
        $final_result['other_final_percent'] = isset($other_final_percent) ? $other_final_percent : 0;
        $array_section['column'] = $column;
        $array_section['terminal_information'] = $terminal_information;
        $array_section['cas_terminal'] = $cas_terminal_information;
        $array_section['student_information'] = $student_information;
        $array_section['student_result_sec'] = $result_sec;
        $array_section['final_terminal'] = $final_result;
        $array_section['is_practical'] = $is_practical;
        return $array_section;

    }

    /*upload excel ledger file start*/
    /**
    * Upload CSV or EXCEL     
    */
    public function  actionUploadFile(){
        $model = new MarkObtained;
        $insert_type = 'both';
        $school_id = $terminal_id = $subject_id = null;
        $excel_array_data = $rules_information = $school_information = $subject_information =  array();
        if(isset($_POST['MarkObtained'])){
            $terminal_id = 1;
            $insert_type = isset($_POST['MarkObtained']['insert_type']) ?  $_POST['MarkObtained']['insert_type'] : 'both';
            $user_information = UtilityFunctions::UserInformations();
            $school_id_sec = isset($user_information) ? $user_information['school_id'] : 0;
            $school_id = !$school_id_sec && is_numeric($_POST['MarkObtained']['school_id']) ? $_POST['MarkObtained']['school_id'] : $school_id_sec;
            $excel_files = CUploadedFile::getInstance($model, 'upload_files');
            $strPath = 'images/upload_files/' . $excel_files;
            $file_upload_section = CUploadedFile::getInstance($model, 'upload_files')->saveAs($strPath, true);
            if(!empty($file_upload_section)){
                Yii::import('ext.PHPExcel.Classes.PHPExcel',true);
                $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                $objPHPExcel = $objReader->load('images/upload_files/'.$excel_files); //$file --> your filepath and filename
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
                $highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5
                $is_practical = in_array($insert_type, ['both','practical']) ?  1 : null;  
                $th_marks = $is_practical ? 'TH_Marks' : "Obt_Marks";
                $max_theory_mark = $max_practical_mark = 0;
                for ($row = 2; $row <= $highestRow; ++$row) {
                  for ($col = 0; $col <= $highestColumnIndex; ++$col) {
                    $excel_array_data[$row-2][$objWorksheet->getCellByColumnAndRow($col, 1)->getValue()] = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                    $theory_mark = isset($excel_array_data[$row-2]['TH_Marks']) ? $excel_array_data[$row-2]['TH_Marks'] : 0;
                    $practical_mark = isset($excel_array_data[$row-2]['PRT_Marks']) ? $excel_array_data[$row-2]['PRT_Marks'] : 0;
                }

            }
            $school_code =  isset($excel_array_data[0]['school_code']) ? $excel_array_data[0]['school_code'] : 0;
            $school_information = BasicInformation::model()->findByAttributes(['schole_code'=>$school_code]);
            $subject_id =  isset($excel_array_data[0]['subject_id']) ? $excel_array_data[0]['subject_id'] : 0;
            $subject_information = SubjectInformation::model()->findByPk($subject_id);
            $rules_information = MarkObtained::model()->TerminalSubjectDetails(UtilityFunctions::AcademicYear(), $school_id, $terminal_id, $subject_id);


        }else{
            die('excel files doesnot save');
        }
    }
    $this->render('upload_choose_subject_school',['model'=>$model,'excel_array_data'=>$excel_array_data,'rules_information'=>$rules_information,'school_id'=>$school_id, 'terminal_id'=>$terminal_id, 'school_information'=>$school_information, 'subject_id'=>$subject_id,'academic_year'=> UtilityFunctions::AcademicYear(), 'insert_type'=>$insert_type, 'subject_information'=>$subject_information]);
    }

    /**
    *upload student raw marks in ledger
    */
    public function actionUploadStudentLedger(){
        $model = new MarkObtained();
        $school_id = $previous_link = '';
        $insert_type = 'both';
        $default_subject = true;
        $excel_array_data = $subjectColumnHeader = $school_information = $subject_detail  = $students_optional_subjects = array();
        if(isset($_POST['MarkObtained']) && isset($_POST['MarkObtained']['school_id'])){
            $school_id = isset($_POST['MarkObtained']['school_id']) ? $_POST['MarkObtained']['school_id'] : null;
            $primary_optional_subject = SubjectInformation::model()->findByAttributes(['subject_nature'=>3,'default_optional'=>1,'status'=>1]);
            $secondary_optional_subject = SubjectInformation::model()->findByAttributes(['subject_nature'=>3,'default_optional'=>0,'school_id'=>$school_id,'status'=>1]);
            $school_information = BasicInformation::model()->findByPk($school_id);
            $insert_type = isset($_POST['MarkObtained']['insert_type']) ? $_POST['MarkObtained']['insert_type'] : 'both';
            $previous_link = isset($_POST['MarkObtained']['prv_link']) ? $_POST['MarkObtained']['prv_link'] : '';
            $initialColumn = ['student_id','name','registration_number','symbol_number'];
            $subject_information = SubjectInformation::model()->StubjectDetail($initialColumn, $insert_type, $school_id);
            $subjectColumnHeader = isset($subject_information['column']) ? $subject_information['column'] : $initialColumn;
            $subject_detail = isset($subject_information['subject_information']) ? $subject_information['subject_information'] : [];
            $excel_files = CUploadedFile::getInstance($model, 'upload_files');
            $strPath = 'images/upload_files/' . $excel_files;
            $file_upload_section = CUploadedFile::getInstance($model, 'upload_files')->saveAs($strPath, true);
            if(!empty($file_upload_section)){
                Yii::import('ext.PHPExcel.Classes.PHPExcel',true);
                $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                $objPHPExcel = $objReader->load('images/upload_files/'.$excel_files); //$file --> your filepath and filename
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
                $highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5
                
                for ($row = 2; $row <= $highestRow; ++$row) {
                    $subject_column = [];
                    $student_id = $objWorksheet->getCellByColumnAndRow(0, $row)->getValue();
                    if(empty($student_id))
                        continue;
                    for ($col = 0; $col < $highestColumnIndex; ++$col) {
                        $student_information = StudentInformation::model()->find('id=:id AND school_id=:school_id',[':id'=>$student_id, ':school_id'=>$school_id]);
                        $excel_array_data[$row-2]['student_id'] = $student_id;
                        $excel_array_data[$row-2]['name'] = $student_information ? ucwords($student_information->first_name.' '.$student_information->middle_name.' '.$student_information->last_name) : 'not-set';
                        $excel_array_data[$row-2]['registration_number'] = $student_information ? $student_information->registration_number : 'not-set';
                        $excel_array_data[$row-2][$objWorksheet->getCellByColumnAndRow($col, 1)->getValue()] = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                        if(!in_array($objWorksheet->getCellByColumnAndRow($col, 1)->getValue(), ['student_id','name','symbol_number']))
                        $subject_column[] = $objWorksheet->getCellByColumnAndRow($col, 1)->getValue();
                    }
                    $primary_opt_name = $primary_optional_subject ? strtolower($primary_optional_subject->title) : null;
                    if(!empty($secondary_optional_subject)){
                        $secondary_opt_name = strtolower($secondary_optional_subject->title);
                        $primary_opt_name = $primary_optional_subject ? strtolower($primary_optional_subject->title) : null;
                        if(in_array($secondary_opt_name.'_theory', $subject_column) || in_array($secondary_opt_name.'_practical', $subject_column)){
                            $default_subject = false;
                            $is_secondary_optional_subject = StudentSubject::model()->find('student_id=:student_id AND subject_id=:subject_id AND status=:status',[':student_id'=>$student_id, ':subject_id'=>$secondary_optional_subject->id, ':status'=>'active']);
                            if(!empty($is_secondary_optional_subject)){
                                $students_optional_subjects[$student_id][$secondary_opt_name.'_theory'] = true;
                                $students_optional_subjects[$student_id][$secondary_opt_name.'_practical'] = true;
                            }else{
                                $students_optional_subjects[$student_id][$primary_opt_name.'_theory'] = true;
                                $students_optional_subjects[$student_id][$primary_opt_name.'_practical'] = true;
                            }
                        }
                    }
            }
        }else
        throw new CHttpException(400,'Invalid request. Excel file is not saved.');
    }
    $this->render('_schoolwise_ledger_upload',['model'=>$model, 'excel_array_data'=>$excel_array_data, 'subjectColumnHeader'=>$subjectColumnHeader, 'school_information'=>$school_information, 'school_id'=>$school_id, 'insert_type'=>$insert_type, 'previous_link'=>$previous_link, 'default_subject'=>$default_subject, 'students_optional_subjects'=>$students_optional_subjects]);
    }


    /**
    *@param student marks in ledger of single school\
    *@return true for insert/ false for uncusses
    */
    public function actionInsertBulkLegerMarks(){
        ini_set('max_input_vars', 2000);
        if(isset($_POST['student_id_'])){
            $error_array = [];
            $school_id = isset($_POST['school_id']) ?  $_POST['school_id'] : null;
            $academic_year = isset($_POST['academic_year']) ?  $_POST['academic_year'] : UtilityFunctions::AcademicYear();
            $school_information = BasicInformation::model()->findByPk($school_id);
            if(empty($school_information))
                throw new CHttpException(400,'Invalid request, Please try again.');
            $previous_link = isset($_POST['previous_link']) ?  $_POST['previous_link'] : null;
            $transaction = Yii::app()->db->beginTransaction();
            $subjectInformation = SubjectInformation::model()->StubjectDetail(null, 'both', $school_id);
            $column = isset($subjectInformation['column']) ? $subjectInformation['column'] : null;
            $subject_information = isset($subjectInformation['subject_information']) ? $subjectInformation['subject_information'] : null;
            if(!empty($subject_information)){
                for ($i=0; $i < sizeof($subject_information) ; $i++) { 
                    $subject = isset($subject_information[$i]['name']) ? strtolower($subject_information[$i]['name']) : null;
                    $subject_id = isset($subject_information[$i]['id']) ? $subject_information[$i]['id'] : null;
                    $theory_heading = isset($subject_information[$i]['theory']) ? $subject_information[$i]['theory'] : null;
                    $practical_heading = isset($subject_information[$i]['practical']) ? $subject_information[$i]['practical'] : null;
                    $theory_fm = isset($subject_information[$i]['theory_fm']) ? $subject_information[$i]['theory_fm'] : null;
                    $practical_fm = isset($subject_information[$i]['practical_fm']) ? $subject_information[$i]['practical_fm'] : null;
                    $is_practical = $practical_fm ? 1 : 0;
                    $subject_order = isset($subject_information[$i]['order']) ? $subject_information[$i]['order'] : null;
                    $theory_pm= isset($subject_information[$i]['theory_pm']) ? $subject_information[$i]['theory_pm'] : null;
                    $practical_pm = isset($subject_information[$i]['practical_pm']) ? $subject_information[$i]['practical_pm'] : null;
                    $student_list = StudentInformation::model()->StudentMarklist($academic_year, $school_id, 'student_id', $subject_id);
                    if(!empty($student_list)){
                        foreach ($student_list as $student_info_) {
                            $student_id = $student_info_->id;
                            $raw_theory_mark = isset($_POST[$student_id.'_'.$subject_id.'_'.$theory_heading]) && is_numeric($_POST[$student_id.'_'.$subject_id.'_'.$theory_heading]) ? $_POST[$student_id.'_'.$subject_id.'_'.$theory_heading] : null;
                            $raw_practical_mark = isset($_POST[$student_id.'_'.$subject_id.'_'.$practical_heading]) && is_numeric($_POST[$student_id.'_'.$subject_id.'_'.$practical_heading]) ? $_POST[$student_id.'_'.$subject_id.'_'.$practical_heading] : null;
                            $mark_obtained_detail = MarkObtained::model()->find('student_id=:student_id AND subject_id=:subject_id AND subject_status=:subject_status AND school_id=:school_id',[':student_id'=>$student_id, ':subject_id'=>$subject_id, ':subject_status'=>1, ':school_id'=>$school_id]);
                            $prv_th = $mark_obtained_detail ? $mark_obtained_detail->theory_mark : 0;
                            $prv_pr = $mark_obtained_detail ? $mark_obtained_detail->practical_mark : 0;
                            $theory_mark = $raw_theory_mark ? $raw_theory_mark : $prv_th;
                            $practical_mark = $raw_practical_mark ? $raw_practical_mark : $prv_pr;
                            $total_mark = $theory_mark + $practical_mark;
                            if(!empty($mark_obtained_detail)){
                                $mark_obtained_detail -> subject_name = $subject;
                                $mark_obtained_detail -> theory_mark = ceil($theory_mark);
                                $mark_obtained_detail -> practical_mark = ceil($practical_mark);
                                $mark_obtained_detail -> total_mark = ceil($total_mark); 
                                $mark_obtained_detail -> is_practical = $is_practical;
                                $mark_obtained_detail -> theory_pass_mark = ceil($theory_pm);
                                $mark_obtained_detail -> practical_pass_mark = ceil($practical_pm);
                                $mark_obtained_detail -> theory_full_mark = ceil($theory_fm);
                                $mark_obtained_detail -> practical_full_mark = ceil($practical_fm);
                                $mark_obtained_detail -> subject_status = 1;
                                if($mark_obtained_detail->validate() && $mark_obtained_detail->update()){
                                    $error_array[] ='true';
                                }else
                                    $error_array[] ='false';
                            }else{
                                $mark_obtained  = new MarkObtained();
                                $mark_obtained -> academic_year = UtilityFunctions::AcademicYear();
                                $mark_obtained -> terminal_id = 1;
                                $mark_obtained -> terminal_ = 100;
                                $mark_obtained -> student_id = $student_id;
                                $mark_obtained -> subject_id = $subject_id;
                                $mark_obtained -> subject_name = $subject;
                                $mark_obtained -> theory_mark = ceil($theory_mark);
                                $mark_obtained -> practical_mark = ceil($practical_mark);
                                $mark_obtained -> total_mark = ceil($total_mark); 
                                $mark_obtained -> cas_mark = 0;
                                $mark_obtained -> is_practical = $is_practical;
                                $mark_obtained -> theory_pass_mark = ceil($theory_pm);
                                $mark_obtained -> practical_pass_mark = ceil($practical_pm);
                                $mark_obtained -> theory_full_mark = ceil($theory_fm);
                                $mark_obtained -> practical_full_mark = ceil($practical_fm);
                                $mark_obtained -> terminal_percent = 100;
                                $mark_obtained -> cas_percent = 0;
                                $mark_obtained -> percent_for_final = 100;
                                $mark_obtained -> grace_mark = 0;
                                $mark_obtained -> school_id = $school_id;
                                $mark_obtained -> subject_status = 1;
                                if($mark_obtained->validate() && $mark_obtained->save()){
                                $obtained_mark_hist = new MarkObtainedHistory();
                                $obtained_mark_hist -> mark_obtained_id = $mark_obtained->id;
                                $obtained_mark_hist -> grace_mark = $mark_obtained->grace_mark;
                                $obtained_mark_hist -> theory_mark = $mark_obtained->theory_mark;
                                $obtained_mark_hist -> practical_mark = $mark_obtained->practical_mark;
                                $obtained_mark_hist -> cas_mark = $mark_obtained->cas_mark;
                                $obtained_mark_hist -> theory_grade = $mark_obtained->theory_grade;
                                $obtained_mark_hist -> practical_grade = $mark_obtained->practical_grade;
                                $obtained_mark_hist -> cas_grade = $mark_obtained->cas_grade;
                                $obtained_mark_hist -> cas = $mark_obtained->cas;
                                $obtained_mark_hist -> total_mark = $mark_obtained->total_mark;
                                $obtained_mark_hist -> total_grade_point = $mark_obtained->total_grade_point;
                                $obtained_mark_hist -> total_grade = $mark_obtained->total_grade;
                                $obtained_mark_hist -> retake_th_number = $mark_obtained->retake_th_number;
                                $obtained_mark_hist -> retake_pr_number = $mark_obtained->retake_pr_number;
                                if(!$obtained_mark_hist -> validate() || !$obtained_mark_hist -> save())
                                    $error_array[] ='false';
                                }
                                else
                                    $error_array[] ='false';
                            }

                        }
                    }
                }
            }
            if(!in_array('false', $error_array)){
                $transaction->commit();
                if($previous_link)
                    $this->redirect([$previous_link]);
                $this->redirect('previewRawMarks');
            }else{
                $transaction->rollback();
                throw new CHttpException(400,'Invalid request. Data Cannot Save, Please try again.');
            }
        }
        throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }
    /*end upload of excel file ledger here*/


    /**
    *student ledger in data table formate due to error in excel download yii1
    */
    public function actionDownloadStudentLedger(){
        $school_information = $subject_information = $resorce_center = $total_student =  $academic_year = null;
        $model = new MarkObtained();
        $terminal ="BASIC LEVEL EXAMINATION";
        $student_information_new = $column = $student_information = $subject_wise_total = $studentMarksArray = array();
        if(isset($_GET['MarkObtained'])){
            $this->beginWidget('CHtmlPurifier');
            $student_details = StudentInformation::model()->StudentExcelLedger($_GET['MarkObtained']);
            $studentArray = $student_details['student'];
            $total_student = count($studentArray);
            $MarksArray = $student_details['student_marks'];
            $academic_year = isset($_GET['MarkObtained']['academic_year']) && is_numeric($_GET['MarkObtained']['academic_year']) ? $_GET['MarkObtained']['academic_year'] : UtilityFunctions::AcademicYear();
            $terminal_id = 1;
            $insert_type = isset($_GET['MarkObtained']['insert_type']) ? $_GET['MarkObtained']['insert_type'] : 'both';
            $user_information = UtilityFunctions::UserInformations();
            $school_id_sec = isset($user_information) ? $user_information['school_id'] : 0;
            $school_id = UtilityFunctions::ShowSchool() && !is_numeric($_GET['MarkObtained']['school_id']) ? $school_id_sec : (int) $_GET['MarkObtained']['school_id'];
            $this->endWidget();
            $school_information = BasicInformation::model()->findByPk($school_id);
            if(empty($school_information))
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
            $excel_header = !empty($school_information) ? $school_information -> title.' :  '.$academic_year. ' BASIC LEVEL EXAMINATION' : 'null';
            $resorce_center = BasicInformation::model()->findByAttributes(['id'=>$school_information->resource_center_id, 'status'=>1]);
            $column[] ='student_id';
            $column[] ='name';
            $column[] ='symbol_number';
            $subjectInformation = PassMark::model()->SubjectList($school_id);
            if(!empty($subjectInformation)){
                $ik = 0 ;
                foreach ($subjectInformation as $information){
                    $subject_nature = $information->subject_information ?  $information->subject_information->subject_nature : 0;
                    $default_optional = $information->subject_information ?  $information->subject_information->default_optional : 0;
                    $secondary_optional_school = $information->subject_information ?  $information->subject_information->school_id : 0;
                    $subject = $information->subject_information ?  $information->subject_information->title : '';
                    $subject_name = UtilityFunctions::ColumnName($subject);
                    if($subject_nature == 1 || ($subject_nature == 3 && $default_optional == 1)){
                        if($information->is_practical && $information->is_practical !=0 && in_array($insert_type, ['both','theory']))
                            $column[] = $subject_name.'_theory';
                        if($information->is_practical && $information->is_practical !=0 && in_array($insert_type, ['both','practical']))
                            $column[] = $subject_name.'_practical';
                        if($information->is_practical==0){
                            if(in_array($insert_type, ['both','theory']))
                                $column[] = $subject_name;
                        }
                        if($information->is_practical){$is_practical_[] = 'true';}else{$is_practical_[] = 'false';}
                        $subject_information[$ik]['name'] = $subject;
                        $subject_information[$ik]['theory_fm'] = $information->theory_full_mark;
                        $subject_information[$ik]['practical_fm'] = $information->practical_full_mark != 0 ? $information->practical_full_mark :'';
                        $ik++;
                    }
                    if($subject_nature == 3 && $default_optional == 0 && $secondary_optional_school == $school_id){
                        if($information->is_practical && $information->is_practical !=0 && in_array($insert_type, ['both','theory']))
                            $column[] = $subject_name.'_theory';
                        if($information->is_practical && $information->is_practical !=0 && in_array($insert_type, ['both','practical']))
                            $column[] = $subject_name.'_practical';
                        if($information->is_practical==0){
                            if(in_array($insert_type, ['both','theory']))
                                $column[] = $subject_name;
                        }
                        if($information->is_practical){$is_practical_[] = 'true';}else{$is_practical_[] = 'false';}
                        $subject_information[$ik]['name'] = $subject;
                        $subject_information[$ik]['theory_fm'] = $information->theory_full_mark;
                        $subject_information[$ik]['practical_fm'] = $information->practical_full_mark != 0 ? $information->practical_full_mark :'';
                    }

                }
            }
            if(!empty($MarksArray)){
                foreach ($MarksArray as $marksInformation) {
                    if($marksInformation->is_practical != 0){
                        $studentMarksArray[$marksInformation->student_id.'_'.$marksInformation->subject_id.'_theory'] = $marksInformation->theory_mark;
                        $studentMarksArray[$marksInformation->student_id.'_'.$marksInformation->subject_id.'_practical'] = $marksInformation->practical_mark;
                    }
                    else{
                        $studentMarksArray[$marksInformation->student_id.'_'.$marksInformation->subject_id] = $marksInformation->theory_mark;
                    }
                }
            }
            $i=0;
            if(!empty($studentArray)){

                foreach ($studentArray as $student) {
                    $registration_number = $student->id;
                    $student_information[$i]['student_id'] = $student->id;
                    $student_information[$i]['name'] = ucwords($student->first_name." ".$student->middle_name." ".$student->last_name);
                    $student_information[$i]['symbol_number'] = isset($student) ? $student->symbol_number : 'null';
                    if(!empty($subjectInformation)){
                        foreach ($subjectInformation as $information){
                            $subject_id = $information->subject_id;
                            $subject = $information->subject_information ?  $information->subject_information->title : '';
                            $subject_name = UtilityFunctions::ColumnName($subject);
                            if($information->is_practical && $information->is_practical !=0){
                                $student_information[$i][$subject_name.'_theory'] = isset($studentMarksArray[$registration_number.'_'.$subject_id.'_theory']) ? $studentMarksArray[$registration_number.'_'.$subject_id.'_theory'] : '';
                                $student_information[$i][$subject_name.'_practical'] = isset($studentMarksArray[$registration_number.'_'.$subject_id.'_practical']) ? $studentMarksArray[$registration_number.'_'.$subject_id.'_practical'] : '';
                            }else
                            $student_information[$i][$subject_name] = isset($studentMarksArray[$registration_number.'_'.$subject_id]) ? $studentMarksArray[$registration_number.'_'.$subject_id] : '';
                        }
                    }

                    $i++;
                }
            }
            $student_information_new = new CArrayDataProvider(
                $student_information, 
                array(
                    'pagination'=>array(
                        'pageSize'=>40,
                    ),
                    'keyField'=>'name'
                ));

        }
        $this->render('_download_student_ledger',['StudentList'=>$student_information_new, 'column'=>$column,'student_information'=>$student_information,'model'=>$model, 'school_information'=>$school_information, 'subject_information'=>$subject_information, 'resorce_center'=>$resorce_center, 'total_student'=>$total_student, 'academic_year'=>$academic_year]);
    }

    /**
    *
    */

    public function actionReArrangeGrade(){
        //die('choose subject/school');

        $sql='UPDATE mark_obtained SET grace_mark=0 , grace_total=total_mark, grace_grade=total_grade , extra_gp = total_grade_point';
        $command=Yii::app()->db->createCommand($sql);
        $update=$command->execute();
        $student_mark_information = Yii::app()->db->createCommand()
        ->from('mark_obtained')
        ->andWhere("subject_id IN (6,7) AND subject_status=1")
        ->queryAll();
        //$transaction = Yii::app()->db->beginTransaction();
        if(!empty($student_mark_information)){
            for ($i=0; $i < sizeof($student_mark_information) ; $i++) { 
                $id = $student_mark_information[$i]['id'];
                $total_mark = $student_mark_information[$i]['total_mark'];
                $theory_full_mark = $student_mark_information[$i]['theory_full_mark'];
                $practical_full_mark = $student_mark_information[$i]['practical_full_mark'];
                $combine_full_mark = $theory_full_mark + $practical_full_mark;
                $total_grade_information = UtilityFunctions::GradeInformation((int) $total_mark, (int) $combine_full_mark);
                $gp = isset($total_grade_information['grade_point']) ?  number_format($total_grade_information['grade_point'], 2) : 0;

                $sql_='UPDATE mark_obtained SET total_grade_point='.$gp.' WHERE id ='.$id;
                $command_=Yii::app()->db->createCommand($sql_);
                $update_=$command_->execute();
            }
        }
        die('re-arrange grade and marks');
    }


    /**
    * retake exam for those who get below D+
    */
    public function actionRetakeReport(){
        $this->layout = '_datatable_layout';
        $model = new MarkObtained();
        $retake_ledger_report = [];
        if(isset($_POST['MarkObtained'])){
            $retake_ledger_report = MarkObtained::model()->RetakeReport($_POST['MarkObtained']);
        }
        $this->render('_retake_report',['model'=>$model,'retake_ledger_report'=>$retake_ledger_report]);
    }

    /**
    * retake exam for those who get below D+
    */
    public function actionRetakeStudentSummery(){
        $this->layout = '_datatable_layout';
        $model = new MarkObtained();
        $retake_ledger_report = [];
        if(isset($_POST['MarkObtained'])){
            $retake_ledger_report = MarkObtained::model()->RetakeStudentList($_POST['MarkObtained']);
        }
        $this->render('_retake_student_list',['model'=>$model,'retake_ledger_report'=>$retake_ledger_report]);
    }

    /**
    */
    public function actionRetakeStudentNumber(){
        $this->layout = '_datatable_layout';
        $model = new MarkObtained();
        $academic_year = UtilityFunctions::AcademicYear();
        $retake_ledger_report = [];
        if(isset($_POST['MarkObtained'])){
            $retake_ledger_report = MarkObtained::model()->RetakeStudentNumber($_POST['MarkObtained']);
            $academic_year = isset($_POST['MarkObtained']['academic_year']) ? $_POST['MarkObtained']['academic_year'] : UtilityFunctions::AcademicYear();
            if(isset($_POST['MarkObtained']['options']) && $_POST['MarkObtained']['options'] == 'pdf'){
                $mPDF1 = Yii::app()->ePdf->mpdf();
                #You can easily override default constructor's params
                $mPDF1->writeHTMLfooter=true;

                $mPDF1->AddPage('P');
                $mPDF1->WriteHTML($this->renderPartial('_retake_student_number_subject_pdf',['model'=>$model,'retake_ledger_report'=>$retake_ledger_report,'academic_year'=>$academic_year], true));
                $mPDF1->Output(strtoupper(Yii::app()->params['municipality'].' subject wise retake student report').'.pdf', 'I');
            }

        }
        $this->render('_retake_student_number_subject',['model'=>$model,'retake_ledger_report'=>$retake_ledger_report,'academic_year'=>$academic_year]);
    }

    /**
    *retake subject wise list section
    */
    public function actionRetakeSubjectWiseDetail(){
        $this->layout = '_datatable_layout';
        $model = new MarkObtained();
        $academic_year = UtilityFunctions::AcademicYear();
        $retake_ledger_report = $subject_information = [];
        if(isset($_GET['MarkObtained'])){
            $retake_ledger_report = MarkObtained::model()->RetakeSubjectStudentList($_GET['MarkObtained']);
            $subject_id = isset($_GET['MarkObtained']['subject_id']) ? $_GET['MarkObtained']['subject_id'] : null;
            $academic_year = isset($_GET['MarkObtained']['academic_year']) ? $_GET['MarkObtained']['academic_year'] : UtilityFunctions::AcademicYear();
            $subject_information = SubjectInformation::model()->findByPk($subject_id);
        }
        $this->render('_retake_subject_student_list',['model'=>$model,'retake_ledger_report'=>$retake_ledger_report, 'subject_information'=>$subject_information, 'academic_year'=>$academic_year]);

    }


    /**
    * retake exam for those who get below D+
    */
    public function actionRetakeSchoolWiseDetail(){
        $this->layout = '_datatable_layout';
        $model = new MarkObtained();
        $retake_ledger_report = [];
        if(isset($_POST['MarkObtained'])){
            $retake_ledger_report = MarkObtained::model()->RetakeSchoolDetail($_POST['MarkObtained']);
        }
        $this->render('_retake_school_wise_detail',['model'=>$model,'retake_ledger_report'=>$retake_ledger_report]);
    }

    /**
    * 
    */

    public function actionGraceUpgrade(){
        $this->layout = '_datatable_layout';
        $upgrade_student_detail = [];
        $model = new GraceMarks;
        if(isset($_POST['GraceMarks'])){
            $model->attributes=$_POST['GraceMarks'];
            if($model->validate() && $model->save())
                $upgrade_student_detail = MarkObtained::model()->UpgradeByGrace();
            else
                throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
        $this->render('_grace_adding_section',['upgrade_student_detail'=>$upgrade_student_detail, 'model'=>$model]);

    }

    public function actionGraceAuditReport(){
        $this->layout = '_datatable_layout';
        $model = new MarkObtained();
        $academic_year = isset($_POST['MarkObtained']['academic_year']) ? $_POST['MarkObtained']['academic_year'] : UtilityFunctions::AcademicYear();
        $criteria = new CDbCriteria;
        $criteria -> order = 'id DESC';
        $latest_garce = GraceMarks::model()->find($criteria);
        $grace_mark = $latest_garce ? $latest_garce->grace_mark : '';
        $grace_mark_report_information = MarkObtained::model()->GraceAuditReport($academic_year);
        if(isset($_POST['YII_CSRF_TOKEN'])){

            $mPDF1 = Yii::app()->ePdf->mpdf();
            #You can easily override default constructor's params
            $mPDF1->writeHTMLfooter=true;

            $mPDF1->AddPage('P');
            $mPDF1->WriteHTML($this->renderPartial('_grace_audit_report_pdf',['grace_mark_report_information'=>$grace_mark_report_information, 'academic_year'=>$academic_year, 'grace_mark'=>$grace_mark], true));
            $mPDF1->Output(strtoupper(Yii::app()->params['municipality']).'.pdf', 'I');
            exit;

        }
        $this->render('_grace_audit_report',['grace_mark_report_information'=>$grace_mark_report_information, 'academic_year'=>$academic_year, 'grace_mark'=>$grace_mark,'model'=>$model]);


    }





    /**
    * retake exam for those who get below D+
    */
    public function actionRetakeReportStudentWise(){
        $this->layout = '_datatable_layout';
        $model = new MarkObtained();
        $academic_year = UtilityFunctions::AcademicYear();
        $retake_ledger_report = MarkObtained::model()->RetakeReportResourceCenter($academic_year);
        die('working');
        echo "<pre>";
        echo print_r($retake_ledger_report);
        exit;
    }


    /**
    * subject/school wise average grade report
    */

    public function actionSubjectSchoolWiseGrade(){
        $this->layout = '_datatable_layout';
        $model = new MarkObtained();
        $academic_year = isset($_GET['MarkObtained']['academic_year']) ? $_GET['MarkObtained']['academic_year'] : UtilityFunctions::AcademicYear();
        $result_ = isset($_GET['MarkObtained']['result_']) ? $_GET['MarkObtained']['result_'] : UtilityFunctions::AcademicYear();
        $reportData = MarkObtained::model()->SubjecSchoolAvgGrd($academic_year, $result_);
        $this->render('_subject_school_average_report',['model'=>$model,'reportData'=>$reportData, 'academic_year'=>$academic_year,'result_'=>$result_]);
    }

    /**
    *
    */
    public function actionSubjectWiseGradeStudentNumber(){
        $model = new MarkObtained();
        $academic_year = $data_detail_array = null;
        if(isset($_GET['MarkObtained'])){
            $academic_year = isset($_GET['MarkObtained']['academic_year']) ? $_GET['MarkObtained']['academic_year'] : UtilityFunctions::AcademicYear();
            $ledger_type = isset($_GET['MarkObtained']['ledger_type']) ? $_GET['MarkObtained']['ledger_type'] : 'general';
            $data_detail_array = MarkObtained::model()->SubjectWiseGradeStudentNumber($academic_year);
            if($ledger_type == 'pdf'){
                $mPDF1 = Yii::app()->ePdf->mpdf();
                #You can easily override default constructor's params
                $mPDF1->writeHTMLfooter=true;

                $mPDF1->AddPage('L');
                $mPDF1->WriteHTML($this->renderPartial('_school_subject_wise_grade_student_number_pdf',['data_detail_array'=>$data_detail_array, 'academic_year'=>$academic_year], true));
                $mPDF1->Output('_school_subject_wise_grade_student_number.pdf', 'I');
                exit;
            }
        }
        $this->render('_school_subject_wise_grade_student_number',['data_detail_array'=>$data_detail_array, 'academic_year'=>$academic_year, 'model'=>$model]);
    }
    /*
    *
    */
    public function actionSubjectStudentList(){
        if(isset($_GET['grade'])){
            $this->layout = '_datatable_layout';
            $subject_id = isset($_GET['subject_id']) && is_numeric($_GET['subject_id']) ? $_GET['subject_id'] :  null;
            $subject_information = MarkObtained::model()->findByAttributes(['subject_id'=>$subject_id, 'subject_status'=>1]);
            $student_list = MarkObtained::model()->StudentListSubjectGradeWise($_GET);
            $this->render('_student_list_subject',['student_list'=>$student_list, 'subject_information'=>$subject_information]);
            exit;


        }
        throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }

    /**
    * Displays a student list for retake base on given parameters.
    * @param integer $id the ID of the model to be displayed
    */
    public function actionSelectStudent(){
        if(isset($_GET['academic_year']) && isset($_GET['school_id']) && isset($_GET['subject_id'])){
            $subject_id = isset($_GET['subject_id']) && is_numeric($_GET['subject_id']) ?  $_GET['subject_id'] : null;
            $school_id = is_numeric($_GET['school_id']) ?  $_GET['school_id'] : null;
            $school_information = BasicInformation::model()->findByPk($school_id);
            $maximum_range = MarkObtained::model()->SubjectFm($_GET);
            $studentList = MarkObtained::model()->RetakeStudentList($_GET);
            /*echo "<pre>";
            echo print_r($studentList);
            exit;*/
            $this->render('_choose_retake_student',['studentList'=>$studentList, 'params'=>$_GET, 'maximum_range'=>$maximum_range,'school_information'=>$school_information]);
            exit;

        }
        throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }

    /**
    * insert retake exam marks
    */

    public function actionInsertRetakeMark(){
        if(isset($_POST['studentArray']) && isset($_POST['subjectType']) && isset($_POST['subject_id']) && isset($_POST['academic_year'])){
            $insertRetake = MarkObtained::model()->InsertRetakeStudentMark($_POST);
            if($insertRetake)
                $this->redirect(array('RetakeReport'));
            throw new CHttpException(400,'Cannot Save, Somethings is wrong.');

        }
        throw new CHttpException(400,'Invalid request. Something is wrong.');
    }


    /**
    * Displays a particular model.
    * @param integer $id the ID of the model to be displayed
    */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }


    /**
    * Updates a particular model.
    * If update is successful, the browser will be redirected to the 'view' page.
    * @param integer $id the ID of the model to be updated
    */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

    // Uncomment the following line if AJAX validation is needed
    // $this->performAjaxValidation($model);

        if(isset($_POST['MarkObtained']))
        {
            $model->attributes=$_POST['MarkObtained'];
            if($model->validate() && $model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
    * Deletes a particular model.
    * If deletion is successful, the browser will be redirected to the 'admin' page.
    * @param integer $id the ID of the model to be deleted
    */
    public function actionDelete($id)
    {
        if(Yii::app()->request->isPostRequest)
        {
    // we only allow deletion via POST request
            $this->loadModel($id)->delete();

    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }

    /**
    * Lists all models.
    */
    public function actionIndex()
    {
        $dataProvider=new CActiveDataProvider('MarkObtained');
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
    * Manages all models.
    */
    public function actionAdmin()
    {
        $model=new MarkObtained('search');
    $model->unsetAttributes();  // clear any default values
    if(isset($_GET['MarkObtained']))
        $model->attributes=$_GET['MarkObtained'];

    $this->render('admin',array(
        'model'=>$model,
    ));
    }

    /**
    * Returns the data model based on the primary key given in the GET variable.
    * If the data model is not found, an HTTP exception will be raised.
    * @param integer the ID of the model to be loaded
    */
    public function loadModel($id)
    {
        $model=MarkObtained::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
    /**
    *
    */
   /* public function actionMarkReview(){
        //asf
    }*/

    /**
    * Performs the AJAX validation.
    * @param CModel the model to be validated
    */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='mark-obtained-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }



}
