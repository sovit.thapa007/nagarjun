<?php

class DashboardController extends Controller
{

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
//'accessControl', // perform access control for CRUD operations
		);
	}

    /**
    * Specifies the access control rules.
    * This method is used by the 'accessControl' filter.
    * @return array access control rules
    */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
            'actions'=>array('Index'),
            'users'=>array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
	/**
	 * System  Dashboard
	 */
	public function actionindex()
	{
		$userInformation = UtilityFunctions::UserInformations();
		$school_id = isset($userInformation['school_id']) ? $userInformation['school_id'] :  null;
 		$data_entry_report = MarkObtained::model()->DataentryReport();
		$studentNumarray = [];
		if(UtilityFunctions::SuperUser()){
			$studentNumarray = StudentInformation::model()->SchoolWiseNumber(UtilityFunctions::AcademicYear());
			$this->render('admin_dashboard', ['dataPoints'=>$studentNumarray, 'title' => " SCHOOL WISE STUDENT NUMBER  : ".UtilityFunctions::AcademicYear(), 'school_id'=>$school_id, 'data_entry_report'=>$data_entry_report]);
		}
		else{
			$report = Result::model()->ReportSummery(UtilityFunctions::AcademicYear());
			$this->render('index',['report'=>$report, 'academic_year'=>UtilityFunctions::AcademicYear()]);
		}



	}

}

?>