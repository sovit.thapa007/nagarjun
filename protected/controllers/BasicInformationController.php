<?php

class BasicInformationController extends Controller
{
	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
			'actions'=>array('admin','SchoolMarksDetail','view','SchoolMarksReviewDetail','PracticalSchoolSheet'),
			'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
			'actions'=>array('AddResourceCenter','AssignSchoolResourceCenter','View','SchoolReport','Create','EthnicityCast','Update','Delete','ResourceCenterList','ResourceSchoolStudent','ResourceCenterSchool','Index','PracticalSchoolSheet','SchoolMarksDetail','SchoolMarksReviewDetail','Admin','AdminDatatable', 'SchoolSubject', 'SaveSchoolSubject'),
			'users'=>array(Yii::app()->params['superadmin']),
			),
            array('deny', // deny all users
                'users' => array('*'),
            ),
		);
	}


	/**
	*add resource center of munucipality/metroplitan city
	*/
	public function actionAddResourceCenter(){
		$message_array = [];
		$schools_list = BasicInformation::model()->findAll(array("condition" => "status = 1"));
		if(isset($_POST['resource_center_'])){
			$resource_center_array = $_POST['resource_center_'];
			$resource_center_school = BasicInformation::model()->AssignResourceCenter($resource_center_array);
			if($resource_center_school){
				$message_array['status'] = 1;
				$message_array['message'] = 'RESOURCE CENTER FOR SCHOOLS ARE GENERATED';
			}
			else{
				$message_array['status'] = 0;
				$message_array['message'] = 'RESOURCE CENTER CANNOT GENERATED';
			}
		}
		$this->render('_add_resource_center',['schools_list'=>$schools_list, 'message_array'=>$message_array]);
	}

	/**
	*
	*/
	public function actionAssignSchoolResourceCenter(){
		$resource_center = BasicInformation::model()->findAllByAttributes(['status'=>1, 'resource_center'=>1]);
		$school_list = BasicInformation::model()->findAllByAttributes(['status'=>1]);
		$this->render('_assign_school_resource_center',['resource_center'=>$resource_center, 'school_list'=>$school_list]);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$data = $this->loadModel($id);
		$mark_obtained = new MarkObtained();
		$subject_information = SubjectInformation::model()->SubjectDetail();
		$resource_center = BasicInformation::model()->findByPk($data->resource_center_id);
		$this->render('view', array(
			'model' => $data, 'resource_center' =>  $resource_center, 'subject_information'=>$subject_information, 'mark_obtained' => $mark_obtained
		));
	}


	/**
	* school report with contact person/chairman/headteacher/address
	*/

	public function actionSchoolReport(){
		$model = new BasicInformation('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_POST['BasicInformation'])) {
			$reportInformaiton = BasicInformation::model()->SchoolReport($_POST['BasicInformation']);

			$column = isset($reportInformaiton['column']) ? $reportInformaiton['column'] : null;
			$data = isset($reportInformaiton['data']) ? $reportInformaiton['data'] : null;
            $mPDF1 = Yii::app()->ePdf->mpdf();
        # You can easily override default constructor's params
            $mPDF1 = Yii::app()->ePdf->mpdf('UTF-8', 'A4');
            $mPDF1->writeHTMLfooter=true;

            $mPDF1->AddPage('P');
            $mPDF1->WriteHTML($this->renderPartial('_school_detail_report_pdf',['column'=>$column, 'data'=>$data], true));
            $mPDF1->Output($file);

		}
		$this->render('_school_report', array(
			'model' => $model
		));
	}
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new BasicInformation;

		$resource_center_information = BasicInformation::model()->findAll('resource_center=:resource_center',[':resource_center'=>1]); 
		if (isset($_POST['BasicInformation'])) {
			$model->attributes = $_POST['BasicInformation'];
			if ($model->validate() && $model->save()){
				$this->redirect(array('view','id'=>$model->id));
				exit();
			}
		}
		$this->render('create', array(
			'model' => $model
		));
	}
	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	public function actionEthnicityCast($key)
	{
		$castDetails = EthinicGroup::model()->findAllByAttributes(array('parent_id' => $key));
		if (!empty($castDetails)) {
			$option = "<option value=''>Select Cast </option>";
			foreach ($castDetails as $childData) {
				$option .=CHtml::tag('option', array('value' => $childData->id), CHtml::encode($childData->title), true);
			}
			echo CJSON::encode(['success' => true, 'option' => $option]);
		} else
			echo CJSON::encode(['success' => false]);
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		/*
		  $function = Yii::app()->function->parent_idfunction(1, 6);
		  var_dump($function);exit(); */
		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['BasicInformation'])) {
			$prv_log = $model->logo;
			$model->attributes = $_POST['BasicInformation'];
			if(!$model->logo)
				$model->logo = $prv_log;
			if ($model->validate() && $model->save())
				$this->redirect(array('view', 'id' => $model->id));
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
		// we only allow deletion via POST request
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		} else
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
	}

	/**
	*	
	*/
	public function actionResourceCenterList(){
		$this->layout = '_datatable_layout';
		$resource_center_information = BasicInformation::model()->ResourceCenterList(); 
		$this->render('_resource_center',['resource_center_information'=>$resource_center_information]);
	}


	/**
	* Resource center Wise student/school number on basis of school type
	*/

	public function actionResourceSchoolStudent(){
		$resource_center_information = BasicInformation::model()->ResourceCenterList(); 
		$resource_center_det_info = BasicInformation::model()->ResourceCenterDetail(UtilityFunctions::AcademicYear());
		$resource_center_detail= isset($resource_center_det_info['resource_detail']) ? $resource_center_det_info['resource_detail'] : [];
        $mPDF1 = Yii::app()->ePdf->mpdf();
    # You can easily override default constructor's params
        $mPDF1 = Yii::app()->ePdf->mpdf('UTF-8', 'A4');
        $mPDF1->writeHTMLfooter=true;

        $mPDF1->AddPage('L');
        $mPDF1->WriteHTML($this->renderPartial('_resource_center_school_student_number',['resource_center_detail'=>$resource_center_detail, 'resource_center_information'=>$resource_center_information], true));
        $mPDF1->Output($file);
	}

	/**
	*school list of resource center
	*@param int resource_center_id
	*/
	public function actionResourceCenterSchool($id){
		$this->layout = '_datatable_layout';
		$resource_center_information = BasicInformation::model()->findByAttributes(['id'=>$id, 'resource_center'=>1]);
		if(empty($resource_center_information))
			throw new CHttpException(400, 'Invalid resource center. Please check it again.');
		$school_list = BasicInformation::model()->ResourceCenterSchoolDetails($id);
		$this->render('_resource_center_school_list',['school_list'=>$school_list, 'resource_center_information'=>$resource_center_information]);
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->layout = '_datatable_layout';
		$model = new BasicInformation();

		if(isset($_GET['BasicInformation'])){
			$schools_list = BasicInformation::model()->ResourceSchool($_GET['BasicInformation']);
		}else
			$schools_list = BasicInformation::model()->findAll(array("condition" => "status = 1"));
		$this->render('admin', array(
			'schools_list' => $schools_list,
			'model' => $model,
		));
	}

	public function actionPracticalSchoolSheet(){
        $academic_year = UtilityFunctions::AcademicYear();
        $school_id = isset($_GET['school_id']) && is_numeric($_GET['school_id']) ? $_GET['school_id'] : null;
        $type = isset($_GET['type']) && in_array($_GET['type'], ['practical','theory']) ? $_GET['type'] : 'practical';
        $school_information = BasicInformation::model()->findByPk($school_id);
        if(empty($school_information))
			throw new CHttpException(404, 'The requested school does not exist.');
		$resource_information = BasicInformation::model()->findByAttributes(['id'=>$school_information->resource_center_id]);
        $student_information = StudentInformation::model()->ApprovedStudentList($academic_year, $school_id, 'symbol_number');
        $subjectInformation = PassMark::model()->SubjectColumn($type, $school_id);


        $mPDF1 = Yii::app()->ePdf->mpdf();
    # You can easily override default constructor's params
        $mPDF1 = Yii::app()->ePdf->mpdf('UTF-8', 'A3');
        $mPDF1->writeHTMLfooter=true;

        $mPDF1->AddPage('L');
        $mPDF1->WriteHTML($this->renderPartial('_theory_practical_school_sheet',['school_information'=>$school_information, 'type'=>$type, 'student_information'=>$student_information, 'subjectInformation'=>$subjectInformation, 'resource_information'=>$resource_information], true));
        $mPDF1->Output($file);
	}


	/**
	* assign subject to other type of school then general
	*/
	public function actionSchoolSubject($id){
		$model = new SchoolSubject();
		$subject_list = SchoolSubject::model()->findAll('school_id=:school_id AND status=:status',[':school_id'=>$id, ':status'=>1]);
		$subject_array = [];
		if(!empty($subject_list)){
			foreach ($subject_list as $subject)
				$subject_array[] = $subject->subject_id;
		}
		$subject_information = SubjectInformation::model()->SubjectListSection($subject_array);
		$this->render('_school_subject',['subject_list'=>$subject_list, 'model'=>$model, 'id'=>$id, 'subject_information'=>$subject_information]);
	}

	/**
	*save school subject
	*/
	public function actionSaveSchoolSubject(){
		$model = new SchoolSubject();
		if(isset($_POST['SchoolSubject'])){
			$model->attributes = $_POST['SchoolSubject'];
			if($model->save())
				$this->redirect(['BasicInformation/SchoolSubject/'.$model->school_id]);
			else
				throw new CHttpException(404, 'The requested page does not exist.');

		}else
			$this->redirect(['BasicInformation/admin']);
	}

	public function actionSchoolMarksDetail(){

		$this->layout = '_datatable_layout';
		$model = new BasicInformation();

		if(isset($_GET['BasicInformation'])){
			$schools_list = BasicInformation::model()->ResourceSchool($_GET['BasicInformation']);
		}else
			$schools_list = BasicInformation::model()->findAll(array("condition" => "status = 1","order" => "schole_code"));
		$this->render('_school_marks_review', array(
			'schools_list' => $schools_list,
			'model' => $model,
		));
	}

	public function actionSchoolMarksReviewDetail($id){

		$data = $this->loadModel($id);
		$mark_obtained = new MarkObtained();
		$subject_information = SubjectInformation::model()->SubjectDetail($id);
		$resource_center = BasicInformation::model()->findByPk($data->resource_center_id);
		$this->render('_school_marks_review_detail', array(
			'model' => $data, 'resource_center' =>  $resource_center, 'subject_information'=>$subject_information, 'mark_obtained' => $mark_obtained
		));

	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->layout = '_datatable_layout';
		$model = new BasicInformation();

		if(isset($_GET['BasicInformation'])){
			$schools_list = BasicInformation::model()->ResourceSchool($_GET['BasicInformation']);
		}else
			$schools_list = BasicInformation::model()->findAll(array("condition" => "status = 1"));
		$this->render('admin', array(
			'schools_list' => $schools_list,
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model = BasicInformation::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'basic-information-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionAdminDatatable(){
		$schoolInformation = BasicInformation::model()->findAll();
		$schoolArray = [];
		if(!empty($schoolInformation)){
			foreach ($schoolInformation as $schlInfo) {
				$school = "<a href='".Yii::app()->baseUrl."/basicInformation/".$schlInfo->id."'>".$schlInfo->title."</a>";
				$schoolArray[] = [$schlInfo->type, $school, $schlInfo->schole_code, $schlInfo->ward_no.', '.$schlInfo->tole, $schlInfo->create_date];
			}
		}
		$schoolArrayData = ["data"=>$schoolArray];
		echo json_encode($schoolArrayData);
		exit;
	}



}
