<?php

class MessageController extends RController
{

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'rights', // perform access control for CRUD operations
		);
	}


	/**
	*
	*/
	public function actionMessageCompose(){
		$model = new Message();
		$this->render('_compose',['model'=>$model]);
	}

	/**
	*
	*/
	public function actionGenerateMessage(){

		if(isset($_POST['Message'])){
			$school_type = isset($_POST['Message']['school_type']) ? $_POST['Message']['school_type'] : null;
			$post = isset($_POST['Message']['post']) ? $_POST['Message']['post'] : null;
			if($school_type || $post)
				$receiverFromBoard = SchoolBoardInformation::model()->BoadMemberList($school_type, $post);
			else
				$receiverFromBoard = [];

			$event = isset($_POST['Message']['event']) ? $_POST['Message']['event'] : null;
			$message = isset($_POST['Message']['message']) ? $_POST['Message']['message'] : null;
			$receiverName = isset($_POST['name']) ? $_POST['name'] : null;
			$receiverNumber = isset($_POST['mobile']) ? $_POST['mobile'] : null;
			$this->render('_verify_message',['event'=>$event, 'message'=>$message, 'receiverName'=>$receiverName, 'receiverNumber'=>$receiverNumber, 'receiverFromBoard'=>$receiverFromBoard]);
		}else
			$this->redirect(['messageCompose']);
	}

	/**
	* message send
	*/

	public function actionSend(){
        if (isset($_POST)){
            $encraftApiData = UtilityFunctions::encraftApi();
            $board_receiver_= isset($_POST['board_receiver_mobile_number_']) ? $_POST['board_receiver_mobile_number_'] : null;
            $general_receiver_= isset($_POST['general_receiver_mobile_number_']) ? $_POST['general_receiver_mobile_number_'] : null;
            $event_name = isset($_POST['event']) ? $_POST['event'] : null;
            $message = isset($_POST['message']) ? $_POST['message'] : null;
            $mobileArrayInformation = [];
            if(sizeof($board_receiver_) >= 1 ){
                for ($i=0; $i < sizeof($board_receiver_) ; $i++) { 
                    $receivers_information = explode('-', $board_receiver_[$i]);
                    $mobile_number = isset($receivers_information[0]) ?  $receivers_information[0] : null;
                    $receiver_name = isset($receivers_information[1]) ?  $receivers_information[1] : null;
                    $post_argument['api_key'] = $encraftApiData['key'];
                    $post_argument['client_key'] = $encraftApiData['client_key'];
                    $post_argument['mobile'] = $mobile_number;
                    $post_argument['message'] = $message.', From Education Branch - '.ucwords(Yii::app()->params['municipality']);
                    $post_argument['event'] = $event_name;
                    $post_argument['ip_'] = Yii::app()->request->getUserHostAddress();
                    $post_argument['message_id'] = '';

                    //sms pending section due to unsufficient credit or un-active account status
                    $response_code = 400;
                    $remarks = '';
                    $message_id = $credit_consumed = 0;
                    if(strlen($mobile_number) == 10 && is_numeric($mobile_number)){
                        $args = http_build_query($post_argument);
                        $url = $encraftApiData['send-message-url'];
                        # Make the call using API.
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                        // Response
                        $response = curl_exec($ch);
                        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        curl_close($ch);
                        $response_information = json_decode($response);
                        $response_code = isset($response_information->response_code) ? $response_information->response_code : 400;
                        $credit_consumed = isset($response_information->credit_consumed) ? $response_information->credit_consumed : $credit_consumed;
                        $message_id = isset($response_information->message_id) ? $response_information->message_id : null;
                        $remarks = isset($response_information->remarks) ? $response_information->remarks : null;
                    }
                    $status = $response_code == 200 ? 1 : 0;
                    $message_log = new MessageReceiver();
                    $message_log->ip_ = Yii::app()->request->getUserHostAddress();
                    $message_log->mobile = $mobile_number;
                    $message_log->name = $receiver_name;
                    $message_log->message_code = $response_code;
                    $message_log->message = $message;
                    $message_log->credit_consumed = $credit_consumed;
                    $message_log->message_id = $message_id;
                    $message_log->remarks = '';//$remarks;
                    $message_log->event=$event_name;
                    $message_log->status = $status; 
                    $message_log->save(false);
                }
            }
         	if(sizeof($general_receiver_) >= 1 ){
                for ($j=0; $j < sizeof($general_receiver_) ; $j++) { 
                    $receivers_information = explode('-', $general_receiver_[$j]);
                    $general_mobile_number = isset($receivers_information[0]) ?  $receivers_information[0] : null;
                    $general_receiver_name = isset($receivers_information[1]) ?  $receivers_information[1] : null;
                    $post_argument['api_key'] = $encraftApiData['key'];
                    $post_argument['client_key'] = $encraftApiData['client_key'];
                    $post_argument['mobile'] = $general_mobile_number;
                    $post_argument['message'] = $message.', From Education Branch - '.ucwords(Yii::app()->params['municipality']);
                    $post_argument['event'] = $event_name;
                    $post_argument['ip_'] = Yii::app()->request->getUserHostAddress();
                    $post_argument['message_id'] = '';

                    //sms pending section due to unsufficient credit or un-active account status
                    $response_code = 400;
                    $remarks = '';
                    $message_id = $credit_consumed = 0;
                    if(strlen($general_mobile_number) == 10 && is_numeric($general_mobile_number)){
                    	$mobileArrayInformation['mobile_number'] = $general_mobile_number;
                        $args = http_build_query($post_argument);
                        $url = $encraftApiData['send-message-url'];
                        # Make the call using API.
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                        // Response
                        $response = curl_exec($ch);
                        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        curl_close($ch);
                        $response_information = json_decode($response);
                        $response_code = isset($response_information->response_code) ? $response_information->response_code : 400;
                        $credit_consumed = isset($response_information->credit_consumed) ? $response_information->credit_consumed : $credit_consumed;
                        $message_id = isset($response_information->message_id) ? $response_information->message_id : null;
                        $remarks = isset($response_information->remarks) ? $response_information->remarks : null;
                    }
                    $status = $response_code == 200 ? 1 : 0;
                    $message_log = new MessageReceiver();
                    $message_log->ip_ = Yii::app()->request->getUserHostAddress();
                    $message_log->mobile = $general_mobile_number;
                    $message_log->name = $general_receiver_name;
                    $message_log->message_code = $response_code;
                    $message_log->message = $message;
                    $message_log->credit_consumed = $credit_consumed;
                    $message_log->message_id = $message_id;
                    $message_log->remarks = '';//$remarks;
                    $message_log->event=$event_name;
                    $message_log->status = $status; 
                    if(!$message_log->save()){
                    	echo "<pre>";
                    	echo print_r($message_log->errors);
                    	exit;
                    }
                }
            }
            return $this->redirect(['MessageReceiver/index']);
        }else
        return $this->redirect(['messageCompose']);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Message;

	// Uncomment the following line if AJAX validation is needed
	// $this->performAjaxValidation($model);

		if(isset($_POST['Message']))
		{
			$model->attributes=$_POST['Message'];
			if($model->validate() && $model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

	// Uncomment the following line if AJAX validation is needed
	// $this->performAjaxValidation($model);

		if(isset($_POST['Message']))
		{
			$model->attributes=$_POST['Message'];
			if($model->validate() && $model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
	// we only allow deletion via POST request
			$this->loadModel($id)->delete();

	// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Message');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new Message('search');
	$model->unsetAttributes();  // clear any default values
	if(isset($_GET['Message']))
		$model->attributes=$_GET['Message'];

	$this->render('admin',array(
		'model'=>$model,
	));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=Message::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='message-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
