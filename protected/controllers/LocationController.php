<?php

class LocationController extends RController
{

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	/* public function accessRules()
	  {
	  return array(
	  array('allow',  // allow all users to perform 'index' and 'view' actions
	  'actions'=>array('index','view'),
	  'users'=>array('*'),
	  ),
	  array('allow', // allow authenticated user to perform 'create' and 'update' actions
	  'actions'=>array('create','update'),
	  'users'=>array('@'),
	  ),
	  array('allow', // allow admin user to perform 'admin' and 'delete' actions
	  'actions'=>array('admin','delete'),
	  'users'=>array('admin'),
	  ),
	  array('deny',  // deny all users
	  'users'=>array('*'),
	  ),
	  );
	  }
	 */

	public function actionIndex()
	{
		$this->render('location');
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		if (isset($_GET['key']) && !empty($_GET['key'])) {
			$key = $_GET['key'];
			if ($key == 'development_region') {
				$title = !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))->title) : "NULL";
				$form_name = '_form-developmentregion';
			}
			if ($key == 'zone') {
				$title = !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::ZONE))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::ZONE))->title) : "NULL";
				$form_name = '_form-zone';
			}
			if ($key == 'district') {
				$title = !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DISTRICT))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DISTRICT))->title) : "NULL";
				$form_name = '_form-district';
			}
			if ($key == 'vdc') {
				$title = !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::VDC))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::VDC))->title) : "NULL";
				$form_name = '_form-vdc';
			}
			if ($key == 'wordno') {
				$title = !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::WARD_NO))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::WARD_NO))->title) : "NULL";
				$form_name = '_form-wardno';
			}
		} else {
			$title = !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))->title) : "NULL";
			$form_name = '_form-developmentregion';
		}
		$model = new Location;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

		if (isset($_POST['Location'])) {
			$model->attributes = $_POST['Location'];
			if ($model->location_level == 1)
				$cd = Location::DEVELOPMENT_REGION;
			if ($model->location_level == 2)
				$cd = Location::ZONE;
			if ($model->location_level == 3)
				$cd = Location::DISTRICT;
			if ($model->location_level == 4)
				$cd = Location::VDC;
			if ($model->location_level == 5)
				$cd = Location::WARD_NO;
			if (!isset($cd))
				$cd = Location::DEVELOPMENT_REGION;
			$model->location_slug = Yii::app()->function->seoUrl($cd . " " . $model->title);
			$model->created_by = Yii::app()->user->id;
			$model->nepali_date = Yii::app()->session['today_nepali_date'];
			if ($model->validate() && $model->save())
				$this->redirect(array('view', 'id' => $model->id));
		}

		$this->render('create', array(
			'model' => $model, 'title' => $title, 'form_name' => $form_name
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{
		if ((isset($_GET['key']) && !empty($_GET['key'])) && (isset($_GET['id']) && !empty($_GET['id']))) {
			$key = $_GET['key'];
			$id = $_GET['id'];
			if ($key == 'development_region') {
				$title = !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))->title) : "NULL";
				$form_name = '_form-developmentregion';
			}
			if ($key == 'zone') {
				$title = !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::ZONE))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::ZONE))->title) : "NULL";
				$form_name = '_form-zone';
			}
			if ($key == 'district') {
				$title = !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DISTRICT))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DISTRICT))->title) : "NULL";
				$form_name = '_form-district';
			}
			if ($key == 'vdc') {
				$title = !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::VDC))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::VDC))->title) : "NULL";
				$form_name = '_form-vdc';
			}
			if ($key == 'wordno') {
				$title = !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::WARD_NO))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::WARD_NO))->title) : "NULL";
				$form_name = '_form-wardno';
			}

			$model = $this->loadModel($id);

			// Uncomment the following line if AJAX validation is needed
			// $this->performAjaxValidation($model);

			if (isset($_POST['Location'])) {
				$model->attributes = $_POST['Location'];
				if ($model->validate() && $model->save())
					$this->redirect(array('view', 'id' => $model->id));
			}

			$this->render('update', array(
				'model' => $model, 'title' => $title, 'form_name' => $form_name
			));
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
// we only allow deletion via POST request
			$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		} else
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = new Location('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Location']))
			$model->attributes = $_GET['Location'];

		$this->render('admin', array(
			'model' => $model,
		));
	}

	/**
	 * list of location branchese in dropdownlist,
	 * @param $key (parent_id)
	 */
	public function actiondropdownlocationbranches($key)
	{
		$data = Location::model()->findAllByAttributes(['parent_id' => $key]);


		if (!empty($data)) {
			$option = "<option value=''>Select Titles </option>";
			foreach ($data as $childData) {
				$option .= CHtml::tag('option', array('value' => $childData->id), CHtml::encode($childData->title), true);
			}
			echo CJSON::encode(['success' => true, 'option' => $option]);
		} else {
			echo CJSON::encode(['success' => false]);
		}
	}

	/**
	 * list location hirerchy,
	 * @param $key (parent_id)
	 */
	public function actionlocationHierarchy()
	{
		$level = isset($_POST['level']) ? $_POST['level'] : 0;
		$Location = isset($_POST['location']) ? $_POST['location'] : 0;
		$location_level = !empty(Location::model()->findByPk($Location)) ? Location::model()->findByPk($Location)->location_level : 0;
		$selected = Yii::app()->function->parent_idfunction($level, $Location, $location_level);
		$data = Location::model()->findAllByAttributes(['location_level' => $level]);
		if (!empty($data)) {
			$option = "<option value=''>Select Titles </option>";
			foreach ($data as $childData) {
				$selected_option = $childData->id == $selected ? true : false;
				$option .= CHtml::tag('option', array('value' => $childData->id, 'selected' => $selected_option), CHtml::encode($childData->title), true);
			}
			echo CJSON::encode(['success' => true, 'option' => $option]);
		} else {
			echo CJSON::encode(['success' => false]);
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model = Location::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'location-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}
