<?php

class StudentSubjectController extends RController
{

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights',
		//'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	*list of whole student with their fee status
	*/

	public function actionStudent(){
		$model = new StudentInformation();
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['RegistrationHistory']))
			$model->attributes = $_GET['RegistrationHistory'];

		$this->render('_student_list', array(
			'model' => $model,
		));
	}
	/**
	* Add Subject to the student,
	*/
	public function actionAddSubject(){
		if(is_numeric(Yii::app()->request->getQuery('reg_number'))){
			$reg_num = Yii::app()->request->getQuery('reg_number');
			$StudentInformation = StudentInformation::model()->findByPk($reg_num);
			if(!empty($StudentInformation)){
				$criteria = new CDBcriteria;
				$criteria -> condition = 'student_id=:reg_number AND status=:status ';
				$criteria -> params =[':reg_number'=>$reg_num, ':status'=>'active'];
				$books = CHtml::listData(SubjectInformation::model()->findAllByAttributes([
					'status' => 1
					]), 'id', 'title');
				$selected_keys = array_keys(CHtml::listData(StudentSubject::model()->findAll($criteria), 'subject_id' , 'subject_id'));


				$this->render('_add_subject', array('model'=>new StudentSubject, 'StudentInformation'=>$StudentInformation
					,'selected_keys'=>$selected_keys,'reg_num'=>$reg_num
				));
			}
			else
				$this->redirect(array('StudentSubject/Student'));
		}
		else
			$this->redirect(array('StudentSubject/Student'));
	}
	/**
	* subject added to student.. Check subject is previously added or not .. if subject is not present, then added
	*/

	public function actionSaveSubject(){
		$transaction = Yii::app()->db->beginTransaction();
		$error_log = [];
		if (isset($_POST)) {
			$reg_id = $_POST['reg_id'];
			$reg_number = $_POST['reg_num'];
			$class_id = $_POST['class_id'];
			$subject_ = $_POST['StudentSubject']['subject_id'];
			for ($i = 0; $i < sizeof($subject_); $i++) {
				if (is_numeric($subject_[$i])) {

					$subject_id = $subject_[$i];
					$subject_information = StudentSubject::model()->findByAttributes([
						'reg_id' => $reg_id,
						'reg_number' => $reg_number,
						'class_id' => $class_id,
						'subject_id' => $subject_id
					]);
					if (empty($subject_information)) {
						$model = new StudentSubject;
						$model->reg_id = $reg_id;
						$model->reg_number = $reg_number;
						$model->class_id = $class_id;
						$model->subject_id = $subject_id;
						$model->status = 'active';
						if ($model->validate() && $model->save())
							$error_log[] = 'true';
						else
							$error_log[] = 'false';
					}
				}
			}
			$criteria = new CDBcriteria;
			$criteria -> condition ='reg_id=:reg_id AND reg_number=:reg_number AND class_id=:class_id';
			$criteria -> params =[':reg_id'=>$reg_id,
			':reg_number'=>$reg_number,
			':class_id'=>$class_id
			];
			$criteria->addNotInCondition('subject_id', $subject_);
			StudentSubject::model()->deleteAll($criteria);
			if (!in_array('false', $error_log)) {
				$transaction->commit();
				$this->redirect(array('StudentSubject/ViewSubject/?reg_number=' . $reg_number));
			} else {
				$transaction->rollback();
				$this->redirect(array('StudentSubject/Student/?reg_number=' . $reg_number));
			}
		}
		else
			$this->redirect(array('StudentSubject/Student'));
	}
	/**
	* View Details of student Subject.
	*/
	public function actionViewSubject(){
		$model = new StudentSubject;
		if (is_numeric(Yii::app()->request->getQuery('reg_number'))) {
			$reg_num = Yii::app()->request->getQuery('reg_number');
			$RegistrationHistory = RegistrationHistory::model()->findByPk($reg_num);
			if (!empty($RegistrationHistory)) {
				$reg_id = $RegistrationHistory->reg_id;
				$class_id = $RegistrationHistory->class_id;
				$StudentInformation = UtilityFunctions::studentInformation($reg_id);
				$this->render('_view_subject', ['student_information' => $StudentInformation, 'model' => $model]);
			}
		} else
			$this->redirect(array('StudentSubject/Student'));
	}
	/**
	* Classes
	*/
	public function actionClasses(){
		$section_array = array();
		$criteria = new CDbCriteria;
		$criteria -> select = 'id, title, prgdetails_id';
		$criteria -> condition = "status =:status ";
		$criteria -> params = [':status'=>1];
		$classes_information = ClassInformation::model()->findAll($criteria);
		if( !empty($classes_information) ){
			foreach ($classes_information as $cls_information) {
				$cls_id = $cls_information -> id;
				$sectionInfromation = ClassSection::model()->class_SectionInformatin($cls_id);
				if(!empty($sectionInfromation)){
					foreach ($sectionInfromation as $sectionInfo) {
						$section_array[$cls_id]['id'][] = $sectionInfo->id;
						$section_array[$cls_id]['title'][] = $sectionInfo->title;
						$section_array[$cls_id]['class_id'][] = $sectionInfo->class_id;

					}
				}
			}
		}
		$this->render('_availableClasses', array('classes_information'=>$classes_information, 'section_array'=>$section_array));
	}

	/**
	* add subject to that student of classes
	*/
	public function actionassignSubject(){
		if(isset($_GET['class_id']) && is_numeric($_GET['class_id'])){

			$class_id = $_GET['class_id'];
			$model = new RegistrationHistory('search');
			$model->unsetAttributes();  // clear any default values
			if (isset($_GET['RegistrationHistory']))
				$model->attributes = $_GET['RegistrationHistory'];
			$model -> class_id = $_GET['class_id'];
			$class_information = ClassInformation::model()->findByPk($_GET['class_id']);
			if(isset($_GET['status']) && $_GET['status']==1){
				$msg_status = 'success';
				$msg ="Optional Subject is saved ......";
			}
			elseif(isset($_GET['status']) && $_GET['status']==0){
				$msg_status = 'danger';
				$msg ="Optional Subject Cannot saved, Please Check it.";
			}
			elseif(isset($_GET['status']) && $_GET['status']==2){
				$msg_status = 'success';
				$msg ="Compulsary Subject saved.......";
			}
			else{
				$msg_status = '';
				$msg ="";
			}
			$this->render('_add_subject',array('model'=>$model,'class_information'=>$class_information,'cls_id'=>$_GET['class_id'],'msg_status'=>$msg_status,'message'=>$msg));
		}
		else
			$this->redirect(array('/academic/StudentSubject/classes'));
	}

	/**
	*Adding Subject 
	*/
	public function actionSubmitOptionalSubject(){
			//die('working here');
		$error = [];

		if(isset($_POST['class_id']) && is_numeric($_POST['class_id']) ){

			$class_id = $_POST['class_id'];
			$count_student = RegistrationHistory::model()->countStudent($class_id);
			$optional_1= isset($_POST['optional_3_']) ? $_POST['optional_3_'] :  array();

			$optional_2= isset($_POST['optional_2_']) ? $_POST['optional_2_'] :  array();
			$reg_array= isset($_POST['reg_id_']) ? $_POST['reg_id_'] :  array();
			for ($i=0; $i < sizeof($optional_1); $i++) { 

				$reg_num = isset($reg_array[$i]) ? $reg_array[$i] : 0;
				$registrationHistory = RegistrationHistory::model()->findByPk($reg_num);
				$reg_id = !empty($registrationHistory) ? $registrationHistory -> reg_id : 0;	
				$class_id = !empty($registrationHistory) ? $registrationHistory -> class_id : 0;
				$opt_1_id = isset($optional_1[$i]) ? $optional_1[$i] : 0;
				$opt_2_id = isset($optional_2[$i]) ? $optional_2[$i] : 0;

				if($opt_1_id && $reg_id){
					$subject_ = SubjectInformation::model()->findByPk($opt_1_id);
					$subject_nature = !empty($subject_) ? $subject_->subject_nature : 0;
					$subject_information = StudentSubject::model()->findByAttributes([
						'reg_number'=>$reg_num,
						'subject_nature'=> $subject_nature
						]);
					$boolean_checking = !empty($subject_information) ? $subject_information->delete() : true;
					if($boolean_checking){
						$new_model = new StudentSubject;
						$new_model->attributes = $subject_->attributes;
						$new_model-> reg_id = $reg_id;
						$new_model-> reg_number = $reg_num;
						$new_model-> subject_id = $opt_1_id;
						if($new_model->validate() && $new_model->save())
							$optional_1_subject = StudentSubject::DeleteOptionall($reg_id, $class_id, 3, $opt_1_id);
					}
				}
				if($opt_2_id && $reg_id){
					$subject_2 = SubjectInformation::model()->findByPk($opt_2_id);
					$subject_nature = !empty($subject_2) ? $subject_2->subject_nature : 0;
					$subject_information__ = StudentSubject::model()->findByAttributes([
						'reg_number'=>$reg_num,
						'subject_nature'=> $subject_nature
						]);

					$boolean_checking_ = !empty($subject_information__) ? $subject_information__->delete() : true;
					if($boolean_checking_){
						$new_model = new StudentSubject;
						$new_model->attributes = $subject_2->attributes;
						$new_model-> reg_id = $reg_id;
						$new_model-> reg_number = $reg_num;
						$new_model-> subject_id = $opt_2_id;
						if($new_model->validate() && $new_model->save())
							$optional_2_subject = StudentSubject::DeleteOptionall($reg_id, $class_id, 2, $opt_2_id);
						}
					}
				}
			}
			$this->redirect(array('/academic/studentSubject/assignSubject'));
		}
	/**
		*Adding Subject to Student according to class
	*/
	public function actionSubmitCompulsarySubject(){
		$transaction = Yii::app()->db->beginTransaction();
		if(isset($_POST['class_id']) && is_numeric($_POST['class_id'])){
			$class_id = $_POST['class_id'];
			$compulsary_subject = StudentSubject::model()->ClassCompulsarySubject($class_id);
			if(!in_array('false', $compulsary_subject)){
				$transaction->commit();
				$this->redirect(array('/academic/StudentSubject/assignSubject/?class_id='.$class_id.'&status=2'));
			}
			else{
				$transaction->rollback();
				$this->redirect(array('/academic/StudentSubject/assignSubject/?class_id='.$class_id.'&status=2'));
			}

		}
		else
			$this->redirect(array('/academic/studentSubject/classes'));
	}
	/**
	* ADDIng compulsary subject to individuals 
	*/
	public function actionAddCompSubejct(){
		if(isset($_GET['reg_num']) && is_numeric($_GET['reg_num'])){
			$reg_num = $_GET['reg_num'];
			$registrationHistory = RegistrationHistory::model()->findByPk($reg_num);
			$reg_id = !empty($registrationHistory) ? $registrationHistory->reg_id : 0; 
			$class_id = !empty($registrationHistory) ? $registrationHistory->class_id : 0; 
			$compulsary_subject = StudentSubject::model()->SaveCompulsarySubject($reg_id, $reg_num, $class_id);
			if($compulsary_subject == 'true')
					$this->redirect(array('/academic/StudentSubject/assignSubject/?class_id='.$class_id.'&status=2'));
				else
					$this->redirect(array('/academic/StudentSubject/assignSubject/?class_id='.$class_id));
		}
		else
			$this->redirect(array('/academic/studentSubject/classes'));
	}




	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id){
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate(){
		$model = new StudentSubject;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['StudentSubject'])) {
			$model->attributes = $_POST['StudentSubject'];
			if ($model->validate() && $model->save())
				$this->redirect(array('view', 'id' => $model->id));
		}

		$this->render('create', array(
			'model' => $model,2
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id){
		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['StudentSubject'])) {
			$model->attributes = $_POST['StudentSubject'];
			if ($model->validate() && $model->save())
				$this->redirect(array('view', 'id' => $model->id));
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id){
		if (Yii::app()->request->isPostRequest) {
// we only allow deletion via POST request
			$this->loadModel($id)->delete();
// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		} else
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model = new StudentSubject('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['StudentSubject']))
			$model->attributes = $_GET['StudentSubject'];

		$this->render('admin', array(
			'model' => $model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = new StudentSubject('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['StudentSubject']))
			$model->attributes = $_GET['StudentSubject'];

		$this->render('admin', array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model = StudentSubject::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'student-subject-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


	/**
	*/
	public function actionAssignOptionalSubject(){
		$secondary_optional_subject = $student_list = $school_information = $resource_center_detail = $primary_optional_subject = $student_optional_subject = null;
		$model = new StudentSubject();
		if(isset($_GET['StudentSubject']['school_id']) && is_numeric($_GET['StudentSubject']['school_id'])){
			$school_id = $_GET['StudentSubject']['school_id'];
			$primary_optional_subject = SubjectInformation::model()->find('subject_nature=:subject_nature AND default_optional=:default_optional',[':subject_nature'=>3, ':default_optional'=>1]);
			$school_information = BasicInformation::model()->findByAttributes(['id'=>$school_id, 'status'=>1]);
			if(empty($school_information))
				throw new CHttpException(400, 'Invalid school. Please check it again.');
			$resource_center_detail = BasicInformation::model()->findByAttributes(['id'=>$school_information->resource_center_id, 'status'=>1]);
			$secondary_optional_subject = SubjectInformation::model()->find('subject_nature=:subject_nature AND school_id=:school_id AND default_optional=:default_optional',[':subject_nature'=>3, ':school_id'=>$school_id, ':default_optional'=>0]);
  			$student_list = StudentInformation::model()->findAll('academic_year=:academic_year AND school_id=:school_id AND status=:status',[':academic_year'=>UtilityFunctions::AcademicYear(), ':school_id'=>$school_id, ':status'=>1]);
			$student_optional_subject = StudentSubject::model()->StudentOptionalSubject($secondary_optional_subject, $student_list);
		}
		$this->render('_choose_optional_subject',['model'=>$model, 'secondary_optional_subject'=>$secondary_optional_subject, 'student_list' => $student_list, 'school_information' => $school_information, 'resource_center_detail' => $resource_center_detail, 'primary_optional_subject'=>$primary_optional_subject, 'student_optional_subject'=>$student_optional_subject]);
	}

	/**
	*
	*/
	public function actionSubmitSecondaryOptional(){
		if(isset($_POST['school_id']) && is_numeric($_POST['school_id'])){
			$school_id = $_POST['school_id'];
			$school_information = BasicInformation::model()->find('id=:school_id AND status=:status',[':school_id'=>$school_id, ':status'=>1]);
			if(empty($school_information))
				throw new CHttpException(400, 'Invalid school. Please check it again.');
			$is_save_secondary_option = StudentSubject::model()->SaveSecondaryOptionalSubject($_POST);
			if($is_save_secondary_option)
				$this->redirect(["/studentSubject/AssignOptionalSubject?StudentSubject%5Bschool_id%5D=".$school_id.'&status=1']);
			else
				$this->redirect(["/studentSubject/AssignOptionalSubject?StudentSubject%5Bschool_id%5D=".$school_id.'&status=0']);

		}
	}


}
