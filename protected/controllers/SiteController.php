<?php

class SiteController extends Controller
{

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if(Yii::app()->user->isGuest)
			$this->layout='guestmain';
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}


	public function actionIndex(){
		$this->layout = '_layout';
		$studentDetail = $result = $markInformation = [];
		$type = '';
		$academic_year = UtilityFunctions::AcademicYear();
		$model = new StudentInformation();
		if(isset($_POST['StudentInformation']['symbol_number']) && is_numeric($_POST['StudentInformation']['symbol_number'])) {
			$symbol_number = $_POST['StudentInformation']['symbol_number'];
			$studentDetail = StudentInformation::model()->find('symbol_number=:symbol_number',[':symbol_number'=>$symbol_number]);
			if(empty($studentDetail))
				throw new CHttpException(404,'The requested student does not exist.');
			$student_id = $studentDetail->id;
			$type = 'original';
			$result = Result::model()->find('student_id=:student_id AND status=:status',[':student_id'=>$student_id, ':status'=>1]);
			$markInformation = MarkObtained::model()->findAll('student_id=:student_id AND subject_status=:status',[':student_id'=>$student_id, ':status'=>1]);
			if(empty($result))
				throw new CHttpException(404,'Result doesnot publish, please check it again.');
		}
		$this->render('_result_page',['studentDetail'=>$studentDetail, 'result'=>$result, 'academic_year'=>$academic_year,'markInformation'=>$markInformation, 'type'=>$type,'model'=>$model]);
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model = new ContactForm;
		if (isset($_POST['ContactForm'])) {
			$model->attributes = $_POST['ContactForm'];
			if ($model->validate()) {
				$name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
				$subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
				$headers = "From: $name <{$model->email}>\r\n" .
					"Reply-To: {$model->email}\r\n" .
					"MIME-Version: 1.0\r\n" .
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
				Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact', array('model' => $model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$this->layout = 'login_layout';
		$model = new LoginForm;
		// collect user input data
		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login()){
				$this->lastViset();
				$this->redirect('/dashboard');
			}
		}
		// display the login form
		$this->render('login', array('model' => $model));
	}


	private function lastViset()
	{
		$lastVisit = Users::model()->notsafe()->findByPk(Yii::app()->user->id);
		$lastVisit->lastvisit = time();
		if($lastVisit->validate() && $lastVisit->save())
			return true;
		return false;
	}


	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	

}
