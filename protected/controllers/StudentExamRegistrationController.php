<?php

class StudentExamRegistrationController extends RController
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}


	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new StudentExamRegistration;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['StudentExamRegistration']))
		{
			$model->attributes=$_POST['StudentExamRegistration'];
			if($model->validate() && $model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	* SCHOOL LIST SECTION
	*/
	public function actionSchoolList(){
		$model = new StudentExamRegistration();
		$academic_year = UtilityFunctions::AcademicYear();
		if(isset($_POST['StudentExamRegistration']))
		{
			$type = isset($_POST['StudentExamRegistration']['type']) ? $_POST['StudentExamRegistration']['type'] : '';
			$order_by = isset($_POST['StudentExamRegistration']['order_by']) ? $_POST['StudentExamRegistration']['order_by'] : '';
			$schoolData = BasicInformation::model()->SchoolList($type, $order_by);
			$this->render('_assign_school_registration',['schoolData'=>$schoolData, 'academic_year'=>$academic_year]);
			exit;
		}

		$this->render('_school_list',array(
			'model'=>$model,
		));
	}

	/**
	* Generate Symbol Number Of Student
	*/

	public function actionGenerateStudentSymbolNo(){
		die('working here');
		$academic_year = UtilityFunctions::AcademicYear();
		$schoolInformation = BasicInformation::model()->findAllByAttributes(['id'=>[134,97,85]]);
		$transaction = Yii::app()->db->beginTransaction();
		if(!empty($schoolInformation)){
			foreach ($schoolInformation as $schools_info) {
				$school_id = $schools_info->id;
				$school_code = $schools_info->schole_code;
				$initial_symbol_no = $school_code.'000';
				$student_list = StudentInformation::model()->ApprovedStudentList($academic_year, $school_id, null);
				$sn = 1;
				if(!empty($student_list)){
					foreach ($student_list as $student_info) {
						$symbol_number = $initial_symbol_no + $sn;
						$student_info -> symbol_number = trim($symbol_number);
						if( !$student_info -> update()){
							$transaction->rollback();
							throw new CHttpException(400,'Invalid request. Symbol Number cannot generate of .'.ucwords($schools_info->title).'Student Name : '.ucwords($student_info->first_name.' '.$student_info->middle_name.' '.$student_info->last_name) );

						}
						$sn++;
					}
				}

				$model = new StudentExamRegistration();
				$studentRegistration = StudentExamRegistration::model()->find('school_id=:school_id AND academic_year=:academic_year AND status=:status',[':school_id'=>$school_id, ':academic_year'=>$academic_year, ':status'=>1]);
				if(empty($studentRegistration)){
					$model->academic_year = $academic_year;
					$model->school_id = $school_id;
					$model->start_point = $school_code.'001';
					$model->end_point = $symbol_number;
					$model->total_student = $sn-1;
					if(!$model->validate() || !$model->save())
						$error_array[] = 'false';
				}else{
					$studentRegistration->start_point = $school_code.'001';
					$studentRegistration->end_point = $symbol_number;
					$studentRegistration->total_student = $sn-1;
					if(!$studentRegistration->update())
						$error_array[] = 'false';
				}
			}
		}
		$transaction->commit();
		$this->redirect(['schoolWiseSymbolNo']);

	}
	/**
	*symbol number details school wise
	*/
	public function actionSchoolWiseStudent(){
		$this->layout= '_datatable_layout';
		$resource_center_det_info = BasicInformation::model()->ResourceCenterDetail(UtilityFunctions::AcademicYear());
		$resource_school_detail= isset($resource_center_det_info['school_array_data']) ? $resource_center_det_info['school_array_data'] : [];
		$schoolRegistrationList = StudentExamRegistration::model()->findAllByAttributes(['academic_year'=>UtilityFunctions::AcademicYear(), 'status'=>1]);

		$this->render('_student_number_school_wise_sample',['schoolRegistrationList'=>$schoolRegistrationList, 'resource_school_detail'=>$resource_school_detail]);
	}
	/**
	*
	*/

	public function actionSchoolWiseStudentPdf(){
		//$this->layout= '_datatable_layout';

		$resource_center_det_info = BasicInformation::model()->ResourceCenterDetail(UtilityFunctions::AcademicYear());
		$resource_school_detail= isset($resource_center_det_info['school_array_data']) ? $resource_center_det_info['school_array_data'] : [];
		$schoolRegistrationList = StudentExamRegistration::model()->findAllByAttributes(['academic_year'=>UtilityFunctions::AcademicYear(), 'status'=>1]);


       	$mPDF1 = Yii::app()->ePdf->mpdf();
        # You can easily override default constructor's params
        $mPDF1 = Yii::app()->ePdf->mpdf('UTF-8', 'A4');
        $mPDF1->writeHTMLfooter=true;

        $mPDF1->AddPage('P');
        $mPDF1->WriteHTML($this->renderPartial('_student_number_school_wise',['schoolRegistrationList'=>$schoolRegistrationList, 'resource_school_detail'=>$resource_school_detail], true));
        $mPDF1->Output($file);
	}



	/**
	*symbol number details school wise
	*/
	public function actionSchoolWiseSymbolNo(){
		$this->layout= '_datatable_layout';
		$schoolRegistrationList = StudentExamRegistration::model()->findAllByAttributes(['academic_year'=>UtilityFunctions::AcademicYear(), 'status'=>1]);
		$this->render('_school_registration_number_range',['schoolRegistrationList'=>$schoolRegistrationList]);
	}




	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['StudentExamRegistration']))
		{
			$model->attributes=$_POST['StudentExamRegistration'];
			if($model->validate() && $model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('StudentExamRegistration');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new StudentExamRegistration('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['StudentExamRegistration']))
			$model->attributes=$_GET['StudentExamRegistration'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=StudentExamRegistration::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='student-exam-registration-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
