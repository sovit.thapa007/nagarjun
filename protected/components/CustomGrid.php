<?php 

Yii::import('bootstrap.widgets.TbGridView');

class CustomGrid extends TbGridView
{
    /**
     * @var boolean whether the ID in the button options should be evaluated.
     */
    public function renderPager() {   


    if(!$this->enablePagination)
            return;

        $pager=array();
        $class='CLinkPager';
        if(is_string($this->pager))
            $class=$this->pager;
        elseif(is_array($this->pager))
        {
            $pager=$this->pager;
            if(isset($pager['class']))
            {
                $class=$pager['class'];
                unset($pager['class']);
            }
        }
        $pager['pages']=$this->dataProvider->getPagination();


        if($pager['pages']->getPageCount()>1)
        {
            echo '<div class="'.$this->pagerCssClass.'">';
            $this->widget($class,$pager);
            echo '</div>';
        }
        else {
            echo '
            <div class="pagination">
                <ul class="yiiPager" id="yw0">
                    <li class="previous disabled"><a href="javascript:void(0)">← &lt;</a></li>
                    <li class=""><a herf="">Page 1 of 1 </a></li>
                    <li class="next"><a href="javascript:void(0)">&gt; →</a>
                    </li>
                </ul>
            </div>' ;   
        }
    }

    /**
     * @var boolean whether the ID in the button options should be evaluated.
     */
    public function renderPagerWithPageSize() {   

        if(!$this->enablePagination)
            return;

        $pager = [];
        $pagerClass = 'BootPager';

        if(is_string($this -> pager))
            $class = $this -> pager;
        elseif(is_array($this -> pager)) {
            $pager = $this -> pager;
            if(isset($pager['class'])) {
                $class = $pager['class'];
                unset($pager['class']);
            }
        }

        $pager['pages'] = $this -> dataProvider -> getPagination();

        echo "<div class='{$this -> pagerCssClass}'>";
                $this -> widget($pagerClass, $pager);
                $this -> renderPageSize();
        echo "</div>";
    }


    /**
     * DropDown Page Size
     */
    public function renderPageSize() {      
        $totalPage = $this-> dataProvider -> getPagination() -> getItemCount();
        
        $pageSizes  = UtilityFunctions::pageSizes();
        $pageSize   = Yii::app()->user->getState('pageSize');

        $dropDown  = [];

        $dropDown[] = '<div class="pagi-R">';
        $dropDown[] = '<div class="wrap-page-per">'; 
        $dropDown[] = CHtml::dropDownList('page-size', $pageSize, $pageSizes, ['class' => '.page-size-grid']);
        $dropDown[] = '</div>';
        $dropDown[] = '</div>';

        echo  implode("\r\n", $dropDown);
    }
}
?>