<?php


class UtilityFunctions {

	/**
	 * Page Sizes
	 */

	public static function pageSizes() {
        $pageSizes = array( 10 => '10 per page', 20 => '20 per page', 50 => '50 per page', 100 => '100    per page', 250 => '250 per page');
        return $pageSizes;
	}


	/**
	 * Page Set And Usent
	 */

	public static function pageSetAndUnset() {
		Yii::app()->user->setState('pageSize', 10);

        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
            unset($_GET['pageSize']);
        }
	}

	public static function createrName($id)
	{

		$userInformation = Users::model()->findByPk($id);
		if (!empty($userInformation))
			return $userInformation->username;
		else
			return "null";
	}

    /**
     * This function will create an SEO friendly string.
     * @param raw string
     * @return seo friendly string
     */
    public static function seoUrl($string){
        $string = strtolower($string);
        //Make alphanumeric (removes all other characters)
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);
        return trim($string, '-');
    }


	public static function preBillPaying($reg_id, $amount_paid)
	{
		$remaining_amount = $amount_paid;
		$item_id = array();
		if (isset($reg_id) && is_numeric($reg_id)) {
			$to_paid_amt = 0;
			$new_paid_amt = 0;
			$criteria = new CDbCriteria;
			$criteria->select = ' discount_amount ,amount, fee_id';
			$criteria->order = ' amount DESC';
			$criteria->condition = ' reg_id =:reg_id AND fee_status=:fee_status ';
			$criteria->params = [':reg_id' => $reg_id, ':fee_status' => 0];
			$student_fee = StudentFee::model()->findAll($criteria);
			foreach ($student_fee as $fee_info) {
				$new_amt = $fee_info->amount - $fee_info->discount_amount;
				$to_paid_amt += $new_amt;
				if ($remaining_amount >= $new_amt) {
					$remaining_amount -= $new_amt;
					$item_id[] = $fee_info->fee_id;
					$new_paid_amt += $new_amt;
				}
			}
			$result = $amount_paid - $new_paid_amt;
			if($amount_paid - $to_paid_amt >=  0 && $result>=0)
				$status = 'clear';
			else
				$status = 'due';
		}	
		return [$result, $status];
	}
	public static function studentInformation($reg_id){
		$basicInformation = array();
		$registration = Registration::model()->findByPk($reg_id);
		if (!empty($registration)){
			$basicInformation['name'] = $registration->name;
			$basicInformation['sex'] = $registration->sex;
		}
		$registration_history = RegistrationHistory::model()->findByAttributes(array('reg_id' => $reg_id, 'status' => 'active', 'current_status' => 1));
		if (!empty($registration_history)) {
			$_Section = !empty(ClassSection::model()->findByPk($registration_history->section_id)) ? ClassSection::model()->findByPk($registration_history->section_id)->title : 'Null';
			$class_id = $registration_history->class_id;
			$_class = !empty(ClassInformation::model()->findByPk($class_id)) ? ClassInformation::model()->findByPk($class_id)->title : 'Null';
			$program_id = Yii::app()->function->classInfo($class_id, 4);
			$_roll_number = $registration_history->roll_number;
			$_programInformation = !empty(ProgramDetails::model()->findByPk($program_id)) ? ProgramDetails::model()->findByPk($program_id)->title : 'Null';
			$basicInformation['program'] = $_programInformation;
			$basicInformation['class'] = $_class;
			$basicInformation['section'] = $_Section;
			$basicInformation['rollNumber'] = $_roll_number;
		}
		return $basicInformation;
	}

	public static function dateConveter($nepali_date){
		$english_date = date('Y-m-d');
		$data_information = DateInformation::model()->findByAttributes(['nepali_date'=>$nepali_date]);
		if(!empty($data_information))
			$english_date = $data_information -> english_date;
		return $english_date;
	}


	public static function AdmissionFeeRules($reg_type, $class_id){
		$rules_array = array();
		$fee_array = [];
		$charge_items = [];
		$program_information = ClassInformation::model()->findByPk($class_id);
		if (!empty($program_information)) {
			$prg_id = $program_information->prgdetails_id;
			$level_id = !empty(ProgramDetails::model()->findByPk($prg_id)) ? ProgramDetails::model()->findByPk($prg_id)->shoollevel_id : 0;
			$fee_rules = FeeSetting::model()->findAllByAttributes(array('class_id' => $class_id, 'section' => $reg_type));
			if (empty($fee_rules))
				$fee_rules = FeeSetting::model()->findAllByAttributes(array('program_id' => $prg_id, 'section' => $reg_type));
			if (empty($fee_rules))
				$fee_rules = FeeSetting::model()->findAllByAttributes(array('school_level_id' => $level_id, 'section' => $reg_type));
			if (empty($fee_rules))
				$fee_rules = FeeSetting::model()->findAllByAttributes(array('section' => $reg_type));
			if (!empty($fee_rules)) {
				foreach ($fee_rules as $rl_info) {
					$item_id = $rl_info->fee_id;
					$fee_array[] = $item_id;
					$feeAmount = FeeAmount::model()->findByAttributes(array('class_id' => $class_id, 'fee_title_id' => $item_id));
					if (empty($feeAmount))
						$feeAmount = FeeAmount::model()->findByAttributes(array('program_id' => $prg_id, 'fee_title_id' => $item_id));
					if (empty($feeAmount))
						$feeAmount = FeeAmount::model()->findByAttributes(array('school_level_id' => $level_id, 'fee_title_id' => $item_id));
					if (empty($feeAmount))
						$feeAmount = FeeAmount::model()->findByAttributes(array('fee_title_id' => $item_id));
					if (!empty($feeAmount)) {
						$charge_items[] = $feeAmount->id;
					}
				}
			}
		}
		$rules_array['fee_id'] = $fee_array;
		$rules_array['charge_id'] = $charge_items;
		return $rules_array;
	}
	public static function TotalSum($record, $index)
	{
		$total = 0;
		if($record && !empty($record)){
		foreach ($record as $rc) {
			$total += $rc-> $index;
		}
		}
		return $total;
	}


	public static function SchoolInformation(){
		try {
			$school_information =  BasicInformation::model()->find();
			return $school_information;
		} catch (Exception $e) {
			return 'null';
		} 
	}

	public static function LedgerHeading($school_id, $terminal_id, $academic_year, $is_final){
		$schoolInformation = BasicInformation::model()->findByPk($school_id);
		if($schoolInformation){
			$terminalInformation = Terminal::model()->findByPk($terminal_id);
			$message = "<h4><strong>FINAL LEDGER OF ".strtoupper($schoolInformation -> title).' : '.$academic_year."</strong></h4>";
		}else{
			$message = "<h4><strong>FINAL LEDGER OF  : ".$academic_year."</strong></h4>";
		}
		return $message;

	}


	/**
     * This function will provide parent id according to level
     * @param (which level location to get,current location id, current location )
     * @return $parent_id
     */

   	public static function FullLocation($child_id)
    {
        $level = 1; //later will be dynamic
        $location_level = 3; //later will be dynamic
        $title_array = array();
        $i = 3;//Location::LOCATION_LEVEL;
        while ($i >= $level) {
                $location_infromation_1 = Location::model()->findByPK($child_id);
                $title_array[] = !empty($location_infromation_1) ? $location_infromation_1['title'] : "Null";
                $child_id = !empty($location_infromation_1) ? $location_infromation_1['parent_id'] : 0;
            $i--;
        }
        $title_array_new = array_reverse($title_array, true);
        return $title_array_new;
    }


	/**
	*dynamic column name for gridview
	*/
	public static function ColumnName($string){

		$string = strtolower($string);
		//Make alphanumeric (removes all other characters)
		$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
		//Clean up multiple dashes or whitespaces
		$string = preg_replace("/[\s-]+/", " ", $string);
		//Convert whitespaces and underscore to dash
		$string = preg_replace("/[\s_]/", "_", $string);
		return trim(str_replace(".","",$string), '_');
	}




    public static function StudentRegistrationNumber($registration_id)
    {
        $registration_num = '';
        $school_informaiton = BasicInformation::model()->find();
        $code = $school_informaiton && !empty($school_informaiton) ? $school_informaiton->schole_code : 'null';
        $active_academic_year = AcademicYear::model()->findByAttributes(['status'=>'active']);
        $year = $active_academic_year && !empty($active_academic_year) ? substr($active_academic_year->year, 2, 4) : '00';
        if(!empty($school_informaiton)){
            $string_len = strlen($registration_id);
            if($string_len==0){$crn_num='000000'.$registration_id;}
            if($string_len==1){$crn_num='00000'.$registration_id;}
            if($string_len==2){$crn_num='0000'.$registration_id;}
            if($string_len==3){$crn_num='000'.$registration_id;}
            if($string_len==4){$crn_num='00'.$registration_id;}
            if($string_len==5){$crn_num='0'.$registration_id;}
            if($string_len==6){$crn_num=$registration_id;}
            $registration_num = $code.'-'.$crn_num.'/'.$year;
        }
        return $registration_num;
    }


    public static function AcademicYear(){
    	$active_academic_year = AcademicYear::model()->findByAttributes(['status'=>'active']);
        $year = $active_academic_year && !empty($active_academic_year) ? $active_academic_year->year : '0000';
        return $year;
    } 


    
    /**
    *nepali to english Date Converter.
    */
    public static function NepaliToEnglish($nepali_date){
    	if($nepali_date !='0000-00-00'){
	        $calender = new NepaliCalender();
	        $today_explore = explode('-', $nepali_date);
	        if(isset($today_explore[0]) && isset($today_explore[1]) && isset($today_explore[2]))
	            $today_information = $calender->nep_to_eng($today_explore[0], $today_explore[1], $today_explore[2]);
	        $year = isset($today_information['year']) ? $today_information['year'] : '0000';
	        $month = isset($today_information['month']) ? $today_information['month'] : '00';
	        $date = isset($today_information['date']) ? $today_information['date'] : '00';
	        return $year.'-'.$month.'-'.$date;
    	}
    	return date('Y-m-d');
    }
    /**
    *nepali to english Date Converter.
    */
    public static function EnglishToNepali($english_date){
        $calender = new NepaliCalender();
        $today_explore = explode('-', $english_date);
        $today_information = $calender->eng_to_nep($today_explore[0], $today_explore[1], $today_explore[2]);
        $year = isset($today_information['year']) ? $today_information['year'] : '0000';
        $month = isset($today_information['month']) ? $today_information['month'] : '00';
        $date = isset($today_information['date']) ? $today_information['date'] : '00';
        return $year.'-'.$month.'-'.$date;
    }

    public static function SchoolHeading(){
    	$heding_section = "";
    	$school_information = BasicInformation::model()->find();
    	if(!empty($school_information)){
    		$heding_section .="<h2>".$school_information->title."</h2>";
    		$heding_section .="<h4>".$school_information->tole.' , '." Estd.".$school_information->establish_date."</h4>";
    	}
    	return $heding_section;
    }




	/*maximum marks of academic year, terminal , class*/
    public static function SubjectHighestMarks($academic_year, $terminal_id, $class_id){
    	$subject_information = [];
    	$criteria = new CDbCriteria;
    	$criteria -> select = "max(total_mark) as total_mark, subject_id, subject_name";
    	$criteria -> condition = "academic_year=:academic_year AND subject_status=:subject_status AND class_id=:class_id AND terminal_id=:terminal_id";
    	$criteria -> params = [':academic_year'=>$academic_year,':subject_status'=>1,':class_id'=>$class_id,':terminal_id'=>$terminal_id];
    	$criteria -> group = "subject_id";
    	$subject_marks_information = MarkObtained::model()->findAll($criteria);
    	if(!empty($subject_marks_information)){
    		foreach ($subject_marks_information as $subject_marks)
    			$subject_information[$subject_marks->subject_id] = $subject_marks->total_mark;
    	}
    	return $subject_information;
    }


	/*average marks of academic year, terminal , class*/
    public static function SubjectAverageMarks($academic_year, $terminal_id, $class_id){
    	$subject_information = [];
    	$criteria = new CDbCriteria;
    	$criteria -> select = "avg(total_mark) as total_mark, subject_id, subject_name";
    	$criteria -> condition = "academic_year=:academic_year AND subject_status=:subject_status";
    	$criteria -> params = [':academic_year'=>$academic_year,':subject_status'=>1];
    	if($class_id){
	    	$criteria -> condition .= " AND class_id=:class_id";
			$criteria -> params = array_merge($criteria->params, [':class_id'=>$class_id]);
    	}
    	if($terminal_id){
	    	$criteria -> condition .= " AND terminal_id=:terminal_id";
			$criteria -> params = array_merge($criteria->params, [':terminal_id'=>$terminal_id]);
    	}
    	$criteria -> group = "subject_id";
    	$subject_marks_information = MarkObtained::model()->findAll($criteria);
    	if(!empty($subject_marks_information)){
    		foreach ($subject_marks_information as $subject_marks)
    			$subject_information[$subject_marks->subject_id] = $subject_marks->total_mark;
    	}
    	return $subject_information;
    }

	/*lowest marks of academic year, terminal , class*/
    public static function SubjectLowestMarks($academic_year, $terminal_id, $class_id){
    	$subject_information = [];
    	$criteria = new CDbCriteria;
    	$criteria -> select = "min(total_mark) as total_mark, subject_id, subject_name";
    	$criteria -> condition = "academic_year=:academic_year AND subject_status=:subject_status";
    	$criteria -> params = [':academic_year'=>$academic_year,':subject_status'=>1];
    	if($class_id){
	    	$criteria -> condition .= " AND class_id=:class_id";
			$criteria -> params = array_merge($criteria->params, [':class_id'=>$class_id]);
    	}
    	if($terminal_id){
	    	$criteria -> condition .= " AND terminal_id=:terminal_id";
			$criteria -> params = array_merge($criteria->params, [':terminal_id'=>$terminal_id]);
    	}
    	$criteria -> group = "subject_id";
    	$subject_marks_information = MarkObtained::model()->findAll($criteria);
    	if(!empty($subject_marks_information)){
    		foreach ($subject_marks_information as $subject_marks)
    			$subject_information[$subject_marks->subject_id] = $subject_marks->total_mark;
    	}
    	return $subject_information;
    }


    /**
    * average gpa
    */
    public static function GPACalculation($gp_sum, $totalFM){
    	$gpa = $totalFM > 0 ? $gp_sum*100/$totalFM : 0;
    	return $gpa;
    }

    public static function GradeCreditHours($full_marks){
	    	$grade_setting =  Yii::app()->params['grade_setting'];
	    	if(!empty($grade_setting)){
	    		foreach ($grade_setting as $key => $value){
	    			$standard_weight = $key;
	    			$standard_credits = $value;
	    		}
	    	}
	    	if(!isset($standard_weight) && !isset($standard_credits))
	    		die('standard weight and credit hrs is not set in config main files');
	    	$crd_ratio = $standard_weight > 0 ? $full_marks/$standard_weight : 0;
	    	return $standard_credits*$crd_ratio;
    }


    public static function UserInformations(){
    	$user_information = array();
		$user_id = Yii::app()->user->id;
		$user_section = Users::model()->findByPk($user_id);
		$user_information['super_user'] = !empty($user_section) ? $user_section->superuser : null;
		$user_information['school_id'] = !empty($user_section) ? $user_section->school_id : null;

    	$active_academic_year = AcademicYear::model()->findByAttributes(['status'=>'active']);
        $user_information['academic_year'] = $active_academic_year && !empty($active_academic_year) ? $active_academic_year->year : '0000';
		$user_information['role'] = !empty($user_section) ? $user_section->role : null;
        return $user_information;
    }

    public static function ShowSchool(){
    	$user_information = array();
		$user_id = Yii::app()->user->id;
		$user_section = Users::model()->findByPk($user_id);
		$role = !empty($user_section) ? $user_section->role : null;
        if(in_array($role,['administrativeofficer','dataentry','superadmin']))
			return true;
		return false;
    }


    public static function CanPublishResult(){
    	$user_information = array();
		$user_id = Yii::app()->user->id;
		$user_section = Users::model()->findByPk($user_id);
		$role = !empty($user_section) ? $user_section->role : null;
        if(in_array($role,['administrativeofficer','superadmin']))
			return true;
		return false;
    }

    /**
    *
    */
    public static function IsSuperAdmin(){
		$user_id = Yii::app()->user->id;
		$user_section = Users::model()->findByPk($user_id);
		$role = !empty($user_section) ? $user_section->role : null;
        if($role == 'superadmin')
			return true;
		return false;

    }

    public static function AdminSection(){
		$user_id = Yii::app()->user->id;
		$user_section = Users::model()->findByPk($user_id);
		$role = !empty($user_section) ? $user_section->role : null;
		$school_id = !empty($user_section) ? $user_section->school_id : null;
        if(in_array($role,['administrativeofficer','dataentry','superadmin']))
			return strtoupper(Yii::app()->params['municipality']);
		else{
			$school_information = BasicInformation::model()->findByPk($school_id);
			return $school_information ? strtoupper($school_information->title) : '';
		}
    }

    public static function HomeHeading(){
		$user_id = Yii::app()->user->id;
		$user_section = Users::model()->findByPk($user_id);
		$super_user = !empty($user_section) ? $user_section->superuser : null;
		$school_id = isset(Yii::app()->session['school_id']) ? Yii::app()->session['school_id'] : $user_section->school_id;
		if($super_user)
			return " <div class='row-fluid text-center'><div style='font-size:20px; font-weight:bold;'> NAGARJUN MUNICIPALITY</div><div style='font-size:15px; font-weight:bold;'> HARISIDDHI, KATHMANDU (PROVINCE-3)</div>
				<div style='font-size:13px; font-weight:bold;' >BASIC LEVEL EXAMINATION</div>
    	</div> ";
		else{
			$school_information = BasicInformation::model()->findByPK($school_id);
			$title_section = !empty($school_information) ? $school_information->title : ' ';
			return $title_section;
		}

    }


    public static function SuperUser(){
    	$user_information = array();
		$user_id = Yii::app()->user->id;
		$user_section = Users::model()->findByPk($user_id);
		$super_user = !empty($user_section) ? $user_section->superuser : null;
		if($super_user && $super_user==1)
			return true;
		return false;
    }

    /*admin section for menu*/
    public static function AdminMenuSection(){
    	$user_information = array();
		$user_id = Yii::app()->user->id;
		$user_section = Users::model()->findByPk($user_id);
		if(empty($user_section))
			return false;
		$roleInformation = AuthAssignment::model()->find('userid=:userid',[':userid'=>$user_id]);
		if(empty($roleInformation) && $user_section->superuser != 1)
			return false;
		if(in_array($roleInformation->itemname, ['admin','administrativeofficer']) || $user_section->superuser == 1)
			return true;
    }
    public function checkStaffHistory($staff_id,$year,$month,$department)
	{
		return MonthlyPayroll::model()->findByAttributes(array('staff_id'=>$staff_id,'month'=>$month,'year'=>$year,'department_id'=>$department)) == null ? true:false;

	}
	public static function reports()
	{
		$array=array('government','private','annex','plus2','library');
		return $array;
	}

	public static function getMonth()
    {
    		$month = array();
    		$month['']="Select a Month";
		    $month[1]="Baisakh";
		    $month[2]="Jestha";
		    $month[3]="Asar";
		    $month[4]="Shrawan";
		    $month[5]="Bhadra";
		    $month[6]="Asoj";
		    $month[7]="Kartik";
		    $month[8]="Mangsir";
		    $month[9]="Poush";
		    $month[10]="Magh";
		    $month[11]="Falgun";
		    $month[12]="Chaitra";
		    return $month;
    }


    public static function getYear()
    {
    		$month = array();
    		$month['']="Select a Year";
		    $month[2072]=2072;
		    $month[2073]=2073;
		    $month[2074]=2074;
		    $month[2075]=2075;
		    $month[2076]=2076;
		    $month[2077]=2077;
		    $month[2078]=2078;
		    $month[2079]=2079;
		   
		    return $month;
    }


    public static function getBankReportHeader()
    {

    		$name = array();
    		$name['']="Select a Department";
		    $name["government"]="Government Grant";
		    $name["private"]="Private Fund";
		    $name["annex"]="Annex";
		    $name["plus2"]="+2";
		    $name["library"]="Library"; 
		    return $name;

    }

   public static function getPartTimeStaffInfo($id,$month)
    {
    	if($month == 0)
    	{
    	$info=PartTimeTeacher::model()->findByAttributes(array('staff_id'=>$id));
    	}
    	else
    	{
    	$info =PartTimeTeacher::model()->findByAttributes(array('staff_id'=>$id,'month'=>$month));
    	}
    	return $info;

    }

    public static function taxAmount($gsalary,$nsalary,$yearlyIncome,$staff,$special)
    {
    		if($staff->marital_status == 'Married')
    			$married_status='married';
    		else
    			$married_status='single';

    	  $taxModel=PayrollTaxSlack::model()->findByAttributes(array('type'=>$married_status));
    	  $special_tax=StaffSpecialCase::model()->findByPk($special)->custom_tax;
    	  $level=Level::model()->findByPk($staff->level_id);
    		if($special_tax != null)
    		{
    			return (($special_tax/100) * $yearlyIncome);
    		}



   
    	   if($yearlyIncome < $taxModel->first_tax_amount)
            {

                 $firstTaxSlack=($taxModel->first_tax_percent/100) *($yearlyIncome);
                  $tax=$firstTaxSlack;
               
            }

            else
            {
                $firstTaxSlack=($taxModel->first_tax_percent/100) *$taxModel->first_tax_amount;
            }

 
            if($yearlyIncome > $taxModel->first_tax_amount && $yearlyIncome < $taxModel->second_tax_amount)
            {
                //$firstTaxSlack=(1/100) *300000;
                $secondTaxSlack=($taxModel->second_tax_percent/100)* ($yearlyIncome - $taxModel->first_tax_amount);
                $tax=$firstTaxSlack + $secondTaxSlack;
            }

            elseif($yearlyIncome > $taxModel->third_tax_amount)
            {
                //$firstTaxSlack=(1/100) *300000;
                $secondTaxSlack=($taxModel->second_tax_percent/100)* ($taxModel->second_tax_amount - $taxModel->first_tax_amount);
                $thirdTaxSlack=($taxModel->third_tax_percent/100)* ($yearlyIncome-$taxModel->second_tax_amount);
                $tax=$firstTaxSlack +$secondTaxSlack +$thirdTaxSlack;
            }
            return $tax;

    }

    /**
    *
    */
    public static function MunicipalLedgerHeader($academic_year = null){
    	$academic_year = $academic_year ? $academic_year : UtilityFunctions::AcademicYear();
    	$header = Yii::app()->params['municipality'].'<br />'.Yii::app()->params['office'].'<br />'.Yii::app()->params['address'].'<br /><font style="font-size:19pt !important;">'.Yii::app()->params['application'].'</font><br /> CLASS-8, '.$academic_year.' B.S.';
    	return $header;
    }

    public static function WardDropDown(){
    	$vdcmu_id = Yii::app()->params['vdcmu_id'];
    	$vdcmu_information = Vdcmu::model()->findByPk($vdcmu_id);
    	$ward_array = [];
    	if(!empty($vdcmu_information)){
    		$totalward = $vdcmu_information->ward;
    		for ($i=1; $i<= $totalward ; $i++) { 
    			$ward_array[$i] = $i;
    		}
    	}
    	return $ward_array;
    }

    /**
    *grade information based on 
    */
    public static function GradeInformation($raw_obtained_mark, $full_marks){
    	if(is_numeric($raw_obtained_mark) && is_numeric($full_marks)){
	    	$obtained_mark = $full_marks > 0 && $raw_obtained_mark > 0 ?  $raw_obtained_mark*100/$full_marks : 0;
	    	$ratio = $full_marks != 0 ? $full_marks/100 : 0;
	    	//general concept 100/25 = 4 generally and 90/4 = 3.6
    		$grade = "Abs<sup>*</sup>";
    		$grade_point = $ratio * 0;
	    	if($obtained_mark <= 100 && $obtained_mark >= 90){
	    		$grade = "A<sup>+</sup>";
	    		$grade_point = $ratio * 4;
	    	}/*
	    	if($raw_obtained_mark <= 100 && $raw_obtained_mark >= 90)
	    		$grade_point = $ratio * 4;*/
	    	if($obtained_mark < 90 && $obtained_mark >= 80){
	    		$grade = "A";
	    		$grade_point = $ratio * 3.6;
	    	}/*
	    	if($raw_obtained_mark < 90 && $raw_obtained_mark >= 80)
	    		$grade_point = $ratio * 3.6;*/
	    	if($obtained_mark < 80 && $obtained_mark >= 70){
	    		$grade = "B<sup>+</sup>";
	    		$grade_point = $ratio * 3.2;
	    	}/*
	    	if($raw_obtained_mark < 80 && $raw_obtained_mark >= 70)
	    		$grade_point = $ratio * 3.2;*/
	    	if($obtained_mark < 70 && $obtained_mark >= 60){
	    		$grade = "B";
	    		$grade_point = $ratio * 2.8;
	    	}/*
	    	if($raw_obtained_mark < 70 && $raw_obtained_mark >= 60)
	    		$grade_point = $ratio * 2.8;*/
	    	if($obtained_mark < 60 && $obtained_mark >= 50){
	    		$grade = "C<sup>+</sup>";
	    		$grade_point = $ratio * 2.4;
	    	}/*
	    	if($raw_obtained_mark < 60 && $raw_obtained_mark >= 50)
	    		$grade_point = $ratio * 2.4;*/
	    	if($obtained_mark < 50 && $obtained_mark >= 40){
	    		$grade = "C";
	    		$grade_point = $ratio * 2;
	    	}/*
	    	if($raw_obtained_mark < 50 && $raw_obtained_mark >= 40)
	    		$grade_point = $ratio * 2;*/
	    	if($obtained_mark < 40 && $obtained_mark >= 30){
	    		$grade = "D<sup>+</sup>";
	    		$grade_point = $ratio * 1.6;
	    	}/*
	    	if($raw_obtained_mark < 40 && $raw_obtained_mark >= 30)
	    		$grade_point = $ratio * 1.6;*/
	    	if($obtained_mark < 30 && $obtained_mark >= 20){
	    		$grade = "D";
	    		$grade_point = $ratio * 1.2;
	    	}/*
	    	if($raw_obtained_mark < 30 && $raw_obtained_mark >= 20)
	    		$grade_point = $ratio * 1.2;*/
	    	if($obtained_mark < 20 && $obtained_mark > 0){
	    		$grade = "E";
	    		$grade_point = $ratio * .8;
	    	}/*
	    	if($raw_obtained_mark < 20 && $raw_obtained_mark > 0)
	    		$grade_point = $ratio * .8;*/
	    	if(!isset($grade)){
	    		echo "<pre>";
	    		echo "Error in Full Mark and Pass Marks <br />";
	    		echo " raw obtained_mark : ".$raw_obtained_mark.' && full marks : '.$full_marks;
	    		exit;
	    	}
	    	return array('grade'=>$grade, 'grade_point'=>$grade_point);
    	}
    }
    /**
    *grade letter based on grade point
    */
    public static function GradeLetter($gradePoint){
    	if($gradePoint <= 4 && $gradePoint > 3.6)
    		return "A<sup>+</sup>";
    	if($gradePoint <= 3.6 && $gradePoint > 3.2)
    		return "A";
    	if($gradePoint <= 3.2 && $gradePoint > 2.8)
    		return "B<sup>+</sup>";
    	if($gradePoint <= 2.8 && $gradePoint > 2.4)
    		return "B";
    	if($gradePoint <= 2.4 && $gradePoint > 2)
    		return "C<sup>+</sup>";
    	if($gradePoint <= 2 && $gradePoint > 1.6)
    		return "C";
    	if($gradePoint <= 1.6 && $gradePoint > 1.2)
    		return "D<sup>+</sup>";
    	if($gradePoint <= 1.2 && $gradePoint > 0.8)
    		return "D";
    	if($gradePoint <= 0.8 && $gradePoint > 0)
    		return "E";
    	if($gradePoint == 0)
    		return "Abs<sup>*</sup>";
    }


    public static function GPARange($grade){
		if($grade == "A<sup>+</sup>")
			return 'above 3.6 To 4';
		if($grade == "A")
			return 'above 3.2 To 3.6';
		if($grade == "B<sup>+</sup>")
			return 'above 2.8 To 3.2';
		if($grade == "B")
			return 'above 2.4 To 2.8';
		if($grade == "C<sup>+</sup>")
			return 'above 2.0 To 2.4';
		if($grade == "C")
			return 'above 1.6 To 2.0';
		if($grade == "D<sup>+</sup>")
			return 'above 1.2 To 1.6';
		if($grade == "D")
			return 'above 0.8 To 1.2';
		if($grade == "E")
			return '0.8 & below';
		if($grade == "Abs<sup>*</sup>")
			return 'Abs<sup>*</sup>';
    }

    public static function VerifyAuthentication($studentId){
		$userInformation =  UtilityFunctions::UserInformations();
		$role = isset($userInformation['role']) ? $userInformation['role'] :  null;
		$previousRole = UtilityFunctions::PreviousRole($role);
		$academic_year = UtilityFunctions::AcademicYear();
		$criteria = new CDbCriteria;
		$criteria -> condition = 'id=:id AND state=:state AND state_status!=:state_status AND status=:status AND academic_year=:academic_year';
		$criteria -> params = [':id'=>$studentId,':state'=>$role, ':state_status'=>'accepted', ':status'=>1, ':academic_year'=>$academic_year];
		if($previousRole){
			$criteria -> condition .= ' OR (id=:id AND state=:previousRole AND state_status=:prv_state_status )';
			$criteria -> params = array_merge($criteria->params, [':id'=>$studentId,':previousRole'=>$previousRole, ':prv_state_status'=>'accepted']);
		}
		$studentInformation = StudentInformation::model()->find($criteria);
		return $studentInformation ?  true : false;
    }



    public static function RoleArray($role){
    	$roleArray = StudentProcessing::APPROVALSTATE;
    	$arrayIndex = array_search($role, $roleArray);
    	$needArray = [];
    	for ($i= $arrayIndex+1; $i < sizeof($roleArray) ; $i++) { 
    		$needArray[] = $roleArray[$i];
    	}
    	return $needArray;
    }

    public static function PreviousRole($role){
    	$roleArray = StudentProcessing::APPROVALSTATE;
    	$arrayIndex = array_search($role, $roleArray);
    	$prvRole = isset($roleArray[$arrayIndex-1]) ?  $roleArray[$arrayIndex-1] : null;
    	return $prvRole;
    }

    public static function SymbolNumber($academic_year, $generalNumber){
    	$lastYearNum = substr($academic_year,2,4);
    	$maxLimit = isset(Yii::app()->params['synLimit']) ? Yii::app()->params['synLimit'] : 9999;
    	$numinf = $generalNumber > 0 ? $generalNumber/$maxLimit : 0;
		$alph = 'A';
    	if($numinf > 1){
    		$wholeNumber = floor($numinf);      // 1
			$fractionNumber = $numinf - $wholeNumber; // .25
			$generalNumber = round($fractionNumber*$maxLimit);
			if($wholeNumber==2 ){$alph = 'B'; }
			if($wholeNumber==3 ){$alph = 'C'; }
			if($wholeNumber==4 ){$alph = 'D'; }
			if($wholeNumber==5 ){$alph = 'E'; }
			if($wholeNumber==6 ){$alph = 'F'; }
			if($wholeNumber==7 ){$alph = 'G'; }
    	}
    	if(strlen($generalNumber) == 3){$generalNumber = '0'.$generalNumber; }
    	if(strlen($generalNumber) == 2){$generalNumber = '00'.$generalNumber; }
    	if(strlen($generalNumber) == 1){$generalNumber = '000'.$generalNumber; }
    	$SymbolNumber=$lastYearNum.$generalNumber.$alph;
    	return $SymbolNumber;
    }


    public static function StudentExamRegistrationNumber($academic_year, $vdcMu, $schoolCode, $generalNumber){
    	$lastYearNum = substr($academic_year,2,4);
    	$totalCodeLength = strlen($schoolCode);
    	$startingNum = $totalCodeLength - 3 > 0 ? $totalCodeLength - 3 : 0;
    	$schlCode = substr($schoolCode, $startingNum, $totalCodeLength);
    	$codeAlp = $schlCode;
		if($schlCode==0 ){$codeAlp = '---'; }
		if($schlCode==1 ){$codeAlp = '--'.$schlCode; }
		if($schlCode==2 ){$codeAlp = '-'.$schlCode; }

    	if(strlen($generalNumber) == 2){$generalNumber = '0'.$generalNumber; }
    	if(strlen($generalNumber) == 1){$generalNumber = '00'.$generalNumber; }
    	$registrationNumber = $lastYearNum.$vdcMu.$codeAlp.$generalNumber;
    	return $registrationNumber;
    }

    public static function RemoveDash($subject_name = 'social_study_population_education_theory'){
    	$subjectInfo = explode('_', $subject_name);
    	$subject_name = '';
    	for ($i=0; $i < sizeof($subjectInfo) ; $i++) {
    		if(!in_array($subjectInfo[$i], ['theory','practical'])) 
    			$subject_name .=' '.$subjectInfo[$i];
    	}
    	$subject_name = ltrim($subject_name, ' ');
    	return $subject_name;
    }

    public static function RemoveMinus($subject_name = 'social_study_population_education_theory'){
    	$subjectInfo = explode('-', $subject_name);
    	$subject_name = '';
    	for ($i=0; $i < sizeof($subjectInfo) ; $i++) {
    		if(!in_array($subjectInfo[$i], ['theory','practical'])) 
    			$subject_name .=' '.$subjectInfo[$i];
    	}
    	$subject_name = ltrim($subject_name, ' ');
    	return $subject_name;
    }

    /**
    *@param string subject name;
    *@return int subject id;
    */
    public static function SubjectId($subject_name = 'social_study_population_education_theory'){
    	$subject_name = UtilityFunctions::RemoveDash($subject_name);
    	$subject_array = [];
    	$SubjectList = SubjectInformation::model()->findAll();
    	if(!empty($SubjectList)){
    		foreach ($SubjectList as $subject) {
    			$subject_list_name = UtilityFunctions::seoUrl($subject->title);
		        $subjectName = UtilityFunctions::RemoveMinus($subject_list_name);
		        if($subject_name == $subjectName)
		        	return $subject->id;
    		}
    	}
    	return null;
    }

    /**
    *@param string subject name;
    *@return int fullmarks;
    */
    public static function SubjectFM($subject_name = 'social_study_population_education_theory'){
		//$subject_name = UtilityFunctions::RemoveDash($subject_name);
		//$subject_name = 'social_study_population_education_theory';
		$th_or_prac_info = explode('_', $subject_name);
		$subinfo = '';
		$general_name = '';
		$subject_post_fix = isset($th_or_prac_info[sizeof($th_or_prac_info) - 1]) ? $th_or_prac_info[sizeof($th_or_prac_info) - 1] : null;
		if($subject_post_fix && in_array($subject_post_fix, ['theory','practical'])){
			for ($i=0; $i < sizeof($th_or_prac_info) - 1 ; $i++)
				$general_name .='-'.$th_or_prac_info[$i];
			$general_name = ltrim($general_name,'-');
		}else
			$general_name = $subject_name;
		$subject_information = [];
		$PassMarkInformation = PassMark::model()->findAll();
		if(!empty($PassMarkInformation)){
			foreach ($PassMarkInformation as $fmInformation) {
			$subject = $fmInformation->subject_information ? $fmInformation->subject_information->title : '';
			$subject_seo = UtilityFunctions::seoUrl($subject);
			$general_name = UtilityFunctions::seoUrl($general_name);
	        if($general_name == $subject_seo){
	        	$subject_information['subject_nature'] = $fmInformation->subject_information ? $fmInformation->subject_information->subject_nature : 1;
	        	$subject_information['default_optional'] = $fmInformation->subject_information ? $fmInformation->subject_information->default_optional : 0;
	        	$subject_information['subject_id'] = $fmInformation->subject_id;
	        	$subject_information['is_practical'] = $fmInformation->is_practical;
	        	if(in_array('practical', $th_or_prac_info)){
	        		$subject_information['fm'] = $fmInformation->practical_full_mark;
	        	}else
	        		$subject_information['fm'] = $fmInformation->theory_full_mark;
	        }
			}
		}
		return $subject_information;
	}

	/**
	* MESSAGE API KEY
	*/
	public static function encraftApi()
    {
        $data=array();
        $data['key']="9a165c2a-8924-40ef-9fcc-f5f301d55ccf";
        $data['client_key']="DNv49j46fj";
        $data['credit-history']="http://samyamgroup.com/encraft-message/api/message/check-credit";
        $data['send-message-url']="http://samyamgroup.com/encraft-message/api/message/send-message";
        return $data;
    }

    public static function encrypt($plaintext) {
    	$password = Users::Key;
	    $method = "AES-256-CBC";
	    $key = hash('sha256', $password, true);
	    $iv = openssl_random_pseudo_bytes(16);
	    $ciphertext = openssl_encrypt($plaintext, $method, $key, OPENSSL_RAW_DATA, $iv);
	    $hash = hash_hmac('sha256', $ciphertext, $key, true);
	    return $iv . $hash . $ciphertext;
	}

	public static function decrypt($ivHashCiphertext) {
    	$password = Users::Key;
	    $method = "AES-256-CBC";
	    $iv = substr($ivHashCiphertext, 0, 16);
	    $hash = substr($ivHashCiphertext, 16, 32);
	    $ciphertext = substr($ivHashCiphertext, 48);
	    $key = hash('sha256', $password, true);
	    if (hash_hmac('sha256', $ciphertext, $key, true) !== $hash) return null;
    	return openssl_decrypt($ciphertext, $method, $key, OPENSSL_RAW_DATA, $iv);

   }

public static function encrypt_decrypt($action, $string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'This is my secret key';
    $secret_iv = 'This is my secret iv';
    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if( $action == 'decrypt' ) {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}

    public static function MunicipalityHeader(){
    	$string = "<div class='row-fluid text-center'><div style='font-size:20px; font-weight:bold;'> ".strtoupper(Yii::app()->params['municipality'])."</div><div style='font-size:15px; font-weight:bold;'> ".strtoupper(Yii::app()->params['address'])."</div><div style='font-size:13px; font-weight:bold;' >BASIC LEVEL EXAMINATION</div>
    	</div>";
    	return $string;
    }

    public static function SchoolID(){
		$user_section = Users::model()->findByPk(Yii::app()->user->id);
		if(!empty($user_section) && $user_section->superuser != 1)
			return $user_section->school_id;
		return null;
    }

    public static function NepaliNumber($string){
    	$number_converter = [1=>'१', 2=>'२', 3=>'३', 4=>'४', 5=>'५', 6=>'६', 7=>'७', 8=>'८', 9=>'९', 0=>'०'];
    	$nepali_number = '';
    	$string_information = str_split($string);
    	for ($i=0; $i < sizeof($string_information) ; $i++) {
    		$char = $string_information[$i];
    		$np_char = isset($number_converter[$char]) ? $number_converter[$char] : $char;
    		$nepali_number .= $np_char;
    	}
    	return $nepali_number;

    }

    public static function SherniNepali($sherni){
    	$sherni_array = [1=>'प्रथम',2=>'द्दितिय', 3=>'तृतिय'];
    	return isset($sherni_array[$sherni]) ? $sherni_array[$sherni] : '';
    }


    public static function MagformSchoolHeading($school_id){
    	$heding_section = "";
    	$school_information = BasicInformation::model()->find($school_id);
    	if(!empty($school_information)){

    		$heding_section .="<div>".$school_information->title."<br />".$school_information->title_nepali.'<br />सह - शिक्षालय<br />Co - Education</div>';
    	}
    	return $heding_section;

    }


    public static function PrincipleAccount($school_id){
    	$pricipleInformation = [];
    	$employee_information = EmployeeDetail::model()->find('post=:post AND status=:status AND school_id=:school_id',[':post'=>'प्र.अ.', ':status'=>1, ':school_id'=>$school_id]);
    	if(!empty($employee_information)){
    		$pricipleInformation['name'] = $employee_information->first_name_nepali.' '.$employee_information->middle_name_nepali.' '.$employee_information->last_name_nepali;
    		$pricipleInformation['number'] = $employee_information->contact_number;
    	}
    	return $pricipleInformation;
    }

    public static function AccountantInformation($school_id){
    	$accountantInformation = [];
    	$employee_information = EmployeeDetail::model()->find('level=:level AND status=:status AND school_id=:school_id',[':level'=>'लेखापाल', ':status'=>1, ':school_id'=>$school_id]);
    	if(!empty($employee_information)){
    		$accountantInformation['name'] = $employee_information->first_name_nepali.' '.$employee_information->middle_name_nepali.' '.$employee_information->last_name_nepali;
    		$accountantInformation['number'] = $employee_information->contact_number;
    	}
    	return $accountantInformation;
    }


    public static function NepaliMonth($month){
    	$month_array = ['बैशाख','जेठ','असार','श्रावण','भाद्र','आश्विन ','कार्तिक ','मंसिर ','पौष','माघ','फाल्गुण ','चैत्र'];
    	return isset($month_array[$month-1]) ? $month_array[$month-1] : '';
    }

    public static function Traimasik($traimasik){
    	if($traimasik == 1)
			return 'प्रथम चौमासिक निकाशा';
		if($traimasik == 2)
			return 'दोस्रो चौमासिक निकाशा';
		return 'त्रितिय चौमासिक निकाशा';
    }

    public static function NikasiType($nikasi){
    	if($nikasi == 'salary')
    		return 'तलब/खर्चा';
    	if($nikasi == 'cloth')
    		return "पोशक भात्ता";
    	if($nikasi == 'dasai_bonus')
    		return "चाड/पर्व बोनस";
    	return 'अन्य';
    }


}