<?php


class UtilityFunctions {

	/**
	 * Page Sizes
	 */

	public static function pageSizes() {
        $pageSizes = array( 10 => '10 per page', 20 => '20 per page', 50 => '50 per page', 100 => '100    per page', 250 => '250 per page');
        return $pageSizes;
	}


	/**
	 * Page Set And Usent
	 */

	public static function pageSetAndUnset() {
		Yii::app()->user->setState('pageSize', 10);

        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
            unset($_GET['pageSize']);
        }
	}

	public static function createrName($id)
	{

		$userInformation = User::model()->findByPk($id);
		if (!empty($userInformation))
			return $userInformation->username;
		else
			return "null";
	}


	public static function preBillPaying($reg_id, $amount_paid)
	{
		$item_id = array();
		if (isset($reg_id) && is_numeric($reg_id)) {
			$to_paid_amt = 0;
			$new_paid_amt = 0;
			$criteria = new CDbCriteria;
			$criteria->select = ' discount_amount ,amount, fee_id';
			$criteria->order = ' amount DESC';
			$criteria->condition = ' reg_id =:reg_id AND fee_status=:fee_status ';
			$criteria->params = [':reg_id' => $reg_id, ':fee_status' => 0];
			$student_fee = StudentFee::model()->findAll($criteria);
			foreach ($student_fee as $fee_info) {
				$new_amt = $fee_info->amount - $fee_info->discount_amount;
				$to_paid_amt += $new_amt;
				if ($amount_paid >= $to_paid_amt) {
					$item_id[] = $fee_info->fee_id;
					$new_paid_amt += $new_amt;
				}
			}
			$result = $amount_paid - $new_paid_amt;
		}
		return $result;
	}

	public static function studentInformation($reg_id){
		$basicInformation = array();
		$registration = Registration::model()->findByPk($reg_id);
		if (!empty($registration)){
			$basicInformation['name'] = $registration->name;
			$basicInformation['sex'] = $registration->sex;
		}
		$registration_history = RegistrationHistory::model()->findByAttributes(array('reg_id' => $reg_id, 'status' => 'active', 'current_status' => 1));
		if (!empty($registration_history)) {
			$_Section = !empty(ClassSection::model()->findByPk($registration_history->section_id)) ? ClassSection::model()->findByPk($registration_history->section_id)->title : 'Null';
			$class_id = $registration_history->class_id;
			$_class = !empty(ClassInformation::model()->findByPk($class_id)) ? ClassInformation::model()->findByPk($class_id)->title : 'Null';
			$program_id = Yii::app()->function->classInfo($class_id, 4);
			$_roll_number = $registration_history->roll_number;
			$_programInformation = !empty(ProgramDetails::model()->findByPk($program_id)) ? ProgramDetails::model()->findByPk($program_id)->title : 'Null';
			$basicInformation['program'] = $_programInformation;
			$basicInformation['class'] = $_class;
			$basicInformation['section'] = $_Section;
			$basicInformation['rollNumber'] = $_roll_number;
		}
		return $basicInformation;
	}

	public static function dateConveter($nepali_date){
		$english_date = date('Y-m-d');
		$data_information = DateInformation::model()->findByAttributes(['nepali_date'=>$nepali_date]);
		if(!empty($data_information))
			$english_date = $data_information -> english_date;
		return $english_date;
	}

	public static function AdmissionFeeRules($reg_type, $class_id){
		$rules_array = array();
		$fee_array = [];
		$charge_items = [];
		$program_information = ClassInformation::model()->findByPk($class_id);
		if (!empty($program_information)) {
			$prg_id = $program_information->prgdetails_id;
			$level_id = !empty(ProgramDetails::model()->findByPk($prg_id)) ? ProgramDetails::model()->findByPk($prg_id)->shoollevel_id : 0;
			$fee_rules = FeeSetting::model()->findAllByAttributes(array('class_id' => $class_id, 'section' => $reg_type));
			if (empty($fee_rules))
				$fee_rules = FeeSetting::model()->findAllByAttributes(array('program_id' => $prg_id, 'section' => $reg_type));
			if (empty($fee_rules))
				$fee_rules = FeeSetting::model()->findAllByAttributes(array('school_level_id' => $level_id, 'section' => $reg_type));
			if (empty($fee_rules))
				$fee_rules = FeeSetting::model()->findAllByAttributes(array('section' => $reg_type));
			if (!empty($fee_rules)) {
				foreach ($fee_rules as $rl_info) {
					$item_id = $rl_info->fee_id;
					$fee_array[] = $item_id;
					$feeAmount = FeeAmount::model()->findByAttributes(array('class_id' => $class_id, 'fee_title_id' => $item_id));
					if (empty($feeAmount))
						$feeAmount = FeeAmount::model()->findByAttributes(array('program_id' => $prg_id, 'fee_title_id' => $item_id));
					if (empty($feeAmount))
						$feeAmount = FeeAmount::model()->findByAttributes(array('school_level_id' => $level_id, 'fee_title_id' => $item_id));
					if (empty($feeAmount))
						$feeAmount = FeeAmount::model()->findByAttributes(array('fee_title_id' => $item_id));
					if (!empty($feeAmount)) {
						$charge_items[] = $feeAmount->id;
					}
				}
			}
		}
		$rules_array['fee_id'] = $fee_array;
		$rules_array['charge_id'] = $charge_items;
		return $rules_array;
	}
	public static function TotalSum($record, $index)
	{
		$total = 0;
		foreach ($record as $rc) {
			$total += $rc-> $index;
		}
		return $total;
	}


}