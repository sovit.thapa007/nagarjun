
<?php

/**
 * HelperService Class repreents the function needed to access by all
 * It contains the function general, comman
 */
class HelperService extends CApplicationComponent
{
    public function getTempRegistration($program_id)
    {
        $tem_generated_ = '';
        $program_information = Registration::find()
        ->select(['MAX(crn) AS crn, program_id'])
        ->where('program_id=:program_id', [':program_id'=>2])
        ->One();
        if(!empty($program_information)){
                $prg_id = $program_information->program_id;
                $string_num = strval($program_information->crn+1);
                $program_model = new Program;
                $program_details  = $program_model->programdetails($prg_id);
                if(!empty($program_details)){
                    $level_code = $program_details->level->level_code;
                    $category = $program_details->category->name;
                    $prg_code = $program_details->certificate_name;
                    $category_code = '';
                    $category_information = explode(' ', $category);
                    for ($i=0; $i < sizeof($category_information) ; $i++) {
                        $category_code .= isset($category_information[$i]) && !empty($category_information[$i]) ? $category_information[$i][0] : ''; 
                    }
                    $category_code = strtoupper($category_code);
                    $string_len = strlen($string_num);
                    if($string_len==0){$crn_num='00000000'.$string_num;}
                    if($string_len==1){$crn_num='0000000'.$string_num;}
                    if($string_len==2){$crn_num='000000'.$string_num;}
                    if($string_len==3){$crn_num='00000'.$string_num;}
                    if($string_len==4){$crn_num='0000'.$string_num;}
                    if($string_len==5){$crn_num='000'.$string_num;}
                    if($string_len==6){$crn_num='00'.$string_num;}
                    if($string_len==7){$crn_num='0'.$string_num;}
                    if($string_len==8){$crn_num=$string_num;}
                    $tem_number_raw = $level_code.'-'.$crn_num.'-'.$category_code.'-'.$prg_code;
                    $tem_number_raw_ =strtoupper(ltrim($tem_number_raw,'-'));
                    $tem_generated_='T-'.$tem_number_raw_;
                }
        }
        return $tem_generated_;
    }

}
