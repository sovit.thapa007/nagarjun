<?php 

Yii::import('zii.widgets.grid.CButtonColumn');


class  ButtonColumn extends CButtonColumn {
        
    public $evaluateID = false;

    public function renderDataCellContent($row, $data)
    {
        $tr=array();
        ob_start();
        foreach($this->buttons as $id=>$button) {
            if($this->evaluateID and isset($button['options']['rel']))  {
                $button['options']['rel'] = $this->evaluateExpression($button['options']['rel'], array('row'=>$row,'data'=>$data));
            } 
            if($this->evaluateID and isset($button['options']['class']))  {
                $button['options']['class'] = $this->evaluateExpression($button['options']['class'], array('row'=>$row,'data'=>$data));
            }
            if($this->evaluateID and isset($button['options']['class']))  {
                $button['options']['title'] = $this->evaluateExpression($button['options']['title'], array('row'=>$row,'data'=>$data));
            }

 
            $this->renderButton($id,$button,$row,$data);
            $tr['{'.$id.'}']=ob_get_contents();
            ob_clean();
        }
        ob_end_clean();
        echo strtr($this->template,$tr);
    }

}