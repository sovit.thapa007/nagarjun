<?php

/**
 *
 * @author TNChalise <teknarayanchalise@lftechnology.com>
 */
interface CrudService
{

	public function prepare();

	public function create();

	public function select();

	public function edit();

	public function delete();
}
