<?php

/**
 * Functions Class repreents the function needed to access by all
 * It contains the function general, comman
 */
class Functions extends CApplicationComponent
{

	/**
	 * This function will create an SEO friendly string.
	 * @param raw string
	 * @return seo friendly string
	 */
	function seoUrl($string)
	{
		//Lower case everything
		$string = strtolower($string);
		//Make alphanumeric (removes all other characters)
		$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
		//Clean up multiple dashes or whitespaces
		$string = preg_replace("/[\s-]+/", " ", $string);
		//Convert whitespaces and underscore to dash
		$string = preg_replace("/[\s_]/", "-", $string);
		return trim($string, '-');
	}

	/**
	 * This function will provide parent id according to level
	 * @param $level and $child_id
	 * @return $parent_id
	 */
	function parent_idfunction($level, $child_id, $location_level)
	{
		$i = 3;
		if ($level == $location_level)
			return $child_id;
		while ($i > $level) {
			if (!isset($parent_id))
				$parent_id = $child_id;
			$location_infromation = Location::model()->findByPK($parent_id);
			$pkid = !empty($location_infromation) ? $location_infromation->parent_id : 0;
			$parent_id = $pkid;
			$location_id = $pkid;
			$i--;
		}
		if (!isset($location_id))
			$location_id = 0;
		return $location_id;
	}

	/**
	 * This function will provide parent id according to level
	 * @param (which level location to get,current location id, current location )
	 * @return $parent_id
	 */
	function fullLocationaTitle($level, $child_id, $location_level)
	{
		$title_array = array();
		$i = Location::LOCATION_LEVEL;
		if ($level == $location_level) {
			$location_infromation = Location::model()->findByPK($child_id);
			$title = !empty($location_infromation) ? $location_infromation->title : 'Null';
			return $title;
		}
		while ($i > $level) {
			if (!isset($parent_id)) {
				$parent_id = $child_id;
				$location_infromation_1 = Location::model()->findByPK($parent_id);
				$title_array[] = !empty($location_infromation_1) ? $location_infromation_1->title : "Null";
			}
			$location_infromation = Location::model()->findByPK($parent_id);
			$pkid = !empty($location_infromation) ? $location_infromation->parent_id : 0;
			$title_array[] = !empty(Location::model()->findByPk($pkid)) ? Location::model()->findByPk($pkid)->title : "Null";
			$parent_id = $pkid;
			$i--;
		}
		$title_array_new = array_reverse($title_array, true);
		return implode(' , ', $title_array_new);
	}

	function feeRules($reg_type, $class_id)
	{
		$program_information = ClassInformation::model()->findByPk($class_id);
		$charge_items = [];
		if (!empty($program_information)) {
			$prg_id = $program_information->prgdetails_id;
			$level_id = !empty(ProgramDetails::model()->findByPk($prg_id)) ? ProgramDetails::model()->findByPk($prg_id)->shoollevel_id : 0;
			$fee_rules = FeeSetting::model()->findAllByAttributes(array('class_id' => $class_id, 'section' => $reg_type));
			if (empty($fee_rules))
				$fee_rules = FeeSetting::model()->findAllByAttributes(array('program_id' => $prg_id, 'section' => $reg_type));
			if (empty($fee_rules))
				$fee_rules = FeeSetting::model()->findAllByAttributes(array('school_level_id' => $level_id, 'section' => $reg_type));
			if (empty($fee_rules))
				$fee_rules = FeeSetting::model()->findAllByAttributes(array('section' => $reg_type));
			if (!empty($fee_rules)) {
				foreach ($fee_rules as $rl_info) {
					$item_id = $rl_info->fee_id;
					$feeAmount = FeeAmount::model()->findByAttributes(array('class_id' => $class_id, 'fee_title_id' => $item_id));
					if (empty($feeAmount))
						$feeAmount = FeeAmount::model()->findByAttributes(array('program_id' => $prg_id, 'fee_title_id' => $item_id));
					if (empty($feeAmount))
						$feeAmount = FeeAmount::model()->findByAttributes(array('school_level_id' => $level_id, 'fee_title_id' => $item_id));
					if (empty($feeAmount))
						$feeAmount = FeeAmount::model()->findByAttributes(array('fee_title_id' => $item_id));
					if (!empty($feeAmount)) {
						$charge_items[] = $feeAmount->id;
					}
				}
			}
		}
		return $charge_items;
	}


	public function convert_number($number)
	{
		if (($number < 0) || ($number > 999999999)) {
			throw new Exception("Number is out of range");
		}

		$Gn = floor($number / 1000000);  /* Millions (giga) */
		$number -= $Gn * 1000000;
		$kn = floor($number / 1000);  /* Thousands (kilo) */
		$number -= $kn * 1000;
		$Hn = floor($number / 100);   /* Hundreds (hecto) */
		$number -= $Hn * 100;
		$Dn = floor($number / 10); /* Tens (deca) */
		$n = $number % 10;   /* Ones */

		$res_ult = "";

		if ($Gn) {
			$res_ult .= $this->convert_number($Gn) . " Million";
		}


		if ($kn) {
			$res_ult .= (empty($res_ult) ? "" : " ") .
				$this->convert_number($kn) . " Thousand";
		}

		if ($Hn) {
			$res_ult .= (empty($res_ult) ? "" : " ") .
				$this->convert_number($Hn) . " Hundred";
		}

		$ones = array("", "One", "Two", "Three", "Four", "Five", "Six",
			"Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen",
			"Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen",
			"Nineteen");
		$tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty",
			"Seventy", "Eigthy", "Ninety");

		if ($Dn || $n) {
			if (!empty($res_ult)) {
				$res_ult .= " and ";
			}


			if ($Dn < 2) {
				$res_ult .= $ones[$Dn * 10 + $n];
			} else {
				$res_ult .= $tens[$Dn];


				if ($n) {
					$res_ult .= "-" . $ones[$n];
				}
			}
		}

		if (empty($res_ult)) {
			$res_ult = "zero";
		}
		return $res_ult;
	}

	public function unAuthorized()
	{
		throw new CHttpException(Constants::CODE_UNAUTHORIZED, 'You are not authorized to perform this action');
	}

	public function notFound()
	{
		throw new CHttpException(Constants::NOT_FOUND, 'The request cannot find your results');
	}

	public function invalidRequest()
	{
		throw new CHttpException(Constants::CODE_UNAUTHORIZED, 'Invalid requst');
	}

	public function nepaliDate($date)
	{
		if ($date) {
			$date = date('Y-m-d', strtotime($date));

			$exploded = explode('-', $date);
			$year = $exploded[0];
			$month = $exploded[1];
			$day = $exploded[2];
			$date = $day . ' - ' . NepaliMonth::model()->find('month_order=:month', array(':month' => $month))->title . ', ' . $year;
		} else {
			$date = 'N/A';
		}
		return $date;
	}

	public function schoolLevelByRegId($regId)
	{
		$ret = array();
		$classInfo = RegistrationHistory::model()->classByRegId($regId);
		$classInformation = $classInfo ? ClassInformation::model()->findByPk($classInfo['class_id']) : '';

		if ($classInformation) {

			$programDetails = ProgramDetails::model()->findByPk($classInformation->prgdetails_id);
			$programDetails ? : Yii::app()->function->notFound();

			$schoolLevel = SchoolLevel::model()->findByPk($programDetails->shoollevel_id);
			$schoolLevel ? : Yii::app()->function->notFound();

			$ret['classInfo'] = $classInfo;
			$ret['stream'] = $programDetails->title;
			$ret['program_type'] = $programDetails->program_type;
			$ret['duration_in_years'] = $programDetails->duration;
			$ret['school_level'] = $schoolLevel->title;
			$sectionInformation = ClassSection::model()->findByAttributes(array('class_id' => $classInfo['class_id']));
			$ret['section'] = $sectionInformation ? $sectionInformation->title : 'N/A';
		}

		return $ret;
	}

	public function getSchoolHierarchy($type, $id, $level)
	{
		$level_id = 0;
		if ($type == 'sec')
			$level_id = $this->classSection($id, $level);
		if ($type == 'cls')
			$level_id = $this->classInfo($id, $level);
		if ($type == 'prg')
			$level_id = $this->prgInformation($id, $level);
		if ($type == 'lvl')
			$level_id = $this->schoolLevel($id, $level);
		if ($type == 'category')
			$level_id = $this->schoolCatgory($id, $level);
		if ($type == 'school')
			$level_id = $this->schoolInfo($id, $level);
		return (int) $level_id;
	}

	function classSection($id, $level)
	{
		$return_id = 0;
		if ($level == 6)
			$return_id = $id;
		if ($level == 5) {
			$sectionInformation = ClassSection::model()->findByPk($id);
			if (!empty($sectionInformation))
				$return_id = $sectionInformation->class_id;
		}
		if ($level == 4) {
			$sectionInformation = ClassSection::model()->findByPk($id);
			if (!empty($sectionInformation)) {
				$classInformation = ClassInformation::model()->findByPk($sectionInformation->class_id);
				if (!empty($classInformation))
					$return_id = $classInformation->prgdetails_id;
			}
		}
		if ($level == 3) {
			$sectionInformation = ClassSection::model()->findByPk($id);
			if (!empty($sectionInformation)) {
				$classInformation = ClassInformation::model()->findByPk($sectionInformation->class_id);
				if (!empty($classInformation)) {
					$prg_id = $classInformation->prgdetails_id;
					$programInformation = ProgramDetails::model()->findByPk($prg_id);
					if (!empty($programInformation))
						$return_id = $programInformation->shoollevel_id;
				}
			}
		}
		if ($level == 2) {
			$sectionInformation = ClassSection::model()->findByPk($id);
			if (!empty($sectionInformation)) {
				$classInformation = ClassInformation::model()->findByPk($sectionInformation->class_id);
				if (!empty($classInformation)) {
					$prg_id = $classInformation->prgdetails_id;
					$programInformation = ProgramDetails::model()->findByPk($prg_id);
					if (!empty($programInformation)) {
						$level_id = $programInformation->shoollevel_id;
						$SchoolLevel = SchoolLevel::model()->findByPk($level_id);
						if (!empty($SchoolLevel))
							$return_id = $SchoolLevel->school_category_id;
					}
				}
			}
		}
		if ($level == 1) {
			$schoolInformation = BasicInformation::model()->find();
			if ($schoolInformation)
				$return_id = $schoolInformation->id;
		}
		return $return_id;
	}

	function classInfo($id, $level)
	{
		$return_id = 0;
		if ($level == 5)
			$return_id = $id;
		if ($level == 4) {
			$classInformation = ClassInformation::model()->findByPk($id);
			if (!empty($classInformation))
				$return_id = $classInformation->prgdetails_id;
		}
		if ($level == 3) {
			$classInformation = ClassInformation::model()->findByPk($id);
			if (!empty($classInformation)) {
				$prg_id = $classInformation->prgdetails_id;
				$programInformation = ProgramDetails::model()->findByPk($prg_id);
				if (!empty($programInformation))
					$return_id = $programInformation->shoollevel_id;
			}
		}
		if ($level == 2) {
			$classInformation = ClassInformation::model()->findByPk($id);
			if (!empty($classInformation)) {
				$prg_id = $classInformation->prgdetails_id;
				$programInformation = ProgramDetails::model()->findByPk($prg_id);
				if (!empty($programInformation)) {
					$level_id = $programInformation->shoollevel_id;
					$SchoolLevel = SchoolLevel::model()->findByPk($level_id);
					if (!empty($SchoolLevel))
						$return_id = $SchoolLevel->school_category_id;
				}
			}
		}
		if ($level == 1) {
			$schoolInformation = BasicInformation::model()->find();
			if ($schoolInformation)
				$return_id = $schoolInformation->id;
		}
		return $return_id;
	}

	function prgInformation($id, $level)
	{
		$return_id = 0;
		if ($level == 4)
			$return_id = $id;
		if ($level == 3) {
			$programInformation = ProgramDetails::model()->findByPk($id);
			if (!empty($programInformation))
				$return_id = $programInformation->shoollevel_id;
		}
		if ($level == 2) {
			$programInformation = ProgramDetails::model()->findByPk($id);
			if (!empty($programInformation)) {
				$level_id = $programInformation->shoollevel_id;
				$SchoolLevel = SchoolLevel::model()->findByPk($level_id);
				if (!empty($SchoolLevel))
					$return_id = $SchoolLevel->school_category_id;
			}
		}
		if ($level == 1) {
			$schoolInformation = BasicInformation::model()->find();
			if ($schoolInformation)
				$return_id = $schoolInformation->id;
		}
		return $return_id;
	}

	function schoolLevel($id, $level)
	{
		$return_id = 0;
		if ($level == 3)
			$return_id = $id;
		if ($level == 2) {
			$SchoolLevel = SchoolLevel::model()->findByPk($id);
			if (!empty($SchoolLevel))
				$return_id = $SchoolLevel->school_category_id;
		}
		if ($level == 1) {
			$schoolInformation = BasicInformation::model()->find();
			if ($schoolInformation)
				$return_id = $schoolInformation->id;
		}
		return $return_id;
	}

	function schoolCatgory($id, $level)
	{
		$return_id = 0;
		if ($level == 2)
			$return_id = $id;
		if ($level == 1) {
			$schoolInformation = BasicInformation::model()->find();
			if ($schoolInformation)
				$return_id = $schoolInformation->id;
		}
		return $return_id;
	}

	function schoolInfo($id, $level)
	{
		$return_id = 0;
		if ($level == 1)
			$return_id = $id;
		return $return_id;
	}

	public function AmountGeneral($class_id, $feeid){
		$amount = 0 ;
		$feeAmount = FeeAmount::model()->findByAttributes(array('class_id'=>$class_id, 'fee_title_id'=>$feeid));
		if(!empty($feeAmount))
			$amount = $feeAmount -> amount;
		else
		{
			$prg_id = $this->getSchoolHierarchy('cls', $class_id, 4);
			$feeAmount = FeeAmount::model()->findByAttributes(array('program_id'=>$prg_id, 'fee_title_id'=>$feeid));
			if(!empty($feeAmount))
				$amount = $feeAmount -> amount;
			else
			{
				$lvl_id = $this->getSchoolHierarchy('cls', $class_id, 3);
				$feeAmount = FeeAmount::model()->findByAttributes(array('school_level_id'=>$lvl_id, 'fee_title_id'=>$feeid));
				if(!empty($feeAmount))
					$amount = $feeAmount -> amount;
				else
				{
					$feeAmount = FeeAmount::model()->findByAttributes(array('fee_title_id'=>$feeid));
					if(!empty($feeAmount))
						$amount = $feeAmount -> amount;
				}
			}
		}
		return $amount;
	}

}
