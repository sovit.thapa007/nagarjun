<?php 

class EmailTemplate {


	public function invitationEmail($to, $subject, $message, $invitation) {

		$from 	   = Yii::app()->params->invitation['email'];
		
		$headers   = [];

		$headers[] = "MIME-Version: 1.0";
		$headers[] = "Content-type: text/html; charset=iso-8859-1";
		$headers[] = "From: {$from}";
		$headers[] = "Reply-To: {$to}";
		$headers[] = "Subject: {$subject}";
		$message   = wordwrap($this -> invitationMessage($message, $invitation, $to), 70, "\r\n");

		$message   = wordwrap($message, 70, "\r\n");
		return mail($to, $subject, $message, implode("\r\n", $headers));
	}



	/**
	 * @param integer $course
	 * @param string $email
	 */
	public function invitationMessage($message, $invitation, $email) {
		$text[] 	= '<html><body>';
		$text[] 	= $message . "<br /> <br />";
		$text[] 	= "Invitation Options: <br /> <br />";
		$text[] 	= "Accept: <a href='http://opicanada.com/scotiabank/site/response/invitation/{$invitation}/email/{$email}' >Yes</a> <br />";
		$text[] 	= "Refuse: <a href='http://opicanada.com/scotiabank/site/cancelresponse/invitation/{$invitation}/email/{$email}'>No</a>";
		$text[]  	= '</body></html>';
         
        return implode("\r\n", $text);
	}
	
	public function invitationReminderEmail($to, $subject, $message, $invitation) {
	
		$from 	   = Yii::app()->params->invitation['email'];
	
		$headers   = [];
	
		$headers[] = "MIME-Version: 1.0";
		$headers[] = "Content-type: text/html; charset=iso-8859-1";
		$headers[] = "From: {$from}";
		$headers[] = "Reply-To: {$to}";
		$headers[] = "Subject: {$subject}";
		$message   = wordwrap($this -> invitationReminderMessage($message, $invitation, $to), 70, "\r\n");
	
		$message   = wordwrap($message, 70, "\r\n");
		return mail($to, $subject, $message, implode("\r\n", $headers));
	}
	
	
	
	/**
	 * @param integer $course
	 * @param string $email
	 */
	public function invitationReminderMessage($message, $invitation, $email) {
		$text[] 	= '<html><body>';
		$text[] 	= $message . "<br /> <br />";
		$text[]  	= '</body></html>';
		 
		return implode("\r\n", $text);
	}
}