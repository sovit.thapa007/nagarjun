<?php

/**
 * Description of Enums
 * All global constants can be placed here.
 *
 * @author TNChalise <tnchalise99@gmail.com>
 * @created_on : 12 Jul, 2015, 9:25:17 PM
 * @package

 */
class Constants extends CEnumerable
{

	const DEFAULT_IMAGE = '/images/student_images/no_image.jpg';
	const CREATE = 'create';
	const EDIT = 'edit';
	const DELETE = 'delete';
	const VIEW = 'view';
	const CODE_UNAUTHORIZED = 401;
	const NOT_FOUND = 404;
	const IMAGE_PATH = '/images/student_images/';
	const ACTIVE_STATUS = 1;
	const INACTIVE_STATUS = 0;

}
