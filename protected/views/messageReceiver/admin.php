<?php
$this->breadcrumbs=array(
	'Message Receivers'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List MessageReceiver','url'=>array('index')),
array('label'=>'Create MessageReceiver','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('message-receiver-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Manage Message Receivers</h1>

<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
		&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'message-receiver-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'ip_',
		'mobile',
		'event',
		'name',
		'school_board_id',
		/*
		'message_id',
		'message',
		'message_code',
		'credit_consumed',
		'status',
		'remarks',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
		*/
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
