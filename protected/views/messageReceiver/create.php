<?php
$this->breadcrumbs=array(
	'Message Receivers'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List MessageReceiver','url'=>array('index')),
array('label'=>'Manage MessageReceiver','url'=>array('admin')),
);
?>

<h1>Create MessageReceiver</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>