<?php
$this->breadcrumbs=array(
	'Message Receivers'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List MessageReceiver','url'=>array('index')),
array('label'=>'Create MessageReceiver','url'=>array('create')),
array('label'=>'View MessageReceiver','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage MessageReceiver','url'=>array('admin')),
);
?>

<h1>Update MessageReceiver <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>