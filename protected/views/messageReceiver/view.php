<?php
$this->breadcrumbs=array(
	'Message Receivers'=>array('index'),
	$model->name,
);

$this->menu=array(
array('label'=>'List MessageReceiver','url'=>array('index')),
array('label'=>'Create MessageReceiver','url'=>array('create')),
array('label'=>'Update MessageReceiver','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete MessageReceiver','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage MessageReceiver','url'=>array('admin')),
);
?>

<h1>View MessageReceiver #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'ip_',
		'mobile',
		'event',
		'name',
		'school_board_id',
		'message_id',
		'message',
		'message_code',
		'credit_consumed',
		'status',
		'remarks',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
),
)); ?>
