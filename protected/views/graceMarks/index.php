<?php
$this->breadcrumbs=array(
	'Grace Marks',
);

$this->menu=array(
array('label'=>'Create GraceMarks','url'=>array('create')),
array('label'=>'Manage GraceMarks','url'=>array('admin')),
);
?>

<h1>Grace Marks</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
