<?php
$this->breadcrumbs=array(
	'Grace Marks'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List GraceMarks','url'=>array('index')),
array('label'=>'Create GraceMarks','url'=>array('create')),
);

?>

<h4 class="text-center">Manage Grace Marks</h4>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'grace-marks-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
		'academic_year',
		[
			'name'=>'level_id',
			'value'=>'$data->levelInformation ? $data->levelInformation->title : "Null"',
		],
		[
			'name'=>'program_id',
			'value'=>'$data->programInformation ? $data->programInformation->title : "Null"',
		],
		[
			'name'=>'class_id',
			'value'=>'$data->classInformation ? $data->classInformation->title : "Null"',
		],
		'grace_mark',
		[
			'name'=>'applied_',
			'header'=>'Applied',
			'value'=>'$data->applied_ == 0 ? "NO" : "YES"',
		],
		/*
		'status',
		'remarks',
		*/
array(
	'header'=>'Action',
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
