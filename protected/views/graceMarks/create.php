<?php
$this->breadcrumbs=array(
	'Grace Marks'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List GraceMarks','url'=>array('index')),
array('label'=>'Manage GraceMarks','url'=>array('admin')),
);
?>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>