

<div class="box alternate">
		<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
			'id'=>'grace-marks-form',
			'type' => 'horizontal',
			'enableAjaxValidation'=>false,
			'htmlOptions' => array(
				'class' => 'form-horizontal well',
			),
			'clientOptions' => array(
				'validateOnSubmit' => true,
			),
			)); ?>
		<div class="box-title"><p class="help-block">Fields with <span class="required">*</span> are required.</p></div>
		<div class="box-content nopadding">
			<div class="control-group">
				<h3 class="text-center" > Grace Mark Information :</h3>
			</div>
            <div class="row-fluid" style="margin-left: 20px;">

			<?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span5')); ?>
			
			<?php

				$status = 0;
				$classId = $model->class_id;
				$arrayLevel = array('level_id', 'program_id', 'class_id');
				echo $this->renderPartial('application.views.classInformation.class_hierarchy', array('model' => $model, 'status' => $status, 'class_id' => $classId, 'form' => $form, 'array_level' => $arrayLevel));
			?>

			<?php echo $form->textFieldRow($model,'grace_mark',array('class'=>'span5','maxlength'=>5)); ?>

			<?php if(!$model->isNewRecord)
				echo $form->dropDownListRow($model,'status',['1'=>'Active', '0'=>'In-Active'],array('class'=>'span5')); ?>

			<?php echo $form->textAreaRow($model,'remarks',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

			<div class="form-actions">
				<?php $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'submit',
					'type'=>'primary',
					'label'=>$model->isNewRecord ? 'Create' : 'Save',
					)); ?>
			</div>
			</div>

		<?php $this->endWidget(); ?>
	</div>
</div>

