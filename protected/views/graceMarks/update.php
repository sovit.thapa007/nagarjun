<?php
$this->breadcrumbs=array(
	'Grace Marks'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List GraceMarks','url'=>array('index')),
array('label'=>'Create GraceMarks','url'=>array('create')),
array('label'=>'View GraceMarks','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage GraceMarks','url'=>array('admin')),
);
?>

<h1>Update GraceMarks <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>