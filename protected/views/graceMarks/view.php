<?php
$this->breadcrumbs=array(
	'Grace Marks'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List GraceMarks','url'=>array('index')),
array('label'=>'Create GraceMarks','url'=>array('create')),
array('label'=>'Update GraceMarks','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete GraceMarks','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage GraceMarks','url'=>array('admin')),
);
?>

<h4 class="text-center">View GraceMarks #<?php echo $model->id; ?></h4>
<!-- reated_by, created_date, updated_by, updated_date -->
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'academic_year',
		[
			'name'=>'level_id',
			'value'=>$model->levelInformation ? $model->levelInformation->title : "Null",
		],
		[
			'name'=>'program_id',
			'value'=>$model->programInformation ? $model->programInformation->title : "Null",
		],
		[
			'name'=>'class_id',
			'value'=>$model->classInformation ? $model->classInformation->title : "Null",
		],
		'grace_mark',
		'status',
		'remarks',/*
		[
			'name'=>'created_by',
			'type' => 'text',
			'value'=>$model->creatorName,
		],*/
),
)); ?>
