<?php
$this->breadcrumbs = array(
	'Add New User' => array('create'),
	'Manage',
);
?>
<div class="form-horizontal well">
<H2 class="text-center"><strong>USERMANAGEMENT MANAGEMENT</strong></H2>
<table id="datatable" class="display items table" style="width:100%">
        <thead>
            <tr>
                <th>USERNAME</th>
                <th>EMAIL</th>
                <th>SCHOOL/MUN</th>
                <th>LAST VISIT</th>
                <th>Role</th>
                <th>STATUS</th>
                <th>Assign Role</th>
            </tr>
        </thead>
        <?php
        if(!empty($userInformation)){
            foreach ($userInformation as $user) {
                if($user->superuser == 0){
                ?>
                <tr>
                    <td><a href="<?= Yii::app()->baseUrl."/users/".$user->id; ?>" > <?= $user->username; ?></a></td>
                    <td><?= $user->email; ?></a></td>
                    <td>
                        <?= $user->school ? $user->school->title : Yii::app()->params['municipality']; ?>
                    </td>
                    <td><?= date("Y-m-d H:i:s", $user->lastvisit); ?></td>
                    <td><?= $user->role; ?></td>
                    <td><?= $user->status == 1 ? 'Active' : 'De-Active'; ?></td>
                    <td><a href="<?= Yii::app()->baseUrl."/rights/assignment/user/id/".$user->id; ?>" >Assign Role</a></td>
                </tr>
            <?php
            }
            }
        }
        ?>
        <tfoot>
            <tr>
                <th>USERNAME</th>
                <th>EMAIL</th>
                <th>SCHOOL/MUN</th>
                <th>LAST VISIT</th>
                <th>Role</th>
                <th>STATUS</th>
                <th>Assign Role</th>
            </tr>
        </tfoot>
    </table>
</div>

