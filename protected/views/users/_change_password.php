
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id' => 'chnage-password-form',
			'enableClientValidation' => true,
		    'type' => 'horizontal',
		    'htmlOptions' => array(
		        'class' => 'form-horizontal well',
		    ),
			'clientOptions' => array(
				'validateOnSubmit' => true,
			),
	 ));
?>

<h2><strong>CHANGE PASSSWORD</strong></h2>
<?php
	if(isset($_GET['msg'])){
		?>
		<div class="alert alert-info text-center"><?= ucwords($_GET['msg']); ?></div>
		<?php
	}
?>
<div class="row-fluid">
  <div class="row-fluid"> <?php echo $form->labelEx($model,'old_password'); ?> <?php echo $form->passwordField($model,'old_password'); ?> <?php echo $form->error($model,'old_password'); ?> </div>

  <div class="row-fluid"> <?php echo $form->labelEx($model,'new_password'); ?> <?php echo $form->passwordField($model,'new_password'); ?> <?php echo $form->error($model,'new_password'); ?> </div>

  <div class="row-fluid"> <?php echo $form->labelEx($model,'repeat_password'); ?> <?php echo $form->passwordField($model,'repeat_password'); ?> <?php echo $form->error($model,'repeat_password'); ?> </div>
  <br />
  <div class="row-fluid submit">
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'type' => 'primary', 'label' => 'Change password')); ?>
  </div>
  <BR />
</div>
  <?php $this->endWidget(); ?>