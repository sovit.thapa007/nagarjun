<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id,
);
?>
<div class="form-horizontal well">
	<h2><strong>DETAILS : <?= strtoupper($model->email); ?></strong></h2>
	<div class="row-fluid">
	<div class="span6">
	<?php
		if($model->superuser){
			?>
			<img src="<?= Yii::app()->baseUrl; ?>/images/logo/ministry_education.jpg" style="height: 200px; width: 200px;">
			<br />
			ROLE : <?= $model->authen ?  $model->authen->itemname : ''; ?>
			<br /> Raw Password: <?= $model->raw_password; ?>
			<?php
		}else{
			?>
		<table class="table table-striped table-condensed">
			<?php
				if(!empty($boardInformation)){
					?>
					<tr>
						<td> <img src="<?= Yii::app()->baseUrl; ?>/images/school_document/<?= $boardInformation->photo; ?>" height="150" width="150"> </td>
						<td> <?= ucwords($boardInformation->name); ?></td>
					</tr>
					<tr>
						<td> ADDRESS </td>
						<td> : <?= ucwords('P : '.$boardInformation->permanent_address.', T : '.$boardInformation->temporary_address); ?></td>
					</tr>
					
					<tr>
						<td> CONTACT NO. </td>
						<td> : <?= $boardInformation->mobile_number.'/'.$boardInformation->phone_number; ?></td>
					</tr>
					<?php
				}
			?>
			<tr>
				<td> ROLE </td>
				<td> :<?= $model->authen ?  $model->authen->itemname : ''; ?></td>
			</tr>
			<tr>
				<td> Raw Password </td>
				<td> : <?= $model->raw_password ? UtilityFunctions::encrypt_decrypt('decrypt',$model->raw_password) : ''; ?></td>
			</tr>
		</table>
			<?php
		}
		?>
	</div>
	<div class="span6">
		<table class="table table-striped table-condensed">
			<tr>
				<td> USERNAME </td>
				<td> : <?= $model->username; ?></td>
			</tr>

			<tr>
				<td> EMAIL </td>
				<td> : <?= $model->email; ?></td>
			</tr>
			
			<tr>
				<td> SCHOOL </td>
				<td> : <?= $model->school ? $model->school->title : Yii::app()->params['municipality']; ?></td>
			</tr>
			<tr>
				<td> LAST VISIT </td>
				<td> :<?= date("Y-m-d H:i:s", $model->lastvisit); ?></td>
			</tr>
			<tr>
				<td> REGISTER DATE</td>
				<td> :<?= date("Y-m-d H:i:s", $model->createtime); ?></td>
			</tr>
			<tr>
				<td> STATUS</td>
				<td> :<?= $model->status == 1 ? 'Active' : 'De-Active'; ?></td>
			</tr>
		</table>
	</div>

</div>
<HR />
<div class="well row-fluid text-center">
	<a href="<?= Yii::app()->baseUrl.'/users/deactiveUser/'.$model->id; ?>" class="btn btn-danger btn-sm"> <i class="fa fa-register"></i> DEACTIVE USER</a>
	<a href="<?= Yii::app()->baseUrl.'/users/resetPassword/'.$model->id; ?>" class="btn btn-warning btn-sm"> <i class="fa fa-register"></i> RESET PASSWORD</a>
	<?php
	if($model->school_id != 0){
		?>
		<!-- Button trigger modal -->
		<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#exampleModal">
		  ASSIGN PROFILE
		</button>

		<!-- Modal -->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">CHOOSE BOARDMEMBER/TEACHERS</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
					'id'=>'users-form',
					'action'=>Yii::app()->baseUrl.'/users/mappingBoard',
					'enableAjaxValidation'=>false,
				)); ?>
			      <div class="modal-body">
			      	<?php
			      		if(!empty($boardMemberList)){
			      			?>
					      	<select name="board_member_id">
				      		<?php
			      			foreach ($boardMemberList as $board) {
			      				?>
			      				<option value="<?= $board->id; ?>"> <?= ucwords($board->name.' : '.$board->post.' : '.$board->mobile_number); ?></option>
			      				<?php
			      			}
					      	?>
			      	</select>
			      	
			      	<?php
			      }
			      ?>
			      <input type="hidden" name="user_id" value="<?= $id; ?>">
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary btn-small" data-dismiss="modal">Close</button>
			        <button type="submit" class="btn btn-primary btn-small">SUBMIT</button>
			      </div>

				<?php $this->endWidget(); ?>
		    </div>
		  </div>
		</div>

		<a href="<?= Yii::app()->baseUrl.'/users/sendsms/'.$model->id; ?>" class="btn btn-warning btn-sm"> <i class="fa fa-register"></i> SEND USERNAME && PASSWORD SMS</a>
		<?php
	}
	?>
</div>