<?php
$this->breadcrumbs = array(
	'Student Subjects' => array('index'),
	$model->id => array('view', 'id' => $model->id),
	'Update',
);

$this->menu = array(
	array('label' => 'List StudentSubject', 'url' => array('index')),
	array('label' => 'Create StudentSubject', 'url' => array('create')),
	array('label' => 'View StudentSubject', 'url' => array('view', 'id' => $model->id)),
	array('label' => 'Manage StudentSubject', 'url' => array('admin')),
);
?>

<h1>Update StudentSubject <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>