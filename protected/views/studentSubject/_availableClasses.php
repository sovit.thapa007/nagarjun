<?php
$this->breadcrumbs=array(
	'Subject Modules' => Yii::app()->getModule('academic')->getBaseUrl(),
	'Classes List',
);
?>

<h3 class="text-center"> CLASS LIST</h3>
<hr />
<div class="row-fluid">
<?php
	if(!empty($classes_information))
	{
		$sn = 1; 
		foreach ($classes_information as $classInfo) {
			if($sn % 3 ==0 ){$n = $sn + 1;}
			if(isset($n) && $sn==$n)
				$style = "margin-left:0px;";
			else
				$style = '';
			$class_information = ClassSection::model()->classDetails($classInfo->id);
			$classInfor = isset($section_array[$classInfo->id]) ? $section_array[$classInfo->id] : '';
			if(!empty($classInfor))
				$section_title = ucwords( implode(' , ', $classInfor['title'] ));
			else
				$section_title = 'null';
			?>
			<div class="span4  quick-actions text-center" style=<?php echo $style; ?> >
				<a href="<?php echo Yii::app()->request->baseUrl; ?>/academic/StudentSubject/assignSubject/?class_id=<?php echo $classInfo->id; ?>" class="quick-action" >
					<h4><em><?php echo ucwords($class_information); ?></em></h4>
					<h5><em><?php echo $section_title; ?></em></h5>
				</a>
			</div>
			<?php
			$sn ++;
		}
	}
?>
</div>
