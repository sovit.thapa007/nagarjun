
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'basic-information-form',
    'type' => 'horizontal',
    'method' => 'get',
    'action' => Yii::app()->request->baseUrl.'/studentSubject/assignOptionalSubject',
    'htmlOptions' => array(
        'class' => 'form-horizontal well',
    ),
    'enableAjaxValidation' => false,
    ));
?>


<div class="row-fluid">
    <div class="span12">
            <div class="box-content nopadding">
                    <h2><strong>SEARCH FORM</strong></h2>
                <div class="row-fluid">
                <?php echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => "title")),'id','title'),array('class'=>'', 'prompt'=>'Select School', 'selected'=>1),array('class'=>'span5', 'label'=>'SCHOOL'));  ?>
                </div>
                    <div class="form-actions text-center">
                        <?php
                        $this->widget('bootstrap.widgets.TbButton', array(
                            'buttonType' => 'submit',
                            'type' => 'primary',
                            'label' => 'SEARCH',
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
<?php $this->endWidget(); ?>

<?php
if(!empty($student_list)){
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'basic-information-form',
    'type' => 'horizontal',
    'action' => Yii::app()->request->baseUrl.'/studentSubject/submitSecondaryOptional',
    'htmlOptions' => array(
        'class' => 'form-horizontal well',
    ),
    'enableAjaxValidation' => false,
    ));
?>
    <div class="row-fulid" style="overflow: auto;">
        <table class="table table-bordered table-stripped" style="border-collapse: collapse; width: 100%;" >
            <thead>
                <tr>
                    <td colspan="2" style="font-weight: bold;">Name : <?= $school_information ? ucwords($school_information->title).'-'.ucwords($school_information->ward_no.', '.$school_information->tole) : ''; ?></td>
                    <td colspan="1" style="font-weight: bold;"> Type : <?= $school_information ? $school_information->type : ''; ?></td>
                    <td colspan="1" style="font-weight: bold;">Code : <?= $school_information ? $school_information->schole_code : ''; ?></td>
                </tr>
                <tr>
                    <td colspan="4" style="font-weight: bold; text-align: center;">Resource Center : <?= $resource_center_detail ? ucwords($resource_center_detail->title. ' - '.$resource_center_detail->tole) : ''; ?></td>
                </tr>
                <tr>
                    <td style="border:2px solid black;background: #0E2A47; color: white">SN</td>
                    <td style="border:2px solid black;background: #0E2A47; color: white">Symbol No.</td>
                    <td style="border:2px solid black;background: #0E2A47; color: white">&nbsp;&nbsp;StudentName&nbsp;&nbsp;</td>
                    <td style="border:2px solid black;background: #0E2A47; color: white">CHOOSE OPTIONAL</td>
                </tr>
            </thead>
            <tbody>

            <?php
                    $sn = 1;
                    foreach ($student_list as $student) {
                        $secondary_subject = isset($student_optional_subject[$student->id]) ? $student_optional_subject[$student->id] : null;
                        $is_secondary_optional = $secondary_optional_subject &&  $secondary_optional_subject->id == $secondary_subject ? true : false;
                    ?>
                    <tr>
                        <td style="border:2px solid black;"><?= $sn; ?></td>
                        <td style="border:2px solid black;">
                            <input type="hidden" name="student_id_[]" value="<?= $student->id; ?>">
                            <input type="hidden" name="symbol_number_[]" value="<?= $student->symbol_number; ?>">
                            <?= $student->symbol_number; ?></td>
                        <td style="border:2px solid black;"><?= strtoupper($student->first_name.' '.$student->middle_name.' '.$student->last_name) ?></td>
                        <td style="border:2px solid black;">
                            <input type="radio" name="_choose_optional_secondary_<?= $student->id; ?>" value="<?= $primary_optional_subject ?  $primary_optional_subject->id : null;?>" <?php if(!$is_secondary_optional){ echo "checked='checked'";} ?>> &nbsp;&nbsp;&nbsp;&nbsp; <?= $primary_optional_subject ?  $primary_optional_subject->title : '';?><br>
                            <?php
                            if($secondary_optional_subject){
                                ?>
                                <input type="radio" name="_choose_optional_secondary_<?= $student->id; ?>" value="<?= $secondary_optional_subject ?  $secondary_optional_subject->id : null;?>" <?php if($is_secondary_optional){ echo "checked='checked'";} ?>> &nbsp;&nbsp;&nbsp;&nbsp; <?= $secondary_optional_subject ?  $secondary_optional_subject->title : '';?>
                                <?php
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                        $sn++;
                    }
            ?>
           </tbody>
        </table>
    </div>
    <input type="hidden" name="school_id" value="<?= $school_information ? $school_information->id : ''; ?>">
    <div class="form-actions text-center">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => 'SUBMIT OPTIONAL SUBJECT',
        ));
        ?>
    </div>
<?php $this->endWidget(); ?>
<?php
    }
?>