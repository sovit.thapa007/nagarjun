<?php
$this->breadcrumbs = array(
	'Academic Section ' => Yii::app()->getModule('academic')->getBaseUrl(),
	'Student Subject' => array('admin'),
	'Add Subject',
);
if(!isset($StudentInformation))
	$StudentInformation = UtilityFunctions::studentInformation($_GET['reg_number']);
?>
<hr />
<h3 class="text-center"> <?php echo isset($StudentInformation['name']) ? ucwords($StudentInformation['name']) . " ( " . $StudentInformation['sex'] . " )" : 'Null'; ?></h3>

<div class="row-fluid">
	<div class="span3">Program : <?php echo isset($StudentInformation['program']) ? ucwords($StudentInformation['program']) : 'Null'; ?></div>
	<div class="span3">Class : <?php echo isset($StudentInformation['class']) ? ucwords($StudentInformation['class']) : 'Null'; ?></div>
	<div class="span3">Section : <?php echo isset($StudentInformation['section']) ? ucwords($StudentInformation['section']) : 'Null'; ?></div>
	<div class="span3">ROll Number : <?php echo isset($StudentInformation['rollNumber']) ? ucwords($StudentInformation['rollNumber']) : 'Null'; ?></div>
</div>
<div class="row-fluid">

	<?php
	$this->widget('bootstrap.widgets.TbGridView', array(
		'id' => 'student-subject-grid',
		'type'=>'bordered',
		'dataProvider' => $model->StudentSubjectList(isset($_GET['reg_number']) ? $_GET['reg_number'] : false, false),
		'columns' => array(
			[
				'name' => 'subjectTitle',
				'value' => ' $data->subject->title',
			],
			'created_date',
			/*
			  'created_date',
			  'created_by',
			  'status',
			 */
		),
	));
	?>

</div>