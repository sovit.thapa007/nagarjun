<?php
$this->breadcrumbs = array(
	'Student Subjects' => array('index'),
	$model->id,
);

$this->menu = array(
	array('label' => 'List StudentSubject', 'url' => array('index')),
	array('label' => 'Create StudentSubject', 'url' => array('create')),
	array('label' => 'Update StudentSubject', 'url' => array('update', 'id' => $model->id)),
	array('label' => 'Delete StudentSubject', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
	array('label' => 'Manage StudentSubject', 'url' => array('admin')),
);
?>

<h1>View StudentSubject #<?php echo $model->id; ?></h1>

<?php
$this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'reg_id',
		'reg_number',
		'class_id',
		'subject_id',
		'nepali_date',
		'created_date',
		'created_by',
		'status',
	),
));
?>
