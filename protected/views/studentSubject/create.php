<?php
$this->breadcrumbs = array(
	'Student Subjects' => array('index'),
	'Create',
);

$this->menu = array(
	array('label' => 'List StudentSubject', 'url' => array('index')),
	array('label' => 'Manage StudentSubject', 'url' => array('admin')),
);
?>

<h1>Create StudentSubject</h1>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>