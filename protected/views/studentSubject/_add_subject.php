<?php
$this->breadcrumbs=array(
	'Student list',
);
?>
<hr />
<?php
	if(isset($msg_status) && !empty($msg_status)){
		?>
		<div class="alert alert-<?php echo $msg_status; ?> text-center"><?php echo $message; ?></div>
		<?php
	}
?>
<div class="row-fluid">
	<div class="span6">
	<span style="float :right; margin-right:100px;">
		<form action="#" method="POST">
	        <button class="btn btn-primary" name="student_class_wise" id="student_class_wise">Export(Excel)</button>
	    </form>
	</span>
	</div>
</div>
	<div class="row-fluid">

		<?php
		$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id' => 'basic-information-form',
			'action'=>Yii::app()->createUrl('/academic/StudentSubject/SubmitCompulsarySubject'),
			'enableAjaxValidation' => false,
			));
		?>

		<div class="form-actions">
			<?php
			$this->widget('bootstrap.widgets.TbButton', array(
				'buttonType' => 'submit',
				'type' => 'primary',
				'label' => 'Add Compulsary Subject',
			));
			?>
		</div>

		<?php $this->endWidget(); ?>

	</div>
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'basic-information-form',
	'action'=>Yii::app()->createUrl('/academic/StudentSubject/SubmitOptionalSubject'),
	'type' => 'horizontal',
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
	),
	'enableAjaxValidation' => false,
	));
?><?php
	$class_id = isset($_GET['class_id']) && is_numeric($_GET['class_id']) ? $_GET['class_id'] : 0;
	$this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'registration-history-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type'=>'bordered',
	'columns'=>array(
       [
       'header'=>'SN',
       'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
       ],
		[
			'name' => 'comp_status',
			'header'=>'Compulsary Subject',
			'type'=>'html',
			'filter' => false,
			'value' => ' StudentSubject::CheckCompulsarySubject($data->id, $data->class_id) ? "Complete" : CHtml::link("ADD SUBJECT",Yii::app()->createUrl("academic/StudentSubject/AddCompSubejct?reg_num=".$data->id))',	
		],
        [
        	'header'=>'Optional One',
   			'type'=>'raw',
   			'value' => 'StudentSubject::OptionalDropDown($data->id, $data->class_id, 3)',
        	//'value'=>'StudentSubject::model()->sectionRadioButton($data->class_id, $this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1), $data->id)',
       		'visible'=>StudentSubject::OptionalStatus($class_id, 3),
        ],
        [
        	'header'=>'Optional Two',
   			'type'=>'raw',
   			'value' => 'StudentSubject::OptionalDropDown($data->id, $data->class_id, 2)',
   			'visible'=>StudentSubject::OptionalStatus($class_id, 2),
        ]

),
)); ?>
<?php echo CHtml::hiddenField('class_id',$cls_id); ?>
<div class="form-actions">
	<?php
	$this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'type' => 'primary',
		'label' => 'Add Optional Subject',
	));
	?>
</div>

<?php $this->endWidget(); ?>

