<?php
$this->breadcrumbs = array(
	'Academic Section' => Yii::app()->getModule('academic')->getBaseUrl(),
	'Student List' => array('student'),
	'Manage',
);

$this->menu = array(
	array('label' => 'List StudentSubject', 'url' => array('index')),
	array('label' => 'Create StudentSubject', 'url' => array('create')),
);
?>

<h3 class="text-center">Manage Student Subjects</h3>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'student-subject-grid',
	'type'=>'stripped bordered',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		[
			'name' => 'reg_id',
			'value' => 'isset($data->Registration)? $data->Registration->name : "Null"',
		],
		[
			'name' => 'class_id',
			'value' => '$data->classInfo ? $data->classInfo->title :  "not-set"',
		],
		[
			'name' => 'subject_id',
			'header'=>'Subject Name',
			'value' => '$data->subject ? $data->subject->title : "not-set"',
		],
		'created_date',
		/*
		  'created_by',
		  'status',
		 */
	),
));
?>
