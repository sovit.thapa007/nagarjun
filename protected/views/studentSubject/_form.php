<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'student-subject-form',
	'enableAjaxValidation' => false,
	));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model, 'reg_id', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'reg_number', array('class' => 'span5', 'maxlength' => 20)); ?>

<?php echo $form->textFieldRow($model, 'class_id', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'subject_id', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'nepali_date', array('class' => 'span5', 'maxlength' => 20)); ?>

<?php echo $form->textFieldRow($model, 'created_date', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'created_by', array('class' => 'span5')); ?>

<?php echo $form->dropDownListRow($model, 'status', array("active" => "active", "in_active" => "in_active",), array('class' => 'input-large')); ?>

<div class="form-actions">
	<?php
	$this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'type' => 'primary',
		'label' => $model->isNewRecord ? 'Create' : 'Save',
	));
	?>
</div>

<?php $this->endWidget(); ?>
