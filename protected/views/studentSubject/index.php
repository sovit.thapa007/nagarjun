<?php
$this->breadcrumbs = array(
	'Student Subjects',
);

$this->menu = array(
	array('label' => 'Create StudentSubject', 'url' => array('create')),
	array('label' => 'Manage StudentSubject', 'url' => array('admin')),
);
?>

<h1>Student Subjects</h1>

<?php
$this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider' => $dataProvider,
	'itemView' => '_view',
));
?>
