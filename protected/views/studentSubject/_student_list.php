<?php
$this->breadcrumbs=array(
	'Manage',
);

?>
<div class="form-horizontal well form-horizontal">
		<h2 class="text-center">STUDENT LIST</h2>
		<?php if(Yii::app()->user->hasFlash('discount_success')):?>
			<div class="alert alert-success text-center">
				<?php echo Yii::app()->user->getFlash('discount_success'); ?>
			</div>
		  <?php endif; ?>

		<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'registration-history-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'type' => 'bordered',
		'columns'=>array(

		       [
		       'header'=>'SN',
		       'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
		       ],
				[
					'header'=>'Student Name',
					'value'=>' ucwords($data->first_name." ".$data->middle_name." ".$data->last_name)',

				],
				[
					'header'=>'Class Section',
					'name'=>'section',
					'value'=>'isset($data->classSection)? $data->classSection->title: "not set"',

				],
				'roll_number',
				[
					'header'=>'Action',
					'type'=>'raw',
					'value' => 'CHtml::link("Add Subject", array("StudentSubject/addSubject/?reg_number=".$data->id))." / ". CHtml::link("View Details",array("StudentSubject/ViewSubject/?reg_number=".$data->id))',
				],
				//'nepali_year',
				/*
				'reg_type',
				'status',
				'current_status',
				'reg_slug',
				'nepali_date',
				'created_date',
				'created_by',
				*/
		),
		)); ?>
	</div>
