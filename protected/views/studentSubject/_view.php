<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reg_id')); ?>:</b>
	<?php echo CHtml::encode($data->reg_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reg_number')); ?>:</b>
	<?php echo CHtml::encode($data->reg_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('class_id')); ?>:</b>
	<?php echo CHtml::encode($data->class_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subject_id')); ?>:</b>
	<?php echo CHtml::encode($data->subject_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nepali_date')); ?>:</b>
	<?php echo CHtml::encode($data->nepali_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<?php /*
	  <b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	  <?php echo CHtml::encode($data->created_by); ?>
	  <br />

	  <b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	  <?php echo CHtml::encode($data->status); ?>
	  <br />

	 */ ?>

</div>