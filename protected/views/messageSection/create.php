<?php
$this->breadcrumbs=array(
	'Message Sections'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List MessageSection','url'=>array('index')),
array('label'=>'Manage MessageSection','url'=>array('admin')),
);
?>

<h1>Create MessageSection</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>