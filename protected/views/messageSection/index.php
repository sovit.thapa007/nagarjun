<?php
$this->breadcrumbs=array(
	'Message Sections',
);

$this->menu=array(
array('label'=>'Create MessageSection','url'=>array('create')),
array('label'=>'Manage MessageSection','url'=>array('admin')),
);
?>

<h1>Message Sections</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
