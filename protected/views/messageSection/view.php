<?php
$this->breadcrumbs=array(
	'Message Sections'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List MessageSection','url'=>array('index')),
array('label'=>'Create MessageSection','url'=>array('create')),
array('label'=>'Update MessageSection','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete MessageSection','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage MessageSection','url'=>array('admin')),
);
?>

<h1>View MessageSection #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'event',
		'message',
		'created_at',
		'created_by',
		'updated_by',
		'updated_at',
),
)); ?>
