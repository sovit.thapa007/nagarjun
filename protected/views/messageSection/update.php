<?php
$this->breadcrumbs=array(
	'Message Sections'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List MessageSection','url'=>array('index')),
array('label'=>'Create MessageSection','url'=>array('create')),
array('label'=>'View MessageSection','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage MessageSection','url'=>array('admin')),
);
?>

<h1>Update MessageSection <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>