
    <div class="form-horizontal well">
        <h2><STRONG>GRADE WISE STUDENTS NUMBER RESPECT TO SCHOOL</STRONG></h2>
        <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
            'type' => 'horizontal',
        )); ?>
        <div class="row-fluid">
            <div class="span6">
                <?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span12')); ?></div>
            <div class="span6">
            <?php 
            if(UtilityFunctions::ShowSchool()){
                echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(),'id','title'),array('class'=>'span12', 'multiple' => true, 'selected' => 'selected')); 
            }
            ?>
            </div>
        </div>
    <div class="row-fluid">
            <?php 
            echo $form->dropDownListRow($model,'result_',['general'=>'General', 'pdf'=>'PDF DOWNLOAD'],array('class'=>'span12', 'selected'=>1)); 
            ?> 
    </div>
        <div class="form-actions text-center">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type'=>'primary',
                'label'=>'Search Report',
            )); ?>
        </div>
        </div>

        <?php $this->endWidget(); ?>



<?php
    if(!empty($reportData)){
        $arrayWiseDataSet = isset($reportData['arrayWiseDataSet']) ? $reportData['arrayWiseDataSet'] : [];
        $school_id_array_data = isset($reportData['school_id_array_data']) ? $reportData['school_id_array_data'] : [];
        $schoolArray = isset($reportData['schoolArray']) ? $reportData['schoolArray'] : [];
        $gradeLetter = isset($reportData['gradeLetter']) ? $reportData['gradeLetter'] : [];
        ?>
        <div class="form-horizontal well form-horizontal" style="width: 100; overflow: auto;">
        <div style='text-align: center; font-weight: bold;'><?= strtoupper(UtilityFunctions::MunicipalLedgerHeader($academic_year)); ?></div>
            <table id="_school_grade_wise_student_number" class="table table-bordered table-striped">
                <thead>
                    <th>SN</th>
                    <th>Type</th>
                    <th>School</th>
                    <th>School Code</th>
                    <?php
                    for ($i=0; $i < sizeof($gradeLetter) ; $i++) { 
                        ?>
                        <th><?= isset($gradeLetter[$i]) ? UtilityFunctions::GPARange($gradeLetter[$i]) : ''; ?></th>
                        <?php
                    }
                    ?>
                    <th>Total</th>
                </thead>
            <?php
            for ($j=0; $j < sizeof($school_id_array_data) ; $j++){
                $school_id = $school_id_array_data[$j];
                $school_information = isset($schoolArray[$school_id]) ? $schoolArray[$school_id] : null;
                $school_info = explode(':', $school_information);
                ?>
                <tr>
                    <td><?= $j+1; ?></td>
                    <td><?= isset($school_info[2]) ? strtoupper($school_info[2]) : ''; ?></td>
                    <td><?= isset($school_info[0]) ? strtoupper($school_info[0]) : ''; ?></td>
                    <td><?= isset($school_info[1]) ? $school_info[1] : ''; ?></td>
                    <?php
                    $total_student_number = 0;
                    for ($k=0; $k < sizeof($gradeLetter) ; $k++) { 
                        $grade = $gradeLetter[$k];
                        $studentNumber = isset($arrayWiseDataSet[$school_id][$grade]) ? $arrayWiseDataSet[$school_id][$grade] : 0;
                        $total_student_number += $studentNumber;
                        ?>
                        <td>
                            <?= $studentNumber; ?>
                        </td>
                        <?php
                    }
                    ?>
                    <th><?= $total_student_number; ?></th>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>
        
<script type="text/javascript">
    
    $(document).ready(function () {
        $('#_school_grade_wise_student_number').DataTable( {
            lengthMenu: [
                [ 25, 30, 50, -1 ],
                [ '25 rows', '30 rows', '50 rows', 'Show all' ]
            ],
            dom: 'Bfrtip',
            buttons: [
                'pageLength','copy', 'csv', 'excel', 'pdf', 'print'
            ],
            //"order": [[ 6, 'asc' ]]
        } );


    });

</script>
        <?php
    }
    
?>
