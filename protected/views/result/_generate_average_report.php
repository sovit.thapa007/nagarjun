<div class="form-horizontal well">
    <h2><STRONG>GENERATE SCHOOL WISE AVERAGE REPORT</STRONG></h2>
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'post',
        'type' => 'horizontal',
        'htmlOptions' => array('target'=>'_blank',
            'class' => 'form-horizontal well'
        )
    )); ?>
    <br />
        <?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span12')); ?>
    <div class="form-actions text-center">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'=>'primary',
            'label'=>'Search Report',
        )); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>
