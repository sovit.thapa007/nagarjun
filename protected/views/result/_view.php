<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('school_id')); ?>:</b>
	<?php echo CHtml::encode($data->school_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('academic_year')); ?>:</b>
	<?php echo CHtml::encode($data->academic_year); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('student_id')); ?>:</b>
	<?php echo CHtml::encode($data->student_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('section')); ?>:</b>
	<?php echo CHtml::encode($data->section); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('result_type')); ?>:</b>
	<?php echo CHtml::encode($data->result_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('terminal_id')); ?>:</b>
	<?php echo CHtml::encode($data->terminal_id); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('terminal_')); ?>:</b>
	<?php echo CHtml::encode($data->terminal_); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_obtained_mark')); ?>:</b>
	<?php echo CHtml::encode($data->total_obtained_mark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_full_mark')); ?>:</b>
	<?php echo CHtml::encode($data->total_full_mark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('percentage')); ?>:</b>
	<?php echo CHtml::encode($data->percentage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('division')); ?>:</b>
	<?php echo CHtml::encode($data->division); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('grade')); ?>:</b>
	<?php echo CHtml::encode($data->grade); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('attendance')); ?>:</b>
	<?php echo CHtml::encode($data->attendance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_attendance')); ?>:</b>
	<?php echo CHtml::encode($data->total_attendance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rank')); ?>:</b>
	<?php echo CHtml::encode($data->rank); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('result_status')); ?>:</b>
	<?php echo CHtml::encode($data->result_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('result_publish_date')); ?>:</b>
	<?php echo CHtml::encode($data->result_publish_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('number_of_subject')); ?>:</b>
	<?php echo CHtml::encode($data->number_of_subject); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('approved_by')); ?>:</b>
	<?php echo CHtml::encode($data->approved_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('approved_date')); ?>:</b>
	<?php echo CHtml::encode($data->approved_date); ?>
	<br />

	*/ ?>

</div>