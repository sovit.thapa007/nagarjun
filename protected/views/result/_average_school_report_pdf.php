
<div class="row" style="text-align: center; font-weight: bold;"><?= strtoupper(UtilityFunctions::MunicipalLedgerHeader($academic_year)); ?><br />SCHOOL WISE AVERAGE</div>
<table id="datatable" class="display items table" style="width:100%;border-collapse: collapse; width: 100%;">
        <thead>
            <th style="border: 2px solid black; padding: 10px;">SN</th>
            <th style="border: 2px solid black; padding: 10px;">TYPE</th>
            <th style="border: 2px solid black; padding: 10px;">SCHOOL NAME</th>
            <th style="border: 2px solid black; padding: 10px;">CODE</th>
            <th style="border: 2px solid black; padding: 10px;">GRADE POINT</th>
        </thead>
        <tbody>
        <?php
            for ($i=0; $i < sizeof($averageReportData) ; $i++) {
                ?>
                <tr>
                    <td style="border: 2px solid black; padding: 10px;"><?= isset($averageReportData[$i][0]) ? $averageReportData[$i][0] : $i+1; ?></td>
                    <td style="border: 2px solid black; padding: 10px;"><?= isset($averageReportData[$i][1]) ? strtoupper($averageReportData[$i][1]) : ''; ?></td>
                    <td style="border: 2px solid black; padding: 10px;"><?= isset($averageReportData[$i][2]) ? strtoupper($averageReportData[$i][2]) : ''; ?></td>
                    <td style="border: 2px solid black; padding: 10px;"><?= isset($averageReportData[$i][3]) ? $averageReportData[$i][3] : ''; ?></td>
                    <td style="border: 2px solid black; padding: 10px;"><?= isset($averageReportData[$i][4]) ? $averageReportData[$i][4] : ''; ?></td>
                </tr>
                <?php
            }
        ?></tbody>

        <tfoot>
            <th style="border: 2px solid black; padding: 10px;">SN</th>
            <th style="border: 2px solid black; padding: 10px;">TYPE</th>
            <th style="border: 2px solid black; padding: 10px;">SCHOOL NAME</th>
            <th style="border: 2px solid black; padding: 10px;">CODE</th>
            <th style="border: 2px solid black; padding: 10px;">GRADE POINT</th>
        </tfoot>
    </table>
</div>