<style type="text/css">
    @media print
    {    
        td, .blank_section,
        {
            height: 20px !important;
        }
    }
</style>
<?php
    if(isset($resultInformation) && !empty($resultInformation)){
        /*new code will start from here*/
        $sn=0;
        foreach ($resultInformation as $information) {
            ?>
            <div style="text-align:center;">
            <div style="width: 19% !important;height: 11em; float: left;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo/ministry_education.jpg" style="float: left;width: 130px; height: 200px;"></div>
            <div style="width: 78% !important;float: right;">
            <h3 class="text-center"> GOVERMENT OF NEPAL</h3><h3 class="text-center"> MINISTRY OF EDUCATION</h3><h3 class="text-center"> DISTRICT EDUCATION OFFICE, XYZ</h3>
            <h4 class="text-center"> DISTRICT LEVEL EXAMINATION, EIGHT <br /> MARK - SHEET</h4>
            </div>
            </div>
            <br />
               <div style="font-size: 13px; float: left; width: 15em; height: 2em;"> THE RESULT(S) SECURED BY </div><div style="font-size: 13px; float: right; border-bottom: thin dotted #000; width: 35.5em;height: 2em;"> &nbsp; &nbsp; &nbsp; <?php echo strtoupper($information->first_name." ".$information->middle_name." ".$information->last_name); ?></div>

               <div style="font-size: 13px; float: left; width: 8em;height: 2em;"> DATE OF BIRTH </div><div style="font-size: 13px; float: right; border-bottom: thin dotted #000; width: 42.5em;height: 2em;"> &nbsp;&nbsp;&nbsp; <?= $information->dob_nepali; ?> (BS) &nbsp;&nbsp;&nbsp; <?= $information->dob_nepali; ?> (AD) </div>
               <div>
               <span style="font-size: 13px; float: left;height: 2em;"> ROLL NO. </span><span style="font-size: 13px; float: right; border-bottom: thin dotted #000; width: 20em !important;height: 2em;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?= $information->roll_number; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
               <span style="font-size: 13px; float: left;height: 2em;"> SYMBOL NO. </span><span style="font-size: 13px; float: right; border-bottom: thin dotted #000; width: 10em;height: 2em;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?= $information->symbol_number; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
               </div>
               <div style="font-size: 13px; float: left; width:5em; height: 2em;"> OF </div><div style="font-size: 13px; float: right; border-bottom: thin dotted #000; width: 45.5em;height: 2em;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo isset($information->school_sec) ? strtoupper($information->school_sec->title) :  'Null'; ?></div>

               <div style="font-size: 13px; float: left; width:30em; height: 2em;"> IN THE ANNUAL EIGHT CLASS EXAMINATION OF </div><div style="font-size: 13px; float: left; border-bottom: thin dotted #000; width: 8.5em;height: 2em;"> &nbsp;&nbsp;&nbsp;&nbsp;<?php echo $information->academic_year." ( BS )"; ?></div>ARE GIVEN BELOW
        <br/><br/>
        <?php 
                if($cas_percent !=0 && $is_cas)
                    $title =" Terminal & CAS Evaluation <br /> ( Terminal : ".(100-$cas_percent)." % & CAS : ".$cas_percent." % )";
                else
                    $title = "Terminal Evaluation";
                $pf_col_span = $is_practical ? 2 : 0;
                $first_line_row_span = $is_cas || $is_practical ? 2 : 0;
                $mark_obt_col = 0;
                if($is_practical && $is_cas)
                    $mark_obt_col = 4;
                if(!$is_cas && $is_practical)
                    $mark_obt_col = 2;
                if($is_cas && !$is_practical)
                    $mark_obt_col = 2;
                $p_f_row_span = $is_cas && !$is_practical ? 2:0;
                $grnd_total_row_span = $pf_col_span+$pf_col_span+$mark_obt_col;
            ?>


                <table style="border-collapse: collapse;">
                      <tr>
                        <td align="center" rowspan="<?= $first_line_row_span;?>" style="width: 5% !important;border:1px solid black;">S&nbsp;N <br />U&nbsp;U <br />B&nbsp;M <br />J&nbsp;B <br />E&nbsp;E <br />C&nbsp;R <br />T&nbsp;S <br /></td>

                        <td align="center" rowspan="<?= $first_line_row_span;?>" style="width:45%!important; border:1px solid black;">SUBJECTS</td>

                        <td align="center" rowspan="<?= $p_f_row_span;?>" colspan="<?= $pf_col_span; ?>" style="width: 8% !important;border:1px solid black;">Full Marks</td>
                        <td align="center" rowspan="<?= $p_f_row_span;?>" colspan="<?= $pf_col_span; ?>" style="width: 8% !important;border:1px solid black;">Pass Marks.</td>
                        <td align="center" colspan="<?= $mark_obt_col; ?>" style="width:10%!important;border:1px solid black; text-align:center">OBTAINED MARKS</td>
                    <td align="center" rowspan="<?= $first_line_row_span;?>" style="width:6%!important;border:1px solid black;">TOTAL</td>
                        <td align="center" rowspan="<?= $first_line_row_span;?>" style="width: 8% !important;border:1px solid black;">G&nbsp;M<br />R&nbsp;A<br />A&nbsp;R<br />C&nbsp;K<br />E&nbsp;S</td>
                        <td align="center" rowspan="<?= $first_line_row_span;?>" style="width: 7% !important;border:1px solid black;">R<br />E<br />M<br /> A <br /> R <br /> K <br /> S
                        </td>
                    </tr>
                    <tr>
                        <?php
                            if($is_practical){
                                ?>
                                <td align="center" style="width: 4% !important;border:1px solid black;">Th.</td>
                                <td align="center" style="width: 4% !important;border:1px solid black;">Pr.</td>
                                
                                <td align="center" style="width: 4% !important;border:1px solid black;">Th.</td>
                                <td align="center" style="width: 4% !important;border:1px solid black;">Pr.</td>
                                <?php
                            }
                            if($is_practical && $is_cas){
                                ?>
                                <td align="center" style="width: 5% !important;border:1px solid black;">Th.</td>
                                <td align="center" style="width: 5% !important;border:1px solid black;">Pr.</td>
                                <td align="center" style="width: 5% !important;border:1px solid black;">Terminal Total.</td>
                                <td align="center" style="width: 5% !important;border:1px solid black;">CAS.</td>
                                <?php
                            }
                            if($is_practical && !$is_cas){
                                ?>
                                <td align="center" style="width: 5% !important;border:1px solid black;">Th.</td>
                                <td align="center" style="width: 5% !important;border:1px solid black;">Pr.</td>
                                <?php
                            }
                            if(!$is_practical && $is_cas){
                                ?>
                                <td align="center" style="width: 10% !important;border:1px solid black;">Terminal</td>
                                <td align="center" style="width: 10% !important;border:1px solid black;">CAS</td>
                                <?php
                            }
                        ?>
                    </tr>
                    <?php
                    $marks_information = $information->marks;
                    $th_pass_total = $th_full_total = $pr_pass_total = $pr_full_total = $theory_marks_total = $practical_marks_total = $cas_marks_total = $teminal_marks_total = $total_obtained = $maximum_grand_total = $total_combine_full_mark = 0;
                    $marks_information = $information->marks;
                    $th_pass_total = $th_full_total = $pr_pass_total = $pr_full_total = $theory_marks_total = $practical_marks_total = $cas_marks_total = $teminal_marks_total = $total_obtained = 0;
                    if(!empty($marks_information)){
                        $sn=1;
                        foreach ($marks_information as $mark_info) {
                            $practical_section = $mark_info->is_practical;
                            $terminal_percent = $mark_info -> terminal_percent;
                            $cas_percent = $mark_info -> cas_percent;
                            $theory_pass_mark  = $mark_info -> theory_pass_mark * $terminal_percent / 100;
                            $th_pass_total += $theory_pass_mark;
                            $practical_pass_mark  = $mark_info -> practical_pass_mark * $terminal_percent / 100;
                            $pr_pass_total += $practical_pass_mark;
                            $theory_full_mark  = $mark_info -> theory_full_mark * $terminal_percent / 100;
                            $th_full_total += $theory_full_mark;
                            $practical_full_mark  = $mark_info -> practical_full_mark * $terminal_percent / 100;
                            $pr_full_total += $practical_full_mark;

                            $combine_pass_mark = $theory_pass_mark + $practical_pass_mark;
                            $theory_mark_raw = $mark_info -> theory_mark * $terminal_percent/100;
                            $theory_mark = $theory_pass_mark <= $theory_mark_raw ? $theory_mark_raw : $theory_mark_raw."*";
                            $theory_marks_total += $theory_mark_raw;
                            $practical_mark_raw = isset($mark_info -> practical_mark) && $mark_info -> practical_mark !=0 ? $mark_info -> practical_mark * $terminal_percent /100 : 0;
                            $practical_mark = $practical_pass_mark <= $practical_mark_raw ? $practical_mark_raw : $practical_mark_raw."*";
                            $practical_marks_total += $practical_mark_raw;
                            $cas_mark_raw = $mark_info -> cas_mark * $cas_percent /100;
                            $cas_marks_total += $cas_mark_raw;
                            $teminal_marks_total += ($theory_mark_raw+$practical_mark_raw);
                            $total_mark = $mark_info->total_mark; 
                            $total_obtained += $total_mark;
                            ?>
                            <tr>
                            <td style="border-left:1px solid black;"><?= "0".$sn++; ?></td>
                            <td style="border-left:1px solid black;"><?= $mark_info->subject_name; ?></td>
                            <?php
                                if($is_practical){

                                    ?>
                                    <td align="center" style="border-left:1px solid black;"><?= $theory_full_mark; ?></td>
                                    <td align="center" style="border-left:1px solid black;"><?php if($practical_section && $practical_section !=0){ echo $practical_full_mark; } ?></td>
                                    <td align="center" style="border-left:1px solid black;"><?= $theory_pass_mark; ?></td>
                                    <td align="center" style="border-left:1px solid black;"><?php if($practical_section && $practical_section !=0){echo $practical_pass_mark; }?></td>
                                    <?php
                                }else{
                                    ?>
                                    <td align="center" style="border-left:1px solid black;"><?= $theory_full_mark; ?></td>
                                    <td align="center" style="border-left:1px solid black;"><?= $theory_pass_mark; ?></td>
                                    <?php

                                }
                            if($is_practical && $is_cas){

                                ?>
                                <td align="center" style="border-left:1px solid black;"><?= $theory_mark != 0 ? $theory_mark : 'Abs<sup>*</sup>' ; ?></td>
                                <?php if($practical_section && $practical_section !=0){
                                    ?>
                                <td align="center" style="border-left:1px solid black;"><?php if($practical_section && $practical_section !=0){ echo $practical_mark != 0 ? $practical_mark : 'Abs<sup>*</sup>' ; } ?></td>
                                <?php
                                    }
                                ?>
                                <td align="center" style="border-left:1px solid black;"><?= $practical_mark+$theory_mark != 0 ? $practical_mark+$theory_mark : 'Abs<sup>*</sup>' ;?></td>
                                <td align="center" style="border-left:1px solid black;"><?= $mark_info->cas != 0 ? $mark_info->cas : 'Abs<sup>*</sup>' ; ?></td>
                                <td align="center" style="border-left:1px solid black;"><?= $total_mark != 0 ? $total_mark : 'Abs<sup>*</sup>' ;?></td>
                                <?php
                            }
                            if($is_practical && !$is_cas){
                                ?>
                                <td align="center" style="border-left:1px solid black;"><?= $theory_mark != 0 ? $theory_mark : 'Abs<sup>*</sup>' ; ?></td>
                                <td align="center" style="border-left:1px solid black;"><?php if($practical_section && $practical_section !=0){
                                    echo $practical_mark != 0 ? $practical_mark : 'Abs<sup>*</sup>' ;
                                    } ?></td>
                                <td align="center" style="border-left:1px solid black;"><?= $total_mark != 0 ? $total_mark : 'Abs<sup>*</sup>' ; ?></td>
                                <?php
                            }
                            if(!$is_practical && $is_cas){
                                ?>
                                <td align="center" style="border-left:1px solid black;"><?= $theory_mark != 0 ? $theory_mark : 'Abs<sup>*</sup>' ; ?></td>
                                <td align="center" style="border-left:1px solid black;"><<?= $mark_info->cas != 0 ? $mark_info->cas : 'Abs<sup>*</sup>' ; ?></td>
                                <td align="center" style="border-left:1px solid black;"><?= $total_mark != 0 ? $total_mark : 'Abs<sup>*</sup>' ; ?></td>
                                <?php
                            }
                            if(!$is_practical && !$is_cas){
                                ?>
                                <td align="center" style="border-left:1px solid black;"><?= $total_mark != 0 ? $total_mark : 'Abs<sup>*</sup>' ; ?></td>
                                <?php
                            }
                            ?>
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                        <td class="blank_section" style="border-left: 1px solid black;border-right: 1px solid black;"></td>
                            </tr>
                            <?php
                        }
                    }
                    for ($i=$sn; $i <15 ; $i++) { 
                        ?>
                        <tr >
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                        <?php
                            if($is_practical){
                                ?>
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                                
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                                <?php
                            }else{
                                ?>
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                                <?php

                            }
                            if($is_practical && $is_cas){
                                ?>
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                                <?php
                            }
                            if($is_practical && !$is_cas){
                                ?>
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                                <?php
                            }
                            if(!$is_practical && $is_cas){
                                ?>
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                                <?php
                            }
                            if(!$is_practical && !$is_cas){
                                ?>
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                                <?php
                            }
                        ?>
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                        <td class="blank_section" style="border-left: 1px solid black;"></td>
                    </tr>
                        <?php
                    }
                    $division = '';
                    $result_information_section = $information->Result;

                    if(!empty($result_information_section)){
                        foreach ($result_information_section as $result_information)
                            $division = $result_information->division;
                    }
                    ?>


                        <tr>
                        <td colspan="2" style="border:1px solid black;">
                            <table style="width:100% !important;">
                            <tr>
                            <td style="border-left:1px solid black;">FM</td>
                            <td style="border-left:1px solid black;">DIST</td>
                            <td style="border-left:1px solid black;">1<sup>st</sup> DIV</td>
                            <td style="border-left:1px solid black;">2<sup> nd</sup> DIV</td>
                            <td style="border-left:1px solid black;">3<sup>rd</sup> DIV</td>
                            </tr>
                            <tr >
                            <?php
                            $total_combine_full_mark_s = $th_full_total+$pr_full_total;
                            ?>
                            <td style="border-left:1px solid black;"><?= ceil($total_combine_full_mark_s); ?></td>
                            <td style="border-left:1px solid black;"><?= ceil($total_combine_full_mark_s*80/100); ?></td>
                            <td style="border-left:1px solid black;"><?= ceil($total_combine_full_mark_s*60/100); ?></td>
                            <td style="border-left:1px solid black;"><?= ceil($total_combine_full_mark_s*45/100); ?></td>
                            <td style="border-left:1px solid black;"><?= ceil($total_combine_full_mark_s*40/100); ?></td>
                            </tr>
                            </table>
                        </td>
                        <?php
                            $main_section = 62;
                            $sub_section = 100 - ceil($main_section);
                        ?>
                        <td colspan="<?= $grnd_total_row_span+3; ?>" style="border:1px solid black; ">
                            <table style="width:100% !important;">
                            <tr>
                                <td style="width: <?= ceil($main_section); ?>% !important;">Grand Total => </td><td style="width: <?= ceil($sub_section); ?>% !important;"><?= $total_obtained; ?></td>
                            </tr>
                            <tr>
                                <td style="width: <?= ceil($main_section); ?>% !important;"></td><td style="width: <?= ceil($sub_section); ?>% !important;"><?= $result_information->percentage."%"; ?></td>
                            </tr>
                            <tr>
                                <td style="width: <?= ceil($main_section); ?>% !important;">Result => </td><td style="width: <?= ceil($sub_section); ?>% !important;"><?= $division;?> </td>
                            </tr>

                            </table>
                        </td>

                        </tr>
                </table>
                


                <div>
                <ol start="1">
                <li> # Means Subject(S) appeared in the suplementary examination</li>
                <li> F and an astrisk (*). Fail</li>
                <li> A- Absent</li>
                <li> W- Result Withheld</li>
                <li> C- Subject Cancelled</li>
                <li> CP-Comp.pr.mark.
                </ol>
                </div>
                <table style="width: 100% !important;">
                    <tr>
                        <td style="width: 50% !important;"><small>CHECKED BY :- </small></td>
                        <td style="width: 50% !important;"></td>
                    </tr>
                    <tr>
                        <td style="width: 50% !important;"><strong>DISTRICT EDUCATION OFFICE, ABC</strong></td>
                        <td style="width: 50% !important;"></td>
                    </tr>
                    <tr>
                        <td style="width: 50% !important;"><small>DATE OF ISSUE</small></td>
                        <td style="width: 50% !important;"></td>
                    </tr>
                    <tr>
                        <td style="width: 50% !important;"></td>
                        <td style="width: 50% !important;border-top:1px solid;"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OFFICER</strong></td>
                    </tr>
                </table>
        </div>
        <?php
        }
    }

?>
<script type="text/javascript">
    $(function() {
    var header_height = 0;
    $('table th span').each(function() {
        if ($(this).outerWidth() > header_height) header_height = $(this).outerWidth();
    });

    $('table th').height(header_height);
});
</script>


