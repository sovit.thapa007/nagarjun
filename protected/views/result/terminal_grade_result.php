<?php
/**
 * Created by PhpStorm.
 * User: Sovit Thapa
 * Date: 7/28/15
 * Time: 12:03 PM
 */
//$this->renderPartial('index');
?>
<style type="text/css">
    @media print
    {   
        .printing_div {
            page-break-inside: avoid;
        }
    }
</style>
<?php
    if(isset($resultInformation) && !empty($resultInformation)){
        /*new code will start from here*/
        $sn=0;
        foreach ($resultInformation as $information) {
            ?>
            <div id="printing_div" style="height: 100em;">
            <div>
            <div style="width: 19% !important;height: 11em; float: left;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo/ministry_education.jpg" style="float: left;width: 130px; height: 200px;"> 
            </div>
            <div style="width: 78% !important;float: right; text-align: center;">
                <div style='font-size:20px; font-weight:bold;'> <?= strtoupper(Yii::app()->params['municipality']);?></div>
                <div style='font-size:15px; '><?= strtoupper(Yii::app()->params['result_address']);?></div>
                <div style='font-size:18px; font-weight:bold;'>BASIC LEVEL EXAMINATION <br /><?= $academic_year;?></div>
                <div style="background-color:blue">GRADE SHEET</div>
            </div>
            </div>
            SYMBOL NO. : <?= $information->symbol_number ; ?>
            <br /><br /><br />
               <div style="font-size: 15px; float: left; width: 18em; height: 2em;"> THIS IS TO CERTIFY THAT MR./MISS </div><div style="font-size: 15px; float: right; border-bottom: thin dotted #000; width: 25em;height: 2em;"> <?php echo strtoupper($information->first_name." ".$information->middle_name." ".$information->last_name); ?></div>

               <div style="font-size: 15px; float: left; width: 8em;height: 2em;"> STUDENT OF </div><div style="font-size: 15px; float: right; border-bottom: thin dotted #000;height: 2em;"> <?= $schoolInformaiton ? $schoolInformaiton->title : ''; ?></div>
               <div style="font-size: 15px; float: left; height: 2em; padding-top: 10px;">
                HAS APPEARED IN THE ANNUAL EXAMINATION OF <?= $year ; ?> COMPLETED </div>
                <div style="font-size: 15px; float: left; height: 2em;">
                BASIC LEVEL EXAMINATION (GRADE 8) ACCORDING TO SCHOOL RECORD HIS/HER
               </div>
               <div>
               <span style="font-size: 15px; float: left;height: 2em;"> DATE OF BIRTH IS </span><span style="font-size: 15px !important; float: right; border-bottom: thin dotted #000; width: 20em !important;height: 2em;">  <?= $information->dob_nepali.' ('. $information->don_english .' A.D. )' ; ?> </span>
                </div>
           <br />
                <table  style="border-collapse: collapse;">

                    <tr>
                        <th align="center" rowspan="2" style="width: 5%; border:1px solid black;">SN</th>
                        <th rowspan="2" style="width: 35% !important; border:1px solid black;">SUBJECTS</th>
                        <th align="center" rowspan="2" style="width: 10%; border:1px solid black;">CREDITS <br /> HOURS</th>
                        <th align="center" colspan="3" style="width: 26% !important;border:1px solid black; text-align:center">OBTAINED GRADE</th>
                        <th align="center" rowspan="2" style="width: 8%; border:1px solid black;">GP</th>
                        <th align="center" rowspan="2" style="width: 16%; border:1px solid black;">REMARKS</th>
                    </tr>
                    <tr>
                        <th align="center" style="width: 7% !important;border:1px solid black;">TH</th>
                        <th align="center" style="width: 7% !important;border:1px solid black;">PR</th>
                        <th align="center" style="width: 12% !important;border:1px solid black;">FINAL</th>
                    </tr>
                    <?php
                    $sn = 0;
                    $result = $information->Result;
                    $marks_information = $information->marks;
                    $total_credits_hours = $credit_section = 0;
                    if(!empty($marks_information)){
                        foreach ($marks_information as $mark_info) {
                            $practical_section = $mark_info->is_practical;
                            $combine_full_marks = $mark_info -> theory_full_mark + $mark_info -> practical_full_mark;
                            $th_grade_information = UtilityFunctions::GradeInformation((int) $mark_info -> theory_mark, (int) $mark_info -> theory_full_mark);
                            $th_grade = isset($th_grade_information['grade']) ? $th_grade_information['grade'] : '--';
                            if($is_practical){
                                $pr_grade_information = UtilityFunctions::GradeInformation((int) $mark_info -> practical_mark, (int) $mark_info -> practical_full_mark);
                                $pr_grade = isset($pr_grade_information['grade']) ? $pr_grade_information['grade'] : '--';
                            }
                            if($is_cas){
                                $cas_grade_information = UtilityFunctions::GradeInformation((int) $mark_info -> cas_mark, (int) $combine_full_marks);
                                $cas_grade = isset($cas_grade_information['grade']) ? $cas_grade_information['grade'] : '--';
                            }
                            $credits_hrs = UtilityFunctions::GradeCreditHours((int)$combine_full_marks);
                            $final_grade = $mark_info->total_grade ?  $mark_info->total_grade :'';
                            $grade_point = $mark_info->total_grade_point ?  $mark_info->total_grade_point :'';
                            $total_gpa = '';
                            foreach ($result as $resultInfo)
                                $total_gpa = $resultInfo->total_gpa;
                            ?>
                            <tr>
                            <td align="center" style="border:1px solid black;"><?= $sn+1; ?></td>
                            <td style=" padding:5px;border:1px solid black;"><?= $mark_info->subject_name; ?></td>
                            <td align="center" style="border:1px solid black;"><?= $credits_hrs; ?></td>
                            <td align="center" style="border:1px solid black;"> <?= $th_grade; ?></td>
                            <?php
                                if($is_practical && isset($is_practical)){
                                    ?>
                                    <td align="center" style="border:1px solid black;"> <?php if($practical_section && $practical_section != 0) {echo $pr_grade;} ?></td>
                                    <?php
                                }
                            ?>
                            <?php
                                if($is_cas && isset($is_cas)){
                                    ?>
                                    <td align="center" style="border:1px solid black;"> <?= $cas_grade; ?></td>
                                    <?php
                                }
                            ?>
                            <td align="center" style="width: 8%; border:1px solid black;"><?= $final_grade; ?></td>
                            <td align="center" style="width: 8%; border:1px solid black;"><?= $grade_point; ?></td>
                            <td style="border-right: 1px solid black;"></td>
                            </tr>
                            <?php
                            $sn++;
                        }
                    }
                    ?>  
                    <tr class="testing_section">
                        <td align="center" colspan="8" style="border:1px solid black; text-align: center">GRAND TOTAL : <?= $total_gpa; ?></td>
                    </tr>
                </table>
                <br />
                <div style="font-size: 7.5pt !important;">
                <sup>*</sup>Grading:A+:Outstanding A:Excellent B+:Very Good B:Good C+:Satisfactory C:Acceptable D+:Partially Acceptable D:insufficient E:Very Insufficient <br />
                <sup>*</sup>A:Absent <sup>*</sup>GP:Grade Point <sup>*</sup>GPA:Grade Point Average
                </div>
                <br /><br />
                <table>
                    <tr>
                        <td style="width: 25%; border-bottom: 1px solid #000;"></td>
                        <td style="width: 12%"></td>
                        <td style="width: 25%;"></td>
                        <td style="width: 8%"></td>
                        <td style="width: 30%; border-bottom: 1px solid #000;"></td>
                    </tr>
                    <tr>
                        <td style="font-size: 8pt !important;">CHECKED BY<br /><br /><br /><br /><br /><br /><br /><br />
                        <td></td>
                        <td style="border: 2px solid #000;">  </td>
                        <td></td>
                        <td style="font-size: 8pt !important;">CHIEF ADMINISTRATIVE OFFICER <br /><br /><br /><br /><br /></td>

                    </tr>
                </table>
        </div>
    </div>
        <?php        }
    }

?>