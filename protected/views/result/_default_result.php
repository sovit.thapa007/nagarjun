
            <div style="height: 10em !important; ">
            </div>
            <div style="font-size: 17px; float: left; height: 2em; padding-top: 10px; font-weight: bold;" >
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?= $information->symbol_number ; ?>
            </div>
            <div style="margin-right: 20em !important; font-size: 16px !important;font-weight: bold;"> <?php echo strtoupper($information->first_name." ".$information->middle_name." ".$information->last_name); ?></div>
            <div style="margin-right: 12em !important; font-size: 16px !important; float: right;height: 2em;font-weight: bold;"> <?= $schoolInformaiton ? $schoolInformaiton->title : ''; ?></div>

               <div style="height: 6em !important; width: 100% !important;"></div>
               <div style="margin-right: 13em !important; font-size: 16px !important; font-weight: bold;">  <?= $information->dob_nepali; ?> </div>
                <div style="width:100%; height: 3em !important;"></div>
                <table  style="border-collapse: collapse; border: 2px solid #000; width: 96% !important;">

                    <tr>
                        <th align="center" rowspan="2" style="width: 5%; border:2px solid black;">SN</th>
                        <th rowspan="2" style="width: 38% !important; border:2px solid black;">SUBJECT</th>
                        <th align="center" rowspan="2" style="width: 7% !important; border:2px solid black;">CREDIT<br /> HOUR</th>
                        <th align="center" colspan="3" style="width: 26% !important;border:2px solid black; text-align:center">OBTAINED GRADE</th>
                        <th align="center" rowspan="2" style="width: 8%; border:2px solid black;">GP</th>
                        <th align="center" rowspan="2" style="width: 16%; border:2px solid black;">REMARKS</th>
                    </tr>
                    <tr>
                        <th align="center" style="width: 7% !important;border:2px solid black;">TH</th>
                        <th align="center" style="width: 7% !important;border:2px solid black;">PR</th>
                        <th align="center" style="width: 12% !important;border:2px solid black;">FINAL</th>
                    </tr>
                    <?php
                    $sn = 0;
                    $result = $information->Result;
                    $marks_information = $information->marks;
                    $total_credits_hours = $credit_section = 0;
                    if(!empty($marks_information)){
                        foreach ($marks_information as $mark_info) {
                            $practical_section = $mark_info->is_practical;
                            $combine_full_marks = $mark_info -> theory_full_mark + $mark_info -> practical_full_mark;
                            $th_grade_information = UtilityFunctions::GradeInformation((int) $mark_info -> theory_mark, (int) $mark_info -> theory_full_mark);
                            $th_grade = isset($th_grade_information['grade']) ? $th_grade_information['grade'] : '--';
                            if($is_practical){
                                $pr_grade_information = UtilityFunctions::GradeInformation((int) $mark_info -> practical_mark, (int) $mark_info -> practical_full_mark);
                                $pr_grade = isset($pr_grade_information['grade']) ? $pr_grade_information['grade'] : '--';
                            }
                            if($is_cas){
                                $cas_grade_information = UtilityFunctions::GradeInformation((int) $mark_info -> cas_mark, (int) $combine_full_marks);
                                $cas_grade = isset($cas_grade_information['grade']) ? $cas_grade_information['grade'] : '--';
                            }
                            $credits_hrs = UtilityFunctions::GradeCreditHours((int)$combine_full_marks);
                            $final_grade = $mark_info->total_grade ?  $mark_info->total_grade :'';
                            $grade_point = $mark_info->total_grade_point ?  $mark_info->total_grade_point :'';
                            $total_gpa = '';
                            $result_publish_date = '';
                            foreach ($result as $resultInfo)
                                $total_gpa = $resultInfo->total_gpa;
                                $result_publish_date = $resultInfo->result_publish_date;
                            ?>
                            <tr>
                            <td align="center" style="border:2px solid black;"><?= $sn+1; ?></td>
                            <td style=" padding:2px;border:2px solid black;"><?= strtoupper($mark_info->subject_name); ?></td>
                            <td align="center" style="border:2px solid black;"><?= $credits_hrs; ?></td>
                            <td align="center" style="border:2px solid black;"> <?= $th_grade; ?></td>
                            <?php
                                if($is_practical && isset($is_practical)){
                                    ?>
                                    <td align="center" style="border:2px solid black;"> <?php if($practical_section && $practical_section != 0) {echo $pr_grade;} ?></td>
                                    <?php
                                }
                            ?>
                            <?php
                                if($is_cas && isset($is_cas)){
                                    ?>
                                    <td align="center" style="border:2px solid black;"> <?= $cas_grade; ?></td>
                                    <?php
                                }
                            ?>
                            <td align="center" style="width: 8%; border:2px solid black;"><?= $final_grade; ?></td>
                            <td align="center" style="width: 8%; border:2px solid black;"><?= $grade_point; ?></td>
                            <td style="border-right: 1px solid black;"></td>
                            </tr>
                            <?php
                            $sn++;
                        }
                    }
                    ?>  
                    <tr class="testing_section">
                        <td align="center" colspan="8" style="border:2px solid black; text-align: center">GPA : <?= $total_gpa; ?></td>
                    </tr>
                </table>
                <br /><br />
                <div style="width:100%; font-size: 16px !important; text-align: right;"> RESULT DATE : <?= $result_publish_date; ?></div>