
<div class="form-horizontal well form-horizontal">
<div class="row-fluid text-center" style="text-align: center; font-weight: bold;"><?= strtoupper(UtilityFunctions::MunicipalLedgerHeader($academic_year)); ?><br /><strong>SCHOOL WISE AVERAGE REPORT</strong></div>
<table id="datatable" class="display items table" style="border-collapse: collapse; width:100%">
        <thead>
            <tr>
                <th style="border: 2px solid black; padding: 10px;">SN</th>
                <th style="border: 2px solid black; padding: 10px;">School Name</th>
                <th style="border: 2px solid black; padding: 10px;">Type</th>
                <th style="border: 2px solid black; padding: 10px;">Name</th>
                <th style="border: 2px solid black; padding: 10px;">Symbol No.</th>
                <th style="border: 2px solid black; padding: 10px;">Grade</th>
                <th style="border: 2px solid black; padding: 10px;">GP</th>
            </tr>
        </thead>
        <tbody>
            
        <?php
            if(!empty($studentResultInformation)){
                $sn = 1;
                foreach ($studentResultInformation as $studentResult) {
                    ?>
                    <tr>
                        <td style="border: 2px solid black; padding: 10px;"><?= $sn++; ?></td>
                        <td style="border: 2px solid black; padding: 10px;"><?= $studentResult->school ? strtoupper($studentResult->school->title) : ''; ?></td>
                        <td style="border: 2px solid black; padding: 10px;"><?= $studentResult->school ? strtoupper($studentResult->school->type) : ''; ?></td>
                        <td style="border: 2px solid black; padding: 10px;"><?= $studentResult->student  ? strtoupper($studentResult->student->first_name.' '.$studentResult->student->middle_name.' '.$studentResult->student->last_name) : ''; ?></td>
                        <td style="border: 2px solid black; padding: 10px;"><?= $studentResult->student  ? $studentResult->student->symbol_number : ''; ?></td>
                        <td style="border: 2px solid black; padding: 10px;"><?= $studentResult->grade; ?></td>
                        <td style="border: 2px solid black; padding: 10px;"><?= $studentResult->total_gpa; ?></td>
                    </tr>
                    <?php
                    # code...
                }
            }
        ?>
        </tbody>
        <tfoot>
            <tr>
                <th style="border: 2px solid black; padding: 10px;">SN</th>
                <th style="border: 2px solid black; padding: 10px;">School Name</th>
                <th style="border: 2px solid black; padding: 10px;">Type</th>
                <th style="border: 2px solid black; padding: 10px;">Name</th>
                <th style="border: 2px solid black; padding: 10px;">Symbol No.</th>
                <th style="border: 2px solid black; padding: 10px;">Grade</th>
                <th style="border: 2px solid black; padding: 10px;">GP</th>
            </tr>
        </tfoot>
    </table>
</div>
