
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'get',
        'type' => 'horizontal',
        'htmlOptions' => array('target'=>'_blank', 'class'=>'form-horizontal well')
    )); ?>
    <h2><STRONG>SCHOOL AVERAGE GARDE REPORT</STRONG></h2>
    <div class="row-fluid">
        <div class="span6">
            <?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span12' , 'required'=>'required')); ?></div>
            <div class="span6">
                <?php echo $form->textFieldRow($model,'top', array('class'=>'span12', 'required'=>'required')); ?>
            </div>
    </div>
        <div class="row-fluid">
                <?php echo $form->dropDownListRow($model,'ledger_type',array('pdf'=>'PDF','excel'=>'Excel'), array('class'=>'span12')); ?>
        </div>
        <div class="form-actions text-center">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type'=>'primary',
                'label'=>'Search Report',
            )); ?>
        </div>

    <?php $this->endWidget(); ?>