
<div class="alert alert-info"><?= $execution_time; ?></div>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    //'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'htmlOptions'=> array(
        'class' => 'form-horizontal well',
        'target'=>'blank'
    ),
    'type' => 'horizontal',
)); ?>
<h2><strong>Generate</strong></h2>
<div class="row-fluid">
    <div class="span6">
        <?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span12', 'required'=>'required')); ?>
    </div>
    <div class="span6">
        <?php echo $form->dropDownListRow($model,'order_by',array('symbol_number'=>'Symbol Number','name'=>'Name','student_id'=>'Registration Number'), array('class'=>'span12')); ?>
    </div>

</div>

<div class="row-fluid">
        <div class="span6">
        <?php echo $form->dropDownListRow($model, 'result_type',array('grade'=>'Grade Result','general'=>"General Result"), array('class'=>"span12", 'required'=>'required')); ?>
        </div>
        <div class="span6">
        <?php 
        if(UtilityFunctions::ShowSchool()){
            echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => Yii::app()->params['select_'])),'id',Yii::app()->params['select_']),array('class'=>'span12', 'prompt'=>'Select School', 'selected'=>1)); 
        }
        ?>
    </div>
</div>
    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'=>'primary',
            'label'=>'Search',
        )); ?>
    </div>

<?php $this->endWidget(); ?>

