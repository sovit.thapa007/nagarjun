
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    //'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'htmlOptions'=> array(
        'class' => 'form-horizontal well',
        'target'=>'blank'
    ),
    'type' => 'horizontal',
)); ?>
<h2><strong>STUDENT LIST</strong></h2>
<div class="row-fluid">
    <div class="span6">
        <?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span12')); ?>
    </div>
    <div class="span6">
        <?php echo $form->dropDownListRow($model,'order_by',array('student_id'=>'Registration Number','name'=>'Name', 'symbol_number'=>'Symbol Number'), array('class'=>'span12')); ?>
    </div>

</div>

<div class="row-fluid">
    <div class="span6">
        <?php echo $form->textFieldRow($model,'symbol_number',array('class'=>'span12')); ?>
    </div>
    <div class="span6">

    <?php 
    if(UtilityFunctions::ShowSchool()){
        echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => Yii::app()->params['select_'])),'id',Yii::app()->params['select_']),array('class'=>'span12', 'prompt'=>'Select School', 'selected'=>1)); 
    }
    ?>
    </div>
</div>
<div class="row-fluid">
    
</div>
<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type'=>'primary',
        'label'=>'Search',
    )); ?>
</div>

<?php $this->endWidget(); ?>
    <?php
        if(!empty($resultData)){
            ?>
            <table id="payment_table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Student Name</th>
                    <th>Registration No.</th>
                    <th>Symbol No.</th>
                    <th>GPA</th>
                    <th>Action</th>
                </tr>
                </thead>
            <?php
            $sn = 0;
            foreach ($resultData as $resultInfo) {
                ?>
                <tr>
                    <td class="serial"><?= $sn+1; ?></td>
                    <td class="serial"><?= $resultInfo->student ? strtoupper($resultInfo->student->first_name.' '.$resultInfo->student->middle_name.' '.$resultInfo->student->last_name) : '' ; ?></td>
                    <td class="serial"><?= $resultInfo->student ? $resultInfo->student->registration_number : '' ; ?></td>
                    <td class="serial"><?= $resultInfo->student ? $resultInfo->student->symbol_number : '' ; ?></td>
                    <td class="serial"><?= $resultInfo->total_gpa; ?></td>
                    <td class="button-column">
                        <a class="view" title="PRINT ORIGINAL GARDE SHEET" data-toggle="tooltip" href="<?php echo Yii::app()->request->baseUrl; ?>/result/gradeSheet/?student_id=<?= $resultInfo->student_id?>&&type=original" target='_blank'><i class="fa fa-file"></i></a>
                        <?php 
                        if(UtilityFunctions::ShowSchool()){
                            ?>
                            <a class="view" title="PRINT COPY GARDE SHEET" data-toggle="tooltip" href="<?php echo Yii::app()->request->baseUrl; ?>/result/gradeSheet/?student_id=<?= $resultInfo->student_id?>&&type=copy" target='_blank'><i class="fa fa-copy"></i></a>
                        <?php
                        }
                        ?>
                    </td>
                </tr>
                <?php
                $sn++;
            }
            ?>
        </table>
        
<script type="text/javascript">
    
    $(document).ready(function () {
        $('#payment_table').DataTable( {
            lengthMenu: [
                [ 25, 30, 50, -1 ],
                [ '25 rows', '30 rows', '50 rows', 'Show all' ]
            ],
            dom: 'Bfrtip',
            buttons: [
                'pageLength','copy', 'csv', 'excel', 'pdf', 'print'
            ],
            //"order": [[ 6, 'asc' ]]
        } );


    });

</script>
        <?php
        }
    ?>
