<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'result-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'school_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'student_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'section',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->dropDownListRow($model,'result_type',array("terminal"=>"terminal","final"=>"final",),array('class'=>'input-large')); ?>

		<?php echo $form->textFieldRow($model,'terminal_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'terminal_',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'total_obtained_mark',array('class'=>'span5','maxlength'=>7)); ?>

		<?php echo $form->textFieldRow($model,'total_full_mark',array('class'=>'span5','maxlength'=>7)); ?>

		<?php echo $form->textFieldRow($model,'percentage',array('class'=>'span5','maxlength'=>5)); ?>

		<?php echo $form->textFieldRow($model,'division',array('class'=>'span5','maxlength'=>20)); ?>

		<?php echo $form->textFieldRow($model,'grade',array('class'=>'span5','maxlength'=>20)); ?>

		<?php echo $form->textFieldRow($model,'attendance',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'total_attendance',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'rank',array('class'=>'span5')); ?>

		<?php echo $form->dropDownListRow($model,'result_status',array("pass"=>"pass","fail"=>"fail",),array('class'=>'input-large')); ?>

		<?php echo $form->textFieldRow($model,'status',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'result_publish_date',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->textFieldRow($model,'number_of_subject',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'approved_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'approved_date',array('class'=>'span5')); ?>

	<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
