<?php
/**
 * Created by PhpStorm.
 * User: Sovit Thapa
 * Date: 7/28/15
 * Time: 12:03 PM
 */
//$this->renderPartial('index');
?>
<style type="text/css">
    @media print
    {   
        .printing_div {
            page-break-inside: avoid;
        }
    }
</style>
           
    <?php
    if(!empty($studentDetail)){
    ?>

            <div id="printing_div" style="height: 40cm !important;">
            <div style="width: 19% !important;float: left;">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo/ministry_education.jpg" style="float: left;width: 130px;"> 
            </div>
            
            <div style="width: 78% !important;float: right; text-align: center;">
                <div style='font-size:20px; font-weight:bold;'>BASIC LEVEL EXAMINATION BOARD</div>
                <div style='font-size:15px; font-weight:bold;'><?= strtoupper(Yii::app()->params['municipality'].','.Yii::app()->params['district']);?></div>
                <div style='font-size:18px; font-weight:bold;'><?= strtoupper(Yii::app()->params['provinance']); ?></div>

                <div style='font-size:15px; font-weight:bold;'>BASIC LEVEL EXAMINATION - <?= $academic_year.' B.S';?></div>
                <div style='font-size:15px; font-weight:bold;'><strong>GRADE SHEET</strong></div>

            </div>
        <div style="font-size: 12px; width: 100% !important; text-align: left;">
            SYMBOL NO. : <?= $studentDetail->symbol_number ; ?>
        </div>
       <div style="font-size: 12px; width: 100% !important; text-align: left;"> THIS IS TO CERTIFY THAT MR./MISS <span style="border-bottom: thin dotted #000;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo strtoupper($studentDetail->first_name." ".$studentDetail->middle_name." ".$studentDetail->last_name); ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
       </div>

        <div style="font-size: 12px; width: 100% !important; text-align: left;"> STUDENT OF<span style="border-bottom: thin dotted #000;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <?= $studentDetail->school_sec ? strtoupper($studentDetail->school_sec->title.', '.Yii::app()->params['municipality'].'-'.$studentDetail->school_sec->ward_no) : ''; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
        </div>
        <div style="font-size: 12px; width: 100% !important; text-align: left;">
                HAS APPEARED IN THE ANNUAL EXAMINATION OF <?= $academic_year ; ?> COMPLETED 
                BASIC LEVEL EXAMINATION (GRADE 8) ACCORDING TO SCHOOL RECORD HIS/HER  DATE OF BIRTH IS <?= $studentDetail->dob_nepali.' ('. $studentDetail->don_english .' A.D. )' ; ?>
        </div>
        <br /><br />
        <table  style="border-collapse: collapse; width: 100% !important;">

            <tr>
                <td align="center" rowspan="2" style="width: 5%; border:1px solid black;">SN</td>
                <td rowspan="2" style="width: 35% !important; border:1px solid black;">SUBJECTS</td>
                <td align="center" rowspan="2" style="width: 10%; border:1px solid black;">CREDITS <br /> HOURS</td>
                <td align="center" colspan="3" style="width: 26% !important;border:1px solid black; text-align:center">OBTAINED GRADE</td>
                <td align="center" rowspan="2" style="width: 8%; border:1px solid black;">GP</td>
                <td align="center" rowspan="2" style="width: 16%; border:1px solid black;">REMARKS</td>
            </tr>
            <tr>
                <td align="center" style="width: 7% !important;border:1px solid black;">TH</td>
                <td align="center" style="width: 7% !important;border:1px solid black;">PR</td>
                <td align="center" style="width: 12% !important;border:1px solid black;">FINAL</td>
            </tr>
            <?php
            $sn = 0;
            $total_credits_hours = $credit_section = 0;
            if(!empty($result)){
                foreach ($markInformation as $mark_info) {
                    $practical_section = $mark_info->is_practical;
                            $combine_full_marks = $mark_info -> theory_full_mark + $mark_info -> practical_full_mark;
                            $credit_section = UtilityFunctions::GradeCreditHours($combine_full_marks);
                            $th_grade_information = UtilityFunctions::GradeInformation((int) $mark_info -> theory_mark, (int) $mark_info -> theory_full_mark);
                            $th_grade = isset($th_grade_information['grade']) ? $th_grade_information['grade'] : '--';
                            if($practical_section){
                                $pr_grade_information = UtilityFunctions::GradeInformation((int) $mark_info -> practical_mark, (int) $mark_info -> practical_full_mark);
                                $pr_grade = isset($pr_grade_information['grade']) ? $pr_grade_information['grade'] : '--';
                            }
                            $final_grade = $mark_info->total_grade ?  $mark_info->total_grade :'';
                            $grade_point = $mark_info->total_grade_point ?  $mark_info->total_grade_point :'';
                            $total_gpa = isset($result->total_gpa) ? $result->total_gpa : '';
                            ?>
                            <tr>
                            <td align="center" style="border:1px solid black;"><?= $sn+1; ?></td>
                            <td style=" padding:10px;border:1px solid black;"><?= $mark_info->subject_name; ?></td>
                            <td align="center" style="border:1px solid black;"><?= $credit_section; ?></td>
                            <td align="center" style="border:1px solid black;"> <?= $th_grade; ?></td>
                            <td align="center" style="border:1px solid black;"> <?php if($practical_section && $practical_section != 0) {echo $pr_grade;} ?></td>
                            <td align="center" style="width: 8%; border:1px solid black;"><?= $final_grade; ?></td>
                            <td align="center" style="width: 8%; border:1px solid black;"><?= $grade_point; ?></td>
                            <td style="border-right: 1px solid black;"></td>
                            </tr>
                            <?php
                            $sn++;
                        }
                    }
                    ?>  
                    <tr class="testing_section">
                        <td align="center" colspan="8" style="border:1px solid black; text-align: center">GPA : <?= $total_gpa; ?></td>
                    </tr>
                </table>
                <br />
                <div style="font-size: 7.5pt !important;">
                <sup>*</sup>Grading:A+:Outstanding A:Excellent B+:Very Good B:Good C+:Satisfactory C:Acceptable D+:Partially Acceptable D:insufficient E:Very Insufficient <br />
                <sup>*</sup>A:Absent <sup>*</sup>GP:Grade Point <sup>*</sup>GPA:Grade Point Average
                </div>
                <br /><br />
                <table>
                    <tr>
                        <td style="width: 25%; border-bottom: 1px solid #000;"></td>
                        <td style="width: 12%"></td>
                        <td style="width: 25%;"></td>
                        <td style="width: 8%"></td>
                        <td style="width: 30%; border-bottom: 1px solid #000;"></td>
                    </tr>
                    <tr>
                        <td style="font-size: 8pt !important;">CHECKED BY<br />
                        <td></td>
                        <td height="20" style="border: 2px solid #000;">  </td>
                        <td></td>
                        <td style="font-size: 8pt !important;">CHIEF ADMINISTRATIVE OFFICER <br /><br /><br /><br /><br />

                    </tr>
                </table>

         <?php
            }
         ?>   
    </div>