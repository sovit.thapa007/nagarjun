<div id="content">
    <div class="form-horizontal well">
        <h2><STRONG>REPORT ACCORDING TO STUDENT GARDE</STRONG></h2>
        <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
            'type' => 'horizontal',
            'htmlOptions' => array('target'=>'_blank')
        )); ?>
        <hr />
        <div class="row-fluid">
            <div class="span6">
                <?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span12')); ?></div>
            <div class="span6">
                <?php echo $form->dropDownListRow($model,'grade',array('4-3.6'=>'A+', '3.6-3.2' => 'A', '3.2-2.8'=>'B+', '2.8-2.4'=>'B', '2.4-2'=>'C+','2-1.6'=>'C','1.6-1.2'=>'D+', '1.2-0.8'=>'D', '0.8-0'=>'E', '0'=>'Abs'), array('prompt'=>'Select Grade','class'=>'span12')); ?>
            </div>
        </div>
            <div class="row-fluid">
                <div class="span6">
                    <?php echo $form->textFieldRow($model,'top', array('class'=>'span12')); ?>
                </div>
                <div class="span6">
                    <?php echo $form->dropDownListRow($model,'ledger_type',array('pdf'=>'PDF','excel'=>'Excel'), array('class'=>'span12')); ?>
                </div>
            </div>
            <div class="row-fluid">
                <?php 
                if(UtilityFunctions::ShowSchool()){
                    echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(),'id','title'),array('class'=>'span12', 'prompt'=>'LIST OF SCHOOL')); 
                }
                ?>
            </div>
            <div class="form-actions text-center">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType' => 'submit',
                    'type'=>'primary',
                    'label'=>'Search Report',
                )); ?>
            </div>
        </div>

        <?php $this->endWidget(); ?>
</div>
