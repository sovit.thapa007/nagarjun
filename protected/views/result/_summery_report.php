
<div class="form-horizontal well form-horizontal">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    //'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'htmlOptions'=> array(
        'class' => 'form-horizontal well',
        'target'=>'blank'
    ),
    'type' => 'horizontal',
)); ?>
<h2><strong>Generate</strong></h2>
<div class="row-fluid">
    <div class="span6">
        <?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span12', 'required'=>'required')); ?>
    </div>
    <div class="span6">
        <?php echo $form->dropDownListRow($model,'result_',array('general'=>'General','pdf'=>'PDF Download'), array('class'=>'span12')); ?>
    </div>

</div>

    <div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>$model->isNewRecord ? 'Create' : 'Save',
        )); ?>
</div>

<?php $this->endWidget(); ?>
<?php
    if(!empty($report)){
?>
    <br />
    <div  style="text-align: center; font-weight: bold;"><?= strtoupper(UtilityFunctions::MunicipalLedgerHeader($academic_year)); ?><br /> SUMMERY REPORT</div>
    <br />
    <table class="table table-bordered table-stripped">
            <tr>
                <th colspan="7" style="text-align: center;">HIGHEST GPA HOLDER STUDENTS LIST</th>
            </tr>
            <tr>
                <th>SN</th>
                <th>SYMBOL NO.</th>
                <th>NAME</th>
                <th>SCHOOL</th>
                <th>SCHOOL CODE</th>
                <th>SCHOOL TYPE</th>
                <th>GPA</th>
            </tr>
            <?php
            $highest_gpa_students = isset($report['highest_gpa_students']) ? $report['highest_gpa_students'] : [];
            $school_array = isset($highest_gpa_students['school']) ? $highest_gpa_students['school'] : [];
                for ($i=0; $i < sizeof($school_array); $i++) { 
                    ?>
                    <tr>
                        <td><?= $i+1; ?></td>
                        <td><?= isset($highest_gpa_students['symbol_number'][$i]) ? $highest_gpa_students['symbol_number'][$i] : '' ?></td>
                        <td><?= isset($highest_gpa_students['name'][$i]) ? $highest_gpa_students['name'][$i] : '' ?></td>
                        <td><?= isset($highest_gpa_students['school'][$i]) ? $highest_gpa_students['school'][$i] : '' ?></td>
                        <td><?= isset($highest_gpa_students['school_code'][$i]) ? $highest_gpa_students['school_code'][$i] : '' ?></td>
                        <td><?= isset($highest_gpa_students['school_type'][$i]) ? strtoupper($highest_gpa_students['school_type'][$i]) : '' ?></td>
                        <td><?= isset($highest_gpa_students['gpa'][$i]) ? $highest_gpa_students['gpa'][$i] : '' ?></td>
                    </tr>
                    <?php
                }
            ?>
        
    </table>

    <br /><br />

    <table class="table table-bordered table-stripped">
            <tr>
                <th colspan="7" style="text-align: center;">HIGHEST GPA HOLDER GENDER WISE</th>
            </tr>
            <tr>
                <th>SN</th>
                <th>SEX</th>
                <th>SYMBOL NO.</th>
                <th>NAME</th>
                <th>SCHOOL</th>
                <th>SCHOOL CODE</th>
                <th>SCHOOL TYPE</th>
                <th>GPA</th>
            </tr>
            <?php
            $sex_wise_highest_gpa_array = isset($report['sex_wise_highest_gpa_array']) ? $report['sex_wise_highest_gpa_array'] : [];

            $male_highest_gpa = isset($sex_wise_highest_gpa_array['male']['school']) ? $sex_wise_highest_gpa_array['male']['school'] : [];
                for ($s=0; $s < sizeof($male_highest_gpa); $s++) {
                    ?>
                    <tr>
                        <td><?= $s+1; ?></td>
                        <td>Male</td>
                        <td><?= isset($sex_wise_highest_gpa_array['male']['symbol_number'][$s]) ? $sex_wise_highest_gpa_array['male']['symbol_number'][$s] : '' ?></td>
                        <td><?= isset($sex_wise_highest_gpa_array['male']['name'][$s]) ? $sex_wise_highest_gpa_array['male']['name'][$s] : '' ?></td>
                        <td><?= isset($sex_wise_highest_gpa_array['male']['school'][$s]) ? $sex_wise_highest_gpa_array['male']['school'][$s] : '' ?></td>
                        <td><?= isset($sex_wise_highest_gpa_array['male']['school_code'][$s]) ? $sex_wise_highest_gpa_array['male']['school_code'][$s] : '' ?></td>
                        <td><?= isset($sex_wise_highest_gpa_array['male']['school_type'][$s]) ? strtoupper($sex_wise_highest_gpa_array['male']['school_type'][$s]) : '' ?></td>
                        <td><?= isset($sex_wise_highest_gpa_array['male']['gpa'][$s]) ? $sex_wise_highest_gpa_array['male']['gpa'][$s] : '' ?></td>
                    </tr>
                    <?php
                }
            $female_highest_gpa = isset($sex_wise_highest_gpa_array['female']['school']) ? $sex_wise_highest_gpa_array['female']['school'] : [];
                for ($fs=0; $fs < sizeof($female_highest_gpa); $fs++) {
                    ?>
                    <tr>
                        <td><?= $fs+1; ?></td>
                        <td>Female</td>
                        <td><?= isset($sex_wise_highest_gpa_array['female']['symbol_number'][$fs]) ? $sex_wise_highest_gpa_array['female']['symbol_number'][$fs] : '' ?></td>
                        <td><?= isset($sex_wise_highest_gpa_array['female']['name'][$fs]) ? $sex_wise_highest_gpa_array['female']['name'][$fs] : '' ?></td>
                        <td><?= isset($sex_wise_highest_gpa_array['female']['school'][$fs]) ? $sex_wise_highest_gpa_array['female']['school'][$fs] : '' ?></td>
                        <td><?= isset($sex_wise_highest_gpa_array['female']['school_code'][$fs]) ? $sex_wise_highest_gpa_array['female']['school_code'][$fs] : '' ?></td>
                        <td><?= isset($sex_wise_highest_gpa_array['female']['school_type'][$fs]) ? strtoupper($sex_wise_highest_gpa_array['female']['school_type'][$fs]) : '' ?></td>
                        <td><?= isset($sex_wise_highest_gpa_array['female']['gpa'][$fs]) ? $sex_wise_highest_gpa_array['female']['gpa'][$fs] : '' ?></td>
                    </tr>
                    <?php
                }
            ?>
        
    </table>
    <h3 style="text-align: center;"><strong>HIGHEST GPA OF <?= strtoupper(implode('/',Yii::app()->params['school_type'])) ?> WITH GENDER WISE</strong> 
    </h3>
    <br />
    <?php
        $type_sex_wise_highest_gpa_array = $report['type_sex_wise_highest_gpa_array'];

        $school_type =  array_keys(Yii::app()->params['school_type']);
        for ($k=0; $k < sizeof($school_type) ; $k++) { 
            $type = $school_type[$k];
            $male_school_type_highest_gpa = isset($type_sex_wise_highest_gpa_array[$type]['male']) ? $type_sex_wise_highest_gpa_array[$type]['male'] : [];
            $female_school_type_highest_gpa = isset($type_sex_wise_highest_gpa_array[$type]['female']) ? $type_sex_wise_highest_gpa_array[$type]['female'] : [];
            $male_student_number = isset($male_school_type_highest_gpa['name']) ? $male_school_type_highest_gpa['name'] : [];
            $female_student_number = isset($female_school_type_highest_gpa['name']) ? $female_school_type_highest_gpa['name'] : [];
            ?>

            <table class="table table-bordered table-stripped">
                    <tr>
                        <th colspan="6" style="text-align: center;">HIGHEST GPA OF <strong><?= strtoupper($type) ?> - MALE</strong></th>
                    </tr>
                    <tr>
                        <th>SN</th>
                        <th>SYMBOL NO.</th>
                        <th>NAME</th>
                        <th>SCHOOL</th>
                        <th>SCHOOL CODE</th>
                        <th>GPA</th>
                    </tr>
                    <?php
                        for ($m=0; $m < sizeof($male_student_number); $m++) { 
                            ?>
                            <tr>
                                <td><?= $m+1; ?></td>
                                <td><?= isset($male_school_type_highest_gpa['symbol_number'][$m]) ? $male_school_type_highest_gpa['symbol_number'][$m] : '' ?></td>
                                <td><?= isset($male_school_type_highest_gpa['name'][$m]) ? $male_school_type_highest_gpa['name'][$m] : '' ?></td>
                                <td><?= isset($male_school_type_highest_gpa['school'][$m]) ? $male_school_type_highest_gpa['school'][$m] : '' ?></td>
                                <td><?= isset($male_school_type_highest_gpa['school_code'][$m]) ? $male_school_type_highest_gpa['school_code'][$m] : '' ?></td>
                                <td><?= isset($male_school_type_highest_gpa['gpa'][$m]) ? $male_school_type_highest_gpa['gpa'][$m] : '' ?></td>
                            </tr>
                            <?php
                        }
                    ?>
                
            </table>
            <table class="table table-bordered table-stripped">
                    <tr>
                        <th colspan="6" style="text-align: center;">HIGHEST GPA OF <strong><?= strtoupper($type) ?> - FEMALE</strong></th>
                    </tr>
                    <tr>
                        <th>SN</th>
                        <th>SYMBOL NO.</th>
                        <th>NAME</th>
                        <th>SCHOOL</th>
                        <th>SCHOOL CODE</th>
                        <th>GPA</th>
                    </tr>
                    <?php
                        for ($f=0; $f < sizeof($female_student_number); $f++) { 
                            ?>
                            <tr>
                                <td><?= $f+1; ?></td>
                                <td><?= isset($female_school_type_highest_gpa['symbol_number'][$f]) ? $female_school_type_highest_gpa['symbol_number'][$f] : '' ?></td>
                                <td><?= isset($female_school_type_highest_gpa['name'][$f]) ? $female_school_type_highest_gpa['name'][$f] : '' ?></td>
                                <td><?= isset($female_school_type_highest_gpa['school'][$f]) ? $female_school_type_highest_gpa['school'][$f] : '' ?></td>
                                <td><?= isset($female_school_type_highest_gpa['school_code'][$f]) ? $female_school_type_highest_gpa['school_code'][$f] : '' ?></td>
                                <td><?= isset($female_school_type_highest_gpa['gpa'][$f]) ? $female_school_type_highest_gpa['gpa'][$f] : '' ?></td>
                            </tr>
                            <?php
                        }
                    ?>
                
            </table>
            <?php
        }

    ?>

    <table class="table table-bordered table-stripped">
            <tr>
                <th colspan="3" style="text-align: center;">AVERAGE GPA</th>
            </tr>
            <tr>
                <th>SN</th>
                <th>TYPE</th>
                <th>GPA</th>
            </tr>
            <?php
            $average_school_type = $report['average_school_type'];
                $school_type =  array_keys(Yii::app()->params['school_type']);
                for ($av=0; $av < sizeof($school_type) ; $av++) { 
                    $type = $school_type[$av];
                    ?>
                    <tr>
                        <td><?= $av+1; ?></td>
                        <td><?= strtoupper($type); ?></td>
                        <td><?= isset($average_school_type[$type]) ? $average_school_type[$type] : ''; ?></td>
                    </tr>
                    <?php
                }
            ?>
            <tr>
                <td><?= $av; ?></td>
                <td><?= strtoupper(Yii::app()->params['municipality']); ?> </td>
                <td><?= isset($report['municipality_average']) ? $report['municipality_average'] : ''; ?></td>
            </tr>
</table>
<?php
    }
?>


</div>