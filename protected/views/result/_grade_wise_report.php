    <div class="form-horizontal well">
        <h2><STRONG>REPORT ON GARDE/STUDENT NUMBER</STRONG></h2>
        <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
            'type' => 'horizontal',
            'htmlOptions' => array('target'=>'_blank')
        )); ?>
        <div class="row-fluid">
            <div class="span6">
                <?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span12', 'required'=>'required')); ?></div>
            <div class="span6">
            <?php 
            if(UtilityFunctions::ShowSchool()){
                echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(),'id','title'),array('class'=>'span12', 'prompt'=>'LIST OF SCHOOL')); 
            }
            ?>
            </div>
        </div>
        <div class="form-actions text-center">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type'=>'primary',
                'label'=>'Search Report',
            )); ?>
        </div>
        </div>

        <?php $this->endWidget(); ?>
    <?php
    if(!empty($years)){
    ?>
    <div class="row-fluid well">
    <div class="row-fluid">
        <table  class="display items table dataTable" style="width: 100%;" role="grid" aria-describedby="example_info">
        <tbody>
            <tr>
                <td style="width: 20%;"> Academic Years : </td>
                <td style="width: 30%;">
                <ol>
                    <?php
                        for ($i=0; $i < sizeof($years) ; $i++) { 
                            ?>
                            <li> <?= $years[$i]; ?> </li>
                            <?php
                        }
                    ?>
                </ol> 
                </td>
                <td style="width: 10%;"> Schools : </td>
                <td style="width: 40%;">
                <ol>
                    <?php
                        for ($j=0; $j < sizeof($school) ; $j++) { 
                            ?>
                            <li> <?= $school[$j]; ?> </li>
                            <?php
                        }
                    ?>
                </ol> 
                </td>
            </tr>
        </tbody>
        </table>
    </div>
        <div id="chartContainer" style="height: 300px; width: 100%;"></div>

        <H3 class="text-center"><strong>NUMBER OF STUDENT GRADE WISE REPORT</strong></H3>
        <div class="row-fluid">
        <table id="example" class="items table table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Aca. Year</th>
                    <?php
                    for ($r=0; $r < sizeof($grade) ; $r++) { 
                        ?>
                        <th><?= $grade[$r]; ?></th>
                        <?php
                    }
                    ?>
                </tr>
                </thead>

                <tbody>
                <?php
                for ($s=0; $s < sizeof($years) ; $s++) { 
                    $ac_year =$years[$s];
                    ?>
                    <tr>
                        <td><?= $s+1; ?></td>
                        <td><?= $years[$s]; ?></td>
                        <?php
                        for($t=0; $t < sizeof($grade) ; $t++) { 
                            $gradeSe =$grade[$t];
                            ?>
                            <td><?= isset($data[$ac_year][$gradeSe]) ? $data[$ac_year][$gradeSe] : 0 ; ?></td>
                            <?php
                        }
                        ?>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>

    </div>
    <?php
    }
    ?>

<script type="text/javascript">

window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
    theme:"dark1",
    animationEnabled: true,
    title:{
        text: "NUMBER OF STUDENT GRADE WISE REPORT"
    },
    axisY :{
        includeZero: false,
        title: "Number of Students",
        //suffix: "mn"
    },
    toolTip: {
        shared: "true"
    },
    legend:{
        cursor:"pointer",
        itemclick : toggleDataSeries
    },
    data: <?= $arrayData; ?>
});
chart.render();

function toggleDataSeries(e) {
    if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible ){
        e.dataSeries.visible = false;
    } else {
        e.dataSeries.visible = true;
    }
    chart.render();
}

    $('#example').DataTable( {
        lengthMenu: [
            [ 25, 30, 50, -1 ],
            [ '25 rows', '30 rows', '50 rows', 'Show all' ]
        ],
        dom: 'Bfrtip',
        buttons: [
            'pageLength','copy', 'csv', 'excel', 'pdf', 'print'
        ],

    } );

}
</script>