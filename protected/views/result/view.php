<?php
$this->breadcrumbs=array(
	'Results'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Result','url'=>array('index')),
array('label'=>'Create Result','url'=>array('create')),
array('label'=>'Update Result','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Result','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Result','url'=>array('admin')),
);
?>

<h1>View Result #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'school_id',
		'academic_year',
		'student_id',
		'section',
		'result_type',
		'terminal_id',
		'terminal_',
		'total_obtained_mark',
		'total_full_mark',
		'percentage',
		'division',
		'grade',
		'attendance',
		'total_attendance',
		'rank',
		'result_status',
		'status',
		'result_publish_date',
		'number_of_subject',
		'created_by',
		'created_date',
		'approved_by',
		'approved_date',
),
)); ?>
