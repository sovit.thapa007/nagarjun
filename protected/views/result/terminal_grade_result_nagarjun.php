<?php
/**
 * Created by PhpStorm.
 * User: Sovit Thapa
 * Date: 7/28/15
 * Time: 12:03 PM
 */
//$this->renderPartial('index');
?>
<style type="text/css">
    @media print
    {   
        .printing_div {
            page-break-inside: avoid;
        }
        th, td {
            padding: 5px; 
            font-weight: bold;
            font-size: 12px !important;
        }
    }
</style>
<?php
if(isset($resultInformation) && !empty($resultInformation)){
    foreach ($resultInformation as $information) {
        $dob_bs_information = $information->dob_nepali ? explode('-', $information->dob_nepali) : [];
        $year = isset($dob_bs_information[0]) ? $dob_bs_information[0] : '';
        $month = isset($dob_bs_information[1]) && strlen($dob_bs_information[1]) == 2 ? $dob_bs_information[1] : '0'.$dob_bs_information[1];
        $day = isset($dob_bs_information[2]) && strlen($dob_bs_information[2]) == 2 ? $dob_bs_information[2] : '0'.$dob_bs_information[2];
        ?>
        <div id="printing_div" style="width: 100% !important;">
        <div style="height: 4.5cm !important;">
        </div>
        <div style="margin-left: 3.8cm !important;font-size: 17px;font-weight: bold"><?= $information->symbol_number ; ?> 
        </div>
        <div style="margin-left: 8.8cm !important; margin-top: 0.7cm !important; font-size: 16px !important;font-weight: bold;"> <?php echo strtoupper($information->first_name." ".$information->middle_name." ".$information->last_name); ?>
        </div>
        <div style="margin-left: 4.3cm !important; margin-top: 0.4cm !important; font-size: 16px !important;font-weight: bold;"> <?= $schoolInformaiton ? strtoupper($schoolInformaiton->title) : ''; ?></div>
        <div style="margin-left: 5.3cm !important; margin-top: 2.1cm !important; font-size: 16px !important;font-weight: bold;"> <?= $year.'-'.$month.'-'.$day; ?></div>
        <div style="height: 2cm !important;">
        </div>
        <div style="margin-left: 0.8cm !important; margin-right: 1cm !important;">
        <table  style="border-collapse: collapse; border: 2px solid #000;">

            <tr>
                <th align="center" rowspan="2" style="width: 5%; border:2px solid black;">SN</th>
                <th rowspan="2" style="width: 42% !important; border:2px solid black;">SUBJECT</th>
                <th align="center" rowspan="2" style="width: 7% !important; border:2px solid black;">CREDIT<br /> HOUR</th>
                <th align="center" colspan="3" style="width: 26% !important;border:2px solid black; text-align:center">OBTAINED GRADE</th>
                <th align="center" rowspan="2" style="width: 8%; border:2px solid black;">GP</th>
                <th align="center" rowspan="2" style="width: 12%; border:2px solid black;">REMARKS</th>
            </tr>
            <tr>
                <th align="center" style="width: 7% !important;border:2px solid black;">TH</th>
                <th align="center" style="width: 7% !important;border:2px solid black;">PR</th>
                <th align="center" style="width: 12% !important;border:2px solid black;">FINAL</th>
            </tr>
            <?php
            $sn = 0;
            $result = $information->Result;
            $marks_information = $information->marks;
            $total_credits_hours = $credit_section = 0;
            if(!empty($marks_information)){
                foreach ($marks_information as $mark_info) {
                    $practical_section = $mark_info->is_practical;
                    $combine_full_marks = $mark_info -> theory_full_mark + $mark_info -> practical_full_mark;
                    $th_grade_information = UtilityFunctions::GradeInformation((int) $mark_info -> theory_mark, (int) $mark_info -> theory_full_mark);
                    $th_grade = isset($th_grade_information['grade']) ? $th_grade_information['grade'] : '--';
                    if($is_practical){
                        $pr_grade_information = UtilityFunctions::GradeInformation((int) $mark_info -> practical_mark, (int) $mark_info -> practical_full_mark);
                        $pr_grade = isset($pr_grade_information['grade']) ? $pr_grade_information['grade'] : '--';
                    }
                    if($is_cas){
                        $cas_grade_information = UtilityFunctions::GradeInformation((int) $mark_info -> cas_mark, (int) $combine_full_marks);
                        $cas_grade = isset($cas_grade_information['grade']) ? $cas_grade_information['grade'] : '--';
                    }
                    $credits_hrs = UtilityFunctions::GradeCreditHours((int)$combine_full_marks);
                    $final_grade = $mark_info->total_grade ?  $mark_info->total_grade :'';
                    $grade_point = $mark_info->total_grade_point ?  $mark_info->total_grade_point :'';
                    $total_gpa = '';
                    $result_publish_date = '';
                    foreach ($result as $resultInfo)
                        $total_gpa = $resultInfo->total_gpa;
                        $result_publish_date = $resultInfo->result_publish_date;
                    ?>
                    <tr>
                    <td align="center" style="border:2px solid black;"><?= $sn+1; ?></td>
                    <td style=" padding:2px;border:2px solid black;"><?= strtoupper($mark_info->subject_name); ?></td>
                    <td align="center" style="border:2px solid black;"><?= $credits_hrs; ?></td>
                    <td align="center" style="border:2px solid black;"> <?= $th_grade; ?></td>
                    <?php
                        if($is_practical && isset($is_practical)){
                            ?>
                            <td align="center" style="border:2px solid black;"> <?php if($practical_section && $practical_section != 0) {echo $pr_grade;} ?></td>
                            <?php
                        }
                    ?>
                    <?php
                        if($is_cas && isset($is_cas)){
                            ?>
                            <td align="center" style="border:2px solid black;"> <?= $cas_grade; ?></td>
                            <?php
                        }
                    ?>
                    <td align="center" style="width: 8%; border:2px solid black;"><?= $final_grade; ?></td>
                    <td align="center" style="width: 8%; border:2px solid black;"><?= number_format($grade_point,1); ?></td>
                    <td style="border-right: 1px solid black;"></td>
                    </tr>
                    <?php
                    $sn++;
                }
            }
            ?>  
            <tr class="testing_section">
                <td align="center" colspan="8" style="border:2px solid black; text-align: center; font-weight: bold; font-size: 16px !important;">GPA : <?= $total_gpa; ?></td>
            </tr>
        </table>
        </div>
        <?php
        if($total_gpa != 0){
            ?>
            <div style="margin-top: 3cm !important; font-size: 16px !important;font-weight: bold; text-align: center;">
                <u><?= $result_publish_date; ?></u>
                </div>
            <div style="font-size: 16px !important;font-weight: bold; text-align: center;">
                RESULT DATE
                </div>
            </div>

            <?php
        }else{
            ?>
            <div style="margin-top: 1.8cm !important; font-size: 16px !important;font-weight: bold; text-align: center;">
                <u><?= $result_publish_date; ?></u>
                </div>
            <div style="font-size: 16px !important;font-weight: bold; text-align: center;">
                RESULT DATE
                </div>
            </div>
            <?php
        }
        ?>
    <?php
    }
}
?>