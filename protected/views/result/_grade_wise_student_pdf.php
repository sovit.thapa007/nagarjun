<style type="text/css">
    tr td{
    page-break-inside: avoid;
    white-space: nowrap;
}
</style>
<?php
    if(!empty($reportData)){
        $arrayWiseDataSet = isset($reportData['arrayWiseDataSet']) ? $reportData['arrayWiseDataSet'] : [];
        $school_type_data_array = isset($reportData['school_type_data_array']) ? $reportData['school_type_data_array'] : [];
        $school_id_array_data = isset($reportData['school_id_array_data']) ? $reportData['school_id_array_data'] : [];
        $school_type = array_keys(Yii::app()->params['school_type']);
        $schoolArray = isset($reportData['schoolArray']) ? $reportData['schoolArray'] : [];
        $gradeLetter = isset($reportData['gradeLetter']) ? $reportData['gradeLetter'] : [];
        ?>
        <h3 style='text-align: center; font-weight: bold;'><?= strtoupper(UtilityFunctions::MunicipalLedgerHeader($academic_year)); ?></h3>
        <table style="border-collapse: collapse; width: 100% !important; border: 1px solid #000;">
            <thead>
                <tr style="border: 1px solid #000;">
                    <th style="border: 1px solid #000;">SN</th>
                    <th style="border: 1px solid #000;">Type</th>
                    <th style="border: 1px solid #000;">School</th>
                    <th style="border: 1px solid #000;">School Code</th>
                    <?php
                    for ($i=0; $i < sizeof($gradeLetter) ; $i++) { 
                        ?>
                        <th style="border: 1px solid #000;"><?= isset($gradeLetter[$i]) ? UtilityFunctions::GPARange($gradeLetter[$i]) : ''; ?></th>
                        <?php
                    }
                    ?>
                    <th style="border: 1px solid #000;">Total</th>
                </tr>
            </thead>
            <?php
            $total_student_ = 0;
            $total_grade_wise_student =  [];
            for ($j=0; $j < sizeof($school_id_array_data) ; $j++){
                $school_id = $school_id_array_data[$j];
                $school_information = isset($schoolArray[$school_id]) ? $schoolArray[$school_id] : null;
                $school_info = explode(':', $school_information);
                ?>
                <tr style="border: 1px solid #000;">
                    <td style="border: 1px solid #000;"><?= $j+1; ?></td>
                    <td style="border: 1px solid #000;"><?= isset($school_info[2]) ? strtoupper($school_info[2]) : ''; ?></td>
                    <td style="border: 1px solid #000;"><?= isset($school_info[0]) ? strtoupper($school_info[0]) : ''; ?></td>
                    <td style="border: 1px solid #000;"><?= isset($school_info[1]) ? $school_info[1] : ''; ?></td>
                    <?php
                    $total_student_number = 0;
                    for ($k=0; $k < sizeof($gradeLetter) ; $k++) { 
                        $grade = $gradeLetter[$k];
                        $studentNumber = isset($arrayWiseDataSet[$school_id][$grade]) ? $arrayWiseDataSet[$school_id][$grade] : 0;
                        $total_student_number += $studentNumber;
                        if(isset($total_grade_wise_student[$grade])){
                            $total_grade_wise_student[$grade] = $studentNumber + $total_grade_wise_student[$grade];
                        }else
                            $total_grade_wise_student[$grade] = $studentNumber;
                        ?>
                        <td style="border: 1px solid #000;">
                            <?= $studentNumber; ?>
                        </td>
                        <?php
                    }
                    ?>
                    <td style="border: 1px solid #000;"><?= $total_student_number; ?></td>
                </tr>
                <?php
                $total_student_ = $total_student_ + $total_student_number;
            }
            ?>
            <tr>
                <td colspan="4">
                    Total   
                </td>
                <?php
                for ($o=0; $o < sizeof($gradeLetter) ; $o++) { 
                    $grade = $gradeLetter[$o];
                    ?>
                    <td style="border: 1px solid #000;">
                        <?= isset($total_grade_wise_student[$grade]) ? $total_grade_wise_student[$grade] : 0; ?>
                    </td>
                    <?php
                }
                ?>
                <td style="border: 1px solid #000;"><?= $total_student_; ?></td>
            </tr>
        </table>
        <br /><br />
        <h3 style="text-align: center;"> <strong><?= strtoupper(Yii::app()->params['municipality']) ?>, BASIC EXAMINATION RESULT <?= $academic_year; ?></strong>  </h3>
      <table style="border-collapse: collapse; width: 100% !important; border: 1px solid #000;">
            <thead>
                <tr style="border: 1px solid #000;">
                    <th style="border: 1px solid #000;">SN</th>
                    <th style="border: 1px solid #000;">Type</th>
                    <?php
                    for ($i=0; $i < sizeof($gradeLetter) ; $i++) { 
                        ?>
                        <th style="border: 1px solid #000;"><?= isset($gradeLetter[$i]) ? UtilityFunctions::GPARange($gradeLetter[$i]) : ''; ?></th>
                        <?php
                    }
                    ?>
                    <th style="border: 1px solid #000;">Total</th>
                </tr>
            </thead>
            <?php
            $total_student_ = 0;
            $total_grade_wise_student_ =  [];
            for ($t=0; $t < sizeof($school_type) ; $t++){
                $type = $school_type[$t];
                ?>
                <tr style="border: 1px solid #000;">
                    <td style="border: 1px solid #000;"><?= $t+1; ?></td>
                    <td style="border: 1px solid #000;"><?= strtoupper($type); ?></td>
                    <?php
                    $total_student_number = 0;
                    for ($k=0; $k < sizeof($gradeLetter) ; $k++) { 
                        $grade = $gradeLetter[$k];
                        $student_number = isset($school_type_data_array[$type][$grade]) ? $school_type_data_array[$type][$grade] : 0;
                        $total_student_number += $student_number;
                        if(isset($total_grade_wise_student_[$grade])){
                            $total_grade_wise_student_[$grade] = $student_number + $total_grade_wise_student_[$grade];
                        }else
                            $total_grade_wise_student_[$grade] = $student_number;
                        ?>
                        <td style="border: 1px solid #000;">
                            <?= $student_number; ?>
                        </td>
                        <?php
                    }
                    ?>
                    <td style="border: 1px solid #000;"><?= $total_student_number; ?></td>
                </tr>
                <?php
                $total_student_ = $total_student_ + $total_student_number;
            }
            ?>
            <tr>
                <td colspan="2">
                    Total   
                </td>
                <?php
                for ($o=0; $o < sizeof($gradeLetter) ; $o++) { 
                    $grade = $gradeLetter[$o];
                    ?>
                    <td style="border: 1px solid #000;">
                        <?= isset($total_grade_wise_student_[$grade]) ? $total_grade_wise_student_[$grade] : 0; ?>
                    </td>
                    <?php
                }
                ?>
                <td style="border: 1px solid #000;"><?= $total_student_; ?></td>
            </tr>
        </table>
        <?php
    }
    
?>
