<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'marks-convertor-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListRow($model,'type',array("grade"=>"grade","terminal"=>"terminal",),array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'code',array('class'=>'span5','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'min_range',array('class'=>'span5','maxlength'=>5)); ?>

		<?php echo $form->textFieldRow($model,'max_range',array('class'=>'span5','maxlength'=>5)); ?>

		<?php //echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

		<?php //echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?>

	<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
