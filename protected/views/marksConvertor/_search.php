<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->dropDownListRow($model,'type',array("grade"=>"grade","terminal"=>"terminal",),array('class'=>'input-large')); ?>

		<?php echo $form->textFieldRow($model,'code',array('class'=>'span5','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'min_range',array('class'=>'span5','maxlength'=>5)); ?>

		<?php echo $form->textFieldRow($model,'max_range',array('class'=>'span5','maxlength'=>5)); ?>

		<?php echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
</div>

<?php $this->endWidget(); ?>
