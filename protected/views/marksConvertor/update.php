<?php
$this->breadcrumbs=array(
	'Marks Convertors'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List MarksConvertor','url'=>array('index')),
array('label'=>'Create MarksConvertor','url'=>array('create')),
array('label'=>'View MarksConvertor','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage MarksConvertor','url'=>array('admin')),
);
?>

<h1>Update MarksConvertor <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>