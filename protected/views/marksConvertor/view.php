<?php
$this->breadcrumbs=array(
	'Marks Convertors'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List MarksConvertor','url'=>array('index')),
array('label'=>'Create MarksConvertor','url'=>array('create')),
array('label'=>'Update MarksConvertor','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete MarksConvertor','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage MarksConvertor','url'=>array('admin')),
);
?>

<h1>View MarksConvertor #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'type',
		'code',
		'min_range',
		'max_range',
		'created_by',
		'created_date',
),
)); ?>
