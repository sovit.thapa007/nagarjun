<?php
$this->breadcrumbs=array(
	'Marks Convertors'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List MarksConvertor','url'=>array('index')),
array('label'=>'Manage MarksConvertor','url'=>array('admin')),
);
?>

<h1>Create MarksConvertor</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>