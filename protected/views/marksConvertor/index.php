<?php
$this->breadcrumbs=array(
	'Marks Convertors',
);

$this->menu=array(
array('label'=>'Create MarksConvertor','url'=>array('create')),
array('label'=>'Manage MarksConvertor','url'=>array('admin')),
);
?>

<h1>Marks Convertors</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
