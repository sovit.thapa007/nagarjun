
	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'student-exam-registration-form',
	'enableAjaxValidation'=>false,
	'method'=>'GET',
    'htmlOptions' => array('class'=>'form-horizontal well'),
	'action'=>Yii::app()->baseUrl.'/studentExamRegistration/studentList',
	)); ?>
    <h2><STRONG>ORDER/ARRANGE STUDENT BY NAME/REGISTRATION NO./REGISTRATION DATE FOR ASSIGNING SYMBOL NO.</STRONG></h2>
		<input type="hidden" name="school_id" value="<?= $school_id; ?>" >

    <div class="row-fluid">
		<label>ARRANGE BY</label>
		<select name="order_by" id="order_by">
			<option value="name"> Name (alphabate)</option>
			<option value="registration_no"> Registration No.</option>
			<option value="updated_date"> Registration Date</option>
		</select> 
	</div>
	<BR />
	<div class="form-actions text-center">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'SEARCH BUTTON',
		)); ?>
	</div>

		<?php $this->endWidget(); ?>


		<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'student-exam-registration-form',
		'enableAjaxValidation'=>false,
		'method'=>'POST',
    	'htmlOptions' => array('class'=>'form-horizontal well'),
		'action'=>Yii::app()->baseUrl.'/studentExamRegistration/generatSymbolNumber',
		)); ?>
    	<h2><STRONG>STUDENTS LIST (SYMBOL NUMBER WILL GENERATE IN THIS ORDER)</STRONG></h2>
    	<div class="row-fluid">

		<table class="table table-striped table-bordered table-condensed">
			<tr>
				<td>School Name : <?= !empty($schoolRegistrationSection) && $schoolRegistrationSection->school ?  ucwords($schoolRegistrationSection->school->title) : ''; ?></td>
				<td>School Code : <?= !empty($schoolRegistrationSection) && $schoolRegistrationSection->school ?  ucwords($schoolRegistrationSection->school->schole_code) : ''; ?></td>
				<td>Address : <?= !empty($schoolRegistrationSection) && $schoolRegistrationSection->school ?  ucwords($schoolRegistrationSection->school->ward_no.' - '.$schoolRegistrationSection->school->tole) : ''; ?></td>
			</tr>

			<tr>
				<td>Contact No. :<?= !empty($schoolRegistrationSection) && $schoolRegistrationSection->school ? ucwords($schoolRegistrationSection->school->contact_number.'  - '.$schoolRegistrationSection->school->school_email) : ''; ?></td>
				<td>Contact Person : <?= !empty($schoolRegistrationSection) && $schoolRegistrationSection->school ? ucwords($schoolRegistrationSection->school->contact_person) : ''; ?></td>
				<td>Academic Year : <?= UtilityFunctions::AcademicYear(); ?> </td>
			</tr>
			<tr>
				<td>Total Student : <?= !empty($$schoolRegistrationSection) ? $schoolRegistrationSection->total_student : ''; ?></td>
				<td>Starting NO. : <?= !empty($$schoolRegistrationSection) ? $schoolRegistrationSection->start_point :'' ; ?></td>
				<td>Maximum NO. : <?= !empty($$schoolRegistrationSection) ? $schoolRegistrationSection->end_point : ''; ?></td>
			</tr>
		</table>
    	</div>
		<ol id="sortable">
 
		<?php
		if(!empty($student_list)){
		$sn =  0;
		foreach ($student_list as $student) {
			?>
  			<li class="ui-state-default" style="padding: 10px; margin-bottom: 5px;"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><?= ucwords($student->first_name.' '.$student->middle_name.' '.$student->last_name).' && Symbol No. : '.$student->symbol_number.' && Registration Number : '.$student->registration_number; ?>
			<input type="hidden" id="student_[]" name="student_[]" value="<?= $student->id; ?>">
  			</li>
			<?php
			$sn++;
		}
		}
		?>
		</ol>
		</table>
		<input type="hidden" name="school_id" value="<?= $school_id; ?>" >
		<div class="form-actions text-center">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>'SUBMIT INFORMATION',
			)); ?>
		</div>
	<?php $this->endWidget(); ?>
</div>

<script type="text/javascript">

	/*$(document).ready(function(){*/
	$('#minimum_range_0').keyup(function() {
		var startingNumber = $(this).val();
		var total_school_number = $('#total_school_number').val();
		for (var i = 0; i < total_school_number; i++) {
			var school_wise_student = $('#_total_student_number_'+i).val();
			if(i != 0)
				startingNumber = parseInt(maximum_range_number) + 1;
			var maximum_range_number = parseInt(startingNumber) + parseInt(school_wise_student) - parseInt(1);
			if(i != 0)
				$('#minimum_range_'+i).val(startingNumber);
			$('#maximum_range_'+i).val(maximum_range_number)
		}
	});
	
  $( function() {
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
  } );
	
</script>