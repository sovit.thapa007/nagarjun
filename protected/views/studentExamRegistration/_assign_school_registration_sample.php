
<div class="row-fluid">
	<div class="well text-center"> <STRONG>ASSIGN SCHOOL MINIMUM AND MAXIMUM NUMBER</STRONG></div>


	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'student-exam-registration-form',
	'enableAjaxValidation'=>false,
	'action'=>'reviewRegistrationNumber',
	)); ?>

		<table class="table table-striped table-bordered table-condensed">
			<thead>
				<th>SN</th>
				<th>Type</th>
				<th>Name</th>
				<th>Total Student Number</th>
				<th>Minimum Range</th>
				<th>Minimum Range</th>
			</thead>
		<?php
		if(!empty($schoolData)){
			$sn = $total_student_number = 0;
			foreach ($schoolData as $school) {
				$studentNumber = StudentInformation::model()->StudentNumber($academic_year, $school->id);
				$total_student_number += $studentNumber;
				?>
				<tr>
					<td> <?= $sn+1; ?> </td>
					<td> <?= $school->type; ?> </td>
					<td> <?= $school->title; ?> </td>
					<td> <?= $studentNumber; ?> 
					<input type="hidden" id="_total_student_number_<?= $sn; ?>" name="_total_student_number_<?= $sn; ?>" value="<?= $studentNumber; ?>">

					<input type="hidden" id="_total_student_number_<?= $sn; ?>" name="_total_student_number_<?= $sn; ?>" value="<?= $studentNumber; ?>">
				</td>
					<td>
					<input type="number" id="minimum_range_<?= $sn; ?>" name="minimum_range_<?= $school->id; ?>" value=""> 
					</td>
					<td> <input type="number" id="maximum_range_<?= $sn; ?>" name="maximum_range_<?= $school->id; ?>" value=""> </td>
				</tr>
				<?php
				$sn++;
			}
		}
		?>
		<tr>
			<td colspan="3">
				TOTAL STUDENT NUMBER 
			</td>
			<td colspan="3">
				<?= $total_student_number; ?>
			</td>

		</tr>
		</table>
		<input type="hidden" id="total_school_number" name="total_school_number" value="<?= $sn; ?>">
		<div class="form-actions text-center">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>'SUBMIT INFORMATION',
			)); ?>
		</div>

	<?php $this->endWidget(); ?>
</div>

<script type="text/javascript">

	/*$(document).ready(function(){*/
	$('#minimum_range_0').keyup(function() {
		var startingNumber = $(this).val();
		var total_school_number = $('#total_school_number').val();
		for (var i = 0; i < total_school_number; i++) {
			var school_wise_student = $('#_total_student_number_'+i).val();
			if(i != 0)
				startingNumber = parseInt(maximum_range_number) + 1;
			var maximum_range_number = parseInt(startingNumber) + parseInt(school_wise_student) - parseInt(1);
			if(i != 0)
				$('#minimum_range_'+i).val(startingNumber);
			$('#maximum_range_'+i).val(maximum_range_number)
		}
	});
	
</script>