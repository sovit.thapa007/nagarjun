<?php
$this->breadcrumbs=array(
	'Student Exam Registrations',
);

$this->menu=array(
array('label'=>'Create StudentExamRegistration','url'=>array('create')),
array('label'=>'Manage StudentExamRegistration','url'=>array('admin')),
);
?>

<h1>Student Exam Registrations</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
