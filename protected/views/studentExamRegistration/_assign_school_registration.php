
<div class="form-horizontal well form-horizontal">
	<H2 class="text-center"> <STRONG>STUDENT SYMBOL NUMBER WILL BE</STRONG></H2>


	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'student-exam-registration-form',
	'enableAjaxValidation'=>false,
	'action'=>'reviewRegistrationNumber',
	)); ?>
	<table class="table table-bordered">
		<thead>
			<td>SN</td>
			<td>SCHOOL</td>
			<td>CODE</td>
			<td>STUDENT NO.</td>
			<td>START FROM</td>
			<td>END AT</td>
		</thead>
		<?php
		if(!empty($schoolData)){
			$sn = $total_student_number = 0;
			foreach ($schoolData as $school) {
				$studentNumber = StudentInformation::model()->StudentNumber($academic_year, $school->id);
				$total_student_number += $studentNumber;
				?>
				<tr>
					<td><?= $sn+1; ?></td>
					<td><?= strtoupper($school->title); ?>
						<input type="hidden" id="school_id_[]" name="school_id_[]" value="<?= $school->id; ?>"></td>
					<td><?= $school->schole_code; ?>
						<input type="hidden" id="_school_code_<?= $sn; ?>" name="_school_code_<?= $school->id; ?>" value="<?= $school->schole_code; ?>">
					</td>
					<td><?= $studentNumber; ?>
						<input type="hidden" id="_total_student_number_<?= $sn; ?>" name="total_student_number_<?= $school->id; ?>" value="<?= $studentNumber; ?>">
					</td>
					<td>
						<span id="minimum_range_<?= $sn; ?>">
			  			<input type="hidden" id="minimum_range_value_<?= $sn; ?>" name="minimum_range_<?= $school->id; ?>" value="">
					</td>
					<td>
						<span id="maximum_range_<?= $sn; ?>">
			  			<input type="hidden" id="maximum_range_value_<?= $sn; ?>" name="maximum_range_<?= $school->id; ?>" value=""> 
					</td>

				</tr>
					
				<?php
				$sn++;
			}
		}
		?></table>
		<input type="hidden" id="total_school_number" name="total_school_number" value="<?= $sn; ?>">
		<div class="form-actions text-center">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>'REGISTER INFORMATION',
			)); ?>
		</div>

	<?php $this->endWidget(); ?>
</div>

<script type="text/javascript">

	$(document).ready(function(){
		var total_school_number = $('#total_school_number').val();
		for (var i = 0; i < total_school_number; i++) {
			var school_wise_student = $('#_total_student_number_'+i).val();
			var school_code = $('#_school_code_'+i).val();
			var startingNumber = school_code + '001';
			var maximum_range_number = parseInt(startingNumber) + parseInt(school_wise_student) - parseInt(1);
			$('#minimum_range_'+i).text(startingNumber);
			$('#maximum_range_'+i).text(maximum_range_number)
		}
	});

</script>