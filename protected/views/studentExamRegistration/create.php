<?php
$this->breadcrumbs=array(
	'Student Exam Registrations'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List StudentExamRegistration','url'=>array('index')),
array('label'=>'Manage StudentExamRegistration','url'=>array('admin')),
);
?>

<h1>Create StudentExamRegistration</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>