<div class="form-horizontal well form-horizontal">
<table class="table table-bordered" style="border: 1px solid #000; border-collapse: collapse;">
    <tr style="border: 1px solid #000;">
        <td colspan="11" style="border: 1px solid #000; text-align: center;">
            <strong><?= strtoupper(UtilityFunctions::MunicipalLedgerHeader()); ?></strong>
        </td>
    </tr>
            <tr style="border: 1px solid #000;">
				<td rowspan="2" style="border: 1px solid #000;">SN</td>
				<td rowspan="2" style="border: 1px solid #000;">Code</td>
				<td rowspan="2" style="border: 1px solid #000;">School</td>
				<td colspan="2" style="border: 1px solid #000; text-align: center;">Address</td>
				<td rowspan="2" style="border: 1px solid #000;">School Type</td>
				<td colspan="3" style="border: 1px solid #000; text-align: center;">Student</td>
				<td colspan="3" style="border: 1px solid #000; text-align: center;">Symbol No.</td>
            </tr>
            <tr style="border: 1px solid #000;">
            	<td style="border: 1px solid #000;">Metropolitan city</td>
            	<td style="border: 1px solid #000;">Ward</td>
            	<td style="border: 1px solid #000;">Girls</td>
            	<td style="border: 1px solid #000;">Boys</td>
            	<td style="border: 1px solid #000;">Total</td>
            	<td style="border: 1px solid #000;">From</td>
            	<td style="border: 1px solid #000;">To</td>
            </tr>
			<?php
				$sn = $total_male = $total_female = $grand_total = 0;
			if(!empty($schoolRegistrationList)){
				foreach ($schoolRegistrationList as $schoolList) {
					$male = isset($resource_school_detail[$schoolList->id]) && isset($resource_school_detail[$schoolList->id]['male']) ? $resource_school_detail[$schoolList->id]['male'] : 0;
					$female = isset($resource_school_detail[$schoolList->id]) && isset($resource_school_detail[$schoolList->id]['female']) ? $resource_school_detail[$schoolList->id]['female'] : 0;
					$total = $male + $female;
					$tr_backgroun = '';
					if($schoolList->total_student != $total)
						$tr_backgroun = 'background-color: red;';
					$total_male += $male;
					$total_female += $female;
					$grand_total += $schoolList->total_student;
					?>
					<tr style="border: 1px solid #000; <?= $tr_backgroun; ?>">
						<td style="border: 1px solid #000;"> <?= $sn+1; ?> </td>
						<td style="border: 1px solid #000;"> <?= $schoolList->school ? $schoolList->school->schole_code : '' ?></a></td>
						<td style="border: 1px solid #000;"> <?= strtoupper($schoolList->school ? $schoolList->school->title : '') ?></td>
						<td style="border: 1px solid #000;"> <?= strtoupper(Yii::app()->params['municipality_short']); ?> </td>
						<td style="border: 1px solid #000;"> <?= $schoolList->school ? $schoolList->school->ward_no : '' ?> </td>
						<td style="border: 1px solid #000;"> <?= $schoolList->school ? strtoupper($schoolList->school->type) : '' ?> </td>
						<td style="border: 1px solid #000;"> <?= $female; ?> </td>
						<td style="border: 1px solid #000;"> <?= $male; ?> </td>
						<td style="border: 1px solid #000;"> <?= $schoolList->total_student; ?> </td>
						<td style="border: 1px solid #000;"> <?= $schoolList->start_point; ?> </td>
						<td style="border: 1px solid #000;"> <?= $schoolList->end_point; ?> </td>
					</tr>
					<?php
					$sn++;
				}
			}
			?>
			<tr>
				<td colspan="6" style="border: 1px solid #000;">Total</td>
				<td style="border: 1px solid #000;"><?= $total_female; ?></td>
				<td style="border: 1px solid #000;"><?= $total_male; ?></td>
				<td style="border: 1px solid #000;"><?= $grand_total; ?></td>
				<td colspan="2"></td>


			</tr>
    </table>
</div>

