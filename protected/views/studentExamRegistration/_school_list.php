<?php
	$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'student-exam-registration-form',
		'enableAjaxValidation'=>false,
	)); ?>
	<div class="alert well text-center"><strong>ORDER SCHOOL</strong></div>
	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->dropDownListRow($model,'type',array('title'=>'School Name', 'schole_code'=>'School Code', 'type'=>'School Type'),array('class'=>'span12')); ?>
		</div>
		<div class="span6">
			<?php echo $form->dropDownListRow($model,'order_by',array('ASC'=>'ASC ORDER', 'DESC'=>'DESC ORDER'),array('class'=>'span12')); ?>
		</div>
	</div>

	<div class="form-actions text-center">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'SUBMIT ORDER',
		)); ?>
</div>

<?php $this->endWidget(); ?>
