<div class="form-horizontal well form-horizontal">
	<br />
	<div class="row-fluid text-center" style="font-weight: bold;">
		<?= strtoupper(UtilityFunctions::MunicipalLedgerHeader()); ?> <p style="text-align: right;"> <a href="<?= Yii::app()->request->baseUrl; ?>/studentExamRegistration/SchoolWiseStudentPdf" class='btn btn-primary btn-sm' target='_blank'><i class="fa fa-file"></i> &nbsp;&nbsp; PDF DOWNLOAD</a> 
	</div>
	<hr />
<table id="datatable" class="table table-bordered">
	<thead>
		<th>SN.</th>
		<th>Code</th>
		<th>School</th>
		<th>Address</th>
		<th>Type</th>
		<th>Girls</th>
		<th>Boys</th>
		<th>Total</th>
		<th>Symbol No. From</th>
		<th>Symbol No. To</th>
	</thead>
	<tbody>
			<?php
				$sn = $total_male = $total_female = $grand_total = 0;
			if(!empty($schoolRegistrationList)){
				foreach ($schoolRegistrationList as $schoolList) {
					$male = isset($resource_school_detail[$schoolList->id]) && isset($resource_school_detail[$schoolList->id]['male']) ? $resource_school_detail[$schoolList->id]['male'] : 0;
					$female = isset($resource_school_detail[$schoolList->id]) && isset($resource_school_detail[$schoolList->id]['female']) ? $resource_school_detail[$schoolList->id]['female'] : 0;
					$total = $male + $female;
					$tr_backgroun = '';
					if($schoolList->total_student != $total)
						$tr_backgroun = 'background-color: red;';
					$total_male += $male;
					$total_female += $female;
					$grand_total += $schoolList->total_student;
					?>
					<tr style="<?= $tr_backgroun; ?>">
						<td> <?= $sn+1; ?> </td>
						<td> <?= $schoolList->school ? $schoolList->school->schole_code : '' ?></a></td>
						<td> <?= strtoupper($schoolList->school ? $schoolList->school->title : '') ?></td>
						<td> <?= $schoolList->school ? strtoupper(Yii::app()->params['municipality_short']).'-'.$schoolList->school->ward_no : ''; ?> </td>
						<td> <?= $schoolList->school ? strtoupper($schoolList->school->type) : '' ?> </td>
						<td> <?= $female; ?> </td>
						<td> <?= $male; ?> </td>
						<td> <?= $schoolList->total_student; ?> </td>
						<td> <?= $schoolList->start_point; ?> </td>
						<td> <?= $schoolList->end_point; ?> </td>
					</tr>
					<?php
					$sn++;
				}
			}
			?>
		</tbody>
	<tfoot>
		<th>SN.</th>
		<th>Code</th>
		<th>School</th>
		<th>Address</th>
		<th>Type</th>
		<th>Girls</th>
		<th>Boys</th>
		<th>Total</th>
		<th>Symbol No. From</th>
		<th>Symbol No. To</th>
	</tfoot>
    </table>
</div>
