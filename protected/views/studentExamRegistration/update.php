<?php
$this->breadcrumbs=array(
	'Student Exam Registrations'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List StudentExamRegistration','url'=>array('index')),
array('label'=>'Create StudentExamRegistration','url'=>array('create')),
array('label'=>'View StudentExamRegistration','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage StudentExamRegistration','url'=>array('admin')),
);
?>

<h1>Update StudentExamRegistration <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>