
<div class="form-horizontal well form-horizontal">
	<h2><STRONG>SYMBOL NUMBER STARTING AND END POINT OF SCHOOL</STRONG></h2>
<table id="datatable" class="items table table-bordered table-striped" style="width:100%">
        <thead>
            <tr>
				<th>SN</th>
				<th>Year</th>
				<th>School</th>
				<th>Code</th>
				<th>Students No.</th>
				<th>Symbol No. Start</th>
				<th>Symbol No. End</th>
            </tr>
        </thead>
        <tbody>
			<?php
				$sn = $total_student_number = 0;
			if(!empty($schoolRegistrationList)){
				foreach ($schoolRegistrationList as $schoolList) {
					?>
					<tr>
						<td> <?= $sn+1; ?> </td>
						<td><?= $schoolList->academic_year; ?></td>
						<td> <a href="<?= Yii::app()->baseUrl.'/studentInformation/index?StudentInformation%5Bschool_id%5D='.$schoolList->id.'&StudentInformation%5Border_by%5D=symbol_number' ?>" ><?= strtoupper($schoolList->school ? $schoolList->school->title : '') ?></a></td>
						<td> <?= $schoolList->school ? $schoolList->school->schole_code : '' ?></a></td>
						<td> <?= $schoolList->total_student; ?> </td>
						<td> <?= $schoolList->start_point; ?> </td>
						<td> <?= $schoolList->end_point; ?> </td>
					</tr>
					<?php
					$sn++;
				}
			}
			?>
        </tbody>

        <tfoot>
            <tr>
				<th>SN</th>
				<th>Year</th>
				<th>School</th>
				<th>Code</th>
				<th>Students No.</th>
				<th>Symbol No. Start</th>
				<th>Symbol No. End</th>
            </tr>
        </tfoot>
    </table>
</div>

