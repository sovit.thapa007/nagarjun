<?php
$this->breadcrumbs=array(
	'Student Exam Registrations'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List StudentExamRegistration','url'=>array('index')),
array('label'=>'Create StudentExamRegistration','url'=>array('create')),
array('label'=>'Update StudentExamRegistration','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete StudentExamRegistration','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage StudentExamRegistration','url'=>array('admin')),
);
?>

<h1>View StudentExamRegistration #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'academic_year',
		'school_id',
		'start_point',
		'end_point',
		'total_student',
		'status',
		'approved_',
		'created_at',
		'created_by',
		'updated_by',
		'updated_date',
),
)); ?>
