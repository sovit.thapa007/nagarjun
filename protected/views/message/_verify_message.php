 
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'id'=>'message-form',
        'enableAjaxValidation'=>false,
        'action'=>Yii::app()->baseUrl.'/message/send',
        'htmlOptions' => array(
            'class' => 'form-horizontal well',
        ),
    )); ?>
    <br />
    <div class="row-fluid">

        <label for="Message_event">Event</label><input class="span12" maxlength="250" placeholder="Event" name="event" id="Message_event" type="text" value="<?= $event; ?>">
        <label for="Message_message" class="required">Message <span class="required">*</span></label><textarea rows="6" cols="50" class="span12" placeholder="Message" name="message" id="Message_message" required><?= $message; ?></textarea>        
    </div>
    <br />

    <div class="row-fluid input_fields_wrap">
        <div class="row-fluid">
        <?php
            if(!empty($receiverFromBoard)){
            foreach ($receiverFromBoard as $boardInfo) {
                $boardMobile = $boardInfo->mobile_number;
                $boardName = $boardInfo->name;
                if(strlen($boardMobile) == 10){
                    ?>
                    <div class="span4 well" style="padding-left: 0px; margin-left: 0px; margin-right: 4px;"><input type="hidden" name="board_receiver_mobile_number_[]" value="<?= $boardMobile.'-'.$boardName; ?>"> <?= $boardName.'-'.$boardMobile; ?>&nbsp;&nbsp;&nbsp; <a href="#" class="remove_field"><i class="icon-remove-circle icon-4x"></i></a></div>
                    <?php
                }
                }
            }
            if($receiverNumber){
            for ($i=0; $i <sizeof($receiverNumber) ; $i++) { 
                $name = isset($receiverName[$i]) ? $receiverName[$i] : '';
                $mobile = isset($receiverNumber[$i]) ? $receiverNumber[$i] : '';
                if(strlen($mobile) == 10){
                    ?>
                    <div class="span4 well" style="padding-left: 0px; margin-left: 0px; margin-right: 4px;"><input type="hidden" name="general_receiver_mobile_number_[]" value="<?= $mobile.'-'.$name; ?>"> <?= $name.'-'.$mobile; ?>&nbsp;&nbsp;&nbsp; <a href="#" class="remove_field"><i class="icon-remove-circle icon-4x"></i></a></div>
                    <?php
                }
                }
            }
        ?>
        </div>
    </div>
    <br />

    <div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'COMPOSED',
        )); ?>
    </div>

    <?php $this->endWidget(); ?>
    <script type="text/javascript">

    $(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});


   /**/

</script> 