
    <div class="row-fluid form-horizontal well"> 
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
            'id'=>'message-form',
            'enableAjaxValidation'=>false,
            'method' => 'POST',
            'action' => Yii::app()->baseUrl.'/message/generateMessage',
            'htmlOptions' => array(
                'class' => 'form-horizontal well',
                'enctype' => 'multipart/form-data',
            ),
        )); ?>

        <div class="row-fluid">
            <div class="span6">
                <?php echo $form->dropDownListRow($model, 'school_type', ['community'=>'community','institutional_company'=>'institutional company','institutional_public_guthi'=>'institutional private guthi','institutional_private_guthi'=>'institutional private guthi'], array('prompt'=>'Choose School Type','multiple' => true, 'selected' => 'selected')); ?>
            </div>
            <div class="span6">

                <?php echo $form->dropDownListRow($model, 'post', ['principal'=>'principal','headteacher'=>'headteacher','chairman'=>'chairman','teacher'=>'teacher','member'=>'member','contact_person'=>'contact_person'], array('prompt'=>'Choose','multiple' => true, 'selected' => 'selected')); ?>
            </div>
        </div>
        <br />
        <div class="row-fluid input_fields_wrap">
                
                <div class="row-fluid">
                    <div class="span5"><input type="text" name="name[]" placeholder="name"></div>
                    <div class="span5"><input type="text" name="mobile[]" placeholder="mobile number" max="10" min="10"></div>
                </div>
        </div>
        <br />
        <div class="row-fluid"><button class="add_field_button btn btn-info">ADD NUMBER AND PRESON </button></div>

        <?php echo $form->textFieldRow($model,'event',array('class'=>'span12','maxlength'=>250)); ?>

        <?php echo $form->textAreaRow($model,'message',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>
        <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>'COMPOSED',
            )); ?>
        </div>

        <?php $this->endWidget(); ?>

</div>
    <script type="text/javascript">

    $(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).on("click",function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<br /><div class="row-fluid"><div class="span5"><input type="text" name="name[]" placeholder="name"></div><div class="span5"><input type="text" name="mobile[]" placeholder="mobile number" max="10" min="10"></div><div class="span2"><a href="#" class="remove_field btn btn-info btn-small">Remove</a></div></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});


   /**/

</script> 