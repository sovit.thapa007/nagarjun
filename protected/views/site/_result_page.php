
<div class="form-horizontal well">
<h2 style="text-align: center; font-weight: bold;">
    <?= strtoupper(UtilityFunctions::MunicipalLedgerHeader($academic_year)); ?> 
</h2>
<br/>
    <strong>ENTER SYMBOL NUMBER AND VIEW GRADE</strong>
    <br />
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        //'action'=>Yii::app()->createUrl($this->route),
        'method'=>'post',
        'htmlOptions'=> array(
            'class' => 'form-horizontal well',
        ),
        'type' => 'horizontal',
    )); ?>
    <br />
    <div class="row-fluid">
        <?php echo $form->textFieldRow($model,'symbol_number',array('class'=>'span12')); ?>
    </div>
    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'=>'primary',
            'label'=>'Search',
        )); ?>
    </div>


<?php $this->endWidget(); ?>
    <?php
    if(!empty($studentDetail)){
        $dob_bs_information = $studentDetail->dob_nepali ? explode('-', $studentDetail->dob_nepali) : [];
        $year = isset($dob_bs_information[0]) ? $dob_bs_information[0] : '';
        $month = isset($dob_bs_information[1]) && strlen($dob_bs_information[1]) == 2 ? $dob_bs_information[1] : '0'.$dob_bs_information[1];
        $day = isset($dob_bs_information[2]) && strlen($dob_bs_information[2]) == 2 ? $dob_bs_information[2] : '0'.$dob_bs_information[2];
    ?>
        <table class="table table-bordered">
            <tr>
                <td>SYMBOL NO. </td><td> <?= $studentDetail->symbol_number ; ?></td>
                <td>NAME. </td><td><?php echo strtoupper($studentDetail->first_name." ".$studentDetail->middle_name." ".$studentDetail->last_name); ?></td>
            </tr>
            <tr>
                <td> SHCHOOL NAME </td>
                <td> <?= $studentDetail->school_sec ? strtoupper($studentDetail->school_sec->title) : ''; ?></td>
                <td>DOB. </td><td><?= $year.'-'.$month.'-'.$day; ?></td>
            </tr>
        </table>
        <table  style="border-collapse: collapse; width: 100% !important;">

            <tr>
                <td align="center" rowspan="2" style="width: 5%; border:1px solid black;">SN</td>
                <td rowspan="2" style="width: 35% !important; border:1px solid black;">SUBJECTS</td>
                <td align="center" rowspan="2" style="width: 10%; border:1px solid black;">CREDITS <br /> HOURS</td>
                <td align="center" colspan="3" style="width: 26% !important;border:1px solid black; text-align:center">OBTAINED GRADE</td>
                <td align="center" rowspan="2" style="width: 8%; border:1px solid black;">GP</td>
                <td align="center" rowspan="2" style="width: 16%; border:1px solid black;">REMARKS</td>
            </tr>
            <tr>
                <td align="center" style="width: 7% !important;border:1px solid black;">TH</td>
                <td align="center" style="width: 7% !important;border:1px solid black;">PR</td>
                <td align="center" style="width: 12% !important;border:1px solid black;">FINAL</td>
            </tr>
            <?php
            $sn = 0;
            $total_credits_hours = $credit_section = 0;
            if(!empty($result)){
                foreach ($markInformation as $mark_info) {
                    $practical_section = $mark_info->is_practical;
                            $combine_full_marks = $mark_info -> theory_full_mark + $mark_info -> practical_full_mark;
                            $credit_section = UtilityFunctions::GradeCreditHours($combine_full_marks);
                            $th_grade_information = UtilityFunctions::GradeInformation((int) $mark_info -> theory_mark, (int) $mark_info -> theory_full_mark);
                            $th_grade = isset($th_grade_information['grade']) ? $th_grade_information['grade'] : '--';
                            if($practical_section){
                                $pr_grade_information = UtilityFunctions::GradeInformation((int) $mark_info -> practical_mark, (int) $mark_info -> practical_full_mark);
                                $pr_grade = isset($pr_grade_information['grade']) ? $pr_grade_information['grade'] : '--';
                            }
                            $final_grade = $mark_info->total_grade ?  $mark_info->total_grade :'';
                            $grade_point = $mark_info->total_grade_point ?  $mark_info->total_grade_point :'';
                            $total_gpa = isset($result->total_gpa) ? $result->total_gpa : '';
                            ?>
                            <tr>
                            <td align="center" style="border:1px solid black;"><?= $sn+1; ?></td>
                            <td style=" padding:10px;border:1px solid black;"><?= ucwords($mark_info->subject_name); ?></td>
                            <td align="center" style="border:1px solid black;"><?= $credit_section; ?></td>
                            <td align="center" style="border:1px solid black;"> <?= $th_grade; ?></td>
                            <td align="center" style="border:1px solid black;"> <?php if($practical_section && $practical_section != 0) {echo $pr_grade;} ?></td>
                            <td align="center" style="width: 8%; border:1px solid black;"><?= $final_grade; ?></td>
                            <td align="center" style="width: 8%; border:1px solid black;"><?= $grade_point; ?></td>
                            <td style="border-right: 1px solid black;"></td>
                            </tr>
                            <?php
                            $sn++;
                        }
                    }
                    ?>  
                    <tr class="testing_section">
                        <td align="center" colspan="8" style="border:1px solid black; text-align: center">GPA : <?= $total_gpa; ?></td>
                    </tr>
                </table>

         <?php
            }
         ?>       
    </div>