<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/table_style.css" rel="stylesheet">
</head>
<body>
    <div class="row-fluid">
        <h3 class="text-center">Detailed Employee's Information</h3>
    </div>
    <hr />
    <div class="row">
        <div class="span9" style="margin-left: 12%;">   
        <div class="widget stacked widget-table action-table">    
                <div class="widget-header">
                    <i class="icon-th-list"></i>
                    <h3>Gender Wise Employee's</h3>
                </div> <!-- /widget-header -->
                <div class="widget-content">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <th> Total</th>
                            <th>Males</th>
                            <th>Females</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?php echo Staff::model()->getAll(); ?></td>
                            <td><?php echo Staff::model()->getAllMaleEmployee(); ?></td>
                            <td><?php echo Staff::model()->getAllFemaleEmployee(); ?></td>

                        </tr>
                        </tbody>
                        </table>
                </div> <!-- /widget-content -->
            </div> <!-- /widget -->
        </div>

                <div class="span9" style="margin-left: 12%; margin-top:25px;   ">   
                <div class="widget stacked widget-table action-table">    
                <div class="widget-header">
                    <i class="icon-th-list"></i>
                    <h3>Department Wise Employee's</h3>
                </div> <!-- /widget-header -->
                <div class="widget-content">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <th>Total</th>
                            <th>Asst Head</th>
                            <th>Head Teacher</th>
                            <th>ECA Chief</th>
                            <th>Class Teacher</th>
                            <th>Co-ordinator</th>
                            <th>Other</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?php echo Staff::model()->getAll(); ?></td>
                            <td><?php echo DepartmentStaff::model()->getDepartment(1); ?></td>
                            <td><?php echo DepartmentStaff::model()->getDepartment(2); ?></td>
                            <td><?php echo DepartmentStaff::model()->getDepartment(3); ?></td>
                            <td><?php echo DepartmentStaff::model()->getDepartment(4); ?></td>
                            <td><?php echo DepartmentStaff::model()->getDepartment(5); ?></td>
                            <td><?php echo DepartmentStaff::model()->getDepartment(6); ?></td>
                        </tr>
                        </tbody>
                        </table>
                </div> <!-- /widget-content -->
            </div> <!-- /widget -->
        </div>
       
        <div class="span9" style="margin-left: 12%; margin-top:25px;">   
        <div class="widget stacked widget-table action-table">    
                <div class="widget-header">
                    <i class="icon-th-list"></i>
                    <h3>Special Case Wise Employee's</h3>
                </div> <!-- /widget-header -->
                <div class="widget-content">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <th> Total</th>
                            <th>Permanent Government</th>
                            <th>Temporary Government</th>
                             <th>Permanent Private</th>
                            <th>Temporary Private</th>
                             <th>Disable Mahila</th>
                            <th>Disable Purush</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?php echo Staff::model()->getAll(); ?></td>
                            <td><?php echo SpecialCaseStaff::model()->getSpecialCase(1); ?></td>
                            <td><?php echo SpecialCaseStaff::model()->getSpecialCase(2); ?></td>
                            <td><?php echo SpecialCaseStaff::model()->getSpecialCase(3); ?></td>
                            <td><?php echo SpecialCaseStaff::model()->getSpecialCase(4); ?></td>
                            <td><?php echo SpecialCaseStaff::model()->getSpecialCase(6); ?></td>
                            <td><?php echo SpecialCaseStaff::model()->getSpecialCase(7); ?></td>

                        </tr>
                        </tbody>
                        </table>
                </div> <!-- /widget-content -->
            </div> <!-- /widget -->
        </div>
    </div> 
</body>
</html>