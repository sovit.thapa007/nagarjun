
<div class="col-lg-12 col-md-12" style="margin-left: -15px; margin-right: -15px;">
  <div class="breadcrumnbs">
      <?php $this->breadcrumbs = array('Service Portal');?>
  </div>
  <h2>Service Portal</h2>

  <div>
    <?php
    $this->widget('zii.widgets.jui.CJuiTabs', array(
      'tabs' => array(
        'Tickets' => array('ajax' => $this->createUrl('/autotask/default/tickets'), 'id' => 'TicketsTab'),
        'Contacts' => array('ajax' => $this->createUrl('/autotask/default/contact'), 'id' => 'ContactsTab'),
        'Assets' => array('ajax' => $this->createUrl('/autotask/default/assets'), 'id' => 'AssetsTab'),
                // 'Project Note' => array('ajax' => $this->createUrl('/autotask/default/projectNote'), 'id' => 'ProjectNote'),
        ),
        // additional javascript options for the tabs plugin
      'options'=> [
      'collapsible' => true,
      'selected' => isset($_GET['tab']) && is_numeric($_GET['tab']) ? $_GET['tab'] : 0,
      //'load' => "js:function(){ $('#loading').hide() }",
      ]
      ));
      ?>

      <div style="position: absolute; left: 50%; top: 165px;" id="loading">
        <div style="position: relative; left: -50%;">
          Loading...
        </div>
      </div>

    </div>
  </div>
  <script>
    $(document).ready(function(){
      $('body').on('click', '.ui-tabs-nav', function(){
        //$('#loading').show();
      });
      $('body').on('click', '.jui-ajax-btn', function(){
        var title = $(this).data('title');
        var id = $(this).data('id');
        var dialogBox = $("#" + id).dialog({
          title: title,
          buttons: {
            'Close': function(){
              $(this).dialog("close");
            }
          },
          autoOpen: false,
        }).dialog("open");
        dialogBox.children('.jui-ajax-content').html('Loading please wait...');

        var url = $(this).data('href');

        $.ajax({
          url: url,
          success: function(response)  {
            dialogBox.children('.jui-ajax-content').html(response);
          }
        });

        return false;

      });
    });
  </script>
