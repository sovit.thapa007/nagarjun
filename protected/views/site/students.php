<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/table_style.css" rel="stylesheet">
</head>
<body>
    <div class="row-fluid">
        <h3 class="text-center">Detailed Student's Information</h3>
    </div>
    <hr />
    <div class="row">
        <div class="span9" style="margin-left: 12%;">   
        <div class="widget stacked widget-table action-table">    
                <div class="widget-header">
                    <i class="icon-th-list"></i>
                    <h3>Gender Wise Students</h3>
                </div> <!-- /widget-header -->
                <div class="widget-content">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <th> Total</th>
                            <th>Dropped Students</th>
                            <th>Males</th>
                            <th>Females</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?php echo RegistrationHistory::model()->getAll(); ?></td>
                            <td><?php echo RegistrationHistory::model()->getDroppedStudents(); ?></td>
                            <td><?php echo RegistrationHistory::model()->getAllMales(); ?></td>
                            <td><?php echo RegistrationHistory::model()->getAllFemales(); ?></td>
                        </tr>
                        </tbody>
                        </table>
                </div> <!-- /widget-content -->
            </div> <!-- /widget -->
        </div>

                <div class="span9" style="margin-left: 12%; margin-top:25px; ">   
                <div class="widget stacked widget-table action-table">    
                <div class="widget-header">
                    <i class="icon-th-list"></i>
                    <h3>Ethinicity Wise Student</h3>
                </div> <!-- /widget-header -->
                <div class="widget-content">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <th> Total</th>
                            <th>Brahaman/Chhetri</th>
                            <th>Tarai/Madhesi</th>
                            <th>Dalits</th>
                            <th>Janajati</th>
                            <th>Others</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?php echo RegistrationHistory::model()->getAll(); ?></td>
                            <td><?php echo RegistrationHistory::model()->getEthinic(1); ?></td>
                            <td><?php echo RegistrationHistory::model()->getEthinic(2); ?></td>
                            <td><?php echo RegistrationHistory::model()->getEthinic(3); ?></td>
                            <td><?php echo RegistrationHistory::model()->getEthinic(4); ?></td>
                            <td><?php echo RegistrationHistory::model()->getEthinic(5); ?></td>
                        </tr>
                        </tbody>
                        </table>
                </div> <!-- /widget-content -->
            </div> <!-- /widget -->
        </div>
       
                <div class="span9" style="margin-left: 12%; margin-top:25px; ">   
                <div class="widget stacked widget-table action-table">    
                <div class="widget-header">
                    <i class="icon-th-list"></i>
                    <h3>Class Wise Student</h3>
                </div> <!-- /widget-header -->
                <div class="widget-content">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <th> Total</th>
                            <th>Dropped Students</th>
                            <th>KG</th>
                            <th>Nursery</th>
                            <th>One</th>
                            <th>Two</th>
                            <th>Three</th>
                            <th>Four</th>
                            <th>Five</th>
                            <th>Six</th>
                            <th>Seven</th>
                            <th>Eight</th>
                            <th>Nine</th>
                            <th>Ten</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td><?php echo RegistrationHistory::model()->getAll(); ?></td>
                            <td><?php echo RegistrationHistory::model()->getDroppedStudents(); ?></td>
                            <td><?php echo RegistrationHistory::model()->getClasses(1); ?></td>
                            <td><?php echo RegistrationHistory::model()->getClasses(17); ?></td>
                            <td><?php echo RegistrationHistory::model()->getClasses(2); ?></td>
                            <td><?php echo RegistrationHistory::model()->getClasses(3); ?></td>
                            <td><?php echo RegistrationHistory::model()->getClasses(4); ?></td>
                            <td><?php echo RegistrationHistory::model()->getClasses(5); ?></td>
                            <td><?php echo RegistrationHistory::model()->getClasses(6); ?></td>
                            <td><?php echo RegistrationHistory::model()->getClasses(7); ?></td>
                            <td><?php echo RegistrationHistory::model()->getClasses(8); ?></td>
                            <td><?php echo RegistrationHistory::model()->getClasses(9); ?></td>
                            <td><?php echo RegistrationHistory::model()->getClasses(10); ?></td>
                            <td><?php echo RegistrationHistory::model()->getClasses(11); ?></td>
                        </tr>
                        </tbody>
                        </table>
                </div> <!-- /widget-content -->
            </div> <!-- /widget -->
        </div>
    </div> 
</body>
</html>