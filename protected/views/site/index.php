<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Dashboard</title>
     <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/style_school.css" rel="stylesheet">
     <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css" rel="stylesheet">
     <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/AdminLTE.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/skins/_all-skins.css" rel="stylesheet">
    
    <style type="text/css">
    h4{
      font-size: 26px;
    }
    </style>



  </head>
  <body>
  
  <h3 class="text-center"> Dashboard</h3>
  <hr />
    

        <!-- Main content -->
        <section>
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h4>Students</h4>
                  <br />
                  <p>Students Details</p>
                </div>
                <div class="icon">
                  <i class="ion ion-ios-people"></i>
                </div>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/site/student" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h4>Employee</h4>
                  <br />
                  <p>Employee Details</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-stalker"></i>
                </div>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/site/employee" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h4>Account</h4>
                  <br />
                  <p>Account Details</p>
                </div>
                <div class="icon">
                  <i class="ion ion-ios-calculator"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h4>Reports</h4>
                  <br />
                  <p>All reports</p>
                </div>
                <div class="icon">
                  <i class="ion ion-document-text"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        

     </body>
     </html>   