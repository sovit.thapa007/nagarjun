<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('symbol_number')); ?>:</b>
	<?php echo CHtml::encode($data->symbol_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_name')); ?>:</b>
	<?php echo CHtml::encode($data->first_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('middle_name')); ?>:</b>
	<?php echo CHtml::encode($data->middle_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_name')); ?>:</b>
	<?php echo CHtml::encode($data->last_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sex')); ?>:</b>
	<?php echo CHtml::encode($data->sex); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('father_name')); ?>:</b>
	<?php echo CHtml::encode($data->father_name); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('mother_name')); ?>:</b>
	<?php echo CHtml::encode($data->mother_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gaurdain_name')); ?>:</b>
	<?php echo CHtml::encode($data->gaurdain_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('section')); ?>:</b>
	<?php echo CHtml::encode($data->section); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('roll_number')); ?>:</b>
	<?php echo CHtml::encode($data->roll_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dob_nepali')); ?>:</b>
	<?php echo CHtml::encode($data->dob_nepali); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('don_english')); ?>:</b>
	<?php echo CHtml::encode($data->don_english); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stream')); ?>:</b>
	<?php echo CHtml::encode($data->stream); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('special_case')); ?>:</b>
	<?php echo CHtml::encode($data->special_case); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ethinicity')); ?>:</b>
	<?php echo CHtml::encode($data->ethinicity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cast')); ?>:</b>
	<?php echo CHtml::encode($data->cast); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('permanent_location')); ?>:</b>
	<?php echo CHtml::encode($data->permanent_location); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('temporary_location')); ?>:</b>
	<?php echo CHtml::encode($data->temporary_location); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contact_number')); ?>:</b>
	<?php echo CHtml::encode($data->contact_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gaurdain_occupassion')); ?>:</b>
	<?php echo CHtml::encode($data->gaurdain_occupassion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('school_id')); ?>:</b>
	<?php echo CHtml::encode($data->school_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('photo')); ?>:</b>
	<?php echo CHtml::encode($data->photo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>