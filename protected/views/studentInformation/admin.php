<?php
$this->breadcrumbs=array(
	'Create Student Informations'=>array('create'),
	'Manage',
);

$this->menu=array(
array('label'=>'Create Student Information','url'=>array('create')),
);

?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    //'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'htmlOptions'=> array(
        'class' => 'form-horizontal well',
    ),
    'type' => 'horizontal',
)); ?>
<h2><strong>SEARCH FORM SECTION </strong></h2>
<div class="row-fluid">
    <div class="span6">
        <?php echo $form->textFieldRow($model,'dob_nepali',array('class'=>'span12', 'lable'=>'DOB(BS)')); ?>
    </div>
    <div class="span6">
        <?php echo $form->textFieldRow($model,'symbol_number',array('class'=>'span12')); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span6">
	<?php echo $form->textFieldRow($model,'school_code',array('class'=>'span12', 'lable'=>'SCHOOL CODE')); ?>
	</div>
    <div class="span6">
        <?php echo $form->dropDownListRow($model,'order_by',array('symbol_number'=>'Symbol Number', 'name'=>'Name','student_id'=>'Registration Number'), array('class'=>'span12')); ?>
    </div>
</div>
    <?php 
    if(UtilityFunctions::ShowSchool()){
        echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => "title")),'id','title'),array('class'=>'span12', 'prompt'=>'CHOOSE SCHOOL')); 
    }
    ?>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type'=>'primary',
        'label'=>'Search',
    )); ?>
</div>


<?php $this->endWidget(); ?>

<div class="form-horizontal well">
	<h2><strong>STUDENT LIST</strong> 
	<a href="<?php echo Yii::app()->request->baseUrl; ?>/studentInformation/downloadpdf" style="float: right;" target="_blank"><strong>DOWNLOAD PDF</strong></a></h2>

    <?php
        echo CHtml::linkButton('DOWNLOAD EXCEL',array(
            'type'=>'submit',
            'method'=>'POST',
            'class'=>'btn btn-primary',
            'confirm'=>"Do you want to download excel file",
            'params'=> array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken)));
    ?>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'student-information-grid',
'dataProvider'=>$model->search(),
//'filter'=>$model,
'htmlOptions'=>['class'=>'bordered'],
'type'=>'stripped bordered',
'columns'=>array(
    	[
			'name'=>'school_id',
			'header'=>'School',
			'value'=>'$data->school_sec ?  strtoupper($data->school_sec->title.", ".Yii::app()->params["municipality_short"])."-".$data->school_sec->ward_no : "null"',
			'visible'=>UtilityFunctions::SuperUser(),
			'filter'=>CHtml::listData(BasicInformation::model()->findAll(),'id','title')
		],
    	[
			'name'=>'school_id',
			'header'=>'School Code',
			'value'=>'$data->school_sec ?  $data->school_sec->schole_code : "null"',
		],
		'symbol_number',
		[
			'name'=>'first_name',
			'header'=>'Name',
			'value'=>function($data){
				return strtoupper($data->first_name.' '.$data->middle_name.' '.$data->last_name);
			}
		],
		[
			'name'=>'father_name',
			'header'=>'Father',
			'value'=>function($data){
				return strtoupper($data->father_name);
			}
		],
		[
			'name'=>'dob_nepali',
			'header'=>'DOB(BS)',
			'value'=>function($data){
				return $data->dob_nepali;
			}
		],
		[
			'name'=>'don_english',
			'header'=>'DOB(AD)',
			'value'=>function($data){
				return $data->don_english;
			}
		],
array(
	'header'=>'Action',
	'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>

<?php

if(isset($_POST) && !empty($_POST))
{
    $this->widget('EExcelView', array(
     'dataProvider'=>$model->search(),
            'grid_mode'=>'export',
            'title'=>'Result Legder',
            'filename'=>'STUDENT LIST ('.date('Y-m-d').')',
            'stream'=>true,
            'autoWidth'=>false,
            'exportType'=>'Excel2007',
            'columns'=>[
        	[
				'name'=>'school_id',
				'header'=>'School Name',
				'value'=>'$data->school_sec ?  strtoupper($data->school_sec->title.", ".Yii::app()->params["municipality_short"])."-".$data->school_sec->ward_no : "null"',
			],
        	[
				'name'=>'school_id',
				'header'=>'School Code',
				'value'=>'$data->school_sec ?  $data->school_sec->schole_code : "null"',
			],
			'symbol_number',
			[
				'name'=>'first_name',
				'header'=>'Name',
				'value'=>function($data){
					return strtoupper($data->first_name.' '.$data->middle_name.' '.$data->last_name);
				}
			],
			[
				'name'=>'father_name',
				'header'=>'Father Name',
				'value'=>function($data){
					return strtoupper($data->father_name);
				}
			],
			[
				'name'=>'dob_nepali',
				'header'=>'DOB(BS)',
				'value'=>function($data){
					return $data->dob_nepali;
				}
			],
			[
				'name'=>'don_english',
				'header'=>'DOB(AD)',
				'value'=>function($data){
					return $data->don_english;
				}
			],
			//'permanent_location',
			
            ],
        ) );

}
?>

</div>