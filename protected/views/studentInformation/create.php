<?php
$this->breadcrumbs=array(
	'Student Informations'=>array('admin'),
	'Create',
);

$this->menu=array(
array('label'=>'List StudentInformation','url'=>array('index')),
array('label'=>'Manage StudentInformation','url'=>array('admin')),
);
?>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>