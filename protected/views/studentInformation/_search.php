<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'symbol_number',array('class'=>'span5','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'first_name',array('class'=>'span5','maxlength'=>100)); ?>

		<?php echo $form->textFieldRow($model,'middle_name',array('class'=>'span5','maxlength'=>100)); ?>

		<?php echo $form->textFieldRow($model,'last_name',array('class'=>'span5','maxlength'=>100)); ?>

		<?php echo $form->dropDownListRow($model,'sex',array("male"=>"male","female"=>"female","other"=>"other",),array('class'=>'input-large')); ?>

		<?php echo $form->textFieldRow($model,'father_name',array('class'=>'span5','maxlength'=>200)); ?>

		<?php echo $form->textFieldRow($model,'mother_name',array('class'=>'span5','maxlength'=>200)); ?>

		<?php echo $form->textFieldRow($model,'gaurdain_name',array('class'=>'span5','maxlength'=>200)); ?>

		<?php echo $form->textFieldRow($model,'section',array('class'=>'span5','maxlength'=>4)); ?>

		<?php echo $form->textFieldRow($model,'roll_number',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'dob_nepali',array('class'=>'span5','maxlength'=>10)); ?>

		<?php echo $form->datepickerRow($model,'don_english',array('options'=>array(),'htmlOptions'=>array('class'=>'span5')),array('prepend'=>'<i class="icon-calendar"></i>','append'=>'Click on Month/Year at top to select a different year or type in (mm/dd/yyyy).')); ?>

		<?php echo $form->dropDownListRow($model,'stream',array("english"=>"english","nepali"=>"nepali",),array('class'=>'input-large')); ?>

		<?php echo $form->textFieldRow($model,'special_case',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'ethinicity',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'cast',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'permanent_location',array('class'=>'span5','maxlength'=>250)); ?>

		<?php echo $form->textFieldRow($model,'temporary_location',array('class'=>'span5','maxlength'=>250)); ?>

		<?php echo $form->textFieldRow($model,'contact_number',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'gaurdain_occupassion',array('class'=>'span5','maxlength'=>200)); ?>

		<?php echo $form->textFieldRow($model,'school_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'photo',array('class'=>'span5','maxlength'=>200)); ?>

		<?php echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_date',array('class'=>'span5')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
</div>

<?php $this->endWidget(); ?>
