<?php
    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
  ?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'student-information-form',
	'enableAjaxValidation'=>false,
	'type' => 'horizontal',
	'enableClientValidation' => true,
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
		'enctype' => 'multipart/form-data',
	),
	'focus' => array($model, 'symbol_number'),
)); ?>
    <h2><STRONG><?= $model->isNewRecord ?  "REGISTRATION FORM" : "Update : ".ucwords($model->first_name.' '.$model->middle_name.' '.$model->last_name); ?></STRONG></h2>
	<div class="row-fluid">
    	<div class="span6">
            <?php
            if(UtilityFunctions::SuperUser()){
                echo $form->textFieldRow($model,'symbol_number',array('class'=>'span12','maxlength'=>50));
            }
            ?>
    	</div>
        <div class="span6">
            <?php
            if(UtilityFunctions::SuperUser()){
                echo $form->textFieldRow($model,'registration_number',array('class'=>'span12','maxlength'=>50));
            }
            ?>
        </div>
	</div>
    <div class="row-fluid">
        <div class="span6">
            <div class="row-fluid">
                <div class="span6" >
                  <?php echo $form->fileFieldRow($model,'photo',array('maxlength'=>200)); ?>
                </div>
                <div class="span6">
                <?php
                    if(!empty($model->photo)){
                        ?>
                        <img src="<?= Yii::app()->baseUrl; ?>/images/students_photo/<?= $model->photo; ?>" height="120" width="120">
                        <?php
                    }
                ?>
                </div>
            </div>
        </div>
        <div class="span6">
            <div class="row-fluid">
                <div class="span6">
                  <?php echo $form->fileFieldRow($model,'signature',array('maxlength'=>200)); ?>
                </div>
                <div class="span6">
                <?php
                    if(!empty($model->signature)){
                        ?>
                        <img src="<?= Yii::app()->baseUrl; ?>/images/students_photo/signature/<?= $model->signature; ?>" height="120" width="120">
                        <?php
                    }
                ?>
                </div>
                
            </div>
        </div>
    </div>
	<div class="row-fluid">
    	<div class="span2"> <strong>NAME  : </strong>
    	</div>
    	<div class="span4"> 
   			<?php echo CHtml::activeTextField($model, 'first_name', array('class'=>'span12','placeholder'=>'First Name', 'id'=>'first_name')) ?>
            <?php echo CHtml::error($model, 'first_name');?>
   		</div>
    	<div class="span3">
   			<?php echo CHtml::activeTextField($model, 'middle_name', array('class'=>'span12','placeholder'=>'Middle Name', 'id'=>'first_name')) ?>
            <?php echo CHtml::error($model, 'middle_name');?>
   		</div>
    	<div class="span3">
   			<?php echo CHtml::activeTextField($model, 'last_name', array('class'=>'span12','placeholder'=>'Last Name', 'id'=>'last_name')) ?>
            <?php echo CHtml::error($model, 'last_name');?>
   		</div>
	</div>
	<br/>
	<div class="row-fluid">
    	<div class="span2"> <strong>नाम (देवनागरिमा)  : </strong>
    	</div>
    	<div class="span4">
   			<?php echo CHtml::activeTextField($model, 'first_name_nepali', array('class'=>'span12 nepaliUnicode', 'placeholder'=>'पहिलो नाम ', 'id'=>'first_name_nepali')) ?>
            <?php echo CHtml::error($model, 'first_name_nepali');?>
   		</div>
    	<div class="span3">
   			<?php echo CHtml::activeTextField($model, 'middle_name_nepali', array('class'=>'span12 nepaliUnicode', 'placeholder'=>'बिच्को नाम ', 'id'=>'middle_name_nepali')) ?>
            <?php echo CHtml::error($model, 'middle_name_nepali');?>
   		</div>
    	<div class="span3">
   			<?php echo CHtml::activeTextField($model, 'last_name_nepali', array('class'=>'span12 nepaliUnicode', 'placeholder'=>'थर', 'id'=>'last_name_nepali')) ?>
            <?php echo CHtml::error($model, 'last_name_nepali');?>
   		</div>
	</div>
	<br />
	<div class="row-fluid">
    	<div class="span2"> <strong>Father/Mother's Name </strong>
    	</div>
    	<div class="span5">
   			<?php echo CHtml::activeTextField($model, 'father_name', array('class'=>'span12', 'placeholder'=>'Father\'s Name', 'id'=>'father_name')) ?>
    	</div>
    	<div class="span5">
   			<?php echo CHtml::activeTextField($model, 'mother_name', array('class'=>'span12', 'placeholder'=>'Mother\'s Name', 'id'=>'mother_name')) ?>
    	</div>
    </div>
    <br />
	<div class="row-fluid">
    	<div class="span3">
    		<label>SEX</label>
		<?php echo CHtml::activeDropDownList($model,'sex',array("male"=>"male","female"=>"female","other"=>"other")); ?>
    	</div>
    	<div class="span3">
    		<label>Student Number</label>
		<?php echo CHtml::activeTextField($model,'roll_number',array('class'=>'span12', 'placeholder'=>'Student Number')); ?>
    	</div>
    	<div class="span3">
    		<label>DOB(BS)</label>
    		<?php
    			$this->widget("ext.maskedInput.MaskedInput", array(
                "model" => $model,
                "attribute" => "dob_nepali",
                "mask" => "9999-99-99",
                "clientOptions"=>array("greedy"=>false, 'class'=>'span12','placeholder'=>'yyyy-mm-dd','removeMaskOnSubmit' => false)   
            ));    
    		?>
    	</div>
    	<div class="span3">
    		<label>DOB(AD)</label>
    		<?php
    			$this->widget("ext.maskedInput.MaskedInput", array(
                "model" => $model,
                "attribute" => "don_english",
                "mask" => "9999-99-99",
                "htmlOptions"=>array('class'=>'span12'),
                "clientOptions"=>array("greedy"=>false, 'placeholder'=>'yyyy-mm-dd','removeMaskOnSubmit' => false)   
            ));    
    		?>
    	</div>
    </div>
    <br /><br />
	<h2><strong> Additional Information</strong> </h4>
	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->dropDownListRow($model,'special_case',CHtml::listData(SpecialCase::model()->findAll(),'id','title'),array('class'=>'span12', 'prompt'=>'Select Espicial Case')); ?>
			<?php
			echo $form->dropDownListRow($model, 'ethinicity', CHtml::listData(EthinicGroup::model()->findAllByAttributes(['parent_id'=>'0']), 'id', 'title'), array('prompt' => 'Select Ethinicity', 'class' => 'span12', 'id' => 'ethinic_level', 'data-url' => Yii::app()->createUrl('basicInformation/ethnicityCast')), array('label' => 'Ethinicity'));
			?>
			<?php echo $form->dropDownListRow($model, 'cast', array(), ['prompt' => 'Select Cast', 'id' => 'cast_level',  'data-url' => Yii::app()->createUrl('basicInformation/previousethnicityCast'),'class' => 'span12'], ['label' => 'Cast']); ?>
		</div>
		<div class="span6">
			<?php echo $form->textFieldRow($model,'permanent_location',array('class'=>'span12','maxlength'=>250)); ?>

			<?php echo $form->textFieldRow($model,'temporary_location',array('class'=>'span12','maxlength'=>250)); ?>

			<?php echo $form->textFieldRow($model,'contact_number',array('class'=>'span12')); ?>
		</div>
	</div>
	<?php echo CHtml::hiddenField('prv_cast', $model->cast,array('class'=>'span12', 'id'=>'prv_cast','data-url' => Yii::app()->createUrl('basicInformation/previousethnicityCast'))); ?>
    <div class="row-fluid">
        <div class="span6">
        <?php
            if(!empty($model->document)){
                ?>
                <img src="<?= Yii::app()->baseUrl; ?>/images/students_photo/documents/<?= $model->document; ?>" height="120" width="120">
                <?php
            }
        ?>
          <?php echo $form->fileFieldRow($model,'document',array('maxlength'=>200)); ?>
        </div>
        <div class="span6">
            <?php echo $form->textFieldRow($model,'stream', ['nepali'=>'Nepali', 'english'=>'English', 'other'=>'Other'],array('class'=>'span12')); ?>
        </div>
    </div>
	<?php 
	if(UtilityFunctions::ShowSchool()){
			echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => "title")),'id','title'),array('class'=>'span12', 'prompt'=>'Select School', 'required'=>'required')); 
			}
			?>
	<div class="text-center form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create Student' : 'Save Information',
		)); ?>
</div>

<?php $this->endWidget(); ?>

  <!-- load jquery files -->
  <?php
  $cs->registerScriptFile($baseUrl . '/js/unicode/pramukhime.js');
  $cs->registerScriptFile($baseUrl . '/js/unicode/pramukhindic-i18n.js');
  $cs->registerScriptFile($baseUrl . '/js/unicode/pramukhindic.js');
  ?>
<script type="text/javascript">

    $(document).ready(function(){
      
    pramukhIME.addKeyboard("PramukhIndic");
    pramukhIME.enable();
    $(".nepaliUnicode").focus(function () {
    pramukhIME.setLanguage("nepali", "pramukhindic");
    });

    $(".nepaliUnicode").focusout(function () {
    pramukhIME.setLanguage("english", "pramukhime");
    });

    $('a.sidebar-toggle').click(function(){
      $('body').toggleClass('sidebar-open');
      $('body').toggleClass('sidebar-collapse');

    })
    });
    $('body').on
    (
        'change', '#ethinic_level', function() 
    	{

            var this_ = $(this);
            var selected = this_.val();
            var url = this_.data('url') + '/key/' + selected;
            if (selected.length > 0) 
            {
                $.ajax(
                {
                    type: 'POST',
                    url: url,
                    dataType: 'JSON',
                    success: function(response) 
                    {
                        if (response.success) 
                        {
                            $('#cast_level').html(response.option);
                        } 
                        else 
                        {
                            $('#cast_level').html('');
                           
                        }

                        }})
            }
        }
        );
    $(document).ready(function(){
        var this_ = $('#prv_cast');
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        if (selected.length > 0) 
        {
            $.ajax(
            {
                type: 'POST',
                url: url,
                dataType: 'JSON',
                success: function(response) 
                {
                    if (response.success) 
                    {
                        $('#cast_level').html(response.option);
                    } 
                    else 
                    {
                        $('#cast_level').html('');
                       
                    }

                    }})
        }
    });


</script>