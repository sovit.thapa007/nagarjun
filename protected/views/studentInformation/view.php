
<div class="form-horizontal well form-horizontal">
	<h2><STRONG>STUDENT INFORMATION</STRONG></h2>
	<table class="items table table-bordered" style="width:100%">
		<tbody>
			<tr>
				<td style="width: 30%">
					<img class="image" src="<?= Yii::app()->request->baseUrl.'/images/students_photo/'.$model->photo; ?>" data-zoom-image="<?= Yii::app()->request->baseUrl.'/images/students_photo/'.$model->photo; ?>" hegiht="200" width="200"></td>
				<td>
				<td>
					<table class="items table table-bordered" style="width:100%">
						<tr>
							<td> NAME (नाम)</td>
							<td> : <?= ucwords($model->first_name." ".$model->middle_name." ".$model->last_name).' ( '.$model->first_name_nepali." ".$model->middle_name_nepali." ".$model->last_name_nepali.' )'; ?></td>
						</tr>
						<tr>
							<td> Symbol Number </td>
							<td> : <?= $model->symbol_number; ?></td>
						</tr>
						<tr>
							<td> Sex </td>
							<td> : <?= ucwords($model->sex); ?></td>
						</tr>
						<tr>
							<td> Father/Mother Name </td>
							<td> : <?= ucwords($model->father_name).'/'.ucwords($model->mother_name); ?></td>
						</tr>
						<tr>
							<td> Register Class/Year</td>
							<td> : <?= ucwords($model->registered_class).'/'.$model->registered_year; ?></td>
						</tr>
						<tr>
							<td> Date Of Birth </td>
							<td> : <?= $model->dob_nepali.' BS ( '.$model->don_english.' AD)'; ?></td>
						</tr>
						<tr>
							<td> School Name </td>
							<td>: <?= isset($model->school_sec) ? $model->school_sec->title : 'null'; ?></td>
						</tr>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
	<div class="alert alert-info text-center"><STRONG>ADDITIONAL INFORMATION </STRONG></div>
	<table class="items table table-bordered" style="width:100%">
			<tr>
				<td> Special Case : <?= isset($model->special_sec) ? $model->special_sec->title : ''; ?></td>
				<td> Ethinicity/Cast : <?php
				$ethinicity = isset($model->ethinicity_sec) ? $model->ethinicity_sec->title : '';
				$cast = isset($model->cast_sec) ? $model->cast_sec->title : '';
				echo $ethinicity.'/'.$cast; ?></td>
			</tr>
			<tr>
				<td> Permanent Location : <?= $model->permanent_location; ?></td>
				<td> Temporary Location : <?= $model->temporary_location; ?></td>
			</tr>
			<tr>
				<td> Gaurdain Occupassion : <?= $model->gaurdain_occupassion; ?></td>
				<td> Contact Number : <?= $model->contact_number; ?></td>
			</tr>
			<tr>
				<td> Create By/At : <?= $model->created_by.'/'.$model->created_date; ?></td>
				<td> Change By/At : <?= $model->updated_by.'/'.$model->updated_date; ?></td>
			</tr>
			<tr>
				<td> Signature : 
					<img class="image" src="<?= Yii::app()->request->baseUrl.'/images/students_photo/Signature/'.$model->signature; ?>" data-zoom-image="<?= Yii::app()->request->baseUrl.'/images/students_photo/Signature/'.$model->signature; ?>"  hegiht="200" width="200">
				</td>
				<td> Document : 
					<img class="image" src="<?= Yii::app()->request->baseUrl.'/images/students_photo/documents/'.$model->document; ?>" data-zoom-image="<?= Yii::app()->request->baseUrl.'/images/students_photo/documents/'.$model->document; ?>" hegiht="200" width="200">
				</td>
			</tr>
	</table>
	<?php
		if($approvalBox){
			?>
			<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
				'id'=>'student-information-form',
				'enableAjaxValidation'=>false,
				'type' => 'horizontal',
				'htmlOptions' => array(
					'class' => 'form-horizontal well',
				),
			)); ?>
			<div class="alert alert-info text-center"><STRONG>STUDENT APPROVAL SECTION</STRONG></div>
			<?php echo $form->dropDownListRow($model,'state_status',['processing'=>'Processing','accepted' =>'Accepted','rejected'=>'Rejected'],array('class'=>'span12')); ?>
			<?php echo $form->textAreaRow($model, 'remarks', array('rows' => 6, 'cols' => 50, 'class' => 'span12')); ?>

					<div class="form-actions text-center">
						<?php
						$this->widget('bootstrap.widgets.TbButton', array(
							'buttonType' => 'submit',
							'type' => 'primary',
							'label' => 'SUBMIT INFORMATION',
						));
						?>
					</div>

			<?php $this->endWidget(); ?>
			<?php
		}
	?>
	<div class="button-column">
		<!-- <a href="<?php echo Yii::app()->request->baseUrl; ?>/studentInformation/printAdmitCard/<?= $model->id; ?>" class="btn btn-primary btn-xs"  target='_blank'>ADMIT CARD</a> -->
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/studentInformation/admitCardSample/<?= $model->id; ?>" class="btn btn-primary btn-xs" target='_blank'>EXAM ADMIT CARD</a>
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/result/gradeSheet/?student_id=<?= $model->id?>&&type=original" class="btn btn-primary btn-xs" target='_blank' title="PRINT ORIGINAL GARDE SHEET" data-toggle="tooltip">ORIGINAL GARDE SHEET</a>
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/result/gradeSheet/?student_id=<?= $model->id?>&&type=copy" class="btn btn-primary btn-xs" target='_blank' title="PRINT ORIGINAL GARDE SHEET" data-toggle="tooltip">COPY GARDE SHEET</a>
	</div>


 
