<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'subject-information-form',
	'enableAjaxValidation'=>false,
	'type' => 'horizontal',
	'enableClientValidation' => true,
	'htmlOptions' => array(
		'class' => 'form-horizontal well form-horizontal',
		'enctype' => 'multipart/form-data',
		'target' => '_blank',
	),
	'focus' => array($model, 'title'),
)); ?>
<h2><strong>SEARCH FORM FOR REGISTRATION REPORT</strong></h2>
	<div class="row-fluid">
		<div class="span6">
		<?php echo $form->dropDownListRow($model,'order_by',array('first_name'=>'Name','registration_number'=>'Registration Number', 'symbol_number'=>'Symbol Number'),['prompt'=>' ORDER BY']); ?></div>
		<div class="span6">
		<?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span12','maxlength'=>4)); ?>
		</div>
	</div>
	<?php echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => "title")),'id','title'),array('class'=>'span12', 'required'=>'required')); ?>
	<div class="form-actions text-center">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'SEARCH REPORT',
		)); ?>
	</div>

<?php $this->endWidget(); ?>