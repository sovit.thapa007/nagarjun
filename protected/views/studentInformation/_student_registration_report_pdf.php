
<div class="row-fulid" style="overflow: auto;">
    <table class="table table-bordered" style="border-collapse: collapse; width: 100%;" >
        <thead>
            <tr>
                <th colspan="10" style="text-align: center; "> <?= strtoupper(UtilityFunctions::MunicipalLedgerHeader()); ?>
                </th>
            </tr>
            <tr><th colspan="10"><hr /></th></tr>
            <tr>
                <td colspan="8" style="font-weight: bold;">School : <?= $school_information ? strtoupper($school_information->title).', '.strtoupper(Yii::app()->params['municipality_short']).'-'.$school_information->ward_no : ''; ?></td>
                <td colspan="2" style="font-weight: bold;">Code : <?= $school_information ?$school_information->schole_code : ''; ?></td>
            </tr>
            <tr>
                <td colspan="10" style="font-weight: bold; text-align: center;">Resource Center : <?= $resource_information ? strtoupper($resource_information->title. ', '.Yii::app()->params['municipality_short']).'-'.$resource_information->ward_no : ''; ?></td>
            </tr>
            <tr>
                <td style="border:2px solid black;background: #0E2A47; color: white">SN</td>
                <td style="border:2px solid black;background: #0E2A47; color: white">Symbol_No.</td>
                <td style="border:2px solid black;background: #0E2A47; color: white">&nbsp;&nbsp;StudentName&nbsp;&nbsp;</td>
                <td style="border:2px solid black;background: #0E2A47; color: white">&nbsp;&nbsp;Sex&nbsp;&nbsp;</td>
                <td style="border:2px solid black;background: #0E2A47; color: white">Father_Name</td>
                <td style="border:2px solid black;background: #0E2A47; color: white">Mother_Name</td>
                <td style="border:2px solid black;background: #0E2A47; color: white">DOB(BS)</td>
                <td style="border:2px solid black;background: #0E2A47; color: white">DOB(AD)</td>
                <td style="border:2px solid black;background: #0E2A47; color: white">Opt._Subject</td>
                <td style="border:2px solid black;background: #0E2A47; color: white">Remarks</td>
            </tr>
        </thead>
        <tbody>

        <?php
            if(!empty($student_information)){
                $sn = 1;
                foreach ($student_information as $student) {
                ?>
                <tr>
                    <td style="border:2px solid black; font-size: 7.5pt;"><?= $sn; ?></td>
                    <td style="border:2px solid black; font-size: 7.5pt;"><?= $student->symbol_number; ?></td>
                    <td style="border:2px solid black; font-size: 7.5pt;"><?= strtoupper($student->first_name.' '.$student->middle_name.' '.$student->last_name) ?></td>
                    <td style="border:2px solid black; font-size: 7.5pt;"><?= strtoupper($student->sex); ?></td>
                    <td style="border:2px solid black; font-size: 7.5pt;"><?= strtoupper($student->father_name) ?></td>
                    <td style="border:2px solid black; font-size: 7.5pt;"><?= strtoupper($student->mother_name) ?></td>
                    <td style="border:2px solid black; font-size: 7.5pt;"><?= $student->dob_nepali; ?></td>
                    <td style="border:2px solid black; font-size: 7.5pt;"><?= $student->don_english;?></td>
                    <td style="border:2px solid black; font-size: 7.5pt;"><?= strtoupper($student->optional_subject) ?> </td>
                    <td style="border:2px solid black;"></td>
                </tr>
                <?php
                    $sn++;
                }
            }
        ?>
       </tbody>
    </table>
</div>
