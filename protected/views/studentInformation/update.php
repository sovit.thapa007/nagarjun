<?php
$this->breadcrumbs=array(
	'Student Informations'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List StudentInformation','url'=>array('index')),
array('label'=>'Create StudentInformation','url'=>array('create')),
array('label'=>'View StudentInformation','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage StudentInformation','url'=>array('admin')),
);
?>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>