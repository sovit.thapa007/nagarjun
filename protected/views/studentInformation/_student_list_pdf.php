
<div class="row-fulid" style="overflow: auto;">
    <table class="display items table" style="border-collapse: collapse;" >
        <thead>
            <tr>
                <th colspan="9" style="text-align: center; "> <?= strtoupper(UtilityFunctions::MunicipalLedgerHeader()); ?>
                </th>
            </tr>
            <tr><th colspan="9"><hr /></th></tr>
            <tr>
                <td colspan="4" style="font-weight: bold;">SCHOOL NAME : <?= $school_information ? $school_information->title : ''; ?></td>
                <td colspan="2" style="font-weight: bold;">SCHOOL TYPE : <?= $school_information ? $school_information->type : ''; ?></td>
                <td colspan="2" style="font-weight: bold;">SCHOOL CODE. : <?= $school_information ? $school_information->schole_code : ''; ?></td>
                <td colspan="1" style="font-weight: bold;">Address : <?= $school_information ? $school_information->tole : ''; ?></td>
            </tr>
            <tr>
                <td style="border:2px solid black;background: #0E2A47; color: white">SN</td>
                <td style="border:2px solid black;background: #0E2A47; color: white">&nbsp;&nbsp;StudentName&nbsp;&nbsp;</td>
                <td style="border:2px solid black;background: #0E2A47; color: white">Registration_No.</td>
                <td style="border:2px solid black;background: #0E2A47; color: white">Symbol_No.</td>
                <td style="border:2px solid black;background: #0E2A47; color: white">Father_Name</td>
                <td style="border:2px solid black;background: #0E2A47; color: white">Mother_Name</td>
                <td style="border:2px solid black;background: #0E2A47; color: white">DOB</td>
                <td style="border:2px solid black;background: #0E2A47; color: white">Opt._Subject</td>
                <td style="border:2px solid black;background: #0E2A47; color: white">Remarks</td>
            </tr>
        </thead>
        <tbody>

        <?php
            if(!empty($student_information)){
                $sn = 1;
                foreach ($student_information as $student) {
                ?>
                <tr>
                    <td style="border:2px solid black;"><?= $sn; ?></td>
                    <td style="border:2px solid black;"><?= strtoupper($student->first_name.' '.$student->middle_name.' '.$student->last_name) ?></td>
                    <td style="border:2px solid black;"><?= $student->registration_number; ?></td>
                    <td style="border:2px solid black;"><?= $student->symbol_number; ?></td>
                    <td style="border:2px solid black;"><?= strtoupper($student->father_name) ?></td>
                    <td style="border:2px solid black;"><?= strtoupper($student->mother_name) ?></td>
                    <td style="border:2px solid black;"><?= $student->dob_nepali.' B.S ('.$student->don_english.' A.D)' ?></td>
                    <td style="border:2px solid black;"> </td>
                    <td style="border:2px solid black;"> </td>
                </tr>
                
                <?php
                    $sn++;
                }
            }
        ?>
       </tbody>
    </table>
</div>
