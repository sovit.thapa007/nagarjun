<?php
$this->breadcrumbs=array(
	'Create Student Informations'=>array('create'),
	'Manage',
);

$this->menu=array(
array('label'=>'Create Student Information','url'=>array('create')),
);

?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    //'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'htmlOptions'=> array(
        'class' => 'form-horizontal well',
    ),
    'type' => 'horizontal',
)); ?>
<h2><strong>SEARCH FORM SECTION </strong></h2>
<div class="row-fluid">
    <div class="span6">
        <?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span12')); ?>
    </div>
    <div class="span6">
        <?php echo $form->dropDownListRow($model,'order_by',array('student_id'=>'Registration Number','name'=>'Name', 'symbol_number'=>'Symbol Number'), array('class'=>'span12')); ?>
    </div>

</div>

<div class="row-fluid">
    <div class="span6">
        <?php echo $form->dropDownListRow($model,'sex',array('male'=>'Male','female'=>'Female'), array('prompt'=>'CHOOSE SEX','class'=>'span12')); ?>
    </div>
    <div class="span6">
        <?php echo $form->textFieldRow($model,'symbol_number',array('class'=>'span12')); ?>
    </div>
</div>
<div class="row-fluid">
    <?php 
    if(UtilityFunctions::ShowSchool()){
        echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(),'id','title'),array('class'=>'span12', 'prompt'=>'CHOOSE SCHOOL')); 
    }
    ?>
</div>
<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type'=>'primary',
        'label'=>'Search',
    )); ?>
</div>


<?php $this->endWidget(); ?>

<div class="form-horizontal well">
	<h2><strong>STUDENT LIST</strong></h2>

    <?php
        echo CHtml::linkButton('DOWNLOAD EXCEL',array(
            'type'=>'submit',
            'method'=>'POST',
            'class'=>'btn btn-primary',
            'confirm'=>"Do you want to download excel file",
            'params'=> array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken)));
    ?>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'student-information-grid',
'dataProvider'=>$model->allsearch(),
'filter'=>$model,
'htmlOptions'=>['class'=>'bordered'],
'type'=>'stripped bordered',
'columns'=>array(
		'academic_year',
		'symbol_number',
		[
			'name'=>'first_name',
			'header'=>'Name',
			'value'=>function($data){
				return strtoupper($data->first_name.' '.$data->middle_name.' '.$data->last_name);
			}
		],

		[
			'name'=>'sex',
			'value'=>function($data){
				return strtoupper($data->sex);
			}
		],

		//'father_name',
		//'mother_name',
		'dob_nepali',
		
		[
			'name'=>'school_id',

			'value'=>'$data->school_sec ?  strtoupper($data->school_sec->title) : "null"',
			'visible'=>UtilityFunctions::ShowSchool(),
			'filter'=>CHtml::listData(BasicInformation::model()->findAll(),'id','title')
		],
array(
	'header'=>'Action',
	'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>

<?php


if(isset($_POST) && !empty($_POST))
{
    $this->widget('EExcelView', array(
     'dataProvider'=>$model->search(),
            'grid_mode'=>'export',
            'title'=>'Result Legder',
            'filename'=>'STUDENT LIST ('.date('Y-m-d').')',
            'stream'=>true,
            'autoWidth'=>false,
            'exportType'=>'Excel2007',
            'columns'=>[
				'academic_year',
				'symbol_number',
				[
					'name'=>'first_name',
					'header'=>'Name',
					'value'=>function($data){
						return strtoupper($data->first_name.' '.$data->middle_name.' '.$data->last_name);
					}
				],

				[
					'name'=>'sex',
					'value'=>function($data){
						return strtoupper($data->sex);
					}
				],

				//'father_name',
				//'mother_name',
				'dob_nepali',
				
				[
					'name'=>'school_id',

					'value'=>'$data->school_sec ?  strtoupper($data->school_sec->title) : "null"',
					'visible'=>UtilityFunctions::ShowSchool(),
					'filter'=>CHtml::listData(BasicInformation::model()->findAll(),'id','title')
				],
            ],
        ) );

}
?>

</div>