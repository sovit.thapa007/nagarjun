	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'subject-information-form',
		'enableAjaxValidation'=>false,
		'type' => 'horizontal',
		'enableClientValidation' => true,
		'htmlOptions' => array(
			'class' => 'form-horizontal well form-horizontal',
			'enctype' => 'multipart/form-data',
		),
		'focus' => array($model, 'title'),
	)); ?>
	<h2><strong>UPLOAD EXCEL FILE OF STUDENTS LIST <a href="<?= Yii::app()->request->baseUrl; ?>/studentInformation/sampleList" target='_blank' style='float: right;'>DOWNLOAD FORMATE </a></strong></h2>
	<div class="row-fluid">
		<div class="span6">
		<?php echo $form->fileFieldRow($model,'upload_files',array('class'=>'span12','maxlength'=>200)); ?></div>
		<div class="span6">
		<?php echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => "title")),'id','title'),array('class'=>'', 'prompt'=>'Select School', 'selected'=>1),array('class'=>'span12')); ?></div>
		</div>
		<div class="form-actions text-center">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>'IMPORT DATA',
			)); ?>
	</div>

<?php $this->endWidget(); ?>
<?php
    if(!empty($excel_array_data) && !empty($school_information)){
    	?>
    	<div class="form-horizontal well form-horizontal" style="overflow-y: scroll;">
    		<h2><STRONG>REVIEW STUDENT LEDGER MARKS</STRONG></h2>
    	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
			'action' => Yii::app()->request->baseUrl.'/StudentInformation/insertStudentList',
			'htmlOptions' => array(
				'class' => 'form-horizontal well form-horizontal',
				'enctype' => 'multipart/form-data',
			),
			'focus' => array($model, 'title'),
		)); ?>
		    <table class="table table-hover table-bordered">
		        <thead>
		            <th>SN</th>
		            <?php
	        		for ($i=0; $i <  sizeof($column); $i++) { 
		            	?>
		            	<th><?= $column[$i]; ?></th>
		            	<?php
		            }
		            ?>
		        </thead>
		        <?php
		        for ($j=0; $j < sizeof($excel_array_data) ; $j++) { 
		        	?>
		        	<tr>
		        		<td><?= $j+1; ?></td>
		        		<?php
		        		for ($k=0; $k <  sizeof($column); $k++) { 
		        			$index = $column[$k];
		        			$symbol_number = isset($excel_array_data[$j]['symbol_number']) ? $excel_array_data[$j]['symbol_number'] : null;
		        			$registration_number = isset($excel_array_data[$j]['registration_number']) ? $excel_array_data[$j]['registration_number'] : null;
		        			if($index=='symbol_number'){
		        					echo "<input type='hidden' name='symbol_number[]' value='".$symbol_number."' >";
		        				?>
		        				<td><?= isset($excel_array_data[$j][$index]) ? $excel_array_data[$j][$index] : ''; ?></td>
		        				<?php
		        			}else{
		        			?>
		        			<td>
		        				<input type="text" name="<?= $symbol_number.'_'.$index; ?>" value="<?= isset($excel_array_data[$j][$index]) ? $excel_array_data[$j][$index] : ''; ?>">
		        			</td>
		        			<?php

		        			}
		        		}
		        		?>
		        	</tr>
		        	<?php
		        }
		        ?>
		    </table>
		    <input type="hidden" name="school_id" value="<?= $school_id; ?>">
		    <div class="form-actions text-center">
			<button class="btn btn-primary" id="yw0" type="submit" name="yt0">SUBMIT MARKS</button>	</div>

		<?php $this->endWidget(); ?>
	</div>
<?php
	}
?>