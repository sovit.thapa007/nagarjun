<div class="row-fluid">
	<div style="width:10%; float: left; ">
		<img src="<?= Yii::app()->request->baseUrl.'/images/logo/ministry_education.jpg'; ?>" width="100" height="80" />
	</div>
	<div style='width: 90%; font-size:15px; font-weight:bold; text-align: center; float: right;'>
		<div> <?= ucwords($municipality);?></div><div> Office Of Municipal Excutive</div><div ><?= ucwords($address);?></div>
	</div>
	<br />
	<div style='font-size:17px; font-weight:bold; margin-top: -1em;'>
		MUNICIPALITY LEVEL EXAMINATION REGISTRATION/APPLICATION FORM
	</div>

	<table style="width:100%; border-collapse: collapse;">
		<tr>
			<td style="width: 30%">Symbol No. : <?= $student_information->symbol_number; ?> <br />
				To, <br />
				Chief Administrative Officer <br />

				<?= ucwords($municipality); ?> <br />
				<?= ucwords($address); ?> <br />
			</td>
			<td style="width: 40%;">
				<table class="table" style="width:100%; border-collapse: collapse;">
					<tr>
						<td colspan="10" style="text-align: center;">STUDENT COPY</td></tr>
					<tr style="border: 1px solid #000;">
						<td colspan="10" style="text-align: center;">Registration No. (10 digits)</td>
					</tr>
					<tr style="border: 1px solid #000;">
						<td colspan="2" style="border: 1px solid #000;"> Year
						</td>
						<td colspan="2" style="border: 1px solid #000;"> Ward No.
						</td>
						<td colspan="3" style="border: 1px solid #000;"> School Code
						</td>
						<td colspan="3" style="border: 1px solid #000;"> Student Number
						</td>
					</tr>
					<tr style="border: 1px solid #000;">
						<td style="border: 1px solid #000;"><?= $year_digit_1; ?></td>
						<td style="border: 1px solid #000;"><?= $year_digit_2; ?></td>
						<td style="border: 1px solid #000;"><?= $ward_digit_1;?></td>
						<td style="border: 1px solid #000;"><?= $ward_digit_2; ?></td>
						<td style="border: 1px solid #000;"><?= $school_digit_1; ?></td>
						<td style="border: 1px solid #000;"><?= $school_digit_2; ?></td>
						<td style="border: 1px solid #000;"><?= $school_digit_3; ?></td>
						<td style="border: 1px solid #000;"><?= $student_digit_1; ?></td>
						<td style="border: 1px solid #000;"><?= $student_digit_2; ?></td>
						<td style="border: 1px solid #000;"><?= $student_digit_3; ?></td>
					</tr>
				</table>
			</td>
			<td height="100" width="100" style="border: 1px solid #000;"></td>
		</tr>
	</table>
	<div style="text-align: justify-all;">
		I hearby submit this registration/application form with the following details for the Municipality Level Examination <?= $student_information->academic_year.' B.S.' ?> on condition that I will accept if this application is rejected as per Municipality Office rules and regulation.
	</div>
	<table style="width:100%">
		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 30% !important;">Student Name <br /> (IN BLOCK LETTER)</td>
			<td style="border: 1px solid #000; width: 70% !important;"><?= strtoupper($student_information->first_name.' '.$student_information->middle_name.' '.$student_information->last_name) ?></td>
		</tr>

		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 30% !important;">Nepali Name</td>
			<td style="border: 1px solid #000; width: 70% !important;"><?= $student_information->first_name_nepali.' '.$student_information->middle_name_nepali.' '.$student_information->last_name_nepali ?></td>
		</tr>

		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 30% !important;">Father/Mother's Name</td>
			<td style="border: 1px solid #000; width: 70% !important;"><?= $student_information->father_name ? ucwords($student_information->father_name) : ucwords($student_information->mother_name); ?></td>
		</tr>

		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 30% !important;">Student's Date Of Birth</td>
			<td style="border: 1px solid #000; width: 70% !important;"><?= $student_information->dob_nepali.' (B.S.) &nbsp;&nbsp;&nbsp; '.$student_information->don_english.' (A.D.)'; ?></td>
		</tr>
		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 30% !important;">Type Of School</td>
			<td style="border: 1px solid #000; width: 70% !important;"><?= $student_information->school_sec ? ucwords($student_information->school_sec->type) : 'Other'; ?></td>
		</tr>
	</table>
	SUBJECT TO BE TAKEN IN THE EXAMINATION
	<table style="width:100%; border-collapse: collapse;">
		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 5% !important;">S.N.</td>
			<td style="border: 1px solid #000; width: 45% !important;">Subject</td>
			<td style="border: 1px solid #000; width: 5% !important;">S.N.</td>
			<td style="border: 1px solid #000; width: 45% !important;">Subject</td>
		</tr>
		<?php
			for ($i=0; $i < 5 ; $i++) {
				$newI = 5 + $i;
		?>
		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 5% !important;"><?= $i+1; ?>.</td>
			<td style="border: 1px solid #000; width: 45% !important;"><?= isset($subjectArray[$i]) ? ucwords($subjectArray[$i]) : ''; ?></td>
			<td style="border: 1px solid #000; width: 5% !important;"><?= $i+6; ?>.</td>
			<td style="border: 1px solid #000; width: 45% !important;"><?= isset($subjectArray[$newI]) ? ucwords($subjectArray[$newI]) : ''; ?></td>
		</tr>
		<?php
			}
		?>
	</table>
	<br />
	<table  style="width:100%; border-collapse: collapse;">
		<tr>
			<td style="width: 33%; text-align: center;"> --------------------------------------------</td>
			<td style="width: 33%; text-align: center;"> --------------------------------------------</td>
			<td style="width: 33%; text-align: center;"> ---------------------------------------------</td>
		</tr>
		<tr>
			<td style="text-align: center;">Student's Signature</td>
			<td style="text-align: center;">Head Tearcher's Signature <br />
				<p style="border: 1px solid #000;">Stamp Of School</p>
			</td>
			<td style="text-align: center;">Chief Administrative Officer<br />
				<p style="border: 1px solid #000;">Municipality Stamp</p>
			</td>
		</tr>
	</table>
	---------------------------------------------------------------------------------------------------------------------------------------------------------
</div>
