<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin-card-print.css">
  <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.js"></script>
    <div class="admit-card-wrapper">
        <div class="admint-card-header text-center">
            <h1><?= strtoupper(Yii::app()->params['municipality']);?></h1>
            <h2>OFFICE OF MUNICIPAL EXCUTIVE <br/>
            <?= strtoupper(Yii::app()->params['address']);?></h2>
            <h3>MUNICIPALITY LEVEL EXAMINATION REGISTRATION/APPLICATION FORM<br/>
            <strong>EXAM ADMIT CARD - <?= $academicYear; ?></strong></h3>
        </div>
        <div class="admit-card-body">
            <div class="admit-card-content">
                <ul style="margin-bottom: 5px;">
                    <li>Symbol Number : <span><?= $student_information->symbol_number; ?></span></li>
                    <li>Registration Number : <span><?= $student_information->registration_number; ?></span></li>
                    <li>Name of Student : <span><?= strtoupper($student_information->first_name.' '.$student_information->middle_name.' '.$student_information->last_name) ?></span></li>
                    <li>Name of the Education Institution : <span><?= $student_information->school_sec ? strtoupper($student_information->school_sec->title) : '' ?></span></li>
                </ul>

                <table style="width:100%; border-collapse: collapse;">
                    <tr style="border: 1px solid #000;">
                        <td style="border: 1px solid #000; width: 5% !important;">S.N.</td>
                        <td style="border: 1px solid #000; width: 45% !important;">Subject</td>
                        <td style="border: 1px solid #000; width: 5% !important;">S.N.</td>
                        <td style="border: 1px solid #000; width: 45% !important;">Subject</td>
                    </tr>
                    <?php
                        for ($i=0; $i < 5 ; $i++) {
                            $newI = 5 + $i;
                    ?>
                    <tr style="border: 1px solid #000;">
                        <td style="border: 1px solid #000; width: 5% !important;"><?= $i+1; ?>.</td>
                        <td style="border: 1px solid #000; width: 45% !important;"><?= isset($subjectArray[$i]) ? ucwords($subjectArray[$i]) : ''; ?></td>
                        <td style="border: 1px solid #000; width: 5% !important;"><?= $i+6; ?>.</td>
                        <td style="border: 1px solid #000; width: 45% !important;"><?= isset($subjectArray[$newI]) ? ucwords($subjectArray[$newI]) : ''; ?></td>
                    </tr>
                    <?php
                        }
                    ?>
                </table>

            </div>
            <div class="admin-card-photo" style="border: 2px solid #000; height: 80px; width: 100px;">
                <!-- <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/students_photo/img_avatar.png" alt="Student Name"> -->
            </div>
        </div>
        <div class="signature-wrapper">
            <div class="first-signature">
                <div class="signature-box">&nbsp</div>
                <div> Student Signature</div> 
            </div>
            <div class="second-signature">
                <div class="signature-box">&nbsp</div>
                <div>Head Tearcher's Signaturer<br />Stamp Of School</div>
            </div>
            <div class="third-signature">
                <div class="signature-box">&nbsp</div>
                <div>  Chief Administrative Officer<br />
                Municipality Stamp</div>
            </div>
        </div>
        <div class="exam-rules">
        <strong>परिक्षार्थिहरूलाई निर्देशन : </strong>
        <ol style="margin-top: 4px; margin-bottom: 0px; ">
            <li> परिक्षा सुरु हुने पहिलो दिन परिक्षा सुरु हुनु भन्दा आधा घण्टा अगाडि र दोस्रो दिनबाट १५ मिनट पहिले मात्र परिक्षा भवन खुल्ने छ । </li>
            <li> परिक्षा सुरु भएको १ घण्टा भन्दा ढिला आउने परिक्षार्थिलाई परिक्षा हलमा प्रवेश गराईने छैन ।</li>
            <li> केन्द्राधक्षको अनुमति बिना आफ्नो स्थान वा परिक्षा भवन छाडेर बाहिर जान पाईने छैन । </li>
            <li> परिक्षा भवन प्रवेश गर्दा आफ्नो परिक्षा प्रवेश पत्र अनिवार्य रुपमा लिएर आउन पर्ने र केन्द्राधक्ष वानिरिक्षकले मागेको बेलामा देखाउन पर्नेछ । </li>

            <li> परिक्षा भवन भित्र कुनै किसिमका पुस्तक, नोटबुक, गाइड, चिट, मोबाइल, हातहतियार, अबैध प्रदार्थ वाहरु निषेध गरिएका सर सामान परिक्षा हलमा प्रवेश गराए वा गराउने प्रयत्न गरेको पाइएमा त्यस्ता बिद्यार्थीलाई परिक्षाहलबाट निस्कासन गरो परिक्षा समेत रध्द गरिनेछ । </li>
            <li> परिक्षार्थीले परिक्षासुरु भई प्रश्न पत्र पाएको एक घण्टा नभइकन परिक्षाहल छाड्न पाउने छैनन् । </li>
            <li> परिक्षार्थीले परिक्षाको उत्तर पुस्तिका बुझाइ परिक्षाहलबाट बाहिरिए पछि निजलाई कुनै पनि हालतमा पुन: परिक्षाहल प्रवेश गराई परिक्षा दिन दिईने छैन । </li>
            <li> केन्द्राधक्षले तोकेको निर्देशन पालना गरि निजले तोकेको स्थानमा बसि परिक्षा दिन पर्ने छ । केन्द्राधक्ष वानिजले तोकेको ब्यक्तिले जुन सुकै समयमा परिक्षा हलमा खानतलासी गर्न सक्नेछ । </li>
            <li> परिक्षार्थीले एक बिषयमा फारम भरि अर्को बिषय बिषयमा परिक्षा दिएको वा फारम नभरेको बिषयमा
            परिक्षा दिएमा स्तह परिक्षाफल रध्द गरिनेछ । </li>
            <li> परिक्षार्थीले परिक्षाको मर्यादा भंगहुने , परिक्षा खलल पुग्ने आदि अमर्यादित काम गरेमा, एक परिक्षार्थीकोसट्टा हर्को परिक्षार्थी भई परिक्षा दिएमा निरिक्षकलाई उत्तर पुस्तिका नबुझाई बाहिरिएमा त्यस्तापरिक्षार्थीको परिक्षा फल स्वत: रद्ध हुनेछ । </li>
            <li> परिक्षामा आफ्नो सिम्बोल नम्बेरबाहेक अन्य अरु कुनै कोड वा चिनारी छाडेमा त्यस्ता परिक्षार्थीको परिक्षा फल प्रकाशन गरिने छैन ।</li>
        </ol>
        </div>
    </div>

    <script type="text/javascript">
        
    $(document).ready(function() {
        window.print();
    } );
    </script>