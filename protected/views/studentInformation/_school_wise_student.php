<script type="text/javascript">


	$(document).ready(function() {
  	$('#example thead th').each( function () {
  		var title = $('#example tfoot th').eq( $(this).index() ).text();
    	$(this).html( '<input class="span12" type="text" placeholder="'+title+'" />' );
    } );
    $('#example').DataTable( {
    	lengthMenu: [
            [ 25, 40, 60, -1 ],
            [ '25 rows', '40 rows', '50 rows', 'Show all' ]
        ],
          "ajax": {
		    "url": "<?= Yii::app()->baseUrl; ?>/studentInformation/schoolReport",
		  },
        dom: 'Bfrtip',
        buttons: [
            'pageLength','copy', 'csv', 'excel', 'pdf', 'print'
        ],

        //"order": [[ 7, 'asc' ]]
    } );

	var table = $('#example').DataTable();
    // Apply the search
	table.columns().eq(0).each(function(colIdx) {
	    $('input', table.column(colIdx).header()).on('keyup change', function() {
	        table
	            .column(colIdx)
	            .search(this.value)
	            .draw();
	    });
	 
	    $('input', table.column(colIdx).header()).on('click', function(e) {
	        e.stopPropagation();
	    });
	});

    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
} );
</script>
<style type="text/css">
    
.dataTables_wrapper .dataTables_filter {
float: right;
text-align: right;
visibility: hidden;
}
tfoot{
    visibility: hidden;
}
</style>


<ul class="breadcrumb" style="float: right;">
  <li><a href="<?= Yii::app()->baseUrl.'/dashboard'; ?>">Home</a> / </li>
  <li class="active">Users List's</li>
</ul>
<H3 class="text-center"><strong>SCHOOL WISE STUDENT FOR EXAM REGISTRATION</strong></H3>
<div class="row-fluid">
<table id="example" class="items table table-bordered table-striped" style="width:100%">
        <thead>
            <tr>
                <th></th>
                <th>SCHOOL CODE</th>
                <th>SCHOOL</th>
                <th>TOTAL STUDENT</th>
            </tr>
        </thead>

        <tfoot>
            <tr>
                <th></th>
                <th>SCHOOL CODE</th>
                <th>SCHOOL</th>
                <th>TOTAL STUDENT</th>
            </tr>
        </tfoot>
    </table>
</div>

