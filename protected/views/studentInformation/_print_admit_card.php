<style type="text/css">

@media print {
   footer, #print_button{
   	display: none;
   }
   #sinlgeSection{
   	page-break-inside: avoid;
   }
}

table, td{
	font-size: 10pt;
	padding:0px 0px 0px 5px !important;
}
</style>

<div class="row-fluid" id="sinlgeSection">
	<br /><br /> <br />
	<div style="width: 12%; height: 800px; float: left;"> </div>
	<div style="width: 85%; float: right;">
	<div style="width:10%; float: left; ">
		<img src="<?= Yii::app()->request->baseUrl.'/images/logo/ministry_education.jpg'; ?>" width="100" height="80" />
	</div>
	<div style='width: 90%; font-size:15px; font-weight:bold; text-align: center; float: right;'>
		<div> Nagarjun Municipality</div><div> Office Of Municipal Excutive</div><div > Harisiddhi, Sitapaila, Kathmandu</div>
	</div>
	<br />
	<div style='font-size:14px; font-weight:bold; margin-top: -1em;'>
		MUNICIPALITY LEVEL EXAMINATION REGISTRATION/APPLICATION FORM
	</div>

	<table style="width:100%; border-collapse: collapse;">
		<tr>
			<td style="width: 30%">Symbol No. : <?= $student_information->symbol_number; ?> <br />
				To, <br />
				Chief Administrative Officer <br />

				<?= ucwords($municipality); ?> <br />
				<?= ucwords($address); ?> 
			</td>
			<td style="width: 50%;">
				<table class="table" style="width:100%; border-collapse: collapse;margin-bottom:0px;">
					<tr>
						<td colspan="10" style="text-align: center;">OFFICE COPY</td></tr>
					<tr >
						<td colspan="10" style="text-align: center;">Registration No. (10 digits)</td>
					</tr>
					<tr style="border: 1px solid #000;">
						<td colspan="2" style="border: 1px solid #000; width: 20%; font-size: 10pt;"> Year
						</td>
						<td colspan="2" style="border: 1px solid #000; width: 25%; font-size: 10pt;"> Ward No.
						</td>
						<td colspan="3" style="border: 1px solid #000; width: 25%; font-size: 10pt;"> School Code
						</td>
						<td colspan="3" style="border: 1px solid #000; width: 30%; font-size: 10pt;"> Student Number
						</td>
					</tr>
					<tr style="border: 1px solid #000;">
						<td style="border: 1px solid #000;"><?= $year_digit_1; ?></td>
						<td style="border: 1px solid #000;"><?= $year_digit_2; ?></td>
						<td style="border: 1px solid #000;"><?= $ward_digit_1;?></td>
						<td style="border: 1px solid #000;"><?= $ward_digit_2; ?></td>
						<td style="border: 1px solid #000;"><?= $school_digit_1; ?></td>
						<td style="border: 1px solid #000;"><?= $school_digit_2; ?></td>
						<td style="border: 1px solid #000;"><?= $school_digit_3; ?></td>
						<td style="border: 1px solid #000;"><?= $student_digit_1; ?></td>
						<td style="border: 1px solid #000;"><?= $student_digit_2; ?></td>
						<td style="border: 1px solid #000; padding:0px !important;"><?= $student_digit_3; ?></td>
					</tr>
				</table>
			</td>
			<td> <p style="height: 100px; width: 100px !important; float: right; border: 1px solid #000;width: 20%;"></p> </td>
		</tr>
	</table>
	<div style="text-align: justify-all;">
		I hearby submit this registration/application form with the following details for the Municipality Level Examination <?= $student_information->academic_year.' B.S.' ?> on condition that I will accept if this application is rejected as per Municipality Office rules and regulation.
	</div>
	<table style="width:100%">
		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 30% !important;">Student Name(BLOCKLETTER)</td>
			<td style="border: 1px solid #000; width: 70% !important;"><?= strtoupper($student_information->first_name.' '.$student_information->middle_name.' '.$student_information->last_name) ?></td>
		</tr>

		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 30% !important;">Nepali Name</td>
			<td style="border: 1px solid #000; width: 70% !important;"><?= $student_information->first_name_nepali.' '.$student_information->middle_name_nepali.' '.$student_information->last_name_nepali ?></td>
		</tr>

		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 30% !important;">Father/Mother's Name</td>
			<td style="border: 1px solid #000; width: 70% !important;"><?= $student_information->father_name ? ucwords($student_information->father_name) : ucwords($student_information->mother_name); ?></td>
		</tr>

		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 30% !important;">Student's Date Of Birth</td>
			<td style="border: 1px solid #000; width: 70% !important;"><?= $student_information->dob_nepali.' (B.S.) &nbsp;&nbsp;&nbsp; '.$student_information->don_english.' (A.D.)'; ?></td>
		</tr>
		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 30% !important;">Type Of School</td>
			<td style="border: 1px solid #000; width: 70% !important;"><?= $student_information->school_sec ? ucwords($student_information->school_sec->type) : 'Other'; ?></td>
		</tr>
	</table>
	SUBJECT TO BE TAKEN IN THE EXAMINATION
	<table style="width:100%; border-collapse: collapse;">
		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 5% !important;">S.N.</td>
			<td style="border: 1px solid #000; width: 45% !important;">Subject</td>
			<td style="border: 1px solid #000; width: 5% !important;">S.N.</td>
			<td style="border: 1px solid #000; width: 45% !important;">Subject</td>
		</tr>
		<?php
			for ($i=0; $i < 5 ; $i++) {
				$newI = 5 + $i;
		?>
		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 5% !important;"><?= $i+1; ?>.</td>
			<td style="border: 1px solid #000; width: 45% !important;"><?= isset($subjectArray[$i]) ? ucwords($subjectArray[$i]) : ''; ?></td>
			<td style="border: 1px solid #000; width: 5% !important;"><?= $i+6; ?>.</td>
			<td style="border: 1px solid #000; width: 45% !important;"><?= isset($subjectArray[$newI]) ? ucwords($subjectArray[$newI]) : ''; ?></td>
		</tr>
		<?php
			}
		?>
	</table>
	<br />
	<table  style="width:100%; border-collapse: collapse;">
		<tr>
			<td style="width: 33%; text-align: center;"> --------------------------------------------</td>
			<td style="width: 33%; text-align: center;"> --------------------------------------------</td>
			<td style="width: 33%; text-align: center;"> ---------------------------------------------</td>
		</tr>
		<tr>
			<td style="text-align: center;">Student's Signature</td>
			<td style="text-align: center;">Head Tearcher's Signature <br />
				<p style="border: 1px solid #000;">Stamp Of School</p>
			</td>
			<td style="text-align: center;">Chief Administrative Officer<br />
				<p style="border: 1px solid #000;">Municipality Stamp</p>
			</td>
		</tr>
	</table>
	---------------------------------------------------------------------------------------------------------------------------
	<br />
	<div class="row-fluid">
		<h4 class="text-center">फारम भर्ने बिद्यार्थी भराउने प्रधानाध्यापकले ध्यान दिन पर्ने कुराहरू :</h4>
		<ol>
		<li> रिजिष्ट्रेसन आवेदन फाराममा पछि सम्म फरक नपर्ने गरि नाम थर र जन्म मिति उल्लेख गर्नु पर्नेछ । </li>
		<li> रिजिष्ट्रेसन आवेदन फाराम भर्दा कक्षा ७ को मार्ट सिट वा ग्रेडसिट र नागरिकतावा जन्म दर्ताको
		प्रमाणपत्रको प्रतिलिपि अनिवार्यरुपमा पेस गर्नुपर्ने छ । </li>
		<li> रिजिष्ट्रेसन आवेदन फाराममा अनिवार्यरुपमा बिद्यार्थि स्वयमले भरि हस्ताक्षर गर्नुपर्ने ।</li>
		<li> फारम भर्दा हालसालै खिचेको एउटै नेगेटिभको फोटो प्रयोग गर्नुपर्ने छ।</li>
		<li> फारम भर्दा केरमेट र टिपेक्सको प्रयोग मान्यहुने छैन ।</li>
		नोट: कुनै ब्यहोरा भर्न, संलग्न गर्न छुट भएमा झुट्टा भरिएका कारण परिक्षा फल स्थगित भएमा वा
		प्रकासित नभएमा वा रद्द भएमा कुनै उजुर गर्ने गराउने छैन । उपर्युक्त बिबरण सबै ठिक भनि प्रमाणित
		गर्दछु ।</li>
		</ol>
		<div class="row-fluid text-center">
			............................................ <br /> Signature <br /> Head Teacher <br /> <span style="border-bottom: thin dotted #000; "> <?= $student_information->school_sec ? ucwords($student_information->school_sec->title) : ''; ?> </span> Secondary  <span style="border-bottom: thin dotted #000; "> <?= $student_information->school_sec ? $student_information->school_sec->ward_no : ''; ?> </span> <br />
			 <span style="border-bottom: thin dotted #000; ">Date :  <?= $student_information->created_date ? date('Y-m-d', strtotime($student_information->created_date)) : ''; ?> </span> Contact No.  <span style="border-bottom: thin dotted #000; "> <?= $student_information->school_sec ? $student_information->school_sec->contact_number : ''; ?> </span> <br />
		</div>
	</div>
	</div>
</div>

<br />
<div class="row-fluid" id="sinlgeSection">
	<br /><br /> <br /><br />
	<div style="width: 12%; height: 800px; float: left;"> </div>
	<div style="width: 85%; float: right;">
	<div style="width:10%; float: left; ">
		<img src="<?= Yii::app()->request->baseUrl.'/images/logo/ministry_education.jpg'; ?>" width="100" height="80" />
	</div>
	<div style='width: 90%; font-size:15px; font-weight:bold; text-align: center; float: right;'>
		<div> Nagarjun Municipality</div><div> Office Of Municipal Excutive</div><div > Harisiddhi, Sitapaila, Kathmandu</div>
	</div>
	<br />
	<div style='font-size:14px; font-weight:bold; margin-top: -1em;'>
		MUNICIPALITY LEVEL EXAMINATION REGISTRATION/APPLICATION FORM
	</div>
	<table style="width:100%; border-collapse: collapse;">
		<tr>
			<td style="width: 30%">Symbol No. : <?= $student_information->symbol_number; ?> <br />
				To, <br />
				Chief Administrative Officer <br />

				<?= ucwords($municipality); ?> <br />
				<?= ucwords($address); ?> 
			</td>
			<td style="width: 50%;">
				<table class="table" style="width:100%; border-collapse: collapse;margin-bottom:0px;">
					<tr>
						<td colspan="10" style="text-align: center;">SCHOOL COPY</td></tr>
					<tr >
						<td colspan="10" style="text-align: center;">Registration No. (10 digits)</td>
					</tr>
					<tr style="border: 1px solid #000;">
						<td colspan="2" style="border: 1px solid #000; width: 20%; font-size: 10pt;"> Year
						</td>
						<td colspan="2" style="border: 1px solid #000; width: 25%; font-size: 10pt;"> Ward No.
						</td>
						<td colspan="3" style="border: 1px solid #000; width: 25%; font-size: 10pt;"> School Code
						</td>
						<td colspan="3" style="border: 1px solid #000; width: 30%; font-size: 10pt;"> Student Number
						</td>
					</tr>
					<tr style="border: 1px solid #000;">
						<td style="border: 1px solid #000;"><?= $year_digit_1; ?></td>
						<td style="border: 1px solid #000;"><?= $year_digit_2; ?></td>
						<td style="border: 1px solid #000;"><?= $ward_digit_1;?></td>
						<td style="border: 1px solid #000;"><?= $ward_digit_2; ?></td>
						<td style="border: 1px solid #000;"><?= $school_digit_1; ?></td>
						<td style="border: 1px solid #000;"><?= $school_digit_2; ?></td>
						<td style="border: 1px solid #000;"><?= $school_digit_3; ?></td>
						<td style="border: 1px solid #000;"><?= $student_digit_1; ?></td>
						<td style="border: 1px solid #000;"><?= $student_digit_2; ?></td>
						<td style="border: 1px solid #000; padding:0px !important;"><?= $student_digit_3; ?></td>
					</tr>
				</table>
			</td>
			<td> <p style="height: 100px; width: 100px !important; float: right; border: 1px solid #000;width: 20%;"></p> </td>
		</tr>
	</table>
	<div style="text-align: justify-all;">
		I hearby submit this registration/application form with the following details for the Municipality Level Examination <?= $student_information->academic_year.' B.S.' ?> on condition that I will accept if this application is rejected as per Municipality Office rules and regulation.
	</div>
	<table style="width:100%">
		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 30% !important;">Student Name(BLOCKLETTER)</td>
			<td style="border: 1px solid #000; width: 70% !important;"><?= strtoupper($student_information->first_name.' '.$student_information->middle_name.' '.$student_information->last_name) ?></td>
		</tr>

		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 30% !important;">Nepali Name</td>
			<td style="border: 1px solid #000; width: 70% !important;"><?= $student_information->first_name_nepali.' '.$student_information->middle_name_nepali.' '.$student_information->last_name_nepali ?></td>
		</tr>

		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 30% !important;">Father/Mother's Name</td>
			<td style="border: 1px solid #000; width: 70% !important;"><?= $student_information->father_name ? ucwords($student_information->father_name) : ucwords($student_information->mother_name); ?></td>
		</tr>

		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 30% !important;">Student's Date Of Birth</td>
			<td style="border: 1px solid #000; width: 70% !important;"><?= $student_information->dob_nepali.' (B.S.) &nbsp;&nbsp;&nbsp; '.$student_information->don_english.' (A.D.)'; ?></td>
		</tr>
		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 30% !important;">Type Of School</td>
			<td style="border: 1px solid #000; width: 70% !important;"><?= $student_information->school_sec ? ucwords($student_information->school_sec->type) : 'Other'; ?></td>
		</tr>
	</table>
	SUBJECT TO BE TAKEN IN THE EXAMINATION
	<table style="width:100%; border-collapse: collapse;">
		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 5% !important;">S.N.</td>
			<td style="border: 1px solid #000; width: 45% !important;">Subject</td>
			<td style="border: 1px solid #000; width: 5% !important;">S.N.</td>
			<td style="border: 1px solid #000; width: 45% !important;">Subject</td>
		</tr>
		<?php
			for ($i=0; $i < 5 ; $i++) {
				$newI = 5 + $i;
		?>
		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 5% !important;"><?= $i+1; ?>.</td>
			<td style="border: 1px solid #000; width: 45% !important;"><?= isset($subjectArray[$i]) ? ucwords($subjectArray[$i]) : ''; ?></td>
			<td style="border: 1px solid #000; width: 5% !important;"><?= $i+6; ?>.</td>
			<td style="border: 1px solid #000; width: 45% !important;"><?= isset($subjectArray[$newI]) ? ucwords($subjectArray[$newI]) : ''; ?></td>
		</tr>
		<?php
			}
		?>
	</table>
	<br />
	<table  style="width:100%; border-collapse: collapse;">
		<tr>
			<td style="width: 33%; text-align: center;"> --------------------------------------------</td>
			<td style="width: 33%; text-align: center;"> --------------------------------------------</td>
			<td style="width: 33%; text-align: center;"> ---------------------------------------------</td>
		</tr>
		<tr>
			<td style="text-align: center;">Student's Signature</td>
			<td style="text-align: center;">Head Tearcher's Signature <br />
				<p style="border: 1px solid #000;">Stamp Of School</p>
			</td>
			<td style="text-align: center;">Chief Administrative Officer<br />
				<p style="border: 1px solid #000;">Municipality Stamp</p>
			</td>
		</tr>
	</table>
	---------------------------------------------------------------------------------------------------------------------------
	<br />
	<div class="row-fluid">
		<h4 class="text-center">फारम भर्ने बिद्यार्थी भराउने प्रधानाध्यापकले ध्यान दिन पर्ने कुराहरू :</h4>
		<ol>
		<li> रिजिष्ट्रेसन आवेदन फाराममा पछि सम्म फरक नपर्ने गरि नाम थर र जन्म मिति उल्लेख गर्नु पर्नेछ । </li>
		<li> रिजिष्ट्रेसन आवेदन फाराम भर्दा कक्षा ७ को मार्ट सिट वा ग्रेडसिट र नागरिकतावा जन्म दर्ताको
		प्रमाणपत्रको प्रतिलिपि अनिवार्यरुपमा पेस गर्नुपर्ने छ । </li>
		<li> रिजिष्ट्रेसन आवेदन फाराममा अनिवार्यरुपमा बिद्यार्थि स्वयमले भरि हस्ताक्षर गर्नुपर्ने ।</li>
		<li> फारम भर्दा हालसालै खिचेको एउटै नेगेटिभको फोटो प्रयोग गर्नुपर्ने छ।</li>
		<li> फारम भर्दा केरमेट र टिपेक्सको प्रयोग मान्यहुने छैन ।</li>
		नोट: कुनै ब्यहोरा भर्न, संलग्न गर्न छुट भएमा झुट्टा भरिएका कारण परिक्षा फल स्थगित भएमा वा
		प्रकासित नभएमा वा रद्द भएमा कुनै उजुर गर्ने गराउने छैन । उपर्युक्त बिबरण सबै ठिक भनि प्रमाणित
		गर्दछु ।</li>
		</ol>
		<div class="row-fluid text-center">
			............................................ <br /> Signature <br /> Head Teacher <br /> <span style="border-bottom: thin dotted #000; "> <?= $student_information->school_sec ? ucwords($student_information->school_sec->title) : ''; ?> </span> Secondary  <span style="border-bottom: thin dotted #000; "> <?= $student_information->school_sec ? $student_information->school_sec->ward_no : ''; ?> </span> <br />
			 <span style="border-bottom: thin dotted #000; ">Date :  <?= $student_information->created_date ? date('Y-m-d', strtotime($student_information->created_date)) : ''; ?> </span> Contact No.  <span style="border-bottom: thin dotted #000; "> <?= $student_information->school_sec ? $student_information->school_sec->contact_number : ''; ?> </span> <br />
		</div>
	</div>
</div>
</div>


<div class="row-fluid" id="sinlgeSection">
	<br /><br /> <br /><br />
	<div style="width: 12%; height: 800px; float: left; "> </div>
	<div style="width: 85%; float: right;">
	<div style="width:10%; float: left; ">
		<img src="<?= Yii::app()->request->baseUrl.'/images/logo/ministry_education.jpg'; ?>" width="100" height="80" />
	</div>
	<div style='width: 90%; font-size:15px; font-weight:bold; text-align: center; float: right;'>
		<div> Nagarjun Municipality</div><div> Office Of Municipal Excutive</div><div > Harisiddhi, Sitapaila, Kathmandu</div>
	</div>
	<br />
	<div style='font-size:14px; font-weight:bold; margin-top: -1em;'>
		MUNICIPALITY LEVEL EXAMINATION REGISTRATION/APPLICATION FORM
	</div>

	<table style="width:100%; border-collapse: collapse;">
		<tr>
			<td style="width: 30%">Symbol No. : <?= $student_information->symbol_number; ?> <br />
				To, <br />
				Chief Administrative Officer <br />

				<?= ucwords($municipality); ?> <br />
				<?= ucwords($address); ?> 
			</td>
			<td style="width: 50%;">
				<table class="table" style="width:100%; border-collapse: collapse;margin-bottom:0px;">
					<tr>
						<td colspan="10" style="text-align: center;">STUDENT COPY</td></tr>
					<tr >
						<td colspan="10" style="text-align: center;">Registration No. (10 digits)</td>
					</tr>
					<tr style="border: 1px solid #000;">
						<td colspan="2" style="border: 1px solid #000; width: 20%; font-size: 10pt;"> Year
						</td>
						<td colspan="2" style="border: 1px solid #000; width: 25%; font-size: 10pt;"> Ward No.
						</td>
						<td colspan="3" style="border: 1px solid #000; width: 25%; font-size: 10pt;"> School Code
						</td>
						<td colspan="3" style="border: 1px solid #000; width: 30%; font-size: 10pt;"> Student Number
						</td>
					</tr>
					<tr style="border: 1px solid #000;">
						<td style="border: 1px solid #000;"><?= $year_digit_1; ?></td>
						<td style="border: 1px solid #000;"><?= $year_digit_2; ?></td>
						<td style="border: 1px solid #000;"><?= $ward_digit_1;?></td>
						<td style="border: 1px solid #000;"><?= $ward_digit_2; ?></td>
						<td style="border: 1px solid #000;"><?= $school_digit_1; ?></td>
						<td style="border: 1px solid #000;"><?= $school_digit_2; ?></td>
						<td style="border: 1px solid #000;"><?= $school_digit_3; ?></td>
						<td style="border: 1px solid #000;"><?= $student_digit_1; ?></td>
						<td style="border: 1px solid #000;"><?= $student_digit_2; ?></td>
						<td style="border: 1px solid #000; padding:0px !important;"><?= $student_digit_3; ?></td>
					</tr>
				</table>
			</td>
			<td> <p style="height: 100px; width: 100px !important; float: right; border: 1px solid #000;width: 20%;"></p> </td>
		</tr>
	</table>
	<div style="text-align: justify-all;">
		I hearby submit this registration/application form with the following details for the Municipality Level Examination <?= $student_information->academic_year.' B.S.' ?> on condition that I will accept if this application is rejected as per Municipality Office rules and regulation.
	</div>
	<table style="width:100%">
		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 30% !important;">Student Name(BLOCKLETTER)</td>
			<td style="border: 1px solid #000; width: 70% !important;"><?= strtoupper($student_information->first_name.' '.$student_information->middle_name.' '.$student_information->last_name) ?></td>
		</tr>

		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 30% !important;">Nepali Name</td>
			<td style="border: 1px solid #000; width: 70% !important;"><?= $student_information->first_name_nepali.' '.$student_information->middle_name_nepali.' '.$student_information->last_name_nepali ?></td>
		</tr>

		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 30% !important;">Father/Mother's Name</td>
			<td style="border: 1px solid #000; width: 70% !important;"><?= $student_information->father_name ? ucwords($student_information->father_name) : ucwords($student_information->mother_name); ?></td>
		</tr>

		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 30% !important;">Student's Date Of Birth</td>
			<td style="border: 1px solid #000; width: 70% !important;"><?= $student_information->dob_nepali.' (B.S.) &nbsp;&nbsp;&nbsp; '.$student_information->don_english.' (A.D.)'; ?></td>
		</tr>
		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 30% !important;">Type Of School</td>
			<td style="border: 1px solid #000; width: 70% !important;"><?= $student_information->school_sec ? ucwords($student_information->school_sec->type) : 'Other'; ?></td>
		</tr>
	</table>
	SUBJECT TO BE TAKEN IN THE EXAMINATION
	<table style="width:100%; border-collapse: collapse;">
		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 5% !important;">S.N.</td>
			<td style="border: 1px solid #000; width: 45% !important;">Subject</td>
			<td style="border: 1px solid #000; width: 5% !important;">S.N.</td>
			<td style="border: 1px solid #000; width: 45% !important;">Subject</td>
		</tr>
		<?php
			for ($i=0; $i < 5 ; $i++) {
				$newI = 5 + $i;
		?>
		<tr style="border: 1px solid #000;">
			<td style="border: 1px solid #000; width: 5% !important;"><?= $i+1; ?>.</td>
			<td style="border: 1px solid #000; width: 45% !important;"><?= isset($subjectArray[$i]) ? ucwords($subjectArray[$i]) : ''; ?></td>
			<td style="border: 1px solid #000; width: 5% !important;"><?= $i+6; ?>.</td>
			<td style="border: 1px solid #000; width: 45% !important;"><?= isset($subjectArray[$newI]) ? ucwords($subjectArray[$newI]) : ''; ?></td>
		</tr>
		<?php
			}
		?>
	</table>
	<br />
	<table  style="width:100%; border-collapse: collapse;">
		<tr>
			<td style="width: 33%; text-align: center;"> --------------------------------------------</td>
			<td style="width: 33%; text-align: center;"> --------------------------------------------</td>
			<td style="width: 33%; text-align: center;"> ---------------------------------------------</td>
		</tr>
		<tr>
			<td style="text-align: center;">Student's Signature</td>
			<td style="text-align: center;">Head Tearcher's Signature <br />
				<p style="border: 1px solid #000;">Stamp Of School</p>
			</td>
			<td style="text-align: center;">Chief Administrative Officer<br />
				<p style="border: 1px solid #000;">Municipality Stamp</p>
			</td>
		</tr>
	</table>
	---------------------------------------------------------------------------------------------------------------------------
	<br />
	<div class="row-fluid">
		<h4 class="text-center">परिक्षार्थिहरूलाई निर्देशन :</h4>
		<ol>
			<li> परिक्षा सुरु हुने पहिलो दिन परिक्षा सुरु हुनु भन्दा आधा घण्टा अगाडि र दोस्रो दिनबाट १५ मिनट पहिले मात्र परिक्षा भवन खुल्ने छ । </li>
			<li> परिक्षा सुरु भएको १ घण्टा भन्दा ढिला आउने परिक्षार्थिलाई परिक्षा हलमा प्रवेश गराईने छैन ।</li>
			<li> केन्द्राधक्षको अनुमति बिना आफ्नो स्थान वा परिक्षा भवन छाडेर बाहिर जान पाईने छैन । </li>
			<li> परिक्षा भवन प्रवेश गर्दा आफ्नो परिक्षा प्रवेश पत्र अनिवार्य रुपमा लिएर आउन पर्ने र केन्द्राधक्ष वानिरिक्षकले मागेको बेलामा देखाउन पर्नेछ । </li>

			<li> परिक्षा भवन भित्र कुनै किसिमका पुस्तक, नोटबुक, गाइड, चिट, मोबाइल, हातहतियार, अबैध प्रदार्थ वाहरु निषेध गरिएका सर सामान परिक्षा हलमा प्रवेश गराए वा गराउने प्रयत्न गरेको पाइएमा त्यस्ता बिद्यार्थीलाई परिक्षाहलबाट निस्कासन गरो परिक्षा समेत रध्द गरिनेछ । </li>
			<li> परिक्षार्थीले परिक्षासुरु भई प्रश्न पत्र पाएको एक घण्टा नभइकन परिक्षाहल छाड्न पाउने छैनन् । </li>
			<li> परिक्षार्थीले परिक्षाको उत्तर पुस्तिका बुझाइ परिक्षाहलबाट बाहिरिए पछि निजलाई कुनै पनि हालतमा पुन: परिक्षाहल प्रवेश गराई परिक्षा दिन दिईने छैन । </li>
			<li> केन्द्राधक्षले तोकेको निर्देशन पालना गरि निजले तोकेको स्थानमा बसि परिक्षा दिन पर्ने छ । केन्द्राधक्ष वानिजले तोकेको ब्यक्तिले जुन सुकै समयमा परिक्षा हलमा खानतलासी गर्न सक्नेछ । </li>
			<li> परिक्षार्थीले एक बिषयमा फारम भरि अर्को बिषय बिषयमा परिक्षा दिएको वा फारम नभरेको बिषयमा
			परिक्षा दिएमा स्तह परिक्षाफल रध्द गरिनेछ । </li>
			<li> परिक्षार्थीले परिक्षाको मर्यादा भंगहुने , परिक्षा खलल पुग्ने आदि अमर्यादित काम गरेमा, एक परिक्षार्थीकोसट्टा हर्को परिक्षार्थी भई परिक्षा दिएमा निरिक्षकलाई उत्तर पुस्तिका नबुझाई बाहिरिएमा त्यस्तापरिक्षार्थीको परिक्षा फल स्वत: रद्ध हुनेछ । </li>
			<li> परिक्षामा आफ्नो सिम्बोल नम्बेरबाहेक अन्य अरु कुनै कोड वा चिनारी छाडेमा त्यस्ता परिक्षार्थीको परिक्षा फल प्रकाशन गरिने छैन ।</li>
		</ol>
	</div>
</div>
</div>
<!-- <a class="btn btn-success" href="#" onclick="window.open(<?= $student_information->id; ?>,'POPUP WINDOW TITLE HERE','width=650,height=800').print()">Print</a> -->

<button target='_blank' id="print_button" class="btn btn-success"> Print AdmitCard </button>
<script type="text/javascript">

$(document).ready(function(){
    window.print();
    $("#print_button").click(function () {
    	window.print();
    });
});
</script>
