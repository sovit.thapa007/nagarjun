<?php
$this->breadcrumbs = array(
	'REGISTER STUDENT' => array('create'),
	'UNDER AGE STUDENT LIST',
);

?>
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'basic-information-form',
    'type' => 'horizontal',
    'method' => 'get',
    'action' => Yii::app()->request->baseUrl.'/studentInformation/belowAgeStudents',
    'htmlOptions' => array(
        'class' => 'form-horizontal well',
    ),
    'enableAjaxValidation' => false,
    ));
?>


<div class="row-fluid">
    <div class="span12">
            <div class="box-content nopadding">
                <div class="control-group clear-fix">
                    <h2><strong>SEARCH FORM</strong></h2>
                </div>
                <div class="row-fluid">
                    <label>DOB(BS)</label>
                    <?php
                        $this->widget("ext.maskedInput.MaskedInput", array(
                        "model" => $model,
                        "attribute" => "dob_nepali",
                        "mask" => "9999-99-99",
                        "clientOptions"=>array("greedy"=>false, 'class'=>'span12','placeholder'=>'yyyy-mm-dd','removeMaskOnSubmit' => false)   
                    ));    
                    ?>
                </div>
                <br />

                    <div class="form-actions text-center">
                        <?php
                        $this->widget('bootstrap.widgets.TbButton', array(
                            'buttonType' => 'submit',
                            'type' => 'primary',
                            'label' => 'SAVE INFORMATION',
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
<?php $this->endWidget(); ?>


<div class="form-horizontal well">
<h2><strong>STUDENT LIST BELOW : <?= $nepali_date_limit; ?></strong></h2>
<table id='datatable' class="table table-bordered" style="width: 100%;">
        <thead>
            <tr>
                <th>SN.</th>
                <th>SCHOOL</th>
                <th>CODE</th>
                <th>SYMBOL NO.</th>
                <th>NAME</th>
                <th>DOB(BS)</th>
                <th>DOB(AD)</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(!empty($student_list)){
                    $sn = 1;
                    foreach ($student_list as $student_info) {
                        ?>
                        <tr>
                            <td><?= $sn; ?></td>
                            <td><a href="<?php echo Yii::app()->baseUrl.'/basicInformation/'.$student_info->school_id; ?>"><?= $student_info->school_sec ?  strtoupper($student_info->school_sec->title) : ''; ?></a></td>
                            <td><?= $student_info->school_sec ? $student_info->school_sec->schole_code : ''; ?></td>
                            <td><a href="<?php echo Yii::app()->baseUrl.'/studentInformation/update/'.$student_info->id; ?>"><?= $student_info->symbol_number; ?></a>
                                </td>
                            <td><?= strtoupper($student_info->first_name. ' '.$student_info->middle_name.' '.$student_info->last_name); ?></td>
                            <td><?= $student_info->dob_nepali; ?></td>
                            <td><?= $student_info->don_english; ?></td>
                        </tr>
                        <?php
                        $sn++;
                    }
                }
            ?>
        </tbody>

        <tfoot>
            <tr>
                <th>SN.</th>
                <th>SCHOOL</th>
                <th>CODE</th>
                <th>SYMBOL NO.</th>
                <th>NAME</th>
                <th>DOB(BS)</th>
                <th>DOB(AD)</th>
            </tr>
        </tfoot>
    </table>
</div>
