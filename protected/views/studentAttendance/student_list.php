<?php
/**
 * Created by PhpStorm.
 * User: Sovit Thapa
 * Date: 7/25/15
 * Time: 2:07 PM
 */
?>
<div class="row-fluid well">
    <div class="span5 text-center"> <h4 >School : <?= $school_information ? $school_information->title : 'null'?></h4></div>
    <div class="span5 text-center"> <h4 >Terminal : <?= $terminal_information ? $terminal_information->title : 'null'?></h4>
</div>
</div>
<form method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/StudentAttendance/InsertAttendace">
    <div class="row-fluid">
        <div class="span5"><h4 class="text-center"> Student Attendace </h4></div>
        <div class="span5">
        Total Terminal Class Days <input type="number" name="total_terminal_class_days" required="required" value="<?= $total_attendannce; ?>" placeholder="Total School Days">
        </div>
    </div>
    <table class="table table-hover table-bordered">
        <tr>
            <th>SN</th>
            <th colspan="2">Student Name</th>
            <th>Section</th>
            <th>Roll Number</th>
            <th> Attendace </th>
        </tr>
        <?php
        $sn=0;
        foreach($student_list as $studentdata)
        {
            $student_id = $studentdata->id;
            $present_days = isset($student_attendance[$student_id]) ? $student_attendance[$student_id] : 0; 
            ?>
            <tr>
                <td><?php echo $sn+1; ?>
                    <input type="hidden" name="<?php echo $sn.'_registration_number'; ?>" value="<?php echo $student_id; ?>">
                </td>
                <td colspan="2"><?= ucwords($studentdata->first_name." ".$studentdata->middle_name." ".$studentdata->last_name); ?></td>
                
                <td><?= isset($studentdata->classSection) ? ucwords($studentdata->classSection->title) : "null" ; ?></td>
                <td><?= $studentdata->roll_number; ?></td>
                <td><input type="number" name="student_attendance_<?= $sn.'_'.$student_id ;?>" placeholder="Total Present Days " max="<?= $total_attendannce; ?>" value="<?= $present_days; ?>" required>
                </td>
            </tr>
        <?php
        $sn++;
        }
        ?>
    </table>
    <dl>
        <dt>
            <input type="hidden" name="terminal_id" value="<?php echo $terminal_id; ?>">
            <input type="hidden" name="academic_year" value="<?php echo $academic_year; ?>">
            <input type="hidden" name="school_id" value="<?php echo $school_id; ?>">
            <input type="hidden" name="total_number" value="<?php echo $sn; ?>">
            <input  class="text-center" type="submit" name="submit" value="Insert Attendance" class="btn btn-info">
        </dt>
    </dl>
</form>