<?php
/**
 * Created by PhpStorm.
 * User: Sovit Thapa
 * Date: 7/25/15
 * Time: 2:02 PM
 */
?>

    <?php
    if(isset($message))
    {
        ?>
        <h4 class="text-center"><?= $message; ?></h4>
    <?php
    }
    ?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'action'=>'StudentAttendanceSec',
    'type' => 'horizontal',
    'htmlOptions' => array(
        'class' => 'form-horizontal well',
    ),
)); ?>
<h2><strong>Insert Attendance</strong></h2>
<div class="row-fluid">
    <div class="span6">
        <?php echo $form->textFieldRow($model,'total_attendannce',array('class'=>'span12')); ?></div>
    <div class="span6">
        <?php echo $form->dropDownListRow($model,'arrange_by',array('name'=>'Name', 'roll_number' => 'Roll Number', 'section'=>'Section'), array('class'=>'span12')); ?>
    </div>
</div>
    <div class="row-fluid">
        <div class="span6">
        <?php echo $form->dropDownListRow($model,'terminal_id',CHtml::listData(Terminal::model()->findAll(),'class_terminal_id','title'),array('prompt'=>'Select Terminal', 'class'=>'span12')); ?>
        </div>
        <div class="span6">
            <?php 
            if(UtilityFunctions::ShowSchool()){
                echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(),'id','title'),array('class'=>'span12')); 
            }
            ?>
        </div>
    </div>
    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'=>'primary',
            'label'=>'Search',
        )); ?>
    </div>

<?php $this->endWidget(); ?>

