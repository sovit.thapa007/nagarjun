<?php
$this->breadcrumbs=array(
	'Student Attendances'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List StudentAttendance','url'=>array('index')),
array('label'=>'Create StudentAttendance','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('student-attendance-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Manage Student Attendances</h1>

<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
		&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'student-attendance-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'academic_year',
		'terminal_id',
		'student_id',
		'attendance',
		'total_attendannce',
		/*
		'school_id',
		'created_by',
		'created_date',
		'approved_by',
		'approved_date',
		*/
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
