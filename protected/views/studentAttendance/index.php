<?php
$this->breadcrumbs=array(
	'Student Attendances',
);

$this->menu=array(
array('label'=>'Create StudentAttendance','url'=>array('create')),
array('label'=>'Manage StudentAttendance','url'=>array('admin')),
);
?>

<h1>Student Attendances</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
