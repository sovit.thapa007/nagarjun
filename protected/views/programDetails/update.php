<?php
$this->breadcrumbs = array(
	'System Module' => array('dashboard/systemSetting'),
	'Create Program' => array('create'),
	$model->title => array('view', 'id' => $model->id),
	'Update',
);
/*
$this->menu = array(
	array('label' => 'List ProgramDetails', 'url' => array('index')),
	array('label' => 'Create ProgramDetails', 'url' => array('create')),
	array('label' => 'View ProgramDetails', 'url' => array('view', 'id' => $model->id)),
	array('label' => 'Manage ProgramDetails', 'url' => array('admin')),
);*/
?>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>