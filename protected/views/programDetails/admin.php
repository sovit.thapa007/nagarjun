<?php
$this->breadcrumbs = array(
	'System Module' => array('dashboard/systemSetting'),
	'Create Program' => array('create'),
	'Manage',
);

$this->menu = array(
	array('label' => 'List ProgramDetails', 'url' => array('index')),
	array('label' => 'Create ProgramDetails', 'url' => array('create')),
);
?>
<hr />
<h1 class="text-center">Manage Program Details</h1>
<hr />



<?php
$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'program-details-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		[
			'name'=>'shoollevel_id',
			'value'=>' $data->school_level ? $data->school_level->title :"Null" ',
		],
		//'shoollevel_id',
		'title',
		'program_type',
		'duration',
		[
			'name'=>'status',
			'value'=>'$data->status == 1 ? "Active" : "Deactive" ',
		],
		//'status',
		/*
		  'nepali_date',
		  'created_date',
		  'created_by',
		 */
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
		),
	),
));
?>
