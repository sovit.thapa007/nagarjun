<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'program-details-form',
	'type' => 'horizontal',
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
	),
	'enableAjaxValidation' => false,
	));
?>

<div class="row-fluid">
	<div class="span12">
		
			<div class="box-title"><p class="help-block">Fields with <span class="required">*</span> are required.</p></div>
			<div class="box-content nopadding">
				<div class="control-group">
					<h3 class="text-center" >Program</h3>
					<hr />

				</div>
				<div class="row-fluid">
					<?php echo $form->errorSummary($model); ?>

					<?php echo $form->dropDownListRow($model, 'shoollevel_id',CHtml::listData(SchoolLevel::model()->findAll(),'id', 'title'),array('prompt'=>'Select School Level'), array('class' => 'span5')); ?>
					<?php //echo $form->textFieldRow($model, 'shoollevel_id', array('class' => 'span5')); ?>

					<?php echo $form->textFieldRow($model, 'title', array('style'=> 'width:222px;', 'maxlength' => 250)); ?>

					<?php echo $form->dropDownListRow($model, 'program_type', array("semester" => "semester", "trimester" => "trimester", "yearly" => "yearly",), array('style'=> 'width:222px;')); ?>

					<?php echo $form->textFieldRow($model, 'duration', array('style'=> 'width:222px;')); ?>

					<?php
					if(!$model->isNewRecord)
					 echo $form->textFieldRow($model, 'status', array('class' => 'span5'));
					  ?>

					<?php //echo $form->textFieldRow($model, 'nepali_date', array('class' => 'span5', 'maxlength' => 20)); ?>

					<?php //echo $form->textFieldRow($model, 'created_date', array('class' => 'span5')); ?>

					<?php //echo $form->textFieldRow($model, 'created_by', array('class' => 'span5')); ?>

					<div class="form-actions">
						<?php
						$this->widget('bootstrap.widgets.TbButton', array(
							'buttonType' => 'submit',
							'type' => 'primary',
							'label' => $model->isNewRecord ? 'Create' : 'Save',
						));
						?>
					</div>
				</div>
			</div>
		
	</div>
</div>


			<?php $this->endWidget(); ?>
