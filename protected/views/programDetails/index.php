<?php
$this->breadcrumbs = array(
	'Program Details',
);

$this->menu = array(
	array('label' => 'Create ProgramDetails', 'url' => array('create')),
	array('label' => 'Manage ProgramDetails', 'url' => array('admin')),
);
?>

<h1>Program Details</h1>

<?php
$this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider' => $dataProvider,
	'itemView' => '_view',
));
?>
