<?php
$this->breadcrumbs = array(
	'System Module' => array('dashboard/systemSetting'),
	'Create Program' => array('create'),
	$model->title,
);

$this->menu = array(
	array('label' => 'List ProgramDetails', 'url' => array('index')),
	array('label' => 'Create ProgramDetails', 'url' => array('create')),
	array('label' => 'Update ProgramDetails', 'url' => array('update', 'id' => $model->id)),
	array('label' => 'Delete ProgramDetails', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
	array('label' => 'Manage ProgramDetails', 'url' => array('admin')),
);
?>
<hr />
<h1 class="text-center">View ProgramDetails #<?php echo $model->id; ?></h1>
<hr />

<?php
$this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'type'=>'bordered',
	'attributes' => array(
		'id',
		[
			'name'=>'shoollevel_id',
			'value'=>$model->school_level ? $model->school_level->title :"Null" ,
		],
		'title',
		'program_type',
		'duration',
		[
			'name'=>'status',
			'value'=>$model->status == 0 ? "Deactive" : "Active",
		],
		//'status',
		'nepali_date',
		'created_date',
		[
			'name'=>'created_by',
			'value'=>UtilityFunctions::createrName($model->created_by),
		],
		//'created_by',
	),
));
?>
