<?php
$this->breadcrumbs = array(
	'System Module' => array('dashboard/systemSetting'),
	'Program Details' => array('admin'),
	'Create',
);

$this->menu = array(
	array('label' => 'List ProgramDetails', 'url' => array('index')),
	array('label' => 'Manage ProgramDetails', 'url' => array('admin')),
);
?>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>