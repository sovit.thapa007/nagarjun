<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shoollevel_id')); ?>:</b>
	<?php echo CHtml::encode($data->shoollevel_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('program_type')); ?>:</b>
	<?php echo CHtml::encode($data->program_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('duration')); ?>:</b>
	<?php echo CHtml::encode($data->duration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nepali_date')); ?>:</b>
	<?php echo CHtml::encode($data->nepali_date); ?>
	<br />

	<?php /*
	  <b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	  <?php echo CHtml::encode($data->created_date); ?>
	  <br />

	  <b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	  <?php echo CHtml::encode($data->created_by); ?>
	  <br />

	 */ ?>

</div>