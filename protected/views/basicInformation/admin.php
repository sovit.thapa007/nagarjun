<?php
$this->breadcrumbs = array(
	'ADD NEW SCHOOL' => array('create'),
	'SCHOOL LISTS',
);

$this->menu = array(
	array('label' => 'List BasicInformation', 'url' => array('index')),
	array('label' => 'Create BasicInformation', 'url' => array('create')),
);


?>
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'basic-information-form',
    'type' => 'horizontal',
    'method' => 'get',
    'action' => Yii::app()->request->baseUrl.'/BasicInformation/admin',
    'htmlOptions' => array(
        'class' => 'form-horizontal well',
    ),
    'enableAjaxValidation' => false,
    ));
?>


<div class="row-fluid">
    <div class="span12">
            <div class="box-content nopadding">
                <div class="control-group clear-fix">
                    <h2><strong>SEARCH FORM</strong></h2>
                </div>
                <div class="row-fluid">
                <div class="span6">

                    <?php echo $form->dropDownListRow($model, 'type', Yii::app()->params['school_type'], array('class'=>'span12', 'prompt'=>'Select School Type', 'multiple'=>'true')); ?>
                </div>
                <div class="span6">

                    <?php echo $form->dropDownListRow($model,'resource_center_id',CHtml::listData(BasicInformation::model()->ResourceCenter(),'id','name'),array('class'=>'span12', 'multiple'=>'true'));
                    ?>
                </div>
                </div>

                    <div class="form-actions text-center">
                        <?php
                        $this->widget('bootstrap.widgets.TbButton', array(
                            'buttonType' => 'submit',
                            'type' => 'primary',
                            'label' => 'SAVE INFORMATION',
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
<?php $this->endWidget(); ?>

<div class="form-horizontal well">

<h2><strong>REGISTER SCHOOLS LIST</strong></h2>
<table id='datatable' class="table table-bordered" style="width: 100%;">
        <thead>
            <tr>
                <th>SN.</th>
                <th>Name</th>
                <th>Type</th>
                <th>Code</th>
                <th>Resource Center</th>
                <th>Address</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(!empty($schools_list)){
                    $sn = 1;
                    foreach ($schools_list as $school_info) {
                        $resouce_id = $school_info->resource_center_id;
                        $resource_center = BasicInformation::model()->findByPk($resouce_id);
                        ?>
                        <tr>
                            <td><?= $sn; ?></td>
                            <td><a href="<?php echo Yii::app()->baseUrl.'/basicInformation/'.$school_info->id; ?>"><?= strtoupper($school_info->title); ?></a></td>
                            <td><?= strtoupper($school_info->type); ?></td>
                            <td><?= strtoupper($school_info->schole_code); ?></td>
                            <td><?= $resource_center ? strtoupper($resource_center->title. ' '.$resource_center->tole) : ''; ?></td>
                            <td><?= strtoupper(Yii::app()->params['municipality_short'].'-'.$school_info->ward_no); ?></td>
                        </tr>
                        <?php
                        $sn++;
                    }
                }
            ?>
        </tbody>

        <tfoot>
            <tr>
                <th>SN.</th>
                <th>Name</th>
                <th>Type</th>
                <th>Code</th>
                <th>Resource Center</th>
                <th>Address</th>
            </tr>
        </tfoot>
    </table>
</div>
