<?php
$this->breadcrumbs = array(
	'Basic Informations' => array('admin'),
	$model->title => array('view', 'id' => $model->id),
	'Update',
);

$this->menu = array(
	array('label' => 'List BasicInformation', 'url' => array('index')),
	array('label' => 'Create BasicInformation', 'url' => array('create')),
	array('label' => 'View BasicInformation', 'url' => array('view', 'id' => $model->id)),
	array('label' => 'Manage BasicInformation', 'url' => array('admin')),
);
?>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>