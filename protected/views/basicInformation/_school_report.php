
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'POST',
	'type' => 'horizontal',
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
		'enctype' => 'multipart/form-data',
		'target' => '_blank',
	),
	'enableAjaxValidation' => false,
	));
?>
<div class="row-fluid">
	<div class="span12">
			<div class="box-content nopadding">
				<div class="control-group clear-fix">
					<h2><strong>CHOOSE PARAMETERS AS FOR SELECTED RESULT</strong></h2>
				</div>
				<div class="row-fluid">
					<div class="span6">

						<?php echo $form->dropDownListRow($model, 'type', Yii::app()->params['school_type'], array('prompt'=>'Choose School Type','multiple' => true, 'selected' => 'selected')); ?>

						<?php echo $form->dropDownListRow($model, 'ward_no', UtilityFunctions::WardDropDown(), array('prompt'=>'Choose Ward Number', 'multiple' => true, 'selected' => 'selected'));?>
						
					</div>
					<div class="span6">

						<?php echo $form->textFieldRow($model, 'title', array('class' => 'span12', 'maxlength' => 250 ,'placeholder'=>'SCHOOL NAME')); ?>


						<?php echo $form->textFieldRow($model, 'schole_code', array('class' => 'span12', 'maxlength' => 100)); ?>
						<?php echo $form->textFieldRow($model, 'tole', array('class' => 'span12', 'maxlength' => 250)); ?>
						<?php echo $form->textFieldRow($model, 'establish_year', array('class' => 'span12', 'maxlength' => 15 ,'integer'=>'true')); ?>
					</div>
				</div>
				<div class="row-fluid"><!-- 
					<div class="span6">
						<?php echo $form->dropDownListRow($model, 'report_type', ['pdf'=>'Pdf','excel'=>'Excel'], array('class' => 'span12')); ?>
					</div>
					<div class="span6"> -->
						<?php echo $form->dropDownListRow($model, 'order_by', ['type'=>'type','title'=>'School Name','schole_code'=>'School Code', 'ward_no'=>'School Ward'], array('prompt'=>'Choose Order By','class' => 'span12')); ?>
					<!-- </div> -->
				</div>
				<div class="form-actions text-center">
					<?php
					$this->widget('bootstrap.widgets.TbButton', array(
						'buttonType' => 'submit',
						'type' => 'primary',
						'label' => 'SUBMIT INFORMATION',
					));
					?>
				</div>
				</div>
			</div>
		</div>
	
</div>

<?php $this->endWidget(); ?>
