<?php
$this->breadcrumbs = array(
    'ADD NEW SCHOOL' => array('create'),
    'SCHOOL LISTS',
);

$this->menu = array(
    array('label' => 'List BasicInformation', 'url' => array('index')),
    array('label' => 'Create BasicInformation', 'url' => array('create')),
);


?>

  <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/datatable/jquery.dataTables.min.css" rel="stylesheet">
  <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/datatable/buttons.dataTables.min.css" rel="stylesheet">
  
<style type="text/css">
    
.dataTables_wrapper .dataTables_filter {
float: right;
text-align: right;
visibility: hidden;
}
tfoot{
    visibility: hidden;
}
</style>
<div class="row-fluid form-horizontal well">
<h2><strong>REGISTER SCHOOLS LIST</strong></h2>
    <?php
        if(!empty($message_array)){
            $code = $message_array['status'];
            ?>
            <div class="alert alert-<?= $code == 1 ? 'success' : 'danger'; ?> text-center"> <?= isset($message_array['message']) ?  $message_array['message'] : ''; ?></div>
            <?php
        }
    ?>
    <div class="row-fluid">
    <form method="post" action="">
    <table id='datatable' class="display items table table-bordered">
            <thead>
                <tr>
                    <th>Check Box</th>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Code</th>
                    <th>Address</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(!empty($schools_list)){
                        foreach ($schools_list as $school_info) {
                            ?>
                            <tr>
                                <td><input type="checkbox" name="resource_center_[]" <?php echo $school_info->resource_center == 1 ? 'checked="checked"' : '' ; ?> value="<?= $school_info->id; ?>"></td>
                                <td><?= ucwords($school_info->type); ?></td>
                                <td><?= ucwords($school_info->title); ?></td>
                                <td><?= ucwords($school_info->schole_code); ?></td>
                                <td><?= ucwords(Yii::app()->params['municipality_short'].'-'.$school_info->ward_no); ?></td>
                            </tr>
                            <?php
                        }
                    }
                ?>
            </tbody>

            <tfoot>
                <tr>
                    <th>Check Box</th>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Code</th>
                    <th>Address</th>
                </tr>
            </tfoot>
        </table>
        <button type="submit" class="btn btn-sm btn-primary">SUBMIT INFORMATION</button>
    </form>
    </div>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.elevateZoom-3.0.8.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/canvasjs.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/datatable/jquery.datatables.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/datatable/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/datatable/buttons.flash.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/datatable/jszip.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/datatable/pdfmake.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/datatable/vfs_fonts.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/datatable/buttons.html5.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/datatable/buttons.print.min.js"></script>

<script type="text/javascript">


  $(document).ready(function() {
    $('#datatable thead th').each( function () {
      var title = $('#datatable tfoot th').eq( $(this).index() ).text();
      $(this).html( '<input class="span12" type="text" placeholder="'+title+'" />' );
    } );
    $('#datatable').DataTable( {
      lengthMenu: [
            [-1 ],
            ['Show all' ]
        ],
        dom: 'Bfrtip',
        buttons: [
            'pageLength','copy', 'csv', 'excel', 'pdf', 'print'
        ],
    } );
    var table = $('#datatable').DataTable();
    // Apply the search
    table.columns().eq(0).each(function(colIdx) {
        $('input', table.column(colIdx).header()).on('keyup change', function() {
            table
                .column(colIdx)
                .search(this.value)
                .draw();
        });
     
        $('input', table.column(colIdx).header()).on('click', function(e) {
            e.stopPropagation();
        });
    });

  });
  jQuery(document).ready(function(){
    $(".image").elevateZoom({
      zoomType        : "inner",
      cursor: "crosshair"
    });

    //pass the images to Fancybox
    $(".image").bind("click", function(e) {  
      var ez =   $('.image').data('elevateZoom'); 
      $.fancybox(ez.getGalleryList());
      return false;
    });
  })
</script>

