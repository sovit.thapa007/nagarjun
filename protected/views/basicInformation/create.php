<?php
$this->breadcrumbs = array(
	'Basic Informations' => array('admin'),
	'Create',
);

$this->menu = array(
	array('label' => 'Manage BasicInformation', 'url' => array('admin')),
);
?>
<?php echo $this->renderPartial('_form', array('model' => $model)); ?>