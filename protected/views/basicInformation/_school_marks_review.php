<?php
$this->breadcrumbs = array(
	'ADD NEW SCHOOL' => array('create'),
	'SCHOOL LISTS',
);

$this->menu = array(
	array('label' => 'List BasicInformation', 'url' => array('index')),
	array('label' => 'Create BasicInformation', 'url' => array('create')),
);


?>
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'basic-information-form',
    'type' => 'horizontal',
    'method' => 'get',
    'action' => Yii::app()->request->baseUrl.'/BasicInformation/admin',
    'htmlOptions' => array(
        'class' => 'form-horizontal well',
    ),
    'enableAjaxValidation' => false,
    ));
?>


<div class="row-fluid">
    <div class="span12">
            <div class="box-content nopadding">
                <div class="control-group clear-fix">
                    <h2><strong>SEARCH FORM</strong></h2>
                </div>
                <div class="row-fluid">
                <div class="span6">

                    <?php echo $form->dropDownListRow($model, 'type', Yii::app()->params['school_type'], array('class'=>'span12', 'prompt'=>'Select School Type', 'multiple'=>'true')); ?>
                </div>
                <div class="span6">

                    <?php echo $form->dropDownListRow($model,'resource_center_id',CHtml::listData(BasicInformation::model()->ResourceCenter(),'id','name'),array('class'=>'span12', 'multiple'=>'true'));
                    ?>
                </div>
                </div>

                    <div class="form-actions text-center">
                        <?php
                        $this->widget('bootstrap.widgets.TbButton', array(
                            'buttonType' => 'submit',
                            'type' => 'primary',
                            'label' => 'SAVE INFORMATION',
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
<?php $this->endWidget(); ?>

<div class="form-horizontal well">

<h2><strong>REGISTER SCHOOLS LIST</strong></h2>
<table id='datatable' class="table table-bordered" style="width: 100%;">
        <thead>
            <tr>
                <th>Name</th>
                <th>Type</th>
                <th>Code</th>
                <th>Address</th>
                <th style="width: 18% !important;">Action</th>
            </tr>
        </thead>
        <tbody>
        <?php
            if(!empty($schools_list)){
            foreach ($schools_list as $school_info) {
                ?>
                <tr>
                    <td><a href="<?php echo Yii::app()->baseUrl.'/basicInformation/SchoolMarksReviewDetail/'.$school_info->id; ?>"><?= ucwords($school_info->title); ?></a></td>
                    <td><?= ucwords($school_info->type); ?></td>
                    <td><?= ucwords($school_info->schole_code); ?></td>
                    <td><?= ucwords(Yii::app()->params['municipality_short'].'-'.$school_info->ward_no); ?></td>
                    <td style="width: 18% !important;">
                        <a href="<?= Yii::app()->request->baseUrl.'/markObtained/ledgerAudit/'.$school_info->id; ?>" class="btn btn-xs" data-toggle="tooltip" title="Marks Audit Report" target='_blank'><i class="fa fa-bar-chart"></i></a> &nbsp;
                        <a href="<?php echo Yii::app()->baseUrl.'/basicInformation/SchoolMarksReviewDetail/'.$school_info->id; ?>" class="btn btn-xs" data-toggle="tooltip" title="Insert Marks Subject Wise"><i class="fa fa-pencil"></i></a> &nbsp;
                        <a href="<?= Yii::app()->request->baseUrl.'/basicInformation/PracticalSchoolSheet/?school_id='.$school_info->id.'&type=practical'; ?>" class="btn btn-xs" data-toggle="tooltip" title="Download Practical Sheet" target='_blank'><i class="fa fa-file"></i></a>

                    </td>
                </tr>
                <?php
                }
            }
        ?>
        </tbody>

        <tfoot>
            <tr>
                <th>Name</th>
                <th>Type</th>
                <th>Code</th>
                <th>Address</th>
                <th style="width: 18% !important;">Action</th>
            </tr>
        </tfoot>
    </table>
</div>
