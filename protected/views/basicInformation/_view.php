<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ward_no')); ?>:</b>
	<?php echo CHtml::encode($data->ward_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tole')); ?>:</b>
	<?php echo CHtml::encode($data->tole); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('schole_code')); ?>:</b>
	<?php echo CHtml::encode($data->schole_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('establish_date')); ?>:</b>
	<?php echo CHtml::encode($data->establish_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('moto')); ?>:</b>
	<?php echo CHtml::encode($data->moto); ?>
	<br />

	<?php /*
	  <b><?php echo CHtml::encode($data->getAttributeLabel('remarks')); ?>:</b>
	  <?php echo CHtml::encode($data->remarks); ?>
	  <br />

	  <b><?php echo CHtml::encode($data->getAttributeLabel('create_date')); ?>:</b>
	  <?php echo CHtml::encode($data->create_date); ?>
	  <br />

	 */ ?>

</div>