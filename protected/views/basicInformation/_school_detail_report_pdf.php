<div class="row-fulid" style="overflow: auto;">
    <table class="table table-stripped" style="border-collapse: collapse;" >
        <thead>
            <tr>
                <th colspan="<?= sizeof($column); ?>" style="font-size: 20pt;"> <?= strtoupper(UtilityFunctions::MunicipalLedgerHeader()); ?>
                </th>
            </tr>
            <tr>
                <td style="border:2px solid black; padding: 10px; background: #0E2A47; color: white;">SN</td>
                <?php
                for ($i=0; $i < sizeof($column) ; $i++) { 
                    ?>
                    <td style="border:2px solid black; padding: 10px; background: #0E2A47; color: white;"><?= $column[$i]; ?></td>
                    <?php
                }
                    ?>
            </tr>
        </thead>
        <tbody>

        <?php
            for ($s=0; $s < sizeof($data) ; $s++) { 
                ?>
                <tr>
                    <td style="border:2px solid black; padding: 10px;"><?= $s+1; ?></td>
                <?php
                for ($t=0; $t < sizeof($column) ; $t++) { 
                    ?>
                    <td style="border:2px solid black; padding: 10px;"><?= isset($data[$s][$column[$t]]) ? strtoupper($data[$s][$column[$t]]) : 'null'; ?></td>
                    <?php
                }
                ?>
                </tr>
                <?php
            }
        ?>
       </tbody>
    </table>
</div>
