
<br />
    <table class="table table-bordered" style="border-collapse: collapse; width: 100%;" >
        <thead style="border:2px solid black;">
            <tr>
                <td colspan="<?= sizeof($subjectInformation) + 7 ?>" style="text-align: center;font-weight: bold;"> <?= strtoupper(UtilityFunctions::MunicipalLedgerHeader()); ?>
                </td>
            </tr>
            <tr>
                <td colspan="<?= sizeof($subjectInformation) + 7 ?>" style="text-align: center;font-weight: bold;"> <br />
                </td>
            </tr>
            <tr>
                <td colspan="<?= sizeof($subjectInformation) + 7 ?>" style="font-weight: bold;">School Name & Address : <?= $school_information ? strtoupper($school_information->title).', '.strtoupper(Yii::app()->params['municipality_short']).'-'.$school_information->ward_no : ''; ?></td>
            </tr>
            <tr>
                <td colspan="<?= sizeof($subjectInformation) + 3 ?>" style="font-weight: bold;">Resource Center : <?= $resource_information ? strtoupper($resource_information->title).', '.strtoupper(Yii::app()->params['municipality_short']).'-'.$resource_information->ward_no : ''; ?></td>
                <td colspan="4" style="font-weight: bold;">School Code : <?= $school_information ? $school_information->schole_code : ''; ?> </td>
            </tr>
            <tr><td colspan="<?= sizeof($subjectInformation) + 7 ?>"><br /><br /></td></tr>
            <tr style="border:2px solid black;">
                <td style="border:2px solid black; font-weight: bold;">SN</td>
                <td style="border:2px solid black; font-weight: bold;">Symbol No.</td>
                <td style="border:2px solid black; font-weight: bold;">Name of Student</td>
                <td style="border:2px solid black; font-weight: bold;">Fathers Name</td>
                <td style="border:2px solid black; font-weight: bold;">DOB  B.S</td>
                <td style="border:2px solid black; font-weight: bold;">DOB  A.D</td>
                <?php
                	if(!empty($subjectInformation)){
                		for ($i=0; $i < sizeof($subjectInformation) ; $i++) { 
                			?>
                			<td style="border:2px solid black; font-weight: bold;">
                				<?= isset($subjectInformation[$i]['subject_name']) ? ucwords($subjectInformation[$i]['subject_name']) : ''; ?> <br />
                				<?= isset($subjectInformation[$i]['pm/fm']) ? $subjectInformation[$i]['pm/fm'] : ''; ?>
                			</td>
                			<?php
                		}
                	}
                ?>

                <td style="border:2px solid black; font-weight: bold;">Remarks</td>
            </tr>
        </thead>
        <tbody>

        <?php
            if(!empty($student_information)){
                $sn = 1;
                foreach ($student_information as $student) {
                ?>
                <tr>
                    <td style="border:2px solid black;"><?= $sn; ?></td>
                    <td style="border:2px solid black;"><?= $student->symbol_number; ?></td>
                    <td style="border:2px solid black;"><?= strtoupper($student->first_name.' '.$student->middle_name.' '.$student->last_name) ?></td>
                    <td style="border:2px solid black;"><?= strtoupper($student->father_name) ?></td>
                    <td style="border:2px solid black;"><?= $student->dob_nepali; ?></td>
                    <td style="border:2px solid black;"><?= $student->don_english;?></td>
                     <?php
                	if(!empty($subjectInformation)){
                		for ($j=0; $j < sizeof($subjectInformation) ; $j++) { 
                			?>
                			<td style="border:2px solid black;">
                			</td>
                			<?php
                		}
                	}
                ?>
                    <td style="border:2px solid black;"></td>
                </tr>
                <?php
                    $sn++;
                }
            }
        ?>
       </tbody>
    </table>
