<?php
$this->breadcrumbs = array(
	'Basic Informations',
);

$this->menu = array(
	array('label' => 'Create BasicInformation', 'url' => array('create')),
	array('label' => 'Manage BasicInformation', 'url' => array('admin')),
);
?>

<h1>Basic Informations</h1>

<?php
$this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider' => $dataProvider,
	'itemView' => '_view',
));
?>
