<?php


echo $form->dropDownListRow($model, $array_level[0], CHtml::listData(EthinicGroup::model()->findAllByAttributes(['parent_id'=>'0']), 'id', 'title'), array('prompt' => 'Select Ethinicity', 'class' => 'span12', 'id' => 'ethinic_level', 'data-url' => Yii::app()->createUrl(GeneralInformation::ETHCAST)), array('label' => 'Ethinicity'));
?>



<?php echo $form->dropDownListRow($model, $array_level[1], array(), ['prompt' => 'Select Cast', 'id' => 'cast_level', 'class' => 'span12'], ['label' => 'Cast']); ?>



<script type="text/javascript">

    $('body').on
    (
        'change', '#ethinic_level', function() 
    {

            var this_ = $(this);
            var selected = this_.val();
            var url = this_.data('url') + '/key/' + selected;
            if (selected.length > 0) 
            {
                $.ajax(
                {
                    type: 'POST',
                    url: url,
                    dataType: 'JSON',
                    success: function(response) 
                    {
                        if (response.success) 
                        {
                            $('#cast_level').html(response.option);
                        } 
                        else 
                        {
                            $('#cast_level').html('');
                           
                        }

                        }})
            }
        }
        );


</script>
