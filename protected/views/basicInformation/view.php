<?php
$this->breadcrumbs = array(
	'SCHOOLS LIST' => array('admin'),
	$model->title,
);

$this->menu = array(
	array('label' => 'Add New School', 'url' => array('create')),
	array('label' => 'Update', 'url' => array('update', 'id' => $model->id)),
	array('label' => 'Remove', 'url' => array('delete', 'id' => $model->id)),
);
?>

<div class="form-horizontal well">

	<h2><strong>DETAILS : <?= strtoupper($model->title); ?> <?= $model->resource_center == 1  ? ' ( RESOURCE CENTER ) ': ''; ?></strong> 
	<a class="btn btn-middle btn-primary" title="" data-toggle="tooltip" href="<?= Yii::app()->request->baseUrl.'/basicInformation/update/'.$model->id ?>" data-original-title="Update" style='float: right;'><i class="fa fa-pencil"></i> UPDATE INFORMATION</a></h2>
	<table class="items table table-bordered" style="width:100%">
		<tbody>
			<tr>
				<td style="width: 30%">
					<img id="image_id" 
					src="<?= Yii::app()->request->baseUrl.'/images/logo/'.$model->logo; ?>" data-zoom-image="<?= Yii::app()->request->baseUrl.'/images/logo/'.$model->logo; ?>"  hegiht="300" width="300"/>
				</td>
				<td>
					<table class="items table table-bordered" style="width:100%">
						<tr>
							<td> NAME </td>
							<td><?= strtoupper($model->title); ?></td>
						</tr>
						<tr>
							<td> TYPE </td>
							<td><?= strtoupper($model->type); ?></td>
						</tr>
						<?php
						if($model->resource_center_id != 0){
							?>
							<tr>
								<td> RESOURCE CENTER </td>
								<td><?= strtoupper($resource_center->title. ', '.Yii::app()->params['municipality_short'].'-'.$resource_center->ward_no) ?></td>
							</tr>
							<?php
						}
						?>
						<tr>
							<td> CODE </td>
							<td><?= strtoupper($model->schole_code); ?></td>
						</tr>
						<tr>
							<td> ADDRESS </td>
							<td><?= strtoupper(Yii::app()->params['municipality_short'].'-'.$model->ward_no); ?></td>
						</tr>
						<tr>
							<td> Est. Year </td>
							<td><?= $model->establish_year; ?></td>
						</tr>
						<tr>
							<td> CONTACT PERSON </td>
							<td><?= strtoupper($model->contact_person).' : '.$model->contact_person_number; ?></td>
						</tr>
						<tr>
							<td> Moto </td>
							<td><?= $model->moto; ?></td>
						</tr>
						<?php
						if($model->subject_type == 'school_wise'){
							?>
							<tr>
								<td> Subject List </td>
								<td> <a href="<?= Yii::app()->request->baseUrl; ?>/basicInformation/SchoolSubject/<?= $model->id; ?>" class='btn btn-sm'> ADD  SCHOOL SUBJECT</a></td>
							</tr>
							<?php

						}
						?>	
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	
	jQuery(document).ready(function(){
		$("#image_id").elevateZoom({
		  zoomType				: "inner",
		  cursor: "crosshair"
		});

		//pass the images to Fancybox
		$("#image_id").bind("click", function(e) {  
		  var ez =   $('#image_id').data('elevateZoom');	
			$.fancybox(ez.getGalleryList());
		  return false;
		});
	})
</script>
