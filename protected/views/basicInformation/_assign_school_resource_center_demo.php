<?php
$this->breadcrumbs = array(
    'REGISTER NEW SCHOOL' => array('create'),
    'SCHOOL LISTS',
);

$this->menu = array(
    array('label' => 'List BasicInformation', 'url' => array('index')),
    array('label' => 'Create BasicInformation', 'url' => array('create')),
);


?>
<style type="text/css">
    #elePool,
#eleGroups {
    float:left;
    margin-right:30px;
    width:300px;
    border:1px solid grey;
    min-height:25px;
}
#eleGroups {
    min-height:300px;
}
#eleGroups .group div,
#elePool div {
    border:1px solid grey;
    background-color:lightcyan;
    line-height:25px;
    cursor: -webkit-grab;
    cursor: grab;
    text-indent:15px;
}
#eleGroups > div {
    position:relative;
    box-sizing: border-box;
    min-height:100px; 
    border:1px dashed lightgray;
    padding-top: 20px;
}
#eleGroups > div::after {
   position:absolute;
   top:1px;
   left:2px;
   font-size:12px;
   color: grey;
}
#eleGroups > div:nth-child(1)::after {
    content: "FOOD HERE"; 
}
#eleGroups > div:nth-child(2)::after {  
    content: "CARS HERE"; 
}
#eleGroups > div:nth-child(3)::after {  
    content: "ANIMALS HERE"; 
}
</style>

<div class="row-fluid form-horizontal well">
<h2><strong>SCHOOL RESOURCE CENTER</strong></h2>
<div id='elePool' class='sortme'>
  <div>Pizza <input type="hidden" name="name_"></div>
  <div>Fiat</div>
  <div>Jeep</div>
  <div>Cat</div>
  <div>Dog</div>
</div>
<div id='eleGroups'>
  <div class='sortme group group-food'></div>
  <div class='sortme group group-cars'></div>
  <div class='sortme group group-animals'></div>
</div>
    </div>

<script type="text/javascript">
    $(function(){
      $('.sortme').sortable({
            connectWith: '.sortme'
      }).disableSelection();

    });
</script>