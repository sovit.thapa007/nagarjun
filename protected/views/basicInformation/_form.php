<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'basic-information-form',
	'type' => 'horizontal',
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
		'enctype' => 'multipart/form-data',
	),
	'enableAjaxValidation' => false,
	));
?>





<div class="row-fluid">
	<div class="span12">
			<div class="box-content nopadding">
				<div class="control-group clear-fix">
					<h2><strong>SCHOOL INFORMATION FORM</strong> <p class="help-block pull-right">Fields with <span class="required">*</span> are required.</p></h2>
				</div>
				<div class="row-fluid">
				<div class="span6">

					<?php echo $form->dropDownListRow($model, 'type', Yii::app()->params['school_type'], array('class'=>'span12')); ?>

					<?php echo $form->textFieldRow($model, 'title', array('class' => 'span12', 'maxlength' => 250 ,'placeholder'=>'SCHOOL NAME')); ?>


					<?php echo $form->textFieldRow($model, 'schole_code', array('class' => 'span12', 'maxlength' => 100)); ?>

					<?php echo $form->dropDownListRow($model, 'ward_no', UtilityFunctions::WardDropDown(), ['class'=>'span12']); ?>

					<?php echo $form->dropDownListRow($model, 'resource_center_id',CHtml::listData(BasicInformation::model()->findAll('resource_center=:resource_center',[':resource_center'=>1]),'id','title'), ['class'=>'span12']); ?>
					
					</div>
					<div class="span6">
					<?php echo $form->fileFieldRow($model, 'logo', array('class' => 'span12', 'maxlength' => 250)); ?>
					<?php
						if(isset($model->logo) && !empty($model->logo)){
							?>
							<img src="<?php echo Yii::app()->request->baseUrl.'/images/logo/'.$model->logo; ?>" style="height: 200px; width: 200px; margin-left: 200px;">
							<?php
						}
					?>
					<?php echo $form->textFieldRow($model, 'tole', array('class' => 'span12', 'maxlength' => 250)); ?>
					<?php echo $form->textFieldRow($model, 'establish_year', array('class' => 'span12', 'maxlength' => 15 ,'required'=>'true','integer'=>'true')); ?>

					<?php echo $form->textFieldRow($model, 'contact_person', array('class' => 'span12', 'maxlength' => 100)); ?>
					<?php echo $form->textFieldRow($model, 'contact_person_number', array('class' => 'span12', 'maxlength' => 100)); ?>
					<?php if(!$model->isNewRecord)
						echo $form->dropDownListRow($model, 'status', [1=>'ACTIVE', 0=>'DE-ACTIVE'], ['class'=>'span12']); ?>
					</div>
					</div>
					<div class="row-fluid">

					<?php echo $form->textAreaRow($model, 'moto', array('rows' => 6, 'cols' => 50, 'class' => 'span12')); ?></div>
					<br/>
					<?php //echo $form->textFieldRow($model,'create_date',array('class'=>'span12'));  ?>

					<div class="form-actions text-center">
						<?php
						$this->widget('bootstrap.widgets.TbButton', array(
							'buttonType' => 'submit',
							'type' => 'primary',
							'label' => 'SAVE INFORMATION',
						));
						?>
					</div>
				</div>
			</div>
		</div>
	
</div>

<?php $this->endWidget(); ?>
