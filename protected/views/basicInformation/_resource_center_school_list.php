<?php
$this->breadcrumbs = array(
    'Resource Center List' => array('resourceCenterList'),
    'SCHOOL LISTS',
);
?>
<div class="form-horizontal well">

<h2><strong>RESOURCE CENTER : <?= ucwords($resource_center_information->title.' '.$resource_center_information->ward_no.'-'.$resource_center_information->tole); ?></strong></h2>
<table id='datatable' class="table table-bordered" style="width: 100%;">
        <thead>
            <tr>
                <th>Type</th>
                <th>Code</th>
                <th>Name</th>
                <th>Address</th>
                <th>Total Students</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(!empty($school_list)){
                    $sn = 1;
                    foreach ($school_list as $school_info) {
                        ?>
                        <tr>
                            <td><?= ucwords($school_info->type); ?></td>
                            <td><?= ucwords($school_info->schole_code); ?></td>
                            <td><a href="<?php echo Yii::app()->baseUrl.'/basicInformation/'.$school_info->id; ?>"><?= ucwords($school_info->title); ?></a></td>
                            <td><?= ucwords(Yii::app()->params['municipality_short'].'-'.$school_info->ward_no); ?></td>
                            <td><?= ucwords($school_info->total_student); ?></td>
                        </tr>
                        <?php
                        $sn++;
                    }
                }
            ?>
        </tbody>

        <tfoot>
            <tr>
                <th>Code</th>
                <th>Name</th>
                <th>Address</th>
                <th>Total Students</th>
            </tr>
        </tfoot>
    </table>
</div>