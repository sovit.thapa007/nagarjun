<?php
$this->breadcrumbs = array(
    'RESOURCE CENTER LIST',
);
?>
<div class="form-horizontal well">

<h2><strong>REGISTER SCHOOLS LIST</strong></h2>
<table id='datatable' class="table table-bordered" style="width: 100%;">
        <thead>
            <tr>
                <th>SN.</th>
                <th>Resource Center</th>
                <th>Total School</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(!empty($resource_center_information)){
                    $sn = 1;
                    foreach ($resource_center_information as $resource_center) {
                        $resouce_id = $resource_center->resource_center_id;
                        $resource_center_detail = BasicInformation::model()->findByPk($resouce_id);
                        ?>
                        <tr>
                            <td><?= $sn; ?></td>
                            <td><a href="<?php echo Yii::app()->baseUrl.'/basicInformation/resourceCenterSchool/'.$resouce_id; ?>"><?= $resource_center_detail ? ucwords($resource_center_detail->title. ' '.$resource_center_detail->tole) : ''; ?></a></td>
                            <td><?= $resource_center->id; ?></td>
                        </tr>
                        <?php
                        $sn++;
                    }
                }
            ?>
        </tbody>

        <tfoot>
            <tr>
                <th>SN.</th>
                <th>Resource Center</th>
                <th>Total School</th>
            </tr>
        </tfoot>
    </table>
</div>