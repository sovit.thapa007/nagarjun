<?php
$this->breadcrumbs = array(
	'SCHOOL WISE MARK REVIEW' => array('schoolMarksDetail'),
	'VIEW DETAIL',
);

$this->menu = array(
	array('label' => 'Add New School', 'url' => array('create')),
	array('label' => 'Update', 'url' => array('update', 'id' => $model->id)),
	array('label' => 'Remove', 'url' => array('delete', 'id' => $model->id)),
);
?>

<div class="form-horizontal well">
	<h2><strong><?= ucwords($model->title.', '.Yii::app()->params['municipality_short'].'-'.$model->ward_no); ?> && CODE : <?= $model->schole_code; ?></strong></h2>
	<?php
		if(isset($_GET['status']) && in_array($_GET['status'], [0,1])){
			$message = $_GET['status'] == 1 ? 'SUBJECT MARKS HAS BEEN INSERTED.' : 'MARKS CANNOT INSERT, PLEASE TRY AGAIN.';
			$class = $_GET['status'] == 1 ? 'alert alert-success' : 'alert alert-danger';
			?>
			<div class="<?= $class; ?>"> <?= $message; ?></div>
			<?php
		}
	?>
	<table class="table table-bordered" style="width:100%">
		<thead>
			<th> SUBJECT</th>
			<th> <a class="btn btn-middle btn-default" title="" data-toggle="tooltip" href="<?= Yii::app()->request->baseUrl.'/markObtained/ledgerAudit/'.$model->id ?>" data-original-title="Update" style='float: right;'><i class="fa fa-pencil"></i> AUDIT REPORT</a> &nbsp;&nbsp;&nbsp;
			<a class="btn btn-middle btn-default" title="" data-toggle="tooltip" href="<?= Yii::app()->request->baseUrl.'/basicInformation/PracticalSchoolSheet/?school_id='.$model->id.'&type=practical'?>" data-original-title="DOWNLOAD PRACTICAL SCHOOL SHEET" target='_blank'  style='float: right;'><i class="fa fa-eye"></i> DOWNLOAD PRACTICAL SCHOOL SHEET</a> 

			 </th>
		</thead>
		<tbody>
			<?php
				if(!empty($subject_information)){
					foreach ($subject_information as $subject_) {
						?>
						<tr>
							<td style="width: 20% !important;">
								<?= $subject_->title; ?>
							</td>
							<td style="width: 80% !important;">
								<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
										'enableAjaxValidation'=>false,
										'enableClientValidation' => true,
										'action'=>Yii::app()->request->baseUrl.'/markObtained/StudentList',
									)); ?>
										<div class="row-fluid">
											<div class="span3">
												<?php echo $form->dropDownListRow($mark_obtained,'options',array('insert_marks'=>'Insert Marks', 'download_student_list'=>'Download Student List'),array('class'=>'span12')); ?>
											</div>
											<div class="span3">

									        	<?php echo $form->dropDownListRow($mark_obtained,'arrange_by',array('student_id'=>'Registration Number', 'symbol_number'=>'Symbol Number','name'=>'Name'), array('class'=>'span12')); ?>
									        	</div>

												<div class="span3">

									        	<?php echo $form->dropDownListRow($mark_obtained,'insert_type',array('both'=>'Theory && Practical', 'theory'=>'Theory','practical'=>'Practical'), array('class'=>'span12')); ?>
									        	</div>

											<div class="span3" style="padding-top: 20px;">
											<?php echo $form->hiddenField($mark_obtained,'subject_id',array('value'=>$subject_->id)); ?>
											<?php echo $form->hiddenField($mark_obtained,'school_id',array('value'=>$model->id)); ?>
											<?php echo $form->hiddenField($mark_obtained,'prv_link',array('value'=>'basicInformation/'.$model->id)); ?>
												<button class="btn btn-default" type="submit"> Search </button>
											</div>

										</div>

									<?php $this->endWidget(); ?>
							</td>
						</tr>
						<?php
					}
				}
			?>
			<tr>
				<td colspan="2">
					<h4 class="text-center"> VIEW/DOWNLOAD RAW LEDGER </h4>
				<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
						'enableAjaxValidation'=>false,
						'enableClientValidation' => true,
				        'method' => 'get',
						'action'=>Yii::app()->request->baseUrl.'/markObtained/downloadStudentLedger',
				        'htmlOptions' => array(
				            'target' => '_blank',
				        ),
					)); ?>
						<div class="row-fluid">
							<div class="span4">
								<?php echo $form->dropDownListRow($mark_obtained,'insert_type',array('both'=>'Theory && Practical', 'theory'=>'Theory','practical'=>'Practical'), array('class'=>'span12')); ?>
							</div>
							<div class="span4">
					        	<?php echo $form->dropDownListRow($mark_obtained,'arrange_by',array('student_id'=>'Registration Number', 'symbol_number'=>'Symbol Number','name'=>'Name'), array('class'=>'span12')); ?>
					        	</div>

							<div class="span4" style="padding-top: 20px;">
							<?php echo $form->hiddenField($mark_obtained,'academic_year',array('value'=> UtilityFunctions::AcademicYear())); ?>
							<?php echo $form->hiddenField($mark_obtained,'school_id',array('value'=>$model->id)); ?>
							<?php echo $form->hiddenField($mark_obtained,'prv_link',array('value'=>'basicInformation/'.$model->id)); ?>
								<button class="btn btn-default" type="submit"> Search </button>
							</div>

						</div>

					<?php $this->endWidget(); ?>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<h4 class="text-center"> UPLOAD EXCEL RAW LEDGER </h4>
				<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
						'enableAjaxValidation'=>false,
						'enableClientValidation' => true,
				        'method' => 'post',
						'action'=>Yii::app()->request->baseUrl.'/markObtained/uploadStudentLedger',
				        'htmlOptions' => array(
				            'target' => '_blank',
							'enctype' => 'multipart/form-data',
				        ),
					)); ?>
						<div class="row-fluid">
							<div class="span4">
								<?php echo $form->fileFieldRow($mark_obtained,'upload_files',array('class'=>'span12','maxlength'=>200, 'required'=>'required')); ?>
									
							</div>
							<div class="span4">
							<?php echo $form->dropDownListRow($mark_obtained,'insert_type',array('both'=>'Theory && Practical', 'theory'=>'Theory','practical'=>'Practical'), array('class'=>'span12')); ?>
							</div>

							<div class="span4" style="padding-top: 20px;">
							<?php echo $form->hiddenField($mark_obtained,'academic_year',array('value'=> UtilityFunctions::AcademicYear())); ?>
							<?php echo $form->hiddenField($mark_obtained,'school_id',array('value'=>$model->id)); ?>
							<?php echo $form->hiddenField($mark_obtained,'prv_link',array('value'=>'basicInformation/'.$model->id)); ?>
								<button class="btn btn-default" type="submit"> Search </button>
							</div>

						</div>

					<?php $this->endWidget(); ?>
				</td>
			</tr>
		</tbody>
	</table>

</div>
<script type="text/javascript">
	
	jQuery(document).ready(function(){
		$("#image_id").elevateZoom({
		  zoomType				: "inner",
		  cursor: "crosshair"
		});

		//pass the images to Fancybox
		$("#image_id").bind("click", function(e) {  
		  var ez =   $('#image_id').data('elevateZoom');	
			$.fancybox(ez.getGalleryList());
		  return false;
		});
	})
</script>
