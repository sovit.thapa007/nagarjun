
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'POST',
	'type' => 'horizontal',
	'action' => Yii::app()->request->baseUrl.'/basicInformation/SaveSchoolSubject',
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
		'enctype' => 'multipart/form-data',
		'target' => '_blank',
	),
	'enableAjaxValidation' => false,
	));
?>
<div class="row-fluid">
	<div class="span12">
			<div class="box-content nopadding">
				<div class="control-group clear-fix">
					<h2><strong>CHOOSE SUJECT AND ADD SUBJECT</strong></h2>
				</div>
				<div class="row-fluid">
 					<?php echo $form->hiddenField($model,'school_id',array('value'=>$id)); ?>
					<div class="span6">
						<?php echo $form->dropDownListRow($model, 'subject_id',$subject_information, array('prompt'=>'Choose Subject'));?>
					</div>
				</div>
				<div class="form-actions text-center">
					<?php
					$this->widget('bootstrap.widgets.TbButton', array(
						'buttonType' => 'submit',
						'type' => 'primary',
						'label' => 'SUBMIT INFORMATION',
					));
					?>
				</div>
				</div>
			</div>
		</div>
	
</div>

<?php $this->endWidget(); ?>

<?php
	if(!empty($subject_list)){
		?>

		<table class="table table-bordered table-stripped">
			<thead>
			<th>SN</th>
			<th>Subject</th>
			<th>Status</th>
			<th>Action</th>
			</thead>
			<tbody>
			<?php
			$sn = 0;
			foreach ($subject_list as $sub_info) {
			?>
			<tr>
				<td><?= $sn+1; ?></td>
				<td><?= $sub_info->subject ? ucwords($sub_info->subject->title) : ''; ?></td>
				<td><?= $sub_info->status == 1 ?  'Active': 'De-Active'; ?></td>
				<td><a href="#" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Deacitve</a></td>
			</tr>
			<?php
			}
			?>
			</tbody>
		</table>
			<?php
	}
?>
