<div class="form-horizontal well">
<table class="table table-bordered" style="border: 1px solid #000; border-collapse: collapse;">
    <tr  style="border: 1px solid #000;">
        <td colspan="14" style="border: 1px solid #000; text-align: center;">
            <strong><?= strtoupper(UtilityFunctions::MunicipalLedgerHeader()); ?></strong>
        </td>
    </tr>
        <tr  style="border: 1px solid #000;">
            <td rowspan="3" style="border: 1px solid #000;">SN</td>
            <td rowspan="3" style="border: 1px solid #000;">Resource Center</td>
            <td colspan="4" style="border: 1px solid #000;">Community</td>
            <td colspan="4" style="border: 1px solid #000;">Institutional</td>
            <td colspan="4" style="border: 1px solid #000;">Total</td>
        </tr>
        <tr style="border: 1px solid #000;">
            <td rowspan="2" style="border: 1px solid #000;">No. of School</td>
            <td colspan="3" style="border: 1px solid #000;">Students</td>

            <td rowspan="2" style="border: 1px solid #000;">No. of School</td>
            <td colspan="3" style="border: 1px solid #000;">Students</td>

            <td rowspan="2" style="border: 1px solid #000;">No. of School</td>
            <td colspan="3" style="border: 1px solid #000;">Students</td>
        </tr>
        <tr style="border: 1px solid #000;">
            <td style="border: 1px solid #000;">Girls</td>
            <td style="border: 1px solid #000;">Boys</td>
            <td style="border: 1px solid #000;">Total</td>

            <td style="border: 1px solid #000;">Girls</td>
            <td style="border: 1px solid #000;">Boys</td>
            <td style="border: 1px solid #000;">Total</td>

            <td style="border: 1px solid #000;">Girls</td>
            <td style="border: 1px solid #000;">Boys</td>
            <td style="border: 1px solid #000;">Total</td>
        </tr>
        <?php
            if(!empty($resource_center_information)){
                $sn = 1;
                $tt_community_school_number = $tt_community_male_students = $tt_community_female_students = $tt_cmt_total_student = $tt_institutional_school_number = $tt_institutional_male_students = $tt_institutional_female_students = $tt_int_total_student = $tt_school_number = $tt_male_students = $tt_female_students = $tt_total_student = 0;
                foreach ($resource_center_information as $resource_center) {
                    $center_id = $resource_center->resource_center_id;
                    $resource = BasicInformation::model()->findByPk($resource_center->resource_center_id);
                    $community_school_number = isset($resource_center_detail[$center_id]['community_school_number']) ? $resource_center_detail[$center_id]['community_school_number'] : 0;
                    $tt_community_school_number += $community_school_number;
                    $community_male_students = isset($resource_center_detail[$center_id]['community_male_students']) ? $resource_center_detail[$center_id]['community_male_students'] : 0;
                    $tt_community_male_students += $community_male_students;
                    $community_female_students = isset($resource_center_detail[$center_id]['community_female_students']) ? $resource_center_detail[$center_id]['community_female_students'] : 0;
                    $tt_community_female_students += $community_female_students;
                    $cmt_total_student = $community_male_students + $community_female_students;
                    $tt_cmt_total_student += $cmt_total_student;
                    $institutional_school_number = isset($resource_center_detail[$center_id]['institutional_school_number']) ? $resource_center_detail[$center_id]['institutional_school_number'] : 0;
                    $tt_institutional_school_number += $institutional_school_number;

                    $institutional_male_students = isset($resource_center_detail[$center_id]['institutional_male_students']) ? $resource_center_detail[$center_id]['institutional_male_students'] : 0;
                    $tt_institutional_male_students += $institutional_male_students;
                    $institutional_female_students = isset($resource_center_detail[$center_id]['institutional_female_students']) ? $resource_center_detail[$center_id]['institutional_female_students'] : 0;
                    $tt_institutional_female_students += $institutional_female_students;
                    $int_total_student = $institutional_male_students + $institutional_female_students;
                    $tt_int_total_student += $int_total_student;

                    $rc_total_school = $community_school_number + $institutional_school_number;
                    $tt_school_number += $rc_total_school;
                    $rc_total_male_student = $community_male_students + $institutional_male_students;
                    $tt_male_students += $rc_total_male_student;
                    $rc_total_female_student = $community_female_students + $institutional_female_students;
                    $tt_female_students += $rc_total_female_student;
                    $rc_total_student = $cmt_total_student + $int_total_student;
                    $tt_total_student += $rc_total_student;

                    ?>
                    <tr>
                        <td style="border: 1px solid #000;"><?= $sn; ?></td>
                        <td style="border: 1px solid #000;"><?= $resource ? strtoupper($resource->title) : ''; ?></td>
                        <td style="border: 1px solid #000;"><?= $community_school_number; ?></td>
                        <td style="border: 1px solid #000;"><?= $community_female_students; ?></td>
                        <td style="border: 1px solid #000;"><?= $community_male_students; ?></td>
                        <td style="border: 1px solid #000;"><?= $cmt_total_student; ?></td>
                        <td style="border: 1px solid #000;"><?= $institutional_school_number; ?></td>
                        <td style="border: 1px solid #000;"><?= $institutional_female_students; ?></td>
                        <td style="border: 1px solid #000;"><?= $institutional_male_students; ?></td>
                        <td style="border: 1px solid #000;"><?= $int_total_student; ?></td>
                        <td style="border: 1px solid #000;"><?= $rc_total_school; ?></td>
                        <td style="border: 1px solid #000;"><?= $rc_total_female_student; ?></td>
                        <td style="border: 1px solid #000;"><?= $rc_total_male_student; ?></td>
                        <td style="border: 1px solid #000;"><?= $rc_total_student; ?></td>

                    </tr>
                    <?php
                    $sn ++;
                }
            }
        ?>
        <tr style="border: 1px solid #000;">
            <td colspan="2" style="border: 1px solid #000;"> TOTAL </td>
            <td style="border: 1px solid #000;"><?= $tt_community_school_number; ?></td>
            <td style="border: 1px solid #000;"><?= $tt_community_female_students; ?></td>
            <td style="border: 1px solid #000;"><?= $tt_community_male_students; ?></td>
            <td style="border: 1px solid #000;"><?= $tt_cmt_total_student; ?></td>
            <td style="border: 1px solid #000;"><?= $tt_institutional_school_number; ?></td>
            <td style="border: 1px solid #000;"><?= $tt_institutional_female_students; ?></td>
            <td style="border: 1px solid #000;"><?= $tt_institutional_male_students; ?></td>
            <td style="border: 1px solid #000;"><?= $tt_int_total_student; ?></td>
            <td style="border: 1px solid #000;"><?= $tt_school_number; ?></td>
            <td style="border: 1px solid #000;"><?= $tt_female_students; ?></td>
            <td style="border: 1px solid #000;"><?= $tt_male_students; ?></td>
            <td style="border: 1px solid #000;"><?= $tt_total_student; ?></td>
        </tr>
</table>
</div>

