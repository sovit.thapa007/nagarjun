<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'teacher-details-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>
<div class="row-fluid">
	<div class="span2"></div>
	<div class="span10">
			<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>200), array('class'=>'span12')); ?>

			<?php echo $form->textFieldRow($model,'address',array('class'=>'span5','maxlength'=>250)); ?>

			<?php echo $form->textFieldRow($model,'mobile_number',array('class'=>'span5','maxlength'=>10 )); ?>

			<?php echo $form->dropDownListRow($model,'sex',array("male"=>"male","female"=>"female","others"=>"others")); ?>
		</div>
	</div>
<!-- 
		<?php echo $form->textFieldRow($model,'school_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?> -->

	<div class="form-actions text-center">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'SAVE INFORMATION',
		)); ?>
</div>

<?php $this->endWidget(); ?>
