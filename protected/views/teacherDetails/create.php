<?php
$this->breadcrumbs=array(
	'Teacher Details'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List TeacherDetails','url'=>array('index')),
array('label'=>'Manage TeacherDetails','url'=>array('admin')),
);
?>

<h4 class="text-center"><strong>ADD TEACHER DETAILS</strong></h4><hr />

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>