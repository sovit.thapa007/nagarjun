<?php
$this->breadcrumbs=array(
	'Teacher Details'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List TeacherDetails','url'=>array('index')),
array('label'=>'Create TeacherDetails','url'=>array('create')),
array('label'=>'View TeacherDetails','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage TeacherDetails','url'=>array('admin')),
);
?>

<h1>Update TeacherDetails <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>