<?php
$this->breadcrumbs=array(
	'Teacher Details',
);

$this->menu=array(
array('label'=>'Create TeacherDetails','url'=>array('create')),
array('label'=>'Manage TeacherDetails','url'=>array('admin')),
);
?>

<h1>Teacher Details</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
