<?php
$this->breadcrumbs=array(
	'Teacher Details'=>array('index'),
	$model->name,
);

$this->menu=array(
array('label'=>'List TeacherDetails','url'=>array('index')),
array('label'=>'Create TeacherDetails','url'=>array('create')),
array('label'=>'Update TeacherDetails','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete TeacherDetails','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage TeacherDetails','url'=>array('admin')),
);
?>

<h4 class="text-center"><strong>View TeacherDetails #<?php echo $model->name; ?></strong></h4><hr />

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		//'id',
		'name',
		'address',
		'mobile_number',
		'sex',
		/*'school_id',
		'created_by',
		'created_date',*/
),
)); ?>
