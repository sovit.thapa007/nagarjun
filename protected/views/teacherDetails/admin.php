<?php
$this->breadcrumbs=array(
	'Teacher Details'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List TeacherDetails','url'=>array('index')),
array('label'=>'Create TeacherDetails','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('teacher-details-grid', {
data: $(this).serialize()
});
return false;
});
");
$school_array = array();
$school_information = BasicInformation::model()->findAll();
if(!empty($school_information)){
	foreach ($school_information as $school) {
		$school_array[$school->id] = $school->title;
	}
}
?>

<h4 class="text-center"><STRONG>TEACHER'S LIST</STRONG></h4><HR />
<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'teacher-details-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
		'name',
		'address',
		'mobile_number',
		'sex',
		array(
                'name'=>'school_id',
                'filter'=>$school_array,
                'value'=>function($data){
                    return $data->school_sec ? $data->school_sec->title : 'null';
                },
                'visible'=>User::model()->findByPk(Yii::app()->user->id)->superuser == 1 ?  true : false,
		),
		//'school_id',
		/*
		'created_by',
		'created_date',
		*/
array(
	'header'=>'ACTION',
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
