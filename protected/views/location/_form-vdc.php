
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'location-form',
	'enableAjaxValidation' => false,
	));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>


<?php echo $form->labelEx($model, 'parent_id', ['label' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))->title) : "NULL"]); ?>
<?php
echo $form->dropDownList($model, 'parent_id', CHtml::listData(Location::model()->findAll("parent_id=0"), 'id', 'title'), ['prompt' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))) ? "Select " . ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))->title) : " NULL ", 'class' => 'span5', 'data-url' => Yii::app()->createUrl("Location/dropdownlocationbranches")])
;
?>
<br />
<?php echo $form->labelEx($model, 'parent_id', ['label' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::ZONE))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::ZONE))->title) : " NULL "]); ?>
<?php echo $form->dropDownList($model, 'parent_id', array(), ['prompt' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::ZONE))) ? "Select " . ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::ZONE))->title) : " NULL ", 'class' => 'span5', 'id' => 'second_parent_id', 'data-url' => Yii::app()->createUrl("Location/dropdownlocationbranches")]); ?>
<br />
<?php echo $form->labelEx($model, 'parent_id', ['label' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DISTRICT))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DISTRICT))->title) : " NULL "]); ?>
<?php echo $form->dropDownList($model, 'parent_id', array(), ['prompt' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DISTRICT))) ? "Select " . ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DISTRICT))->title) : " NULL ", 'class' => 'span5', 'id' => 'third_parent_id']); ?>

<br />
<?php echo $form->textFieldRow($model, 'title', array('class' => 'span5', 'maxlength' => 250)); ?>

<?php //echo $form->textFieldRow($model,'nepali_date',array('class'=>'span5 nepali-calendar','maxlength'=>15 , 'id' => 'nepaliDate2'));  ?>


<?php echo $form->hiddenField($model, 'location_level', array('value' => 4)); ?>
<div class="form-actions">
	<?php
	$this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'type' => 'primary',
		'label' => $model->isNewRecord ? 'Create' : 'Save',
	));
	?>
</div>

<?php $this->endWidget(); ?>


<script>
    $('body').on('change', '#Location_parent_id', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        console.log(url);
        if (selected.length != '') {
            $.ajax({
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    console.log(response);

                    if (response.success) {
                        $('#second_parent_id').html(response.option);
                    } else {
                        $('#second_parent_id').html('');
                        $('#third_parent_id').html('');
                    }
                }
            });
        }
    });
    $('body').on('change', '#second_parent_id', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        console.log(url);
        if (selected.length != '') {
            $.ajax({
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#third_parent_id').html(response.option);
                    } else {
                        $('#third_parent_id').html('');
                    }
                }
            });
        }
    });

</script>