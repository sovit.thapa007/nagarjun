<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parent_id')); ?>:</b>
	<?php echo CHtml::encode($data->parent_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('level')); ?>:</b>
	<?php echo CHtml::encode($data->level); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('level_code')); ?>:</b>
	<?php echo CHtml::encode($data->level_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('location_slug')); ?>:</b>
	<?php echo CHtml::encode($data->location_slug); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nepali_date')); ?>:</b>
	<?php echo CHtml::encode($data->nepali_date); ?>
	<br />

	<?php /*
	  <b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	  <?php echo CHtml::encode($data->created_date); ?>
	  <br />

	 */ ?>

</div>