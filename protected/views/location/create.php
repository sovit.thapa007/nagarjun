<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/nepali.datepicker.min.js"></script>

<?php
$this->breadcrumbs = array(
	'Locations' => array('index'),
	'Create',
);

$this->menu = array(
	array('label' => 'List Location', 'url' => array('index')),
	array('label' => 'Manage Location', 'url' => array('admin')),
);
?>

<h2 class="text-center"><?php echo $title; ?></h2>

<?php echo $this->renderPartial($form_name, array('model' => $model)); ?>
