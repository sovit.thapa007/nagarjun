<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'location-form',
	'enableAjaxValidation' => false,
	));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>


<?php echo $form->labelEx($model, 'parent_id', ['label' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))->title) : "NULL"]); ?>
<?php
echo $form->dropDownList($model, 'parent_id', CHtml::listData(Location::model()->findAll("parent_id=0"), 'id', 'title'), ['prompt' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))) ? "Select " . ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))->title) : " NULL ", 'class' => 'span5'
	, 'options' => [$model->parent_id => ['selected' => true]]
])
;
?>
<!-- 

	<div class="row">
		<input type="hidden" name="count" value="1" />
        <div class="control-group" id="fields">
            <label class="control-label" for="field1">Nice Multiple Form Fields</label>
            <div class="controls" id="profs"> 
                <form class="input-append">
                    <div id="field"><input autocomplete="off" class="input" id="field1" name="prof1" type="text" placeholder="Type something" data-items="8"/><button id="b1" class="btn add-more" type="button">+</button></div>
                </form>
            <br>
            <small>Press + to add another form field :)</small>
            </div>
        </div>
	</div> -->
<?php echo $form->textFieldRow($model, 'title', array('class' => 'span5', 'maxlength' => 250)); ?>

<?php //echo $form->textFieldRow($model,'nepali_date',array('class'=>'span5 nepali-calendar','maxlength'=>15 , 'id' => 'nepaliDate2')); ?>


<?php echo $form->hiddenField($model, 'location_level', array('value' => 2)); ?>
<div class="form-actions">
	<?php
	$this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'type' => 'primary',
		'label' => $model->isNewRecord ? 'Create' : 'Save',
	));
	?>
</div><!-- 
<div class="input_fields_wrap">
    <button class="add_field_button">Add More Fields</button>
    <div><input type="text" name="mytext[]"></div>
</div> -->
<?php $this->endWidget(); ?>
<script type="text/javascript">
	$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
   
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
        }
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});

	$(document).ready(function(){
    var next = 1;
    $(".add-more").click(function(e){
        e.preventDefault();
        var addto = "#field" + next;
        var addRemove = "#field" + (next);
        next = next + 1;
        var newIn = '<input autocomplete="off" class="input form-control" id="field' + next + '" name="field' + next + '" type="text">';
        var newInput = $(newIn);
        var removeBtn = '<button id="remove' + (next - 1) + '" class="btn btn-danger remove-me" >-</button></div><div id="field">';
        var removeButton = $(removeBtn);
        $(addto).after(newInput);
        $(addRemove).after(removeButton);
        $("#field" + next).attr('data-source',$(addto).attr('data-source'));
        $("#count").val(next);  
        
            $('.remove-me').click(function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#field" + fieldNum;
                $(this).remove();
                $(fieldID).remove();
            });
    });
    

    
});

</script>
