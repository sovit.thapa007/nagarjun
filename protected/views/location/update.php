<?php
$this->breadcrumbs = array(
	'Locations' => array('index'),
	$model->title => array('view', 'id' => $model->id),
	'Update',
);

$this->menu = array(
	array('label' => 'List Location', 'url' => array('index')),
	array('label' => 'Create Location', 'url' => array('create')),
	array('label' => 'View Location', 'url' => array('view', 'id' => $model->id)),
	array('label' => 'Manage Location', 'url' => array('admin')),
);
?>

<h3 class="text-center">Update Location <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial($form_name, array('model' => $model)); ?>