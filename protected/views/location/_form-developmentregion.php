<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'location-form',
	'enableAjaxValidation' => false,
	));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model, 'title', array('class' => 'span5', 'maxlength' => 250)); ?>

<?php //echo $form->textFieldRow($model,'nepali_date',array('class'=>'span5 nepali-calendar','maxlength'=>15 , 'id' => 'nepaliDate2'));  ?>

<?php echo $form->hiddenField($model, 'location_level', array('value' => 1)); ?>

<?php echo $form->hiddenField($model, 'location_level', array('value' => 1)); ?>
<div class="form-actions">
	<?php
	$this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'type' => 'primary',
		'label' => $model->isNewRecord ? 'Create' : 'Save',
	));
	?>
</div>

<?php $this->endWidget(); ?>

