

<?php
echo $form->dropDownListRow($model, $prefix_temp . 'location_id', CHtml::listData(Location::model()->findAll("parent_id=0"), 'id', 'title'), [
	'prompt' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))) ? "Select " . ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))->title) : " NULL ",
	'class' => 'span12',
	'id' => 'Sec_Location_initail_id',
	'data-url' => Yii::app()->createUrl("Location/dropdownlocationbranches")], ['label' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))->title) : "NULL"])
;
?>

<?php echo $form->dropDownListRow($model, $prefix_temp . 'location_id', array(), ['prompt' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::ZONE))) ? "Select " . ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::ZONE))->title) : " NULL ", 'class' => 'span12', 'id' => 'Sec_second_parent_id', 'data-url' => Yii::app()->createUrl("Location/dropdownlocationbranches")], ['label' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::ZONE))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::ZONE))->title) : " NULL "]); ?>

<?php echo $form->dropDownListRow($model, $prefix_temp . 'location_id', array(), ['prompt' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DISTRICT))) ? "Select " . ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DISTRICT))->title) : " NULL ", 'class' => 'span12', 'id' => 'Sec_third_parent_id', 'data-url' => Yii::app()->createUrl("Location/dropdownlocationbranches")], ['label' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DISTRICT))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DISTRICT))->title) : " NULL "]); ?>

<?php
echo CHtml::hiddenField('filed_prefix', $prefix_temp);
echo CHtml::hiddenField('location_status', $status_temp);
echo CHtml::hiddenField('location_level', Location::LOCATION_LEVEL);
?>
<?php echo CHtml::hiddenField('initial_location_id_section', $locationid_temp, array('data-url' => Yii::app()->createUrl("location/locationHierarchy"))); ?>

<script type="text/javascript">

    $(function() {
        if ($('#location_status').val() == 1) {
            var this_ = $('#initial_location_id_section');
            var lvl = $('#location_level').val();
            var id_string = '';
            var int_lvl = parseInt(lvl) + 1;
            var location = this_.val();
            var url = this_.data('url');
            for (var i = 1; i < int_lvl; i++) {
                if (i == 1)
                    var id_string = 'Sec_Location_initail_id';
                if (i == 2)
                    var id_string = 'Sec_second_parent_id';
                if (i == 3)
                    var id_string = 'Sec_third_parent_id';
                $.ajax({
                    type: 'POST',
                    url: url,
                    async: false,
                    data: {location: location, level: i},
                    dataType: 'JSON',
                    success: function(response) {
                        if (response.success) {
                            $('#' + id_string).html(response.option);
                        }
                    }
                });
            }
            ;

        }
    });
    $('body').on('change', '#Sec_Location_initail_id', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        if (selected.length != '') {
            $.ajax({
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#Sec_second_parent_id').html(response.option);
                    } else {
                        $('#Sec_second_parent_id').html('');
                        $('#Sec_third_parent_id').html('');
                        $('#Sec_fourth_parent_id').html('');
                        $('#Sec_fifth_parent_id').html('');
                    }
                }
            });
        }
    });
    $('body').on('change', '#Sec_second_parent_id', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        console.log(url);
        if (selected.length != '') {
            $.ajax({
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#Sec_third_parent_id').html(response.option);
                    } else {
                        $('#Sec_third_parent_id').html('');
                        $('#Sec_fourth_parent_id').html('');
                        $('#Sec_fifth_parent_id').html('');
                    }
                }
            });
        }
    });
    $('body').on('change', '#Sec_third_parent_id', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        console.log(url);
        if (selected.length != '') {
            $.ajax({
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#Sec_fourth_parent_id').html(response.option);
                    } else {
                        $('#Sec_fourth_parent_id').html('');
                        $('#Sec_fifth_parent_id').html('');
                    }
                }
            });
        }
    });


    $('body').on('change', '#Sec_fourth_parent_id', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        console.log(url);
        if (selected.length != '') {
            $.ajax({
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#Sec_fifth_parent_id').html(response.option);
                    } else {
                        $('#Sec_fifth_parent_id').html('');
                    }
                }
            });
        }
    });

</script>