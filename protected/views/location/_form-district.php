
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'location-form',
	'enableAjaxValidation' => false,
	));
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>



<?php echo $form->labelEx($model, 'parent_id', ['label' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))->title) : "NULL"]); ?>
<?php
echo $form->dropDownList($model, 'parent_id', CHtml::listData(Location::model()->findAll("parent_id=0"), 'id', 'title'), ['prompt' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))) ? "Select " . ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))->title) : " NULL ", 'class' => 'span5', 'data-url' => Yii::app()->createUrl("Location/dropdownlocationbranches")])
;
?>
<br />
<?php echo $form->labelEx($model, 'parent_id', ['label' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::ZONE))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::ZONE))->title) : " NULL "]); ?>
<?php echo $form->dropDownList($model, 'parent_id', array(), ['prompt' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::ZONE))) ? "Select " . ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::ZONE))->title) : " NULL ", 'class' => 'span5', 'id' => 'second_parent_id', 'data-url' => Yii::app()->createUrl("Location/dropdownlocationbranches")]); ?>
<br />
<?php echo $form->textFieldRow($model, 'title', array('class' => 'span5', 'maxlength' => 250)); ?>

<?php
$status = $model->isNewRecord ? 0 : 1;
echo CHtml::hiddenField('location_status', $status);
?>
<?php echo $form->hiddenField($model, 'location_level', array('value' => 3, 'id' => 'location_level')); ?>
<?php echo CHtml::hiddenField('current_location', $model->id, array('data-url' => Yii::app()->createUrl("location/locationHierarchy"))); ?>

<div class="form-actions">
	<?php
	$this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'type' => 'primary',
		'label' => $model->isNewRecord ? 'Create' : 'Save',
	));
	?>
</div>

<?php $this->endWidget(); ?>


<script>


    $(function() {
        if ($('#location_status').val() == 1) {
            var this_ = $('#current_location');
            var location = this_.val();
            var url = this_.data('url');
            for (var i = 1; i < 3; i++) {
                if (i == 1)
                    var id_string = 'Location_parent_id';
                if (i == 2)
                    var id_string = 'second_parent_id';
                $.ajax({
                    type: 'POST',
                    url: url,
                    async: false,
                    data: {location: location, level: i},
                    dataType: 'JSON',
                    success: function(response) {
                        if (response.success) {
                            $('#' + id_string).html(response.option);
                        }
                    }
                });
            }
            ;

        }
    });

    $('body').on('change', '#Location_parent_id', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        console.log(url);
        if (selected.length != '') {
            $.ajax({
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    console.log(response);

                    if (response.success) {
                        $('#second_parent_id').html(response.option);
                    } else {
                        $('#second_parent_id').html('');
                    }
                }
            });
        }
    });

</script>