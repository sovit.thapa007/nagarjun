
<div class="row-fluid quick-actions">
	<h3 class="text-center">Location Dashboard</h3>
	<hr />

	<div class="span2 quick-action" style="margin-left: 20%"><span class="icon file"></span>
		<?php echo CHtml::link(!empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))->title) : "NULL", Yii::app()->createUrl("location/create/?key=development_region"), array('class' => '')); ?>
	</div>

	<div class=" span1 quick-action"> <span class="icon file"></span>
		<?php echo CHtml::link(!empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::ZONE))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::ZONE))->title) : "NULL", Yii::app()->createUrl("location/create/?key=zone")); ?>
	</div>
	<div class="span1 quick-action"> <span class="icon file"></span>
		<?php echo CHtml::link(!empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DISTRICT))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DISTRICT))->title) : "NULL", Yii::app()->createUrl("location/create/?key=district")); ?>
	</div>
	<div class="span2 quick-action"> <span class="icon file"></span>
		<?php echo CHtml::link(!empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::VDC))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::VDC))->title) : "NULL", Yii::app()->createUrl("location/create/?key=vdc")); ?>
	</div>

	<div class="span1 quick-action"> <span class="icon file"></span>
		<?php echo CHtml::link(!empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::WARD_NO))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::WARD_NO))->title) : "NULL", Yii::app()->createUrl("location/create/?key=wordno")); ?>
	</div>

</div>