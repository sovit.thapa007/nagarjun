
<!--  'options' => array($locationid=>array('selected'=>true)), -->
<?php echo $form->labelEx($model, 'location_id', ['label' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))->title) : "NULL"]); ?>
<?php
echo $form->dropDownList($model, $prefix . 'location_id', CHtml::listData(Location::model()->findAll("parent_id=0"), 'id', 'title'), ['prompt' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))) ? "Select " . ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DEVELOPMENT_REGION))->title) : " NULL ", 'class' => 'span5', 'id' => 'Location_initail_id', 'data-url' => Yii::app()->createUrl("Location/dropdownlocationbranches")])
;
?>
<br />
<?php echo $form->labelEx($model, 'location_id', ['label' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::ZONE))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::ZONE))->title) : " NULL "]); ?>
<?php echo $form->dropDownList($model, $prefix . 'location_id', array(), ['prompt' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::ZONE))) ? "Select " . ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::ZONE))->title) : " NULL ", 'class' => 'span5', 'id' => 'second_parent_id', 'data-url' => Yii::app()->createUrl("Location/dropdownlocationbranches")]); ?>
<br />
<?php echo $form->labelEx($model, 'location_id', ['label' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DISTRICT))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DISTRICT))->title) : " NULL "]); ?>
<?php echo $form->dropDownList($model, $prefix . 'location_id', array(), ['prompt' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DISTRICT))) ? "Select " . ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::DISTRICT))->title) : " NULL ", 'class' => 'span5', 'id' => 'third_parent_id', 'data-url' => Yii::app()->createUrl("Location/dropdownlocationbranches")]); ?>

<br />
<?php echo $form->labelEx($model, 'location_id', ['label' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::VDC))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::VDC))->title) : " NULL "]); ?>
<?php echo $form->dropDownList($model, $prefix . 'location_id', array(), ['prompt' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::VDC))) ? "Select " . ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::VDC))->title) : " NULL ", 'class' => 'span5', 'id' => 'fourth_parent_id', 'data-url' => Yii::app()->createUrl("Location/dropdownlocationbranches")]); ?>

<br />
<?php echo $form->labelEx($model, 'location_id', ['label' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::WARD_NO))) ? ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::WARD_NO))->title) : " NULL "]); ?>
<?php echo $form->dropDownList($model, $prefix . 'location_id', array(), ['prompt' => !empty(ConstantCode::model()->findByAttributes(array('loc_code' => Location::WARD_NO))) ? "Select " . ucwords(ConstantCode::model()->findByAttributes(array('loc_code' => Location::WARD_NO))->title) : " NULL ", 'class' => 'span5', 'id' => 'fifth_parent_id']); ?>

<?php
echo CHtml::hiddenField('location_status', $status);
echo CHtml::hiddenField('location_level', Location::LOCATION_LEVEL);
?>
<?php echo CHtml::hiddenField('initial_location_id', $locationid, array('data-url' => Yii::app()->createUrl("location/locationHierarchy"))); ?>
<script>
    $(function() {
        if ($('#location_status').val() == 1211) {
            var this_ = $('#initial_location_id');
            var lvl = $('#location_level').val();
            var id_string = '';
            var int_lvl = parseInt(lvl) + 1;
            var location = this_.val();
            var url = this_.data('url');
            for (var i = 1; i < int_lvl; i++) {
                if (i == 1)
                    var id_string = 'Location_initail_id';
                if (i == 2)
                    var id_string = 'second_parent_id';
                if (i == 3)
                    var id_string = 'third_parent_id';
                if (i == 4)
                    var id_string = 'fourth_parent_id';
                if (i == 5)
                    var id_string = 'fifth_parent_id';
                $.ajax({
                    type: 'POST',
                    url: url,
                    async: false,
                    data: {location: location, level: i},
                    dataType: 'JSON',
                    success: function(response) {
                        if (response.success) {
                            console.log('#' + id_string);
                            $('#' + id_string).html(response.option);
                        }
                    }
                });
            }
            ;

        }
    });
    $('body').on('change', '#Location_initail_id', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        if (selected.length != '') {
            $.ajax({
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#second_parent_id').html(response.option);
                    } else {
                        $('#second_parent_id').html('');
                        $('#third_parent_id').html('');
                        $('#fourth_parent_id').html('');
                        $('#fifth_parent_id').html('');
                    }
                }
            });
        }
    });
    $('body').on('change', '#second_parent_id', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        console.log(url);
        if (selected.length != '') {
            $.ajax({
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#third_parent_id').html(response.option);
                    } else {
                        $('#third_parent_id').html('');
                        $('#fourth_parent_id').html('');
                        $('#fifth_parent_id').html('');
                    }
                }
            });
        }
    });
    $('body').on('change', '#third_parent_id', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        console.log(url);
        if (selected.length != '') {
            $.ajax({
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#fourth_parent_id').html(response.option);
                    } else {
                        $('#fourth_parent_id').html('');
                        $('#fifth_parent_id').html('');
                    }
                }
            });
        }
    });


    $('body').on('change', '#fourth_parent_id', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        console.log(url);
        if (selected.length != '') {
            $.ajax({
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#fifth_parent_id').html(response.option);
                    } else {
                        $('#fifth_parent_id').html('');
                    }
                }
            });
        }
    });

</script>