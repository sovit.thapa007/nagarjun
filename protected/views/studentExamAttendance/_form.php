<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'student-exam-attendance-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'terminal_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'student_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'subject_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'is_present',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'school_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'approved_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'approved_date',array('class'=>'span5')); ?>

	<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
