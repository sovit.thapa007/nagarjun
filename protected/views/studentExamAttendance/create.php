<?php
$this->breadcrumbs=array(
	'Student Exam Attendances'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List StudentExamAttendance','url'=>array('index')),
array('label'=>'Manage StudentExamAttendance','url'=>array('admin')),
);
?>

<h1>Create StudentExamAttendance</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>