<?php
$this->breadcrumbs=array(
	'Student Exam Attendances'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List StudentExamAttendance','url'=>array('index')),
array('label'=>'Create StudentExamAttendance','url'=>array('create')),
array('label'=>'Update StudentExamAttendance','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete StudentExamAttendance','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage StudentExamAttendance','url'=>array('admin')),
);
?>

<h1>View StudentExamAttendance #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'academic_year',
		'terminal_id',
		'student_id',
		'subject_id',
		'is_present',
		'school_id',
		'created_by',
		'created_date',
		'approved_by',
		'approved_date',
),
)); ?>
