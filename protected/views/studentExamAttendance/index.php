<?php
$this->breadcrumbs=array(
	'Student Exam Attendances',
);

$this->menu=array(
array('label'=>'Create StudentExamAttendance','url'=>array('create')),
array('label'=>'Manage StudentExamAttendance','url'=>array('admin')),
);
?>

<h1>Student Exam Attendances</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
