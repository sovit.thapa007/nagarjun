<?php
$this->breadcrumbs=array(
	'Student Exam Attendances'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List StudentExamAttendance','url'=>array('index')),
array('label'=>'Create StudentExamAttendance','url'=>array('create')),
array('label'=>'View StudentExamAttendance','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage StudentExamAttendance','url'=>array('admin')),
);
?>

<h1>Update StudentExamAttendance <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>