<?php
$this->breadcrumbs=array(
	'Special Cases'=>array('index'),
	$model->title,
);

$this->menu=array(
array('label'=>'List SpecialCase','url'=>array('index')),
array('label'=>'Create SpecialCase','url'=>array('create')),
array('label'=>'Update SpecialCase','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete SpecialCase','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage SpecialCase','url'=>array('admin')),
);
?>

<h1>View SpecialCase #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'title',
		'description',
		'created_by',
		'created_date',
),
)); ?>
