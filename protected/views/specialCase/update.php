<?php
$this->breadcrumbs=array(
	'Special Cases'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List SpecialCase','url'=>array('index')),
array('label'=>'Create SpecialCase','url'=>array('create')),
array('label'=>'View SpecialCase','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage SpecialCase','url'=>array('admin')),
);
?>

<h1>Update SpecialCase <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>