<?php
$this->breadcrumbs=array(
	'Special Cases'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List SpecialCase','url'=>array('index')),
array('label'=>'Manage SpecialCase','url'=>array('admin')),
);
?>

<h1>Create SpecialCase</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>