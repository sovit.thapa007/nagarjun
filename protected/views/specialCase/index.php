<?php
$this->breadcrumbs=array(
	'Special Cases',
);

$this->menu=array(
array('label'=>'Create SpecialCase','url'=>array('create')),
array('label'=>'Manage SpecialCase','url'=>array('admin')),
);
?>

<h1>Special Cases</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
