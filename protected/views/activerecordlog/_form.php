<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'activerecordlog-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textAreaRow($model,'description',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

		<?php echo $form->textFieldRow($model,'controllerName',array('class'=>'span5','maxlength'=>250)); ?>

		<?php echo $form->textAreaRow($model,'action',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

		<?php echo $form->textFieldRow($model,'model',array('class'=>'span5','maxlength'=>45)); ?>

		<?php echo $form->textFieldRow($model,'idModel',array('class'=>'span5','maxlength'=>20)); ?>

		<?php echo $form->textFieldRow($model,'field',array('class'=>'span5','maxlength'=>45)); ?>

		<?php echo $form->textFieldRow($model,'creationdate',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'userid',array('class'=>'span5','maxlength'=>45)); ?>

		<?php echo $form->textFieldRow($model,'ipAddress',array('class'=>'span5','maxlength'=>250)); ?>

	<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
