
<?php
if(!empty($data_detail_array)){
    $school_id_array = isset($data_detail_array['school_id_array']) ?  $data_detail_array['school_id_array'] : null;
    $subject_id_array = isset($data_detail_array['subject_id_array']) ? $data_detail_array['subject_id_array'] : null;
    $subject_name_array = isset($data_detail_array['subject_name_array']) ? $data_detail_array['subject_name_array'] : null;
    $grade_ = isset($data_detail_array['grade_information']) ? $data_detail_array['grade_information'] : null;
    $data_information_array = isset($data_detail_array['data_information_array']) ? $data_detail_array['data_information_array'] : null;
    ?>

    <table style="border-collapse: collapse; width: 100%; border: 2px solid #000;" >
        <tr style="border: 2px solid #000;">
            <th colspan="<?= sizeof($subject_id_array) + 5; ?>" style="text-align: center;font-weight: bold; border: 2px solid #000;"> <?= strtoupper(UtilityFunctions::MunicipalLedgerHeader($academic_year)); ?>
            </th>
        </tr>
        <tr style="border: 2px solid #000;">
            <td style="border: 2px solid #000;">SN</td>
            <td style="border: 2px solid #000;">School Name</td>
            <td style="border: 2px solid #000;">School Code</td>
            <td style="border: 2px solid #000;">Grade</td>
            <td style="border: 2px solid #000;" colspan="<?= sizeof($subject_id_array); ?>">Subject Information</td>
            <td style="border: 2px solid #000;">Student NO.</td>
        </tr>
        <?php
            for ($j=0; $j < sizeof($school_id_array) ; $j++) {
                $school_id = $school_id_array[$j];
                
                $school_name_array = $data_detail_array['school_name_array'] ? $data_detail_array['school_name_array'] :null;
                $school_name = $school_name_array && isset($school_name_array[$school_id]['name']) ? $school_name_array[$school_id]['name'] : '';
                $school_code = $school_name_array && isset($school_name_array[$school_id]['code']) ? $school_name_array[$school_id]['code'] : '';
                ?>
                <tr style="border: 2px solid #000;">
                    <td rowspan="2" style="border: 2px solid #000;"><?= $j+1; ?></td>
                    <td rowspan="2" style="border: 2px solid #000;"><?= $school_name; ?></td>
                    <td rowspan="2" style="border: 2px solid #000;"><?= $school_code; ?></td>
                    <td style="border: 2px solid #000;"></td>
                        <?php
                        if($subject_id_array){
                            for ($i=0; $i < sizeof($subject_id_array) ; $i++) {
                                $name = isset($subject_name_array[$subject_id_array[$i]]) ? $subject_name_array[$subject_id_array[$i]] : '';
                                ?>
                                <td style="border: 2px solid #000;"><?= $name; ?></td>
                                <?php
                            }
                        }
                    ?>
                    </tr>
                    <tr style="border: 2px solid #000;">
                    <td style="border: 2px solid #000;">
                    <?php
                        for ($k=0; $k < sizeof($grade_) ; $k++) {
                        $grade = $grade_[$k]; 
                        echo $grade.'<br />';
                        } 
                    ?>
                    </td>
                    <?php
                    if($subject_id_array){
                    for ($i=0; $i < sizeof($subject_id_array) ; $i++) {
                        $subject_id = $subject_id_array[$i];
                        $total_student_number = 0;
                        ?>
                        <td style="border: 2px solid #000;">
                        <?php
                        for ($k=0; $k < sizeof($grade_) ; $k++) {
                            $grade = $grade_[$k]; 
                            $number = isset($data_information_array[$school_id][$subject_id][$grade]) ? $data_information_array[$school_id][$subject_id][$grade] : 0;
                            $total_student_number += $number;
                            $number = $number == 0 || !$number ? '-' : $number;
                            echo $number.'<br />';
                        }
                        ?>
                        </td>
                        <?php
                    }
                    }
                    ?> 
                    <td style="border: 2px solid #000;"><?= $total_student_number; ?></td> 
                </tr>
                <?php
            }

        ?>
    </table>
    <?php
}

?>