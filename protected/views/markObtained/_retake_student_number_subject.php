<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'subject-information-form',
	'enableAjaxValidation'=>false,
	'type' => 'horizontal',
	'enableClientValidation' => true,
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
	),
	'focus' => array($model, 'title'),
)); ?>

	<h2><strong>SEARCH FORM FOR RETAKE 2</strong></h2>
	<div class="row-fluid">
		<div class="span6">
        <?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span12')); ?></div>
        <div class="span6">
            <?php 
            echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(),'id','title'),array('class'=>'span12', 'prompt'=>'Select School', 'selected'=>1)); 
            ?> 
		</div>
	</div>
    <div class="row-fluid">
            <?php 
            echo $form->dropDownListRow($model,'options',['general'=>'General', 'pdf'=>'PDF DOWNLOAD'],array('class'=>'span12', 'selected'=>1)); 
            ?> 
    </div>
	<div class="form-actions text-center">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Submit Form',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<?php
if(!empty($retake_ledger_report)){

    $student_number_detail_array = $retake_ledger_report['student_number_detail_array'];
    $subject_id_array = $retake_ledger_report['subject_id_array'];
    $subject_header = $retake_ledger_report['subject_header'];
?>
<div class="row-fluid form-horizontal well" style="width: 100%; overflow: auto;">
    <h2><strong>LIST OF STUDENT'S OBTAINED D && E GRADE IN TOTAL, SUBJECT WISE (TO GET ADMISSION IN CLASS NINE THOSE STUDENTS SHOULD UPGRADE THEIR GARDE)</strong></h2>

    <table id='datatable' class="table table-bordered" style="margin-right: 10px !important; ">
        <thead>
            <tr>
                <th>SN.</th>
                <th>SUBJECT</th>
                <th>STUDENT'S NUMBER</th>
            </tr>
        </thead>
        <tbody>
        <?php
        if(!empty($subject_id_array)){
            for ($i=0; $i < sizeof($subject_id_array) ; $i++) { 
                $subject_id = $subject_id_array[$i];
            ?>
            <tr>
                <td><?= $i+1; ?></td>
                <td><?= isset($subject_header[$subject_id]) ? strtoupper($subject_header[$subject_id]) : ''; ?></td>
                <td><a href="<?= Yii::app()->request->baseUrl; ?>/markObtained/retakeSubjectWiseDetail?MarkObtained%5Bacademic_year%5D=<?= $academic_year; ?>&MarkObtained%5Bsubject_id%5D=<?= $subject_id; ?>"> <?= isset($student_number_detail_array[$subject_id]) ? strtoupper($student_number_detail_array[$subject_id]) : ''; ?> </a></td>
            </tr>
            <?php
            }
        }
        ?>

        </tbody>

        <tfoot>
            <tr>
                <th>SN.</th>
                <th>SUJECT</th>
                <th>STUDENT'S NUMBER</th>
            </tr>
        </tfoot>
    </table>
</div>
<?php
    }
?>