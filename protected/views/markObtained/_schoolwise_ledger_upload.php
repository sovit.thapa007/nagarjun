<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'subject-information-form',
	'enableAjaxValidation'=>false,
	'enableClientValidation' => true,
	'htmlOptions' => array(
		'class' => 'form-horizontal well form-horizontal',
		'enctype' => 'multipart/form-data',
	),
	'focus' => array($model, 'title'),
)); ?>
<h2><strong>UPLOAD EXCEL FILE OF MARKS</strong></h2>
<div class="row-fluid">
	<div class="span4">
	<?php echo $form->fileFieldRow($model,'upload_files',array('class'=>'span12','maxlength'=>200, 'required'=>'required')); ?></div>
	<div class="span4">
	<?php echo $form->dropDownListRow($model,'insert_type',array('both'=>'Theory && Practical', 'theory'=>'Theory','practical'=>'Practical'), array('class'=>'span12')); ?>
	</div>
	<div class="span4">

        <?php
        if(UtilityFunctions::ShowSchool()){
        echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => Yii::app()->params['select_'])),'id',Yii::app()->params['select_']),array('class'=>'span12', 'prompt'=>'Select School', 'selected'=>1)); 
        }else{
            $user_information = UtilityFunctions::UserInformations();
            $school_id = isset($user_information['school_id']) ? $user_information['school_id'] : null;
            echo $form->hiddenField($model,'school_id',array('value'=>$school_id));
        }
        ?> </div>
	</div>

	<div class="form-actions text-center">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'IMPORT DATA',
		)); ?>
	</div>

<?php $this->endWidget(); ?>


<?php
    if(!empty($excel_array_data) && !empty($school_information)){
    	?>
    	<div class="form-horizontal well form-horizontal" style="overflow-y: scroll;">
    		<h2><STRONG>REVIEW LEDGER MARKS : <?= $school_information ? ucwords($school_information->title.', '.Yii::app()->params['municipality_short'].'-'.$school_information->ward_no) : ''; ?>  && CODE : <?= $school_information ?  $school_information->schole_code : '';?></STRONG></h2>
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'bulk-mark-insert-form',
		'enableAjaxValidation'=>false,
		'enableClientValidation' => true,
		'action'=> Yii::app()->request->baseUrl.'/MarkObtained/insertBulkLegerMarks',
		'htmlOptions' => array(
			'class' => 'form-horizontal well form-horizontal',
			'enctype' => 'multipart/form-data',
		),
		'focus' => array($model, 'title'),
	)); ?>
		    <table class="table table-hover table-bordered">
		        <thead>
		            <th>SN</th>
		            <?php
	        		for ($i=0; $i <  sizeof($subjectColumnHeader); $i++) { 
		            	?>
		            	<th><?= $subjectColumnHeader[$i]; ?></th>
		            	<?php
		            }
		            ?>
		        </thead>
		        <?php
		        for ($j=0; $j < sizeof($excel_array_data) ; $j++) { 
		        	?>
		        	<tr>
		        		<td><?= $j+1; ?></td>
		        		<?php
		        		for ($k=0; $k <  sizeof($subjectColumnHeader); $k++) { 
		        			$column = $subjectColumnHeader[$k];
		        			$student_id = isset($excel_array_data[$j]['student_id']) ? $excel_array_data[$j]['student_id'] : null;
		        			if(in_array($column, ['student_id','name','registration_number','symbol_number'])){
		        				if($column=='student_id')
		        					echo "<input type='hidden' name='student_id_[]' value='".$student_id."' >";
		        				?>
		        				<td><?= isset($excel_array_data[$j][$column]) ? $excel_array_data[$j][$column] : ''; ?></td>
		        				<?php
		        			}else{
		        				$subjectInformationArray = UtilityFunctions::SubjectFM($column);
		        				$subject_nature = isset($subjectInformationArray['subject_nature']) ?  $subjectInformationArray['subject_nature'] : 1; 
		        				$subject_id = isset($subjectInformationArray['subject_id']) ? $subjectInformationArray['subject_id'] : null;
		        				$fm = isset($subjectInformationArray['fm']) ? $subjectInformationArray['fm'] : 100;
		        				$value = isset($excel_array_data[$j][$column]) ? $excel_array_data[$j][$column] : '';
		        				if($subject_nature == 3 && !$default_subject){
		        					if(isset($students_optional_subjects[$student_id][$column]) && $students_optional_subjects[$student_id][$column]){
		        						?>
					        			<td>
					        				<input type="number" name="<?= $student_id.'_'.$subject_id.'_'.$column; ?>" value="<?= $value;  ?>" max="<?= $fm; ?>" placeholder="<?= 'FM. : '.$fm; ?>">
					        			</td>
		        						<?php
		        					}else{
		        						?>
					        			<td>
					        			</td>
		        						<?php
		        					}

		        				}else{
			        			?>
			        			<td>
			        				<input type="number" name="<?= $student_id.'_'.$subject_id.'_'.$column; ?>" value="<?= $value;  ?>" max="<?= $fm; ?>" placeholder="<?= 'FM. : '.$fm; ?>">
			        			</td>
			        			<?php
		        				}

		        			}
		        		}
		        		?>
		        	</tr>
		        	<?php
		        }
		        ?>
		    </table>
		    <input type="hidden" name="previous_link" value="<?= $previous_link; ?>">
		    <input type="hidden" name="insert_type" value="<?= $insert_type; ?>">
		    <input type="hidden" name="school_id" value="<?= $school_id; ?>">
			<div class="form-actions text-center">
			<?php $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'submit',
					'type'=>'primary',
					'label'=>'SUBMIT LEDGER MARKS',
				)); ?>
			</div>

		<?php $this->endWidget(); ?>
	</div>
<?php
	}
?>