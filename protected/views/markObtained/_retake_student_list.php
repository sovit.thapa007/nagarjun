<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'subject-information-form',
	'enableAjaxValidation'=>false,
	'type' => 'horizontal',
	'enableClientValidation' => true,
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
	),
	'focus' => array($model, 'title'),
)); ?>

	<h2><strong>SEARCH FORM FOR RETAKE</strong></h2>
	<div class="row-fluid">
		<div class="span6">
        <?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span12', 'required'=>'required')); ?></div>
        <div class="span6">
            <?php 
            echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(),'id','title'),array('class'=>'span12', 'prompt'=>'Select School', 'selected'=>1)); 
            ?> 
		</div>
	</div>
	<div class="form-actions text-center">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Submit Form',
		)); ?>
	</div>

<?php $this->endWidget(); ?>


<?php
if(!empty($retake_ledger_report)){
    $student_mark_detail_array = $retake_ledger_report['student_mark_detail_array'];
    $student_array = $retake_ledger_report['student_array'];
    $student_detail_array = $retake_ledger_report['student_detail_array'];
    $subject_header = $retake_ledger_report['subject_header'];
?>
<div class="row-fluid form-horizontal well" style="width: 100%; overflow: auto;">
    <h2><strong>RETAKE STUDENT'S LISTS </strong></h2>
	<table id='datatable' class="table table-bordered" style="margin-right: 10px !important; ">
        <thead>
            <tr>
                <th>School</th>
                <th>Code</th>
                <th>Student Name</th>
                <th>Symbol No.</th>
                <?php
                for ($j=0; $j < sizeof($subject_header) ; $j++) { 
                ?>
                <th><?= $subject_header[$j]; ?></th>
                <?php
                }
                ?>
                <th>Retake Sub. No.</th>
            </tr>
        </thead>
        <tbody>
    	<?php
		if(!empty($student_array)){
            for ($i=0; $i < sizeof($student_array) ; $i++) { 
                $student_id = $student_array[$i];
                $total_retake_subject = isset($student_mark_detail_array[$student_id]['subject']) ? sizeof($student_mark_detail_array[$student_id]['subject']) : 0 ;
			?>
			<tr>
				<td><?= isset($student_detail_array[$student_id]['school']) ? $student_detail_array[$student_id]['school'] : ''; ?></td>
                <td><?= isset($student_detail_array[$student_id]['school_code']) ? $student_detail_array[$student_id]['school_code'] : ''; ?></td>
                <td><?= isset($student_detail_array[$student_id]['name']) ? $student_detail_array[$student_id]['name'] : ''; ?></td>
                <td><?= isset($student_detail_array[$student_id]['symbol_number']) ? $student_detail_array[$student_id]['symbol_number'] : ''; ?></td>
                <?php
                $retake_number = 0 ;
                for ($k=0; $k < sizeof($subject_header) ; $k++) { 
                    $subject_hd = $subject_header[$k];
                    $number_info = isset($student_mark_detail_array[$student_id][$subject_hd]) ? $student_mark_detail_array[$student_id][$subject_hd] : '';
                ?>
                <td><?= $number_info; ?></td>
                <?php
                }
                ?>
                <td><?= $total_retake_subject; ?></td>
            </tr>
			<?php
			}
		}
    	?>

        </tbody>

        <tfoot>
            <tr>
                <th>School</th>
                <th>Code</th>
                <th>Student Name</th>
                <th>Symbol No.</th>
                <?php
                for ($l=0; $l < sizeof($subject_header) ; $l++) { 
                ?>
                <th><?= $subject_header[$l]; ?></th>
                <?php
                }
                ?>
                <th>Retake Sub. No.</th>
            </tr>
        </tfoot>
    </table>
</div>
<?php
	}
?>