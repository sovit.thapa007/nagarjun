
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    //'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'type' => 'horizontal',
    'htmlOptions' => array(
        'class' => 'form-horizontal well',
    ),
)); ?>
<h2><strong>REQUIREMENT FORM TO DOWNLOAD EXCEL OF STUDENT LEDGER</strong></h2>
<div class="row-fluid">
    <div class="span6">
        <?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span12')); ?></div>
    <div class="span6">
        <?php echo $form->dropDownListRow($model,'arrange_by',array('symbol_number' => 'Symbol Number','name'=>'Name','id'=>'Register Number'), array('class'=>'span12')); ?>
    </div>
</div>
    <div class="row-fluid">
        <?php
        if(UtilityFunctions::ShowSchool()){
        echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => Yii::app()->params['select_'])),'id',Yii::app()->params['select_']),array('class'=>'span12', 'prompt'=>'Select School', 'selected'=>1)); 
        }else{
            $user_information = UtilityFunctions::UserInformations();
            $school_id = isset($user_information['school_id']) ? $user_information['school_id'] : null;
            echo $form->hiddenField($model,'school_id',array('value'=>$school_id));
        }
        ?> 
        <?php echo $form->hiddenField($model,'terminal_id',array('value'=>1)); ?>
    </div>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'=>'primary',
            'label'=>'Search',
        )); ?>
    </div>

<?php $this->endWidget(); ?>


<?php
if(isset($column) && !empty($column)){
?>
    <div class="form-horizontal well form-horizontal">
        <h2><strong><?= $school_information ? ucwords($school_information->title) : '';  ?> : <?= $academic_year; ?></strong> </h2>
        <table class="table table-striped table-bordered" style="width: 100%;">
            <tbody>
                <tr>
                    <td> </td>
                    <td rowspan="5">
                       <!--  <table class="table table-striped table-bordered">
                            <thead>
                                <td style="width: 30%;">Subject</td>
                                <td style="width: 10%;">Th. FM.</td>
                                <td style="width: 10%;">Pr. FM.</td>
                                <td style="width: 30%;">Subject</td>
                                <td style="width: 10%;">Th. FM.</td>
                                <td style="width: 10%;">Pr. FM.</td>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= isset($subject_information[0]['name']) ? ucwords($subject_information[0]['name']) : ''; ?></td>
                                    <td><?= isset($subject_information[0]['theory_fm']) ? ucwords($subject_information[0]['theory_fm']) : ''; ?></td>
                                    <td><?= isset($subject_information[0]['practical_fm']) ? ucwords($subject_information[0]['practical_fm']) : ''; ?></td>
                                    <td><?= isset($subject_information[1]['name']) ? ucwords($subject_information[1]['name']) : ''; ?></td>
                                    <td><?= isset($subject_information[1]['theory_fm']) ? ucwords($subject_information[1]['theory_fm']) : ''; ?></td>
                                    <td><?= isset($subject_information[1]['practical_fm']) ? ucwords($subject_information[1]['practical_fm']) : ''; ?></td>
                                </tr>
                                <tr>
                                    <td><?= isset($subject_information[2]['name']) ? ucwords($subject_information[2]['name']) : ''; ?></td>
                                    <td><?= isset($subject_information[2]['theory_fm']) ? ucwords($subject_information[2]['theory_fm']) : ''; ?></td>
                                    <td><?= isset($subject_information[2]['practical_fm']) ? ucwords($subject_information[2]['practical_fm']) : ''; ?></td>
                                    <td><?= isset($subject_information[3]['name']) ? ucwords($subject_information[3]['name']) : ''; ?></td>
                                    <td><?= isset($subject_information[3]['theory_fm']) ? ucwords($subject_information[3]['theory_fm']) : ''; ?></td>
                                    <td><?= isset($subject_information[3]['practical_fm']) ? ucwords($subject_information[3]['practical_fm']) : ''; ?></td>
                                </tr>
                                <tr>
                                    <td><?= isset($subject_information[4]['name']) ? ucwords($subject_information[4]['name']) : ''; ?></td>
                                    <td><?= isset($subject_information[4]['theory_fm']) ? ucwords($subject_information[4]['theory_fm']) : ''; ?></td>
                                    <td><?= isset($subject_information[4]['practical_fm']) ? ucwords($subject_information[4]['practical_fm']) : ''; ?></td>
                                    <td><?= isset($subject_information[5]['name']) ? ucwords($subject_information[5]['name']) : ''; ?></td>
                                    <td><?= isset($subject_information[5]['theory_fm']) ? ucwords($subject_information[5]['theory_fm']) : ''; ?></td>
                                    <td><?= isset($subject_information[5]['practical_fm']) ? ucwords($subject_information[5]['practical_fm']) : ''; ?></td>
                                </tr>
                                <tr>
                                    <td><?= isset($subject_information[6]['name']) ? ucwords($subject_information[6]['name']) : ''; ?></td>
                                    <td><?= isset($subject_information[6]['theory_fm']) ? ucwords($subject_information[6]['theory_fm']) : ''; ?></td>
                                    <td><?= isset($subject_information[6]['practical_fm']) ? ucwords($subject_information[6]['practical_fm']) : ''; ?></td>
                                    <td><?= isset($subject_information[7]['name']) ? ucwords($subject_information[7]['name']) : ''; ?></td>
                                    <td><?= isset($subject_information[7]['theory_fm']) ? ucwords($subject_information[7]['theory_fm']) : ''; ?></td>
                                    <td><?= isset($subject_information[7]['practical_fm']) ? ucwords($subject_information[7]['practical_fm']) : ''; ?></td>
                                </tr>
                                <tr>
                                    <td><?= isset($subject_information[8]['name']) ? ucwords($subject_information[8]['name']) : ''; ?></td>
                                    <td><?= isset($subject_information[8]['theory_fm']) ? ucwords($subject_information[8]['theory_fm']) : ''; ?></td>
                                    <td><?= isset($subject_information[8]['practical_fm']) ? ucwords($subject_information[8]['practical_fm']) : ''; ?></td>


                                    <td><?= isset($subject_information[9]['name']) ? ucwords($subject_information[9]['name']) : ''; ?></td>
                                    <td><?= isset($subject_information[9]['theory_fm']) ? ucwords($subject_information[9]['theory_fm']) : ''; ?></td>
                                    <td><?= isset($subject_information[9]['practical_fm']) ? ucwords($subject_information[9]['practical_fm']) : ''; ?></td>
                                </tr>
                            </tbody>
                        </table>
 -->
                    </td>
                </tr>
                <tr>
                    <td> School Code  :  <?= $school_information ?  $school_information->schole_code : '' ?></td>
                </tr>
                <tr>
                    <td> Resource Center  :  <?= $resorce_center ?  ucwords($resorce_center->title) : '' ?></td>
                </tr>
                <tr>
                    <td> Address  :  <?= $school_information ?  ucwords($school_information->ward_no.' '.$school_information->tole) : '' ?></td>
                </tr>
                <tr>
                    <td> Total Student  :  <?= $total_student; ?></td>
                </tr>

            </tbody>
        </table>
        <?php
            echo CHtml::linkButton('DOWNLOAD EXCEL',array(
                'type'=>'submit',
                'method'=>'POST',
                'class'=>'btn btn-primary',
                'confirm'=>"Do you want to download excel file",
                'params'=> array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken)));
        ?>
        <div class="well row-fulid" style="overflow: auto; border: 2px solid #000;">
        <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'id'=>'indfeerecord-grid',
                'dataProvider'=>$StudentList,
                'htmlOptions'=>array('class'=>'table table-striped table-bordered', 'type'=>'raw'),
                'columns'=>$column,
        ));
        ?>
        </div>
    </div>
<?php
}
if(isset($_POST) && !empty($_POST))
{
    $this->widget('EExcelView', array(
     'dataProvider'=>new CArrayDataProvider(
        $student_information,array(
            'keyField'=>'name'
            )),
            'grid_mode'=>'export',
            'title'=>'Result Legder',
            'filename'=>$academic_year.' : '.$school_information->schole_code.'_'.$school_information->title.' Raw Marks Legder  ('.date('Y-m-d').')',
            'stream'=>true,
            'autoWidth'=>false,
            'exportType'=>'Excel2007',
            'columns'=>$column,
        ) );

}
?>

<script type="text/javascript">

    $('body').on('change', '#school_level', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        if (selected.length > 0) {
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#program_level').html(response.option);
                    } else {
                        $('#program_level').html('');
                        $('#class_id').html('');
                    }
                }
            });

            var qual_url = $('#qualification_url').val();
            if (typeof qual_url != "undefined"){
                var url_2 = qual_url + '/key/' + selected;
                $.ajax({
                    type: 'POST',
                    url: url_2,
                    dataType: 'JSON',
                    success: function(response) {
                        $('#qualification_details_1').html(response.view);
                    }
                });
            }
        }
    });


    $('body').on('change', '#program_level', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        if (selected.length > 0) {
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#class_id').html(response.option);
                    } else {
                        $('#class_id').html('');
                    }
                }
            });
        }
    });


    $('body').on('change', '#class_id', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        if (selected.length > 0) {
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#termnial_id').html(response.option);
                    } else {
                        $('#termnial_id').html('');
                    }
                }
            });
        }
    });
</script>
