<?php
$this->breadcrumbs = array(
	'Add New School' => array('create'),
	'Manage',
);

$this->menu = array(
	array('label' => 'List BasicInformation', 'url' => array('index')),
	array('label' => 'Create BasicInformation', 'url' => array('create')),
);


?>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet">

<script type="text/javascript">


	$(document).ready(function() {
  	$('#example thead th').each( function () {
  		var title = $('#example tfoot th').eq( $(this).index() ).text();
    	$(this).html( '<input class="span12" type="text" placeholder="'+title+'" />' );
    } );
    $('#example').DataTable( {
    	lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],
          "ajax": {
		    "url": "<?= Yii::app()->baseUrl; ?>/basicInformation/adminDatatable",
		  },
        dom: 'Bfrtip',
        buttons: [
            'pageLength','copy', 'csv', 'excel', 'pdf', 'print'
        ],

        //"order": [[ 4, 'asc' ]]
    } );

	var table = $('#example').DataTable();
    // Apply the search
	table.columns().eq(0).each(function(colIdx) {
	    $('input', table.column(colIdx).header()).on('keyup change', function() {
	        table
	            .column(colIdx)
	            .search(this.value)
	            .draw();
	    });
	 
	    $('input', table.column(colIdx).header()).on('click', function(e) {
	        e.stopPropagation();
	    });
	});
} );
</script>
<style type="text/css">
    
.dataTables_wrapper .dataTables_filter {
float: right;
text-align: right;
visibility: hidden;
}
tfoot{
    visibility: hidden;
}
</style>

<ul class="breadcrumb" style="float: right;">
  <li><a href="<?= Yii::app()->baseUrl.'/dashboard'; ?>">Home</a> / </li>
  <li class="active">Users List's</li>
</ul>
<H3 class="text-center"><strong>SCHOOL LIST'S</strong></H3>
<div class="row-fluid">
    <a href="<?= Yii::app()->baseUrl; ?>/basicInformation/create" class="btn btn-sm btn-primary" style="float: right;"> <i class="fa fa-plus"> </i> ADD NEW SCHOOL</a>
<table id="example" class="display items table" style="width:100%">
        <thead>
            <tr>
                <th>Type</th>
                <th>Name</th>
                <th>Code</th>
                <th>Establish Year</th>
                <th>Ward No.</th>
                <th>Tole</th>
            </tr>
        </thead>

        <tfoot>
            <tr>
                <th>Type</th>
                <th>Name</th>
                <th>Code</th>
                <th>Establish Year</th>
                <th>Ward No.</th>
                <th>Tole</th>
            </tr>
        </tfoot>
    </table>
</div>

