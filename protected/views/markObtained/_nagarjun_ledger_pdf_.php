<style type="text/css">
    tr td{
    page-break-inside: avoid;
    white-space: nowrap;
}
</style>
<?php
    $tr_pr_colspan = ($result_ == 'grade') ? 4 : 3;
    $tr_no_pr_colspan = ($result_ == 'grade') ? 2 : 1;
    if($result_ == 'both'){
        $tr_pr_colspan = 7;
        $tr_no_pr_colspan = 3;
    }
    $column = isset($subject_information['column']) ? $subject_information['column'] : null;
    $column = $column ? array_values(array_diff($column, ['school_name','school_code'])) : [];
    $subject_information_ = isset($subject_information['subject_detail']) ? $subject_information['subject_detail'] : [];
    $colspan = $result_ == 'both' ?  '32' : '16';
    $rowspan = $result_ == 'both' ?  '3' : '2';
    $colspan_both = $result_ == 'both' ?  '2' : '1';
?>

<div class="row-fulid" style="overflow: auto; font-family:Times New Roman;">
    <table class="display items table" style="border-collapse: collapse;" >
        <thead>
            <tr>
                <th colspan="<?= sizeof($column); ?>" style="font-size: 12pt !important;text-align: center;"> <?= strtoupper(UtilityFunctions::MunicipalLedgerHeader($academic_year)); ?> <div style="font-size:18pt !important;"> GRADE LEDGER </div>
                </th>
            </tr>
            <tr>
                <td colspan="<?= sizeof($column); ?>"> <hr />
                </td>
            </tr>

            <tr>
                <td colspan="<?= $colspan; ?>" style="font-size: 10pt !important;">School Name : <?= $school_information ?  strtoupper($school_information->title.', '.Yii::app()->params['municipality_short'].'-'.$school_information->ward_no) : ''; ?></td>
                <td colspan="<?= $colspan/2; ?>" style="font-size: 10pt !important;">Type : <?= $school_information ? strtoupper(str_replace("_"," / ",$school_information->type)) : ''; ?></td>
                <td colspan="<?= 3+sizeof($column) - ($colspan + $colspan/2);  ?>" style="font-size: 10pt !important; text-align: right;">Result : <?= $result_date; ?></td>
            </tr>
            <tr>
                <td rowspan="<?= $rowspan; ?>" style="border:0.3px solid black;">SN</td>
                <td rowspan="<?= $rowspan; ?>" style="border:0.3px solid black; padding: 5px;">Symbol No.</td>
                <td rowspan="<?= $rowspan; ?>" style="border:0.3px solid black; padding: 5px;">&nbsp;StudentName&nbsp;</td>
                <td rowspan="<?= $rowspan; ?>" style="border:0.3px solid black; padding: 5px;">&nbsp;DOB&nbsp;</td>
                <?php
                for ($i=0; $i < sizeof($subject_information_) ; $i++) { 
                    $subject_ = explode('-', $subject_information_[$i]);
                    $name = isset($subject_[0]) ? $subject_[0] : '';
                    $is_practical = isset($subject_[2]) && $subject_[2] != 0 ? 1 : 0;
                    $border_style = $is_practical ? 'border:0.3px solid black;font-size: 9pt !important;' : 'border-top:0.3px solid black;font-size: 9pt !important;';
                    ?>
                    <td style="<?= $border_style; ?>;" colspan="<?= $is_practical == 1 ?  $tr_pr_colspan : $tr_no_pr_colspan ; ?>"><?= $name; ?></td>
                    <?php
                }
                    ?>
                <?php
                    if($result_ != 'general'){
                ?>
                <td rowspan="<?= $rowspan; ?>" style="border:0.3px solid black;font-size: 9pt !important;">Total.</td>
                <td rowspan="<?= $rowspan; ?>" style="border:0.3px solid black;font-size: 9pt !important;">GPA.</td>
                <?php
                    }else{
                        ?>
                <td rowspan="<?= $rowspan; ?>" style="border:0.3px solid black;font-size: 9pt !important;">Total Obtained</td>
                <td rowspan="<?= $rowspan; ?>" style="border:0.3px solid black;font-size: 9pt !important;">TOTAL FM</td>
                <td rowspan="<?= $rowspan; ?>" style="border:0.3px solid black;font-size: 9pt !important;">STATUS</td>
                <td rowspan="<?= $rowspan; ?>" style="border:0.3px solid black;font-size: 9pt !important;">RANK</td>
                <td rowspan="<?= $rowspan; ?>" style="border:0.3px solid black;font-size: 9pt !important;">%</td>
                <td rowspan="<?= $rowspan; ?>" style="border:0.3px solid black;font-size: 9pt !important;">DIVISION</td>
                        <?php
                    }
                ?>
            </tr>

            <tr>
                <?php
                for ($j=0; $j < sizeof($subject_information_) ; $j++) { 
                    $subject_ = explode('-', $subject_information_[$j]);
                    $name = isset($subject_[0]) ? ucwords($subject_[0]) : '';
                    $is_practical = isset($subject_[2]) && $subject_[2] != 0 ? 1 : 0;
                    if($is_practical == 1){
                        ?>
                        <td colspan="<?= $colspan_both; ?>" style="border:0.3px solid black;font-size: 9pt !important;">TH</td>
                        <td colspan="<?= $colspan_both; ?>" style="border:0.3px solid black;font-size: 9pt !important;">PR</td>
                        <td colspan="<?= $colspan_both; ?>" style="border:0.3px solid black;font-size: 9pt !important;">TT.</td>
                        <?php
                    }else{
                        if($result_ != 'general'){
                        ?>
                        <td colspan="<?= $colspan_both; ?>" style="border:0.3px solid black;font-size: 9pt !important;"> Obt. </td>
                        <?php
                        }else{
                            ?>
                            <td> </td>
                            <?php
                        }
                    }
                    if($result_ != 'general'){
                    ?>
                    <td rowspan="<?= $result_=='both' ? 2 : 1; ?>" style="border:0.3px solid black;font-size: 9pt !important;"> GP&nbsp;&nbsp;&nbsp;</td>
                    <?php
                    }

                }
                ?>
            </tr>
            <tr>

            <?php
            for ($k=0; $k < sizeof($subject_information_) ; $k++) { 
                $subject_ = explode('-', $subject_information_[$k]);
                $name = isset($subject_[0]) ? ucwords($subject_[0]) : '';
                $is_practical = isset($subject_[2]) && $subject_[2] != 0 ? 1 : 0;
                $th_fm = isset($subject_[1]) ? (int) $subject_[1] : 0;
                $pr_fm = isset($subject_[2]) ? (int) $subject_[2] : 0;
                $total_fm = $th_fm + $pr_fm;
                if($result_ == 'both'){
                    if($is_practical == 1){
                    ?>
                        <td colspan="2" style="border:0.3px solid black;font-size: 9pt !important;"><?= isset($th_fm) ? $th_fm : '';?></td>
                        <td colspan="2" style="border:0.3px solid black;font-size: 9pt !important;"><?= isset($pr_fm) ? $pr_fm : '';?></td>
                        <td colspan="2" style="border:0.3px solid black;font-size: 9pt !important;"><?= isset($total_fm) ? $total_fm : '';?></td>
                    <?php
                    }else{
                        ?>
                        <td colspan="2" style="border:0.3px solid black;font-size: 9pt !important;"><?= isset($total_fm) ? $total_fm : '';?></td>
                        <?php
                    }
                }
            }
            ?>
            </tr>

        </thead>
        <tbody>

                <?php
                    for ($s=0; $s < sizeof($result_information) ; $s++) { 
                        $col_ = sizeof($column) - 2;
                        $dob_bs = isset($result_information[$s]['dob_bs']) ? str_replace("-","/",$result_information[$s]['dob_bs']) : '';
                        $dob_ad = isset($result_information[$s]['dob_ad']) ? str_replace("-","/",$result_information[$s]['dob_ad']) : '';
                        ?>
                        <tr style="border:0.3px solid black;">
                            <td style="border:0.3px solid black; padding: 5px; font-size: 9pt !important; "><?= $s+1; ?></td>
                            <td style="border:0.3px solid black; padding: 5px; font-size: 9pt !important;"><?= isset($result_information[$s]['symbol_number']) ? $result_information[$s]['symbol_number'] : 'null'; ?></td>
                            <td style="border:0.3px solid black; padding: 5px; font-size: 9pt !important;"><?= isset($result_information[$s]['name']) ? $result_information[$s]['name'] : 'null'; ?></td>
                            <td style="border:0.3px solid black; padding: 5px; font-size: 9pt !important;"><?= $dob_bs; ?></td>
                    <?php
                        for ($t=0; $t < sizeof($column) ; $t++) {
                            if(!in_array($column[$t], ['name', 'symbol_number'])){ 
                                $col_ = sizeof($column) - 2;
                            ?>
                            <td style="font-size: 8pt !important;border:0.3px solid black;"><?= isset($result_information[$s][$column[$t]]) ? $result_information[$s][$column[$t]] : 'null'; ?></td>
                            <?php
                            }
                        }
                        ?>
                        </tr> 
                        <?php
                    }
                ?>
       </tbody>
    </table>
    <br /><br />
    <div style="width: 33% !important; float: left;">Checked by</div>
    <div style="width: 33% !important; float: center; text-align: right;">Education Section</div>
    <div style="width: 34% !important; float: right; text-align: right;">Chief Administrative Officer</div>
    <br /><br />
    <small>Note : N: Obtain Number/ T H: Theory / PR : Practical / G : Grade / GP: Grade Point / GPA : Grade Point Average</small>
</div>
