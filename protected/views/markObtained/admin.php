<?php
$this->breadcrumbs=array(
	'Mark Obtaineds'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List MarkObtained','url'=>array('index')),
array('label'=>'Create MarkObtained','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('mark-obtained-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Manage Mark Obtaineds</h1>

<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
		&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'mark-obtained-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'academic_year',
		'terminal_id',
		'terminal_',
		'student_id',
		'subject_id',
		/*
		'subject_name',
		'grace_mark',
		'theory_mark',
		'practical_mark',
		'cas_mark',
		'total_mark',
		'is_practical',
		'is_cas',
		'theory_pass_mark',
		'practical_pass_mark',
		'theory_full_mark',
		'practical_full_mark',
		'terminal_percent',
		'cas_percent',
		'percent_for_final',
		'subject_status',
		'status',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
		'approved_by',
		'approved_date',
		'school_id',
		*/
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
