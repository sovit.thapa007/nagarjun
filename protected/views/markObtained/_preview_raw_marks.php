<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    //'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'type' => 'horizontal',
    'htmlOptions' => array(
        'class' => 'form-horizontal well',
    ),
)); ?>
<h2><strong>Preview Raw Marks</strong></h2>
<div class="row-fluid">
    <div class="span6">
        <?php echo $form->dropDownListRow($model,'ledger_type',array('grade'=>'Grade','general'=>'General Marks'), array('class'=>'span12')); ?></div>
    <div class="span6">
        <?php echo $form->dropDownListRow($model,'arrange_by',array('symbol_number'=>'Symbol Number','student_id'=>'Registration Number','name'=>'Name'), array('class'=>'span12')); ?>
    </div>
</div>
    <div class="row-fluid">

        <?php
        if(UtilityFunctions::ShowSchool()){
        echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => Yii::app()->params['select_'])),'id',Yii::app()->params['select_']),array('class'=>'span12', 'prompt'=>'Select School', 'selected'=>1)); 
        }else{
            $user_information = UtilityFunctions::UserInformations();
            $school_id = isset($user_information['school_id']) ? $user_information['school_id'] : null;
            echo $form->hiddenField($model,'school_id',array('value'=>$school_id));
        }
        ?> 
    </div>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'=>'primary',
            'label'=>'Search',
        )); ?>
    </div>

<?php $this->endWidget(); ?>


<div class="form-horizontal well form-horizontal">
<?php
if(isset($column) && !empty($column)){
?>
<h2 class="text-center"><strong> RAW LEDGER OF <?= $school_information ? ucwords($school_information->title.', '.Yii::app()->params['municipality_short'].'-'.$school_information->ward_no) : ''; ?> </strong>
</h2>
    <table class="table table-bordered table-striped" style="border-collapse: collapse;" >
        <tbody>
            <tr>
                <td style="font-weight: bold;">School Name : <?= $school_information ? $school_information->title : ''; ?></td>
                <td style="font-weight: bold;">Type : <?= $school_information ? ucwords($school_information->type) : ''; ?></td>
                <td style="font-weight: bold;">Address. : <?= $school_information ? ucwords(Yii::app()->params['municipality_short']).'-'.$school_information->ward_no : ''; ?></td>
                <td style="font-weight: bold;">Code : <?= $school_information ? $school_information->schole_code : ''; ?></td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <td style="width: 10%;">SN.</td>
                            <td style="width: 50%;">Subject</td>
                            <td style="width: 20%;">Th. FM</td>
                            <td style="width: 2o%;">Pr. FM</td>
                        </thead>
                        <?php
                        if(!empty($subject_information)){
                            $sn = 0;
                            foreach ($subject_information as $subject) {
                                ?>
                                <tr>
                                    <td><?= $sn+1; ?></td>
                                    <td><?= ucwords($subject->subject_name) ?></td>
                                    <td><?= (int) $subject->theory_full_mark ?></td>

                                    <td><?= (int) $subject->practical_full_mark ?></td>
                                </tr>
                                <?php
                            $sn++;
                            if($sn > 4)
                                break;
                            }
                        }
                        ?>
                    </table>
                </td>
                <td colspan="2">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <td style="width: 10%;">SN.</td>
                            <td style="width: 50%;">Subject</td>
                            <td style="width: 20%;">Th. FM</td>
                            <td style="width: 2o%;">Pr. FM</td>
                        </thead>
                        <?php
                        if(!empty($subject_information)){
                            $n = 0;
                            foreach ($subject_information as $subject_) {
                                $n++;
                                if(in_array($n, [1,2,3,4,5]))
                                    continue;
                                ?>
                                <tr>
                                    <td><?= $n; ?></td>
                                    <td><?= ucwords($subject_->subject_name) ?></td>
                                    <td><?= (int) $subject_->theory_full_mark ?></td>

                                    <td><?= (int) $subject_->practical_full_mark ?></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    <div class="row-fluid">
        <?php 
        echo CHtml::linkButton('Export EXCEL',array(
            'type'=>'submit',
            'method'=>'POST',
            'class'=>'btn btn-primary',
            'confirm'=>"Do you want to download excel file",
            'params'=> array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken)));
        ?>
    </div>
    <div class="row-fulid" style="overflow: auto;">
    <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'indfeerecord-grid',
            'dataProvider'=>$StudentList,
            'htmlOptions'=>array('class'=>'table table-striped table-bordered', 'type'=>'raw'),
            'columns'=>$column,
    ));
if(isset($_POST) && !empty($_POST))
{
        $this->widget('EExcelView', array(
         'dataProvider'=>new CArrayDataProvider(
            $student_information,array(
                'keyField'=>'name'
                )),
                'grid_mode'=>'export',
                'title'=>'Result Legder',
                'filename'=>ucwords($school_information->title).'-Raw Marks Legder:'.$academic_year.' ('.date('Y-m-d').')',
                'stream'=>true,
                'autoWidth'=>false,
                'exportType'=>'Excel2007',
                'columns'=>$column,
            ) );

    }
}
?>
</div>

