<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('academic_year')); ?>:</b>
	<?php echo CHtml::encode($data->academic_year); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('terminal_id')); ?>:</b>
	<?php echo CHtml::encode($data->terminal_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('terminal_')); ?>:</b>
	<?php echo CHtml::encode($data->terminal_); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('student_id')); ?>:</b>
	<?php echo CHtml::encode($data->student_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subject_id')); ?>:</b>
	<?php echo CHtml::encode($data->subject_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subject_name')); ?>:</b>
	<?php echo CHtml::encode($data->subject_name); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('grace_mark')); ?>:</b>
	<?php echo CHtml::encode($data->grace_mark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('theory_mark')); ?>:</b>
	<?php echo CHtml::encode($data->theory_mark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('practical_mark')); ?>:</b>
	<?php echo CHtml::encode($data->practical_mark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cas_mark')); ?>:</b>
	<?php echo CHtml::encode($data->cas_mark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_mark')); ?>:</b>
	<?php echo CHtml::encode($data->total_mark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_practical')); ?>:</b>
	<?php echo CHtml::encode($data->is_practical); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_cas')); ?>:</b>
	<?php echo CHtml::encode($data->is_cas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('theory_pass_mark')); ?>:</b>
	<?php echo CHtml::encode($data->theory_pass_mark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('practical_pass_mark')); ?>:</b>
	<?php echo CHtml::encode($data->practical_pass_mark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('theory_full_mark')); ?>:</b>
	<?php echo CHtml::encode($data->theory_full_mark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('practical_full_mark')); ?>:</b>
	<?php echo CHtml::encode($data->practical_full_mark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('terminal_percent')); ?>:</b>
	<?php echo CHtml::encode($data->terminal_percent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cas_percent')); ?>:</b>
	<?php echo CHtml::encode($data->cas_percent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('percent_for_final')); ?>:</b>
	<?php echo CHtml::encode($data->percent_for_final); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subject_status')); ?>:</b>
	<?php echo CHtml::encode($data->subject_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('approved_by')); ?>:</b>
	<?php echo CHtml::encode($data->approved_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('approved_date')); ?>:</b>
	<?php echo CHtml::encode($data->approved_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('school_id')); ?>:</b>
	<?php echo CHtml::encode($data->school_id); ?>
	<br />

	*/ ?>

</div>