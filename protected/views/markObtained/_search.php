<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'terminal_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'terminal_',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'student_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'subject_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'subject_name',array('class'=>'span5','maxlength'=>200)); ?>

		<?php echo $form->textFieldRow($model,'grace_mark',array('class'=>'span5','maxlength'=>5)); ?>

		<?php echo $form->textFieldRow($model,'theory_mark',array('class'=>'span5','maxlength'=>5)); ?>

		<?php echo $form->textFieldRow($model,'practical_mark',array('class'=>'span5','maxlength'=>5)); ?>

		<?php echo $form->textFieldRow($model,'cas_mark',array('class'=>'span5','maxlength'=>5)); ?>

		<?php echo $form->textFieldRow($model,'total_mark',array('class'=>'span5','maxlength'=>6)); ?>

		<?php echo $form->textFieldRow($model,'is_practical',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'is_cas',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'theory_pass_mark',array('class'=>'span5','maxlength'=>5)); ?>

		<?php echo $form->textFieldRow($model,'practical_pass_mark',array('class'=>'span5','maxlength'=>5)); ?>

		<?php echo $form->textFieldRow($model,'theory_full_mark',array('class'=>'span5','maxlength'=>5)); ?>

		<?php echo $form->textFieldRow($model,'practical_full_mark',array('class'=>'span5','maxlength'=>5)); ?>

		<?php echo $form->textFieldRow($model,'terminal_percent',array('class'=>'span5','maxlength'=>5)); ?>

		<?php echo $form->textFieldRow($model,'cas_percent',array('class'=>'span5','maxlength'=>5)); ?>

		<?php echo $form->textFieldRow($model,'percent_for_final',array('class'=>'span5','maxlength'=>5)); ?>

		<?php echo $form->textFieldRow($model,'subject_status',array('class'=>'span5')); ?>

		<?php echo $form->dropDownListRow($model,'status',array("pass"=>"pass","fail"=>"fail",),array('class'=>'input-large')); ?>

		<?php echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_date',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'approved_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'approved_date',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'school_id',array('class'=>'span5')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
</div>

<?php $this->endWidget(); ?>
