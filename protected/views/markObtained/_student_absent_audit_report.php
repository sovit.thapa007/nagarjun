
<div class="form-horizontal well">
	<h2><strong>ABSENT AUDIT REPORT</strong></h2>
	<div class="row-fluid" style="overflow: auto;">
	<table class='table table-bordered' id="datatable">
		<thead>
			<th>SCHOOL</th>
			<th>CODE</th>
			<th>STUDENT</th>
			<th>STUDENT</th>
			<?php
				for ($i=0; $i < sizeof($subject_header) ; $i++) { 
					?>
					<th><?= $subject_header[$i]; ?></th>
					<?php
				}
			?>
		</thead>
		<tbody>
		<?php
			for ($j=0; $j < sizeof($student_id_array) ; $j++) { 
				$student_id = $student_id_array[$j];
				$school = isset($student_detail_array[$student_id]['school']) ? $student_detail_array[$student_id]['school'] : '';
				$school_code = isset($student_detail_array[$student_id]['school_code']) ? $student_detail_array[$student_id]['school_code'] : '';
				$name = isset($student_detail_array[$student_id]['name']) ? $student_detail_array[$student_id]['name'] : '';
				$symbol_number = isset($student_detail_array[$student_id]['symbol_number']) ? $student_detail_array[$student_id]['symbol_number'] : '';
				?>
				<tr>
					<td><?= $school; ?></td>
					<td><?= $school_code; ?></td>
					<td><?= $name; ?></td>
					<td><?= $symbol_number; ?></td>
					<?php
					for ($p=0; $p < sizeof($subject_header) ; $p++) {
						$subject = $subject_header[$p];
						$obtained = isset($mark_detail_information[$student_id][$subject]) ? $mark_detail_information[$student_id][$subject] : '';
						?>
						<th><?= $obtained; ?></th>
						<?php
					}
					?>
				</tr>
				<?php
			}
		?>
		</tbody>
	<tfoot>
		<th>SCHOOL</th>
		<th>CODE</th>
		<th>STUDENT</th>
		<th>STUDENT</th>
		<?php
			for ($i=0; $i < sizeof($subject_header) ; $i++) { 
				?>
				<th><?= $subject_header[$i]; ?></th>
				<?php
			}
		?>
	</tfoot>
	</table>
	</div>
</div>