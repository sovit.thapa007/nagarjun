<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'type' => 'horizontal',
    'htmlOptions' => array(
        'class' => 'form-horizontal well',
    ),
)); ?>
<h2><strong>Add Grace Marks</strong></h2>
<div class="row-fluid">
    <div class="span6">
		<?php echo $form->textFieldRow($model,'minimum_mark',array('class'=>'span12')); ?>
		</div>
    <div class="span6">
		<?php echo $form->textFieldRow($model,'maximum_subject',array('class'=>'span12')); ?>
			
    </div>
</div>
    <div class="row-fluid">

		<?php echo $form->textFieldRow($model,'grace_mark',array('class'=>'span12')); ?>
    </div>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'=>'primary',
            'label'=>'Submit',
        )); ?>
    </div>

<?php $this->endWidget(); ?>

<?php
$this->breadcrumbs = array(
	'View Report' => array('graceAuditReport'),
	'Grace Detail',
);
?>
<?php
	if(!empty($upgrade_student_detail)){
		$student_array = $upgrade_student_detail['student_array'];
		$student_detail_array = $upgrade_student_detail['student_detail_array'];
		$grace_mark_detail = $upgrade_student_detail['grace_mark_detail'];
		?>
		<div class="form-horizontal well">

		<h2><strong>UPGRADED STUDENT SUBJECT LIST</strong></h2>
		<table id='datatable' class="table table-bordered" style="width: 100%;">
		        <thead>
		            <tr>
		            	<th>School Code </th>
		                <th>Name</th>
		                <th>Symbol Number</th>
		                <th>Detail</th>
		            </tr>
		        </thead>
		        <tbody>
		        	<?php
		        		for ($i=0; $i < sizeof($student_array) ; $i++) { 
		        			$student_id = $student_array[$i];
		        			?>
		        			<tr>
		        				<td><?= isset($student_detail_array[$student_id]['school_code']) ? $student_detail_array[$student_id]['school_code'] : ''; ?></td>
		        				<td><?= isset($student_detail_array[$student_id]['name']) ? $student_detail_array[$student_id]['name'] : ''; ?></td>
		        				<td><?= isset($student_detail_array[$student_id]['symbol_number']) ? $student_detail_array[$student_id]['symbol_number'] : '';?></td>
		        				<td><?=  isset($grace_mark_detail[$student_id]) ? print_r($grace_mark_detail[$student_id]) : '';
		        				?></td>
		        			</tr>
		        			<?php
		        		}
		        	?>
		        </tbody>
		        <tfoot>
		            <tr>
		            	<th>School Code </th>
		                <th>Name</th>
		                <th>Symbol Number</th>
		                <th>Detail</th>
		            </tr>
		        </tfoot>
		    </table>
		<?php
	}
?>

