<?php
$this->breadcrumbs=array(
	'Mark Obtaineds',
);

$this->menu=array(
array('label'=>'Create MarkObtained','url'=>array('create')),
array('label'=>'Manage MarkObtained','url'=>array('admin')),
);
?>

<h1>Mark Obtaineds</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
