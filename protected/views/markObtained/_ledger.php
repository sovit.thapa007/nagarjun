
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    //'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'type' => 'horizontal',
    'htmlOptions' => array('target'=>'_blank', 
        'class' => 'form-horizontal well')
)); ?>
<h2><strong>PREVIEW <?= UtilityFunctions::ShowSchool() ?  'LEDGER' : 'RAW LEDGER'; ?> SCHOOL WISE</strong></h2>
<div class="row-fluid">
    <div class="span6">
        <?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span12', 'required'=>'required')); ?></div>
    <div class="span6">
        <?php echo $form->dropDownListRow($model,'arrange_by',array( 'symbol_number'=>'Symbol Number','student_id'=>'Registration Number','name'=>'Name'), array('class'=>'span12')); ?>
    </div>
</div>
    <div class="row-fluid">
        <div class="span6">
            <?php echo $form->dropDownListRow($model,'result_',array('grade'=>'Grade', 'both'=>'Combine Ledger','general'=>'General Marks'), array('class'=>'span12')); ?>
        </div>
        <div class="span6">
            <?php echo $form->dropDownListRow($model,'ledger_type',array('pdf'=>'PDF','excel'=>'Excel'), array('class'=>'span12')); ?>
        </div>
    </div>
    <div class="row-fluid">
            <?php 
            if(UtilityFunctions::ShowSchool()){
                echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => Yii::app()->params['select_'])),'id',Yii::app()->params['select_']),array('class'=>'span12', 'prompt'=>'Select School', 'selected'=>1)); 
            }
            ?>
    </div>
    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'=>'primary',
            'label'=>'Search',
        )); ?>
    </div>

<?php $this->endWidget(); ?>


<?php
if(isset($subject_information) && !empty($subject_information)){
    $column = isset($subject_information['column']) ? $subject_information['column'] : null;
    $title = $school_information ? ucwords($school_information->title.', '.Yii::app()->params['municipality_short']).'-'.$school_information->ward_no : '';
?>
<div class="row-fluid form-horizontal well">
    <br />
    <div class="row-fluid text-center" style="font-weight: bold;">
        <?= strtoupper(UtilityFunctions::MunicipalLedgerHeader()).'<br />'.$title; ?> 
    </div>
<?php
    echo CHtml::linkButton('DOWNLOAD EXCEL',array(
        'type'=>'submit',
        'method'=>'POST',
        'class'=>'btn btn-primary',
        'confirm'=>"Do you want to download excel file",
        'params'=> array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken)));
?>
<div class="clear-fix"></div>
<div class="row-fluid" style="overflow: auto;">
<?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'indfeerecord-grid',
        'dataProvider'=>$raw_result_information,
        'htmlOptions'=>array('class'=>'table table-striped table-bordered'),
        'columns'=>$column,
));
}
?>
</div>
<?php
if(isset($_POST) && !empty($_POST))
{
    $this->widget('EExcelView', array(
     'dataProvider'=>new CArrayDataProvider(
        $result_information,array(
            'keyField'=>'name'
            )),
            'grid_mode'=>'export',
            'title'=>'Result Legder',
            'filename'=>$title.' ledger ('.date('Y-m-d').')',
            'stream'=>true,
            'autoWidth'=>false,
            'exportType'=>'Excel2007',
            'columns'=>$column,
        ) );

}
?>