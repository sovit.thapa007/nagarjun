<?php
$is_practical = isset($subject_information['is_practical']) && $subject_information['is_practical'] !=0 ? 1 : null;
$is_cas = isset($subject_information['is_cas']) && $subject_information['is_cas'] !=0 ? 1 : null;
$theory_full_mark = isset($subject_information['theory_full_mark']) && $subject_information['theory_full_mark'] !=0 ? $subject_information['theory_full_mark'] : 0;
$practical_full_mark = isset($subject_information['practical_full_mark']) && $subject_information['practical_full_mark'] !=0 ? $subject_information['practical_full_mark'] : 0;
$total_full_mark = $theory_full_mark + $practical_full_mark;
$colspan = $is_practical ?  3 : 0;
$theory_title = !$is_practical ?  "Obtaine Mark " : "Theory Mark";
?>

<div class="row-fluid" style="text-align:right;">
    <form action="#" method="POST">
        <button class="btn btn-primary" name="excel_result" id="export_marks_entry">Export(Excel)</button>
    </form>
</div>

<div class="row-fluid well"><h4 class="text-center">Subject Informaiton</h4>
        <table class="table table-striped table-bordered">
            <thead>
                <td colspan="<?= $colspan; ?>" > <strong>SCHOOL : <?= ucwords($school_information->title.', '.Yii::app()->params['municipality_short'].'-'.$school_information->ward_no) ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; CODE :  <?= $school_information->schole_code; ?></strong></td>
            </thead>
            <tr><td colspan="<?= $colspan; ?>" >Name :  <?= isset($subject_information['subject_name'])? ucwords($subject_information['subject_name']) : 'Null' ;?></td></tr>

            <tr>
                <?php
                    if(!$is_practical){
                        ?>
                        <td>Full Mark : <?= $theory_full_mark;?></td>
                        <?php
                    }else{
                        ?>
                        <td>Theory FM : <?= $theory_full_mark;?></td>
                        <td>Practical FM : <?= $practical_full_mark;?></td>
                        <td>Full Mark : <?= $total_full_mark;?></td>
                        <?php
                    }
                ?>
            </tr>
        </table>
</div>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'id'=>'student-information-form',
    'enableAjaxValidation'=>false,
    'type' => 'horizontal',
    'action'=> Yii::app()->request->baseUrl.'/MarkObtained/InsertMarks',
    'enableClientValidation' => true,
    'htmlOptions' => array(
        'class' => 'form-horizontal well',
    ),
)); ?>
    <h2><strong>STUDENT'S LISTS</strong></h2>
    <table class="table table-striped table-bordered">
        <tr>
            <th>SN</th>
            <th>Symbol No.</th>
            <th>Student Name</th>
            <?php
            if(in_array($insert_type, ['both','theory'])){
                ?>
                <th><?= $theory_title; ?></th>
                <?php
            }
            ?>

            <?php
            if($is_practical && in_array($insert_type, ['both','practical']))
            {
                ?>
                <th colspan="1">Pratical Marks</th>
                <?php
            }
            ?>

        </tr>
        <?php
        $sn=0;
        if(!empty($model)){
            foreach($model as $studentdata)
            {
                $registration_number = $studentdata->id;
                $marks_information = MarkObtained::model()->findByAttributes(['subject_id'=>$subjectid,'student_id'=>$registration_number,'academic_year'=>$academic_year,'school_id'=>$school_id]);
                ?>
                <tr>
                    <td><?php echo $sn+1; ?>
                        <input type="hidden" name="<?php echo $sn.'_registration_number'; ?>" value="<?php echo $registration_number; ?>">
                    </td>

                    <td><?= $studentdata->symbol_number; ?></td>
                    <td><?= ucwords($studentdata->first_name." ".$studentdata->middle_name." ".$studentdata->last_name); ?></td>

                    <?php
                    if(in_array($insert_type, ['both','theory'])){
                        ?>
                        <td><input type="number" <?php if(!empty($marks_information)){ ?>value="<?php echo $marks_information->theory_mark ?>" <?php } ?>  id="<?php echo $registration_number.'_'.$sn.'_theory_mark'; ?>" name="<?php echo $registration_number.'_'.$sn.'_theory_mark'; ?>" max="<?= $theory_full_mark; ?>" required> </td>


                    <?php
                    }
                    if($is_practical && in_array($insert_type, ['both','practical'])){
                        ?>
                        <td><input type="number" <?php if(!empty($marks_information)){ ?>value="<?php echo $marks_information->practical_mark ?>" <?php } ?>  id="<?php echo $registration_number.'_'.$sn.'_practical_mark'; ?>" name="<?php echo $registration_number.'_'.$sn.'_practical_mark'; ?>" max="<?= $practical_full_mark; ?>" required> </td>
                    <?php
                    }
                    ?>
                </tr>
            <?php
            $sn++;
            }
        ?>
            <?php
        }
        ?>
    </table>
            <input type="hidden" name="previous_link" value="<?= $previous_link; ?>">
            <input type="hidden" name="subject_id" value="<?php echo $subjectid; ?>">
            <input type="hidden" name="terminal_id" value="<?php echo $terminal_id; ?>">
            <input type="hidden" name="academic_year" value="<?php echo $academic_year; ?>">
            <input type="hidden" name="school_id" value="<?php echo $school_id; ?>">
            <input type="hidden" name="total_number" value="<?php echo $sn; ?>">

            <input type="hidden" name="is_practical" value="<?= isset($subject_information['is_practical']) ?  1 : 0; ?>">
            <input type="hidden" name="theory_full_mark" value="<?= isset($subject_information['theory_full_mark']) ?  $subject_information['theory_full_mark'] : 0; ?>">
            <input type="hidden" name="practical_full_mark" value="<?= isset($subject_information['practical_full_mark']) ?  $subject_information['practical_full_mark'] : 0; ?>">

    <?php if($sn !=0){
        ?>

    <div class="text-center form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Submit Marks',
        )); ?>
    </div>
    <?php
    }
    ?>

<?php $this->endWidget(); ?>
<?php

if(isset($_POST['export_marks_entry']))
{
    $this->widget('EExcelView', array(
     'dataProvider'=>new CArrayDataProvider(
        $student_information,array(
            'keyField'=>'name'
            )),
            'grid_mode'=>'export',
            'title'=>'Result Legder',
            'filename'=>$subject_information['subject_name']."_2073",
            'stream'=>true,
            'autoWidth'=>false,
            'exportType'=>'Excel2007',
            'columns'=>$column,
        ) );

}

?>

