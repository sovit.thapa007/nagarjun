<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'type' => 'horizontal',
    'htmlOptions' => array(
        'class' => 'form-horizontal well',
    ),
)); ?>
    <h2><strong>PUBLISH RESULT </strong></h2>
    <div class="row-fluid">
        <div class="span12">
            <?php
                $this->widget("ext.maskedInput.MaskedInput", array(
                "model" => $model,
                "attribute" => "result_publish_date",
                "mask" => "9999-99-99",
                "clientOptions"=>array("greedy"=>false, 'class'=>'span12','placeholder'=>'yyyy-mm-dd','removeMaskOnSubmit' => false)   
            ));

            ?>
                <?php echo CHtml::error($model, 'result_publish_date');?>
        </div><!-- 
    <input type="text" id="result_publish_date_data" name="result_publish_date_data" placeholder="yyyy-mm-dd" required="required"> -->
    <p id='result_error_section'></p>
    </div>
    <br />
    <div class="text-center form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'=>'primary',
            'label'=>'PUBLIS RESULT',
            'htmlOptions' => array(
                'id' => 'submit',
                'name'=>'ActionButton',
                'confirm' => 'Are you Sure ?  Do you want to Publish Result.',
                'onclick' => 'return validate()',
            ),
            
        )); ?>
    </div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    $("#result_publish_date_data").inputmask("yyyy-mm-dd");

    function validate(){
        var academic_year = <?= $academic_year; ?>;
        var result_date = $('#Result_result_publish_date').val(); 
        var result_inforamtion = result_date.split('-');
        if( !$('#Result_result_publish_date').val() ) {
            $('#result_error_section').html('<font style="color:red;"><strong> Result Publish Date is Empty</strong></font>');
            return false;
        }
        var year = typeof result_inforamtion[0] !== "undefined" && result_inforamtion[0] ? parseInt(result_inforamtion[0]) : null; 
        var month = typeof result_inforamtion[1] !== "undefined" && result_inforamtion[1] ? parseInt(result_inforamtion[1]) : null; 
        var day = typeof result_inforamtion[2] !== "undefined" && result_inforamtion[2] ? parseInt(result_inforamtion[2]) : null; 
        if(year != academic_year){
            $('#result_error_section').html('<font style="color:red;"><strong> Incorrect Year, Please Choose Current Academic Year.</strong></font>');
            return false;
        }
        if(month > 12 || month == 0){
            $('#result_error_section').html('<font style="color:red;"><strong> Incorrect Month, Month Schould Be Between 1-12.</strong></font>');
            return false;
        }
        if(day > 32 || day == 0){
            $('#result_error_section').html('<font style="color:red;"><strong> Incorrect Month, Month Schould Be Between 1-32.</strong></font>');
            return false;
        }

        $("#submit").text("Loading...");
        $(this).submit('loading').delay(1000).queue(function () {
            $('#submit').attr('disabled','disabled');
        });

        return true;
      /* 
        alert("result year information : " + result_inforamtion[0]);*/
    }

</script>