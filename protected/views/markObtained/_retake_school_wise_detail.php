<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'subject-information-form',
	'enableAjaxValidation'=>false,
	'type' => 'horizontal',
	'enableClientValidation' => true,
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
	),
	'focus' => array($model, 'title'),
)); ?>

	<h2><strong>SEARCH FORM FOR RETAKE</strong></h2>
	<div class="row-fluid">
		<div class="span6">
        <?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span12')); ?></div>
        <div class="span6">
        <?php 
        echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(),'id','title'),array('class'=>'span12', 'prompt'=>'Select School', 'selected'=>1)); 
        ?> 
		</div>
	</div>
	<div class="form-actions text-center">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Submit Form',
		)); ?>
	</div>

<?php $this->endWidget(); ?>


<?php
if(!empty($retake_ledger_report)){
    $school_array = $retake_ledger_report['school_array'];
    $school_array_detail = $retake_ledger_report['school_array_detail'];
    $subject_header = $retake_ledger_report['subject_header'];
?>
<div class="row-fluid form-horizontal well" style="width: 100%; overflow: auto;">
    <h2><strong>RETAKE STUDENT'S LISTS </strong></h2>
	<table id='datatable' class="table table-bordered" style="margin-right: 10px !important; ">
        <thead>
            <tr>
                <th>School</th>
                <th>Code</th>
                <?php
                for ($j=0; $j < sizeof($subject_header) ; $j++) { 
                ?>
                <th><?= $subject_header[$j]; ?></th>
                <?php
                }
                ?>
                <th>Retake Sub. No.</th>
            </tr>
        </thead>
        <tbody>
    	<?php
		if(!empty($school_array)){
            for ($i=0; $i < sizeof($school_array) ; $i++) { 
                $school_id = $school_array[$i];
                $total_retake_subject = isset($school_array_detail[$school_id]['total_student']) ? $school_array_detail[$school_id]['total_student'] : '';
			?>
			<tr>
				<td><?= isset($school_array_detail[$school_id]['school']) ? $school_array_detail[$school_id]['school'] : ''; ?></td>
                <td><?= isset($school_array_detail[$school_id]['school_code']) ? $school_array_detail[$school_id]['school_code'] : ''; ?></td>
                <?php
                $retake_number = 0 ;
                for ($k=0; $k < sizeof($subject_header) ; $k++) { 
                    $subject_hd = $subject_header[$k];
                    $number_info = isset($school_array_detail[$school_id][$subject_hd]) ? $school_array_detail[$school_id][$subject_hd] : '';
                ?>
                <td><?= $number_info; ?></td>
                <?php
                }
                ?>
                <td><?= $total_retake_subject; ?></td>
            </tr>
			<?php
			}
		}
    	?>

        </tbody>

        <tfoot>
            <tr>
                <th>School</th>
                <th>Code</th>
                <?php
                for ($j=0; $j < sizeof($subject_header) ; $j++) { 
                ?>
                <th><?= $subject_header[$j]; ?></th>
                <?php
                }
                ?>
                <th>Retake Sub. No.</th>
            </tr>
        </tfoot>
    </table>
</div>
<?php
	}
?>