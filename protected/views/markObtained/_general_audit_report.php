
<div class="form-horizontal well">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    //'action'=>Yii::app()->createUrl($this->route),
    //'method'=>'get',
    'type' => 'horizontal',
    'htmlOptions' => array('class' => 'form-horizontal well')
)); ?>
<h2><strong>PREVIEW SCHOOL AUDIT</strong></h2>
	<div class="row-fluid">
       <?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span12', 'required'=>'required')); ?>
        	
        </div>
    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'=>'primary',
            'label'=>'Search',
        )); ?>
    </div>

<?php $this->endWidget(); ?>

	<h2><strong>SCHOOL AUDIT REPORT  OF : <?= $academic_year.' B.S'; ?></strong></h2>

	<table class='table table-bordered'>
		<tbody>
			<tr>
				<td> TOTAL NO. OF SCHOOL STUDENTS ARE NOT REGISTERED </td>
				<td> <?= isset($auditReportInformation['no_student_registered']) ? $auditReportInformation['no_student_registered'] : '' ?></td>
				<td> TOTAL SUBJECT FM ARE NOT SET </td>
				<td> <?= isset($auditReportInformation['no_subject_setting']) ? $auditReportInformation['no_subject_setting'] : '' ?></td>
			</tr>
			<tr>
				<td>TOTAL NO. STUDENTS MISSING THEORY MARK </td>
				<td> <?= isset($auditReportInformation['missing_theory_mark']) ? $auditReportInformation['missing_theory_mark'] : '' ?></td>
				<td>TOTAL NO. STUDENTS MISSING PRACTICAL MARK</td>
				<td> <?= isset($auditReportInformation['missing_practical_mark']) ? $auditReportInformation['missing_practical_mark'] : '' ?></td>
			</tr>
			<tr>
			</tr>
			<tr>
				<td colspan="2">TOTAL NO. STUDENTS MISSING SUBJECT MARKS </td>
				<td colspan="2"> <?= isset($auditReportInformation['missing_subject_marks']) ? $auditReportInformation['missing_subject_marks'] : '' ?></td>
			</tr>
		</tbody>
	</table>

	<table class='table table-bordered' id="datatable">
		<thead>
			<th>SN</th>
			<th>SCHOOL</th>
			<th>CODE</th>
			<th style="width: 40% !important;">MESSAGE</th>
		</thead>
		<tbody>
		<?php
		if(!empty($auditReport)){
			$school_wise_information = $auditReportInformation['school_wise_information'];
			for ($i=0; $i < sizeof($auditReport) ; $i++) { 
				$school_id = isset($auditReport[$i]['id']) ? $auditReport[$i]['id'] : null;
				$background = isset($school_wise_information[$school_id]) ? '#b31f17' : '#1f751f';
				$school_information_array = isset($school_wise_information[$school_id]) ? $school_wise_information[$school_id] : null;
				$schl_info_keys = $school_information_array ? array_keys($school_information_array) : null;
				?>
				<tr style="background-color: <?= $background; ?>">
					
					<td><?= $i+1; ?></td>
					<td><a href="<?= Yii::app()->baseUrl.'/MarkObtained/ledgerAudit/'.$school_id; ?>" target='_blank' ><?= isset($auditReport[$i]['name']) ?  strtoupper($auditReport[$i]['name']) : ''; ?></a></td>
					<td>
						<?= isset($auditReport[$i]['school_code']) ? $auditReport[$i]['school_code'] : ''; ?>
					</td>
					<td>
						<?php
							for ($k=0; $k < sizeof($schl_info_keys) ; $k++) { 
								$key = $schl_info_keys[$k];
								$sn= $k+1;
								?>
								 <?= $sn.'. ' . ucwords(str_replace('_',' ',$key)); ?> : <?= isset($school_information_array[$key]) ? $school_information_array[$key] : '';  ?>
								<br />
								<?php
							}
						?>
					</td>

				</tr>
				<?php
			}
		}
		?>
	</tbody>
	<tfoot>
		<th>SN</th>
		<th>SCHOOL</th>
		<th>CODE</th>
		<th style="width: 40% !important;">MESSAGE</th>
	</tfoot>
	</table>
</div>