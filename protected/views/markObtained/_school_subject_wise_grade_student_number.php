
<div class="form-horizontal well">
    <h2><STRONG>SCHOOL SUBJECT WISE GRADE STUDENT NUMBER</STRONG></h2>
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'get',
        'type' => 'horizontal',
    )); ?>
    <div class="row-fluid">
        <div class="span6">
            <?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span12')); ?></div>
        <div class="span6">
            <?php echo $form->dropDownListRow($model,'ledger_type',array('pdf'=>'PDF','general'=>'General'), array('class'=>'span12')); ?>
        </div>
    </div>
    <div class="form-actions text-center">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'=>'primary',
            'label'=>'Search Report',
        )); ?>
    </div>
    </div>

        <?php $this->endWidget(); ?>
        <?php
            if(!empty($data_detail_array)){
                $school_id_array = isset($data_detail_array['school_id_array']) ?  $data_detail_array['school_id_array'] : null;
                $subject_id_array = isset($data_detail_array['subject_id_array']) ? $data_detail_array['subject_id_array'] : null;
                $subject_name_array = isset($data_detail_array['subject_name_array']) ? $data_detail_array['subject_name_array'] : null;
                $grade_ = isset($data_detail_array['grade_information']) ? $data_detail_array['grade_information'] : null;
                $data_information_array = isset($data_detail_array['data_information_array']) ? $data_detail_array['data_information_array'] : null;
                ?>
<div class="form-horizontal well" style="overflow: auto; border-collapse: collapse;">

                <table class="table table-bordered table-stripped" style="border-collapse: collapse;" >
                    <tr>
                        <th colspan="<?= sizeof($subject_id_array) + 5; ?>" style="text-align: center;font-weight: bold; "> <?= strtoupper(UtilityFunctions::MunicipalLedgerHeader($academic_year)); ?>
                        </th>
                    </tr>
                    <tr>
                        <td>SN</td>
                        <td>School Name</td>
                        <td>School Code</td>
                        <td>Grade</td>
                        <?php
                            if($subject_id_array){
                                for ($i=0; $i < sizeof($subject_id_array) ; $i++) {
                                    $name = isset($subject_name_array[$subject_id_array[$i]]) ? $subject_name_array[$subject_id_array[$i]] : '';
                                    ?>
                                    <td><?= $name; ?></td>
                                    <?php
                                }
                            }
                        ?>
                        <td>Student NO.</td>
                    </tr>
                    <?php
                        for ($j=0; $j < sizeof($school_id_array) ; $j++) {
                            $school_id = $school_id_array[$j];
                            $school_name_array = $data_detail_array['school_name_array'] ? $data_detail_array['school_name_array'] :null;
                            $school_name = $school_name_array && isset($school_name_array[$school_id]['name']) ? $school_name_array[$school_id]['name'] : '';
                            $school_code = $school_name_array && isset($school_name_array[$school_id]['code']) ? $school_name_array[$school_id]['code'] : '';
                            ?>
                            <tr>
                                <td><?= $j+1; ?></td>
                                <td><?= $school_name; ?></td>
                                <td><?= $school_code; ?></td>
                                <td>
                                <?php
                                for ($k=0; $k < sizeof($grade_) ; $k++) {
                                    $grade = $grade_[$k]; 
                                    echo $grade.'<br />';
                                    } 
                                    ?>
                                <?php
                                if($subject_id_array){
                                for ($i=0; $i < sizeof($subject_id_array) ; $i++) {
                                    $subject_id = $subject_id_array[$i];
                                    $total_student_number = 0;
                                    echo '<td>';
                                    for ($k=0; $k < sizeof($grade_) ; $k++) {
                                        $grade = $grade_[$k]; 
                                        $plus = stripos($grade, "+") ? 1 : 0;
                                        $number = isset($data_information_array[$school_id][$subject_id][$grade]) ? $data_information_array[$school_id][$subject_id][$grade] : 0;
                                        $total_student_number += $number;
                                        echo '<a href='.Yii::app()->request->baseUrl.'/markObtained/SubjectStudentList/?academic_year='.$academic_year.'&school_id='.$school_id.'&subject_id='.$subject_id.'&grade='.CHtml::encode($grade).'&plus='.$plus.'>'.$number.'</a><br />';
                                    }
                                    echo '</td>'; 
                                }
                                }
                                ?> 
                                <td><?= $total_student_number; ?></td> 
                            </tr>
                            <?php
                        }

                    ?>
                </table>
</div>
                <?php
            }

        ?>
