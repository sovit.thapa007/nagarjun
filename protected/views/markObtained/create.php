<?php
$this->breadcrumbs=array(
	'Mark Obtaineds'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List MarkObtained','url'=>array('index')),
array('label'=>'Manage MarkObtained','url'=>array('admin')),
);
?>

<h1>Create MarkObtained</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>