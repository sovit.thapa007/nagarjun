<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'subject-information-form',
	'enableAjaxValidation'=>false,
	'type' => 'horizontal',
	'enableClientValidation' => true,
	'action'=>'StudentList',
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
	),
	'focus' => array($model, 'title'),
)); ?>

<?php if($is_save){
	?>
		<div class="text-center <?= $is_save=='1' ? "alert alert-success" : "alert alert-danger" ;?>"><?= $message; ?></div>
	<?php
	}?>
	<h2><strong>INSERT MARKS / DOWNLOAD OFFLINE MARKS FORM</strong></h2>

	<?php 
	if(UtilityFunctions::ShowSchool()){
	echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => Yii::app()->params['select_'])),'id',Yii::app()->params['select_']),array('class'=>'span12', 'prompt'=>'Select School', 'selected'=>1)); 
	}else{
    	$user_information = UtilityFunctions::UserInformations();
    	$school_id = isset($user_information['school_id']) ? $user_information['school_id'] : null;
        echo $form->hiddenField($model,'school_id',array('value'=>$school_id));
    }
	?> 
	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->dropDownListRow($model,'subject_id',array(),array('class'=>'span12', 'prompt'=>'Select Subject')); ?>
			<?php echo $form->dropDownListRow($model,'options',array('insert_marks'=>'Insert Marks', 'download_student_list'=>'Download Student List'),array('class'=>'span12')); ?>
		</div>
		<div class="span6">

        	<?php echo $form->dropDownListRow($model,'arrange_by',array('symbol_number'=>'Symbol Number','student_id'=>'Registration Number','name'=>'Name'), array('class'=>'span12')); ?>
        	<?php echo $form->dropDownListRow($model,'insert_type',array('both'=>'Theory && Practical', 'theory'=>'Theory','practical'=>'Practical'), array('class'=>'span12')); ?>
		</div>
	</div>

	<div class="form-actions text-center">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Submit Form',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">

    $('body').on('change', '#MarkObtained_school_id', function() {
    	var school_id = $(this).val();
    	var YII_CSRF_TOKEN = $("input[name=YII_CSRF_TOKEN]").val();
        if ($.isNumeric(school_id)) {
        	$.ajax({
                type: 'POST',
                data : {school_id:school_id, YII_CSRF_TOKEN:YII_CSRF_TOKEN},
                url: '/subjectInformation/optionalSubjectList',
                dataType: 'JSON',
                success: function(response) {
                    $('#MarkObtained_subject_id').html(response.option);
                }
            });
        }
    });
</script>
