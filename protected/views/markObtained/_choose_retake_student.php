<?php
$this->breadcrumbs=array(
    'Retake Dashboard'=>array('retakeReport'),
    'Create',
);
	if(!empty($studentList)){
        $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'id'=>'subject-information-form',
        'enableAjaxValidation'=>false,
        'type' => 'horizontal',
        'enableClientValidation' => true,
        'action' => Yii::app()->baseUrl."/markObtained/insertRetakeMark",
        'htmlOptions' => array(
            'class' => 'form-horizontal well',
        ),
        )); 

        $type = isset($params['type']) ? $params['type'] : null;
        $academic_year = isset($params['academic_year']) ? $params['academic_year'] : null;
        $subject_id = isset($params['subject_id']) ? $params['subject_id'] : null;
        //$subjectInformation = $studentList['subjectInformation'];
        $student_array = $studentList['student_array'];
        $subjectType = $type == 'TH' ? 'theory' : 'practical';
        ?>
        <h2><strong><?= ucwords($school_information->title).', '.Yii::app()->params['municipality_short'].'-'.$school_information->ward_no; ?></strong>
            <br /></h2>
            <table id="retak_exam_section" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>Selection</th>
                    <th>Symbol No.</th>
                    <th>Name</th>
                    <th><?= $type == 'TH' ? 'Prv. TH' : 'Prv. PR'; ?></th>
                    <th><?= ucwords($subjectType.' Mark'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $sn = 0;
                for ($i=0; $i < sizeof($student_array) ; $i++) { 
                    $student_id = $student_array[$i];
                    $student_detail_array = isset($studentList['student_detail_array'][$student_id]) ? $studentList['student_detail_array'][$student_id] : [];
                    $mark_detail = isset($studentList['student_mark_detail_array'][$student_id]) ? $studentList['student_mark_detail_array'][$student_id] : [];
                    $mark_keys_array = array_keys($mark_detail);
                    ?>
                    <tr>
                        <td><?= $sn+1; ?></td>
                        <td><input type="checkbox" name="studentArray[]" value="<?= $student_id; ?>"></td>
                        <td><?= $student_detail_array['symbol_number'] ? $student_detail_array['symbol_number'] : ''?></td>
                        <td><?= $student_detail_array['name'] ? $student_detail_array['name'] : ''?></td>
                        <td>
                            <?php
                            $mark_information = '';
                               for ($k=0; $k < sizeof($mark_keys_array) ; $k++) { 
                                    $key = $mark_keys_array[$k];
                                    $value = isset($mark_detail[$key]) ? $mark_detail[$key] : '';
                                    $mark_information .= $key.' : '.$value.' <br />';
                                } 
                                echo $mark_information;
                            ?>   

                        </td>
                        <td><input type="number" name="<?= $student_id.'_'.$subject_id.'_'.$subjectType; ?>" max='<?= $maximum_range; ?>'></td>

                    </tr>
                    <?php
                $sn++;
                }
                ?>
                </tbody>
        </table>
        <input type="hidden" name="academic_year" value="<?= $academic_year; ?>" />
        <input type="hidden" name="subject_id" value="<?= $subject_id; ?>" />
        <div class="form-actions text-center">
        <small style="float: left;"><strong>Please Checked those student who are taking part in retake exam section.</strong></small>
        <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>'Submit Data',
                'htmlOptions' => array(
                    'name'=>'ActionButton',
                    'confirm' => 'Are you Sure ?  Do you want to Submit this inserted Marks.',
                ),
            )); ?>
    </div>

<?php $this->endWidget(); ?>
        
        <?php
    }

    ?>