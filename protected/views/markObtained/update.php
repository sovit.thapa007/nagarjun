<?php
$this->breadcrumbs=array(
	'Mark Obtaineds'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List MarkObtained','url'=>array('index')),
array('label'=>'Create MarkObtained','url'=>array('create')),
array('label'=>'View MarkObtained','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage MarkObtained','url'=>array('admin')),
);
?>

<h1>Update MarkObtained <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>