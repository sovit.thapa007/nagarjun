<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'subject-information-form',
	'enableAjaxValidation'=>false,
	'type' => 'horizontal',
	'enableClientValidation' => true,
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
	),
	'focus' => array($model, 'title'),
)); ?>

	<h2><strong>SEARCH FORM FOR RETAKE</strong></h2>
	<div class="row-fluid">
		<div class="span6">
        <?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span12', 'required'=>'required')); ?></div>
        <div class="span6">
			<?php echo $form->dropDownListRow($model,'subject_id',CHtml::listData(SubjectInformation::model()->findAll(),'id','title'),array('class'=>'span12', 'prompt'=>'Select Subject')); ?>
		</div>
	</div>
	<div class="row-fluid">
		<?php 
		echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(),'id','title'),array('class'=>'span12', 'prompt'=>'Select School', 'selected'=>1)); 
		?> 
	</div>

	<div class="form-actions text-center">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Submit Form',
		)); ?>
	</div>

<?php $this->endWidget(); ?>


<?php
	if(!empty($retake_ledger_report)){
		$academic_year = isset($retake_ledger_report['academic_year']) ? $retake_ledger_report['academic_year'] : null;
		$school_array = isset($retake_ledger_report['school_id']) ? $retake_ledger_report['school_id'] : [];
		$school_information_array = isset($retake_ledger_report['school_name']) ? $retake_ledger_report['school_name'] : [];
		$subject_array = isset($retake_ledger_report['subject']) ? $retake_ledger_report['subject'] : [];
		$subject_id_array = isset($retake_ledger_report['subject_id_array']) ? $retake_ledger_report['subject_id_array'] : [];
		$data_array = isset($retake_ledger_report['data']) ? $retake_ledger_report['data'] : []; 
		if(!empty($data_array)){
			?>
            <div class="form-horizontal well form-horizontal" style="overflow: auto;">
			<h2 class="text-center"> <STRONG>ELIGIBLE TO RETAKE EXAM STUDENT'S NUMBER : <?= !empty($subject_array) ? strtoupper(implode(',', $subject_array)) :'' ?> </STRONG> </h2>
            <table id="retak_exam_section" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>School</th>
                    <th>Code</th>
                    <th>Address</th>
                    <?php
                    for ($i=0; $i < sizeof($subject_array) ; $i++) { 
                    	echo "<th>".$subject_array[$i]."</th>";
                    }
                    ?>
                </tr>
                </thead>
            <?php
            for ($j=0; $j < sizeof($school_array) ; $j++){
            	$school_id = $school_array[$j];
            	$schoolName = isset($school_information_array[$school_id]['name']) ? $school_information_array[$school_id]['name'] : null;
                $code = isset($school_information_array[$school_id]['school_code']) ? $school_information_array[$school_id]['school_code'] : null;
                $address = isset($school_information_array[$school_id]['address']) ? $school_information_array[$school_id]['address'] : null;
                ?>
                <tr>
                	<td><?= $j+1; ?></td>
                	<td><?= $schoolName; ?></td>
                    <td><?= $code; ?></td>
                    <td><?= $address; ?></td>
                    <?php
                    for ($k=0; $k < sizeof($subject_id_array) ; $k++) { 
                    	$subject_id = $subject_id_array[$k];
                    	$retake_number = isset($data_array[$school_id.'_'.$subject_id]) ? $data_array[$school_id.'_'.$subject_id] : null;
                    	?>
                    	<td>
                            <a href="<?= Yii::app()->baseUrl; ?>/markObtained/SelectStudent/?academic_year=<?= $academic_year; ?>&school_id=<?= $school_id; ?>&subject_id=<?= $subject_id; ?>"><?= $retake_number; ?></a>
                    	</td>
                    	<?php
                    }
                    ?>
                </tr>
                <?php
            }
            ?>
        </table>
        </div>
			<?php
		}
	}

?>
