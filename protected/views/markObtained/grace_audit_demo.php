
<div class="row-fluid form-horizontal well">
    <?php
    ?>
<?php
    
    $total_male = isset($grace_mark_report_information['total_male']) ? $grace_mark_report_information['total_male'] : 0;
    $total_female = isset($grace_mark_report_information['total_female']) ? $grace_mark_report_information['total_female'] : 0;
    $above_d = isset($grace_mark_report_information['above_d']) ? $grace_mark_report_information['above_d'] : [];
    $below_d = isset($grace_mark_report_information['below_d']) ? $grace_mark_report_information['below_d'] : [];
    $absent = isset($grace_mark_report_information['absent']) ? $grace_mark_report_information['absent'] : [];
    $insert_male = $insert_female = 0;
    $school_type = array_keys(Yii::app()->params['school_type']);
?>


<table class="table table-bordered" style="border: 1px solid; width: 100%;border-collapse: collapse;">
    <tr style="border: 1px solid;">
        <th colspan="7" style="border: 1px solid;">STUDENT GRADE AUDIT REPORT</th>
    </tr>
    <tr style="border: 1px solid;">
        <td rowspan="5" style="border: 1px solid;"> TOTAL STUDENT <br /> <?=  $total_male + $total_female; ?> </td>
        <td rowspan="2" style="border: 1px solid;"> Title </td>
        <?php
        for ($q=0; $q < sizeof($school_type) ; $q++) { 
            $type = $school_type[$q];
        ?>
            <td colspan="2" style="border: 1px solid;"> <?= strtoupper($type); ?></td>
        <?php
            }
        ?>
        <td rowspan="3" style="border: 1px solid;"> </td>
    </tr>
    <tr style="border: 1px solid;">
        <td style="border: 1px solid;"> Male </td>
        <td style="border: 1px solid;"> Female </td>
        <td style="border: 1px solid;"> Male </td>
        <td style="border: 1px solid;"> Female </td>
    </tr>
    <tr style="border: 1px solid;">
        <td style="border: 1px solid;"> Above D+</td>
        <?php
        for ($i=0; $i < sizeof($school_type) ; $i++) { 
            $type = $school_type[$i];
            $ab_m = isset($above_d[$type]['male']) ? $above_d[$type]['male'] :0;
            $ab_f = isset($above_d[$type]['female']) ? $above_d[$type]['female'] :0;
            $insert_male = $insert_male + $ab_m;
            $insert_female = $insert_female + $ab_f;
            ?>
            <td style="border: 1px solid;"><?= $ab_m; ?></td>
            <td style="border: 1px solid;"><?= $ab_f; ?></td>
            <?php
        }
        ?>

    </tr>

    <tr style="border: 1px solid;">
        <td style="border: 1px solid;"> Below D+</td>
        <?php
        for ($j=0; $j < sizeof($school_type) ; $j++) { 
            $type_ = $school_type[$j];
            $bl_m = isset($below_d[$type_]['male']) ? $below_d[$type_]['male'] :0;
            $bl_f = isset($below_d[$type_]['female']) ? $below_d[$type_]['female'] :0;
            $insert_male = $insert_male + $bl_m;
            $insert_female = $insert_female + $bl_f;
            ?>
            <td style="border: 1px solid;"><?= $bl_m; ?></td>
            <td style="border: 1px solid;"><?= $bl_f; ?></td>
            <?php
        }
        ?>

        <td style="border: 1px solid;"> Missing <br /> Male && Female </td>
    </tr>

    <tr style="border: 1px solid;">
        <td style="border: 1px solid;"> Absent</td>
        <?php
        for ($p=0; $p < sizeof($school_type) ; $p++) { 
            $type = $school_type[$p];
            $ab_m = isset($absent[$type]['male']) ? $absent[$type]['male'] :0;
            $ab_f = isset($absent[$type]['female']) ? $absent[$type]['female'] :0;
            $insert_male = $insert_male + $ab_m;
            $insert_female = $insert_female + $ab_f;
            ?>
            <td style="border: 1px solid;"><?= $ab_m; ?></td>
            <td style="border: 1px solid;"><?= $ab_f; ?></td>
            <?php
        }
        $missing_male = $total_male - $insert_male;
        $missing_female = $total_female - $insert_female;
        ?>
        <td style="border: 1px solid;"><?= 'M : '.$missing_male.' && F : '.$missing_female; ?></td>
    </tr>
</table>
</div>