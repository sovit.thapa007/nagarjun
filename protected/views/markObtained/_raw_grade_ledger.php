
<div class="alert alert-info"><?= $execution_time; ?></div>
<h3 class="text-center">Preview Raw Marks</h3>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    //'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'type' => 'horizontal',
)); ?>
<hr />
<div class="row-fluid">
    <div class="span6">
        <?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span12')); ?></div>
    <div class="span6">
        <?php echo $form->dropDownListRow($model,'arrange_by',array('name'=>'Name', 'symbol_number' => 'Symbol Number'), array('class'=>'span12')); ?>
    </div>
</div>
    <div class="row-fluid">
        <div class="span6">
        <?php echo $form->dropDownListRow($model,'terminal_id',CHtml::listData(Terminal::model()->findAll(),'class_terminal_id','title'),array('prompt'=>'Select Terminal', 'class'=>'span12')); ?>
        </div>
        <div class="span6">
            <?php 
            if(UtilityFunctions::ShowSchool()){
                echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(),'id','title'),array('class'=>'span12', 'prompt'=>'Select Terminal')); 
            }
            ?>
        </div>
    </div>
    <div class="row-fluid">
        <?php echo $form->dropDownListRow($model,'ledger_type',array('grade'=>'Grade','general'=>'General Marks'), array('class'=>'span12')); ?>
    </div>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'=>'primary',
            'label'=>'Search',
        )); ?>
    </div>

<?php $this->endWidget(); ?>


<?php
if(isset($column) && !empty($column)){
?>
<div class="row-fluid" style="text-align:center;"><?php
 if($ledgerHeader){echo $ledgerHeader; } 
 ?></div>
 <div class="row-fluid">
 <div class="span4 alert alert-info">
 <table class="table table-striped table-bordered">
    <?php if(isset($cas_terminal_information['cas']) && $cas_terminal_information['cas'] !=0 ){
        ?>
        <thead><th>CAS</th> <th><?= $cas_terminal_information['cas'].' %'; ?></th></thead>
        <?php
    }
    ?>
    <?php if(isset($cas_terminal_information['terminal']) && $cas_terminal_information['terminal'] !=0 ){
        ?>
        <thead><th>TERMINAL : </th> <th><?= $cas_terminal_information['terminal'].' %'; ?></th></thead>
        <?php
    }
    ?>
 </table>
 </div>
 <div class="span8">
    <button class="btn btn-primary" id='show_terminal_information'> Show Terminal Information </button>
    <button class="btn btn-primary" id='hide_terminal_information'> hide Terminal Information </button>
    <table class="table table-striped table-bordered" id="pass_full_mark_information" style="display: none;">
    <?php
        $colspan = $is_practical ? 5 : 3;
    ?>
    <thead><th colspan="<?= $colspan; ?>"><h4 class="text-center">TERMINAL</h4></th></thead>
    <tr>
        <th>Subejct</th>
        <th><? if($is_practical){echo "Theory PM";}else{echo "Pass Mark";}?></th>
        <th><? if($is_practical){echo "Theory FM";}else{echo "Full Mark";}?></th>
        <?php
        if($is_practical){
            ?>
            <th><?= 'Practical PM'; ?></th>
            <th><?= 'Practical FM'; ?></th>
            <?php
        }
        ?>
    </tr>
    <?php
        for ($i=0; $i < sizeof($terminal_information) ; $i++) { 
            $subject_info = $terminal_information[$i];
            $subject_information = explode('-', $subject_info);
            ?>
            <tr>
                <td><?echo isset($subject_information[0]) ? $subject_information[0] : 'null'; ?></td>
                <td><?echo isset($subject_information[1]) ? $subject_information[1] : ''; ?></td>
                <td><?echo isset($subject_information[2]) ? $subject_information[2] : ''; ?></td>
                <?php
                    if($is_practical){
                        ?>
                        <td><?echo isset($subject_information[3]) ? $subject_information[3] : ''; ?></td>
                        <td><?echo isset($subject_information[4]) ? $subject_information[4] : ''; ?></td>
                        <?php
                    }
                ?>
            </tr>
            <?php
        }
    ?>

    </table>
 </div>
 </div>
 <div class="row-fluid">

    <?php
        echo CHtml::linkButton('DOWNLOAD EXCEL',array(
            'type'=>'submit',
            'method'=>'POST',
            'class'=>'btn btn-primary',
            'confirm'=>"Do you want to download excel file",
            'params'=> array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken)));
    ?>
 </div>
<div class="clear-fix"></div>
<div class="row-fulid" style="overflow: auto;">
<?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'indfeerecord-grid',
        'dataProvider'=>$StudentList,
        'htmlOptions'=>array('class'=>'table table-striped table-bordered', 'type'=>'raw'),
        'columns'=>$column,
));
}
echo "</div>";
if(isset($_POST) && !empty($_POST))
{
    $this->widget('EExcelView', array(
     'dataProvider'=>new CArrayDataProvider(
        $student_information,array(
            'keyField'=>'name'
            )),
            'grid_mode'=>'export',
            'title'=>'Result Legder',
            'filename'=>$excel_header.' Raw Marks Legder '.$academic_year.' ('.date('Y-m-d').')',
            'stream'=>true,
            'autoWidth'=>false,
            'exportType'=>'Excel2007',
            'columns'=>$column,
        ) );

}
?>

<script type="text/javascript">

    $('#show_terminal_information').on('click', function(){
        $('#pass_full_mark_information').show('slow');
    });
    $('#hide_terminal_information').on('click', function(){
        $('#pass_full_mark_information').hide('slow');
    });

    $('body').on('change', '#school_level', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        if (selected.length > 0) {
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#program_level').html(response.option);
                    } else {
                        $('#program_level').html('');
                        $('#class_id').html('');
                    }
                }
            });

            var qual_url = $('#qualification_url').val();
            if (typeof qual_url != "undefined"){
                var url_2 = qual_url + '/key/' + selected;
                $.ajax({
                    type: 'POST',
                    url: url_2,
                    dataType: 'JSON',
                    success: function(response) {
                        $('#qualification_details_1').html(response.view);
                    }
                });
            }
        }
    });


    $('body').on('change', '#program_level', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        if (selected.length > 0) {
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#class_id').html(response.option);
                    } else {
                        $('#class_id').html('');
                    }
                }
            });
        }
    });


    $('body').on('change', '#class_id', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        if (selected.length > 0) {
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#termnial_id').html(response.option);
                    } else {
                        $('#termnial_id').html('');
                    }
                }
            });
        }
    });
</script>
