<?php
$this->breadcrumbs=array(
	'Mark Obtaineds'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List MarkObtained','url'=>array('index')),
array('label'=>'Create MarkObtained','url'=>array('create')),
array('label'=>'Update MarkObtained','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete MarkObtained','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage MarkObtained','url'=>array('admin')),
);
?>

<h1>View MarkObtained #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'academic_year',
		'terminal_id',
		'terminal_',
		'student_id',
		'subject_id',
		'subject_name',
		'grace_mark',
		'theory_mark',
		'practical_mark',
		'cas_mark',
		'total_mark',
		'is_practical',
		'is_cas',
		'theory_pass_mark',
		'practical_pass_mark',
		'theory_full_mark',
		'practical_full_mark',
		'terminal_percent',
		'cas_percent',
		'percent_for_final',
		'subject_status',
		'status',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
		'approved_by',
		'approved_date',
		'school_id',
),
)); ?>
