
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'subject-information-form',
	'enableAjaxValidation'=>false,
	'type' => 'horizontal',
	'enableClientValidation' => true,
	'htmlOptions' => array(
		'class' => 'form-horizontal well form-horizontal',
		'enctype' => 'multipart/form-data',
	),
	'focus' => array($model, 'title'),
)); ?>
<h2><strong>UPLOAD EXCEL FILE OF MARKS</strong></h2>
    <div class="row-fluid">
    	<div class="span6">
    	<?php echo $form->fileFieldRow($model,'upload_files',array('class'=>'span12','maxlength'=>200, 'required'=>'required')); ?></div>
    	<div class="span6">
        <?php echo $form->dropDownListRow($model,'insert_type',array('both'=>'Theory && Practical', 'theory'=>'Theory','practical'=>'Practical'), array('class'=>'span12')); ?>
            
        </div>
	</div>
    <div class="row-fluid">
        <?php
        if(UtilityFunctions::ShowSchool()){
        echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => Yii::app()->params['select_'])),'id',Yii::app()->params['select_']),array('class'=>'span12', 'prompt'=>'Select School', 'selected'=>1)); 
        }else{
            $user_information = UtilityFunctions::UserInformations();
            $school_id = isset($user_information['school_id']) ? $user_information['school_id'] : null;
            echo $form->hiddenField($model,'school_id',array('value'=>$school_id));
        }
        ?> 

    </div>
	<div class="form-actions text-center">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Submit Data',
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<?php
	if(isset($subject_information) && !empty($subject_information) && isset($excel_array_data) && !empty($excel_array_data)){
        $genral_practical = $subject_information->passMark ?  $subject_information->passMark->is_practical : null;
		$is_practical = in_array($insert_type, ['both','practical']) && $genral_practical  ?  1 : null;  
		$theory_full_mark = isset($rules_information['theory_full_mark']) && $rules_information['theory_full_mark'] !=0 ? $rules_information['theory_full_mark'] : 0;
		$practical_full_mark = isset($rules_information['practical_full_mark']) && $rules_information['practical_full_mark'] !=0 ? $rules_information['practical_full_mark'] : 0;
		$total_full_mark = $theory_full_mark + $practical_full_mark;
		?>

<div class="form-horizontal well form-horizontal">

<div class="row-fluid">
      
<div class="row-fluid well">
    <h4 class="text-center">Subject Informaiton</h4>
        <table class="table table-striped table-bordered">
            <thead>
                <td colspan="<?= $is_practical ? 3 :1; ?>" > <strong>SCHOOL : <?= ucwords($school_information->title.', '.Yii::app()->params['municipality_short'].'-'.$school_information->ward_no) ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; CODE :  <?= $school_information->schole_code; ?></strong></td>
            </thead>
            <tr><td colspan="<?= $is_practical ? 3 :1; ?>" >Name :  <?= isset($subject_information)? ucwords($subject_information->title) : 'Null' ;?></td></tr>

            <tr>
                <?php
                    if(!$is_practical){
                        ?>
                        <td>Full Mark : <?= $theory_full_mark;?></td>
                        <?php
                    }else{
                        ?>
                        <td>Theory FM : <?= $theory_full_mark;?></td>
                        <td>Practical FM : <?= $practical_full_mark;?></td>
                        <td>Full Mark : <?= $total_full_mark;?></td>
                        <?php
                    }
                ?>
            </tr>
        </table>
</div>

</div>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'id'=>'insert-student-mark',
    'action'=>Yii::app()->request->baseUrl.'/markObtained/insertMarks',
    'htmlOptions' => array(
        'class' => 'form-horizontal well',
    ),
    'focus' => array($model, 'title'),
)); ?>
    <table class="table table-hover table-bordered">
        <tr>
            <th>SN</th>
            <th colspan="2">Student Name</th>
            <th>Symbol Number</th>
            <?php
            if(in_array($insert_type, ['both','theory'])){
                ?>
                <th colspan="1"><?= isset($rules_information['is_practical']) && $rules_information['is_practical'] ?  "Obtaine Mark " : "Theory Mark"; ?></th>

                <?php
            }
            if($is_practical && in_array($insert_type, ['both','practical'])){
                ?>
                <th colspan="1">Pratical Marks</th>
            <?php
            }
            ?>

        </tr>
        <?php
        $sn=0;
        if(!empty($excel_array_data) && !empty($school_information)){
        	for ($i=0; $i <= sizeof($excel_array_data) ; $i++) 
        	{
                $registration_number = isset($excel_array_data[$i]['registration_number']) ? $excel_array_data[$i]['registration_number'] : 0;
	            if($registration_number && $registration_number!=0){
	                $subject_id = isset($excel_array_data[$i]['subject_id']) ? $excel_array_data[$i]['subject_id'] : 0;
	                $academic_year = UtilityFunctions::AcademicYear();
	                $marks_information = MarkObtained::model()->findByAttributes(['subject_id'=>$subject_id,'student_id'=>$registration_number,'academic_year'=>$academic_year,'school_id'=>$school_id]);

                    $th_marks = $is_practical ? 'TH_Marks' : "Obt_Marks";
                    $th_value = isset($excel_array_data[$i]['TH_Marks']) ? $excel_array_data[$i]['TH_Marks'] : null;
                    if(!$th_value && isset($excel_array_data[$i]['Obt_Marks']))
                        $th_value = $excel_array_data[$i]['Obt_Marks'];
	                ?>
	                <tr>
	                    <td><?php echo $sn+1; ?>
	                        <input type="hidden" name="<?php echo $sn.'_registration_number'; ?>" value="<?php echo $registration_number; ?>">
	                    </td>
	                    <td><?= isset($excel_array_data[$i]['name']) ? strtoupper($excel_array_data[$i]['name']) : ''; ?></td>
	                    <td colspan="2"><?= isset($excel_array_data[$i]['symbol_number']) ? strtoupper($excel_array_data[$i]['symbol_number']) : ''; ?></td>
                        <?php
                        if(in_array($insert_type, ['both','theory'])){
                        ?>
	                    <td><input type="number" <?php if($th_value){ ?>value="<?= $th_value; ?>" <?php } ?>  id="<?php echo $registration_number.'_'.$sn.'_theory_mark'; ?>" name="<?php echo $registration_number.'_'.$sn.'_theory_mark'; ?>" max="<?= $theory_full_mark; ?>" required> </td>


	                <?php 
                    }
                    if($is_practical && in_array($insert_type, ['both','practical']))
	                {
	                    ?>
	                    <td><input type="number" <?php if(!empty($excel_array_data[$i]['PRT_Marks'])){ ?>value="<?php echo $excel_array_data[$i]['PRT_Marks']; ?>" <?php } ?>  id="<?php echo $registration_number.'_'.$sn.'_practical_mark'; ?>" name="<?php echo $registration_number.'_'.$sn.'_practical_mark'; ?>" max="<?= $practical_full_mark; ?>" required> </td>
	                   <?php
	                }
	                ?>
	                </tr>
		            <?php
		            $sn++;
	        	}
            }
        ?>
            <?php
        }else{
            ?>
            <div class="alert aler-info text-center">Please, Check Your Excel data, We cannot find school </div>
            <?php
        }
        ?>
    </table>
            <input type="hidden" name="subject_id" value="<?php echo $subject_id; ?>">
            <input type="hidden" name="terminal_id" value="<?php echo $terminal_id; ?>">
            <input type="hidden" name="academic_year" value="<?php echo $academic_year; ?>">
            <input type="hidden" name="school_id" value="<?php echo $school_id; ?>">
            <input type="hidden" name="total_number" value="<?php echo $sn; ?>">
            <input type="hidden" name="is_practical" value="<?= $is_practical; ?>">
            <input type="hidden" name="theory_full_mark" value="<?= isset($rules_information['theory_full_mark']) ?  $rules_information['theory_full_mark'] : 0; ?>">
            <input type="hidden" name="practical_full_mark" value="<?= isset($rules_information['practical_full_mark']) ?  $rules_information['practical_full_mark'] : 0; ?>">
    <?php if($sn !=0){
        ?>
    <div class="row-fluid text-center">
        <input type="submit" name="submit" value="Submit Marks" class="btn btn-info">
    </div>
    <?php
    }
    ?>
    <?php $this->endWidget(); ?>
    <?php
	}


?>
</div>
