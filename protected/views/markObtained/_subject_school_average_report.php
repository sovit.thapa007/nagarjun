<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'id'=>'subject-information-form',
    'enableAjaxValidation'=>false,
    'type' => 'horizontal',
    'enableClientValidation' => true,
    'method' => 'GET',
    'htmlOptions' => array(
        'class' => 'form-horizontal well form-horizontal',
    ),
    'focus' => array($model, 'title'),
)); ?>
<h2><strong>SUBJECT / SCHOOL WISE AVERAGE GRADE REPORT - ACADEMIC YEAR</strong></h2>
<div class="row-fluid">
    <div class="span6">
    <?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span12','maxlength'=>200, 'required'=>'required')); ?>
    </div>
        <div class="span6">
            <?php echo $form->dropDownListRow($model,'result_',array('gp'=>'AVERAGE GP', 'numeric_mark'=>'Average Numeric Mark','both'=>'Both Average Gp/Mark'), array('class'=>'span12')); ?>
        </div>
    </div>
    <div class="form-actions text-center">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'SEARCH RESULT',
        )); ?>
    </div>

<?php $this->endWidget(); ?>
<?php
    if(!empty($reportData)){

        $dataArray = $reportData['dataArray'];
        $school_id_arry = $reportData['school_id_arry'];
        $school_array = $reportData['school_array'];
        $subject_array = $reportData['subject_array'];
        $subject_id_array = $reportData['subject_id_array'];
        ?>

        <div class="form-horizontal well form-horizontal" style="overflow: auto;">
                <h2 class="text-center"> <STRONG>
                <?= strtoupper(UtilityFunctions::MunicipalLedgerHeader($academic_year)); ?><BR />SCHOOL/SUBJECT WISE AVERAGE GRADE</STRONG> </h2>
            <table id="retak_exam_section" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>SN</th>
                    <th>School</th>
                    <?php
                    for ($i=0; $i < sizeof($subject_id_array) ; $i++) { 
                        $subject_id = $subject_id_array[$i];
                        $name = isset($subject_array[$subject_id]) ? $subject_array[$subject_id] :'' ;
                        ?>
                        <th><?= $name; ?></th>
                        <?php
                    }
                    ?>
                </tr>
                </thead>
            <?php
            for ($j=0; $j < sizeof($school_id_arry) ; $j++){
                $school_id = $school_id_arry[$j];
                $schoolName = isset($school_array[$school_id]) ? strtoupper($school_array[$school_id]) : null;
                ?>
                <tr>
                    <td><?= $j+1; ?></td>
                    <td><?= $schoolName; ?></td>
                    <?php
                    for ($k=0; $k < sizeof($subject_id_array) ; $k++) { 
                        $subject_id = $subject_id_array[$k];
                        $grade = isset($dataArray[$school_id][$subject_id]['g']) ? $dataArray[$school_id][$subject_id]['g'] : '';
                        $gp = isset($dataArray[$school_id][$subject_id]['gp']) ? $dataArray[$school_id][$subject_id]['gp'] : '';

                        $marks = isset($dataArray[$school_id][$subject_id]['marks']) ? $dataArray[$school_id][$subject_id]['marks'] : '';
                        $result = $gp;
                        if($result_ == 'numeric_mark')
                            $result = $marks;
                        if($result_ == 'both')
                            $result = $marks.'('.$gp.')';
                        ?>
                        <td>
                            <?= $result; ?>
                        </td>
                        <?php
                    }
                    ?>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>
        
<script type="text/javascript">
    
    $(document).ready(function () {
        $('#retak_exam_section').DataTable( {
            lengthMenu: [
                [ 25, 30, 50, -1 ],
                [ '25 rows', '30 rows', '50 rows', 'Show all' ]
            ],
            dom: 'Bfrtip',
            buttons: [
                'pageLength','copy', 'csv', 'excel', 'pdf', 'print'
            ],
            //"order": [[ 6, 'asc' ]]
        } );


    });

</script>
        <?php
    }
    
?>

