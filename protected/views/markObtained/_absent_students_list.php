<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'id'=>'subject-information-form',
    'enableAjaxValidation'=>false,
    'type' => 'horizontal',
    'enableClientValidation' => true,
    'htmlOptions' => array(
        'class' => 'form-horizontal well',
    ),
    'focus' => array($model, 'title'),
)); ?>

    <h2><strong>SEARCH FORM FOR RETAKE 2</strong></h2>
    <div class="row-fluid">
        <div class="span6">
        <?php echo $form->textFieldRow($model,'academic_year',array('class'=>'span12')); ?></div>
        <div class="span6">
            <?php 
            echo $form->dropDownListRow($model,'options',['general'=>'General', 'pdf'=>'PDF DOWNLOAD'],array('class'=>'span12', 'selected'=>1)); 
            ?> 
        </div>
    </div>
    <div class="form-actions text-center">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Submit Form',
        )); ?>
    </div>

<?php $this->endWidget(); ?>

<?php
if(!empty($student_id_array)){
?>
<div class="row-fluid form-horizontal well" style="width: 100%; overflow: auto;">
    <h2><strong>LIST OF ABSENT STUDENTS</strong></h2>

    <table id='datatable' class="table table-bordered" style="margin-right: 10px !important; ">
        <thead>
            <tr>
                <th>SYMBOL NO.</th>
                <th>NAME</th>
                <th>TYPE</th>
                <th>SCHOOL CODE</th>
                <th>SCHOOL</th>
            </tr>
        </thead>
        <tbody>
        <?php
        for ($i=0; $i < sizeof($student_id_array) ; $i++) { 
            $student_id = $student_id_array[$i];

            ?>
            <tr>
                <td><?= isset($student_detail_array[$student_id]['symbol_number']) ? $student_detail_array[$student_id]['symbol_number'] : ''; ?></td>
                <td><?= isset($student_detail_array[$student_id]['name']) ? $student_detail_array[$student_id]['name'] : ''; ?></td>
                <td><?= isset($student_detail_array[$student_id]['school_type']) ? $student_detail_array[$student_id]['school_type'] : ''; ?></td>
                <td><?= isset($student_detail_array[$student_id]['school_code']) ? $student_detail_array[$student_id]['school_code'] : ''; ?></td>
                <td><?= isset($student_detail_array[$student_id]['school_name']) ? $student_detail_array[$student_id]['school_name'] : ''; ?></td>
            </tr>
            <?php
        } 
        ?>

        </tbody>

        <tfoot>
            <tr>
                <th>SYMBOL NO.</th>
                <th>NAME</th>
                <th>TYPE</th>
                <th>SCHOOL CODE</th>
                <th>SCHOOL</th>
            </tr>
        </tfoot>
    </table>
</div>
<?php
    }
?>