<?php
$this->breadcrumbs = array(
    'Subject Wise Grade No.' => array('SubjectWiseGradeStudentNumber'),
    'STUDENT LIST',
);
$colspan = $subject_information->is_practical ? 6 : 3;
?>
<div class="form-horizontal well">

<h2><strong>Subject Name : <?= $subject_information ? ucwords($subject_information->subject_name) : ''; ?></strong></h2>
<table id='datatable' class="table table-bordered" style="width: 100%;">
        <thead>
            <tr>
                <th>School</th>
                <th>Code</th>
                <th>Student Name</th>
                <th>Obtained Marks</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php
                if(!empty($student_list)){
                    $sn = 1;
                    foreach ($student_list as $student_info) {
                        $grade = $student_info->total_grade;
                        if($student_info->is_practical == 0){
                            $grade .= ' <small> ( '.(int) $student_info->total_mark.' )<small>';
                        }else{
                            $grade .= ' <small> ( Th. : '.(int) $student_info->theory_mark.'/'.$student_info->theory_grade.', Pr : '.(int) $student_info->practical_mark.'/'.$student_info->practical_grade.', Total : '.(int) $student_info->total_mark.' )<small>';
                        }
                        ?>
                        <tr>
                            <td><?= $student_info->school ? ucwords($student_info->school->title) : '' ; ?></td>
                            <td><?= $student_info->school ? $student_info->school->schole_code : '' ; ?></td>
                            <td><?= $student_info->student ? ucwords($student_info->student->first_name.' '.$student_info->student->middle_name.' '.$student_info->student->last_name) : '' ; ?></td>
                            <td><?= $grade; ?></td>
                            <td><?= $student_info->status; ?></td>
                        </tr>
                        <?php
                        $sn++;
                    }
                }
            ?>
        </tbody>
        <tfoot>
            <tr>
                <th>School</th>
                <th>Code</th>
                <th>Student Name</th>
                <th>Obtained Marks</th>
                <th>Status</th>
            </tr>
        </tfoot>
    </table>
</div>