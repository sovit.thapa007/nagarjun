<style type="text/css">
    tr td{
    page-break-inside: avoid;
    white-space: nowrap;
}
</style>
    <div  style="text-align: center; font-weight: bold;"><?= strtoupper(UtilityFunctions::MunicipalLedgerHeader($academic_year)); ?><br /> SUMMERY REPORT</div>
    <br />
    <h2 style="text-align: center;"><strong>LIST OF ABSENT STUDENTS</strong></h2>

    <table style="border-collapse: collapse; width: 100% !important; border: 1px solid #000;">
            <tr style="border: 1px solid #000;">
                <th style="border: 1px solid #000;">SYMBOL NO.</th>
                <th style="border: 1px solid #000;">NAME</th>
                <th style="border: 1px solid #000;">TYPE</th>
                <th style="border: 1px solid #000;">SCHOOL CODE</th>
                <th style="border: 1px solid #000;">SCHOOL</th>
            </tr>
        <?php
        for ($i=0; $i < sizeof($student_id_array) ; $i++) { 
            $student_id = $student_id_array[$i];

            ?>
            <tr style="border: 1px solid #000;">
                <td style="border: 1px solid #000;"><?= isset($student_detail_array[$student_id]['symbol_number']) ? $student_detail_array[$student_id]['symbol_number'] : ''; ?></td>
                <td style="border: 1px solid #000;"><?= isset($student_detail_array[$student_id]['name']) ? $student_detail_array[$student_id]['name'] : ''; ?></td>
                <td style="border: 1px solid #000;"><?= isset($student_detail_array[$student_id]['school_type']) ? $student_detail_array[$student_id]['school_type'] : ''; ?></td>
                <td style="border: 1px solid #000;"><?= isset($student_detail_array[$student_id]['school_code']) ? $student_detail_array[$student_id]['school_code'] : ''; ?></td>
                <td style="border: 1px solid #000;"><?= isset($student_detail_array[$student_id]['school_name']) ? $student_detail_array[$student_id]['school_name'] : ''; ?></td>
            </tr>
            <?php
        } 
        ?>
    </table>