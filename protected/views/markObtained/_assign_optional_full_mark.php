<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'mark-obtained-form',
	'enableAjaxValidation'=>false,
)); ?>
		<?php echo $form->hiddenField($model,'subject_id',array('value'=>$model->subject_id)); ?>
		<?php echo $form->hiddenField($model,'school_id',array('value'=>$model->school_id)); ?>
		<?php echo $form->hiddenField($model,'options',array('value'=>$options)); ?>
		<?php echo $form->hiddenField($model,'order_by',array('value'=>$order_by)); ?>
		<?php echo $form->dropDownListRow($model,'sep_th_fm',array('100-0'=>'THEORY ONLY/NO PRACTICAL', '50-50'=>'50 THEORY / 50 PRACTICAL', '75-25'=>'75 THEORY / 25 PRACTICAL'),array('class'=>'span12'),['label' => 'OPT. SUBJECT TYPE']); ?>

	<div class="form-actions text-center">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'SUBMIT INFORMATION'
		)); ?>
</div>

<?php $this->endWidget(); ?>
