
<?php
    $tr_pr_colspan = ($result_ == 'general') ? 3 : 4;
    $tr_no_pr_colspan = ($result_ == 'general') ? 1 : 2;
?>

<div class="row-fulid" style="overflow: auto;">
    <table class="display items table" style="border-collapse: collapse; font-size: 25pt;" >
        <thead>
            <tr>
                <th colspan="<?= sizeof($column); ?>" style="font-size: 50pt;text-align: center; "> <?= strtoupper(Yii::app()->params['municipality']) ?> <br /><?= strtoupper(Yii::app()->params['office'])?> <br /><?= $academic_year; ?> <br /><?= strtoupper(Yii::app()->params['provinance'])?><br /> <?= strtoupper(Yii::app()->params['address'])?>. <br>LOCAL LEVEL EXAMINATION GRADE EIGHT B.S <?= $academic_year; ?> 
                </th>
            </tr>

            <tr>
                <td colspan="9" style="font-size: 35pt;font-weight: bold;">SCHOOL NAME : <?= $school_information ? $school_information->title : ''; ?></td>
                <td colspan="9" style="font-size: 35pt;font-weight: bold;">SCHOOL TYPE : <?= $school_information ? $school_information->type : ''; ?></td>
                <td colspan="<?= sizeof($column) - 18;  ?>" style="font-size: 35pt;font-weight: bold;">Ward No. : <?= $school_information ? $school_information->ward_no : ''; ?></td>
            </tr>
            <tr>
                <td rowspan="6" style="border:2px solid black; padding: 10px; padding: 10px;">SN</td>
                <td rowspan="6" style="border:2px solid black; padding: 10px; padding: 10px;">Student Name</td>
                <td rowspan="6" style="border:2px solid black; padding: 10px; padding: 10px;">Symbol No.</td>
                <?php
                for ($i=0; $i < sizeof($subject) ; $i++) { 
                    ?>
                    <td style="border:2px solid black; padding: 10px; padding: 10px;" colspan="<?= $subject[$i]['is_practical'] == 1 ?  $tr_pr_colspan : $tr_no_pr_colspan ; ?>"><?= $subject[$i]['name']; ?></td>
                    <?php
                }
                if($result_ == 'general'){
                    ?>
                    <td rowspan="6" style="border:2px solid black; padding: 10px;">Total Obtained</td>
                    <td rowspan="6" style="border:2px solid black; padding: 10px;">Total FM.</td>
                    <td rowspan="6" style="border:2px solid black; padding: 10px;">Result</td>
                    <td rowspan="6" style="border:2px solid black; padding: 10px;">Ranks</td>
                    <td rowspan="6" style="border:2px solid black; padding: 10px;">Percentage</td>
                    <td rowspan="6" style="border:2px solid black; padding: 10px;">Division</td>
                    <td rowspan="6" style="border:2px solid black; padding: 10px;">Attendance</td>
                    <td rowspan="6" style="border:2px solid black; padding: 10px;">Total Attendance</td>
                    <?php
                }else{
                    ?>
                    <td rowspan="6" style="border:2px solid black; padding: 10px;">Total Obtained</td>
                    <td rowspan="6" style="border:2px solid black; padding: 10px;">GPA.</td>
                    <td rowspan="6" style="border:2px solid black; padding: 10px;">Total FM.</td>
                    <td rowspan="6" style="border:2px solid black; padding: 10px;">Attendance</td>
                    <td rowspan="6" style="border:2px solid black; padding: 10px;">Total Attendance</td>
                    <?php
                }
                ?>
            </tr>

            <tr>
                <?php
                for ($i=0; $i < sizeof($subject) ; $i++) { 
                    ?>
                    <td colspan="<?= $subject[$i]['is_practical'] == 1 ?  $tr_pr_colspan : $tr_no_pr_colspan; ?>" style="border:2px solid black; padding: 10px;">Full Marks</td>
                    <?php
                }
                ?>
            </tr>
            <tr>
                <?php
                for ($i=0; $i < sizeof($subject) ; $i++) { 
                    if($subject[$i]['is_practical'] == 1){
                        ?>
                        <td style="border:2px solid black; padding: 10px;">Theory</td>
                        <td style="border:2px solid black; padding: 10px;">Practical</td>
                        <td style="border:2px solid black; padding: 10px;">Total</td>
                        <?
                    }else{
                        ?>
                        <td> </td>
                        <?php
                    }
                    if($result_ != 'general'){
                    ?>
                    <td rowspan="2" style="border:2px solid black; padding: 10px;"> GP </td>
                    <?php
                    }
                }
                ?>
            </tr>
            <tr>
                <?php
                for ($o=0; $o < sizeof($subject) ; $o++) { 
                    if($subject[$o]['is_practical'] == 1){
                        ?>
                        <td style="border:2px solid black; padding: 10px;"><?= (int) $subject[$o]['theory_full_mark']; ?></td>
                        <td style="border:2px solid black; padding: 10px;"><?= (int) $subject[$o]['practical_full_mark']; ?></td>
                        <td style="border:2px solid black; padding: 10px;"><?= (int) $subject[$o]['theory_full_mark'] + (int) $subject[$o]['practical_full_mark']; ?></td>
                        <?
                    }else{
                        ?>
                        <td style="border:0px 2px 2px 2px solid black; padding: 10px;"><?= $subject[$o]['theory_full_mark']; ?></td>
                        <?php
                    }
                }
                ?>
            </tr>
            <tr>
                <?php
                for ($p=0; $p < sizeof($subject) ; $p++) { 
                    ?>
                    <td colspan="<?= $subject[$p]['is_practical'] == 1 ?  $tr_pr_colspan : $tr_no_pr_colspan; ?>" style="border:2px solid black; padding: 10px;">Obtained Marks</td>
                    <?php
                }
                ?>
            </tr>
            <tr>
                <?php
                for ($r=0; $r < sizeof($subject) ; $r++) { 
                    if($subject[$r]['is_practical'] == 1){
                        ?>
                        <td style="border:2px solid black; padding: 10px;">Theory</td>
                        <td style="border:2px solid black; padding: 10px;">Practical</td>
                        <td style="border:2px solid black; padding: 10px;">Total</td>
                        <?
                    }else{
                        ?>
                        <td style="border:2px solid black; padding: 10px;"> Theory </td>
                        <?php
                    }
                    if($result_ != 'general'){
                        ?>
                        <td style="border:2px solid black; padding: 10px;"> GP </td>
                        <?php
                    }
                }
                ?>
            </tr>
        </thead>
        <tbody>

                <?php
                    for ($s=0; $s < sizeof($student_information) ; $s++) { 
                        ?>
                        <tr>
                            <td style="border:2px solid black; padding: 10px;"><?= $s+1; ?></td>
                        <?php
                        for ($t=0; $t < sizeof($column) ; $t++) { 
                            ?>
                            <td style="border:2px solid black; padding: 10px;"><?= isset($student_information[$s][$column[$t]]) ? $student_information[$s][$column[$t]] : 'null'; ?></td>
                            <?php
                        }
                        ?>
                        </tr>
                        <?php
                    }
                ?>
       </tbody>
       <tfooter>
           
            <tr>
                <td colspan="9" style="font-size: 35pt;font-weight: bold;">data entry</td>
                <td colspan="9" style="font-size: 35pt;font-weight: bold;">checked by</td>
                <td colspan="<?= sizeof($column) - 18;  ?>" style="font-size: 35pt;font-weight: bold;">verify</td>
            </tr>
       </tfooter>
    </table>
    <div align="center"><b>{PAGENO} / {nbpg}</b></div>
</div>