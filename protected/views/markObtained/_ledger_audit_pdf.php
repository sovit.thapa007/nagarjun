
<div class="row-fulid" style="overflow: auto;">
    <table class="display items table" style="border-collapse: collapse;" >
        <thead>
            <tr>
                <th colspan="21" style="text-align: center; "> <?= strtoupper(UtilityFunctions::MunicipalLedgerHeader($academic_year)); ?>
                </th>
            </tr>
            <tr>
                <td colspan="<?= sizeof($column); ?>"> <hr />
                </td>
            </tr>

            <tr>
                <td colspan="15">SCHOOL NAME : <?= $school_information ? strtoupper($school_information->title.', '.Yii::app()->params['municipality_short'].' - '.$school_information->ward_no) : ''; ?></td>
                <td colspan="<?= sizeof($column) - 15;  ?>">School Code : : <?= $school_information ? strtoupper($school_information->schole_code) : ''; ?></td>
            </tr>

            <tr>
                <td rowspan="2" style="border:1px solid black;">SN</td>
                <td rowspan="2" style="border:1px solid black;">&nbsp;&nbsp;StudentName&nbsp;&nbsp;</td>
                <td rowspan="2" style="border:1px solid black;">Symbol No.</td>
                <?php
                if(!empty($subjectInformation)){
                foreach ($subjectInformation as $subject) {
                    $is_practical = $subject->passMark ? $subject->passMark->is_practical : null;
                    $border_style = $is_practical || (in_array($subjectNature_, [2,3]) && $opt_is_practical) ? 'border:1px solid black' : 'border-top:1px solid black';
                    ?>
                    <td style="<?= $border_style; ?>;" colspan="<?= $is_practical == 1 ?  2 : 1 ; ?>"><?= ucwords($subject->title); ?></td>
                    <?php
                }
                }
                    ?>
            </tr>

            <tr>
                <?php
                if(!empty($subjectInformation)){
                foreach ($subjectInformation as $subject) {
                $subjectNature_ = $subject->subject_nature;
                    $is_practical = $subject->passMark ? $subject->passMark->is_practical : null;
                    if($is_practical || (in_array($subjectNature_, [2,3]) && $opt_is_practical)){
                        ?>
                        <td style="border:1px solid black;">TH</td>
                        <td style="border:1px solid black;">PR</td>
                        <?php
                    }else{
                        ?>
                        <td> </td>
                        <?php
                    }
                }
                }
                ?>
            </tr>
        </thead>
        <tbody>

                <?php
                    for ($s=0; $s < sizeof($dataArray) ; $s++) { 
                        ?>
                        <tr>
                            <td style="border:1px solid black;"><?= $s+1; ?></td>
                        <?php
                        for ($t=0; $t < sizeof($column) ; $t++) {
                            $value =  isset($dataArray[$s][$column[$t]]) ? $dataArray[$s][$column[$t]] : 'null';
                            $background = $value == 'not-set' || $value == 'null' ? 'red' : '';
                            ?>
                            <td style="border:1px solid black; background-color: <?= $background; ?>"><?= $value; ?></td>
                            <?php
                        }
                        ?>
                        </tr>
                        <?php
                    }
                ?>
       </tbody>
    </table>
</div>
