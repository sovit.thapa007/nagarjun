
<?php
if(!empty($retake_ledger_report)){

    $student_number_detail_array = $retake_ledger_report['student_number_detail_array'];
    $subject_id_array = $retake_ledger_report['subject_id_array'];
    $subject_header = $retake_ledger_report['subject_header'];
?>
<div class="row-fluid form-horizontal well" style="width: 100%; overflow: auto;">

    <h3 style="text-align: center;"><?= strtoupper(UtilityFunctions::MunicipalLedgerHeader($academic_year)); ?></h3>
    <h4><strong>LIST OF STUDENT'S OBTAINED D && E GRADE IN TOTAL, SUBJECT WISE (TO GET ADMISSION IN CLASS NINE THOSE STUDENTS SHOULD UPGRADE THEIR GARDE)</strong></h4>

    <table style="border-collapse: collapse; width: 100% !important; border: 1px solid #000;">
        <thead>
            <tr style=" border: 1px solid #000;">
                <th style=" border: 1px solid #000;">SN.</th>
                <th style=" border: 1px solid #000;">SUBJECT</th>
                <th style=" border: 1px solid #000;">STUDENT'S NUMBER</th>
            </tr>
        </thead>
        <tbody>
        <?php
        if(!empty($subject_id_array)){
            for ($i=0; $i < sizeof($subject_id_array) ; $i++) { 
                $subject_id = $subject_id_array[$i];
            ?>
            <tr style=" border: 1px solid #000;">
                <td style=" border: 1px solid #000;"><?= $i+1; ?></td>
                <td style=" border: 1px solid #000;"><?= isset($subject_header[$subject_id]) ? strtoupper($subject_header[$subject_id]) : ''; ?></td>
                <td style=" border: 1px solid #000;"><?= isset($student_number_detail_array[$subject_id]) ? strtoupper($student_number_detail_array[$subject_id]) : ''; ?></td>
            </tr>
            <?php
            }
        }
        ?>

        </tbody>
    </table>
</div>
<?php
    }
?>