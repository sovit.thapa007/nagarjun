<?php
$this->breadcrumbs=array(
	'Student Processings'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List StudentProcessing','url'=>array('index')),
array('label'=>'Manage StudentProcessing','url'=>array('admin')),
);
?>

<h1>Create StudentProcessing</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>