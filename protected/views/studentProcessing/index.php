<?php
$this->breadcrumbs=array(
	'Student Processings',
);

$this->menu=array(
array('label'=>'Create StudentProcessing','url'=>array('create')),
array('label'=>'Manage StudentProcessing','url'=>array('admin')),
);
?>

<h1>Student Processings</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
