<?php
$this->breadcrumbs=array(
	'Student Processings'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List StudentProcessing','url'=>array('index')),
array('label'=>'Create StudentProcessing','url'=>array('create')),
array('label'=>'Update StudentProcessing','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete StudentProcessing','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage StudentProcessing','url'=>array('admin')),
);
?>

<h1>View StudentProcessing #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'student_id',
		'school_id',
		'state',
		'status',
		'remarks',
		'created_by',
		'created_at',
),
)); ?>
