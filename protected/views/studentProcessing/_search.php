<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'student_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'school_id',array('class'=>'span5')); ?>

		<?php echo $form->dropDownListRow($model,'state',array("dataentry"=>"dataentry","headteacher"=>"headteacher","administrativeofficer"=>"administrativeofficer","result"=>"result",),array('class'=>'input-large')); ?>

		<?php echo $form->dropDownListRow($model,'status',array("not_checked"=>"not_checked","processing"=>"processing","accepted"=>"accepted","rejected"=>"rejected",),array('class'=>'input-large')); ?>

		<?php echo $form->textAreaRow($model,'remarks',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

		<?php echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_at',array('class'=>'span5')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
</div>

<?php $this->endWidget(); ?>
