<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'student-processing-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'student_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'school_id',array('class'=>'span5')); ?>

		<?php echo $form->dropDownListRow($model,'state',array("dataentry"=>"dataentry","headteacher"=>"headteacher","administrativeofficer"=>"administrativeofficer","result"=>"result",),array('class'=>'input-large')); ?>

		<?php echo $form->dropDownListRow($model,'status',array("not_checked"=>"not_checked","processing"=>"processing","accepted"=>"accepted","rejected"=>"rejected",),array('class'=>'input-large')); ?>

		<?php echo $form->textAreaRow($model,'remarks',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

		<?php echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_at',array('class'=>'span5')); ?>

	<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
