<?php
$this->breadcrumbs=array(
	'Student Processings'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List StudentProcessing','url'=>array('index')),
array('label'=>'Create StudentProcessing','url'=>array('create')),
array('label'=>'View StudentProcessing','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage StudentProcessing','url'=>array('admin')),
);
?>

<h1>Update StudentProcessing <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>