<!DOCTYPE html>
<html>

<style type="text/css">
.general_layout{
  min-height: 400px !important;
  text-align: center;
  margin-left: 5% !important;
  margin-right: 5% !important;
}

.preload { width:100px;
    height: 100px;
    position: fixed;
    top: 50%;
    left: 50%;}
</style>
<div class="preload"><img src="<?= Yii::app()->request->baseUrl ?>/images/load.gif"></div>
<body class="skin-blue sidebar-mini">
  <div class="wrapper">
    <div class="general_layout">
      <?php echo $content; ?>
    </div>
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          
        </div>
        <strong>Copyright &copy;  <?= date('Y').' '.Yii::app()->name; ?>

        <strong style="float: right;">Powered By  <a href="#" style="color: rgb(22, 160, 133); font-weight: normal;"><span></span></a>:SAMYAM GROUP</strong> 
      </footer>
    </div><!-- ./wrapper -->

   
  </body>

  <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.min.js"></script>
  <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.elevateZoom-3.0.8.min.js"></script><!-- 
   <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.fancybox.min.js"></script> -->
   
  <script type="text/javascript">
    $(window).load(function(){
     // PAGE IS FULLY LOADED  
     // FADE OUT YOUR OVERLAYING DIV
     $('.preload').fadeOut();
    });
    $( "form" ).submit(function( event ) {
      event.disabled = true;
      event.value = "Please wait...";
    });

  
  jQuery(document).ready(function(){
    $(".image").elevateZoom({
      zoomType        : "inner",
      cursor: "crosshair"
    });
/*
    //pass the images to Fancybox
    $(".image").bind("click", function(e) {  
      var ez =   $('.image').data('elevateZoom'); 
      $.fancybox(ez.getGalleryList());
      return false;
    });*/
  })
</script>
  </html>
