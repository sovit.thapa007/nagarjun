<!DOCTYPE html>
<html>
<head>
  <meta charset="utf8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="Medium Health Care Pvt Ltd">
  <title>School Control</title>
  
  <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/style_school.css" rel="stylesheet">
   <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/AdminLTE.css" rel="stylesheet">
  <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/skins/_all-skins.css" rel="stylesheet">

  <?php


    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
  ?>
</head>

<body class="skin-blue sidebar-mini">
  <div class="wrapper">
     <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo" style="height: 200px !important;">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <!-- logo for regular state and mobile devices -->
          <?php
          if(UtilityFunctions::SuperUser()){
            ?>
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo/ministry_education.jpg" style="height: 200px; width: 200px;">
            <?php
          }else{
            $user_information = UtilityFunctions::UserInformations();
            $school_information = BasicInformation::model()->findByPk($user_information['school_id']);
            $logo = !empty($school_information) ? $school_information->logo : 'null';
            ?>
            <img src="<?= Yii::app()->request->baseUrl; ?>/images/logo/<?= $logo; ?>" style="height: 200px; width: 200px;">
            <?php
          }

          ?>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav" style="font-family: Trebuchet MS, Helvetica, sans-serif;">
            <li>
             <a href="<?php echo Yii::app()->request->baseUrl; ?>/dashboard"><i class="fa fa-home"></i>&nbsp;&nbsp;Home</a>
             </li>
             <li>
              <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/user/logout"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;Logout</a>

            </li>
          </ul>
        </div>
      </nav>
      </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar" style="font-family: Trebuchet MS, Helvetica, sans-serif;">
      <!-- sidebar: style can be found in sidebar.less -->

      <section class="sidebar" style="margin-top: 140px !important;">
        <!-- Sidebar user panel -->
       <!-- <div class="user-panel">
          <a class="navbar-brand" href="#"><p style="margin-left: 12px; font-size: 22px; margin-top: 10px;">SCHOOL ADMIN</p></a>
        </div>-->
        <div class="side-user" style="padding-bottom: 40px;">
       
              <div class="clearfix"></div>
          </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" style="height: 20px; overflow: visible; ">
          <li class="header text-center"><strong><?= UtilityFunctions::AdminSection(); ?></strong></li>
          

           <!-- <li>
              <a href="<?php echo Yii::app()->request->baseUrl; ?>/dashboard">
                <i class="fa fa-home"></i> <span>Home</span> <i class="fa fa-angle-right pull-right"></i>
              </a>
            </li>-->


            <li>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/dashboard">
              <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-right pull-right"></i>
            </a>
             
            </li>

            <?php
              if(UtilityFunctions::SuperUser()){
                ?>
                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/BasicInformation/admin">
                  <i class="fa fa-user"></i> <span>SCHOOL'S LIST</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li>
                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/TeacherDetails/admin">
                  <i class="fa fa-user"></i> <span>TEACHERS LIST</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li>

                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/StudentInformation/create">
                  <i class="fa fa-user"></i> <span>NEW REGISTRATION</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li>
                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/StudentInformation">
                  <i class="icon-list"></i> <span>STUDENT LIST</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li>
                 <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/academicYear/admin">
                  <i class="icon-fullscreen"></i> <span>ACADEMIC YEAR</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li>

                <!-- 
                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/ClassSection/create">
                  <i class="icon-fullscreen"></i> <span>CLASS SECTION</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li> -->
                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/SubjectInformation">
                  <i class="icon-fullscreen"></i> <span>SUBJECT INFORMATION</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li>
                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/PassMark">
                  <i class="icon-fullscreen"></i> <span>PASS & FULL MARKS</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li>

                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/MarkObtained/ChooseSubject">
                  <i class="icon-pencil"></i> <span>INSERT STUDENT MARKS</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li>

                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/MarkObtained/UploadFile">
                  <i class="icon-pencil"></i> <span>UPLOADS OFFLINE MARKS</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li>

                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/result/generateAverageSchoolReport">
                  <i class="icon-pencil"></i> <span>GENERATE SCHOOL AVERAGE</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li>

                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/result/StudentGradeReport">
                  <i class="icon-pencil"></i> <span>STUDENT REPORT</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li>

                <!-- 
                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/studentAttendance/SelectClass">
                  <i class="icon-pencil"></i> <span>INSERT ATTENDANCE</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li> -->
                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/MarkObtained/PreviewRawMarks">
                  <i class="icon-list-alt"></i> <span>RAW MARKS LEDGERS</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li>
                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/markObtained/ledger">
                  <i class="icon-list-alt"></i> <span>FINAL MARKS LEDGERS</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li>
                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/MarkObtained/ResultPublishDate">
                  <i class="icon-list-alt"></i> <span>PUBLISH RESULT'S</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 </li>
                 <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/Result/ResultPDF">
                  <i class="icon-list-alt"></i> <span>RESULT'S</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                </li>
                <?php
              }else{
                ?>

                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/PassMark">
                  <i class="icon-fullscreen"></i> <span>PASS & FULL MARKS</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li>
                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/TeacherDetails/create">
                  <i class="icon-fullscreen"></i> <span>ADD TEACHER DETAILS</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li>
                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/StudentInformation/create">
                  <i class="fa fa-user"></i> <span>NEW REGISTRATION</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li>
                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/StudentInformation">
                  <i class="icon-list"></i> <span>STUDENT LIST</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li><!-- 
                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/ClassSection/create">
                  <i class="icon-fullscreen"></i> <span>CLASS SECTION</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li> -->

                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/MarkObtained/ChooseSubject">
                  <i class="icon-pencil"></i> <span>INSERT STUDENT MARKS</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li>
                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/MarkObtained/UploadFile">
                  <i class="icon-pencil"></i> <span>UPLOADS OFFLINE MARKS</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li><!-- 
                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/studentAttendance/SelectClass">
                  <i class="icon-pencil"></i> <span>INSERT ATTENDANCE</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li> -->
                <li>
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/MarkObtained/PreviewRawMarks">
                  <i class="icon-list-alt"></i> <span>MARKS LEDGERS</span> <i class="fa fa-angle-right pull-right"></i>
                </a>
                 
                </li>
                
            <?php
              }

            ?>
            

          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->

          <!-- Main content -->
          <section class="content">
              <?= UtilityFunctions::MunicipalityHeader(); ?>
              <?php echo $content; ?>
          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
        <footer class="main-footer">
          <div class="pull-right hidden-xs">
            
          </div>
          <strong>Copyright &copy; 2018 <?= Yii::app()->name; ?>

          <strong style="float: right;">Powered By  <a href="#" style="color: rgb(22, 160, 133); font-weight: normal;"><span style="font-weight: 500; font-family: 'Architects Daughter'; color: rgb(22, 160, 133);"></span></a>:SAMYAM GROUP</strong> 
        </footer>
      <!-- Add the sidebar's background. This div must be placed
      immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

   
  </body>

  <!-- load jquery files -->
  <?php
  $cs->registerScriptFile($baseUrl . '/js/jquery.validate.min.js');
  $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js');
  $cs->registerScriptFile($baseUrl . '/js/nepali.datepicker.min.js');
  $cs->registerScriptFile($baseUrl . '/js/unicode/pramukhime.js');
  $cs->registerScriptFile($baseUrl . '/js/unicode/pramukhindic-i18n.js');
  $cs->registerScriptFile($baseUrl . '/js/unicode/pramukhindic.js');
  ?>
  <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
      
    pramukhIME.addKeyboard("PramukhIndic");
    pramukhIME.enable();
    $(".nepaliUnicode").focus(function () {
    pramukhIME.setLanguage("nepali", "pramukhindic");
    });

    $(".nepaliUnicode").focusout(function () {
    pramukhIME.setLanguage("english", "pramukhime");
    });
    });

    </script>

  </html>
