
    <!-- Bootstrap core CSS -->
    <link href="<?= Yii::app()->request->baseUrl; ?>/bootstrap/css/bootstrap.css" rel="stylesheet" />
  <!-- Fixed navbar -->
  <div id="head-nav" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="fa fa-gear"></span>
        </button>
        <a class="navbar-brand" href="#"><img class="img-responsive" src="<?= Yii::app()->request->baseUrl; ?>/img/logos/logo.png"></a>
      </div>
      <div class="navbar-collapse collapse">

        <!-- User Account setting-->
        <ul class="nav navbar-nav navbar-right user-nav">
          <li class="dropdown profile_menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img alt="Avatar" src="<?= Yii::app()->request->baseUrl; ?>/img/admin2.jpg"  /><span>Jason Smith</span> <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="#">Account Setting</a></li>
              <li><a href="#">Help</a></li>
              <li class="divider"></li>
              <li><a href="#">Sign Out</a></li>
            </ul>

          </li>
        </ul><!-- User Account setting-->

        <ul class="nav navbar-nav navbar-right not-nav">
          <li class="button dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:;"><i class=" fa fa-comments"></i></i><span class="bubble">2</span></a>
            <ul class="dropdown-menu messages">
              <li>
                <div class="nano nscroller has-scrollbar">
                  <div class="content" tabindex="0" style="right: -17px;">
                    <ul>
                      <li>
                        <a href="#">
                          <img alt="avatar" src="<?= Yii::app()->request->baseUrl; ?>/img/avatar4_50.jpg"><span class="date pull-right">2 Nov.</span><span class="name">Michael</span> Have changed made on ..
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img alt="avatar" src="<?= Yii::app()->request->baseUrl; ?>/img/avatar3_50.jpg"><span class="date pull-right">2 Nov.</span><span class="name">Lucy</span> I have updated new one.
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img alt="avatar" src="<?= Yii::app()->request->baseUrl; ?>/img/avatar4_50.jpg"><span class="date pull-right">2 Nov.</span><span class="name">Michael</span> Have changed made on ..
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img alt="avatar" src="<?= Yii::app()->request->baseUrl; ?>/img/avatar3_50.jpg"><span class="date pull-right">2 Nov.</span><span class="name">Lucy</span> I have updated new one.
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div class="pane" style="display: none;"><div class="slider" style="height: 20px; top: 0px;"></div></div></div>
                  <ul class="foot"><li><a href="#">View all messages </a></li></ul>
                </li>
              </ul>
            </li>
          </ul>
      </div><!--/.nav-collapse animate-collapse -->
    </div>
  </div>