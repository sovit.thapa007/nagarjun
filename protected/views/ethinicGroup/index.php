<?php
$this->breadcrumbs = array(
	'Ethinic Groups',
);

$this->menu = array(
	array('label' => 'Create EthinicGroup', 'url' => array('create')),
	array('label' => 'Manage EthinicGroup', 'url' => array('admin')),
);
?>

<h1>Ethinic Groups</h1>

<?php
$this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider' => $dataProvider,
	'itemView' => '_view',
));
?>
