<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'ethinic-group-form',
	'enableAjaxValidation' => false,
	'type' => 'horizontal',
	'enableClientValidation' => true,
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
	),
	'focus' => array($model, 'title'),
	));
?>

<p class="text-center alert alert-info help-block">Fields with <span class="required">*</span> are required.</p><br />

<?php echo $form->errorSummary($model); ?>

<?php echo $form->dropDownListRow($model, 'parent_id', CHtml::listData(EthinicGroup::model()->findAll('parent_id =0'), 'id', 'title'), array('prompt' => 'Select Ethinic Group ', 'style' => 'width:455px; margin-bottom:1%;')); ?>
<?php //echo $form->textFieldRow($model,'parent_id',array('class'=>'span5')); ?>

<?php echo $form->textFieldRow($model, 'title', array('class' => 'span5', 'maxlength' => 50)); ?>

<?php echo $form->textFieldRow($model, 'description', array('class' => 'span5', 'maxlength' => 250)); ?>

<?php //echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

<?php //echo $form->textFieldRow($model,'created_date',array('class'=>'span5'));  ?>
<div class="text-center form-actions">
	<?php
	$this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'type' => 'primary',
		'label' => $model->isNewRecord ? 'Create Information' : 'Save Information',
	));
	?>
</div>

<?php $this->endWidget(); ?>
