<?php
$this->breadcrumbs = array(
	'Ethinic Groups' => array('index'),
	'Create',
);

$this->menu = array(
	array('label' => 'List EthinicGroup', 'url' => array('index')),
	array('label' => 'Manage EthinicGroup', 'url' => array('admin')),
);
?>

<h3>Ethinic Group</h3>
<hr />

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>