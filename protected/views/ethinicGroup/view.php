<?php
$this->breadcrumbs = array(
	'Ethinic Groups' => array('index'),
	$model->title,
);

$this->menu = array(
	array('label' => 'List EthinicGroup', 'url' => array('index')),
	array('label' => 'Create EthinicGroup', 'url' => array('create')),
	array('label' => 'Update EthinicGroup', 'url' => array('update', 'id' => $model->id)),
	array('label' => 'Delete EthinicGroup', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
	array('label' => 'Manage EthinicGroup', 'url' => array('admin')),
);
?>

<h2 class="text-center">View Ethinic Group #<?php echo $model->id; ?></h2>

<?php
$this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'type'=>'bordered stripped-bordered',
	'attributes' => array(
		'id',
		'parent_id',
		[
			'name'=>'parent_id',
			'value'=>EthinicGroup::model()->findByPk($model->parent_id) ? EthinicGroup::model()->findByPk($model->parent_id)->title : "null",
		],
		'title',
		'description',
		//'created_by',
		'created_date',
	),
));
?>
