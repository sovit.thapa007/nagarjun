<?php
$this->breadcrumbs = array(
	'Ethinic Groups' => array('index'),
	$model->title => array('view', 'id' => $model->id),
	'Update',
);

$this->menu = array(
	array('label' => 'List EthinicGroup', 'url' => array('index')),
	array('label' => 'Create EthinicGroup', 'url' => array('create')),
	array('label' => 'View EthinicGroup', 'url' => array('view', 'id' => $model->id)),
	array('label' => 'Manage EthinicGroup', 'url' => array('admin')),
);
?>

<h1>Update EthinicGroup <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>