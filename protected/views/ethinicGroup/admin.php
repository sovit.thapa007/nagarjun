<?php
$this->breadcrumbs = array(
	'Create Ethinic Groups' => array('create'),
	'Manage',
);

$this->menu = array(
	array('label' => 'List EthinicGroup', 'url' => array('index')),
	array('label' => 'Create EthinicGroup', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('ethinic-group-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2 class="text-center">Manage Ethinic Groups</h2>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'ethinic-group-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'type'=>'bordered stripped-bordered',
	'columns' => array(
		//'id',
		[
			'name'=>'parent_id',
			'value'=>'EthinicGroup::model()->findByPk($data->parent_id) ? EthinicGroup::model()->findByPk($data->parent_id)->title : "null"',
		],
		'title',
		'description',
		//'created_by',
		//'created_date',
		array(
			'header'=>'Action',
			'class' => 'bootstrap.widgets.TbButtonColumn',
		),
	),
));
?>
