<div class="row-fluid quick-actions">
    <?php
      $user_information = UtilityFunctions::UserInformations();
    ?>
    <div class="span4">
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/BasicInformation/view/<?= $user_information['school_id']; ?>" class="quick-action">
            <span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/school_register.svg" alt="school register"></span>
            <h3><strong>SCHOOL DETAIL</strong></h3>
        </a>
    </div>
    <div class="span4">
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/studentInformation/index" class="quick-action">
            <span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/student_register.svg"></span>
            <h3><strong>STUDENTS LIST</strong></h3>
        </a>
    </div>
    <div class="span4">
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/markObtained/subjectSchoolWiseGrade" class="quick-action">
            <span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/diagram.svg"></span>
            <h3><strong>SUBJECT/SCHOOL AVERAGE GRADE STUDENT'S</strong></h3>
        </a>
    </div>

    <div class="span4">
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/markObtained/ledger" class="quick-action">
            <span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/preview_ledger.svg"></span>
            <h3><strong>RAW LEDGER</strong></h3>
        </a>
    </div>
    <div class="span4">
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/result/studentList" class="quick-action">
            <span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/score.svg"></span>
            <h3><strong>PRINT INDIVIDUAL GRADE SHEET</strong></h3>
        </a>
    </div>
    <div class="span4">
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/result/ReportSummery" class="quick-action">
            <span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/report_summery.svg"></span>
            <h3><strong>SUMMERY REPORT</strong></h3>
        </a>
    </div>
    <div class="span4">
        <a href="<?php echo Yii::app()->request->baseUrl; ?>/users/changePassword" class="quick-action">
            <span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/authentication.svg"></span>
            <h3><strong>CHANGE PASSWORD</strong></h3>
        </a>
    </div>

</div>







