<div class="row-fluid quick-actions">
	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/BasicInformation/create" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/school_register.svg" alt="school register"></span>
			<h3><strong>SCHOOL REGISTER</strong></h3>
		</a>
	</div>
	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/StudentInformation/create" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/student_register.svg"></span>
			<h3><strong>STUDENT REGISTRATION</strong></h3>
		</a>
	</div>


	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/StudentInformation" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/view_students.svg"></span>
			<h3><strong>VIEW STUDENT'S</strong></h3>
		</a>
	</div>

	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/MarkObtained/ChooseSubject" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/insert_marks.svg"></span>
			<h3><strong>INSERT MARKS</strong></h3>
		</a>
	</div>
	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/studentAttendance/SelectClass" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/insert_attendance.svg"></span>
			<h3><strong>INSERT ATTENDANCE'S</strong></h3>
		</a>
	</div>

	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/MarkObtained/PreviewRawMarks" class="quick-action"
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/preview_ledger.svg"></span>
			<h3><strong>PREVIEW MARKS LEDGER</strong></h3>
		</a>
	</div>

	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/SubjectInformation" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/subject_info.svg"></span>
			<h3><strong>SUBJECT INFORMATION</strong></h3>
		</a>
	</div>
	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/studentAttendance/PassMark" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/pass_n_full_marks.svg"></span>
			<h3><strong>PASS && FULL MARKS</strong></h3>
		</a>
	</div>
	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/MarkObtained/ResultPublishDate" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/publish_result.svg"></span>
			<h3><strong>PUBLISH RESULT'S</strong></h3>
		</a>
	</div>

	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/Result/ResultPDF" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/pdf.svg"></span>
			<h3><strong>PDF RESULT'S</strong></h3>
		</a>
	</div>

	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/user" class="quick-action"
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/user_modules.svg"></span>
			<h3><strong>USER MODULES</strong></h3>
		</a>
	</div>

	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/rights/authItem/roles" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/role_mgmt.svg"></span>
			<h3><strong>ROLE'S MANAGEMENT</strong></h3>
		</a>
	</div>

</div>







