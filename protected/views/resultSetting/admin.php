<?php
$this->breadcrumbs=array(
	'Result Settings'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List ResultSetting','url'=>array('index')),
array('label'=>'Create ResultSetting','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('result-setting-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Manage Result Settings</h1>

<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
		&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'result-setting-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'academic_year',
		'school_id',
		'all_passed',
		'min_range_all_pass',
		'selected_fail',
		/*
		'selected_passed',
		'is_used',
		'created_by',
		'created_date',
		*/
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
