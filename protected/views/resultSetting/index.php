<?php
$this->breadcrumbs=array(
	'Result Settings',
);

$this->menu=array(
array('label'=>'Create ResultSetting','url'=>array('create')),
array('label'=>'Manage ResultSetting','url'=>array('admin')),
);
?>

<h1>Result Settings</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
