<?php
$this->breadcrumbs=array(
	'Result Settings'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List ResultSetting','url'=>array('index')),
array('label'=>'Manage ResultSetting','url'=>array('admin')),
);
?>

<h1>Create ResultSetting</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>