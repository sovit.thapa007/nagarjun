<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('academic_year')); ?>:</b>
	<?php echo CHtml::encode($data->academic_year); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('school_id')); ?>:</b>
	<?php echo CHtml::encode($data->school_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('all_passed')); ?>:</b>
	<?php echo CHtml::encode($data->all_passed); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('min_range_all_pass')); ?>:</b>
	<?php echo CHtml::encode($data->min_range_all_pass); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('selected_fail')); ?>:</b>
	<?php echo CHtml::encode($data->selected_fail); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('selected_passed')); ?>:</b>
	<?php echo CHtml::encode($data->selected_passed); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('is_used')); ?>:</b>
	<?php echo CHtml::encode($data->is_used); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	*/ ?>

</div>