<?php
$this->breadcrumbs=array(
	'Result Settings'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List ResultSetting','url'=>array('index')),
array('label'=>'Create ResultSetting','url'=>array('create')),
array('label'=>'View ResultSetting','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage ResultSetting','url'=>array('admin')),
);
?>

<h1>Update ResultSetting <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>