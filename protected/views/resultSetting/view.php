<?php
$this->breadcrumbs=array(
	'Result Settings'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List ResultSetting','url'=>array('index')),
array('label'=>'Create ResultSetting','url'=>array('create')),
array('label'=>'Update ResultSetting','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete ResultSetting','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage ResultSetting','url'=>array('admin')),
);
?>

<h1>View ResultSetting #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'academic_year',
		'school_id',
		'all_passed',
		'min_range_all_pass',
		'selected_fail',
		'selected_passed',
		'is_used',
		'created_by',
		'created_date',
),
)); ?>
