<?php
$this->breadcrumbs=array(
	'Class Informations'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List ClassInformation','url'=>array('index')),
array('label'=>'Manage ClassInformation','url'=>array('admin')),
);
?>

<h1>Create ClassInformation</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>