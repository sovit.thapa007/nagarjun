<?php
$this->breadcrumbs=array(
	'Class Informations'=>array('index'),
	$model->title,
);

$this->menu=array(
array('label'=>'List ClassInformation','url'=>array('index')),
array('label'=>'Create ClassInformation','url'=>array('create')),
array('label'=>'Update ClassInformation','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete ClassInformation','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage ClassInformation','url'=>array('admin')),
);
?>

<h1>View ClassInformation #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'title',
		'title_nepali',
		'numeric_class',
		'status',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
),
)); ?>
