<?php
$this->breadcrumbs=array(
	'Class Informations'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List ClassInformation','url'=>array('index')),
array('label'=>'Create ClassInformation','url'=>array('create')),
array('label'=>'View ClassInformation','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage ClassInformation','url'=>array('admin')),
);
?>

<h1>Update ClassInformation <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>