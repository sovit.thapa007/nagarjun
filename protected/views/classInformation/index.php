<?php
$this->breadcrumbs=array(
	'Class Informations',
);

$this->menu=array(
array('label'=>'Create ClassInformation','url'=>array('create')),
array('label'=>'Manage ClassInformation','url'=>array('admin')),
);
?>

<h1>Class Informations</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
