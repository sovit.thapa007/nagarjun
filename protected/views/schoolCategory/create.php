<?php
$this->breadcrumbs = array(
	'System Module' => array('dashboard/systemSetting'),
	'School Categories' => array('admin'),
	'Create',
);

$this->menu = array(
	array('label' => 'List SchoolCategory', 'url' => array('index')),
	array('label' => 'Manage SchoolCategory', 'url' => array('admin')),
);
?>
<?php echo $this->renderPartial('_form', array('model' => $model)); ?>

