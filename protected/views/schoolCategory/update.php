<?php
$this->breadcrumbs = array(
	'System Module' => array('dashboard/systemSetting'),
	'School Categories' => array('admin'),
	$model->title => array('view', 'id' => $model->id),
	'Update',
);

$this->menu = array(
	array('label' => 'List SchoolCategory', 'url' => array('index')),
	array('label' => 'Create SchoolCategory', 'url' => array('create')),
	array('label' => 'View SchoolCategory', 'url' => array('view', 'id' => $model->id)),
	array('label' => 'Manage SchoolCategory', 'url' => array('admin')),
);
?>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>