<?php
$this->breadcrumbs = array(
	'System Module' => array('dashboard/systemSetting'),
	'Create Category' => array('create'),
	'Manage',
);

$this->menu = array(
	array('label' => 'List SchoolCategory', 'url' => array('index')),
	array('label' => 'Create SchoolCategory', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('school-category-grid', {
data: $(this).serialize()
});
return false;
});
");
?>
<hr />
<h2 class="text-center">Manage School Categories</h2>


<?php
$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'school-category-grid',
	'type' => 'bordered',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		//'id',
		'title',
		'type_code',
		'remarks',
		'nepali_date',
		'created_date',
		/*
		  'status',
		 */
		array(
			'header' => 'Action',
			'class' => 'bootstrap.widgets.TbButtonColumn',
		),
	),
));
?>
