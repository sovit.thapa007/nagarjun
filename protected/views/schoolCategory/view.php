<?php
$this->breadcrumbs = array(
	'System Module' => array('dashboard/systemSetting'),
	'School Categories' => array('admin'),
	$model->title,
);

$this->menu = array(
	array('label' => 'List SchoolCategory', 'url' => array('index')),
	array('label' => 'Create SchoolCategory', 'url' => array('create')),
	array('label' => 'Update SchoolCategory', 'url' => array('update', 'id' => $model->id)),
	array('label' => 'Delete SchoolCategory', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
	array('label' => 'Manage SchoolCategory', 'url' => array('admin')),
);
?>
<hr />
<h2 class="text-center">View School Category #<?php echo $model->id; ?></h2>

<?php
$this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'type' => 'bordered',
	'attributes' => array(
		'id',
		'title',
		'type_code',
		'remarks',
		'nepali_date',
		'created_date',
		[
			'name' => 'status',
			'value' => $model->status == 1 ? "Active" : "Deactive",
		],
	),
));
?>
