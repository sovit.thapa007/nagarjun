<style type="text/css">
	#dynamicRow_0 .control-group{
		margin-left: 0px !important;
	}

</style>

<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'school-category-form',
	'type' => 'horizontal',
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
	),
	'enableClientValidation' => true,
	'enableAjaxValidation' => false,
	));
?>

<div class="row-fluid">
	<div class="span12">
		
			<?php echo $form->errorSummary($model); ?>
			<div class="box-title"><p class="help-block">Fields with <span class="required">*</span> are required.</p></div>
			<div class="box-content nopadding">
				<div class="control-group">
					<h3 class="text-center" >School Category</h3>
					<hr />

				</div>
				<div class="row-fluid">

					<?php echo $form->textFieldRow($model, 'title', array('class' => 'span5', 'maxlength' => 250)); ?>

					<?php echo $form->textFieldRow($model, 'type_code', array('class' => 'span5', 'maxlength' => 20)); ?>

					<?php echo $form->textFieldRow($model, 'nepali_date', array('class' => 'span5 nepali-calendar', 'maxlength' => 15, 'id' => 'nepaliDate2')); ?>

					<?php echo $form->textAreaRow($model, 'remarks', array('rows' => 6, 'cols' => 30, 'class' => 'span5')); ?>

					<?php //echo $form->textFieldRow($model,'nepali_date',array('class'=>'span5','maxlength'=>20)); ?>

					<?php //echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?>

					<?php
					if (!$model->isNewRecord)
						echo $form->dropDownList($model, 'status', array('1' => 'Active', '0' => 'Deactive'), array('class' => 'span5'));
					?>

					<div class="form-actions">
						<?php
						$this->widget('bootstrap.widgets.TbButton', array(
							'buttonType' => 'submit',
							'type' => 'primary',
							'label' => $model->isNewRecord ? 'Create' : 'Save',
						));
						?>
					</div>
				</div>
			</div>
		
	</div>
</div>

<?php $this->endWidget(); ?>