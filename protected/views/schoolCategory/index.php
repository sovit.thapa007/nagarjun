<?php
$this->breadcrumbs = array(
	'School Categories',
);

$this->menu = array(
	array('label' => 'Create SchoolCategory', 'url' => array('create')),
	array('label' => 'Manage SchoolCategory', 'url' => array('admin')),
);
?>

<h1>School Categories</h1>

<?php
$this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider' => $dataProvider,
	'itemView' => '_view',
));
?>
