<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'pass-mark-form',
	'enableAjaxValidation'=>false,
	'type' => 'horizontal',
	'enableClientValidation' => true,
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
	),
	'focus' => array($model, 'title'),
)); ?>

<p class="text-center alert alert-info help-block">Fields with <span class="required">*</span> are required.</p>
<br />
	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->dropDownListRow($model,'subject_id',CHtml::listData(SubjectInformation::model()->findAll(),'id','title'),array('class'=>'span12', 'prompt'=>'Select Subject')); ?>

			<?php echo $form->textFieldRow($model,'theory_full_mark',array('class'=>'span12')); ?>

			<?php echo $form->textFieldRow($model,'theory_pass_mark',array('class'=>'span12')); ?>

			<?php echo $form->textFieldRow($model,'remarks',array('class'=>'span12','maxlength'=>200)); ?>

		</div>
		<div class="span6">
			<?php echo $form->dropDownListRow($model,'is_practical', array(1=>'YES', 0=>'NO'),array('class'=>'span12')); ?>
			
			<?php echo $form->textFieldRow($model,'practical_full_mark',array('class'=>'span12')); ?>

			<?php echo $form->textFieldRow($model,'practical_pass_mark',array('class'=>'span12')); ?>

			<?php if(!$model->isNewRecord){
					 echo $form->dropDownListRow($model,'status', array(1=>'Active', 0=>'De-Active'),array('class'=>'span12'));
					 } ?>
		</div>
	</div>

	<div class="form-actions text-center">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create Pass && Full Marks' : 'Save Pass && Full Marks',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
