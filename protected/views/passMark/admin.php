<?php
$this->breadcrumbs=array(
	'Create Pass && Full Marks'=>array('create'),
	'Manage',
);

$this->menu=array(
array('label'=>'List PassMark','url'=>array('index')),
array('label'=>'Create PassMark','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('pass-mark-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2 class="text-center">Manage Pass && Full Marks</h2>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'pass-mark-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'type'=>'bordered stripped-bordered',
'columns'=>array(
		//'id',
		[
			'name'=>'subject_id',
			'value'=>'$data->subject_information ? $data->subject_information->title :  "null"',
		],
		[
			'name'=>'is_practical',
			'value'=>'$data->is_practical == 1 ? "Yes" : "No"',
			'filter'=>[1=>' Yes ', 0=>' No ' ],
		],
		'theory_full_mark',
		'theory_pass_mark',
		'practical_full_mark',
		'practical_pass_mark',
		[
			'name'=>'status',
			'value'=>'$data->status == 1 ? "Active" : "No-Active"',
		],
		/*
		'is_practical',
		'status',
		'remarks',
		*/
array(
	'header'=>'Action',
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
