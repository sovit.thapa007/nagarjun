<?php
$this->breadcrumbs=array(
	'Pass Marks',
);

$this->menu=array(
array('label'=>'Create PassMark','url'=>array('create')),
array('label'=>'Manage PassMark','url'=>array('admin')),
);
?>

<h1>Pass Marks</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
