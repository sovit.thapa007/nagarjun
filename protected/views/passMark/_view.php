<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subject_id')); ?>:</b>
	<?php echo CHtml::encode($data->subject_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('theory_pass_mark')); ?>:</b>
	<?php echo CHtml::encode($data->theory_pass_mark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('practical_pass_mark')); ?>:</b>
	<?php echo CHtml::encode($data->practical_pass_mark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('theory_full_mark')); ?>:</b>
	<?php echo CHtml::encode($data->theory_full_mark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('practical_full_mark')); ?>:</b>
	<?php echo CHtml::encode($data->practical_full_mark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_practical')); ?>:</b>
	<?php echo CHtml::encode($data->is_practical); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('remarks')); ?>:</b>
	<?php echo CHtml::encode($data->remarks); ?>
	<br />

	*/ ?>

</div>