<?php
$this->breadcrumbs=array(
	'List Pass && Full Marks'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List PassMark','url'=>array('index')),
array('label'=>'Create PassMark','url'=>array('create')),
array('label'=>'View PassMark','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage PassMark','url'=>array('admin')),
);
?>

<h1>Update PassMark <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>