<?php
$this->breadcrumbs=array(
	'List Pass && Full Marks'=>array('index'),
	'Create Pass && Full Marks'=>array('create'),
	$model->id,
);

$this->menu=array(
array('label'=>'List PassMark','url'=>array('index')),
array('label'=>'Create PassMark','url'=>array('create')),
array('label'=>'Update PassMark','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete PassMark','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage PassMark','url'=>array('admin')),
);
?>

<h2 class="text-center">View Pass && Full Mark # <?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'type'=>'bordered stripped-bordered',
'attributes'=>array(
		[
			'name'=>'subject_id',
			'value'=>$model->subject_information ? $model->subject_information->title :  "null",
		],
		[
			'name'=>'is_practical',
			'value'=>$model->is_practical == 1 ? "Yes" : "No",
		],
		'theory_full_mark',
		'theory_pass_mark',
		'practical_full_mark',
		'practical_pass_mark',
		[
			'name'=>'status',
			'value'=>$model->status == 1 ? "Active" : "No-Active",
		],
		'remarks',
),
)); ?>
