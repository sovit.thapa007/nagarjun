<?php
$this->breadcrumbs=array(
	'List Pass && Full Marks'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List PassMark','url'=>array('index')),
array('label'=>'Manage PassMark','url'=>array('admin')),
);
?>

<h1>Create PassMark</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>