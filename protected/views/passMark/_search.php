<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'subject_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'theory_pass_mark',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'practical_pass_mark',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'theory_full_mark',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'practical_full_mark',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'is_practical',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'status',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'remarks',array('class'=>'span5','maxlength'=>200)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
</div>

<?php $this->endWidget(); ?>
