<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'subject-information-form',
	'enableAjaxValidation'=>false,
	'type' => 'horizontal',
	'enableClientValidation' => true,
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
	),
	'focus' => array($model, 'title'),
)); ?>
<h2 class="text-center"><strong>Subject Information</strong></h2>
	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->textFieldRow($model,'title',array('class'=>'span12','maxlength'=>250)); ?>

			<?php echo $form->textFieldRow($model,'title_nepali',array('class'=>'span12 nepaliUnicode','maxlength'=>250)); ?>

			<?php echo $form->textFieldRow($model,'code',array('class'=>'span12','maxlength'=>10)); ?>

			<?php echo $form->textFieldRow($model,'credits',array('class'=>'span12')); ?>
			<?php 
			if(UtilityFunctions::ShowSchool()){
				echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => "title")),'id','title'),array('class'=>'', 'prompt'=>'Select School', 'selected'=>1),array('class'=>'span12')); 
			}
			?>
		</div>
		<div class="span6">

			<?php echo $form->textFieldRow($model, 'subject_order', array('class'=>'span12'))?>

			<?php echo $form->textFieldRow($model,'subject_hrs',array('class'=>'span12')); ?>


			<?php echo $form->dropDownListRow($model,'subject_nature', array(1=>'Comulsary', 3=>'Extra Subject I'),array('class'=>'span12')); ?>

			<?php echo $form->dropDownListRow($model,'default_optional', array(0=>'NO',1=>'YES'),array('class'=>'span12')); ?>
			<?php echo $form->dropDownListRow($model,'subject_type', array('general'=>'General','Custom'=>'custom'),array('class'=>'span12')); ?>


		</div>
	</div>

	<?php echo $form->textAreaRow($model,'descriptions',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>

	<?php
	if(!$model->isNewRecord)
		echo $form->dropDownListRow($model,'status',array(1=>'Active', 0=>'De-Active'),array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?>

	<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
