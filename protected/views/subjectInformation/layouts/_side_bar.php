
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->

      <section>
          
        <!-- Sidebar user panel -->
       <!-- <div class="user-panel">
          <a class="navbar-brand" href="#"><p style="margin-left: 12px; font-size: 22px; margin-top: 10px;">SCHOOL ADMIN</p></a>
        </div>-->
        <div class="side-user">
       
              <!-- Logo -->
          <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
              <!-- logo for regular state and mobile devices -->
              <?php
              if(UtilityFunctions::SuperUser() || in_array($user_information['role'],['administrativeofficer','dataentry'])){
                ?>
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo/ministry_education.jpg">
                <?php
              }else{
                $school_information = BasicInformation::model()->findByPk($user_information['school_id']);
                $logo = !empty($school_information) ? $school_information->logo : 'null';
                ?>
                <img src="<?= Yii::app()->request->baseUrl; ?>/images/logo/<?= $logo; ?>">
                <?php
              }
              ?>
          </a>
          </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" >
          <a href="" class="site-ttl"><h4 class="text-center"><strong><?= UtilityFunctions::AdminSection(); ?></strong></h4></a>
            <div class="menu">
              <div class="accordion-group">
                <div class="accordion-heading green">
                    <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/dashboard">
                      <i class="fa fa-dashboard"></i> <span>DASHBOARD</span> <i class="fa fa-angle-right pull-right"></i>
                    </a>
                </div>
              </div>
              <div class="accordion-group">
                  <div class="accordion-heading green">
                      <a class="accordion-toggle" data-toggle="collapse" href="#schoolManagement"><i class="fa fa-university"></i> <span>SCHOOL'S MANAGEMENT</span> <i class="fa fa-angle-right pull-right"></i></a>
                  </div>
                  <div class="accordion-body collapse" id="schoolManagement" style="height: 0px;">
                      <div class="accordion-inner">
                        <?php
                        if(Yii::app()->user->checkAccess('BasicInformation.create')){
                        ?>
                          <div class="accordion-group">
                            <div class="accordion-heading green">
                                <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/BasicInformation/create">
                                  <i class="fa fa-plus"></i> <span>ADD NEW SCHOOL</span> <i class="fa fa-angle-right pull-right"></i>
                                </a>
                            </div>
                          </div>
                          <?php
                        }
                        if(Yii::app()->user->checkAccess('BasicInformation.admin')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/BasicInformation/admin">
                                    <i class="fa fa-list"></i> <span>SCHOOL'S LIST</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                        <?php
                        }
                        if(Yii::app()->user->checkAccess('BasicInformation.view') && in_array($user_information['role'],['dataentry','headteacher'])){
                        ?>
                          <div class="accordion-group">
                            <div class="accordion-heading green">
                                <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/BasicInformation/view/<?= $user_information['school_id']; ?>">
                                  <i class="fa fa-eye"></i> <span>SCHOOL DETAILS</span> <i class="fa fa-angle-right pull-right"></i>
                                </a>
                            </div>
                          </div>
                        <?php
                        }
                        if(Yii::app()->user->checkAccess('BasicInformation.addResourceCenter')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/BasicInformation/addResourceCenter">
                                    <i class="fa fa-list"></i> <span>ADD RESOURCE CENTER</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                        <?php
                        }
                        if(Yii::app()->user->checkAccess('BasicInformation.assignSchoolResourceCenter')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/BasicInformation/resourceCenterList">
                                    <i class="fa fa-list"></i> <span>RESOURCE CENTER LIST</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                        <?php
                        }
                        if(Yii::app()->user->checkAccess('schoolBoardInformation.admin')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/schoolBoardInformation/admin">
                                    <i class="fa fa-list"></i> <span>STAFF/BOARDMEMBER</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                        <?php
                        }
                        ?>
                      </div>
                  </div>
              </div>
              <div class="accordion-group">
                  <div class="accordion-heading green">
                      <a class="accordion-toggle" data-toggle="collapse" href="#student_registration"><i class="fa fa-graduation-cap"></i> <span>STUDENT REGISTRATION</span> <i class="fa fa-angle-right pull-right"></i></a>
                  </div>
                  <div class="accordion-body collapse" id="student_registration" style="height: 0px;">
                      <div class="accordion-inner">
                        <?php
                        if(Yii::app()->user->checkAccess('studentInformation.create')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/studentInformation/create">
                                    <i class="fa fa-registered"></i> <span>NEW REGISTRATION</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                        <?php
                        }
                        if(Yii::app()->user->checkAccess('studentInformation.index')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/studentInformation/index">
                                    <i class="fa fa-list"></i> <span>STUDENT'S LIST</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                        <?php
                        }
                        if(Yii::app()->user->checkAccess('studentInformation.allRegisteredStudent')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/studentInformation/allRegisteredStudent">
                                    <i class="fa fa-list"></i> <span>TOTAL STUDENT'S LIST</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>

                        <?php
                        }
                        if(Yii::app()->user->checkAccess('studentInformation.uploadStudentExcel')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/studentInformation/uploadStudentExcel">
                                    <i class="fa fa-bar-chart"></i> <span>UPLOAD REGISTER STUDENT EXCEL</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                        <?php
                        }
                        ?>


                      </div>
                  </div>
              </div>
              <div class="accordion-group">
                  <div class="accordion-heading green">
                      <a class="accordion-toggle" data-toggle="collapse" href="#exam"><i class="fa fa-pencil-square-o"></i> <span>MARKS INFORMATION</span> <i class="fa fa-angle-right pull-right"></i></a>
                  </div>

                  <div class="accordion-body collapse" id="exam" style="height: 0px;">
                      <div class="accordion-inner">

                        <?php
                        if(Yii::app()->user->checkAccess('markObtained.chooseSubject')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/markObtained/chooseSubject">
                                    <i class="fa fa-list"></i> <span>INSERT MARKS</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                        <?php
                        }
                        if(Yii::app()->user->checkAccess('markObtained.uploadFile')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/markObtained/uploadFile">
                                    <i class="fa fa-list"></i> <span>UPLOAD EXCEL MARK</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>

                        <?php
                        }
                        if(Yii::app()->user->checkAccess('markObtained.downloadStudentLedger')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/markObtained/downloadStudentLedger">
                                    <i class="fa fa-list"></i> <span>DOWNLOAD EXCEL LEDGER MARK</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                        <?php
                        }
                        if(Yii::app()->user->checkAccess('markObtained.uploadStudentLedger')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/markObtained/uploadStudentLedger">
                                    <i class="fa fa-list"></i> <span>UPLOAD EXCEL LEDGER MARK</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                        <?php
                        }
                        if(Yii::app()->user->checkAccess('markObtained.previewRawMarks')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/markObtained/previewRawMarks">
                                    <i class="fa fa-list"></i> <span>RAW MARKS LEDGERS</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                        <?php
                        }
                        if(Yii::app()->user->checkAccess('markObtained.previewRawMarks')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/markObtained/previewRawMarks">
                                    <i class="fa fa-list"></i> <span>RAW MARKS LEDGERS</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                        <?php
                        }
                        if(Yii::app()->user->checkAccess('basicInformation.schoolMarksDetail')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/BasicInformation/SchoolMarksDetail">
                                    <i class="fa fa-list"></i> <span>SCHOOL MARKS DETAILS (DOWNLOAD/AUDIT/INSERT)</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                        <?php
                        }
                        ?>
                      </div>
                  </div>
              </div>
              <div class="accordion-group">
                  <div class="accordion-heading green">
                      <a class="accordion-toggle" data-toggle="collapse" href="#result"><i class="fa fa-pie-chart"></i> <span>RESULT SECTION</span> <i class="fa fa-angle-right pull-right"></i></a>
                  </div>

                  <div class="accordion-body collapse" id="result" style="height: 0px;">
                      <div class="accordion-inner">
                        <?php
                        if(Yii::app()->user->checkAccess('markObtained.resultPublishDate')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/markObtained/resultPublishDate">
                                    <i class="fa fa-calendar"></i> <span>PUBLISH RESULT'S</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                        <?php
                        }
                        if(Yii::app()->user->checkAccess('result.resultPDF')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/result/resultPDF">
                                    <i class="fa fa-file-pdf-o"></i> <span>PRINT GRADE/MARK SHEET</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                        <?php
                        }
                        if(Yii::app()->user->checkAccess('markObtained.ledger')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/markObtained/ledger">
                                    <i class="fa fa-bar-chart"></i> <span>FINAL LEDGERS</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                        <?php
                        }
                        if(Yii::app()->user->checkAccess('result.studentList')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/result/studentList">
                                    <i class="fa fa-bar-chart"></i> <span>PRINT INDIVIDUAL GRADE SHEET</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                          <?php
                          }
                          ?>
                      </div>
                  </div>
              </div>
              <div class="accordion-group">
                  <div class="accordion-heading green">
                      <a class="accordion-toggle" data-toggle="collapse" href="#report"><i class="fa fa-bar-chart"></i> <span>REPORT</span> <i class="fa fa-angle-right pull-right"></i></a>
                  </div>
                  <div class="accordion-body collapse" id="report" style="height: 0px;">
                      <div class="accordion-inner">

                      <div class="accordion-group">
                          <div class="accordion-heading green">
                              <a class="accordion-toggle" data-toggle="collapse" href="#school_report"><i class="fa fa-university"></i> <span>SCHOOL REPORT</span> <i class="fa fa-angle-right pull-right"></i></a>
                          </div>
                          <div class="accordion-body collapse" id="school_report" style="height: 0px;">
                          <div class="accordion-inner">
                          <?php
                          if(Yii::app()->user->checkAccess('BasicInformation.schoolReport')){
                          ?>
                            <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/BasicInformation/schoolReport">
                                    <i class="fa fa-bar-chart"></i> <span>SCHOOL DETAIL REPORT</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                            </div>
                          <?php
                          }
                        if(Yii::app()->user->checkAccess('studentExamRegistration.SchoolWiseSymbolNo')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/studentExamRegistration/SchoolWiseSymbolNo">
                                    <i class="fa fa-list"></i> <span>SCHOOL WISE SYMBOL NUMBER DETAIL</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                        <?php
                        }
                        if(Yii::app()->user->checkAccess('studentExamRegistration.SchoolWiseStudent')){
                          ?>
                            <div class="accordion-group">
                                <div class="accordion-heading green">
                                    <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/studentExamRegistration/SchoolWiseStudent" target="_blank">
                                      <i class="fa fa-list"></i> <span>GENDER WISE SCHOOL STUDENT NUMBER</span> <i class="fa fa-angle-right pull-right"></i>
                                    </a>
                                </div>
                            </div>
                          <?php
                          }
                          if(Yii::app()->user->checkAccess('BasicInformation.resourceSchoolStudent')){
                          ?>
                            <div class="accordion-group">
                                <div class="accordion-heading green">
                                    <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/BasicInformation/resourceSchoolStudent" target="_blank">
                                      <i class="fa fa-list"></i> <span>SCHOOL/STUDENT NUMBER BY SCHOOL TYPE</span> <i class="fa fa-angle-right pull-right"></i>
                                    </a>
                                </div>
                            </div>
                          <?php
                          }
                          ?>
                          </div>
                          </div>
                      </div>
                      <div class="accordion-group">
                          <div class="accordion-heading green">
                              <a class="accordion-toggle" data-toggle="collapse" href="#student_report"><i class="fa fa-graduation-cap"></i> <span>STUDENT REPORT</span> <i class="fa fa-angle-right pull-right"></i></a>
                          </div>
                          <div class="accordion-body collapse" id="student_report" style="height: 0px;">
                          <div class="accordion-inner">
                          <?php
                          if(Yii::app()->user->checkAccess('studentInformation.studentRegistrationReport')){
                          ?>
                            <div class="accordion-group">
                                <div class="accordion-heading green">
                                    <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/studentInformation/studentRegistrationReport">
                                      <i class="fa fa-list"></i> <span>STUDENTS REGISTRATION REPORT</span> <i class="fa fa-angle-right pull-right"></i>
                                    </a>
                                </div>
                            </div>

                          <?php
                          }
                          if(Yii::app()->user->checkAccess('studentInformation.belowAgeStudents')){
                          ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/studentInformation/belowAgeStudents">
                                    <i class="fa fa-list"></i> <span>BELOW AGE STUDENT'S LIST</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                          <?php
                          }

                          ?>
                          </div>
                          </div>
                      </div>


                      <div class="accordion-group">
                          <div class="accordion-heading green">
                              <a class="accordion-toggle" data-toggle="collapse" href="#mark_result_report"><i class="fa fa-pencil-square-o"></i> <span>MARKS/RESULT REPORT</span> <i class="fa fa-angle-right pull-right"></i></a>
                          </div>
                          <div class="accordion-body collapse" id="mark_result_report" style="height: 0px;">
                          <div class="accordion-inner">
                          <?php

                            if(Yii::app()->user->checkAccess('markObtained.auditReport')){
                            ?>
                              <div class="accordion-group">
                                  <div class="accordion-heading green">
                                      <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/markObtained/auditReport">
                                        <i class="fa fa-bar-chart"></i> <span>STUDENT AUDIT REPORT</span> <i class="fa fa-angle-right pull-right"></i>
                                      </a>
                                  </div>
                              </div>
                            <?php
                              }
                            if(Yii::app()->user->checkAccess('markObtained.retakeReport')){
                            ?>
                              <div class="accordion-group">
                                  <div class="accordion-heading green">
                                      <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/markObtained/retakeReport">
                                        <i class="fa fa-repeat"></i> <span>STUDENTS EXAM RETAKE</span> <i class="fa fa-angle-right pull-right"></i>
                                      </a>
                                  </div>
                              </div>
                              <?php
                              }
                            if(Yii::app()->user->checkAccess('result.gradeWiseReport')){
                            ?>
                            <div class="accordion-group">
                                <div class="accordion-heading green">
                                    <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/result/gradeWiseReport">
                                      <i class="fa fa-bar-chart"></i> <span>GRADE WISE REPORT</span> <i class="fa fa-angle-right pull-right"></i>
                                    </a>
                                </div>
                            </div>
                            <?php
                            }
                            if(Yii::app()->user->checkAccess('result.schoolWiseGradeStudent')){
                            ?>
                            <div class="accordion-group">
                                <div class="accordion-heading green">
                                    <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/result/schoolWiseGradeStudent">
                                      <i class="fa fa-bar-chart"></i> <span>GRADE WISE TOTAL STUDENT</span> <i class="fa fa-angle-right pull-right"></i>
                                    </a>
                                </div>
                            </div>

                            <?php
                            }
                        if(Yii::app()->user->checkAccess('markObtained.subjectSchoolWiseGrade')){
                        ?>

                        <div class="accordion-group">
                            <div class="accordion-heading green">
                                <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/markObtained/subjectSchoolWiseGrade">
                                  <i class="fa fa-bar-chart"></i> <span>SUBJECT/SCHOOL AVERAGE GRADE STUDENT'S</span> <i class="fa fa-angle-right pull-right"></i>
                                </a>
                            </div>
                        </div>
                        <?php
                        }
                        if(Yii::app()->user->checkAccess('markObtained.SubjectWiseGradeStudentNumber')){
                        ?>

                        <div class="accordion-group">
                            <div class="accordion-heading green">
                                <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/markObtained/SubjectWiseGradeStudentNumber">
                                  <i class="fa fa-bar-chart"></i> <span>SCHOOL/SUBJECT WISE GRADE STUDENTS NUMBER</span> <i class="fa fa-angle-right pull-right"></i>
                                </a>
                            </div>
                        </div>
                        <?php
                        }
                        if(Yii::app()->user->checkAccess('result.generateAverageSchoolReport')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/result/generateAverageSchoolReport">
                                    <i class="fa fa-bar-chart"></i> <span>SCHOOL AVERAGE</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                        <?php
                        }
                        if(Yii::app()->user->checkAccess('result.StudentGradeReport')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/result/StudentGradeReport">
                                    <i class="fa fa-file-pdf-o"></i> <span>STUDENT REPORT</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                        <?php
                        }
                        if(Yii::app()->user->checkAccess('markObtained.ledger')){
                        ?>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/markObtained/ledger">
                                    <i class="fa fa-bar-chart"></i> <span>FINAL LEDGERS</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                        <?php
                        }
                        ?>

                          ?>
                          </div>
                          </div>
                      </div>


                      </div>
                  </div>
              </div>
              <?php
              if(UtilityFunctions::SuperUser()){
                ?>
              <div class="accordion-group">
                  <div class="accordion-heading green">
                      <a class="accordion-toggle" data-toggle="collapse" href="#setting"><i class="fa fa-gears"></i> <span>SETTING</span> <i class="fa fa-angle-right pull-right"></i></a>
                  </div>

                  <div class="accordion-body collapse" id="setting" style="height: 0px;">
                      <div class="accordion-inner">


                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/academicYear/admin">
                                    <i class="fa fa-list"></i> <span>ACADEMIC YEAR</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>

                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/basicInformation/AddResourceCenter">
                                    <i class="fa fa-list"></i> <span>ADD RESOURCE CENTER</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>

                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/basicInformation/resourceCenterList">
                                    <i class="fa fa-list"></i> <span>RESOURCE CENTERS LIST</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/subjectInformation">
                                    <i class="fa fa-list"></i> <span>SUBJECTS LIST</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>
                          <div class="accordion-group">
                              <div class="accordion-heading green">
                                  <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/PassMark">
                                    <i class="fa fa-list"></i> <span>SUBJECT'S FM</span> <i class="fa fa-angle-right pull-right"></i>
                                  </a>
                              </div>
                          </div>

                          <div class="accordion-group">
                            <div class="accordion-heading green">
                                <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/users">
                                  <i class="fa fa-users"></i> <span>USER MANAGEMENT</span> <i class="fa fa-angle-right pull-right"></i>
                                </a>
                            </div>
                          </div>

                          <div class="accordion-group">
                            <div class="accordion-heading green">
                                <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/rights/authItem/roles">
                                  <i class="fa fa-plus"></i> <span>ROLE MANAGEMENT</span> <i class="fa fa-angle-right pull-right"></i>
                                </a>
                            </div>
                          </div>
                      </div>
                  </div>
              </div>
              <?php
                }
              ?>

              <div class="accordion-group">
                <div class="accordion-heading green">
                    <a class="accordion-toggle" href="<?php echo Yii::app()->request->baseUrl; ?>/users/changePassword">
                      <i class="fa fa-dashboard"></i> <span>CHANGE PASSWORD</span> <i class="fa fa-angle-right pull-right"></i>
                    </a>
                </div>
              </div>
            </div>

          </ul>


        </section>
        <!-- /.sidebar -->
      </aside>