
  <div class="clearfix"></div>
  <div class="footer">
    <h2 style="margin-right:50px;">Copyright @ <?= date('Y'); ?></h2>
  </div>

  <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/js/jquery.js" ></script>

  <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/js/jquery.nanoscroller.js" ></script>
  <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/js/general.js" ></script>
  <script src="<?= Yii::app()->request->baseUrl; ?>/js/jquery-ui.js" ></script>
  <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/js/jquery.nestable.js" ></script>
  <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/bootstrap/js/bootstrap-switch.min.js" ></script>
  <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/bootstrap/js/bootstrap-datetimepicker.min.js" ></script>
  <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript">
      $(document).ready(function(){
        //initialize the javascript
        App.init();
        App.dashBoard();

          introJs().setOption('showBullets', false).start();

      });
    </script>
  <script src="<?= Yii::app()->request->baseUrl; ?>/bootstrap/js/bootstrap.min.js" ></script>