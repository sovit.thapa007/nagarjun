<!DOCTYPE html>
<html>
<head>
  <meta charset="utf8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="Medium Health Care Pvt Ltd">
  <title>School Control</title>
  
  <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/style_school.css" rel="stylesheet">
   <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/AdminLTE.css" rel="stylesheet">
  <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/skins/_all-skins.css" rel="stylesheet">

  <?php


    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
  ?>
  
</head>

<body class="skin-blue sidebar-mini">
    <div class="row-fluid alert alert-danger text-center" style="font-weight: bold;">
      <?php echo $content; ?>
    </div>
  </body>


  <!-- load jquery files -->
  <?php
  //$cs->registerScriptFile($baseUrl.'/js/jquery.min.js');
  //$cs->registerScriptFile($baseUrl.'/js/bootstrap.min.js');
  $cs->registerScriptFile($baseUrl . '/js/jquery.validate.min.js');
  $cs->registerScriptFile($baseUrl . '/js/jquery.maskedinput.js');
  $cs->registerScriptFile($baseUrl . '/js/nepali.datepicker.min.js');
  //$cs->registerScriptFile($baseUrl.'/js/custom.general.js');
  //$cs->registerScriptFile($baseUrl.'/js/custom.forms.js');
  ?>
  <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.min.js"></script>
    
  </html>
