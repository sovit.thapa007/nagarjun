<!DOCTYPE html>
<html>
<head>
  <meta charset="utf8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="Medium Health Care Pvt Ltd">
  <title><?= Yii::app()->params['municipality']; ?></title>

  <?php


    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
  ?>
</head>
<body class="skin-blue sidebar-mini">
  <div class="wrapper">

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->

          <!-- Main content -->
          <section class="content">
              <?php echo $content; ?>
          </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
        <footer class="main-footer">
          <div class="pull-right hidden-xs">
            
          </div>
          <strong>Copyright &copy; 2018 <?= Yii::app()->name; ?>

          <strong style="float: right;">Powered By  <a href="#" style="color: rgb(22, 160, 133); font-weight: normal;"><span style="font-weight: 500; font-family: 'Architects Daughter'; color: rgb(22, 160, 133);"></span></a>:SAMYAM GROUP</strong> 
        </footer>
      <!-- Add the sidebar's background. This div must be placed
      immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

   
  </body>

  </html>
