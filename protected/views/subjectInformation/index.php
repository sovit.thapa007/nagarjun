<?php
$this->breadcrumbs=array(
	'Subject Informations',
);

$this->menu=array(
array('label'=>'Create SubjectInformation','url'=>array('create')),
array('label'=>'Manage SubjectInformation','url'=>array('admin')),
);
?>

<h1>Subject Informations</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
