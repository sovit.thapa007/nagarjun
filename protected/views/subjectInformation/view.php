<?php
$this->breadcrumbs=array(
	'Subject Informations'=>array('index'),
	$model->title,
);

?>
<div class="form-horizontal well">
<h2 class="text-center">View Subject Information # <?= ucwords($model->title); ?></h2>

	<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'type'=>'stripped bordered',
	'attributes'=>array(
			'title',
			'title_nepali',
			'code',
			'subject_order',
			[
				'name'=>'subject_nature',
				'value' => $model->subject_nature == 1 ? 'Compulsary' : 'Optional',
			],
			[
				'name'=>'default_optional',
				'value' => $model->default_optional == 1 ? 'YES' : 'NO',
			],
			[
				'name'=>'school_id',
				'value' => $model->school ? strtoupper($model->school->title) : '',
			],
			[
				'name'=>'status',
				'value' => $model->status == 1 ? 'Active' : 'Disable',
			],
			[
				'name'=>'created_by',
				'value'=>$model->user_sec ? $model->user_sec->username : 'null',
			],
			'created_date',
			'descriptions',
	),
	)); ?>
</div>