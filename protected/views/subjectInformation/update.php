<?php
$this->breadcrumbs=array(
	'Subject Informations'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List SubjectInformation','url'=>array('index')),
array('label'=>'Create SubjectInformation','url'=>array('create')),
array('label'=>'View SubjectInformation','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage SubjectInformation','url'=>array('admin')),
);
?>

<h4 class="text-center"><strong>Update Subject Information <?php echo $model->title; ?> </strong></h2>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>