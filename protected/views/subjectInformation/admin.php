<?php
$this->breadcrumbs=array(
	'Add Subject'=>array('create'),
	'Manage',
);

?>
<div class="form-horizontal well">
<h2><strong>Manage Subject Informations</strong></h2>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'subject-information-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
		'title',
		'title_nepali',
		'code',
		//'credits',
		'subject_order',
		'subject_type',
		//'descriptions',
		[
			'name'=>'subject_nature',
			'value'=>'$data->subject_nature ==1 ? "Compulsary" : "Optional" ',
		],
		/*
		'subject_nature',
		'status',
		'created_by',
		'created_date',
		*/
array(
	'header'=>'Action',
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
</div>