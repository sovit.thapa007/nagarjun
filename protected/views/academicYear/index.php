<?php
$this->breadcrumbs=array(
	'Academic Years',
);

$this->menu=array(
array('label'=>'Create AcademicYear','url'=>array('create')),
array('label'=>'Manage AcademicYear','url'=>array('admin')),
);
?>

<h4 class="text-center">Academic Years</h4><hr />

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
