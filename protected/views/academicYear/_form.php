<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'academic-year-form',
	'enableAjaxValidation'=>false,
	'type' => 'horizontal',
	'enableClientValidation' => true,
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
	),
)); ?>

<h2><STRONG><?= $model->isNewRecord ?  "ASSINGE NEW ACADEMIC YEAR" : "CHANGE ACADEMIC YEAR "; ?></STRONG></h2>
<p class="help-block">Fields with <span class="required">*</span> are required.</p>
<br />
			<?php echo $form->textFieldRow($model,'year',array('class'=>'span12')); ?>
	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->textFieldRow($model,'starting_nepali_date',array('class'=>'span12','maxlength'=>10)); ?>

			<?php echo $form->textFieldRow($model,'end_nepali_date',array('class'=>'span12','maxlength'=>10)); ?>
		</div>
		<div class="span6">


			<?php echo $form->datepickerRow($model,'starting_english_date',array('options'=>array(
			'format'=>'yyyy-mm-dd'),'htmlOptions'=>array('class'=>'span12')),array('prepend'=>'<i class="icon-calendar"></i>')); ?>
			<?php echo $form->datepickerRow($model,'end_english_date',array('options'=>array(
				'format'=>'yyyy-mm-dd',
			),'htmlOptions'=>array('class'=>'span12')),array('prepend'=>'<i class="icon-calendar"></i>')); ?>

			<?php
				if(!$model->isNewRecord) 
				echo $form->dropDownListRow($model,'status',array("active"=>"active","in-active"=>"in-active",),array('class'=>'span12')); ?>

			<?php //echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

			<?php //echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?>
		</div>
	</div>

	<div class="form-actions text-center">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'SUBMIT',
		)); ?>
</div>

<?php $this->endWidget(); ?>
