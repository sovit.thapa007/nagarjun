<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('year')); ?>:</b>
	<?php echo CHtml::encode($data->year); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('starting_nepali_date')); ?>:</b>
	<?php echo CHtml::encode($data->starting_nepali_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('starting_english_date')); ?>:</b>
	<?php echo CHtml::encode($data->starting_english_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('end_nepali_date')); ?>:</b>
	<?php echo CHtml::encode($data->end_nepali_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('end_english_date')); ?>:</b>
	<?php echo CHtml::encode($data->end_english_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	*/ ?>

</div>