<?php
$this->breadcrumbs=array(
	'New Academic Session'=>array('create'),
	'Manage',
);

?>

<div class="form-horizontal well">
	<h2><strong>ACADEMIC YEAR LIST'S</strong></h2>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'academic-year-grid',
'htmlOptions'=>['class'=>'bordered'],
'type'=>'stripped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
		'year',
		'starting_nepali_date',
		'starting_english_date',
		'end_nepali_date',
		'end_english_date',
		'status',
		/*
		'created_by',
		'created_date',
		*/
array(
	'header'=>'Action',
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
</div>
