<?php
$this->breadcrumbs=array(
	'Academic Years'=>array('admin'),
	'Create',
);

$this->menu=array(
array('label'=>'List AcademicYear','url'=>array('index')),
array('label'=>'Manage AcademicYear','url'=>array('admin')),
);
?>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>