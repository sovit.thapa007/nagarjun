<?php
$this->breadcrumbs=array(
	'Academic Years'=>array('admin'),
	$model->id,
);
?>

<h4 class="text-center">View AcademicYear #<?php echo $model->id; ?></h4><hr />

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'type'=>'stripped bordered',
'attributes'=>array(
		//'id',
		'year',
		'starting_nepali_date',
		'starting_english_date',
		'end_nepali_date',
		'end_english_date',
		'status',
		/*'created_by',
		'created_date',*/
),
)); ?>
