<?php
$this->breadcrumbs=array(
	'Academic Years'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List AcademicYear','url'=>array('index')),
array('label'=>'Create AcademicYear','url'=>array('create')),
array('label'=>'View AcademicYear','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage AcademicYear','url'=>array('admin')),
);
?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>