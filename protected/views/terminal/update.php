<?php
$this->breadcrumbs=array(
	'Terminals'=>array('index'),
	$model->title=>array('view','id'=>$model->class_terminal_id),
	'Update',
);

$this->menu=array(
array('label'=>'List Terminal','url'=>array('index')),
array('label'=>'Create Terminal','url'=>array('create')),
array('label'=>'View Terminal','url'=>array('view','id'=>$model->class_terminal_id)),
array('label'=>'Manage Terminal','url'=>array('admin')),
);
?>

<h1>Update Terminal <?php echo $model->class_terminal_id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>