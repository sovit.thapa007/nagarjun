<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'terminal-form',
	'enableAjaxValidation'=>false,
	'type' => 'horizontal',
	'enableClientValidation' => true,
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
	),
	'focus' => array($model, 'title'),
)); ?>

<p class="text-center alert alert-info help-block">Fields with <span class="required">*</span> are required.</p> <br />
<div class="row-fluid">
	<dvi class="span6">
		<?php echo $form->textFieldRow($model,'title',array('class'=>'span12','maxlength'=>100)); ?>

		<?php echo $form->textFieldRow($model,'terminal_',array('class'=>'span12')); ?>

		<?php echo $form->dropDownListRow($model,'terminal_type',array(1=>'CAS Type',0=>'Terminal Only'),array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'pre_for_terminal',array('class'=>'span12')); ?>
	</dvi>
	<div class="span6">

		<?php echo $form->textFieldRow($model,'csa_percentage',array('class'=>'span12','maxlength'=>6)); ?>

		<?php echo $form->textFieldRow($model,'per_for_final',array('class'=>'span12')); ?>

		<?php 
		if(!$model->isNewRecord)
			echo $form->dropDownListRow($model,'status',array(1=>'Active', 0=>'De-Active'),array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'remarks',array('class'=>'span12')); ?>
	</div>
</div>

	<div class="text-center form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
