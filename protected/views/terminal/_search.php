<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'class_terminal_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>100)); ?>

		<?php echo $form->textFieldRow($model,'pre_for_terminal',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'terminal_type',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'csa_percentage',array('class'=>'span5','maxlength'=>6)); ?>

		<?php echo $form->textFieldRow($model,'terminal_',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'per_for_final',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'status',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'remarks',array('class'=>'span5')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
</div>

<?php $this->endWidget(); ?>
