<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('class_terminal_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->class_terminal_id),array('view','id'=>$data->class_terminal_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pre_for_terminal')); ?>:</b>
	<?php echo CHtml::encode($data->pre_for_terminal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('terminal_type')); ?>:</b>
	<?php echo CHtml::encode($data->terminal_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('csa_percentage')); ?>:</b>
	<?php echo CHtml::encode($data->csa_percentage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('terminal_')); ?>:</b>
	<?php echo CHtml::encode($data->terminal_); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('per_for_final')); ?>:</b>
	<?php echo CHtml::encode($data->per_for_final); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('remarks')); ?>:</b>
	<?php echo CHtml::encode($data->remarks); ?>
	<br />

	*/ ?>

</div>