<?php
$this->breadcrumbs=array(
	'Terminals'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Terminal','url'=>array('index')),
array('label'=>'Create Terminal','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('terminal-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2 class="text-center">Manage Terminals</h2>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'terminal-grid',
'type'=>'bordered stripped-bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'class_terminal_id',
		'title',
		'pre_for_terminal',
		[
			'name'=>'terminal_type',
			'value'=>'$data->terminal_type == 1 ?  "CAS" : "TERMINAL"',
		],
		'csa_percentage',
		'terminal_',
		'remarks',
		/*
		'per_for_final',
		'status',
		'remarks',
		*/
array(
	'header'=>"Action",
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
