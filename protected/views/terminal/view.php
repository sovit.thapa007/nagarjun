<?php
$this->breadcrumbs=array(
	'Terminals'=>array('index'),
	'Create Terminals'=>array('create'),
	$model->title,
);

$this->menu=array(
array('label'=>'List Terminal','url'=>array('index')),
array('label'=>'Create Terminal','url'=>array('create')),
array('label'=>'Update Terminal','url'=>array('update','id'=>$model->class_terminal_id)),
array('label'=>'Delete Terminal','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->class_terminal_id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Terminal','url'=>array('admin')),
);
?>

<h1>View Terminal #<?php echo $model->class_terminal_id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'type'=>'bordered stripped-bordered',
'attributes'=>array(
		'class_terminal_id',
		'title',
		'pre_for_terminal',
		[
			'name'=>'terminal_type',
			'value'=>$model->terminal_type == 1 ?  "CAS" : "TERMINAL",
		],
		'csa_percentage',
		'terminal_',
		'per_for_final',
		'status',
		'remarks',
),
)); ?>
