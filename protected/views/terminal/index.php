<?php
$this->breadcrumbs=array(
	'Terminals',
);

$this->menu=array(
array('label'=>'Create Terminal','url'=>array('create')),
array('label'=>'Manage Terminal','url'=>array('admin')),
);
?>

<h1>Terminals</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
