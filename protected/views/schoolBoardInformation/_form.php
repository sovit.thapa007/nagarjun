<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'school-board-information-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
		'enctype' => 'multipart/form-data',
	),
)); ?>

    <h2><STRONG>REGISTER/UPDATE TEACHERS/BOARDMEMBER INFORMATION</STRONG></h2>
	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->textFieldRow($model,'name',array('class'=>'span12','maxlength'=>200)); ?>

			<?php echo $form->dropDownListRow($model,'post',array("principal"=>"principal","headteacher"=>"headteacher","chairman"=>"chairman","teacher"=>"teacher","member"=>"member", 'contact_person'=>'Contact Person'),array('class'=>'span12')); ?>

			<?php echo $form->textFieldRow($model,'mobile_number',array('class'=>'span12')); ?>

			<?php echo $form->textFieldRow($model,'phone_number',array('class'=>'span12','maxlength'=>20)); ?>
		</div>
		<div class="span6">
			<?php echo $form->textFieldRow($model,'email',array('class'=>'span12','maxlength'=>100)); ?>

			<?php echo $form->textFieldRow($model,'temporary_address',array('class'=>'span12','maxlength'=>200)); ?>

			<?php echo $form->textFieldRow($model,'permanent_address',array('class'=>'span12','maxlength'=>200)); ?>
	        <div class="row-fluid">
	            <div class="span6">
	            <?php
	                if(!empty($model->photo)){
	                    ?>
	                    <img src="<?= Yii::app()->baseUrl; ?>/images/school_document/<?= $model->photo; ?>" height="120" width="120">
	                    <?php
	                }
	            ?>
	            </div>
	            <div class="span6">
	    		  <?php echo $form->fileFieldRow($model,'photo',array('maxlength'=>200)); ?>
	            </div>
	        </div>
	        <div class="row-fluid">
	            <div class="span6">
	            <?php
	                if(!empty($model->signature)){
	                    ?>
	                    <img src="<?= Yii::app()->baseUrl; ?>/images/school_document/signature/<?= $model->signature; ?>" height="120" width="120">
	                    <?php
	                }
	            ?>
	            </div>
	            <div class="span6">
	              <?php echo $form->fileFieldRow($model,'signature',array('maxlength'=>200)); ?>
	            </div>
	        </div>
		</div>
	</div>
	<div class="row-fluid">
            <?php 
            if(UtilityFunctions::ShowSchool()){
                echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => Yii::app()->params['select_'])),'id',Yii::app()->params['select_']),array('class'=>'span12', 'prompt'=>'Select School', 'selected'=>1)); 
            }

		if(!$model->isNewRecord){
			echo $form->dropDownListRow($model,'status',array(1=>'Active', '0'=>'De-Active'), array('class'=>'span12'));
		}		
	?>


	</div>
	<!-- 
		<?php echo $form->textFieldRow($model,'school_id',array('class'=>'span12')); ?> -->

<!-- 
		<?php echo $form->textFieldRow($model,'created_by',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'created_at',array('class'=>'span12')); ?> -->
	<div class="form-actions text-center">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>' SAVE INFORMATION',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
