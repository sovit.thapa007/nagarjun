<?php
$this->breadcrumbs=array(
	'School Board Informations'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List SchoolBoardInformation','url'=>array('index')),
array('label'=>'Create SchoolBoardInformation','url'=>array('create')),
array('label'=>'View SchoolBoardInformation','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage SchoolBoardInformation','url'=>array('admin')),
);
?>

<h4 class="text-center"><strong>Update Information : <?php echo $model->name; ?></strong></h4>
<div class="row-fluid well">
	<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
</div>