<?php
$this->breadcrumbs=array(
	'School Board Informations'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List SchoolBoardInformation','url'=>array('index')),
array('label'=>'Create SchoolBoardInformation','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('school-board-information-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

    <a href="<?= Yii::app()->baseUrl; ?>/schoolBoardInformation/create" class="btn btn-sm btn-primary" style="float: right;"> <i class="fa fa-plus"> </i> ADD NEW TEACHER'S</a>
<div class="form-horizontal well">
    <h2><strong>SCHOOL BOARDMEMBER/TEACHER LIST</strong></h2>
		<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'school-board-information-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
				//'id',
				'name',
				'post',
				'mobile_number',
				//'phone_number',
				//'email',
				[
					'name'=>'school_id',

					'value'=>'$data->school ?  $data->school->title : "null"',
					'visible'=>UtilityFunctions::ShowSchool(),
					'filter'=>CHtml::listData(BasicInformation::model()->findAll(),'id','title')
				],
				[
					'name'=>'status',
					'value' => '$data->status == 1 ?  "ACTIVE" : "IN-ACTIVE"',
					'filter'=>[1=>'ACTIVE', 0=>'IN-ACTIVE'],
				],
				/*
				'ward_no',
				'tol',
				'photo',
				'created_by',
				'created_at',
				*/
		array(
			'header'=>'ACTION',
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
		),
		)); ?>
	</div>
