<?php
$this->breadcrumbs=array(
	'School Board Informations'=>array('index'),
	$model->name,
);

$this->menu=array(
array('label'=>'List SchoolBoardInformation','url'=>array('index')),
array('label'=>'Create SchoolBoardInformation','url'=>array('create')),
array('label'=>'Update SchoolBoardInformation','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete SchoolBoardInformation','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage SchoolBoardInformation','url'=>array('admin')),
);
?>

<div class="form-horizontal well">
	<h2><strong>DETAILS : TEACHERS/BOARD MEMBER INFORMATION</strong></h2>
	
	<table class="table" style="width:100%">
		<tr>
			<td style="width: 30%">
				<img class="image" src="<?= Yii::app()->request->baseUrl.'/images/school_document/'.$model->photo; ?>" data-zoom-image="<?= Yii::app()->request->baseUrl.'/images/school_document/'.$model->photo; ?>"  hegiht="300" width="300"/>
			</td>
			<td>
				<table class="detail-view table table-striped table-condensed" style="width:100%">
					<tr>
						<td> NAME (नाम) : <?= ucwords($model->name); ?></td>
						<td> POST: <?= $model->post; ?></td>
					</tr>
					<tr>
						<td>EMAIL : <?= $model->email; ?> </td>
						<td> CONTACT NO. : <?= $model->mobile_number.' ( '.$model->phone_number.' ) '; ?></td>
					</tr>
					<tr>
						<td>CREATED By/At : <?= $model->created_by.'/'.$model->created_at; ?> </td>
						<td> PERMANENT/TEMPORARY ADDRESS : <?= $model->permanent_address.'-'.$model->temporary_address; ?></td>
					</tr>
					<tr>
						<td colspan="2"> School Name : <?= isset($model->school) ? $model->school->title : 'null'; ?></td>
					</tr>
					<tr><td colspan="2">
						<img class="image" src="<?= Yii::app()->request->baseUrl.'/images/school_document/signature/'.$model->signature; ?>" data-zoom-image="<?= Yii::app()->request->baseUrl.'/images/school_document/signature/'.$model->signature; ?>"  style="height: 100px; width: 100%;"/>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<a class="btn btn-middle btn-primary" title="" data-toggle="tooltip" href="<?= Yii::app()->request->baseUrl.'/schoolBoardInformation/update/'.$model->id ?>" data-original-title="Update"><i class="fa fa-pencil"></i> UPDATE INFORMATION</a>
</div>


