<?php
$this->breadcrumbs=array(
	'School Board Informations'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List SchoolBoardInformation','url'=>array('index')),
array('label'=>'Manage SchoolBoardInformation','url'=>array('admin')),
);
?>

<h4 class="text-center"><strong>SCHOOL BOARD/HEAD MEMBER  INFORMATION</strong></h4>
<div class="row-fluid well">
	<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>