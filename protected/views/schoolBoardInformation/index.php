<?php
$this->breadcrumbs = array(
	'Add New School' => array('create'),
	'Manage',
);

$this->menu = array(
	array('label' => 'List BasicInformation', 'url' => array('index')),
	array('label' => 'Create BasicInformation', 'url' => array('create')),
);


?>
<style type="text/css">
	tfoot {
    display: table-header-group;
}
</style>
<script type="text/javascript">


	$(document).ready(function() {
  	$('#example thead th').each( function () {
  		var title = $('#example tfoot th').eq( $(this).index() ).text();
    	$(this).html( '<input class="span12" type="text" placeholder="'+title+'" />' );
    } );

    $('#example').DataTable( {
    	lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ],
          "ajax": {
		    "url": "<?= Yii::app()->baseUrl; ?>/schoolBoardInformation/boardMemberDatatable",
		  },
        dom: 'Bfrtip',
        buttons: [
            'pageLength','copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );

	var table = $('#example').DataTable();
    // Apply the search
	table.columns().eq(0).each(function(colIdx) {
	    $('input', table.column(colIdx).header()).on('keyup change', function() {
	        table
	            .column(colIdx)
	            .search(this.value)
	            .draw();
	    });
	 
	    $('input', table.column(colIdx).header()).on('click', function(e) {
	        e.stopPropagation();
	    });
	});
} );
</script>
<div class="form-horizontal well">
    <h2><strong>SCHOOL BOARD MEMBER</strong></h2>
    <table id="example" class="display items table" style="width:100%">
            <thead>
                <tr>
                    <th>SN</th>
                    <th>POST</th>
                    <th>NAME</th>
                    <th>MOBILE NUMBER</th>
                    <th>EMAIL</th>
                    <th>SCHOOL</th>
                </tr>
            </thead>

            <tfoot>
                <tr>
                    <th>SN</th>
                    <th>POST</th>
                    <th>NAME</th>
                    <th>MOBILE NUMBER</th>
                    <th>EMAIL</th>
                    <th>SCHOOL</th>
                </tr>
            </tfoot>
        </table>
</div>

