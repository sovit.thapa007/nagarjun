<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>200)); ?>

		<?php echo $form->dropDownListRow($model,'post',array("principal"=>"principal","headteacher"=>"headteacher","chairman"=>"chairman","teacher"=>"teacher","member"=>"member",),array('class'=>'input-large')); ?>

		<?php echo $form->textFieldRow($model,'mobile_number',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'phone_number',array('class'=>'span5','maxlength'=>20)); ?>

		<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>100)); ?>

		<?php echo $form->textFieldRow($model,'ward_no',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'tol',array('class'=>'span5','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'photo',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'school_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'status',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_at',array('class'=>'span5')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
</div>

<?php $this->endWidget(); ?>
