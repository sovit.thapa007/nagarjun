<?php
$this->breadcrumbs=array(
	'School Levels'=>array('index'),
	$model->title,
);

$this->menu=array(
array('label'=>'List SchoolLevel','url'=>array('index')),
array('label'=>'Create SchoolLevel','url'=>array('create')),
array('label'=>'Update SchoolLevel','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete SchoolLevel','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage SchoolLevel','url'=>array('admin')),
);
?>

<h1>View SchoolLevel #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'title',
		'status',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
),
)); ?>
