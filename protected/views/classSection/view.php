<?php
$this->breadcrumbs=array(
	'Class Sections'=>array('index'),
	$model->title,
);

$this->menu=array(
array('label'=>'List ClassSection','url'=>array('index')),
array('label'=>'Create ClassSection','url'=>array('create')),
array('label'=>'Update ClassSection','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete ClassSection','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage ClassSection','url'=>array('admin')),
);
?>

<h2 class="text-center">View Class Section #<?php echo $model->id; ?></h2>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'type'=>'stripped bordered',
'attributes'=>array(
		'id',
		'title',
		'neapli_year',
		'description',
		'capacity',
		'current_status',
		[
			'label'=>'School',
			'value'=> isset($model->school_sec) ? $model->school_sec->title : 'null',
		],
		[
			'label'=>'Created By',
			'value'=> isset($model->user_sec) ? $model->user_sec->username : 'null',
		],
		'created_date',
),
)); ?>
