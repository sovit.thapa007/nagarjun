<?php
$this->breadcrumbs=array(
	'Class Sections',
);

$this->menu=array(
array('label'=>'Create ClassSection','url'=>array('create')),
array('label'=>'Manage ClassSection','url'=>array('admin')),
);
?>

<h1>Class Sections</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
