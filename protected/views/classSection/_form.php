<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'class-section-form',
	'enableAjaxValidation'=>false,
	'type' => 'horizontal',
	'enableClientValidation' => true,
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
	),
	'focus' => array($model, 'title'),
)); ?>


<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>150)); ?>

		<?php echo $form->textFieldRow($model,'neapli_year',array('class'=>'span5')); ?>

		<?php echo $form->textAreaRow($model,'description',array('rows'=>6, 'cols'=>10, 'class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'capacity',array('class'=>'span5')); ?>

		<?php echo $form->dropDownListRow($model,'current_status',array("active"=>"active","closed"=>"closed",),array('class'=>'span5')); ?>

		<?php //echo $form->textFieldRow($model,'school_id',array('class'=>'span5')); ?>

		<?php //echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

		<?php //echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?>

	<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create Section' : 'Save Section',
		)); ?>
</div>

<?php $this->endWidget(); ?>
