<?php
$this->breadcrumbs=array(
	'Class Sections'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List ClassSection','url'=>array('index')),
array('label'=>'Create ClassSection','url'=>array('create')),
array('label'=>'View ClassSection','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage ClassSection','url'=>array('admin')),
);
?>

<h2 class="text-center">Update Class Section <?php echo $model->title; ?></h2>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>