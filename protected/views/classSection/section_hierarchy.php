<?php
$program_id = array();
$criteria = new CDbCriteria;
$criteria->addInCondition('id', $program_id);

echo $form->dropDownListRow($model, $array_level[0], CHtml::listData(SchoolLevel::model()->findAll(), 'id', 'title'), array('prompt' => 'Select Level', 'class' => 'span12', 'id' => 'school_level', 'data-url' => Yii::app()->createUrl(ClassInformation::PRGURL)), array('label' => 'Level'));
?>

<?php echo $form->dropDownListRow($model, $array_level[1], array(), ['prompt' => 'Select Program', 'id' => 'program_level', 'class' => 'span12', 'data-url' => Yii::app()->createUrl(ClassInformation::CLSURL)], ['label' => 'Program Level']); ?>

<?php echo $form->dropDownListRow($model, $array_level[2], array(), ['prompt' => 'Select Class', 'id' => 'class_id', 'class' => 'span12', 'data-url' => Yii::app()->createUrl(ClassInformation::SECURL)], ['label' => 'Class']); ?>

<?php echo $form->dropDownListRow($model, $array_level[3], array(), ['prompt' => 'Select Class Section', 'id' => 'class_section_id', 'class' => 'span12'], ['label' => 'Class Section']); ?>

<?php echo CHtml::hiddenField('section_id', $section_id, array('id' => 'class_information_id', 'data-url' => Yii::app()->createUrl(ClassInformation::PRVCLSURL))); ?>

<script type="text/javascript">
    $('body').on('change', '#school_level', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        if (selected.length > 0) {
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#program_level').html(response.option);
                    } else {
                        $('#program_level').html('');
                        $('#class_id').html('');
                        $('#class_section_id').html('');
                    }
                }
            });
        }
    });


    $('body').on('change', '#program_level', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        if (selected.length > 0) {
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#class_id').html(response.option);
                    } else {
                        $('#class_id').html('');
                        $('#class_section_id').html('');
                    }
                }
            });
        }
    });


    $('body').on('change', '#class_id', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        if (selected.length > 0) {
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#class_section_id').html(response.option);
                    } else {
                        $('#class_section_id').html('');
                    }
                }
            });
        }
    });


    $(function() {
        var this_ = $('#class_information_id');
        var selected_id = this_.val();
        var url = this_.data('url');
        if (selected_id !="") {
            $.ajax({
                type: 'POST',
                url: url,
                data: {class_id: selected_id},
                dataType: 'JSON',
                async: false,
                success: function(response) {
                    if (response.success) {
                        $('#class_id').html(response.option);
                    }
                }
            });


            $.ajax({
                type: 'POST',
                url: "<?php echo Yii::app()->createUrl(ClassInformation::PRVPRGURL); ?>",
                data: {class_id: selected_id},
                dataType: 'JSON',
                async: false,
                success: function(response) {
                    if (response.success) {
                        $('#program_level').html(response.option);
                    }
                }
            });


            $.ajax({
                type: 'POST',
                url: "<?php echo Yii::app()->createUrl(ClassInformation::PRVLVLURL); ?>",
                data: {class_id: selected_id},
                dataType: 'JSON',
                async: false,
                success: function(response) {
                    if (response.success) {
                        $('#school_level').html(response.option);
                    }
                }
            });
        }
        //alert('just testing');
    });

</script>
