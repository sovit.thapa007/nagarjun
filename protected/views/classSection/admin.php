<?php
$this->breadcrumbs=array(
	'Create Class Sections'=>array('create'),
	'Manage',
);

$this->menu=array(
array('label'=>'List ClassSection','url'=>array('index')),
array('label'=>'Create ClassSection','url'=>array('create')),
);
?>

<h2 class="text-center">Manage Class Sections</h2>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'class-section-grid',
'type'=>'stripped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
		'title',
		'neapli_year',
		'description',
		'capacity',
		'current_status',
		/*
		'school_id',
		'created_by',
		'created_date',
		*/
array(
	'header'=>'Action',
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
