<?php
$this->breadcrumbs=array(
	'Class Sections'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List ClassSection','url'=>array('index')),
array('label'=>'Manage ClassSection','url'=>array('admin')),
);
?>

<h2 class="text-center">Create Class Section</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>