<?php

/**
 * This is the model class for table "result_average_report".
 *
 * The followings are the available columns in table 'result_average_report':
 * @property integer $id
 * @property integer $academic_year
 * @property integer $school_id
 * @property string $average_percentage
 * @property string $average_grade
 * @property integer $average_gp
 * @property string $result_date
 * @property integer $created_by
 * @property string $created_at
 * @property integer $approved_by
 * @property string $approved_at
 */
class ResultAverageReport extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'result_average_report';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('academic_year, school_id, average_percentage, average_grade, average_gp, total_student, result_date, created_by, created_at', 'required'),
			array('academic_year, school_id, created_by, approved_by, total_student', 'numerical', 'integerOnly'=>true),
			array('average_percentage, average_gp', 'length', 'max'=>6),
			array('average_grade', 'length', 'max'=>20),
			array('approved_at', 'safe'),
			//html purification
            array('id, academic_year, school_id, average_percentage, average_grade, average_gp, result_date, created_by, created_at, approved_by, approved_at', 'filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, academic_year, school_id, average_percentage, average_grade, average_gp, result_date, created_by, created_at, approved_by, approved_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'academic_year' => 'Academic Year',
			'school_id' => 'School',
			'average_percentage' => 'Average Percentage',
			'average_grade' => 'Average Grade',
			'average_gp' => 'Average Gp',
			'total_student' => 'Total Student',
			'result_date' => 'Result Date',
			'created_by' => 'Created By',
			'created_at' => 'Created At',
			'approved_by' => 'Approved By',
			'approved_at' => 'Approved At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('academic_year',$this->academic_year);
		$criteria->compare('school_id',$this->school_id);
		$criteria->compare('average_percentage',$this->average_percentage,true);
		$criteria->compare('average_grade',$this->average_grade,true);
		$criteria->compare('average_gp',$this->average_gp);
		$criteria->compare('total_student',$this->total_student);
		$criteria->compare('result_date',$this->result_date,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('approved_by',$this->approved_by);
		$criteria->compare('approved_at',$this->approved_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ResultAverageReport the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	*/
	public function AverageReportData($academic_year, $grade_point, $limit){
		$criteria = new CDbCriteria;
		$criteria -> condition = "academic_year=:academic_year";
		$criteria -> params = [':academic_year'=>(int) $academic_year];
		if($grade_point){
			$criteria -> condition .=" AND average_gp=:average_gp";
			$criteria -> params = array_merge($criteria->params, [':average_gp'=>$grade_point]);
		}
		if($limit)
			$criteria -> limit = $limit;
		$criteria -> order = 'average_percentage DESC';
		$averageReport = $this->findAll($criteria);
		$reportData = [];
		if(!empty($averageReport)){
			$sn = 1;
			foreach ($averageReport as $dataInfo) {
				$schoolInformation = BasicInformation::model()->findByPk($dataInfo->school_id);
				$school_type = $schoolInformation ? ucwords($schoolInformation->type) : '';
				$schoolName = $schoolInformation ? strtoupper($schoolInformation->title.', '.Yii::app()->params['municipality_short'].'-'.$schoolInformation->ward_no) : '';
				$school_code = $schoolInformation ? $schoolInformation->schole_code : '';
				$reportData[] = [$sn, $school_type, $schoolName, $school_code, $dataInfo->average_gp];
				$sn++;
			}
		}
		$reportDataArray = ["data"=>$reportData];
		return $reportDataArray;
	}
}
