<?php

/**
 * This is the model class for table "student_processing".
 *
 * The followings are the available columns in table 'student_processing':
 * @property integer $id
 * @property integer $student_id
 * @property integer $school_id
 * @property string $state
 * @property string $status
 * @property string $remarks
 * @property integer $created_by
 * @property string $created_at
 */
class StudentProcessing extends CActiveRecord
{
	const APPROVALSTATE =['dataentry','headteacher','administrativeofficer','result'];
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'student_processing';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('student_id, school_id, state, status', 'required'),
			array('student_id, school_id, created_by', 'numerical', 'integerOnly'=>true),
			array('state', 'length', 'max'=>21),
			array('status', 'length', 'max'=>11),
			array('remarks, created_at', 'safe'),
			//html purification
            array('id, student_id, school_id, state, status, remarks, created_by, created_at', 'filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, student_id, school_id, state, status, remarks, created_by, created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'student_id' => 'Student',
			'school_id' => 'School',
			'state' => 'State',
			'status' => 'Status',
			'remarks' => 'Remarks',
			'created_by' => 'Created By',
			'created_at' => 'Created At',
		);
	}


	/**
	*before validation case */
	public function beforeValidate()
	{
		$this->created_by = Yii::app()->user->id;
		$this->created_at = new CDbExpression('now()');
		return parent::beforeValidate();
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('student_id',$this->student_id);
		$criteria->compare('school_id',$this->school_id);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StudentProcessing the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
