<?php

/**
 * This is the model class for table "academic_year".
 *
 * The followings are the available columns in table 'academic_year':
 * @property integer $id
 * @property integer $year
 * @property string $starting_nepali_date
 * @property string $starting_english_date
 * @property string $end_nepali_date
 * @property string $end_english_date
 * @property string $status
 * @property integer $created_by
 * @property string $created_date
 */
class AcademicYear extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'academic_year';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('year, starting_nepali_date, starting_english_date, end_nepali_date, end_english_date', 'required'),
			array('year, created_by', 'numerical', 'integerOnly'=>true),
			array('starting_nepali_date, end_nepali_date', 'length', 'max'=>10),
			array('status', 'length', 'max'=>9),
			array('created_date', 'safe'),
			//html purification
            array('id, year, starting_nepali_date, starting_english_date, end_nepali_date, end_english_date, status, created_by, created_date', 'filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, year, starting_nepali_date, starting_english_date, end_nepali_date, end_english_date, status, created_by, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'year' => 'Year',
			'starting_nepali_date' => 'Starting Nepali Date',
			'starting_english_date' => 'Starting English Date',
			'end_nepali_date' => 'End Nepali Date',
			'end_english_date' => 'End English Date',
			'status' => 'Status',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('year',$this->year);
		$criteria->compare('starting_nepali_date',$this->starting_nepali_date,true);
		$criteria->compare('starting_english_date',$this->starting_english_date,true);
		$criteria->compare('end_nepali_date',$this->end_nepali_date,true);
		$criteria->compare('end_english_date',$this->end_english_date,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AcademicYear the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function beforeValidate(){
		$this->created_by = Yii::app()->user->id;
		$this->created_date = new CDbExpression('NOW()');
		return parent::beforeValidate();
	}

	public function afterSave()
	{
		$id = $this->id;
		AcademicYear::model()->updateAll(array( 'status' => 'in-active' ), 'id !='.$id );	
		return parent::afterSave();
	}
}
