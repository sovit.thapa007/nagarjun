<?php

/**
 * This is the model class for table "student_exam_registration".
 *
 * The followings are the available columns in table 'student_exam_registration':
 * @property integer $id
 * @property integer $academic_year
 * @property integer $school_id
 * @property integer $start_point
 * @property integer $end_point
 * @property integer $total_student
 * @property integer $status
 * @property integer $approved_
 * @property string $created_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $updated_date
 */
class StudentExamRegistration extends CActiveRecord
{
	public $order_by, $type;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'student_exam_registration';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('academic_year, school_id, start_point, end_point, total_student', 'required'),
			array('academic_year, school_id, start_point, end_point, total_student, status, approved_, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('created_at, updated_date', 'safe'),
			//html purification
            array('id, academic_year, school_id, start_point, end_point, total_student, status, approved_, created_at, created_by, updated_by, updated_date', 'filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, academic_year, school_id, start_point, end_point, total_student, status, approved_, created_at, created_by, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'school' => array(self::BELONGS_TO, 'BasicInformation', 'school_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'academic_year' => 'Academic Year',
			'school_id' => 'School',
			'start_point' => 'Start Point',
			'end_point' => 'End Point',
			'total_student' => 'Total Student',
			'status' => 'Status',
			'approved_' => 'Approved',
			'created_at' => 'Created At',
			'created_by' => 'Created By',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}


	/**
	*before validation case */
	public function beforeValidate()
	{
		if($this->isNewRecord){
			$this->created_by = Yii::app()->user->id;
			$this->created_at = new CDbExpression('now()');
			$this->status = 1;	
		}else{
			$this->updated_by = Yii::app()->user->id;
			$this->updated_date = new CDbExpression('now()');	
		}
		return parent::beforeValidate();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('academic_year',$this->academic_year);
		$criteria->compare('school_id',$this->school_id);
		$criteria->compare('start_point',$this->start_point);
		$criteria->compare('end_point',$this->end_point);
		$criteria->compare('total_student',$this->total_student);
		$criteria->compare('status',$this->status);
		$criteria->compare('approved_',$this->approved_);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StudentExamRegistration the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function SchoolRegistrationDetail($school_id, $academic_year){

		$criteria=new CDbCriteria;
		$criteria->condition = 'school_id=:school_id AND academic_year=:academic_year AND status=:status';
		$criteria->params =[':school_id'=>$school_id, ':academic_year'=>$academic_year, ':status'=>1];
		$detail = $this->find($criteria);
		return $detail;
	}

}
