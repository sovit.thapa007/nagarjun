<?php

/**
 * This is the model class for table "subject_information".
 *
 * The followings are the available columns in table 'subject_information':
 * @property integer $id
 * @property string $title
 * @property string $code
 * @property integer $credits
 * @property double $subject_hrs
 * @property string $descriptions
 * @property integer $subject_nature
 * @property integer $status
 * @property integer $created_by
 * @property string $created_date
 */
class SubjectInformation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'subject_information';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, title_nepali', 'required'),
			array('credits, subject_nature, status, created_by, subject_order', 'numerical', 'integerOnly'=>true),
			array('subject_hrs', 'numerical'),
			array('title', 'length', 'max'=>250),
			array('code', 'length', 'max'=>10),
			array('descriptions, created_date, subject_type', 'safe'),
			//html purification
            array('id, title, title_nepali, code, credits, subject_hrs, descriptions, subject_nature, status, created_by, created_date, subject_type', 'filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, title_nepali, code, credits, subject_hrs, descriptions, subject_nature, status, created_by, created_date, subject_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'school' => array(self::BELONGS_TO, 'BasicInformation', 'school_id'),
			'user_sec' => array(self::BELONGS_TO, 'User', 'created_by'),
			'passMark' => array(self::HAS_ONE, 'PassMark', 'subject_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'title_nepali' => 'Title (Nepali)',
			'code' => 'Code',
			'credits' => 'Credits',
			'subject_hrs' => 'Subject Hrs',
			'descriptions' => 'Descriptions',
			'subject_nature' => 'Subject Nature',
			'subject_order' => 'Subject Order',
			'default_optional'=>'Default Optional',
			'school_id'=>'School',
			'subject_type' => 'Subject Type',
			'status' => 'Status',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('title_nepali',$this->title_nepali,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('credits',$this->credits);
		$criteria->compare('subject_hrs',$this->subject_hrs);
		$criteria->compare('descriptions',$this->descriptions,true);
		$criteria->compare('subject_nature',$this->subject_nature);
		$criteria->compare('subject_order',$this->subject_order);
		$criteria->compare('default_optional',$this->subject_order);
		$criteria->compare('school_id',$this->school_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('subject_type',$this->subject_type);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SubjectInformation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function SubjectColumn($initialColumn, $insert_type){

		$school_information = BasicInformation::model()->findByPk($school_id);
		$criteria = new CDbCriteria;
		$criteria->with = ['passMark'];
		$criteria->condition = 'passMark.status=:status AND t.status=:status';
		$criteria->params = [':status'=>1];
		if(!empty($school_information) && $school_information->subject_type != 'general'){
			$school_wise_subject = SchoolSubject::model()->findAll('school_id=:school_id AND status=:status',[':school_id'=>$school_id,':status'=>1]);
			$subject_id_array = [];
			if(!empty($school_wise_subject)){
				foreach ($school_wise_subject as $subject_)
					$subject_id_array[] = $subject_->subject_id;
			}
			$criteria->addInCondition('t.id',$subject_id_array);
		}
		else{
			$criteria->condition .= ' AND subject_type=:subject_type AND t.school_id IS NULL';
			$criteria->params = array_merge($criteria->params, [':subject_type'=>'general']);

		}
		$criteria->order = 't.subject_order';
		$subjectInformation = $this->findAll($criteria);
		$column = $initialColumn;
		$initialSize = sizeof($initialColumn);
		if(!empty($subjectInformation)){
			foreach ($subjectInformation as $subject) {
	            $subject_name = UtilityFunctions::ColumnName($subject -> title);
				if($subject->passMark && $subject->passMark->is_practical != 0){
					if(in_array($insert_type, ['both','theory']))
						$column[] = $subject_name.'_theory';
					if(in_array($insert_type, ['both','practical'])){
						$initialSize += 1;
						$column[] = $subject_name.'_practical';
					}
				}else{
					if($subject->subject_nature == 1){
						if(in_array($insert_type, ['both','theory']))
							$column[] = $subject_name;
					}
					else{
						if(in_array($insert_type, ['both','theory']))
							$column[] = $subject_name.'_theory';
						if(in_array($insert_type, ['both','practical'])){
							$initialSize += 1;
							$column[] = $subject_name.'_practical';
						}
					}
				}
				$initialSize++;
			}
		}
		$column = array_values($column);
		return $column;
	}


	public function StubjectDetail($initialColumn, $insert_type, $school_id){
		$school_information = BasicInformation::model()->findByPk($school_id);
		$criteria = new CDbCriteria;
		$criteria->with = ['passMark'];
		$criteria->condition = 'passMark.status=:status AND t.status=:status';
		$criteria->params = [':status'=>1];
		if(!empty($school_information) && $school_information->subject_type != 'general'){
			$school_wise_subject = SchoolSubject::model()->findAll('school_id=:school_id AND status=:status',[':school_id'=>$school_id,':status'=>1]);
			$subject_id_array = [];
			if(!empty($school_wise_subject)){
				foreach ($school_wise_subject as $subject_)
					$subject_id_array[] = $subject_->subject_id;
			}
			$criteria->addInCondition('t.id',$subject_id_array);
		}
		else{
			$criteria->condition .= ' AND subject_type=:subject_type AND t.school_id IS NULL';
			$criteria->params = array_merge($criteria->params, [':subject_type'=>'general']);
		}
		$criteria->order = 't.subject_order';
		$subjectInformation = $this->findAll($criteria);
		$subject_information = [];
		$column = $initialColumn;
		$initialSize = sizeof($initialColumn);
        if(!empty($subjectInformation)){
            $ik = 0 ;
            foreach ($subjectInformation as $information){
            	$is_practical = $information->passMark ? $information->passMark->is_practical : 0;
            	$theory_full_mark = $information->passMark ? $information->passMark->theory_full_mark : 0;
            	$practical_full_mark = $information->passMark ? $information->passMark->practical_full_mark : 0;
            	$theory_pm = $information->passMark ? $information->passMark->theory_pass_mark : 0;
            	$practical_pm = $information->passMark ? $information->passMark->practical_pass_mark : 0;
                $subject_nature = $information->subject_nature;
                $default_optional = $information->default_optional;
                $secondary_optional_school = $information->school_id;
                $subject = $information->title;
                $subject_name = UtilityFunctions::ColumnName($subject);
                $subject_information[$ik]['id'] = $information->id;
                $subject_information[$ik]['name'] = $subject;
                if($subject_nature == 1 || ($subject_nature == 3 && $default_optional == 1)){
                    if($is_practical && $is_practical !=0 && in_array($insert_type, ['both','practical'])){
                        $column[] = $subject_name.'_practical';
                		$subject_information[$ik]['practical'] = $subject_name.'_practical';
                    }
                    if($is_practical && $is_practical !=0 && in_array($insert_type, ['both','theory'])){
                        $column[] = $subject_name.'_theory';
                		$subject_information[$ik]['theory'] = $subject_name.'_theory';
                    }
                    if($is_practical==0 && in_array($insert_type, ['both','theory'])){
                        $column[] = $subject_name;
                		$subject_information[$ik]['theory'] = $subject_name;
                    }
                    if($is_practical){$is_practical_[] = 'true';}else{$is_practical_[] = 'false';}
                    $subject_information[$ik]['theory_fm'] = $theory_full_mark;
                    $subject_information[$ik]['practical_fm'] = $practical_full_mark != 0 ? $practical_full_mark :'';
                    $subject_information[$ik]['subject_nature'] = $information->subject_nature;
                    $subject_information[$ik]['default_optional'] = $information->default_optional;
                    $ik++;
                }
                if($subject_nature == 3 && $default_optional == 0 && $secondary_optional_school == $school_id){
                    if($is_practical && $is_practical !=0 && in_array($insert_type, ['both','practical'])){
                        $column[] = $subject_name.'_practical';
                		$subject_information[$ik]['practical'] = $subject_name.'_practical';
                    }
                    if($is_practical && $is_practical !=0 && in_array($insert_type, ['both','theory'])){
                        $column[] = $subject_name.'_theory';
                		$subject_information[$ik]['theory'] = $subject_name.'_theory';
                    }
                    if($is_practical==0 && in_array($insert_type, ['both','theory'])){
                        $column[] = $subject_name;
                		$subject_information[$ik]['theory'] = $subject_name;
                    }
                    if($is_practical){$is_practical_[] = 'true';}else{$is_practical_[] = 'false';}
                    $subject_information[$ik]['order'] = $information->subject_order;
                    $subject_information[$ik]['theory_fm'] = $theory_full_mark;
                    $subject_information[$ik]['practical_fm'] = $practical_full_mark != 0 ? $practical_full_mark :'';
                    $subject_information[$ik]['theory_pm'] = $theory_pm;
                    $subject_information[$ik]['practical_pm'] = $theory_pm != 0 ? $theory_pm :'';
                }
                $initialSize++;
            }
        }

		$column = array_values($column);
        $subject_detail['column'] = $column;
        $subject_detail['subject_information'] = $subject_information;
        return $subject_detail;
	}


	public function SubjectDetail($school_id = null){

		$school_information = BasicInformation::model()->findByPk($school_id);
		$criteria = new CDbCriteria;
		$criteria->with = ['passMark'];
		$criteria->condition = 'passMark.status=:status AND t.status=:status';
		$criteria->params = [':status'=>1];
		if(!empty($school_information) && $school_information->subject_type != 'general'){
			$school_wise_subject = SchoolSubject::model()->findAll('school_id=:school_id AND status=:status',[':school_id'=>$school_id,':status'=>1]);
			$subject_id_array = [];
			if(!empty($school_wise_subject)){
				foreach ($school_wise_subject as $subject_)
					$subject_id_array[] = $subject_->subject_id;
			}
			$criteria->addInCondition('t.id',$subject_id_array);
		}
		else{
			$criteria->condition .= '  AND subject_type=:subject_type AND t.school_id IS NULL';
			$criteria->params = array_merge($criteria->params, [':subject_type'=>'general']);

		}
		$criteria->order = 't.subject_order';
		$subjectInformation = $this->findAll($criteria);
		return $subjectInformation;
	}

	public function SubjectListSection($subject_id_array){
		$criteria = new CDbCriteria;
		$criteria -> condition = 'status=:status';
		$criteria -> params = [':status'=>1];
		if(!empty($subject_id_array))
		$criteria->addNotInCondition('id', $subject_id_array);
		$subject_information = $this->findAll($criteria);
		$subject_array = [];
		if (!empty($subject_information)) {
			$sn = 0;
			foreach ($subject_information as $subject_) {
				//$subject_array['id'] = $subject_->id;
				$subject_array[$subject_->id] = $subject_->title;
				$sn++;
 			}
		}
		return $subject_array;
	}

}
