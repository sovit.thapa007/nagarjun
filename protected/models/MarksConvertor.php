<?php

/**
 * This is the model class for table "marks_convertor".
 *
 * The followings are the available columns in table 'marks_convertor':
 * @property integer $id
 * @property string $type
 * @property string $code
 * @property string $min_range
 * @property string $max_range
 * @property integer $created_by
 * @property string $created_date
 */
class MarksConvertor extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'marks_convertor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('code', 'required'),
			array('created_by', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>8),
			array('code', 'length', 'max'=>50),
			array('min_range, max_range', 'length', 'max'=>5),
			array('created_date', 'safe'),
			//html purification
            array('id, type, code, min_range, max_range, created_by, created_date', 'filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, type, code, min_range, max_range, created_by, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'type' => 'Type',
			'code' => 'Code',
			'min_range' => 'Min Range',
			'max_range' => 'Max Range',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('min_range',$this->min_range,true);
		$criteria->compare('max_range',$this->max_range,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MarksConvertor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
