<?php

/**
 * This is the model class for table "basic_information".
 *
 * The followings are the available columns in table 'basic_information':
 * @property integer $id
 * @property string $title
 * @property integer $ward_no
 * @property string $tole
 * @property string $schole_code
 * @property string $establish_year
 * @property string $moto
 * @property string $remarks
 * @property string $create_date
 *
 * The followings are the available model relations:
 * @property Location $location
 * @property SchoolLevel[] $schoolLevels
 */
class BasicInformation extends CActiveRecord
{

	public $permanent_ward_no, $order_by, $report_type, $total_student;
	


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'basic_information';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, ward_no, schole_code, establish_year, type', 'required'),
			array('ward_no', 'numerical', 'integerOnly' => true),
			array('establish_year', 'numerical', 'integerOnly' => true),
			array('title, tole', 'length', 'max' => 250),
			array('schole_code', 'length', 'max' => 100),
			array('moto, remarks, create_date, type, logo, contact_number, contact_person, contact_person_number, school_email, contact_person_email, status, subject_type,title_nepali, bank_name, bank_account_number', 'safe'),
			//html purification
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, ward_no, tole, schole_code, establish_year, contact_number, moto, remarks, create_date, type, contact_person, contact_person_number, school_email, contact_person_email, status, subject_type, title_nepali', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'schoolLevels' => array(self::HAS_MANY, 'SchoolLevel', 'school_id'),
			'schoolRegistration' => array(self::HAS_MANY, 'StudentExamRegistration', 'school_id'),
			'students' => array(self::HAS_MANY, 'StudentInformation', 'school_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
        	'type'=>Yii::t('app','model.school.type'),
			'id' => 'ID',
			//'type' => 'School Type',
			'title' => 'School',
			'title_nepali' => 'Title Nepal',
			'ward_no' => 'Ward No.',
			'contact_number'=> 'Contact Number',
			'school_email' => 'School Email',
			'tole' => 'Tole',
			'schole_code' => 'School Code',
			'establish_year' => 'Establish Year',
			'moto' => 'Moto',
			'contact_person' => 'Contact Person',
			'contact_person_number' => 'Contact Person Number',
			'contact_person_email' => 'Contact Person Email',
			'logo' => ' Logo',
			'bank_name' => 'Bank Name',
			'bank_account_number' => 'Bank Account Number',
			'remarks' => 'Remarks',
			'resource_center' => 'Resource Center',
			'resource_center_id' => 'Resource Center Name',
			'subject_type' => 'Subject Type',
			'status' => 'Status',
			'create_date' => 'Create Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$user_id = Yii::app()->user->id;
		$user_informaiton = User::model()->findByPk($user_id);
		$school_id = !empty($user_informaiton) ? $user_informaiton->school_id : 0;
		$is_super_user = !empty($user_informaiton) ? $user_informaiton->superuser : null;

		$criteria = new CDbCriteria;

		if(!$is_super_user)
			$criteria->compare('id',$school_id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('logo', $this->logo, true);
		$criteria->compare('ward_no', $this->ward_no);
		$criteria->compare('tole', $this->tole, true);
		$criteria->compare('contact_number', $this->contact_number, true);
		$criteria->compare('school_email', $this->school_email, true);
		$criteria->compare('contact_person_email', $this->contact_person_email, true);
		$criteria->compare('contact_person_number', $this->contact_person_number, true);
		$criteria->compare('contact_person', $this->contact_person, true);
		$criteria->compare('schole_code', $this->schole_code, true);
		$criteria->compare('establish_year', $this->establish_year, true);
		$criteria->compare('moto', $this->moto, true);
		$criteria->compare('status', $this->status);
		$criteria->compare('remarks', $this->remarks, true);
		$criteria->compare('create_date', $this->create_date, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BasicInformation the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	public function beforeValidate()
	{
		$this->create_date = new CDbExpression('now()');
		$uploadedImage = CUploadedFile::getInstance($this, 'logo');
		if (!empty($uploadedImage)) {
			$name_information = explode('.', $uploadedImage->name);
			$size = sizeof($name_information);
			$extension = $name_information[$size-1];
			$image_name = UtilityFunctions::ColumnName($this->title).rand(1, 1000).".".$extension;
			$this->logo = $image_name;
			$strPath = 'images/logo/'. $image_name;
			CUploadedFile::getInstance($this, 'logo')->saveAs($strPath, true);
		}
		return parent::beforeValidate();
	}


	public function SchoolList($type, $order_by){
		$academic_year = UtilityFunctions::AcademicYear();
		$criteria = new CDbCriteria;
		$criteria -> condition = 't.status=:status';
		$criteria -> params = [':status'=>1];
		if($type == 'title')
			$criteria -> order = 'title '.$order_by;
		if($type == 'schole_code')
			$criteria -> order = 'schole_code '.$order_by;
		if($type == 'type')
			$criteria -> order = 'type '.$order_by;
		$schoolList = $this->findAll($criteria);
		return $schoolList;
	}

	public function SchoolReport($params){
		$order_by = isset($params['order_by']) && !empty($params['order_by']) ? $params['order_by'] : null;
		$school_type = isset($params['type']) && !empty($params['type']) ? $params['type'] : null;
		$school_name = isset($params['title']) && !empty($params['title']) ? $params['title'] : null;
		$schole_code = isset($params['schole_code']) && !empty($params['schole_code']) ? $params['schole_code'] : null;
		$ward_no = isset($params['ward_no']) && !empty($params['ward_no']) ? $params['ward_no'] : null;
		$tole = isset($params['tole']) && !empty($params['tole']) ? $params['tole'] : null;
		$establish_year = isset($params['establish_year']) && !empty($params['establish_year']) ? $params['establish_year'] : null;
		$criteria = new CDbCriteria();
		//$criteria -> with = ['board'];
		$criteria->compare('status', 1);
		if($school_type)
			$criteria->addInCondition('type', $school_type);
		if($ward_no)
			$criteria->addInCondition('ward_no', $ward_no);
		if($school_name)
			$criteria->compare('title', $school_name);
		if($schole_code)
			$criteria->compare('schole_code', $schole_code);
		if($establish_year)
			$criteria->compare('establish_year', $establish_year);
		if($tole)
			$criteria->compare('tole', $tole);
		if($order_by)
			$criteria ->  order = $order_by. ' ASC ';
		$schoolInformation = $this->findAll($criteria);
		$columns = $data = $schoolDetailArray = [];
		if(!empty($schoolInformation)){
			$columns = ['school_full_name','school_type' , 'school_code', 'school_address', 'contact_person', 'school_contact_number', 'headteacher', 'headteacher_contact_number', 'remarks'];
			$sn = 0;
			foreach ($schoolInformation as $schInfo) {
				$data[$sn]['school_full_name'] = $schInfo->title;
				$data[$sn]['school_type'] = $schInfo->type;
				$data[$sn]['school_code'] = $schInfo->schole_code;
				$data[$sn]['school_address'] = Yii::app()->params['municipality_short'].'-'.$schInfo->ward_no;
				$data[$sn]['school_contact_number'] = $schInfo->contact_number;
				$contact_person = !empty($schInfo->contact_person) ? $schInfo->contact_person : null;
				$contact_person_contact_number = !empty($schInfo->contact_person_contact_number) ? $schInfo->contact_person_contact_number : 'not-set';
				$headteacher = $headteacher_contact_number = 'not-set';
				$SchoolBoardInformation = SchoolBoardInformation::model()->findAll('school_id=:school_id AND status=:status',[':school_id'=>$schInfo->id, ':status'=>1]);
				if(!empty($SchoolBoardInformation)){
					foreach ($SchoolBoardInformation as $boardInfo) {
						if(in_array($boardInfo->post, ['principal','headteacher'])){
							$headteacher = $boardInfo->name;
							$headteacher_contact_number = $boardInfo->mobile_number.' / '.$boardInfo->phone_number;
						}
						if($boardInfo->post=='contact_person' && $contact_person == 'not-set'){
							$contact_person = $boardInfo->name;
							$contact_person_contact_number = $boardInfo->mobile_number.' / '.$boardInfo->phone_number;
						}
					}
				}

				$data[$sn]['headteacher'] = $headteacher;
				$data[$sn]['headteacher_contact_number'] = $headteacher_contact_number;
				$data[$sn]['contact_person'] = $contact_person;
				$data[$sn]['remarks'] = '';
				//$data[$sn]['contact_person_contact_number'] = $contact_person_contact_number;
				$sn++;
			}
		}
		$schoolDetailArray['column'] = $columns;
		$schoolDetailArray['data'] = $data;
		return $schoolDetailArray;

	}

	public function AssignResourceCenter($resource_array){
		$transaction = Yii::app()->db->beginTransaction();
		$error_array = [];

		$criteria = new CDbCriteria;
		$criteria -> condition = 'status=:status AND resource_center=:resource_center';
		$criteria -> params = [':status'=>1, ':resource_center'=>1];
		$criteria->addNotInCondition('id', $resource_array);
		$remove_resource_center = $this->findAll($criteria);
		if(!empty($remove_resource_center) && !empty($resource_array)){
			$school_id = implode(',', $resource_array);
			$school_id_array = ltrim($school_id, ',');
			$school_id_array = rtrim($school_id, ',');
			$remove_resource = $this->updateAll(array( 'resource_center' => 0 ), 'status = 1 AND resource_center = 1 AND id NOT IN ('.$school_id_array.')' );
			if(!$remove_resource){
				$error_array[] = 'false';
			}
		}

		for ($i=0; $i < sizeof($resource_array) ; $i++) { 
			$school_information = $this->findByPk($resource_array[$i]);
			if(!empty($school_information)){
				$school_information->resource_center = 1;
				if($school_information->validate() && $school_information->update())
					$error_array[] = 'true';
				else
					$error_array[] = 'false';
			}
		}
		if(!in_array('false', $error_array)){
			$transaction->commit();
			return true;
		}
		$transaction->rollback();
		return false;

	}

	public function ResourceCenterList(){
		$criteria = new CDbCriteria;
		$criteria -> select = 'resource_center_id, count(*) as id';
		$criteria -> condition = 'status=:status AND resource_center_id!=:resource_center_id';
		$criteria -> params = [':status'=>1, ':resource_center_id'=>0];
		$criteria -> distinct = true;
		$criteria -> group = 'resource_center_id';
		$resource_center_list = $this->findAll($criteria);
		return $resource_center_list;
	}

	public function ResourceCenterSchoolDetails($id){
		$academic_year = UtilityFunctions::AcademicYear();
		$criteria = new CDbCriteria;
		$criteria -> with = ['students'];
		$criteria -> select = 't.*, count(students.id) as total_student';
		$criteria -> condition = 't.resource_center_id =:resource_center_id AND t.status=:status AND students.academic_year=:academic_year AND students.status=:status';
		$criteria -> params = [':resource_center_id'=>$id,':status'=>1, ':academic_year'=>$academic_year];
		$criteria -> group = 'students.school_id';
		$school_list = $this->findAll($criteria);
		return $school_list;
	}

	/**
	* number of school  base on type
	*/
	public function ResourceCenterDetail($academic_year){
		$data_array = $detail_information = $school_array_data = [];
		$criteria = new CDbCriteria;
		$criteria -> select = 'type,resource_center_id, count(*) as id';
		$criteria -> condition = 'status=:status';
		$criteria -> params = [':status'=>1];
		$criteria -> distinct = true;
		$criteria -> group = 'resource_center_id, type';
		$resource_center_list = $this->findAll($criteria);
		if(!empty($resource_center_list)){
			foreach ($resource_center_list as $resource_) {
				$type = $resource_->type;
				$school_information = BasicInformation::model()->findAll('resource_center_id=:resource_center_id AND type=:type AND status=:status',[':resource_center_id'=>$resource_->resource_center_id, ':type'=>$type, ':status'=>1]);
				$data_array[$resource_->resource_center_id][$type.'_school_number'] = $resource_->id;
				$male_student = $female_student = $total_student = 0;
				if(!empty($school_information)){
					foreach ($school_information as $school_info){
						$school_id = $school_info->id;
						$criteria_ = new CDbCriteria;
						$criteria_ -> select = 'school_id, sex , count(*) as id';
						$criteria_ -> condition = 'academic_year=:academic_year AND status=:status AND school_id=:school_id';
						$criteria_ -> params = [':academic_year'=>$academic_year,':status'=>1, ':school_id'=>$school_id];
						$criteria_ -> group = 'sex';
						$studentSexWise = StudentInformation::model()->findAll($criteria_);
						if(!empty($studentSexWise)){
							$male = $female = 0;
							foreach ($studentSexWise as $student_num) {
								$male += $student_num->sex == 'male' ? $student_num->id : 0;
								$female += $student_num->sex == 'female' ? $student_num->id : 0;
								$male_student += $student_num->sex == 'male' ? $student_num->id : 0;
								$female_student += $student_num->sex == 'female' ? $student_num->id : 0;
							}
							$school_array_data[$school_id]['male'] = $male;
							$school_array_data[$school_id]['female'] = $female;
							$school_array_data[$school_id]['type'] = $type;
						}
					}
				}
				$data_array[$resource_->resource_center_id][$type.'_male_students'] = $male_student;
				$data_array[$resource_->resource_center_id][$type.'_female_students'] = $female_student;
				$data_array[$resource_->resource_center_id][$type.'_total_students'] = $male_student + $female_student;
			}
		}
		$detail_information['resource_detail'] = $data_array;
		$detail_information['school_array_data'] = $school_array_data;
		return $detail_information;
	}


	/**
	*
	*/
	public function ResourceCenter(){
		$criteria = new CDbCriteria;
		$criteria -> condition = 'status=:status AND resource_center=:resource_center';
		$criteria -> params = [':status'=>1, ':resource_center'=>1];
		$resource_center_list = $this->findAll($criteria);
		$resource_array = [];
		if(!empty($resource_center_list)){
			$sn = 0;
			foreach ($resource_center_list as $resource) {
				$resource_array[$sn]['id'] = $resource->id;
				$resource_array[$sn]['name'] = ucwords($resource->title);
				$sn++;
			}
		}
		return $resource_array;
	}

	/**
	*/
	public function ResourceSchool($params){
		$type_array = isset($params['type']) ? $params['type'] : null;
		$resource_center_array = isset($params['resource_center_id']) ? $params['resource_center_id'] : null;
		$criteria = new CDbCriteria;
		$criteria -> condition = 'status=:status';
		$criteria -> params = [':status'=>1];
		if($type_array)
			$criteria->addInCondition('type', $type_array);
		if($resource_center_array)
			$criteria->addInCondition('resource_center_id', $resource_center_array);
		$student_list = $this->findAll($criteria);
		return $student_list;
	}



}
