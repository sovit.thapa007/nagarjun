<?php

/**
 * This is the model class for table "message_receiver".
 *
 * The followings are the available columns in table 'message_receiver':
 * @property integer $id
 * @property string $ip_
 * @property string $mobile
 * @property string $event
 * @property string $name
 * @property integer $school_board_id
 * @property integer $message_id
 * @property string $message
 * @property integer $message_code
 * @property integer $credit_consumed
 * @property integer $status
 * @property string $remarks
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class MessageReceiver extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'message_receiver';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ip_, mobile, message_id, message, message_code, credit_consumed', 'required'),
			array('school_board_id, message_id, message_code, credit_consumed, status, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('ip_, name', 'length', 'max'=>200),
			array('mobile', 'length', 'max'=>10),
			array('event, remarks, updated_date, created_by, created_date', 'safe'),
			//html purification
            array('id, ip_, mobile, event, name, school_board_id, message_id, message, message_code, credit_consumed, status, remarks, created_by, created_date, updated_by, updated_date', 'filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, ip_, mobile, event, name, school_board_id, message_id, message, message_code, credit_consumed, status, remarks, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ip_' => 'Ip',
			'mobile' => 'Mobile',
			'event' => 'Event',
			'name' => 'Name',
			'school_board_id' => 'School Board',
			'message_id' => 'Message',
			'message' => 'Message',
			'message_code' => 'Message Code',
			'credit_consumed' => 'Credit Consumed',
			'status' => 'Status',
			'remarks' => 'Remarks',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('ip_',$this->ip_,true);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('event',$this->event,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('school_board_id',$this->school_board_id);
		$criteria->compare('message_id',$this->message_id);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('message_code',$this->message_code);
		$criteria->compare('credit_consumed',$this->credit_consumed);
		$criteria->compare('status',$this->status);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MessageReceiver the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	*/

	public function beforeSave()
	{
		$this->created_by = Yii::app()->user->id;
		$this->created_date = date('Y-m-d H:i:s');
		return parent::beforeValidate();
	}
}
