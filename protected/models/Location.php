<?php

/**
 * This is the model class for table "location".
 *
 * The followings are the available columns in table 'location':
 * @property integer $id
 * @property integer $parent_id
 * @property string $title
 * @property string $location_slug
 * @property string $nepali_date
 * @property string $created_date
 */
class Location extends CActiveRecord
{

	const DEVELOPMENT_REGION = 'DR';
	const ZONE = 'ZN';
	const DISTRICT = 'DS';
	const VDC = 'VDC';
	const WARD_NO = 'WDN';
	const LOCATION_LEVEL = 3;

	public $development_region;
	public $zone;
	public $district;
	public $vdc;
	public $wardno;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'location';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title , location_level', 'required'),
			array('parent_id , location_level', 'numerical', 'integerOnly' => true),
			array('title, location_slug', 'length', 'max' => 250),
			array('nepali_date', 'length', 'max' => 15),
			array('created_date, location_slug , parent_id, created_by', 'safe'),
			//html purification
            array('id, parent_id, title, location_slug, nepali_date, created_date, created_by , location_level', 'filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, parent_id, title, location_slug, nepali_date, created_date, created_by , location_level', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent_id' => 'Parent',
			'title' => 'Title',
			'location_slug' => 'Location Slug',
			'location_level' => 'Location Level ',
			'nepali_date' => 'Nepali Date',
			'created_date' => 'Created Date',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('parent_id', $this->parent_id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('location_slug', $this->location_slug, true);
		$criteria->compare('location_level', $this->location_level);
		$criteria->compare('nepali_date', $this->nepali_date, true);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('created_by', $this->created_by);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Location the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public static function locationUpdateurl($level, $id)
	{
		if ($level == 1) {
			$key = 'development_region';
		}
		if ($level == 2) {
			$key = 'zone';
		}
		if ($level == 3) {
			$key = 'district';
		}
		if ($level == 4) {
			$key = 'vdc';
		}
		if ($level == 5) {
			$key = 'wordno';
		}
		return Yii::app()->createUrl('location/update/?id=' . $id . '&key=' . $key);
	}

}
