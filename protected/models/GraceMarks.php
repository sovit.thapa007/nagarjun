<?php

/**
 * This is the model class for table "grace_marks".
 *
 * The followings are the available columns in table 'grace_marks':
 * @property integer $id
 * @property integer $academic_year
 * @property integer $minimum_mark
 * @property integer $maximum_subject
 * @property string $grace_mark
 * @property integer $status
 * @property integer $applied_
 * @property string $remarks
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class GraceMarks extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'grace_marks';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('academic_year,grace_mark', 'required'),
			array('academic_year, minimum_mark, maximum_subject, status, applied_, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('grace_mark', 'length', 'max'=>5),
			array('remarks, created_date, updated_date, minimum_mark, maximum_subject', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, academic_year, minimum_mark, maximum_subject, grace_mark, status, applied_, remarks, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'academic_year' => 'Academic Year',
			'minimum_mark' => 'Minimum Mark(%)',
			'maximum_subject' => 'Maximum Subject',
			'grace_mark' => 'Grace Mark',
			'status' => 'Status',
			'applied_' => 'Applied',
			'remarks' => 'Remarks',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('academic_year',$this->academic_year);
		$criteria->compare('minimum_mark',$this->minimum_mark);
		$criteria->compare('maximum_subject',$this->maximum_subject);
		$criteria->compare('grace_mark',$this->grace_mark,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('applied_',$this->applied_);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GraceMarks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function beforeValidate(){
		$this->academic_year = UtilityFunctions::AcademicYear();
		if($this->isNewRecord){
			$this->created_by = Yii::app()->user->id;
			$this->created_date = date('Y-m-d H:i:s');
		}else{
			$this->updated_by = Yii::app()->user->id;
			$this->updated_date = date('Y-m-d H:i:s');
		}
		return parent::beforeValidate();
	}
}
