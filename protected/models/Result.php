<?php

/**
 * This is the model class for table "result".
 *
 * The followings are the available columns in table 'result':
 * @property integer $id
 * @property integer $school_id
 * @property integer $academic_year
 * @property integer $student_id
 * @property string $section
 * @property string $result_type
 * @property integer $terminal_id
 * @property integer $terminal_
 * @property string $total_obtained_mark
 * @property string $total_full_mark
 * @property string $percentage
 * @property string $total_gpa
 * @property string $division
 * @property string $grade
 * @property integer $attendance
 * @property integer $total_attendance
 * @property integer $rank
 * @property string $result_status
 * @property integer $status
 * @property string $result_publish_date
 * @property integer $number_of_subject
 * @property integer $created_by
 * @property string $created_date
 * @property integer $approved_by
 * @property string $approved_date
 */
class Result extends CActiveRecord
{
	public $order_by, $result_, $ledger_type, $top, $symbol_number, $copy_original;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'result';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('school_id, academic_year, student_id, result_type, terminal_id, terminal_, total_obtained_mark, total_full_mark, percentage, total_gpa, division, rank, result_publish_date', 'required'),
			array('school_id, academic_year, student_id, terminal_id, terminal_, attendance, total_attendance, rank, status, number_of_subject, created_by, approved_by', 'numerical', 'integerOnly'=>true),
			array('section, result_publish_date', 'length', 'max'=>10),
			array('result_type', 'length', 'max'=>8),
			array('total_obtained_mark, total_full_mark, percentage', 'length', 'max'=>7),
			array('total_gpa', 'length', 'max'=>7),
			array('division, grade', 'length', 'max'=>20),
			array('created_date, approved_date', 'safe'),
			//html purification
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, school_id, academic_year, student_id, section, result_type, terminal_id, terminal_, total_obtained_mark, total_full_mark, percentage, total_gpa, division, grade, attendance, total_attendance, rank, result_status, status, result_publish_date, number_of_subject, created_by, created_date, approved_by, approved_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'school' => array(self::BELONGS_TO, 'BasicInformation', 'school_id'),
			'marks' => array(self::HAS_MANY, 'MarkObtained', 'student_id'),
			'student' => array(self::BELONGS_TO, 'StudentInformation', 'student_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'school_id' => 'School',
			'academic_year' => 'Academic Year',
			'student_id' => 'Student',
			'section' => 'Section',
			'result_type' => 'Result Type',
			'terminal_id' => 'Terminal',
			'terminal_' => 'Terminal',
			'total_obtained_mark' => 'Total Obtained Mark',
			'total_full_mark' => 'Total Full Mark',
			'percentage' => 'Percentage',
			'total_gpa' => 'Total Gpa',
			'division' => 'Division',
			'grade' => 'Grade',
			'attendance' => 'Attendance',
			'total_attendance' => 'Total Attendance',
			'rank' => 'Rank',
			'result_status' => 'Result Status',
			'status' => 'Status',
			'result_publish_date' => 'Result Publish Date',
			'number_of_subject' => 'Number Of Subject',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'approved_by' => 'Approved By',
			'approved_date' => 'Approved Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('school_id',$this->school_id);
		$criteria->compare('academic_year',$this->academic_year);
		$criteria->compare('student_id',$this->student_id);
		$criteria->compare('section',$this->section,true);
		$criteria->compare('result_type',$this->result_type,true);
		$criteria->compare('terminal_id',$this->terminal_id);
		$criteria->compare('terminal_',$this->terminal_);
		$criteria->compare('total_obtained_mark',$this->total_obtained_mark,true);
		$criteria->compare('total_full_mark',$this->total_full_mark,true);
		$criteria->compare('percentage',$this->percentage,true);
		$criteria->compare('total_gpa',$this->total_gpa,true);
		$criteria->compare('division',$this->division,true);
		$criteria->compare('grade',$this->grade,true);
		$criteria->compare('attendance',$this->attendance);
		$criteria->compare('total_attendance',$this->total_attendance);
		$criteria->compare('rank',$this->rank);
		$criteria->compare('result_status',$this->result_status,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('result_publish_date',$this->result_publish_date,true);
		$criteria->compare('number_of_subject',$this->number_of_subject);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('approved_by',$this->approved_by);
		$criteria->compare('approved_date',$this->approved_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Result the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	*
	*/
	public function SchoolAverageReport($academic_year){
		$criteria = new CDbCriteria;
		$criteria -> select = "academic_year, school_id, result_publish_date,AVG(percentage) as percentage, AVG(total_gpa) as total_gpa, count(student_id) as student_id";
		$criteria -> condition = "academic_year=:academic_year AND status=:status";
		$criteria -> params = [':academic_year'=>(int) $academic_year, ':status'=>1];
		$criteria -> group = 'school_id';
		$averageReport = $this->findAll($criteria);
	    //$transaction = Yii::app()->db->beginTransaction();
		if(!empty($averageReport)){
			foreach ($averageReport as $avgReport) {
				$percentage =  number_format($avgReport -> percentage, 2);
				$total_gpa = number_format($avgReport -> total_gpa, 2);
				$gradeLetter = UtilityFunctions::GradeLetter($total_gpa);
				$reportSection = ResultAverageReport::model()->find('academic_year=:academic_year AND school_id=:school_id',[':academic_year'=>$academic_year,':school_id'=>$avgReport->school_id]);
				if(empty($reportSection))
					$reportSection = new ResultAverageReport();
				$reportSection -> academic_year = $academic_year;
				$reportSection -> school_id = $avgReport->school_id;
				$reportSection -> average_percentage = $percentage;
				$reportSection -> average_grade = $gradeLetter;
				$reportSection -> average_gp = $total_gpa;
				$reportSection -> total_student = $avgReport->student_id;
				$reportSection -> result_date = $avgReport->result_publish_date;
	            $reportSection -> created_by = Yii::app()->user->id;
	            $reportSection -> created_at = new CDbExpression('NOW()');
	            if(!$reportSection->save()){
	            	echo "<pre>";
	            	echo print_r($reportSection->attributes)."<br />";
	            	echo print_r($reportSection->errors);
	            	exit;
	        		//$transaction->rollback();
					throw new CHttpException(500,'Error occur during processing, please try again');
	            }
			}
		}
	    //$transaction->commit();
	    return true;
	}


	public function StudentGradeReport($academic_year, $grade_point, $limit, $school_id){
		$criteria = new CDbCriteria;
		$criteria -> with = ['school', 'student'];
		$criteria -> condition = "t.academic_year=:academic_year AND t.status=:status";
		$criteria -> params = [':academic_year'=>(int) $academic_year, ':status'=>1];
		if($grade_point){
			$grade_point_information = explode('-', $grade_point);
			$max_gp = isset($grade_point_information[0]) ? $grade_point_information[0] :  0;
			$min_gp = isset($grade_point_information[1]) ? $grade_point_information[1] :  0;
			$criteria -> condition .= " AND total_gpa<=:max_gp AND total_gpa >:min_gp";
			$criteria->params = array_merge($criteria -> params, [':max_gp'=>$max_gp, ':min_gp'=>$min_gp]);
		}
		if($school_id){
			$criteria -> condition .= " AND t.school_id=:school_id";
			$criteria->params = array_merge($criteria -> params, [':school_id'=>$school_id]);
		}
		if($limit)
			$criteria -> limit = $limit;
		$criteria -> order = 'percentage DESC';
		$studentReport = $this->findAll($criteria);
		return $studentReport;
	}

	public function StudentResultList($params){
		$academic_year = isset($params['academic_year']) ? $params['academic_year'] : null;
		$order_by = isset($params['order_by']) ? $params['order_by'] : null;
		$terminal_id = isset($params['terminal_id']) ? $params['terminal_id'] : null;
		$symbol_number = isset($params['symbol_number']) ? $params['symbol_number'] : null;
		$school_id = isset($params['school_id']) ? $params['school_id'] : null;
		$criteria = new CDbCriteria;
		$criteria -> with = ['school', 'student'];
		$criteria -> condition = "t.status=:status";
		$criteria -> params = [':status'=>1];
		if($academic_year){
			$criteria -> condition .= " AND t.academic_year=:academic_year";
			$criteria->params = array_merge($criteria -> params, [':academic_year'=>$academic_year]);
		}
		if($terminal_id){
			$criteria -> condition .= " AND t.terminal_id=:terminal_id";
			$criteria->params = array_merge($criteria -> params, [':terminal_id'=>$terminal_id]);
		}
		if($symbol_number){
			$criteria -> condition .= " AND student.symbol_number=:symbol_number";
			$criteria->params = array_merge($criteria -> params, [':symbol_number'=>$symbol_number]);
		}
		if($school_id){
			$criteria -> condition .= " AND t.school_id=:school_id";
			$criteria->params = array_merge($criteria -> params, [':school_id'=>$school_id]);
		}
		if($order_by=='name')
			$criteria -> order = 'student.first_name ASC';
		if($order_by=='symbol_number')
			$criteria -> order = 'student.symbol_number ASC';
		if($order_by=='student_id')
			$criteria -> order = 'student.id ASC';
		$studentReport = $this->findAll($criteria);
		return $studentReport;

	}

	public function GradeWiseNumber($params){
		$academic_year = isset($params['academic_year']) ? $params['academic_year'] : UtilityFunctions::AcademicYear();
		$school_id = isset($params['school_id']) ? $params['school_id'] : null;
		$criteria = new CDbCriteria;
		$criteria -> select = "academic_year, grade, count(DISTINCT student_id) as student_id";
		$criteria -> condition = "status=:status";
		$criteria -> params = [':status'=>1];
		if($academic_year){
			$criteria -> condition .= " AND academic_year=:academic_year";
			$criteria->params = array_merge($criteria -> params, [':academic_year'=>$academic_year]);
		}
		if($school_id){
			$criteria -> condition .= " AND school_id=:school_id";
			$criteria->params = array_merge($criteria -> params, [':school_id'=>$school_id]);
		}
		$criteria -> group = 'academic_year, grade';
		$criteria -> order = 'academic_year, total_gpa DESC';
		$ResultInformation = $this->findAll($criteria);

		$schoolInformaiton = BasicInformation::model()->findAllByAttributes(['id'=>$school_id, 'status'=>1]);
		$schoolArray = [];
		if(!empty($schoolInformaiton)){
			foreach ($schoolInformaiton as $key => $value) 
				$schoolArray[] = ucwords($value->title);
		}

		$gradeLetter = ["A+","A","B+","B","C+","C","D+","D","E","Abs*"];
		$arrayWiseDataSet = $arrayDataPoint = $arrayAcademicYear = $arrayData = $data = [];
		if(!empty($ResultInformation)){
			foreach ($ResultInformation as $resultData) {
				$academic_year = $resultData->academic_year;
				$grade = $resultData->grade;
				$grade = str_replace("<sup>","",$grade);
				$grade = str_replace("</sup>","",$grade);
				$studentNumber = $resultData->student_id;
				if(!in_array($academic_year, $arrayAcademicYear))
					$arrayAcademicYear[] = $academic_year;
				$arrayWiseDataSet[$academic_year.'-'.$grade] = $studentNumber;
			}
		}
		for ($i=0; $i < sizeof($arrayAcademicYear) ; $i++) { 
			$year = $arrayAcademicYear[$i];
	        $arrayData[$i]['showInLegend'] = true;
	        $arrayData[$i]['name'] = $year;
	        $dataPoints = [];
			for ($j=0; $j < sizeof($gradeLetter) ; $j++) { 
				$grade = $gradeLetter[$j];
				$studentNumber = isset($arrayWiseDataSet[$year.'-'.$grade]) ? $arrayWiseDataSet[$year.'-'.$grade] : 0;
				$dataPoints[$j]['label'] = $grade;
				$dataPoints[$j]['y'] = (int) $studentNumber;
				$data[$year][$grade] = (int) $studentNumber;
			}
			$arrayData[$i]['dataPoints'] = $dataPoints;
		}
		$studentResultReport['dataPoints'] = $arrayData;
		$studentResultReport['data'] = $data;
		$studentResultReport['year'] = $arrayAcademicYear;
		$studentResultReport['schoolArray'] = $schoolArray;
		$studentResultReport['grade'] = $gradeLetter;
		return $studentResultReport;
	}

	/**
	*
	*/
	public function GradeWiseNumberGroupBySchool($academic_year, $school_id_array){
		$criteria = new CDbCriteria;
		$criteria -> with = ['school'];
		$criteria -> select = "school_id, academic_year, grade, count(DISTINCT student_id) as student_id";
		$criteria -> condition = "t.status=:status";
		$criteria -> params = [':status'=>1];
		if($academic_year){
			$criteria -> condition .= " AND academic_year=:academic_year";
			$criteria->params = array_merge($criteria -> params, [':academic_year'=>$academic_year]);
		}
		if($school_id_array)
			$criteria->addInCondition('t.school_id', $school_id_array);
		$criteria -> group = 'school_id, academic_year, grade';
		$criteria -> order = 'school.schole_code ASC';
		$resultInformation = $this->findAll($criteria);
		$gradeLetter = ["A<sup>+</sup>","A","B<sup>+</sup>","B","C<sup>+</sup>","C","D<sup>+</sup>","D","E","Abs<sup>*</sup>"];


		$criteria -> group = 'academic_year, school.type, grade';
		$school_type_report = $this->findAll($criteria);

		$schoolArray = $school_id_array_data =  $arrayWiseDataSet =  $school_type_data_array = [];
		if(!empty($resultInformation)){
			foreach ($resultInformation as $resultData) {
				$grade = $resultData->grade;
				$studentNumber = $resultData->student_id;
				$school_id = $resultData->school_id;
				if(!in_array($school_id, $school_id_array_data)){$school_id_array_data[] = $school_id; }
				$schoolArray[$school_id] = $resultData->school ? $resultData->school->title.':'.$resultData->school->schole_code.':'.$resultData->school->type : '';
				$arrayWiseDataSet[$school_id][$grade] = $studentNumber;
			}
		}
		if(!empty($school_type_report)){
			foreach ($school_type_report as $result_info) {
				$grade = $result_info->grade;
				$student_number = $result_info->student_id;
				$school_type = $result_info->school ? $result_info->school->type : '';
				$school_type_data_array[$school_type][$grade] = $student_number;
			}
		}
		$studentResultReport['school_type_data_array'] = $school_type_data_array;
		$studentResultReport['arrayWiseDataSet'] = $arrayWiseDataSet;
		$studentResultReport['school_id_array_data'] = $school_id_array_data;
		$studentResultReport['schoolArray'] = $schoolArray;
		$studentResultReport['gradeLetter'] = $gradeLetter;
		return $studentResultReport;
	}


	public function ReportSummery($academic_year){

		$type_sex_wise_highest_gpa_array = $sex_wise_highest_gpa_array = $highest_gpa_students = $average_school_type = $school_type_data_array = [];
		$gradeLetter = ["A<sup>+</sup>","A","B<sup>+</sup>","B","C<sup>+</sup>","C","D<sup>+</sup>","D","E","Abs<sup>*</sup>"];

		$criteria_new = new CDbCriteria;
		$criteria_new -> with = ['school'];
		$criteria_new -> select = "school_id, academic_year, grade, count(DISTINCT student_id) as student_id";
		$criteria_new -> condition = "t.status=:status AND t.academic_year=:academic_year";
		$criteria_new -> params = [':status'=>1,':academic_year'=>$academic_year];
		$criteria_new -> group = 'academic_year, school.type, grade';
		$school_type_report = $this->findAll($criteria_new);
		if(!empty($school_type_report)){
			foreach ($school_type_report as $result_info) {
				$grade = $result_info->grade;
				$student_number = $result_info->student_id;
				$school_type = $result_info->school ? $result_info->school->type : '';
				$school_type_data_array[$school_type][$grade] = $student_number;
			}
		}
		


		$criteria = new CDbCriteria;
		$criteria -> with = ['student','school'];
		$criteria -> select = 'MAX(total_gpa) as total_gpa';
		$criteria -> condition = 't.status=:status AND t.academic_year=:academic_year';
		$criteria -> params = [':status'=>1, ':academic_year'=>$academic_year];
		$overall_highest_gpa = $this->find($criteria);
		$highest_gpa = $overall_highest_gpa ? $overall_highest_gpa->total_gpa : null;
		$criteria_ = new CDbCriteria;
		$criteria_ -> with = ['student','school'];
		$criteria_ -> condition = 't.status=:status AND t.academic_year=:academic_year AND t.total_gpa=:total_gpa';
		$criteria_ -> params = [':status'=>1, ':academic_year'=>$academic_year, ':total_gpa'=>$highest_gpa];
		$highest_gpa_students_detail = $this->findAll($criteria_);
		if(!empty($highest_gpa_students_detail)){
			$n = 0;
			foreach ($highest_gpa_students_detail as $student_information) {
				$highest_gpa_students['school'][$n] =  $student_information->school ? strtoupper($student_information->school->title) : '';
				$highest_gpa_students['school_code'][$n] = $student_information->school ? $student_information->school->schole_code : '';
				$highest_gpa_students['school_type'][$n] = $student_information->school ? $student_information->school->type : '';
				$highest_gpa_students['symbol_number'][$n] = $student_information->school ? $student_information->student->symbol_number : '';
				$highest_gpa_students['name'][$n] = $student_information->school ? strtoupper($student_information->student->first_name.' '.$student_information->student->middle_name.' '.$student_information->student->last_name) : '';
				$highest_gpa_students['gpa'][$n] = $student_information->total_gpa;
				$n++;
			}
		}
		$criteria -> group = 'school.type, student.sex';
		$sex_type_highest_gpa = $this->findAll($criteria);
		if(!empty($sex_type_highest_gpa)){
			foreach ($sex_type_highest_gpa as $highest_gpa_section) {
				$school_type = $highest_gpa_section->school ? $highest_gpa_section->school->type : '';
				$sex = $highest_gpa_section->student ? $highest_gpa_section->student->sex : '';
				$gap = $highest_gpa_section -> total_gpa;

				$criteria_section = new CDbCriteria;
				$criteria_section -> with = ['student','school'];
				$criteria_section -> condition = 't.status=:status AND t.academic_year=:academic_year AND t.total_gpa=:total_gpa AND student.sex=:sex AND school.type=:type';
				$criteria_section -> params = [':status'=>1, ':academic_year'=>$academic_year, ':total_gpa'=>$gap, ':sex'=>$sex, ':type'=>$school_type];
				$student_detail_with_gpa = $this->findAll($criteria_section);
				if(!empty($student_detail_with_gpa)){
					$sn = 0;
					foreach ($student_detail_with_gpa as $student_detail) {
						$type_sex_wise_highest_gpa_array[$school_type][$sex]['school'][$sn] = $student_detail->school ? strtoupper($student_detail->school->title) : '';
						$type_sex_wise_highest_gpa_array[$school_type][$sex]['school_code'][$sn] = $student_detail->school ? $student_detail->school->schole_code : '';
						$type_sex_wise_highest_gpa_array[$school_type][$sex]['symbol_number'][$sn] = $student_detail->school ? $student_detail->student->symbol_number : '';
						$type_sex_wise_highest_gpa_array[$school_type][$sex]['name'][$sn] = $student_detail->school ? strtoupper($student_detail->student->first_name.' '.$student_detail->student->middle_name.' '.$student_detail->student->last_name) : '';
						$type_sex_wise_highest_gpa_array[$school_type][$sex]['gpa'][$sn] = $student_detail->total_gpa;
						$sn++;
					}
				}
			}
		}

		$criteria -> group = 'student.sex';
		$sex_wise_highest = $this->findAll($criteria);
		if(!empty($sex_wise_highest)){
			foreach ($sex_wise_highest as $sex_wise) {
				$sex_ = $sex_wise->student ? $sex_wise->student->sex : '';
				$gpa_ = $sex_wise->total_gpa;
				$criteria_sex = new CDbCriteria;
				$criteria_sex -> with = ['student','school'];
				$criteria_sex -> condition = 't.status=:status AND t.academic_year=:academic_year AND t.total_gpa=:total_gpa AND student.sex=:sex';
				$criteria_sex -> params = [':status'=>1, ':academic_year'=>$academic_year, ':total_gpa'=>$gpa_, ':sex'=>$sex_];
				$sex_student_ = $this->findAll($criteria_sex);
				if(!empty($sex_student_)){
					$s = 0;
					foreach ($sex_student_ as $student_in) {
						$sex_wise_highest_gpa_array[$sex_]['school'][$s] = $student_in->school ? strtoupper($student_in->school->title) : '';
						$sex_wise_highest_gpa_array[$sex_]['school_code'][$s] = $student_in->school ? $student_in->school->schole_code : '';
						$sex_wise_highest_gpa_array[$sex_]['school_type'][$s] = $student_in->school ? $student_in->school->type : '';
						$sex_wise_highest_gpa_array[$sex_]['symbol_number'][$s] = $student_in->school ? $student_in->student->symbol_number : '';
						$sex_wise_highest_gpa_array[$sex_]['name'][$s] = $student_in->school ? strtoupper($student_in->student->first_name.' '.$student_in->student->middle_name.' '.$student_in->student->last_name) : '';
						$sex_wise_highest_gpa_array[$sex_]['gpa'][$s] = $student_in->total_gpa;


						$s++;
					}
				}

			}
		}
		$whole_average = new CDbCriteria;
		$whole_average -> with = ['school'];
		$whole_average -> select = 'AVG(total_gpa) as total_gpa';
		$whole_average -> condition = 't.status=:status AND t.academic_year=:academic_year AND t.total_gpa!=:total_gpa';
		$whole_average -> params = [':status'=>1, ':academic_year'=>$academic_year, ':total_gpa'=>0];
		$whole_student_average_ = $this->find($whole_average);
		$municipality_average = $whole_student_average_ ? number_format($whole_student_average_->total_gpa,2) : '';

		$whole_average -> group = 'school.type';
		$school_type_average = $this->findAll($whole_average);
		if(!empty($school_type_average)){
			foreach ($school_type_average as $school_average) {
				$type = $school_average->school ? $school_average->school->type : '';
				$gpa = $school_average->total_gpa;
				$average_school_type[$type] = number_format($gpa,2);
			}
		}
		$report_summery['gradeLetter'] = $gradeLetter;
		$report_summery['school_type_data_array'] = $school_type_data_array;
		$report_summery['sex_wise_highest_gpa_array'] = $sex_wise_highest_gpa_array;
		$report_summery['highest_gpa_students'] = $highest_gpa_students;
		$report_summery['type_sex_wise_highest_gpa_array'] = $type_sex_wise_highest_gpa_array;
		$report_summery['municipality_average'] = $municipality_average;
		$report_summery['average_school_type'] = $average_school_type;
		return $report_summery;

	}


}
