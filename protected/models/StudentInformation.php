<?php

/**
 * This is the model class for table "student_information".
 *
 * The followings are the available columns in table 'student_information':
 * @property integer $id
 * @property string $symbol_number
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $sex
 * @property string $father_name
 * @property string $mother_name
 * @property string $gaurdain_name
 * @property string $section
 * @property integer $roll_number
 * @property string $dob_nepali
 * @property string $don_english
 * @property string $stream
 * @property integer $special_case
 * @property integer $ethinicity
 * @property integer $cast
 * @property string $permanent_location
 * @property string $temporary_location
 * @property integer $contact_number
 * @property string $gaurdain_occupassion
 * @property integer $school_id
 * @property string $photo
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class StudentInformation extends CActiveRecord
{
	public $order_by, $school_code, $male_number, $female_number;
	//const []
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'student_information';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('first_name, last_name, sex, dob_nepali, school_id, academic_year', 'required'),/*
			array(['academic_year','first_name', 'middle_name', 'last_name' , 'father_name' , 'dob_nepali', 'school_id'], 'unique', 'message' => "Student is already exit"),*/
			array('photo, signature, document', 'file', 'allowEmpty'=>true, 'types'=>'jpg,jpeg,gif,png'),
			array('roll_number, special_case, ethinicity, cast, contact_number, school_id, created_by, updated_by, registered_year', 'numerical', 'integerOnly'=>true),
			array('symbol_number', 'length', 'max'=>50),
			array('first_name, middle_name, last_name, optional_subject', 'length', 'max'=>100),
			array('sex', 'length', 'max'=>6),
			array('father_name, mother_name, gaurdain_name, gaurdain_occupassion, photo, signature', 'length', 'max'=>200),
			array('section', 'length', 'max'=>4),
			array('dob_nepali, registration_number', 'length', 'max'=>15),
			array('stream', 'length', 'max'=>7),
			array('permanent_location, temporary_location', 'length', 'max'=>250),
			array('symbol_number, don_english, updated_date, created_by, created_date, status, school_code', 'safe'),
			//html purification
            array('id, symbol_number, academic_year, first_name, middle_name, last_name, first_name_nepali, middle_name_nepali, last_name_nepali, sex, father_name, mother_name, gaurdain_name, section, roll_number, dob_nepali, don_english, stream, special_case, ethinicity, cast, permanent_location, temporary_location, contact_number, gaurdain_occupassion, school_id, photo, created_by, created_date, updated_by, updated_date, state, state_status, remarks, status, signature, document, registration_number', 'filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, symbol_number, academic_year, first_name, middle_name, last_name, first_name_nepali, middle_name_nepali, last_name_nepali, sex, father_name, mother_name, gaurdain_name, section, roll_number, dob_nepali, don_english, stream, special_case, ethinicity, cast, permanent_location, temporary_location, contact_number, gaurdain_occupassion, school_id, photo, created_by, created_date, updated_by, updated_date, state, state_status, remarks, status, signature, document, registration_number, school_code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'special_sec' => array(self::BELONGS_TO, 'SpecialCase', 'special_case'),
			'ethinicity_sec' => array(self::BELONGS_TO, 'EthinicGroup', 'ethinicity'),
			'cast_sec' => array(self::BELONGS_TO, 'EthinicGroup', 'cast'),
			'school_sec' => array(self::BELONGS_TO, 'BasicInformation', 'school_id'),
			'user_sec' => array(self::BELONGS_TO, 'User', 'created_by'),
			'updated_sec' => array(self::BELONGS_TO, 'User', 'updated_by'),
            'marks'=>array(self::HAS_MANY,'MarkObtained','student_id'),
            'Result'=>array(self::HAS_MANY,'Result','student_id'),
            'Student_Attendance'=>array(self::HAS_MANY,'StudentAttendance','student_id'),
            'StudentSubject'=>array(self::HAS_MANY,'StudentSubject','student_id'),

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'academic_year'=>'Academic Year',
			'registration_number' => 'Registration Number',
			'symbol_number' => 'Symbol Number',
			'first_name' => 'First Name',
			'first_name_nepali' => 'First Name Nepali',
			'middle_name' => 'Middle Name',
			'middle_name_nepali' => 'Middle Name Nepali',
			'last_name' => 'Last Name',
			'last_name_nepali' => 'Last Name Nepali',
			'sex' => 'Sex',
			'father_name' => 'Father Name',
			'mother_name' => 'Mother Name',
			'gaurdain_name' => 'Guardian Name',
			'section' => 'Section',
			'roll_number' => 'Roll Number',
			'dob_nepali' => 'DOB (BS)',
			'don_english' => 'DOB (AD)',
			'stream' => 'Stream',
			'special_case' => 'Special Case',
			'ethinicity' => 'Ethinicity',
			'cast' => 'Cast',
			'register_class'=>'Register Class',
			'register_year' => 'Register Year',
			'optional_subject' => 'Optional Subject',
			'permanent_location' => 'Permanent Location',
			'temporary_location' => 'Temporary Location',
			'contact_number' => 'Contact Number',
			'gaurdain_occupassion' => 'Guardian Occupation',
			'school_id' => 'School',
			'photo' => 'Photo',
			'remarks' => 'Remark',
			'state' => 'State',
			'status' => 'Status',
			'document' => 'Documents',
			'state_status' => 'State Status',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$user_id = Yii::app()->user->id;
		$user_informaiton = User::model()->findByPk($user_id);
		$school_id = !empty($user_informaiton) ? $user_informaiton->school_id : 0;
		$role = !empty($user_informaiton) ? $user_informaiton->role : null;
		$criteria=new CDbCriteria;
		$criteria->with = ['school_sec'];
		$criteria->compare('id',$this->id);
		$criteria->compare('academic_year', UtilityFunctions::AcademicYear());
		$criteria->compare('registration_number',$this->registration_number, true);
		$criteria->compare('symbol_number',$this->symbol_number,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('sex',$this->sex,true);
		$criteria->compare('father_name',$this->father_name,true);
		$criteria->compare('mother_name',$this->mother_name,true);
		$criteria->compare('gaurdain_name',$this->gaurdain_name,true);
		$criteria->compare('section',$this->section,true);
		$criteria->compare('roll_number',$this->roll_number);
		$criteria->compare('dob_nepali',$this->dob_nepali,true);
		$criteria->compare('don_english',$this->don_english,true);
		$criteria->compare('stream',$this->stream,true);
		$criteria->compare('special_case',$this->special_case);
		$criteria->compare('ethinicity',$this->ethinicity);
		$criteria->compare('document',$this->document);
		$criteria->compare('cast',$this->cast);
		$criteria->compare('permanent_location',$this->permanent_location,true);
		$criteria->compare('temporary_location',$this->temporary_location,true);
		$criteria->compare('contact_number',$this->contact_number);
		$criteria->compare('gaurdain_occupassion',$this->gaurdain_occupassion,true);
		if(!in_array($role,['administrativeofficer','dataentry','superadmin']))
			$criteria->compare('school_id',$school_id);
		else
			$criteria->compare('school_id',$this->school_id);
		$criteria->compare('school_sec.schole_code',$this->school_code, true);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('status',$this->status,true);
		$criteria->order= 'school_id, symbol_number ASC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>[
			'pageSize'=>50,
			],
		));
	}



	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function allsearch()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$user_id = Yii::app()->user->id;
		$user_informaiton = User::model()->findByPk($user_id);
		$school_id = !empty($user_informaiton) ? $user_informaiton->school_id : 0;
		$role = !empty($user_informaiton) ? $user_informaiton->role : null;
		$criteria=new CDbCriteria;
		
		$criteria->compare('id',$this->id);
		$criteria->compare('academic_year', $this->academic_year);
		$criteria->compare('registration_number',$this->registration_number, true);
		$criteria->compare('symbol_number',$this->symbol_number,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('sex',$this->sex,true);
		$criteria->compare('father_name',$this->father_name,true);
		$criteria->compare('mother_name',$this->mother_name,true);
		$criteria->compare('gaurdain_name',$this->gaurdain_name,true);
		$criteria->compare('section',$this->section,true);
		$criteria->compare('roll_number',$this->roll_number);
		$criteria->compare('dob_nepali',$this->dob_nepali,true);
		$criteria->compare('don_english',$this->don_english,true);
		$criteria->compare('stream',$this->stream,true);
		$criteria->compare('special_case',$this->special_case);
		$criteria->compare('ethinicity',$this->ethinicity);

		$criteria->compare('document',$this->document);
		$criteria->compare('cast',$this->cast);
		$criteria->compare('permanent_location',$this->permanent_location,true);
		$criteria->compare('temporary_location',$this->temporary_location,true);
		$criteria->compare('contact_number',$this->contact_number);
		$criteria->compare('gaurdain_occupassion',$this->gaurdain_occupassion,true);
		if(!in_array($role,['administrativeofficer','dataentry','superadmin']))
			$criteria->compare('school_id',$school_id);
		else
			$criteria->compare('school_id',$this->school_id);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('status',$this->status,true);
		$criteria->order= 'school_id, symbol_number ASC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>[
			'pageSize'=>50,
			],
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StudentInformation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	*before validation case */
	public function beforeValidate()
	{
		$user_ = UtilityFunctions::UserInformations();
		$this->academic_year = isset($user_['academic_year']) ? $user_['academic_year'] : '0000';
		$this->school_id = $this->school_id ? $this->school_id : $user_['school_id'];
		if($this->isNewRecord){
			$this->created_by = Yii::app()->user->id;
			$this->created_date = new CDbExpression('now()');
			$this->status = 1;	
		}else{
			$this->updated_by = Yii::app()->user->id;
			$this->updated_date = new CDbExpression('now()');	
		}
		$this->don_english = empty($this->don_english) ?  UtilityFunctions::NepaliToEnglish($this->dob_nepali) : $this->don_english;
		$nameInformation = UtilityFunctions::seoUrl($this->first_name.$this->middle_name.$this->last_name);
		$uploadedImage = CUploadedFile::getInstance($this, 'photo');
		if (!empty($uploadedImage)) {
			$fileName = 'photo_'.$nameInformation.'_'.$this->academic_year.'_'.$this->school_id.'.'.$uploadedImage->getExtensionName();
			$this->photo = $fileName;
			$strPath = 'images/students_photo/' . $fileName;
			CUploadedFile::getInstance($this, 'photo')->saveAs($strPath, true);
		}
		$signatureImage = CUploadedFile::getInstance($this, 'signature');
		if (!empty($signatureImage)) {
			$signatureName = 'signature_'.$nameInformation.'_'.$this->academic_year.'_'.$this->school_id.'.'.$signatureImage->getExtensionName();
			$this->signature = $signatureName;
			$sigstrPath = 'images/students_photo/signature/' . $signatureName;
			CUploadedFile::getInstance($this, 'signature')->saveAs($sigstrPath, true);
		}

		$documentImage = CUploadedFile::getInstance($this, 'document');
		if (!empty($documentImage)) {
			$documentName = 'document_'.$nameInformation.'_'.$this->academic_year.'_'.$this->school_id.'.'.$documentImage->getExtensionName();
			$this->document = $documentName;
			$sigstrPath = 'images/students_photo/documents/' . $documentName;
			CUploadedFile::getInstance($this, 'document')->saveAs($sigstrPath, true);
		}

		if(!$this->isNewRecord){
			$previousInformation = $this->findByPk($this->id);
			if(empty($uploadedImage) && !empty($previousInformation))
				$this->photo = $previousInformation->photo;
			if(empty($signatureImage) && !empty($previousInformation))
				$this->signature = $previousInformation->signature;
			if(empty($documentImage) && !empty($previousInformation))
				$this->document = $previousInformation->document;
		}
		return parent::beforeValidate();
	}

//list of student during marks insert section
	public function StudentMarklist($academic_year, $school_id, $order_by, $subjectid){
		$optional_subject_nature = SubjectInformation::model()->find('id=:subject_id AND subject_nature=:subject_nature AND status=:status', [':subject_id'=>$subjectid,':subject_nature'=>3, ':status'=>1]);
		$criteria = new CDbCriteria;
		$criteria->with = ['StudentSubject'];
		$criteria->condition = 't.academic_year=:academic_year AND t.school_id=:school_id';
		$criteria->params=[':academic_year'=>$academic_year, ':school_id'=>$school_id];
		if(!empty($optional_subject_nature) && $optional_subject_nature->school_id == $school_id && $optional_subject_nature->default_optional != 1){
			$criteria -> condition .= ' AND StudentSubject.subject_id=:subject_id AND StudentSubject.status=:student_subject_section';
			$criteria -> params = array_merge($criteria->params, [':subject_id'=>$subjectid, ':student_subject_section'=>'active']);
		}

		if(!empty($optional_subject_nature)  && $optional_subject_nature->default_optional == 1){
			$student_information = StudentInformation::model()->findAll('academic_year=:academic_year AND school_id=:school_id AND status=:status',[':academic_year'=>$academic_year, ':school_id'=>$school_id, ':status'=>1]);
			if(!empty($student_information)){
				foreach ($student_information as $student)
					$student_array[] = $student->id;
			}
			$optional_criteria = new CDbCriteria;
			$optional_criteria -> condition = 'status=:status';
			$optional_criteria -> params = [':status'=>'active'];
			if(isset($student_array) && !empty($student_array)){
	 			$optional_criteria->addInCondition('student_id', $student_array);
				$optional_student_array = StudentSubject::model()->findAll($optional_criteria);
				$secondary_opt_student = [];
				if(!empty($optional_student_array)){
					foreach ($optional_student_array as $secondary_opt)
						$secondary_opt_student[] = $secondary_opt->student_id;
				}
				$criteria->addNotInCondition('t.id', $secondary_opt_student);
			}
		}

		if($order_by=='name')
			$criteria -> order = 't.first_name ASC';
		if($order_by=='symbol_number')
			$criteria -> order = 't.symbol_number ASC';
		if($order_by=='student_id')
			$criteria -> order = 't.id ASC';
		$criteria->together = true;
		$student_list = $this->findAll($criteria);
		return $student_list;
	}


	/**
	* Student Raw Marks section
	*/
	public function StudentRawMarks($params){
        $user_information = UtilityFunctions::UserInformations();
        $school_id_sec = isset($user_information) ? $user_information['school_id'] : 0;
		$attendance_ = $exam_attendance_ = $student_information = array();
		$academic_year = UtilityFunctions::AcademicYear();
		$school_id = isset($params['school_id']) ? $params['school_id'] : $school_id_sec;
		$terminal_id = 1; 
		$order_by  = isset($params['arrange_by']) ? $params['arrange_by'] : 'name';
		$criteria = new CDbCriteria;
		$criteria->with = ['school_sec','marks'];
		$criteria->condition = 't.academic_year=:academic_year AND t.status=:status AND marks.academic_year=:academic_year AND marks.terminal_id=:terminal_id AND marks.subject_status=:status';
		$criteria->params=[':academic_year'=>$academic_year, ':terminal_id'=>$terminal_id, ':status'=>1];
		if($school_id){
			$criteria -> condition .=" AND t.school_id=:school_id  AND marks.school_id=:sch_school_id";
			$criteria -> params = array_merge($criteria->params, array(':school_id'=>$school_id, ':sch_school_id'=>$school_id));
		}

		if($order_by=='id')
			$criteria -> order = 't.id ASC';
		if($order_by=='name')
			$criteria -> order = 't.first_name, t.middle_name, t.last_name';
		if($order_by=='symbol_number')
			$criteria -> order = 't.symbol_number ASC';
		$criteria->together = true;
		$result_information = $this->findAll($criteria);
		$student_criteria = new CDbCriteria;
		$student_criteria -> condition = " academic_year=:academic_year AND terminal_id=:terminal_id ";
		$student_criteria -> params = [':academic_year'=>$academic_year, ':terminal_id'=>$terminal_id];
		if($school_id){
			$criteria -> condition .=" AND t.school_id=:school_id  AND marks.school_id=:sch_school_id";
			$criteria -> params = array_merge($criteria->params, array(':school_id'=>$school_id, ':sch_school_id'=>$school_id));
		}


		$attendance_terminal_information= StudentAttendance::model()->findAll($student_criteria);
		if(!empty($attendance_terminal_information)){
			foreach ($attendance_terminal_information as $attendance_info)
				$attendance_[$attendance_info->student_id] = $attendance_info->attendance.'/'.$attendance_info->total_attendannce;
		}

		$attendance_exam_information = StudentExamAttendance::model()->findAll($student_criteria);
		if(!empty($attendance_exam_information)){
			foreach ($attendance_exam_information as $exam_attendance) 
				$exam_attendance_[$exam_attendance->student_id.'-'.$exam_attendance->subject_id] = $exam_attendance->is_present;
		}

		$criteria_ = new CDbCriteria;
		$criteria_->condition = 'academic_year=:academic_year AND terminal_id=:terminal_id ';
		$criteria_->params=[':academic_year'=>$academic_year,':terminal_id'=>$terminal_id];
		if($school_id){
			$criteria_ -> condition .=" AND school_id=:school_id ";
			$criteria_ -> params = array_merge($criteria_->params, array(':school_id'=>$school_id));
		}
		$criteria_->group = 'subject_id';
		$student_subject = MarkObtained::model()->findAll($criteria_);
		$student_information['student'] = $result_information;
		$student_information['attendance'] = $attendance_;
		$student_information['exam_attendance'] = $exam_attendance_;
		$student_information['student_subject'] = $student_subject;
		return $student_information;
	}

	/**
	*student marks ledger to upload in excel
	*/

	public function StudentExcelLedger($params){
        $user_information = UtilityFunctions::UserInformations();
        $school_id_sec = isset($user_information) ? $user_information['school_id'] : 0;
        $school_id = UtilityFunctions::ShowSchool() && !is_numeric($_GET['MarkObtained']['school_id']) ? $school_id_sec : (int) $_GET['MarkObtained']['school_id'];

		$academic_year = isset($params['academic_year']) ? $params['academic_year'] : UtilityFunctions::AcademicYear();
		$order_by  = isset($params['arrange_by']) ? $params['arrange_by'] : 'name';
		$criteria = new CDbCriteria;
		$criteria->condition = 'academic_year=:academic_year AND status=:status AND school_id=:school_id';
		$criteria->params=[':academic_year'=>$academic_year, ':status'=>1, ':school_id'=>$school_id];
		if($order_by=='id')
			$criteria -> order = 't.id ASC';
		if($order_by=='name')
			$criteria -> order = 't.first_name ASC';
		if($order_by=='symbol_number')
			$criteria -> order = 't.symbol_number ASC';
		$criteria->together = true;
		$student = $this->findAll($criteria);
		$criteria_ = new CDbCriteria;
		$criteria_->condition = 'academic_year=:academic_year AND subject_status=:subject_status AND school_id=:school_id ';
		$criteria_->params=[':academic_year'=>$academic_year,':subject_status'=>1,':school_id'=>$school_id];
		$studentMark = MarkObtained::model()->findAll($criteria_);
		$student_information['student'] = $student;
		$student_information['student_marks'] = $studentMark;
		return $student_information;
	}




	public function TerminalResultLedger($params){
		$student_information = $information_detail = array();
        $user_information = UtilityFunctions::UserInformations();
        $school_id_sec = isset($user_information) ? $user_information['school_id'] : 0;
		$academic_year = isset($params['academic_year']) ? $params['academic_year'] : UtilityFunctions::AcademicYear();
		$ledger_of = isset($params['ledger_of']) ? $params['ledger_of'] : 'both'; // theory Or practical
		$result_date = date('Y-m-d');
		$result_ = isset($params['result_']) ? $params['result_'] : 'both'; // grade or numberic
		$school_id = isset($params['school_id']) ? $params['school_id'] : $school_id_sec;
		$order_by  = isset($params['arrange_by']) ? $params['arrange_by'] : 'symbol_number';
		$criteria = new CDbCriteria;
		$criteria->with = ['marks', 'Result'];
		$criteria->condition = 't.academic_year=:academic_year AND t.status=:status AND marks.academic_year=:academic_year AND marks.subject_status=:status AND Result.result_type=:result_type ';
		$criteria->params=[':academic_year'=>$academic_year, ':result_type'=>'terminal', ':status'=>1];
		if($school_id != 0){
			$criteria -> condition .=" AND t.school_id=:school_id ";
			$criteria -> params = array_merge($criteria->params, array(':school_id'=>$school_id));
		}

		if($order_by=='name')
			$criteria -> order = 't.first_name ASC';
		if($order_by=='symbol_number')
			$criteria -> order = 't.symbol_number ASC';
		if($order_by=='student_id')
			$criteria -> order = 't.id ASC';
		if($order_by=='rank')
			$criteria -> order = 'Result.result_status ASC, Result.rank ASC';
		$criteria->together = true;
		$student_information_sec = $this->findAll($criteria);
	  	if(!empty($student_information_sec)){
	  		$i = $total_obatined_ = 0;
            foreach ($student_information_sec as $student) {
                $registration_number = $student->id;
                $student_information[$i]['school_name'] = isset($student) && isset($student->school_sec) ? $student->school_sec->title : 'null';
                $student_information[$i]['school_code'] = isset($student) && isset($student->school_sec) ? $student->school_sec->schole_code : 'null';
                $student_information[$i]['name'] = strtoupper($student->first_name." ".$student->middle_name." ".$student->last_name);
                $student_information[$i]['father_name'] = strtoupper($student->father_name);
                $student_information[$i]['dob_bs'] = $student->dob_nepali;
                $student_information[$i]['dob_ad'] = $student->don_english;
                $student_information[$i]['symbol_number'] = isset($student) ? $student->symbol_number : 'null';
                $marks_information = $student->marks;
                $result_information = isset($student->Result) ? $student->Result : null;
                if(!empty($marks_information)){
                    foreach ($marks_information as $information_) {
                        $subject_name = UtilityFunctions::ColumnName($information_ -> subject_name);
                        $theory_mark = in_array($information_->status, ['th_retake','retake']) ? (int) $information_->theory_mark : (int) $information_->theory_mark;
                        $practical_mark = in_array($information_->status, ['pr_retake','retake']) ? (int) $information_->practical_mark : (int) $information_->practical_mark;
                        $grace_mark = (int) $information_->grace_mark;

                        $theory_grade = in_array($information_->status, ['th_retake','retake']) ? $information_->theory_grade : $information_->theory_grade;
                        $practical_grade = in_array($information_->status, ['pr_retake','retake']) ? $information_->practical_grade : $information_->practical_grade;

                        $total_mark = in_array($information_->status, ['th_retake', 'pr_retake','retake'])? (int) $information_->total_mark : (int) $information_->total_mark;

                        $total_grade =  in_array($information_->status, ['th_retake', 'pr_retake','retake']) ? $information_->total_grade : $information_->total_grade;

                		$theory_title = $information_->is_practical && $information_->is_practical !=0 ? '_theory' :'';
		                $subject_name = UtilityFunctions::ColumnName($information_ -> subject_name);
			           $th_mark = in_array($result_, ['both', 'general']) ? $theory_mark :  $theory_grade; 
			            $pr_mark = in_array($result_, ['both', 'general']) ? $practical_mark :  $practical_grade; 
			            $tt_mark = in_array($result_, ['both', 'general']) ? $total_mark :  $total_grade; 
		                if(in_array($ledger_of, ['theory','both'])){
			                $student_information[$i][$subject_name.$theory_title] = $th_mark;
			                if($result_ == 'both')
			                	$student_information[$i][$subject_name.$theory_title.'_grade'] = $theory_grade;
		                }
		                if($information_->is_practical && $information_->is_practical !=0){
		                	if($ledger_of=='practical'){
			                	$student_information[$i][$subject_name.'_practical'] = $pr_mark;
			                	if($result_ == 'both')
			                		$student_information[$i][$subject_name.'_practical_grade'] = $practical_grade;
			                }
			                if($ledger_of == 'both'){
			                	$student_information[$i][$subject_name.'_practical'] = $pr_mark;
			                	if($result_ == 'both')
			                		$student_information[$i][$subject_name.'_practical_grade'] = $practical_grade;
			                	$student_information[$i][$subject_name.'_total'] = $tt_mark;
			                	if($result_ == 'both')
			                		$student_information[$i][$subject_name.'_total_grade'] = $total_grade;
			                }

		                }
		                if(in_array($result_ ,['both', 'grade']) && $ledger_of == 'both'){
		                	if($information_->is_practical && $information_->is_practical !=0)
			                	$student_information[$i][$subject_name.'_total_gp'] = number_format($information_->total_grade_point, 1);
		                	else
			                	$student_information[$i][$subject_name.'_gp'] = number_format($information_->total_grade_point , 1);
		                }



                        $total_full_marks = $information_->theory_full_mark + $information_->practical_full_mark;
	                    $total_obatined_ += (int) $information_->total_mark;
	                    $total_full_marks += (int) $information_ -> theory_full_mark + (int) $information_ ->  practical_full_mark;

                	}
            	}
	            $division = $grade = $result_status = '';
	            if($result_information && !empty($result_information)){
	                foreach ($result_information as $result) {
	                    $result_date = $result->result_publish_date;
	                    $registration_num = $result->student_id;
	                    $total_gpa = $result->total_gpa;
	                    $total_obatined_ = $result->total_obtained_mark;
	                    $total_full_marks = $result->total_full_mark;
	                    $percentage = number_format($result->percentage,2);
	                    $attendance = $result->attendance;
	                    $total_attendance = $result->total_attendance;
	                    $rank = $result->rank;
	                    $division = $result->division;
	                    $grade = $result->grade;
	                    $result_status = $result->result_status;
	                }
	            }
	            $student_information[$i]['Total'] = $total_obatined_ ;
	            $student_information[$i]['GPA'] = $total_gpa ;
	            $student_information[$i]['Result'] = $result_status;
	            $student_information[$i]['Full_Mark'] = $total_full_marks;
	            $student_information[$i]['Ranks'] = isset($rank) ? $rank : 0;
	            $student_information[$i]['Percentage'] = number_format($percentage,2)." %";
	            $student_information[$i]['Division'] = $division;
	            $i++;
	        }
        }
        $information_detail['student_information'] = $student_information;
        $information_detail['result_date'] = $result_date;
        return $information_detail;
	}




	public function TerminalResultPdF($params){
		$information = array();
        $user_information = UtilityFunctions::UserInformations();
        $school_id_sec = isset($user_information) ? $user_information['school_id'] : 0;
		$attendance_ = $exam_attendance_ = $student_information = array();
		$academic_year = isset($params['academic_year']) ? $params['academic_year'] : UtilityFunctions::AcademicYear();
		if($user_information['super_user']==1)
			$school_id = isset($params['school_id']) ? $params['school_id'] : null;
		else
        	$school_id = isset($user_information) ? $user_information['school_id'] : 808087;
		$terminal_id = isset($params['terminal_id']) ? $params['terminal_id'] : 1; 
		$order_by  = isset($params['order_by']) ? $params['order_by'] : 'name';

		$criteria = new CDbCriteria;
		$criteria->with = ['marks', 'Result'];
		$criteria->condition = 't.academic_year=:academic_year AND t.status=:status AND marks.academic_year=:academic_year AND marks.terminal_id=:terminal_id AND marks.subject_status=:status AND Result.result_type=:result_type AND Result.terminal_id=:terminal_id';
		$criteria->params=[':academic_year'=>$academic_year, ':result_type'=>'terminal', ':terminal_id'=>$terminal_id, ':status'=>1];

		if($school_id){
			$criteria -> condition .=" AND t.school_id=:school_id ";
			$criteria -> params = array_merge($criteria->params, array(':school_id'=>$school_id));
		}
		if($order_by=='name')
			$criteria -> order = 't.first_name ASC';
		if($order_by=='symbol_number')
			$criteria -> order = 't.symbol_number ASC';
		if($order_by=='student_id')
			$criteria -> order = 't.id ASC';
		if($order_by=='rank')
			$criteria -> order = 'Result.result_status ASC, Result.rank ASC';
		$criteria->together = true;
		$result_information = $this->findAll($criteria);



		$criteria_ = new CDbCriteria;
		$criteria_->with = ['marks'];
		$criteria_->condition = 'marks.academic_year=:academic_year AND marks.terminal_id=:terminal_id AND marks.subject_status=:subject_status ';
		$criteria_->params = [':academic_year'=>$academic_year, ':terminal_id'=>$terminal_id, ':subject_status'=>1];
		$criteria_->group = 'subject_id';
		$subject_information = $this->findAll($criteria_);
		$information['result_information'] = $result_information;
		$information['subject_information'] = $subject_information;
		return $information;
	}


	public function CanApporvedStudentList($school_id){
		$academic_year = UtilityFunctions::AcademicYear();
		$userInformation =  UtilityFunctions::UserInformations();
		$role = isset($userInformation['role']) ? $userInformation['role'] :  null;
		$previousRole =  UtilityFunctions::PreviousRole($role);
		$criteria = new CDbCriteria;
		$criteria -> condition = 'state=:state AND state_status!=:state_status AND status=:status AND academic_year=:academic_year';
		$criteria -> params = [':state'=>$role, ':state_status'=>'accepted', ':status'=>1, ':academic_year'=>$academic_year];
		if(!$school_id && !in_array($role, StudentProcessing::APPROVALSTATE)){
			$school = isset($userInformation['school_id']) ? $userInformation['school_id'] :  null;
			$criteria -> condition .= ' AND school_id=:school_id';
			$criteria -> params = array_merge($criteria->params, [':school_id'=>$school]);
		}
		if($previousRole){
			$criteria -> condition .= ' OR (state=:previousRole AND state_status=:state_status)';
			$criteria -> params = array_merge($criteria->params, [':previousRole'=>$previousRole, ':state_status'=>'accepted']);
		}
		$studentInformation = $this->findAll($criteria);
		$studentArray = [];
		if(!empty($studentInformation)){
			foreach ($studentInformation as $student) {
				$name =  "<a href='".Yii::app()->baseUrl."/studentInformation/".$student->id."'>".ucwords($student->first_name.' '.$student->middle_name.' '.$student->last_name)."</a>";
				$fatherMotherName = ucwords($student->father_name).'/'.ucwords($student->mother_name);
				$DOB = $student->dob_nepali.' (B.S)/ '.$student->don_english.' (A.D)';
				$studentArray[] = ['',$student->roll_number,$name, $fatherMotherName, $DOB, $student->sex, $student->created_date];
			}
		}
		return $studentArray;
	}

	public function ApprovedList($school_id = null){
		$academic_year = UtilityFunctions::AcademicYear();
        $user_information = UtilityFunctions::UserInformations();
        $role = isset($user_information) ? $user_information['role'] : 0;
        $stateArray = UtilityFunctions::RoleArray($role);
		$criteria = new CDbCriteria;
		$criteria -> condition = 'academic_year=:academic_year AND status=:status';
		$criteria -> params = [':status'=>1, ':academic_year'=>$academic_year];
		$criteria->addInCondition('state', $stateArray);
		if(!$school_id){
			$school = isset($user_information['school_id']) ? $user_information['school_id'] :  null;
			$criteria -> condition .= ' AND school_id=:school_id';
			$criteria -> params = array_merge($criteria->params, [':school_id'=>$school]);
		}

		$criteria -> condition .= ' OR (state=:state AND state_status=:state_status)';
		$criteria -> params = array_merge($criteria->params, [':state'=>$role, ':state_status'=>'accepted']);
		$studentInformation = $this->findAll($criteria);


		$studentArray = [];
		if(!empty($studentInformation)){
			foreach ($studentInformation as $student) {
				$name =  "<a href='".Yii::app()->baseUrl."/studentInformation/".$student->id."'>".ucwords($student->first_name.' '.$student->middle_name.' '.$student->last_name)."</a>";
				$fatherMotherName = ucwords($student->father_name).'/'.ucwords($student->mother_name);
				$DOB = $student->dob_nepali.' (B.S)/ '.$student->don_english.' (A.D)';
				$studentArray[] = ['',$student->roll_number,$name, $fatherMotherName, $DOB, $student->sex, $student->state, $student->state_status, $student->updated_date];
			}
		}
		return $studentArray;
	}

	public function VerifyStudent($studentData, $id){
		$canApproved = UtilityFunctions::VerifyAuthentication($id);
        $user_information = UtilityFunctions::UserInformations();
        $role = isset($user_information) ? $user_information['role'] : 0;
		if(isset($studentData['state_status']) && in_array($studentData['state_status'], ['processing','accepted','rejected'])){
			$status = $studentData['state_status'];
			$remarks = $studentData['remarks'];
			$studentInformation = $this->findByPk($id);
			$transacation = Yii::app()->db->beginTransaction();
    		$error_log = array();
			if(empty($studentInformation))
				throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
			$studentInformation->state = $role;
			$studentInformation->state_status = $status;
			$studentInformation->remarks = $remarks;
			if(!$studentInformation->validate() || !$studentInformation->update())
				$error_log[] = 'false';
			$model = new StudentProcessing();
			$model->student_id = $id;
			$model->school_id = $studentInformation->school_id;
			$model->state = $studentInformation->state;
			$model->status = $studentInformation->state_status;
			if($model->validate() && $model->save())
				$error_log[] = 'true';
			else
				$error_log[] = 'false';
			if(!in_array('false', $error_log)){
				$transacation->commit();
				return true;
			}
			$transacation->rollback();
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');

		}
		return false;
	}




	public function SchoolWiseStudent(){
		$academic_year = UtilityFunctions::AcademicYear();
		$criteria = new CDbCriteria;
		$criteria -> select = "count(id) as id, school_id";
		$criteria -> condition = 'academic_year=:academic_year AND status=:status AND state=:state AND state_status=:state_status';
		$criteria -> params = [':status'=>1, ':academic_year'=>$academic_year, ':state'=>'result', ':state_status'=>'processing'];
		$criteria -> group ='academic_year,school_id';
		$studentInformation = $this->findAll($criteria);
		$studentArray = [];
		if(!empty($studentInformation)){
			foreach ($studentInformation as $schoolReport) {
				$schoolCode = $schoolReport->school_sec ? $schoolReport->school_sec->schole_code : '';
				$name =  "<a href='".Yii::app()->baseUrl."/basicInformation/".$schoolReport->school_id."'>".ucwords($schoolReport->school_sec ? $schoolReport->school_sec->title : '')."</a>";
				$studentArray[] = ['', $schoolCode, $name, $schoolReport->id];
			}
		}
		return $studentArray;
	}

	public function StudentNumber($academic_year, $school_id){
		$criteria = new CDbCriteria;
		$criteria -> select = "count(id) as id, school_id";
		$criteria -> condition = 'academic_year=:academic_year AND status=:status AND state=:state AND state_status=:state_status AND school_id=:school_id';
		$criteria -> params = [':status'=>1, ':academic_year'=>$academic_year, ':state'=>'result', ':state_status'=>'processing', ':school_id'=>$school_id];
		$criteria -> group ='academic_year,school_id';
		$studentNumber = $this->find($criteria);
		return $studentNumber ? $studentNumber->id : 0;

	}

	/**
	* symbol number register student
	*/
	public function ApprovedStudentList($academic_year, $school_id, $order_by = null){
		$criteria = new CDbCriteria;
		$criteria -> condition = 'academic_year=:academic_year AND status=:status AND state=:state AND state_status=:state_status AND school_id=:school_id';
		$criteria -> params = [':status'=>1, ':academic_year'=>$academic_year, ':state'=>'result', ':state_status'=>'processing', ':school_id'=>$school_id];
		if(!$order_by)
			$criteria -> order = 'first_name, middle_name, last_name';
		else
			$criteria -> order = $order_by;
		$studentList = $this->findAll($criteria);
		return $studentList;

	}

	/**
	* @param int school_id, state, academic_year , status
	* @return list of student section
	*/

	public function StudentList($academic_year, $school_id, $sex, $state , $status, $order_by = null){
		$criteria = new CDbCriteria;
		$criteria -> condition = 'academic_year=:academic_year AND status=:status';
		$criteria -> params = [':status'=>1, ':academic_year'=>$academic_year];
		if($school_id){
			$criteria -> condition .= ' AND school_id=:school_id';
			$criteria -> params = array_merge($criteria -> params, [':school_id'=>$school_id]);
		}
		if($sex){
			$criteria -> condition .= ' AND sex=:sex';
			$criteria -> params = array_merge($criteria -> params, [':sex'=>$sex]);
		}
		if($state){
			$criteria -> condition .= ' AND state=:state';
			$criteria -> params = array_merge($criteria -> params, [':state'=>$state]);
		}
		if($status){
			$criteria -> condition .= ' AND state_status=:state_status';
			$criteria -> params = array_merge($criteria -> params, [':state_status'=>$status]);
		}
		$criteria -> order = 'roll_number,id ASC';
		if($order_by && $order_by=='symbol_number')
			$criteria -> order = 'symbol_number ASC';
		if($order_by && $order_by=='updated_date')
			$criteria -> order = 't.updated_date ASC';
		if($order_by && $order_by=='first_name')
			$criteria -> order = 'first_name, middle_name, last_name ASC';
		if($order_by && $order_by=='registration_number')
			$criteria -> order = 't.registration_number ASC';
		$studentList = $this->findAll($criteria);
		return $studentList;

	}
	/**
	*generating student symbol number
	*/
	public function GenerateStudentSymbol($schoolRegistrationInformaiton, $studentArray){
		$error_array = $symbol_number = [];
		$transacation = Yii::app()->db->beginTransaction();
		$schoolInformation = BasicInformation::model()->findByPk($schoolRegistrationInformaiton->school_id);
		$schoolCode = $schoolInformation ? $schoolInformation->schole_code : '';
		$minimum_range = $schoolRegistrationInformaiton && $schoolRegistrationInformaiton->start_point > 0  ? $schoolRegistrationInformaiton->start_point : 1;
		$maximum_range = $schoolRegistrationInformaiton ? $schoolRegistrationInformaiton->end_point : 9999;
		for ($i=0; $i < sizeof($studentArray) ; $i++) {
			$generalNumber = $minimum_range + $i;
			$studentId = $studentArray[$i];
			$studentInformation = $this->findByPk($studentId);
			if(!empty($studentInformation)){
				$symbolNumber = UtilityFunctions::SymbolNumber($studentInformation->academic_year, $generalNumber);

				$registration_number = UtilityFunctions::StudentExamRegistrationNumber($studentInformation->academic_year, Yii::app()->params['vdcCode'], $schoolCode ,$i+1);
				$symbol_number[] = $symbolNumber;
				$studentInformation->roll_number = $i+1;
				$studentInformation->symbol_number = $symbolNumber;
				$studentInformation->registration_number = $registration_number;
				$studentInformation->state = 'result';
				$studentInformation->state_status = 'processing';
				if($studentInformation->update() && $studentInformation->update())
					$error_array[] = 'true';
				else
					$error_array[] = 'false';
			}
		}
		if(!in_array('false', $error_array)){
			$transacation->commit();
			return true;
		}
		$transacation->rollback();
		return false;
	}

	public function SchoolWiseNumber($academic_year){

		/*$student_array[0]['label'] = 'School';
		$student_array[0]['y'] = 12;*/

		$criteria = new CDbCriteria;
		$criteria -> with = ['school_sec'];
		$criteria -> select = "count(first_name) as id, school_sec.title as middle_name";
		$criteria -> condition = 'academic_year=:academic_year AND t.status=:status';
		$criteria -> params = [':status'=>1, ':academic_year'=>$academic_year];
		$criteria -> group ='academic_year,school_id';
		$studentNumber = $this->findAll($criteria);
		$student_array = [];
		if(!empty($studentNumber)){
			$sn = 0;
			foreach ($studentNumber as $information) {
				$student_array[$sn]['label'] = $information->middle_name;
				$student_array[$sn]['y'] = $information->id;
				$sn++;
			}
		}
		if(empty($student_array)){
			$student_array[0]['label'] = 'SCHOOL WISE STUDENT NUMBER';
			$student_array[0]['y'] = 100;
		}
		return $student_array;
	}

	/**
	* insert student by excel
	*/
	public function InsertStudentList($params){
    	$school_id = $params['school_id'];
    	$symbol_number_array = $params['symbol_number'];
		$academic_year = UtilityFunctions::AcademicYear();
    	$column = ['registration_number','first_name','middle_name','last_name','sex','father_name','mother_name','dob_bs','dob_ad','optional_subject','permanent_address','enroll_class','enroll_year'];
		$transacation = Yii::app()->db->beginTransaction();
    	for ($i=0; $i < sizeof($symbol_number_array); $i++) {
    		$symbol_number = $symbol_number_array[$i];
    		$student_information = $this->find('symbol_number=:symbol_number AND status=:status',[':symbol_number'=>$symbol_number, ':status'=>1]);
    		$registration_number = isset($params[$symbol_number.'_registration_number']) ? $params[$symbol_number.'_registration_number'] : null;
    		$first_name = isset($params[$symbol_number.'_first_name']) ? $params[$symbol_number.'_first_name'] : null;
    		$middle_name = isset($params[$symbol_number.'_middle_name']) ? $params[$symbol_number.'_middle_name'] : null;
    		$last_name = isset($params[$symbol_number.'_last_name']) ? $params[$symbol_number.'_last_name'] : null;
    		$sex = isset($params[$symbol_number.'_sex']) ? $params[$symbol_number.'_sex'] : null;
    		$father_name = isset($params[$symbol_number.'_father_name']) ? $params[$symbol_number.'_father_name'] : null;
    		$mother_name = isset($params[$symbol_number.'_mother_name']) ? $params[$symbol_number.'_mother_name'] : null;
    		$dob_bs = isset($params[$symbol_number.'_dob_bs']) ? $params[$symbol_number.'_dob_bs'] : null;
    		$dob_ad = isset($params[$symbol_number.'_dob_ad']) ? $params[$symbol_number.'_dob_ad'] : null;
    		$optional_subject = isset($params[$symbol_number.'_optional_subject']) ? $params[$symbol_number.'_optional_subject'] : null;
    		$permanent_address = isset($params[$symbol_number.'_permanent_address']) ? $params[$symbol_number.'_permanent_address'] : null;
    		$enroll_class = isset($params[$symbol_number.'_enroll_class']) ? $params[$symbol_number.'_enroll_class'] : null;
    		$enroll_year = isset($params[$symbol_number.'_enroll_year']) ? $params[$symbol_number.'_enroll_year'] : null;
    		if(!empty($student_information)){
    			$student_information->academic_year = $academic_year;
    			$student_information->registration_number = $registration_number;
    			$student_information->symbol_number = $symbol_number;
    			$student_information->first_name = strtolower($first_name);
    			$student_information->middle_name = strtolower($middle_name);
    			$student_information->last_name = strtolower($last_name);
    			$student_information->sex = strtolower($sex);
    			$student_information->father_name = strtolower($father_name);
    			$student_information->mother_name = strtolower($mother_name);
    			$student_information->dob_nepali = $dob_bs;
    			$student_information->don_english = $dob_ad;
    			$student_information->optional_subject = strtolower($optional_subject);
    			$student_information->permanent_location = strtolower($permanent_address);
    			$student_information->registered_class = strtolower($enroll_class);
    			$student_information->registered_year = $enroll_year;
    			$student_information->school_id = $school_id;
    			$student_information->status = 1;
				$student_information->updated_by = Yii::app()->user->id;
				$student_information->updated_date = new CDbExpression('now()');
    			if(!$student_information->update(false)){
    				echo "<pre>";
    				echo print_r($student_information->errors);
    				exit;
    				$transacation->rollback();
					throw new CHttpException(400,'Invalid request. Please do not repeat this request again, update.');
    			}

    		}else{
    			$model = new StudentInformation();
    			$model->academic_year = $academic_year;
    			$model->registration_number = $registration_number;
    			$model->symbol_number = $symbol_number;
    			$model->first_name = strtolower($first_name);
    			$model->middle_name = strtolower($middle_name);
    			$model->last_name = strtolower($last_name);
    			$model->sex = strtolower($sex);
    			$model->father_name = strtolower($father_name);
    			$model->mother_name = strtolower($mother_name);
    			$model->optional_subject = strtolower($optional_subject);
    			$model->permanent_location = strtolower($permanent_address);
    			$model->registered_class = strtolower($enroll_class);
    			$model->registered_year = $enroll_year;
    			$model->dob_nepali = $dob_bs;
    			$model->don_english = $dob_ad;
    			$model->school_id = $school_id;
				$model->created_by = Yii::app()->user->id;
				$model->created_date = new CDbExpression('now()');
    			$model->status = 1;
    			if(!$model->save(false)){
    				echo "<pre>";
    				echo print_r($model->errors);
    				exit;
    				$transacation->rollback();
					throw new CHttpException(400,'Invalid request. Please do not repeat this request again, save.');
    			}
    		}
    	}
    	$transacation->commit();
    	return true;

	}

	public function GenderWiseStudent($academic_year){
        $criteria = new CDbCriteria;
        //$criteria -> 
		$criteria -> select = "sum(case when sex = 'male' then 1 else 0 end) as male_number,
			sum(case when sex = 'female' then 1 else 0 end) as female_number";
        $criteria -> condition = 'academic_year=:academic_year AND status=:status';
        $criteria -> params = ['academic_year'=>$academic_year, ':status'=>1];
        $criteria -> group = 'academic_year';
        $register_student_information = StudentInformation::model()->find($criteria);
        return $register_student_information;
	}


}
