<?php

/**
 * This is the model class for table "mark_obtained_history".
 *
 * The followings are the available columns in table 'mark_obtained_history':
 * @property integer $id
 * @property integer $mark_obtained_id
 * @property string $grace_mark
 * @property string $theory_mark
 * @property string $practical_mark
 * @property string $cas_mark
 * @property string $theory_grade
 * @property string $practical_grade
 * @property string $cas_grade
 * @property string $cas
 * @property string $total_mark
 * @property string $total_grade_point
 * @property string $total_grade
 * @property integer $retake_th_number
 * @property integer $retake_pr_number
 * @property integer $created_by
 * @property string $created_date
 */
class MarkObtainedHistory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mark_obtained_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('grace_mark, theory_mark, practical_mark, cas_mark, theory_grade, total_grade_point, total_grade', 'required'),
			array('mark_obtained_id, retake_th_number, retake_pr_number, created_by', 'numerical', 'integerOnly'=>true),
			array('grace_mark, theory_mark, practical_mark, cas_mark', 'length', 'max'=>6),
			array('theory_grade, practical_grade, cas_grade, total_grade', 'length', 'max'=>20),
			array('cas', 'length', 'max'=>100),
			array('total_mark', 'length', 'max'=>6),
			array('total_grade_point', 'length', 'max'=>6),
			array('created_date', 'safe'),
						
			//html purification
            array('id, mark_obtained_id, grace_mark, theory_mark, practical_mark, cas_mark, theory_grade, practical_grade, cas_grade, cas, total_mark, total_grade_point, total_grade, retake_th_number, retake_pr_number, created_by, created_date', 'filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, mark_obtained_id, grace_mark, theory_mark, practical_mark, cas_mark, theory_grade, practical_grade, cas_grade, cas, total_mark, total_grade_point, total_grade, retake_th_number, retake_pr_number, created_by, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'mark_obtained_id' => 'Mark Obtained',
			'grace_mark' => 'Grace Mark',
			'theory_mark' => 'Theory Mark',
			'practical_mark' => 'Practical Mark',
			'cas_mark' => 'Cas Mark',
			'theory_grade' => 'Theory Grade',
			'practical_grade' => 'Practical Grade',
			'cas_grade' => 'Cas Grade',
			'cas' => 'Cas',
			'total_mark' => 'Total Mark',
			'total_grade_point' => 'Total Grade Point',
			'total_grade' => 'Total Grade',
			'retake_th_number' => 'Retake Th Number',
			'retake_pr_number' => 'Retake Pr Number',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('mark_obtained_id',$this->mark_obtained_id);
		$criteria->compare('grace_mark',$this->grace_mark,true);
		$criteria->compare('theory_mark',$this->theory_mark,true);
		$criteria->compare('practical_mark',$this->practical_mark,true);
		$criteria->compare('cas_mark',$this->cas_mark,true);
		$criteria->compare('theory_grade',$this->theory_grade,true);
		$criteria->compare('practical_grade',$this->practical_grade,true);
		$criteria->compare('cas_grade',$this->cas_grade,true);
		$criteria->compare('cas',$this->cas,true);
		$criteria->compare('total_mark',$this->total_mark,true);
		$criteria->compare('total_grade_point',$this->total_grade_point,true);
		$criteria->compare('total_grade',$this->total_grade,true);
		$criteria->compare('retake_th_number',$this->retake_th_number);
		$criteria->compare('retake_pr_number',$this->retake_pr_number);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MarkObtainedHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	
}
