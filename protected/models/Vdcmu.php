<?php

/**
 * This is the model class for table "vdcmu".
 *
 * The followings are the available columns in table 'vdcmu':
 * @property integer $id
 * @property integer $d_id
 * @property string $vdc_eng
 * @property string $vdc_nep
 * @property integer $ward
 */
class Vdcmu extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vdcmu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('d_id, vdc_eng, vdc_nep, ward', 'required'),
			array('d_id, ward', 'numerical', 'integerOnly'=>true),
			array('vdc_eng, vdc_nep', 'length', 'max'=>200),
			//html purification
         	array('id, d_id, vdc_eng, vdc_nep, ward', 'filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, d_id, vdc_eng, vdc_nep, ward', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'd_id' => 'D',
			'vdc_eng' => 'Vdc Eng',
			'vdc_nep' => 'Vdc Nep',
			'ward' => 'Ward',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('d_id',$this->d_id);
		$criteria->compare('vdc_eng',$this->vdc_eng,true);
		$criteria->compare('vdc_nep',$this->vdc_nep,true);
		$criteria->compare('ward',$this->ward);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Vdcmu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
