<?php

/**
 * This is the model class for table "result_setting".
 *
 * The followings are the available columns in table 'result_setting':
 * @property integer $id
 * @property integer $academic_year
 * @property integer $school_id
 * @property integer $all_passed
 * @property integer $min_range_all_pass
 * @property string $selected_fail
 * @property string $selected_passed
 * @property integer $is_used
 * @property integer $created_by
 * @property string $created_date
 */
class ResultSetting extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'result_setting';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('academic_year, is_used', 'required'),
			array('academic_year, school_id, all_passed, min_range_all_pass, is_used, created_by', 'numerical', 'integerOnly'=>true),
			array('selected_fail, selected_passed, created_date', 'safe'),
			//html purification
            array('id, academic_year, school_id, all_passed, min_range_all_pass, selected_fail, selected_passed, is_used, created_by, created_date', 'filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, academic_year, school_id, all_passed, min_range_all_pass, selected_fail, selected_passed, is_used, created_by, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'academic_year' => 'Academic Year',
			'school_id' => 'School',
			'all_passed' => 'All Passed',
			'min_range_all_pass' => 'Min Range All Pass',
			'selected_fail' => 'Selected Fail',
			'selected_passed' => 'Selected Passed',
			'is_used' => 'Is Used',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('academic_year',$this->academic_year);
		$criteria->compare('school_id',$this->school_id);
		$criteria->compare('all_passed',$this->all_passed);
		$criteria->compare('min_range_all_pass',$this->min_range_all_pass);
		$criteria->compare('selected_fail',$this->selected_fail,true);
		$criteria->compare('selected_passed',$this->selected_passed,true);
		$criteria->compare('is_used',$this->is_used);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ResultSetting the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
