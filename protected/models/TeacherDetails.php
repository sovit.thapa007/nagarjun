<?php

/**
 * This is the model class for table "teacher_details".
 *
 * The followings are the available columns in table 'teacher_details':
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property integer $mobile_number
 * @property string $sex
 * @property integer $school_id
 * @property integer $created_by
 * @property string $created_date
 */
class TeacherDetails extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'teacher_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('mobile_number, school_id, created_by', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>200),
			array('address', 'length', 'max'=>250),
			array('sex', 'length', 'max'=>6),
			array('created_date', 'safe'),
			//html purification
            array('id, name, address, mobile_number, sex, school_id, created_by, created_date', 'filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, address, mobile_number, sex, school_id, created_by, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'school_sec' => array(self::BELONGS_TO, 'BasicInformation', 'school_id'),
		);
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'address' => 'Address',
			'mobile_number' => 'Mobile Number',
			'sex' => 'Sex',
			'school_id' => 'School',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$user_id = Yii::app()->user->id;
		$user_informaiton = User::model()->findByPk($user_id);

		
		$criteria=new CDbCriteria;
		$criteria -> with = ['school_sec'];
		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('mobile_number',$this->mobile_number);
		$criteria->compare('sex',$this->sex,true);
		if($user_informaiton->superuser == 1)
			$criteria->compare('school_id',$this->school_id);
		else
			$criteria->compare('school_id',$user_informaiton->school_id);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TeacherDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	*before validation case */
	public function beforeValidate()
	{
		$user_id = Yii::app()->user->id;
		$user_informaiton = User::model()->findByPk($user_id);
		$this->school_id = !empty($user_informaiton) ? $user_informaiton->school_id : null;
		if($this->isNewRecord){
			$this->created_by = Yii::app()->user->id;
			$this->created_date = new CDbExpression('now()');	
		}
		return parent::beforeValidate();
	}


}
