<?php

/**
 * This is the model class for table "student_subject".
 *
 * The followings are the available columns in table 'student_subject':
 * @property integer $id
 * @property integer $student_id
 * @property integer $subject_id
 * @property string $nepali_date
 * @property integer $subject_nature
 * @property string $status
 * @property integer $created_by
 * @property string $created_date
 */
class StudentSubject extends CActiveRecord
{

	public $school_id;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'student_subject';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('student_id, subject_id, subject_nature', 'required'),
			array('student_id, subject_id, subject_nature, created_by', 'numerical', 'integerOnly'=>true),
			array('status', 'length', 'max'=>9),
			array('created_date', 'safe'),
			//html purification
            array('id, student_id, subject_id, subject_nature, status, created_by, created_date', 'filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, student_id, subject_id, subject_nature, status, created_by, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'student_id' => 'Student',
			'subject_id' => 'Subject',
			'subject_nature' => 'Subject Nature',
			'status' => 'Status',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('student_id',$this->student_id);
		$criteria->compare('subject_id',$this->subject_id);
		$criteria->compare('nepali_date',$this->nepali_date,true);
		$criteria->compare('subject_nature',$this->subject_nature);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StudentSubject the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	*/
	public function StudentOptionalSubject($secondary_optional_subject, $student_list){
		$subject_id = $secondary_optional_subject ?  $secondary_optional_subject->id :  null;
		$student_array = [];
		if(!empty($student_list)){
			foreach ($student_list as $student_)
				$student_array[] = $student_->id;
		}
		$criteria = new CDbCriteria;
		$criteria ->  condition = 'status=:status AND subject_nature=:subject_nature AND subject_id=:subject_id';
		$criteria -> params = [':status'=>'active', ':subject_nature'=>3, ':subject_id'=>$subject_id];
		if($student_array){
			$criteria -> addInCondition('student_id', $student_array);
		}
		$optional_subjects = $this->findAll($criteria);
		$student_optional_subject = [];
		if(!empty($optional_subjects)){
			foreach ($optional_subjects as $opt_subject_)
				$student_optional_subject[$opt_subject_->student_id] = $opt_subject_->subject_id;
		}
		return $student_optional_subject;
	}
	/**
	* add secondary optional subject
	*/
	public function SaveSecondaryOptionalSubject($params){
		$school_id = isset($params['school_id']) ? $params['school_id'] : null;
		$student_array = isset($params['student_id_']) ? $params['student_id_'] :  null;
  		$transaction = Yii::app()->db->beginTransaction();
  		for ($i=0; $i < sizeof($student_array) ; $i++) {
  			$student_id =  $student_array[$i];
  			$optional_subject = isset($_POST['_choose_optional_secondary_'.$student_id]) ? $_POST['_choose_optional_secondary_'.$student_id] : null;
  			$is_optional_secondary_subject = SubjectInformation::model()->find('id=:subject_id AND school_id=:school_id AND subject_nature=:subject_nature AND default_optional!=:default_optional',[':subject_id'=>$optional_subject,':school_id'=>$school_id, ':subject_nature'=>3, ':default_optional'=>1]);
  			$student_subject_section = $this->find('student_id=:student_id AND status=:status',[':student_id' => $student_id, ':status'=>'active']);
  			if(!empty($student_subject_section) && !$is_optional_secondary_subject){
  				$student_subject_section -> status = 'in_active';
				if(!$student_subject_section->validate() &&  !$student_subject_section -> update()){
					$transaction->rollback();
					echo "<pre>";
					echo print_r($student_subject_section ->  errors());
					exit;
				}
  			}
  			$secondary_optional = $this->find('student_id=:student_id AND subject_id=:subject_id AND status=:status',[':student_id' => $student_id,':subject_id'=>$optional_subject, ':status'=>'active']);
  			if(empty($secondary_optional) && $is_optional_secondary_subject){
  				$secondary_opt_subject = new StudentSubject();
  				$secondary_opt_subject -> student_id = $student_id;
  				$secondary_opt_subject -> subject_id = $optional_subject;
  				$secondary_opt_subject -> subject_nature = 3;
				$secondary_opt_subject -> created_by = Yii::app()->user->id;
				$secondary_opt_subject -> created_date = new CDbExpression('NOW()');
				if(!$secondary_opt_subject->validate() && !$secondary_opt_subject->save()){
					$transaction->rollback();
					echo "<pre>";
					echo print_r($secondary_opt_subject ->  errors());
					exit;
				}
  			}
  		}
  		$transaction->commit();
  		return true;
			
	}
}
