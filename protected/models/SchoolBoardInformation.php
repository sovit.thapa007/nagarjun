<?php

/**
 * This is the model class for table "school_board_information".
 *
 * The followings are the available columns in table 'school_board_information':
 * @property integer $id
 * @property string $name
 * @property string $post
 * @property integer $mobile_number
 * @property string $phone_number
 * @property string $email
 * @property integer $ward_no
 * @property integer $photo
 * @property integer $school_id
 * @property integer $status
 * @property integer $created_by
 * @property string $created_at
 */
class SchoolBoardInformation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'school_board_information';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, post, school_id, created_by, created_at', 'required'),
			array('mobile_number, school_id, status, created_by, user_id', 'numerical', 'integerOnly'=>true),
			array('name, signature, permanent_address,temporary_address', 'length', 'max'=>200),
			array('phone_number', 'length', 'max'=>20),
			array('email', 'length', 'max'=>100),
			//html purification
            array('id, name, post, mobile_number, phone_number, email, temporary_address, permanent_address, photo, school_id, status, created_by, created_at, user_id', 'filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, post, mobile_number, phone_number, email, temporary_address, permanent_address, photo, school_id, status, created_by, created_at, user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'school' => array(self::BELONGS_TO, 'BasicInformation', 'school_id'),
		);
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'post' => 'Post',
			'mobile_number' => 'Mobile Number',
			'phone_number' => 'Phone Number',
			'email' => 'Email',
			'permanent_address' => 'Permanent Address',
			'temporary_address' => 'Temporary Address',
			'photo' => 'Photo',
			'signature' => 'Signature',
			'school_id' => 'School',
			'user_id' => 'User',
			'status' => 'Status',
			'created_by' => 'Created By',
			'created_at' => 'Created At',
		);
	}


	/**
	*before validation case */
	public function beforeValidate()
	{
		$user_ = UtilityFunctions::UserInformations();
		$this->school_id = $this->school_id ? $this->school_id : $user_['school_id'];
		if($this->isNewRecord){
			$this->created_by = Yii::app()->user->id;
			$this->created_at = new CDbExpression('now()');
			$this->status = 1;	
		}
		$nameInformation = UtilityFunctions::seoUrl($this->name);
		$uploadedImage = CUploadedFile::getInstance($this, 'photo');
		if (!empty($uploadedImage)) {
			$fileName = 'photo_'.$nameInformation.'_'.$this->post.'_'.$this->school_id.'.'.$uploadedImage->getExtensionName();
			$this->photo = $fileName;
			$strPath = 'images/school_document/' . $fileName;
			CUploadedFile::getInstance($this, 'photo')->saveAs($strPath, true);
		}
		$signatureImage = CUploadedFile::getInstance($this, 'signature');
		if (!empty($signatureImage)) {
			$signatureName = 'signature_'.$nameInformation.'_'.$this->post.'_'.$this->school_id.'.'.$signatureImage->getExtensionName();
			$this->signature = $signatureName;
			$sigstrPath = 'images/school_document/signature/' . $signatureName;
			CUploadedFile::getInstance($this, 'signature')->saveAs($sigstrPath, true);
		}
		if(!$this->isNewRecord){
			$previousInformation = $this->findByPk($this->id);
			if(empty($uploadedImage) && !empty($previousInformation))
				$this->photo = $previousInformation->photo;
			if(empty($signatureImage) && !empty($previousInformation))
				$this->signature = $previousInformation->signature;
		}
		return parent::beforeValidate();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('post',$this->post,true);
		$criteria->compare('mobile_number',$this->mobile_number);
		$criteria->compare('phone_number',$this->phone_number,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('permanent_address',$this->permanent_address, true);
		$criteria->compare('temporary_address',$this->temporary_address,true);
		$criteria->compare('photo',$this->photo);
		$criteria->compare('signature',$this->signature);
		$criteria->compare('school_id',$this->school_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SchoolBoardInformation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}



	/**
	*/
	public function DatatableData($school_id = null){
		$criteria = new CDbCriteria;
		$criteria->with = ['school'];
		if($school_id){
			$criteria -> condition = "school_id=:school_id";
			$criteria -> params = [':school_id'=>(int) $school_id];
		}
		$boardInformation = $this->findAll($criteria);
		$reportData = [];
		if(!empty($boardInformation)){
			$sn = 1;
			foreach ($boardInformation as $dataInfo) {
				$schoolName = $dataInfo->school ? $dataInfo->school->title : '';
				$reportData[] = [$sn, $dataInfo->post,  $dataInfo->name, $dataInfo->mobile_number.'(Ph.'.$dataInfo->phone_number.')', $dataInfo->email, $schoolName];
				$sn++;
			}
		}
		$reportDataArray = ["data"=>$reportData];
		return $reportDataArray;
	}

	/**
	*
	*/
	public function BoadMemberList($school_type, $post){
		$criteria = new CDbCriteria();
		$criteria -> with = ['school'];
		$criteria -> condition = 't.status=:status AND school.status=:status';
		$criteria -> params = [':status'=>1];
		$criteria -> order = 't.name ASC';
		if($post)
			$criteria->addInCondition('t.post', $post);
		if($school_type)
		$criteria->addInCondition('school.type', $school_type);
		$criteria -> order = 'school.type, t.name ASC';
		$boardMember = $this->findAll($criteria);
		return $boardMember;

	}
}
