<?php

/**
 * This is the model class for table "program_details".
 *
 * The followings are the available columns in table 'program_details':
 * @property integer $id
 * @property integer $shoollevel_id
 * @property string $title
 * @property string $program_type
 * @property integer $duration
 * @property integer $status
 * @property string $nepali_date
 * @property string $created_date
 * @property integer $created_by
 */
class ProgramDetails extends CActiveRecord
{

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'program_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('shoollevel_id, title, duration', 'required'),
			array('shoollevel_id, duration, status, created_by', 'numerical', 'integerOnly' => true),
			array('title', 'length', 'max' => 250),
			array('program_type', 'length', 'max' => 9),
			array('nepali_date', 'length', 'max' => 20),
			array('created_date, nepali_date', 'safe'),
			//html purification
            array('id, shoollevel_id, title, program_type, duration, status, nepali_date, created_date, created_by', 'filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, shoollevel_id, title, program_type, duration, status, nepali_date, created_date, created_by', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'school_level' => array(self::BELONGS_TO, 'SchoolLevel', 'shoollevel_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'shoollevel_id' => 'Shoollevel',
			'title' => 'Title',
			'program_type' => 'Program Type',
			'duration' => 'Duration',
			'status' => 'Status',
			'nepali_date' => 'Nepali Date',
			'created_date' => 'Created Date',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('school_level.title', $this->shoollevel_id, true);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('program_type', $this->program_type, true);
		$criteria->compare('duration', $this->duration);
		$criteria->compare('status', $this->status);
		$criteria->compare('nepali_date', $this->nepali_date, true);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('created_by', $this->created_by);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProgramDetails the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getConcatened()
	{
		return $this->title . ',' . $this->program_type . ',' . $this->duration;
	}

}
