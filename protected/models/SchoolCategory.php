<?php

/**
 * This is the model class for table "school_category".
 *
 * The followings are the available columns in table 'school_category':
 * @property integer $id
 * @property string $title
 * @property string $type_code
 * @property string $remarks
 * @property string $nepali_date
 * @property string $created_date
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property SchoolLevel[] $schoolLevels
 */
class SchoolCategory extends CActiveRecord
{

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'school_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, type_code', 'required'),
			array('status', 'numerical', 'integerOnly' => true),
			array('title', 'length', 'max' => 250),
			array('type_code, nepali_date', 'length', 'max' => 20),
			array('remarks, created_date', 'safe'),
			//html purification
            array('id, title, type_code, remarks, nepali_date, created_date, status', 'filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, type_code, remarks, nepali_date, created_date, status', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'schoolLevels' => array(self::HAS_MANY, 'SchoolLevel', 'school_category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'type_code' => 'Type Code',
			'remarks' => 'Remarks',
			'nepali_date' => 'Nepali Date',
			'created_date' => 'Created Date',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('type_code', $this->type_code, true);
		$criteria->compare('remarks', $this->remarks, true);
		$criteria->compare('nepali_date', $this->nepali_date, true);
		$criteria->compare('created_date', $this->created_date, true);
		$criteria->compare('status', $this->status);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SchoolCategory the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function beforeValidate()
	{
		$this->nepali_date = Yii::app()->session['today_nepali_date'];
		$this->created_date = new CDbExpression('NOW()');
		return parent::beforeValidate();
	}

}
