<?php

/**
 * This is the model class for table "class_section".
 *
 * The followings are the available columns in table 'class_section':
 * @property integer $id
 * @property string $title
 * @property integer $neapli_year
 * @property string $description
 * @property integer $capacity
 * @property string $current_status
 * @property integer $school_id
 * @property integer $created_by
 * @property string $created_date
 */
class ClassSection extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'class_section';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, neapli_year, school_id', 'required'),
			array('neapli_year, capacity, school_id, created_by', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>150),
			array('current_status', 'length', 'max'=>6),
			array('description, created_date', 'safe'),
			//html purification
            array('id, title, neapli_year, description, capacity, current_status, school_id, created_by, created_date', 'filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, neapli_year, description, capacity, current_status, school_id, created_by, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'school_sec' => array(self::BELONGS_TO, 'BasicInformation', 'school_id'),
			'user_sec' => array(self::BELONGS_TO, 'User', 'created_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'neapli_year' => 'Neapl Year',
			'description' => 'Description',
			'capacity' => 'Capacity',
			'current_status' => 'Current Status',
			'school_id' => 'School',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('neapli_year',$this->neapli_year);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('capacity',$this->capacity);
		$criteria->compare('current_status',$this->current_status,true);
		$criteria->compare('school_id',$this->school_id);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ClassSection the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	*before validation case */
	public function beforeValidate()
	{
		

		$user_id = Yii::app()->user->id;
		$user_informaiton = User::model()->findByPk($user_id);
		$this->school_id = !empty($user_informaiton) ? $user_informaiton->school_id : null;
		if($this->isNewRecord){
			$this->created_by = Yii::app()->user->id;
			$this->created_date = new CDbExpression('now()');	
		}
		return parent::beforeValidate();
	}
}
