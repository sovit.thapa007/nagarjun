<?php

/**
 * This is the model class for table "pass_mark".
 *
 * The followings are the available columns in table 'pass_mark':
 * @property integer $id
 * @property integer $subject_id
 * @property integer $theory_pass_mark
 * @property integer $practical_pass_mark
 * @property integer $theory_full_mark
 * @property integer $practical_full_mark
 * @property integer $is_practical
 * @property integer $status
 * @property string $remarks
 */
class PassMark extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pass_mark';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('subject_id, theory_pass_mark, theory_full_mark, remarks', 'required'),
			array('subject_id, theory_pass_mark, practical_pass_mark, theory_full_mark, practical_full_mark, is_practical, status', 'numerical', 'integerOnly'=>true),
			array('remarks', 'length', 'max'=>200),
			//html purification
            array('id, subject_id, theory_pass_mark, practical_pass_mark, theory_full_mark, practical_full_mark, is_practical, status, remarks', 'filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, subject_id, theory_pass_mark, practical_pass_mark, theory_full_mark, practical_full_mark, is_practical, status, remarks', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'subject_information' => array(self::BELONGS_TO, 'SubjectInformation', 'subject_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'subject_id' => 'Subject',
			'theory_pass_mark' => 'Theory Pass Mark',
			'practical_pass_mark' => 'Practical Pass Mark',
			'theory_full_mark' => 'Theory Full Mark',
			'practical_full_mark' => 'Practical Full Mark',
			'is_practical' => 'Is Practical',
			'status' => 'Status',
			'remarks' => 'Remarks',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria -> with = ['subject_information'];

		$criteria->compare('id',$this->id);
		$criteria->compare('subject_information.title',$this->subject_id, true);
		$criteria->compare('theory_pass_mark',$this->theory_pass_mark);
		$criteria->compare('practical_pass_mark',$this->practical_pass_mark);
		$criteria->compare('theory_full_mark',$this->theory_full_mark);
		$criteria->compare('practical_full_mark',$this->practical_full_mark);
		$criteria->compare('is_practical',$this->is_practical);
		$criteria->compare('status',$this->status);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->together = true;
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PassMark the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	*subject information especially for practical section
	*/

	public function SubjectColumn($type, $school_id){
		$school_information = BasicInformation::model()->findByPk($school_id);
		$criteria = new CDbCriteria;
		$criteria -> with = ['subject_information'];
		$criteria -> condition = 't.status=:status AND subject_information.status=:subject_status';
		$criteria -> params = [':status'=>1, ':subject_status'=>1];
		if($type == 'practical'){
			$criteria -> condition .= ' AND t.is_practical=:is_practical';
			$criteria -> params = array_merge($criteria->params, [':is_practical'=>1]);
		}else{
			$criteria -> condition .= ' AND t.is_practical!=:is_practical';
			$criteria -> params = array_merge($criteria->params, [':is_practical'=>1]);
		}

		if(!empty($school_information) && $school_information->subject_type != 'general'){
			$school_wise_subject = SchoolSubject::model()->findAll('school_id=:school_id AND status=:status',[':school_id'=>$school_id,':status'=>1]);
			$subject_id_array = [];
			if(!empty($school_wise_subject)){
				foreach ($school_wise_subject as $subject_)
					$subject_id_array[] = $subject_->subject_id;
			}
			$criteria->addInCondition('t.subject_id',$subject_id_array);
		}else{
			$criteria->condition .= ' AND subject_type=:subject_type ';
			$criteria->params = array_merge($criteria->params, [':subject_type'=>'general']);
		}

		$criteria -> order = 'subject_information.subject_order ASC';
		$subjectInfor = $this->findAll($criteria);
		$subject_array = [];
		if(!empty($subjectInfor)){
			$sn = 0;
			foreach ($subjectInfor as $subjectfp) {
				if($type == 'practical')
					$pm_fm = (int) $subjectfp->practical_pass_mark.'/'.$subjectfp->practical_full_mark;
				else
					$pm_fm = (int) $subjectfp->theory_pass_mark.'/'.$subjectfp->theory_full_mark;
				$compulsary_name = $subjectfp->subject_information ? ucwords($subjectfp->subject_information->code) : '';
				if($subjectfp->subject_information && !in_array($subjectfp->subject_information->subject_nature, [2,3])){
					$subject_array[$sn]['subject_id'] = $subjectfp->subject_id;
					$subject_array[$sn]['subject_name'] = $compulsary_name;
					$subject_array[$sn]['subject_nature'] = $subjectfp->subject_information ? $subjectfp->subject_information->subject_nature : 1;
					$subject_array[$sn]['pm/fm'] = $pm_fm;
					$sn++;
				}
				if($subjectfp->subject_information && in_array($subjectfp->subject_information->subject_nature, [2,3]) && !$subjectfp->subject_information->school_id){
					$compulsary_name = 'MT.or LS.or sanskrit or other sub.';
					$pm_fm = '..../....';
					$subject_array[$sn]['subject_id'] = $subjectfp->subject_id;
					$subject_array[$sn]['subject_name'] = $compulsary_name;
					$subject_array[$sn]['subject_nature'] = $subjectfp->subject_information ? $subjectfp->subject_information->subject_nature : 1;
					$subject_array[$sn]['pm/fm'] = $pm_fm;
					$sn++;
				}
			}
		}
		return $subject_array;
	}

	public function SubjectList($school_id){
		$school_information = BasicInformation::model()->findByPk($school_id);
		$criteria = new CDbCriteria;
		$criteria -> with = ['subject_information'];
		$criteria->condition = 't.status=:status AND subject_information.status=:status';
		$criteria->params = [':status'=>1];
		if(!empty($school_information) && $school_information->subject_type != 'general'){
			$school_wise_subject = SchoolSubject::model()->findAll('school_id=:school_id AND status=:status',[':school_id'=>$school_id,':status'=>1]);
			$subject_id_array = [];
			if(!empty($school_wise_subject)){
				foreach ($school_wise_subject as $subject_)
					$subject_id_array[] = $subject_->subject_id;
			}
			$criteria->addInCondition('t.subject_id',$subject_id_array);
		}
		if (!empty($school_information) && $school_information->subject_type == 'general') {
			$criteria->condition .= '  AND subject_type=:subject_type';
			$criteria->params = array_merge($criteria->params, [':subject_type'=>'general']);
		}
		$criteria->order = 'subject_information.subject_order';
		return $this->findAll($criteria);
	}
}
