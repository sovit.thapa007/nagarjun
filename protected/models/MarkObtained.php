<?php

/**
 * This is the model class for table "mark_obtained".
 *
 * The followings are the available columns in table 'mark_obtained':
 * @property integer $id
 * @property integer $academic_year
 * @property integer $terminal_id
 * @property integer $terminal_
 * @property integer $student_id
 * @property integer $subject_id
 * @property string $subject_name
 * @property string $grace_mark
 * @property string $theory_mark
 * @property string $practical_mark
 * @property string $cas_mark
 * @property string $theory_grade
 * @property string $practical_grade
 * @property string $cas_grade
 * @property string $cas
 * @property string $total_mark
 * @property string $total_grade_point
 * @property string $total_grade
 * @property integer $is_practical
 * @property integer $is_cas
 * @property string $theory_pass_mark
 * @property string $practical_pass_mark
 * @property string $theory_full_mark
 * @property string $practical_full_mark
 * @property string $terminal_percent
 * @property string $cas_percent
 * @property string $percent_for_final
 * @property integer $subject_status
 * @property integer $subject_order
 * @property string $status
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 * @property integer $approved_by
 * @property string $approved_date
 * @property integer $school_id
 */
class MarkObtained extends CActiveRecord
{
	public $arrange_by, $options, $upload_files, $sep_th_fm, $sep_pr_fm, $ledger_type, $result_;
	public $top, $insert_type, $prv_link;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mark_obtained';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('academic_year, terminal_id, terminal_, student_id, subject_id, subject_name, theory_mark, theory_pass_mark, theory_full_mark, school_id', 'required'),
			array('academic_year, terminal_id, terminal_, student_id, subject_id, is_practical, is_cas, subject_status, created_by, updated_by, approved_by, school_id, retake_th_number, retake_pr_number, subject_order,', 'numerical', 'integerOnly'=>true),
			array('subject_name', 'length', 'max'=>200),
			array('grace_mark, theory_mark, practical_mark, cas_mark, theory_pass_mark, practical_pass_mark, theory_full_mark, practical_full_mark, terminal_percent, cas_percent, percent_for_final, extra_gp, grace_total', 'length', 'max'=>6),
			array('theory_grade, practical_grade, cas_grade, total_grade,grace_grade', 'length', 'max'=>20),
			array('cas', 'length', 'max'=>100),
			array('total_mark', 'length', 'max'=>6),
			array('total_grade_point', 'length', 'max'=>4),
			array('status', 'length', 'max'=>6),
			array('created_date, updated_date, approved_date', 'safe'),			
			//html purification
            array('id, academic_year, terminal_id, terminal_, student_id, subject_id, subject_name, grace_mark, theory_mark, practical_mark, cas_mark, theory_grade, practical_grade, cas_grade, cas, total_mark, total_grade_point, total_grade, is_practical, is_cas, theory_pass_mark, practical_pass_mark, theory_full_mark, practical_full_mark, terminal_percent, cas_percent, percent_for_final, subject_status, status, created_by, created_date, updated_by, updated_date, approved_by, approved_date, school_id, retake_th_number, retake_pr_number, subject_order', 'filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, academic_year, terminal_id, terminal_, student_id, subject_id, subject_name, grace_mark, theory_mark, practical_mark, cas_mark, theory_grade, practical_grade, cas_grade, cas, total_mark, total_grade_point, total_grade, is_practical, is_cas, theory_pass_mark, practical_pass_mark, theory_full_mark, practical_full_mark, terminal_percent, cas_percent, percent_for_final, subject_status, status, created_by, created_date, updated_by, updated_date, approved_by, approved_date, school_id, retake_th_number, retake_pr_number', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'student' => array(self::BELONGS_TO, 'StudentInformation', 'student_id'),
			'school' => array(self::BELONGS_TO, 'BasicInformation', 'school_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'academic_year' => 'Academic Year',
			'terminal_id' => 'Terminal',
			'terminal_' => 'Terminal',
			'student_id' => 'Student',
			'subject_id' => 'Subject',
			'subject_name' => 'Subject Name',
			'grace_mark' => 'Grace Mark',
			'theory_mark' => 'Theory Mark',
			'practical_mark' => 'Practical Mark',
			'cas_mark' => 'Cas Mark',
			'theory_grade' => 'Theory Grade',
			'practical_grade' => 'Practical Grade',
			'cas_grade' => 'Cas Grade',
			'cas' => 'Cas',
			'total_mark' => 'Total Mark',
			'total_grade_point' => 'Total Grade Point',
			'total_grade' => 'Total Grade',
			'grace_total' => 'Grace Total',
			'grace_grade' =>' Grace Grade',
			'is_practical' => 'Is Practical',
			'is_cas' => 'Is Cas',
			'theory_pass_mark' => 'Theory Pass Mark',
			'practical_pass_mark' => 'Practical Pass Mark',
			'theory_full_mark' => 'Theory Full Mark',
			'practical_full_mark' => 'Practical Full Mark',
			'terminal_percent' => 'Terminal Percent',
			'cas_percent' => 'Cas Percent',
			'percent_for_final' => 'Percent For Final',
			'subject_status' => 'Subject Status',
			'subject_order' => 'Subject Order',
			'retake_th_number' =>  'Theory Retake Count', 
			'retake_pr_number' => 'Practical Retake Count',
			'status' => 'Status',
			'extra_gp' => 'extra_gp',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
			'approved_by' => 'Approved By',
			'approved_date' => 'Approved Date',
			'school_id' => 'School',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('academic_year',$this->academic_year);
		$criteria->compare('terminal_id',$this->terminal_id);
		$criteria->compare('terminal_',$this->terminal_);
		$criteria->compare('student_id',$this->student_id);
		$criteria->compare('subject_id',$this->subject_id);
		$criteria->compare('subject_name',$this->subject_name,true);
		$criteria->compare('grace_mark',$this->grace_mark,true);
		$criteria->compare('theory_mark',$this->theory_mark,true);
		$criteria->compare('practical_mark',$this->practical_mark,true);
		$criteria->compare('cas_mark',$this->cas_mark,true);
		$criteria->compare('theory_grade',$this->theory_grade,true);
		$criteria->compare('practical_grade',$this->practical_grade,true);
		$criteria->compare('cas_grade',$this->cas_grade,true);
		$criteria->compare('cas',$this->cas,true);
		$criteria->compare('total_mark',$this->total_mark,true);
		$criteria->compare('total_grade_point',$this->total_grade_point,true);
		$criteria->compare('total_grade',$this->total_grade,true);
		$criteria->compare('grace_total',$this->grace_total,true);
		$criteria->compare('grace_grade',$this->grace_grade,true);
		$criteria->compare('is_practical',$this->is_practical);
		$criteria->compare('is_cas',$this->is_cas);
		$criteria->compare('theory_pass_mark',$this->theory_pass_mark,true);
		$criteria->compare('practical_pass_mark',$this->practical_pass_mark,true);
		$criteria->compare('theory_full_mark',$this->theory_full_mark,true);
		$criteria->compare('practical_full_mark',$this->practical_full_mark,true);
		$criteria->compare('terminal_percent',$this->terminal_percent,true);
		$criteria->compare('cas_percent',$this->cas_percent,true);
		$criteria->compare('percent_for_final',$this->percent_for_final,true);
		$criteria->compare('subject_status',$this->subject_status);
		$criteria->compare('subject_order',$this->subject_order);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('retake_th_number',$this->retake_th_number);
		$criteria->compare('retake_pr_number',$this->retake_pr_number);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('approved_by',$this->approved_by);
		$criteria->compare('approved_date',$this->approved_date,true);
		$criteria->compare('school_id',$this->school_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MarkObtained the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeValidate(){

		$subject_information = SubjectInformation::model()->findByPk($this->subject_id);
		if($this->isNewRecord){
			$this->created_by = Yii::app()->user->id;
			$this->created_date = new CDbExpression('NOW()');
		}else{
			$this->updated_by = Yii::app()->user->id;
			$this->updated_date = new CDbExpression('NOW()');
		}
		$combine_full_mark = $this->theory_full_mark + $this->practical_full_mark;
		$theory_grade_info = UtilityFunctions::GradeInformation((int) $this->theory_mark, (int) $this->theory_full_mark);
		$practical_grade_info = UtilityFunctions::GradeInformation((int) $this->practical_mark, (int) $this->practical_full_mark);
		$cas_garde_information = UtilityFunctions::GradeInformation((int) $this->cas_mark, (int) $combine_full_mark);
		$total_grade_information = UtilityFunctions::GradeInformation((int) $this->total_mark, (int) $combine_full_mark);
		$this->theory_grade =  isset($theory_grade_info['grade']) ? $theory_grade_info['grade'] : 'Abs';
		if($this->is_practical)
			$this->practical_grade = isset($practical_grade_info['grade']) ? $practical_grade_info['grade'] : 'Abs';
		if($this->is_cas)
			$this->cas_grade =  isset($cas_garde_information['grade']) ? $cas_garde_information['grade'] : 'Abs';
		$this->total_grade_point = isset($total_grade_information['grade_point']) ?  number_format($total_grade_information['grade_point'], 2) : 0;
		//$this->extra_gp = isset($total_grade_information['grade_point']) ?  number_format($total_grade_information['grade_point'], 2) : 0;
		
		$this->total_grade = isset($total_grade_information['grade']) ? $total_grade_information['grade'] : 'Abs';
		$this->status = !in_array($this->total_grade, ['D','E','Abs<sup>*</sup>']) ? 'pass':'retake';
		$this->subject_order = $subject_information ? $subject_information->subject_order : 1;
		return parent::beforeValidate();
	}
	public function afterSave()
	{
	    if (!$this->isNewRecord){
			$obtained_mark_hist = new MarkObtainedHistory();
			$obtained_mark_hist -> mark_obtained_id = $this->id;
			$obtained_mark_hist -> grace_mark = $this->grace_mark;
			$obtained_mark_hist -> theory_mark = $this->theory_mark;
			$obtained_mark_hist -> practical_mark = $this->practical_mark;
			$obtained_mark_hist -> cas_mark = $this->cas_mark;
			$obtained_mark_hist -> theory_grade = $this->theory_grade;
			$obtained_mark_hist -> practical_grade = $this->practical_grade;
			$obtained_mark_hist -> cas_grade = $this->cas_grade;
			$obtained_mark_hist -> cas = $this->cas;
			$obtained_mark_hist -> total_mark = $this->total_mark;
			$obtained_mark_hist -> total_grade_point = $this->total_grade_point;
			$obtained_mark_hist -> total_grade = $this->total_grade;
			$obtained_mark_hist -> retake_th_number = $this->retake_th_number;
			$obtained_mark_hist -> retake_pr_number = $this->retake_pr_number;
			if(!$obtained_mark_hist -> validate() || !$obtained_mark_hist -> save()){
				echo "<pre>";
				echo print_r($obtained_mark_hist->errors);
				exit;
			}
		}
		return  parent::afterSave();
	}



	public function TerminalSubjectDetails($academic_year, $school_id, $terminal_id, $subject_id){
		$subject_information = array();
		$terminal_inforamtion = Terminal::model()->findByPk($terminal_id);
		$subject_information['is_cas'] = !empty($terminal_inforamtion) && $terminal_inforamtion -> terminal_type == 1 ? 1 : null;
		$subject_information['terminal_percent'] =!empty($terminal_inforamtion)? $terminal_inforamtion -> pre_for_terminal : 0;
		$subject_information['cas_percent'] = !empty($terminal_inforamtion)? $terminal_inforamtion -> csa_percentage : 0;
		$subject_information['percent_for_final'] = !empty($terminal_inforamtion)? $terminal_inforamtion -> per_for_final : 0;
		$subject_information['terminal_name'] = !empty($terminal_inforamtion)? $terminal_inforamtion -> title : 'null';
		$subject_information['terminal_'] = !empty($terminal_inforamtion)? $terminal_inforamtion -> terminal_ : 0;
		$subject_information_sec = MarkObtained::model()->findByAttributes(['terminal_id'=>$terminal_id,'academic_year'=>$academic_year,'subject_id'=>$subject_id, 'school_id'=>$school_id]);
		/*if(!empty($subject_information_sec)){
				$subject_information['subject_name'] = $subject_information_sec -> subject_name;
				$subject_information['is_cas'] = $subject_information_sec -> is_cas;
				$subject_information['is_practical'] = $subject_information_sec -> is_practical;
				$subject_information['terminal_percent'] = $subject_information_sec -> terminal_percent;
				$subject_information['cas_percent'] = $subject_information_sec -> cas_percent;
				$subject_information['theory_pass_mark'] = $subject_information_sec -> theory_pass_mark;
				$subject_information['practical_pass_mark'] = $subject_information_sec -> practical_pass_mark;
				$subject_information['theory_full_mark'] = $subject_information_sec -> theory_full_mark;
				$subject_information['practical_full_mark'] = $subject_information_sec -> practical_full_mark;
				$subject_information['percent_for_final'] = $subject_information_sec -> percent_for_final;
				$subject_information['terminal_'] = $subject_information_sec -> terminal_;
		}else{*/
		$pass_mark_information = PassMark::model()->findByAttributes(['subject_id'=>$subject_id]);
		if(!empty($pass_mark_information)){
			$subject_information['subject_name'] = isset($pass_mark_information->subject_information) && !empty($pass_mark_information->subject_information) ? $pass_mark_information->subject_information -> title : "null";
			$subject_information['subject_nature'] = $pass_mark_information->subject_information ? $pass_mark_information->subject_information->subject_nature : 1;


			$subject_information['is_practical'] = $pass_mark_information -> is_practical;
			$subject_information['theory_pass_mark'] = $pass_mark_information -> theory_pass_mark;
			$subject_information['practical_pass_mark'] = $pass_mark_information -> practical_pass_mark;
			$subject_information['theory_full_mark'] = $pass_mark_information -> theory_full_mark;
			$subject_information['practical_full_mark'] = $pass_mark_information -> practical_full_mark;
		}

		//}
		return $subject_information;
	}



	public function GenerateTerminalResult($terminal_id,  $result_publish_date){
		$academic_year = UtilityFunctions::AcademicYear();
		$marks_convertor = MarksConvertor::model()->findAll();
		$fail_student_array = $final_result_rank = $attendance_ = $error_array = $theory_array = array();
		$criteria = new CDbCriteria;
		$criteria -> select = "academic_year, terminal_id, terminal_, student_id,sum(theory_mark) as theory_mark, sum(theory_full_mark) as theory_full_mark, sum(practical_full_mark) as practical_full_mark ,sum(total_mark) as total_mark, sum(total_grade_point) as total_grade_point, count(DISTINCT subject_id) as subject_status, school_id";
		$criteria -> condition = "academic_year=:academic_year AND terminal_id=:terminal_id";
		$criteria -> params = [':academic_year'=>$academic_year, ':terminal_id'=>$terminal_id];
		$criteria -> group = 'student_id';
		$student_information = $this->findAll($criteria);

		$fail_model = new CDbCriteria;
		$fail_model -> select = "student_id, status";
		$fail_model -> condition = "academic_year=:academic_year AND terminal_id=:terminal_id";
		$fail_model -> params = [':academic_year'=>$academic_year, ':terminal_id'=>$terminal_id];
		$fail_model -> addInCondition('status', ['fail','retake']);
		$fail_model -> group = 'student_id';
		$fail_student_ = $this->findAll($fail_model);
		if(!empty($fail_student_) && $fail_student_){
			foreach ($fail_student_ as $fail){
				$fail_student_array[$fail->student_id] = $fail->status;
			}
		}
		$terminal_ = 0;
		if(!empty($student_information) && $student_information){
			foreach ($student_information as $information) {
				$registration_num = $information->student_id;
				$terminal_ = $information->terminal_;
				if(!in_array($registration_num, $fail_student_array))
					$final_result_rank[] = $information->total_mark;

				$theory_array[$registration_num] = $information->theory_mark;
			}
		}
		$std_criteria = new CDbCriteria;
		$std_criteria -> select = 'student_id , sum(attendance) as attendance , sum(total_attendannce) as total_attendannce';
		$std_criteria -> condition = 'academic_year=:academic_year AND terminal_id=:terminal_id';
		$std_criteria -> params = [':academic_year'=>$academic_year, ':terminal_id'=>$terminal_id];
		$std_criteria -> group = 'student_id';
		$attendance_terminal_information= StudentAttendance::model()->findAll($std_criteria);
		if(!empty($attendance_terminal_information)){
			foreach ($attendance_terminal_information as $attendance_info)
				$attendance_[$attendance_info->student_id] = $attendance_info->attendance.'/'.$attendance_info->total_attendannce;
		}
		$rank_section_raw = array_unique($final_result_rank);
		rsort($rank_section_raw);
		$rank_section = array_values($rank_section_raw);
	    $transaction = Yii::app()->db->beginTransaction();

		$error_array = array();
		if(!empty($student_information)){
			/*$approved_mark_obtained = MarkObtained::model()->updateAll(array( 'approved_by' => Yii::app()->user->id, 'approved_date' => new CDbExpression('NOW()') ), 'academic_year = '.$academic_year.' AND terminal_id = 1 ' );
			if(!$approved_mark_obtained)
				$error_array[] = 'false';*/
			foreach ($student_information as $information_std) {
				$registration_number = $information_std -> student_id;
				$resultModel = Result::model()->findByAttributes(['terminal_id'=>$terminal_id, 'student_id'=>$registration_number]);
				if(empty($resultModel))
					$resultModel = new Result();
				$total_obtained = $information_std -> total_mark;
				$total_grade_point = $information_std -> total_grade_point;
				$total_full_mark = (int) $information_std -> theory_full_mark + (int) $information_std -> practical_full_mark;
				$percentage_raw = $total_full_mark!=0 ? $total_obtained*100/$total_full_mark : 0;
				$percentage = number_format($percentage_raw, 2);
				$division = "-";
				if(!empty($marks_convertor)){
					foreach ($marks_convertor as $divgard) {
					if($divgard->min_range <= $percentage && $divgard->max_range >= $percentage && $divgard->type=='terminal')
							$division = $divgard -> code ;
					}
				}
				$subject_status = isset($information_std->subject_status) ? $information_std->subject_status :  0;
				$total_grade_point = isset($information_std->total_grade_point) ? $information_std->total_grade_point :  0;
				$result_status = isset($fail_student_array[$registration_number]) ? 'fail' : 'pass';
				$attendance_information = isset($attendance_[$registration_number]) ? explode('/', $attendance_[$registration_number]) : array();

				$resultModel -> student_id = $registration_number;
				$resultModel -> academic_year = $academic_year;
				$resultModel -> result_type = 'terminal';
				$resultModel -> terminal_id = $terminal_id;
				$resultModel -> terminal_ = $terminal_;
				$resultModel -> total_obtained_mark = number_format($information_std -> total_mark, 2);
				$theory_mark_total = isset($theory_array[$registration_number]) ? $theory_array[$registration_number] : 0;
				$resultModel -> total_full_mark = number_format($total_full_mark, 2);
				$resultModel -> percentage = number_format($percentage, 2);
				$resultModel -> division = $division;
				$gpa = UtilityFunctions::GPACalculation($total_grade_point, $total_full_mark);
				$resultModel -> grade = $theory_mark_total != 0 ? UtilityFunctions::GradeLetter($gpa) : 'Abs<sup>*</sup>';
				$resultModel -> total_gpa = $theory_mark_total != 0 ? $gpa : 0;
				$resultModel -> attendance = isset($attendance_information[0]) ? $attendance_information[0] : 0;
				$resultModel -> total_attendance = isset($attendance_information[1]) ? $attendance_information[1] : 0;
				$resultModel -> rank = in_array($total_obtained, $rank_section) && $result_status == 'pass' ?  array_search($total_obtained, $rank_section) + 1 : 0;
				$resultModel -> result_status = $result_status;
				$resultModel -> status = 1;
				$resultModel -> school_id = $information_std->school_id;
				$resultModel -> number_of_subject = $information_std->subject_status;
				$resultModel -> approved_by = Yii::app()->user->id;
				$resultModel -> approved_date = date('Y-m-d H:i:s');
				$resultModel -> result_publish_date = $result_publish_date;
				try {
					if($resultModel->validate() && $resultModel->save())
						$error_array[] = 'true';
					
				} catch (Exception $e) {
					$error_array[] = 'false';
					/*
					echo "<pre>";
					echo print_r($resultModel->attributes);
					exit;*/
				}
			}
		}else
			throw new CHttpException(400,'Cannot Generate Result, Please Publish Again.');
		if(!in_array('false', $error_array)){
			$transaction->commit();
			return true;
		}else{
			$transaction->rollback();
			return false;
		}


	}


	/**
	*
	*/
	public function StudentAuditDetail($academic_year = 2075){
		//return "total subject number : ".$totalSubject;
		$schoolInformation = Yii::app()->db->createCommand()
	    ->select('id, title, ward_no, schole_code')
	    ->from('basic_information')
	    ->where('status=:status', array(':status'=>1))
	    ->queryAll();
	    $schoolAuditDetail = $school_subject_student_number = [];
	    $no_student_registered = $no_subject_setting = $missing_subject_marks = $missing_theory_mark = $missing_practical_mark = 0;
	    if(!empty($schoolInformation)){
	    	for ($i=0; $i < sizeof($schoolInformation) ; $i++) { 
	    		$school_id = isset($schoolInformation[$i]['id']) ? $schoolInformation[$i]['id'] : null;
	    		$schoolTitle = isset($schoolInformation[$i]['title']) ? $schoolInformation[$i]['title'] : null;
	    		$ward_no = isset($schoolInformation[$i]['ward_no']) ? $schoolInformation[$i]['ward_no'] : null;
	    		$schole_code = isset($schoolInformation[$i]['schole_code']) ? $schoolInformation[$i]['schole_code'] : null;
			    $schoolAuditDetail[$i]['id'] = $school_id;
			    $schoolAuditDetail[$i]['name'] = ucwords($schoolTitle.', '.Yii::app()->params['municipality_short'].'-'.$ward_no);
			    $schoolAuditDetail[$i]['school_code'] = $schole_code;
				$StudentInformation = Yii::app()->db->createCommand()
			    ->select('id')
			    ->from('student_information')
			    ->where('school_id=:school_id AND status=:status AND academic_year=:academic_year', array(':school_id'=>$school_id,':status'=>1, ':academic_year'=>$academic_year))
			    ->queryAll();
			    $student_id = $subject_id = 0;


				$subjectInformation = SubjectInformation::model()->SubjectDetail($school_id);
			    if(!empty($StudentInformation)){
			    	$subject_id_array  = [];
			    	for ($j=0; $j < sizeof($StudentInformation) ; $j++) { 
	    			$student_id = isset($StudentInformation[$j]['id']) ? $StudentInformation[$j]['id'] : null;
			    	//$schoolAuditDetail[$school_id]['student_id'][] = $student_id;
	    			if(!empty($subjectInformation)){
	    				foreach ($subjectInformation as $subject) {
	    					$subjectName = UtilityFunctions::ColumnName($subject->title);
	    					$is_practical = $subject->passMark ? $subject->passMark->is_practical : 0;
	    					$subject_id = $subject->id;
	    					$subjectNature = $subject->subject_nature;
	    					$default_optional = $subject->default_optional;
	    					if($subjectNature != 1){
	    						$student_subject = StudentSubject::model()->find('student_id=:student_id AND status=:status',[':student_id'=>$student_id,':status'=>'active']);
	    						if($student_subject && $default_optional == 1)
	    							continue;
	    						if(!$student_subject && $default_optional == 0)
	    							continue;
								$MarkObtained = MarkObtained::model()->find('academic_year=:academic_year AND school_id=:school_id AND student_id=:student_id AND subject_id=:subject_id AND subject_status=:subject_status  ',[':academic_year'=>$academic_year, ':school_id'=>$school_id,  ':student_id'=>$student_id , ':subject_id'=>$subject_id, ':subject_status'=>1]);
	    					}else
								$MarkObtained = MarkObtained::model()->find('academic_year=:academic_year AND school_id=:school_id AND student_id=:student_id AND subject_id=:subject_id AND subject_status=:subject_status  ',[':academic_year'=>$academic_year, ':school_id'=>$school_id,  ':student_id'=>$student_id , ':subject_id'=>$subject_id, ':subject_status'=>1]);
	    					if(empty($MarkObtained)){
	    						$missing_subject_marks++;
	    						$school_subject_student_number[$school_id][$subjectName] = isset($school_subject_student_number[$school_id][$subjectName]) ? $school_subject_student_number[$school_id][$subjectName] + 1 : 1;
					    		$schoolAuditDetail[$i]['error_message'][] = "missing marks  : ".$subjectName;
					    		continue;
	    					}
	    					if(!empty($MarkObtained)){
	    						$subject_id_array[$student_id][] = $subject_id;
	    						if($MarkObtained->theory_mark <= 0){
	    							$missing_theory_mark++;
	    							$school_subject_student_number[$school_id][$subjectName.'_theory'] = isset($school_subject_student_number[$school_id][$subjectName.'_theory']) ? $school_subject_student_number[$school_id][$subjectName.'_theory'] + 1 : 1;
						    		$schoolAuditDetail[$i]['error_message'][] = "missing theory mark  : ".$subjectName;
	    						}
	    						if($MarkObtained->is_practical && $MarkObtained->practical_mark <= 0){
	    							$missing_practical_mark++;
	    							$school_subject_student_number[$school_id][$subjectName.'_practical'] = isset($school_subject_student_number[$school_id][$subjectName.'_practical']) ? $school_subject_student_number[$school_id][$subjectName.'_practical'] + 1 : 1;
						    		$schoolAuditDetail[$i]['error_message'][] = " missing practical mark  : ".$subjectName;
						    		continue;
	    						}
	    					}
	    				}
	    			}
			    }
			    }else{
			    	$school_subject_student_number[$school_id]['no_student_registered'] = "students are not registered";
			    	$no_student_registered++;
			    	$schoolAuditDetail[$i]['error_message'][] = "students are not registered";
			    	continue;
			    }
	    	}
	    }
	    $wholeInformation['school_wise_information'] = $school_subject_student_number;
	    $wholeInformation['no_student_registered'] = $no_student_registered;
	    $wholeInformation['no_subject_setting'] = $no_subject_setting;
	    $wholeInformation['missing_subject_marks'] = $missing_subject_marks;
	    $wholeInformation['missing_theory_mark'] = $missing_theory_mark;
	    $wholeInformation['missing_practical_mark'] = $missing_practical_mark;
	    $wholeInformation['schoolAuditDetail'] = $schoolAuditDetail;
	    return $wholeInformation;
	}



/**
	*
	*/
	public function StudentAbsentReport($academic_year = 2075){
		$mark_obtaine_information = MarkObtained::model()->findAll('academic_year=:academic_year AND subject_status=:subject_status',[':academic_year'=>$academic_year, ':subject_status'=>1]);
		$criteria = new CDbCriteria;
		$subject_information = SubjectInformation::model()->findAll('status=:status ORDER BY subject_order',[':status'=>1]);
		$subject_header = $mark_detail_information = $student_id_array = $student_detail_array = [];
		if(!empty($subject_information)){
			foreach ($subject_information as $subject_) {
				$name = UtilityFunctions::ColumnName($subject_->title);
				if($subject_->passMark && $subject_->passMark->is_practical){
					$subject_header[] = $name.'_theory';
					$subject_header[] = $name.'_practical';
				}else
					$subject_header[] = $name;
			}
		}
		if(!empty($mark_obtaine_information)){
			foreach ($mark_obtaine_information as $mark_infomation) {
				if($mark_infomation->theory_mark == 0){
					$student_id = $mark_infomation->student_id;
					$subject_name = UtilityFunctions::ColumnName($mark_infomation->subject_name);
					if(!in_array($student_id, $student_id_array)){ $student_id_array[] = $student_id;
						$student_detail_array[$student_id]['school'] = $mark_infomation->school ? strtoupper($mark_infomation->school->title) : '';
						$student_detail_array[$student_id]['school_code'] = $mark_infomation->school ? strtoupper($mark_infomation->school->schole_code) : '';
						$student_detail_array[$student_id]['name'] = $mark_infomation->student ? strtoupper($mark_infomation->student->first_name.' '.$mark_infomation->student->middle_name.' '.$mark_infomation->student->last_name) : '';
						$student_detail_array[$student_id]['symbol_number'] = $mark_infomation->student ? strtoupper($mark_infomation->student->symbol_number) : '';
					}
					$mark_detail_information[$student_id] = $mark_infomation->theory_mark;

				}
			}
		}
		$absent_student_information['student_detail_array'] = $student_detail_array;
		$absent_student_information['subject_header'] = $subject_header;
		$absent_student_information['mark_detail_information'] = $mark_detail_information;
		$absent_student_information['student_id_array'] = $student_id_array;
		return $absent_student_information;

	}


	/**
	* absent student list
	*/

	public function AbsentStudentList($academic_year){
		$theory_grade_information = $student_array = $absent_student_detail = $absent_student_id = [];
		$student_mark_information = Yii::app()->db->createCommand()
        ->from('mark_obtained')
		->andWhere("academic_year=".$academic_year." AND subject_status=1")
		->order('school_id, student_id ASC')
        ->queryAll();
        for ($i=0; $i < sizeof($student_mark_information); $i++) {
        	$student_id = $student_mark_information[$i]['student_id'];
        	if(!in_array($student_id, $student_array)){$student_array[] = $student_id;}
    		$theory_grade_information[$student_id]['theory'][] = $student_mark_information[$i]['theory_grade'];
        }
        for ($k=0; $k < sizeof($student_array) ; $k++) { 
        	$student_id_ = $student_array[$k];
			$student_detail = Yii::app()->db->createCommand()
	        ->from('student_information')
			->andWhere("id=".$student_id_."")
	        ->queryAll();
	        $sex = isset($student_detail[0]['sex']) ? $student_detail[0]['sex'] : '';
	        $school_id_ = isset($student_detail[0]['school_id']) ? $student_detail[0]['school_id'] : '';
	        $symbol_number = isset($student_detail[0]['symbol_number']) ? $student_detail[0]['symbol_number'] : '';
	        $name = isset($student_detail[0]) ? strtoupper($student_detail[0]['first_name'].' '.$student_detail[0]['middle_name'].' '.$student_detail[0]['last_name']) : '';
			$school_detail = Yii::app()->db->createCommand()
	        ->from('basic_information')
			->andWhere("id=".$school_id_."")
	        ->queryAll();
	        $school_type = !empty($school_detail) && isset($school_detail[0]['type']) ? $school_detail[0]['type'] : '';
	        $school_name = !empty($school_detail) && isset($school_detail[0]['title']) ? strtoupper($school_detail[0]['title']) : '';
	        $school_code = !empty($school_detail) && isset($school_detail[0]['schole_code']) ? strtoupper($school_detail[0]['schole_code']) : '';
	        $theory_grade_information_array = array_values($theory_grade_information[$student_id_]['theory']);
	        if(array_unique($theory_grade_information_array) === array('Abs<sup>*</sup>')){
	        	$absent_student_id[] = $student_id_;
	        	$absent_student_detail[$student_id_]['symbol_number'] = $symbol_number;
	        	$absent_student_detail[$student_id_]['name'] = $name;
	        	$absent_student_detail[$student_id_]['school_type'] = strtoupper($school_type);
	        	$absent_student_detail[$student_id_]['school_code'] = $school_code;
	        	$absent_student_detail[$student_id_]['school_name'] = strtoupper($school_name);
	        }
	    }

	    $student_absent_summery['student_id_array'] = $absent_student_id;
	    $student_absent_summery['absent_student_detail'] = $absent_student_detail;
	    return $student_absent_summery;

	}

	/**
	*
	*/
	public function SchoolAbsentReport($academic_year = 2075){
		$mark_obtaine_information = MarkObtained::model()->findAll('academic_year=:academic_year AND subject_status=:subject_status AND theory_mark=:theory_mark',[':academic_year'=>$academic_year, ':subject_status'=>1, ':theory_mark'=>0]);

		$subjectInformation = PassMark::model()->SubjectList($school_id);

		if(!empty($mark_obtaine_information)){
			foreach ($mark_obtaine_information as $mark_infomation) {
				if($mark_infomation->theory_mark == 0 || ($mark_infomation->is_practical == 1 && $mark_infomation->practical_mark == 0)){
					$student_id = $mark_infomation->student_id;
					$subject_name = UtilityFunctions::ColumnName($mark_infomation->subject_name);
					if(!in_array($student_id, $student_id_array)){ $student_id_array[] = $student_id;
						$student_detail_array[$student_id]['school'] = $mark_infomation->school ? strtoupper($mark_infomation->school->title) : '';
						$student_detail_array[$student_id]['school_code'] = $mark_infomation->school ? strtoupper($mark_infomation->school->schole_code) : '';
						$student_detail_array[$student_id]['name'] = $mark_infomation->student ? strtoupper($mark_infomation->student->first_name.' '.$mark_infomation->student->middle_name.' '.$mark_infomation->student->last_name) : '';
						$student_detail_array[$student_id]['symbol_number'] = $mark_infomation->student ? strtoupper($mark_infomation->student->symbol_number) : '';
					 }
					if($mark_infomation->is_practical == 1){
						$mark_detail_information[$student_id][$subject_name.'_theory'] = $mark_infomation->theory_mark;
						$mark_detail_information[$student_id][$subject_name.'_practical'] = $mark_infomation->practical_mark;
					}else
						$mark_detail_information[$student_id][$subject_name] = $mark_infomation->theory_mark;

				}
			}
		}
		$absent_student_information['student_detail_array'] = $student_detail_array;
		$absent_student_information['subject_header'] = $subject_header;
		$absent_student_information['mark_detail_information'] = $mark_detail_information;
		$absent_student_information['student_id_array'] = $student_id_array;
		return $absent_student_information;

	}

	/**
	*/

	public function LedgerAudit($school_id, $academic_year = 2074){
		$columns = $dataArray = [];
		$opt_is_practical = 1;
		$subjectInformation = PassMark::model()->SubjectList($school_id);
		if(empty($subjectInformation)){return null;}
		$StudentInformation = Yii::app()->db->createCommand()
	    ->select('id, first_name, middle_name, last_name, symbol_number, registration_number, dob_nepali')
	    ->from('student_information')
	    ->where('school_id=:school_id AND status=:status AND academic_year=:academic_year ORDER BY symbol_number', array(':school_id'=>$school_id,':status'=>1, ':academic_year'=>$academic_year))
	    ->queryAll();
	    if(empty($StudentInformation)){return null;}
    	for ($j=0; $j < sizeof($StudentInformation) ; $j++) { 
			$student_id = isset($StudentInformation[$j]['id']) ? $StudentInformation[$j]['id'] : null;
			$name = isset($StudentInformation[$j]['first_name']) ? $StudentInformation[$j]['first_name'].' '.$StudentInformation[$j]['middle_name'].' '.$StudentInformation[$j]['last_name'] : null;
			$registration_number = isset($StudentInformation[$j]['registration_number']) ? $StudentInformation[$j]['registration_number'] : null;
			$symbol_number = isset($StudentInformation[$j]['symbol_number']) ? $StudentInformation[$j]['symbol_number'] : null;
			foreach ($subjectInformation as $subject) {
				$subjectName = $subject->subject_information ? $subject->subject_information->title :  '';
                $subject_column = UtilityFunctions::ColumnName($subjectName);
				$is_practical = $subject->is_practical;
				$subject_id = $subject->subject_id;
				$subjectNature = $subject->subject_information ? $subject->subject_information->subject_nature :  1;
				$dataArray[$j]['student_id'] = $student_id;
				$dataArray[$j]['name'] = $name;
				//$dataArray[$j]['registration_number'] = $registration_number;
				$dataArray[$j]['symbol_number'] = $symbol_number;
				$theory_mark = $practical_mark = 'not-set';
				$MarkObtained = MarkObtained::model()->find('academic_year=:academic_year AND school_id=:school_id AND subject_status=:subject_status AND subject_id=:subject_id AND student_id=:student_id',[':academic_year'=>$academic_year, ':school_id'=>$school_id, ':subject_status'=>1, ':subject_id'=>$subject_id, ':student_id'=>$student_id]);
				if(!empty($MarkObtained)){
					$is_practical = $MarkObtained->is_practical;
					$theory_mark = $MarkObtained->theory_mark > 0 ? $MarkObtained->theory_mark : $theory_mark;
					$practical_mark = $is_practical && $MarkObtained->practical_mark > 0 ? $MarkObtained->practical_mark : $practical_mark;
					if(in_array($subjectNature, [2,3]) && !$is_practical){$opt_is_practical = 0;}
				}
				if($is_practical){
					$dataArray[$j][$subject_column.'_theory'] = (int) $theory_mark;
					$dataArray[$j][$subject_column.'_practical'] = (int) $practical_mark;
				}else
					$dataArray[$j][$subject_column] = (int) $theory_mark;
			}
	    }
	    $columns[] = 'name';
	    //$columns[] = 'registration_number';
	    $columns[] = 'symbol_number';
		//$subjectInformation = PassMark::model()->findAll('status=:status',[':status'=>1]);
		if(!empty($subjectInformation)){
			foreach ($subjectInformation as $sbjInfo) {
				$subjectName_ = $sbjInfo->subject_information ? $sbjInfo->subject_information->title :  '';
                $subject_column_ = UtilityFunctions::ColumnName($subjectName_);
				$is_practical_ = $sbjInfo->is_practical;
				$subject_id_ = $sbjInfo->subject_id;
				$subjectNature_ = $sbjInfo->subject_information ? $sbjInfo->subject_information->subject_nature :  1;
				if($is_practical_ || (in_array($subjectNature_, [2,3]) && $opt_is_practical)){
					$columns[] = $subject_column_.'_theory';
					$columns[] = $subject_column_.'_practical';
				}else
					$columns[] = $subject_column_;
			}
		}	  
	    $audit['opt_is_practical'] = $opt_is_practical;
	    $audit['columns'] = $columns;
	    $audit['dataArray'] = $dataArray;
	    return $audit;
	}


	public function RetakeReport($params){
		$academic_year = isset($params['academic_year']) && is_numeric($params['academic_year']) ?  $params['academic_year'] : UtilityFunctions::AcademicYear();
		$subject_id = isset($params['subject_id']) ? $params['subject_id'] : null;
		$school_id = isset($params['school_id']) ? $params['school_id'] : null;
		$criteria = new CDbCriteria;
		$criteria->select = "academic_year,school_id, subject_id, subject_name, total_grade, count(DISTINCT student_id) as student_id";
		$criteria -> condition = 'academic_year=:academic_year AND subject_status=:subject_status AND subject_id=:subject_id';
		$criteria -> params = [':academic_year'=>$academic_year,':subject_status'=>1,':subject_id'=>$subject_id];
		if($school_id){
			$criteria -> condition .=' AND school_id=:school_id';
			$criteria -> params = array_merge($criteria -> params, [':school_id'=>$school_id]);
		}
		$criteria->addInCondition('t.total_grade', ['D','E']);
		$criteria -> condition .=' AND t.theory_mark!=:theory_mark';
		$criteria -> params = array_merge($criteria -> params, [':theory_mark'=>0]);
		$criteria -> group = 'academic_year, school_id, subject_id, student_id';
		$marks_details = MarkObtained::model()->findAll($criteria);
		$school_array = $schoolName_array = $subject_id_array = $subject_array = $dataArray = $dataInformation = [];
		if(!empty($marks_details)){
		foreach ($marks_details as $markInfo) {
			$academic_year = $markInfo->academic_year;
			$school_id = $markInfo->school_id;
			$subject_id = $markInfo->subject_id;
            $subjectName = ucwords($markInfo->subject_name);
			$student_number = $markInfo->student_id;
				if(!in_array($school_id, $school_array)){
            		$school_information = BasicInformation::model()->findByPk($school_id);
            		if(!empty($school_information)){
						$schoolName_array[$school_id]['id'] = $school_id;
						$schoolName_array[$school_id]['name'] = $school_information ? ucwords($school_information->title) : '';
						$schoolName_array[$school_id]['school_code'] = $school_information ? $school_information->schole_code : '';
						$schoolName_array[$school_id]['address'] = $school_information ? ucwords(Yii::app()->params['municipality_short'].'-'.$school_information->ward_no) : '';

            		}
					$school_array[] = $school_id;
				}
				if(!in_array( $subject_id, $subject_id_array)){
					$subject_id_array[] = $subject_id;
					$subject_array[] = $subjectName;}
				$dataArray[$school_id.'_'.$subject_id] = $student_number;
			}
		}
		$dataInformation['academic_year'] = $academic_year;
		$dataInformation['school_id'] = $school_array;
		$dataInformation['school_name'] = $schoolName_array;
		$dataInformation['subject'] = $subject_array;
		$dataInformation['subject_id_array'] = $subject_id_array;
		$dataInformation['data'] = $dataArray;
		return $dataInformation;

	}


	/**
	*resource wise retake listing student 
	*/
	public function RetakeReportStudentWise($academic_year){

		$school_infromation = BasicInformation::model()->findAll('status=:status',[':status'=>1]);
		//if(!empty($))

		$criteria = new CDbCriteria;
		//$criteria -> with = ['school'];
		$criteria->select = "academic_year,subject_id, subject_name, school_id, 
			sum(case when is_practical = 1  && practical_grade IN ('D','E','Abs<sup>*</sup>') then 1 else 0 end) as practical_grade,
			sum(case when theory_grade IN ('D','E','Abs<sup>*</sup>') then 1 else 0 end) as theory_grade";
		$criteria -> condition = 'academic_year=:academic_year AND subject_status=:subject_status';
		$criteria -> params = [':academic_year'=>$academic_year,':subject_status'=>1];
		$criteria -> group = 'academic_year, school_id, subject_id';
		$marks_details = MarkObtained::model()->findAll($criteria);
		$school_array = $schoolName_array = $subject_id_array = $subject_array = $dataArray = $dataInformation = [];
		if(!empty($marks_details)){
		foreach ($marks_details as $markInfo) {
			$academic_year = $markInfo->academic_year;
			$school_id = $markInfo->school_id;
            $school_information = BasicInformation::model()->findByPk($school_id);
            if(empty($school_information))
            	continue;
            $
			$subject_id = $markInfo->subject_id;
            $subjectName = ucwords($markInfo->subject_name);
			$thNumber = $markInfo->theory_grade;
			$prNumber = $markInfo->practical_grade;
			if($thNumber !=0 || $prNumber != 0){
				if(!in_array($school_id, $school_array)){
            		if(!empty($school_information)){
						$schoolName_array[$school_id]['id'] = $school_id;
						$schoolName_array[$school_id]['name'] = $school_information ? ucwords($school_information->title) : '';
						$schoolName_array[$school_id]['school_code'] = $school_information ? $school_information->schole_code : '';
						$schoolName_array[$school_id]['address'] = $school_information ? ucwords(Yii::app()->params['municipality_short'].'-'.$school_information->ward_no) : '';

            		}
					$school_array[] = $school_id;
				}
				if(!in_array( $subject_id, $subject_id_array)){
					$subject_id_array[] = $subject_id;
					$subject_array[] = $subjectName;}
				$dataArray[$school_id.'_'.$subject_id] = $thNumber.'-'.$prNumber;
			}
		}
		}
		$dataInformation['academic_year'] = $academic_year;
		$dataInformation['school_id'] = $school_array;
		$dataInformation['school_name'] = $schoolName_array;
		$dataInformation['subject'] = $subject_array;
		$dataInformation['subject_id_array'] = $subject_id_array;
		$dataInformation['data'] = $dataArray;
		return $dataInformation;

	}

/**
* retake student list section
*/
	public function RetakeStudentList($params){
        $academic_year = is_numeric($params['academic_year']) ? $params['academic_year'] : null;
        $school_id = is_numeric($params['school_id']) ? $params['school_id'] : null;
        $subject_id = is_numeric($params['subject_id']) ? $params['subject_id'] : null;
    	$subjectInformation = PassMark::model()->SubjectList($school_id);
		$criteria = new CDbCriteria;
		$criteria -> with = ['student', 'school'];
		$criteria -> condition = 't.academic_year=:academic_year AND subject_status=:subject_status AND t.subject_id=:subject_id';
		$criteria -> params = [':academic_year'=>$academic_year,':subject_status'=>1, ':subject_id'=>$subject_id];
		if($school_id){
			$criteria -> condition .=' AND t.school_id=:school_id';
			$criteria -> params = array_merge($criteria -> params, [':school_id'=>$school_id]);
		}
		$criteria->addInCondition('t.total_grade', ['D','E']);
		$criteria -> condition .=' AND t.theory_mark!=:theory_mark';
		$criteria -> params = array_merge($criteria -> params, [':theory_mark'=>0]);
		$criteria->order = 'student.symbol_number ASC';
		$criteria->group = 'student_id, subject_id';
		$student_mark_obtained = MarkObtained::model()->findAll($criteria);
		$student_mark_detail_array = $student_array = $student_detail_array = [];
		if(!empty($subjectInformation)){
			foreach ($subjectInformation as $subject_) {
				$name = $subject_->subject_information ? UtilityFunctions::seoUrl($subject_->subject_information->title) : '';
				if($subject_->is_practical == 1){
					$subject_header[] = $name.'_theory';
					$subject_header[] = $name.'_practical';
					$subject_header[] = $name.'_total';

				}else{
					$subject_header[] = $name;
				}
			}

		}
		if(!empty($student_mark_obtained)){
			foreach ($student_mark_obtained as $mark_info) {
				$name = UtilityFunctions::seoUrl($mark_info->subject_name);
				if(!in_array($mark_info->student_id, $student_array)){
					$student_array[] = $mark_info->student_id; 
					$student_detail_array[$mark_info->student_id]['school'] = $mark_info->school ? strtoupper($mark_info->school->title.', '.Yii::app()->params['municipality_short'].'-'.$mark_info->school->ward_no) : '';
					$student_detail_array[$mark_info->student_id]['school_code'] = $mark_info->school ? $mark_info->school->schole_code : ''; 
					$student_detail_array[$mark_info->student_id]['symbol_number'] = $mark_info->student ? $mark_info->student->symbol_number : ''; 
					$student_detail_array[$mark_info->student_id]['name'] = $mark_info->student ? strtoupper($mark_info->student->first_name.' '.$mark_info->student->middle_name.' '.$mark_info->student->last_name) : ''; 
				}
				if($mark_info->is_practical == 1){
					$student_mark_detail_array[$mark_info->student_id][$name.'_theory'] = $mark_info->theory_mark.'/'.$mark_info->theory_grade.'(G : '.$mark_info->grace_mark.')';
					$student_mark_detail_array[$mark_info->student_id][$name.'_practical'] = $mark_info->practical_mark.'/'.$mark_info->practical_grade;
					$student_mark_detail_array[$mark_info->student_id][$name.'_total'] = $mark_info->total_mark.'/'.$mark_info->total_grade;
				}else{
					$student_mark_detail_array[$mark_info->student_id][$name] = $mark_info->total_mark.'/'.$mark_info->total_grade;
				}
				//$student_mark_detail_array[$mark_info->student_id]['subject'][] = $mark_info->subject_name;

			}
		}
		$studentRetake['student_mark_detail_array'] = $student_mark_detail_array;
		$studentRetake['student_array'] = $student_array;
		$studentRetake['student_detail_array'] = $student_detail_array;
		return $studentRetake;
	}



	/**
	*
	*/

	public function RetakeStudentNumber($params){
        $academic_year = is_numeric($params['academic_year']) ? $params['academic_year'] : null;
        $school_id = is_numeric($params['school_id']) ? $params['school_id'] : null;
		$criteria = new CDbCriteria;
		$criteria -> select = 'count(DISTINCT student_id) as student_id, subject_id, subject_name, subject_order';
		$criteria -> condition = 't.academic_year=:academic_year AND subject_status=:subject_status';
		$criteria -> params = [':academic_year'=>$academic_year,':subject_status'=>1];
		if($school_id){
			$criteria -> condition .=' AND t.school_id=:school_id';
			$criteria -> params = array_merge($criteria -> params, [':school_id'=>$school_id]);
		}
		$criteria->addInCondition('t.total_grade', ['D','E']);
		$criteria -> condition .=' AND t.theory_mark!=:theory_mark';
		$criteria -> params = array_merge($criteria -> params, [':theory_mark'=>0]);
		$criteria->order = 'subject_order ASC';
		$criteria->group = 'subject_id';
		$student_mark_obtained = MarkObtained::model()->findAll($criteria);
		$subject_header = $student_number_detail_array = $subject_id_array = [];
		if(!empty($student_mark_obtained)){
			foreach ($student_mark_obtained as $mark_info) {
				$subject_header[$mark_info->subject_id] = $mark_info->subject_name;
				$subject_id_array[] = $mark_info->subject_id;
				$student_number_detail_array[$mark_info->subject_id] = $mark_info->student_id;

			}
		}
		$studentRetake['student_number_detail_array'] = $student_number_detail_array;
		$studentRetake['subject_id_array'] = $subject_id_array;
		$studentRetake['subject_header'] = $subject_header;
		return $studentRetake;
	}



	/**
	*/


	public function RetakeSubjectStudentList($params){
        $academic_year = is_numeric($params['academic_year']) ? $params['academic_year'] : UtilityFunctions::AcademicYear();
        $subject_id = is_numeric($params['subject_id']) ? $params['subject_id'] : null;
		$criteria = new CDbCriteria;
		$criteria -> with = ['student', 'school'];
		$criteria -> condition = 't.academic_year=:academic_year AND subject_status=:subject_status AND t.subject_id=:subject_id';
		$criteria -> params = [':academic_year'=>$academic_year,':subject_status'=>1, ':subject_id'=>$subject_id];
		$criteria->addInCondition('t.total_grade', ['D','E']);
		$criteria -> condition .=' AND t.theory_mark!=:theory_mark';
		$criteria -> params = array_merge($criteria -> params, [':theory_mark'=>0]);
		$criteria->order = 'student.symbol_number ASC';
		$criteria->group = 'student_id';
		$student_mark_obtained = MarkObtained::model()->findAll($criteria);
		$student_mark_detail_array = $student_array = $student_detail_array = [];
		if(!empty($student_mark_obtained)){
			foreach ($student_mark_obtained as $mark_info) {
				$name = UtilityFunctions::seoUrl($mark_info->subject_name);
				if(!in_array($mark_info->student_id, $student_array)){$student_array[] = $mark_info->student_id; 
					$student_detail_array[$mark_info->student_id]['school'] = $mark_info->school ? strtoupper($mark_info->school->title.', '.Yii::app()->params['municipality_short'].'-'.$mark_info->school->ward_no) : '';
					$student_detail_array[$mark_info->student_id]['school_code'] = $mark_info->school ? $mark_info->school->schole_code : ''; 
					$student_detail_array[$mark_info->student_id]['symbol_number'] = $mark_info->student ? $mark_info->student->symbol_number : ''; 
					$student_detail_array[$mark_info->student_id]['name'] = $mark_info->student ? strtoupper($mark_info->student->first_name.' '.$mark_info->student->middle_name.' '.$mark_info->student->last_name) : ''; 
				}
				if($mark_info->is_practical == 1){
					$student_mark_detail_array[$mark_info->student_id][$name] = $mark_info->total_mark.'/'.$mark_info->total_grade;
				}else{
					$student_mark_detail_array[$mark_info->student_id][$name] = $mark_info->total_mark.'/'.$mark_info->total_grade;
				}
				$student_mark_detail_array[$mark_info->student_id]['subject'][] = $mark_info->subject_name;

			}
		}
		$studentRetake['student_mark_detail_array'] = $student_mark_detail_array;
		$studentRetake['student_array'] = $student_array;
		$studentRetake['student_detail_array'] = $student_detail_array;
		return $studentRetake;
	}


	/**
	* school wise retake student number
	*/	
	public function RetakeSchoolDetail($params){
        $academic_year = isset($params['academic_year'])  && is_numeric($params['academic_year']) ? $params['academic_year'] : UtilityFunctions::AcademicYear();
        $school_id = isset($params['school_id']) ? $params['school_id'] : null;
    	$subjectInformation = PassMark::model()->SubjectList($school_id);
		$criteria = new CDbCriteria;
		$criteria -> with = ['school'];
		$criteria -> select = 'count(student_id) as student_id, school_id, subject_name, subject_id';
		$criteria -> condition = 't.academic_year=:academic_year AND subject_status=:subject_status';
		$criteria -> params = [':academic_year'=>$academic_year,':subject_status'=>1];
		if($school_id){
			$criteria -> condition .=' AND t.school_id=:school_id';
			$criteria -> params = array_merge($criteria -> params, [':school_id'=>$school_id]);
		}
		$criteria->addInCondition('t.total_grade', ['D','E','Abs<sup>*</sup>']);
		$criteria -> condition .=' OR t.theory_grade=:theory_grade';
		$criteria -> params = array_merge($criteria -> params, [':theory_grade'=>'Abs<sup>*</sup>']);
		$criteria -> order = 'school.schole_code ASC';
		$criteria -> group = 'school_id, subject_id';
		$school_mark_obtained = MarkObtained::model()->findAll($criteria);
		$subject_header = $school_array = $school_array_detail = [];
		if(!empty($subjectInformation)){
			foreach ($subjectInformation as $subject_) {
				$name = $subject_->subject_information ? UtilityFunctions::seoUrl($subject_->subject_information->title) : '';
				$subject_header[] = $name;
			}

		}
		if(!empty($school_mark_obtained)){
			foreach ($school_mark_obtained as $mark_info) {
				$name = UtilityFunctions::seoUrl($mark_info->subject_name);
				$school_id = $mark_info->school_id;
				$criteria_ = new CDbCriteria;
				$criteria_ -> select = 'count(DISTINCT(student_id)) as student_id';
				$criteria_ -> condition = 't.academic_year=:academic_year AND t.subject_status=:subject_status AND t.school_id=:school_id';
				$criteria_ -> params = [':academic_year'=>$academic_year,':subject_status'=>1, ':school_id'=>$school_id];
				$criteria_ -> addInCondition('t.total_grade', ['D','E','Abs<sup>*</sup>']);
				$criteria_ -> condition .=' OR t.theory_grade=:theory_grade';
				$criteria_ -> params = array_merge($criteria_ -> params, [':theory_grade'=>'Abs<sup>*</sup>']);
				$criteria_ -> group = 'school_id';
				$school_mark_obtained_ = MarkObtained::model()->find($criteria_);
				$total_student_number = $school_mark_obtained_ ? $school_mark_obtained_->student_id : '';
				if(!in_array($school_id, $school_array)){
					$school_array[] = $school_id; 
					$school_array_detail[$school_id]['school'] = $mark_info->school ? strtoupper($mark_info->school->title.', '.Yii::app()->params['municipality_short'].'-'.$mark_info->school->ward_no) : '';
					$school_array_detail[$school_id]['school_code'] = $mark_info->school ? $mark_info->school->schole_code : '';
				}
				$student_number = $mark_info->student_id;
				$school_array_detail[$school_id][$name] = $student_number;
				$school_array_detail[$school_id]['total_student'] = $total_student_number;
			}
		}
		$school_wise_retake['school_array_detail'] = $school_array_detail;
		$school_wise_retake['school_array'] = $school_array;
		$school_wise_retake['subject_header'] = $subject_header;
		return $school_wise_retake;
	}


	/**
	* upgrade by grace
	*/
	public function UpgradeByGrace(){
		die('no upgrade');
		$sql='UPDATE mark_obtained SET grace_mark=0 , grace_total=total_mark, grace_grade=total_grade ';
		$command=Yii::app()->db->createCommand($sql);
		$update=$command->execute();
        $academic_year = UtilityFunctions::AcademicYear();
        $grace_models = GraceMarks::model()->find('academic_year=:academic_year ORDER BY id DESC',[':academic_year'=>$academic_year]);
        $grace_mark = !empty($grace_models) ? $grace_models->grace_mark : 0;
        $minimum_mark = !empty($grace_models) ? (int) $grace_models->minimum_mark : null;
        $maximum_subject = !empty($grace_models) ? (int) $grace_models->maximum_subject : null;
		$criteria = new CDbCriteria;
		$criteria -> condition = 't.academic_year=:academic_year AND subject_status=:subject_status';
		$criteria -> params = [':academic_year'=>$academic_year,':subject_status'=>1];
		$criteria->addInCondition('t.total_grade', ['D','E']);
		$criteria -> condition .=' AND t.theory_mark!=:theory_mark';
		$criteria -> params = array_merge($criteria -> params, [':theory_mark'=>0]);
		$criteria->group = 'student_id, subject_id';
		$student_mark_obtained = MarkObtained::model()->findAll($criteria);
		$student_array = $raw_student_list = $grace_marks_student = $grace_marks_added_subject = [];
		if(!empty($student_mark_obtained)){
			foreach ($student_mark_obtained as $student_section){
				$student_id = $student_section->student_id;
				$school_information = BasicInformation::model()->findByPk($student_section->school_id);
				if($school_information->type=='community'){
					$total_full_mark = $student_section->theory_full_mark + $student_section->practical_full_mark;
					$obtained = $student_section->total_mark;
					$percentage_cal = $total_full_mark !=0 ? $obtained*100/$total_full_mark : 0;
					if($percentage_cal >= $minimum_mark){
						if(!in_array($student_id, $student_array)){$student_array[] = $student_id; }
						$raw_student_list[$student_id]['subject'][] = $student_section->subject_id;
					}

				}

			}
		}
		if($maximum_subject != 0){
			for ($i=0; $i < sizeof($student_array) ; $i++) { 
				$student_id = $student_array[$i];
				$total_suject_number = isset($raw_student_list[$student_id]['subject']) ? count($raw_student_list[$student_id]['subject']) : 0;
				if($total_suject_number <= $maximum_subject){
					if(!in_array($student_id, $grace_marks_student))
						$grace_marks_student[] = $student_id;
				}
			}

		}else
			$grace_marks_student = $student_array;
		$error_array = $grace_marks_added_subject = $student_detail_array = [];
        $transaction = Yii::app()->db->beginTransaction();
		for ($j=0; $j < sizeof($grace_marks_student) ; $j++) { 
			$gr_student_id = $grace_marks_student[$j];
			$student_information = StudentInformation::model()->findByPk($gr_student_id);
			$student_detail_array[$gr_student_id]['name'] = $student_information ? strtoupper($student_information->first_name.' '.$student_information->middle_name.' '.$student_information->last_name) : '';
			$student_detail_array[$gr_student_id]['symbol_number'] = $student_information ? $student_information->symbol_number  : '';
			$student_detail_array[$gr_student_id]['school_code'] = $student_information && $student_information->school_sec ? $student_information->school_sec->schole_code  : '';
			$student_subject_array = isset($raw_student_list[$gr_student_id]['subject']) ? $raw_student_list[$gr_student_id]['subject'] : 0;
            $mark_need_to_pass = $mark_need_to_pass_raw = $raw_total_obtained = 0;
            $grace_mark_give = $grace_mark ;
			for ($k=0; $k < sizeof($student_subject_array) ; $k++) { 
				$subject_id = $student_subject_array[$k];
				$mark_information = $this->find('student_id=:student_id AND subject_id=:subject_id AND academic_year=:academic_year AND subject_status=:subject_status',[':student_id'=>$gr_student_id, ':subject_id'=>$subject_id, ':academic_year'=>$academic_year, ':subject_status'=>1]);
				if(!empty($mark_information)){
					$total_full_mark = (int) $mark_information->theory_full_mark + (int) $mark_information->practical_full_mark;
					$total_obtained = $mark_information->total_mark;
					$subject_name = strtoupper($mark_information->subject_name);
					$mark_need_to_pass = 30*$total_full_mark/100 - $total_obtained;
                    if($mark_need_to_pass <= $grace_mark_give){
                        $grace_mark_give -= $mark_need_to_pass;
                        $total_mark_with_grace = $mark_information->theory_mark + $mark_information->practical_mark + $mark_need_to_pass;
                    	$grace_marks_added_subject[$gr_student_id][] = $subject_name.':'.$mark_need_to_pass;
						$grace_upgrade_section = UtilityFunctions::GradeInformation((int) $total_mark_with_grace, (int) $total_full_mark);
						$grade = isset($grace_upgrade_section['grade']) ? $grace_upgrade_section['grade'] : 'Abs';
						$grade_point = isset($grace_upgrade_section['grade_point']) ?  number_format($grace_upgrade_section['grade_point'], 2) : 0;
						$theory_mark = $mark_information->theory_mark + $mark_need_to_pass;
						$theory_grade_information = UtilityFunctions::GradeInformation((int) $theory_mark, (int) $total_full_mark);
						$th_grade = isset($theory_grade_information['grade']) ? $theory_grade_information['grade'] : 'Abs';
						$total_mark = $total_mark_with_grace;
						$sql='UPDATE mark_obtained SET grace_mark=:grace_mark, status=:status, grace_total=:grace_total, grace_grade=:grace_grade
							  WHERE id='.$mark_information->id;
/*
						$sql='UPDATE mark_obtained SET grace_mark=:grace_mark, theory_mark=:theory_mark, theory_grade=:theory_grade, status=:status, grace_total=:grace_total, grace_grade=:grace_grade, total_mark=:total_mark, total_grade_point=:total_grade_point, total_grade=:total_grade
							  WHERE id='.$mark_information->id;
							
						$update=$command->execute(array(
						  'grace_mark'=>$mark_need_to_pass,
						  'theory_mark'=>$theory_mark,
						  'theory_grade'=>$th_grade,
						  'status'=>'grace_pass',
						  'grace_total'=>$total_mark_with_grace,
						  'grace_grade'=>$grade,
						  'total_mark'=>$total_mark,
						  'total_grade_point'=>$grade_point,
						  'total_grade'=>$grade,
						));*/
	
							 
							//ORDER BY id ASC LIMIT 1
						$command=Yii::app()->db->createCommand($sql);
						$update=$command->execute(array(
						  'grace_mark'=>$mark_need_to_pass,
						  'status'=>'grace_pass',
						  'grace_total'=>$total_mark_with_grace,
						  'grace_grade'=>$grade,
						));
						if($update)
							$error_array[] = 'true';
						else
							$error_array[] = 'false';
					}
				}

			}
		}	
        if(in_array('false', $error_array)){
            $transaction->rollback();
            return false;
        }
        $transaction->commit();
        $grace_information['student_detail_array'] = $student_detail_array;
        $grace_information['grace_mark_detail'] = $grace_marks_added_subject;
        $grace_information['student_array'] = $grace_marks_student;
        return $grace_information;

	}

	public function GraceAuditReport($academic_year = null){
		$sn = 0;
        $academic_year = $academic_year ? $academic_year : UtilityFunctions::AcademicYear();
        $register_student_information = StudentInformation::model()->GenderWiseStudent($academic_year);
        $male_total_student = $register_student_information ? $register_student_information->male_number : 0;
        $female_total_student = $register_student_information ? $register_student_information->female_number : 0;
        $student_mark_detail_array = $theory_grade_information = $student_array = $student_number_information = $student_grace_array = $student_grace_mark_array = $absent_grade_array = $absent_student_array = $above_d_array = $below_d_array = $upgraded_student_array = $cannot_upgarde_student = $school_wise_audit = $school_information_array = [];
		$student_mark_information = Yii::app()->db->createCommand()
        ->from('mark_obtained')
		->andWhere("academic_year=".$academic_year." AND subject_status=1")
		->order('school_id, student_id ASC')
        ->queryAll();
        for ($i=0; $i < sizeof($student_mark_information); $i++) {
        	$student_id = $student_mark_information[$i]['student_id'];
        	$subject_id = $student_mark_information[$i]['subject_id'];
        	$school_id = $student_mark_information[$i]['school_id'];
        	if(!in_array($student_id, $student_array)){$student_array[] = $student_id;}
        	$student_mark_detail_array[$student_id][$subject_id] = $student_mark_information[$i]['total_grade'];
    		$grace_mark = $student_mark_information[$i]['grace_mark'];
    		$grace_grade = $student_mark_information[$i]['grace_grade'];
    		$student_grace_mark_array[$student_id][$subject_id] = $grace_grade;
    		if($grace_mark > 0 && !in_array($student_id, $student_grace_array)){ $student_grace_array[] = $student_id; }
    		$theory_grade_information[$student_id]['theory'][] = $student_mark_information[$i]['theory_grade'];
        }
        for ($k=0; $k < sizeof($student_array) ; $k++) { 
        	$student_id_ = $student_array[$k];
			$student_detail = Yii::app()->db->createCommand()
	        ->from('student_information')
			->andWhere("id=".$student_id_."")
	        ->queryAll();
	        $sex = isset($student_detail[0]['sex']) ? $student_detail[0]['sex'] : '';
	        $school_id_ = isset($student_detail[0]['school_id']) ? $student_detail[0]['school_id'] : '';
			$school_detail = Yii::app()->db->createCommand()
	        ->from('basic_information')
			->andWhere("id=".$school_id_."")
	        ->queryAll();
	        $school_type = !empty($school_detail) && isset($school_detail[0]['type']) ? $school_detail[0]['type'] : '';
	        $grade_information_array = array_values($student_mark_detail_array[$student_id_]);
	        $theory_grade_information_array = array_values($theory_grade_information[$student_id_]['theory']);
	        $grade_unique = array_unique($grade_information_array);
	        $partial_absent = array_diff($grade_unique,['Abs<sup>*</sup>']);
	        if(!in_array('D', $grade_information_array) && !in_array('E', $grade_information_array) && !in_array('Abs<sup>*</sup>', $grade_information_array)){
	        	if(isset($student_number_information['above_d'][$school_type][$sex])){
	        		$student_number_information['above_d'][$school_type][$sex] = $student_number_information['above_d'][$school_type][$sex] + 1;
	        	}
	        	else
	        		$student_number_information['above_d'][$school_type][$sex] =1;

	        	if(isset($school_wise_audit['above_d'][$school_id_]))
	        		$school_wise_audit['above_d'][$school_id_] = $school_wise_audit['above_d'][$school_id_] + 1;
	        	else 
	        		$school_wise_audit['above_d'][$school_id_] = 1;
	        	$above_d_array[] = $student_id_;
	        }else{
		        if(array_unique($theory_grade_information_array) === array('Abs<sup>*</sup>') && !in_array($student_id_, $above_d_array) && !in_array($student_id_, $below_d_array)){
		        	if(isset($student_number_information['absent'][$school_type][$sex])){
		        		$student_number_information['absent'][$school_type][$sex] = $student_number_information['absent'][$school_type][$sex] + 1;
		        	}
		        	else
		        		$student_number_information['absent'][$school_type][$sex] =1;
		        	$absent_grade_array[$student_id_] = $theory_grade_information_array;
		        	if(!in_array($student_id_, $absent_student_array)){ $absent_student_array[] =$student_id_;}

		        	if(isset($school_wise_audit['absent'][$school_id_]))
		        		$school_wise_audit['absent'][$school_id_] = $school_wise_audit['absent'][$school_id_] + 1;
		        	else 
		        		$school_wise_audit['absent'][$school_id_] = 1;

		        }else{
		        	if(!in_array($student_id_, $above_d_array) && !in_array($student_id_, $absent_student_array)){
			        	if(isset($student_number_information['below_d'][$school_type][$sex])){
			        		$student_number_information['below_d'][$school_type][$sex] = $student_number_information['below_d'][$school_type][$sex] + 1;
			        	}
			        	else
			        		$student_number_information['below_d'][$school_type][$sex] =1;
			        	$below_d_array[] = $student_id_;


			        	if(isset($school_wise_audit['below_d'][$school_id_]))
			        		$school_wise_audit['below_d'][$school_id_] = $school_wise_audit['below_d'][$school_id_] + 1;
			        	else 
			        		$school_wise_audit['below_d'][$school_id_] = 1;
		        	}
		        }
	        }

	        if(in_array($student_id_, $student_grace_array)){
		        if(array_unique($theory_grade_information_array) === array('Abs<sup>*</sup>'))
		        	$total_absent = 0;
		        else{
			        $grace_mark_detail_information = array_values($student_grace_mark_array[$student_id_]);
		        	if(!in_array('D', $grace_mark_detail_information) && !in_array('E', $grace_mark_detail_information) && !in_array('Abs<sup>*</sup>', $theory_grade_information_array)){
		        		$upgraded_student_array[$sn]['id'] = $student_id_;
		        		$upgraded_student_array[$sn]['name'] = isset($student_detail[0]) ? strtoupper($student_detail[0]['first_name'].' '.$student_detail[0]['middle_name'].' '.$student_detail[0]['last_name']) : '';
		        		$upgraded_student_array[$sn]['symbol_number'] = isset($student_detail[0]) ? $student_detail[0]['symbol_number'] : '';
		        		$upgraded_student_array[$sn]['school'] = isset($school_detail[0]) ? strtoupper($school_detail[0]['title'].', '.Yii::app()->params['municipality_short'].' - '. $school_detail[0]['ward_no']) : '';
		        		$upgraded_student_array[$sn]['school_code'] = isset($school_detail[0]) ? $school_detail[0]['schole_code'] : '';

		        		//$upgrade_student = $upgrade_student + 1;
			        	if(isset($student_number_information['upgrade'][$school_type][$sex])){
			        		$student_number_information['upgrade'][$school_type][$sex] = $student_number_information['upgrade'][$school_type][$sex] + 1;
			        	}
			        	else
			        		$student_number_information['upgrade'][$school_type][$sex] =1;
			        	$sn++;
		        	}else{
		        		$cannot_upgarde_student[] = $student_id_;
		        	}
		        }

	        }
        }
        $student_number_information['total_male'] = $male_total_student;
        $student_number_information['total_female'] = $female_total_student;
        $student_number_information['upgraded_student_array'] = $upgraded_student_array;
        $student_number_information['cannot_upgarde_student'] = $cannot_upgarde_student;
        return $student_number_information;
	}




	public function InsertRetakeStudentMark($params){
        $academic_year = is_numeric($params['academic_year']) ? $params['academic_year'] : null;
        $subject_id = is_numeric($params['subject_id']) ? $params['subject_id'] : null;
        $subjectType = in_array($params['subjectType'], ['TH','PR']) ? $params['subjectType'] : null ;
        $retak_of = $subjectType && $subjectType == 'TH' ?  'theory' : 'practical';
        $studentArray = $params['studentArray'];
        $transaction = Yii::app()->db->beginTransaction();
        for ($i=0; $i < sizeof($studentArray) ; $i++) { 
            $student_id = $studentArray[$i];
            $markObtained = MarkObtained::model()->find('student_id=:student_id AND academic_year=:academic_year AND subject_id=:subject_id',[':student_id'=>$student_id, ':academic_year'=>$academic_year, ':subject_id'=>$subject_id]);
            if(!empty($markObtained)){
                if($subjectType=='TH'){
                    $th_fm = $markObtained->theory_full_mark;
                    $practical_mark = $markObtained->practical_mark;
                    $theory_mark = isset($params[$student_id.'_'.$subject_id.'_'.$retak_of]) && is_numeric($params[$student_id.'_'.$subject_id.'_'.$retak_of]) && $params[$student_id.'_'.$subject_id.'_'.$retak_of] <= $th_fm ? $params[$student_id.'_'.$subject_id.'_'.$retak_of] : 0;
                    $markObtained -> theory_mark = $theory_mark;
                    $markObtained -> total_mark = $practical_mark + $theory_mark;
                    $prv = $markObtained -> retake_th_number;
                    $markObtained -> retake_th_number = $prv + 1;
                }
                if($subjectType=='PR'){
                    $pr_fm = $markObtained->practical_full_mark;
                    $theory_mark = $markObtained->theory_mark;
                    $practical_mark = isset($params[$student_id.'_'.$subject_id.'_'.$retak_of]) && is_numeric($params[$student_id.'_'.$subject_id.'_'.$retak_of]) && $params[$student_id.'_'.$subject_id.'_'.$retak_of] <= $pr_fm ? $params[$student_id.'_'.$subject_id.'_'.$retak_of] : 0;
                    $markObtained -> practical_mark = $practical_mark;
                    $markObtained -> total_mark = $practical_mark + $theory_mark;
                    $prv_re = $markObtained -> retake_pr_number;
                    $markObtained -> retake_pr_number = $prv_re + 1;
                }
                if(!$markObtained->validate() || !$markObtained->update()){
                    $transaction->rollback();
                    throw new CHttpException(400,'Cannot Save, Please Insert Again.');
                }
            }else{
                $transaction->rollback();
                throw new CHttpException(400,'Invalid Student.');   
            }
        }
        $transaction->commit();
        return true;
	}


	/**/
	public function SubjecSchoolAvgGrd($academic_year){
		$criteria = new CDbCriteria;
		$criteria -> with = ['school'];
		$criteria -> select = 't.school_id, t.subject_name, t.subject_id, AVG(total_mark) as total_mark ,t.theory_full_mark, t.practical_full_mark';
		$criteria -> condition = 't.academic_year=:academic_year AND t.subject_status=:subject_status';
		$criteria -> params = [':academic_year'=>$academic_year, ':subject_status'=>1];
		$criteria -> order = 'school.title';
		$criteria -> group = 't.school_id,t.subject_id';
		$averageReport = $this->findAll($criteria);
		$dataArray = $school_id_arry = $school_array = $subject_array = $subject_id_array = [];
		if(!empty($averageReport)){
		foreach ($averageReport as $reportInfo) {
			if(!in_array($reportInfo->school_id, $school_id_arry)){$school_id_arry[] = $reportInfo->school_id;}
			if(!in_array($reportInfo->subject_id, $subject_id_array)){$subject_id_array[] = $reportInfo->subject_id;}
			$school_array[$reportInfo->school_id] = $reportInfo->school ?  $reportInfo->school->title : '';
			$subject_array[$reportInfo->subject_id] = $reportInfo->subject_id == 9 ? 'OPT.' :strtoupper(substr($reportInfo->subject_name, 0, 3)).'.';
			$total_mark = number_format($reportInfo->total_mark, 2);
			$fm = $reportInfo->theory_full_mark + $reportInfo->practical_full_mark;
			$grade_information = UtilityFunctions::GradeInformation($total_mark, $fm);
			$grade = isset($grade_information['grade']) ? $grade_information['grade'] : '';
			$gp = isset($grade_information['grade_point']) ? $grade_information['grade_point'] : '';
			$dataArray[$reportInfo->school_id][$reportInfo->subject_id]['marks'] = $total_mark;
			$dataArray[$reportInfo->school_id][$reportInfo->subject_id]['g'] = $grade;
			$dataArray[$reportInfo->school_id][$reportInfo->subject_id]['gp'] = $gp;
		}
		}
		$dataArrayInformation['dataArray'] = $dataArray;
		$dataArrayInformation['school_id_arry'] = $school_id_arry;
		$dataArrayInformation['school_array'] = $school_array;
		$dataArrayInformation['subject_array'] = $subject_array;
		$dataArrayInformation['subject_id_array'] = $subject_id_array;
		return $dataArrayInformation;
	}
	/**/
	public function SubjectWiseGradeStudentNumber($academic_year){
		$school_id_array = $school_name_array = $subject_name_array = $subject_id_array  = $data_information_array = $data_detail_array = [];
		$criteria = new CDbCriteria;
		$criteria -> with = ['school'];
		$criteria -> select = 't.school_id, t.subject_id, t.subject_name, t.total_grade, count(student_id) as student_id';
		$criteria -> condition = 't.academic_year=:academic_year AND t.subject_status=:subject_status';
		$criteria -> params = [':academic_year'=>$academic_year, ':subject_status'=>1];
		$criteria -> group = 't.school_id, t.subject_id, t.total_grade';
		$student_report = $this->findAll($criteria);
		if(!empty($student_report)){
		foreach ($student_report as $report_info) {
			$school_id = $report_info->school_id;
			$subject_id = $report_info->subject_id;
			$grade = $report_info->total_grade;
			$student_number = $report_info->student_id;
			$data_information_array[$school_id][$subject_id][$grade] = $student_number;
			if(!in_array($report_info->school_id, $school_id_array)){
				$school_id_array[] = $report_info->school_id;
				$school_name_array[$report_info->school_id]['name'] = $report_info->school ? ucwords($report_info->school->title.', '.Yii::app()->params['municipality_short'].'-'.$report_info->school->ward_no) : '' ;
				$school_name_array[$report_info->school_id]['code'] = $report_info->school ? $report_info->school->schole_code : '' ;
			}
			if(!in_array($report_info->subject_id, $subject_id_array)){
				$subject_id_array[] = $report_info->subject_id;
				$subject_name_array[$report_info->subject_id] = strtoupper(substr($report_info->subject_name, 0, 3)).'.';
				}
			}
		}
		$data_detail_array['school_id_array'] = $school_id_array;
		$data_detail_array['school_name_array'] = $school_name_array;
		$data_detail_array['subject_id_array'] = $subject_id_array;
		$data_detail_array['subject_name_array'] = $subject_name_array;
		$data_detail_array['data_information_array'] = $data_information_array;
		$data_detail_array['grade_information'] = ['A<sup>+</sup>','A','B<sup>+</sup>','B','C<sup>+</sup>','C','D<sup>+</sup>','D','E','Abs<sup>*</sup>'];
		return $data_detail_array;
	}
	/**
	*
	*/
	public function StudentListSubjectGradeWise($params){
		$academic_year = isset($params['academic_year']) ? $params['academic_year'] : UtilityFunctions::AcademicYear();
		$school_id = isset($params['school_id']) ? $params['school_id'] : null;
		$subject_id = isset($params['subject_id']) ? $params['subject_id'] : null;
		$grade = isset($params['grade']) ? $params['grade'] : null;
		$plus = isset($params['plus']) ? $params['plus'] : null;
		if($plus && $plus == 1)
			$grade = rtrim($grade,'<sup> </sup>').'<sup>+</sup>';
		$criteria = new CDbCriteria;
		$criteria->with = ['student', 'school'];
		$criteria->condition = 't.academic_year=:academic_year AND t.subject_status=:subject_status';
		$criteria->params = [':academic_year'=>$academic_year, ':subject_status'=>1];
		if($school_id){
			$criteria->condition .= ' AND t.school_id=:school_id AND student.school_id=:school_id';
			$criteria->params = array_merge($criteria->params, [':school_id'=>$school_id]);

		}
		if($subject_id){
			$criteria->condition .= ' AND t.subject_id=:subject_id';
			$criteria->params = array_merge($criteria->params, [':subject_id'=>$subject_id]);

		}
		if($grade){
			$criteria->condition .= ' AND t.total_grade=:total_grade';
			$criteria->params = array_merge($criteria->params, [':total_grade'=>$grade]);
		}
		return $this->findAll($criteria);

	}

	public function SubjectInMarksLedger($params){
		$information_array = $subject_information = array();
        $user_information = UtilityFunctions::UserInformations();
        $school_id_sec = isset($user_information) ? $user_information['school_id'] : 0;
		$academic_year = isset($params['academic_year']) ? $params['academic_year'] : UtilityFunctions::AcademicYear();
		$ledger_of = isset($params['ledger_of']) ? $params['ledger_of'] : 'both'; // theory Or practical
		$result_ = isset($params['result_']) ? $params['result_'] : 'both'; // grade or numberic
		$school_id = isset($params['school_id']) ? $params['school_id'] : $school_id_sec;
		$order_by  = isset($params['arrange_by']) ? $params['arrange_by'] : 'name';
		$criteria_ = new CDbCriteria;
		$criteria_->condition = 'academic_year=:academic_year AND subject_status=:subject_status';
		$criteria_->params=[':academic_year'=>$academic_year, ':subject_status'=>1];
		if($school_id != 0 ){
			$criteria_ -> condition .=" AND t.school_id=:school_id ";
			$criteria_ -> params = array_merge($criteria_->params, array(':school_id'=>$school_id));
		}
		$criteria_->order = 'subject_order ASC';
		$criteria_->group = 'subject_id';
		$student_subject = $this->findAll($criteria_);
        $column[] ='school_name';
        $column[] ='school_code';
        $column[] ='symbol_number';
        $column[] ='name';
        $school_id_array = array();
        if(!empty($student_subject)){
            foreach ($student_subject as $information) {
                $theory_pass_mark = $information -> theory_pass_mark;
                $theory_full_mark = $information -> theory_full_mark;
                $practical_full_mark = $information -> practical_full_mark;
                $practical_pass_mark = $information -> practical_pass_mark;
                $subject_information[] = $information->subject_name.'-'.$theory_full_mark.'-'.$practical_full_mark;
                $theory_title = $information->is_practical && $information->is_practical !=0 ? '_theory' :'';
                $subject_name = UtilityFunctions::ColumnName($information -> subject_name);
                if(in_array($ledger_of, ['theory','both'])){
	                $column[] = $subject_name.$theory_title;
	                if($result_ == 'both'){
	                	$column[] = $subject_name.$theory_title.'_grade';
	                }

                }
                if($information->is_practical && $information->is_practical !=0){
                	if($ledger_of=='practical'){
	                    $column[] = $subject_name.'_practical';
	                	if($result_ == 'both')
	                		$column[] = $subject_name.'_practical_grade';
	                }
	                if($ledger_of == 'both'){
	                    $column[] = $subject_name.'_practical';
	                	if($result_ == 'both')
	                		$column[] = $subject_name.'_practical_grade';
	                    $column[]= $subject_name.'_total';
	                	if($result_ == 'both')
	                		$column[] = $subject_name.'_total_grade';
	                }

                }
                if(in_array($result_ ,['both', 'grade']) && $ledger_of == 'both'){
                	if($information->is_practical && $information->is_practical !=0)
                		$column[] = $subject_name.'_total_gp';
                	else
                		$column[] = $subject_name.'_gp';
                }


            }
        }
        if($result_=='general'){
            $column[] ='Total';
            $column[] ='Full_Mark';
            $column[] ='Result';
            $column[] = 'Rank';
            $column[] ='Percentage';
            $column[] ='Division';
        }
        if(in_array($result_, ['grade', 'both'])){
            $column[] ='Total';
            $column[] ='GPA';
        }
        $information_array['subject_detail'] = $subject_information;
        $information_array['column'] = $column;
        return $information_array;
	}

	public function SubjectFm($params){
        $user_information = UtilityFunctions::UserInformations();
        $school_id_sec = isset($user_information) ? $user_information['school_id'] : 0;
		$academic_year = isset($params['academic_year']) ? $params['academic_year'] : UtilityFunctions::AcademicYear();
		$school_id = isset($params['school_id']) ? $params['school_id'] : $school_id_sec;
		$subject_id = isset($params['subject_id']) ? $params['subject_id'] : null;
		$criteria = new CDbCriteria();
		$criteria -> condition = 'academic_year=:academic_year AND subject_id=:subject_id AND school_id=:school_id AND subject_status=:subject_status';
		$criteria -> params = [':academic_year'=>$academic_year, ':subject_id'=>$subject_id, ':school_id'=>$school_id, ':subject_status'=>1];
		$theory_full_mark = $practical_full_mark = 0;
		$obtained_mark_information = MarkObtained::model()->find($criteria);
		if(!empty($obtained_mark_information)){
			$theory_full_mark = $obtained_mark_information->theory_full_mark;
			$practical_full_mark = $obtained_mark_information->practical_full_mark;
		}
		$full_mark_information = PassMark::model()->findByAttributes(['subject_id'=>$subject_id, 'status'=>1]);
		if(!empty($full_mark_information)){
			$theory_full_mark = $full_mark_information->theory_full_mark;
			$practical_full_mark = $full_mark_information->practical_full_mark;
		}
		return $theory_full_mark;
	}

	public function DataentryReport(){
        $is_super_admin = UtilityFunctions::IsSuperAdmin();
        $criteria = new CDbCriteria;
        $criteria -> select = 'count(student_id) as student_id, subject_id, subject_name, DATE_FORMAT(created_date, "%Y-%m-%d")  as created_date, created_by';
        if(!$is_super_admin){
        	$criteria -> condition = 'created_by=:created_by';
        	$criteria -> params = [':created_by'=>Yii::app()->user->id];
        }
        $criteria -> group = 'created_by,subject_id, DATE_FORMAT(created_date,"%Y-%m-%d")';
        $DataentryReport = $this->findAll($criteria);
        $user_array = $data_entry_array = $date_array = [];
        if(!empty($DataentryReport)){
        	foreach ($DataentryReport as $data_entry) {
        		$user_id = $data_entry->created_by;
        		$date = $data_entry->created_date;
        		$student_number = $data_entry->student_id;
        		$subject_name = $data_entry->subject_name;
        		$user_information = Users::model()->findByPk($user_id);
        		$username = $user_information ? $user_information->username : '';
        		if(!empty($user_information) && !in_array($user_information->username, $user_array)){$user_array[] = $user_information->username;}
        		if(!in_array($date, $date_array)){$date_array[] = $date;}
        		$data_entry_array[$username][$date][$subject_name] = $student_number;


        	}
        }
        $data_entry_information['user_array'] = $user_array;
        $data_entry_information['data_entry_array'] = $data_entry_array;
        $data_entry_information['date_array'] = $date_array;
        return $data_entry_information;

	}

	public function ResultUpdate($student_id, $academic_year){
        $criteria = new CDbCriteria;
        $criteria -> select = "academic_year, terminal_id, terminal_, student_id, sum(practical_mark) as practical_mark, sum(theory_mark) as theory_mark, sum(theory_full_mark) as theory_full_mark, sum(practical_full_mark) as practical_full_mark ,sum(total_mark) as total_mark, sum(total_grade_point) as total_grade_point";
        $criteria -> condition = "academic_year=:academic_year AND student_id=:student_id";
        $criteria -> params = [':academic_year'=>$academic_year, ':student_id'=>$student_id];
        $criteria -> group = 'student_id';
        $student_information = MarkObtained::model()->find($criteria);
		$resultModel = Result::model()->findByAttributes(['academic_year'=>$academic_year,'result_type'=>'terminal', 'terminal_id'=>1 , 'student_id'=>$student_id]);

		if(!empty($resultModel) && !empty($student_information)){
			$theory_mark_total = $student_information -> theory_mark;
			$total_grade_point = $student_information -> total_grade_point;
			$total_full_mark = (int) $student_information -> theory_full_mark + (int) $student_information -> practical_full_mark;
			$total_obtained = $student_information -> total_mark;
			$percentage_raw = $total_full_mark!=0 ? $total_obtained*100/$total_full_mark : 0;
			$percentage = number_format($percentage_raw, 2);
			$resultModel -> total_obtained_mark = $total_obtained;
			$resultModel -> percentage = number_format($percentage, 2);
			$gpa = UtilityFunctions::GPACalculation($total_grade_point, $total_full_mark);
			$resultModel -> grade = $theory_mark_total != 0 ? UtilityFunctions::GradeLetter($gpa) : 'Abs<sup>*</sup>';
			$resultModel -> total_gpa = $theory_mark_total != 0 ? $gpa : 0;
			$resultModel -> approved_date = date('Y-m-d H:i:s');
			if(!$resultModel->update())
				return false;
			return true;
		}
		return true;
	}

}
