<?php

/**
 * This is the model class for table "terminal".
 *
 * The followings are the available columns in table 'terminal':
 * @property integer $class_terminal_id
 * @property string $title
 * @property integer $pre_for_terminal
 * @property integer $terminal_type
 * @property string $csa_percentage
 * @property integer $terminal_
 * @property integer $per_for_final
 * @property integer $status
 * @property integer $remarks
 */
class Terminal extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'terminal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, pre_for_terminal, status', 'required'),
			array('pre_for_terminal, terminal_type, terminal_, per_for_final, status, remarks', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>100),
			array('csa_percentage', 'length', 'max'=>6),
			//html purification
            array('class_terminal_id, title, pre_for_terminal, terminal_type, csa_percentage, terminal_, per_for_final, status, remarks', 'filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('class_terminal_id, title, pre_for_terminal, terminal_type, csa_percentage, terminal_, per_for_final, status, remarks', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'class_terminal_id' => 'Class Terminal',
			'title' => 'Title',
			'pre_for_terminal' => 'Pre For Terminal',
			'terminal_type' => 'Terminal Type',
			'csa_percentage' => 'Csa Percentage',
			'terminal_' => 'Terminal',
			'per_for_final' => 'Per For Final',
			'status' => 'Status',
			'remarks' => 'Remarks',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('class_terminal_id',$this->class_terminal_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('pre_for_terminal',$this->pre_for_terminal);
		$criteria->compare('terminal_type',$this->terminal_type);
		$criteria->compare('csa_percentage',$this->csa_percentage,true);
		$criteria->compare('terminal_',$this->terminal_);
		$criteria->compare('per_for_final',$this->per_for_final);
		$criteria->compare('status',$this->status);
		$criteria->compare('remarks',$this->remarks);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Terminal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
