<?php

/**
 * This is the model class for table "student_attendance".
 *
 * The followings are the available columns in table 'student_attendance':
 * @property integer $id
 * @property integer $academic_year
 * @property integer $terminal_id
 * @property integer $student_id
 * @property integer $attendance
 * @property integer $total_attendannce
 * @property integer $school_id
 * @property integer $created_by
 * @property string $created_date
 * @property integer $approved_by
 * @property string $approved_date
 */
class StudentAttendance extends CActiveRecord
{
	public $arrange_by;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'student_attendance';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('academic_year, terminal_id, student_id, attendance, total_attendannce, school_id', 'required'),
			array('academic_year, terminal_id, student_id, attendance, total_attendannce, school_id, created_by, approved_by', 'numerical', 'integerOnly'=>true),
			array('created_date, approved_date', 'safe'),
			//html purification
            array('id, academic_year, terminal_id, student_id, attendance, total_attendannce, school_id, created_by, created_date, approved_by, approved_date', 'filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, academic_year, terminal_id, student_id, attendance, total_attendannce, school_id, created_by, created_date, approved_by, approved_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'academic_year' => 'Academic Year',
			'terminal_id' => 'Terminal',
			'student_id' => 'Student',
			'attendance' => 'Attendance',
			'total_attendannce' => 'Total Attendannce',
			'school_id' => 'School',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'approved_by' => 'Approved By',
			'approved_date' => 'Approved Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('academic_year',$this->academic_year);
		$criteria->compare('terminal_id',$this->terminal_id);
		$criteria->compare('student_id',$this->student_id);
		$criteria->compare('attendance',$this->attendance);
		$criteria->compare('total_attendannce',$this->total_attendannce);
		$criteria->compare('school_id',$this->school_id);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('approved_by',$this->approved_by);
		$criteria->compare('approved_date',$this->approved_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StudentAttendance the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	*insert student Attendance in bulk amount
	*/
	public function StudentAttendanceSection($academic_year, $school_id, $terminal_id, $order_by){
		$total_attendannce=0;
		$criteria = new CDbCriteria;
		$criteria->condition = 't.academic_year=:academic_year AND t.status=:status AND t.school_id=:school_id';
		$criteria->params=[':academic_year'=>$academic_year, ':status'=>1,':school_id'=>$school_id];
		if($order_by=='name')
			$criteria -> order = 't.first_name ASC';
		if($order_by=='roll_number')
			$criteria -> order = 't.roll_number ASC';
		if($order_by=='section')
			$criteria -> order = 't.section ASC';
		$student_list = StudentInformation::model()->findAll($criteria);

		$student_attendance = array();
		$AttendanceInformation = StudentAttendance::model()->findAllByAttributes(['academic_year'=>$academic_year, 'terminal_id'=>$terminal_id, 'school_id'=>$school_id]);
		if(!empty($AttendanceInformation)){
			foreach ($AttendanceInformation as $information) {
				$student_attendance[$information->student_id] = $information -> attendance;
				$total_attendannce = $information -> total_attendannce;
			}
		}
		$student_details['student_list'] = $student_list;
		$student_details['student_attendance'] = $student_attendance;
		$student_details['total_attendannce'] = $total_attendannce;
		return $student_details;

	}
}
