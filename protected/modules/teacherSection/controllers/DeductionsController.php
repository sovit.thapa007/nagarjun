<?php

class DeductionsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	//public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Deductions;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if(isset($_POST['Deductions']))
		{
			$transaction = Yii::app()->db->beginTransaction();
			$error=array();

			$model->attributes=$_POST['Deductions'];
			$model->created_date=date('Y:m:d');
			if($model->save())
			{
					$string=$model->name;
		            $deduction=strtolower(str_replace(' ', '_',$string));
					$success = Yii::app()->db->createCommand()
		           ->addColumn('monthly_payroll', $deduction,'int NOT NULL default 0');

		           $reports=UtilityFunctions::reports();
		           foreach ($reports as $key => $value) 
		           {
		           		$report_id=Reports::model()->findByAttributes(array('type'=>$value))->id;
						//$report_allowances=ReportAllowance::model()->findAllByAttributes(array('report_id'=>$report_id));
			           	$report_deduction=new ReportDeduction();
			           	$report_deduction->report_id=$report_id;
			           	$report_deduction->dedcution_id=$model->id;
			           	$report_deduction->active=0;
			           	if(!$report_deduction->save())
			           	{
			           		$error[]='error';
			           	}
		           }

		          if(!in_array('error', $error))
		          {
		          	$transaction->commit();
		          }
		          else
		          {
		          	$transaction->rollback();
		          	$this->render('create',array(
							'model'=>new Deductions,
						));

		          }
				$this->redirect(array('view','id'=>$model->id));
			}
			else
			{
				print_r($model->errors);exit;
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Deductions']))
		{
			$model->attributes=$_POST['Deductions'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Deductions');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Deductions('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Deductions']))
			$model->attributes=$_GET['Deductions'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Deductions the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Deductions::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Deductions $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='deductions-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
