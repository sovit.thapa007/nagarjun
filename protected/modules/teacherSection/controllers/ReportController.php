<?php

class ReportController extends RController{

	

	public function filters(){
		return array(
				'rights',
			);
	}

	public function actionindex(){
			$this->render('index');

	}

	public function actionList($type)
	{
		$report_id=Reports::model()->findByAttributes(array('type'=>$type))->id;
		$report_allowances=ReportAllowance::model()->findAllByAttributes(array('report_id'=>$report_id,'active'=>1));
		$report_deductions=ReportDeduction::model()->findAllByAttributes(array('report_id'=>$report_id,'active'=>1));	

		$report_allowances_array=array();
		foreach ($report_allowances as $key => $value) 
		{
			$report_allowances_array[]=$value->allowance_id;
		}

		$report_deductions_array=array();
		foreach ($report_deductions as $key => $value) 
		{
			$report_deductions_array[]=$value->dedcution_id;
		}

		$allowances = Allowances::model()->findAll();
		$deductions=Deductions::model()->findAll();
		$this->render('list',['report_id'=>$report_id,'report_allowances'=>$report_allowances_array,'report_deductions'=>$report_deductions_array,'allowances'=>$allowances,'deductions'=>$deductions,'type'=>$type]);
	}

	public function actionactivateAllowance($allowance_id,$report_id,$type)
	{
		$report_allowance=ReportAllowance::model()->findByAttributes(array('report_id'=>$report_id,'allowance_id'=>$allowance_id));
		if($report_allowance == null)
		{
			$report_allowance= new ReportAllowance();
			$report_allowance->report_id=$report_id;
			$report_allowance->allowance_id=$allowance_id;
		}
		$report_allowance->active=1;
		$report_allowance->save();
		$this->redirect(array('list','type'=>$type));

	}

	public function actiondeactivateAllowance($allowance_id,$report_id,$type)
	{
		$report_allowance=ReportAllowance::model()->findByAttributes(array('report_id'=>$report_id,'allowance_id'=>$allowance_id));
		if($report_allowance == null)
		{
			$report_allowance= new ReportAllowance();
			$report_allowance->report_id=$report_id;
			$report_allowance->allowance_id=$allowance_id;
		}
		$report_allowance->active=0;
		$report_allowance->save();
		$this->redirect(array('list','type'=>$type));

	}

	public function actionactivateDeduction($deduction_id,$report_id,$type)
	{
		$report_deduction=ReportDeduction::model()->findByAttributes(array('report_id'=>$report_id,'dedcution_id'=>$deduction_id));
		if($report_deduction == null)
		{
			$report_deduction= new ReportDeduction();
			$report_deduction->report_id=$report_id;
			$report_deduction->dedcution_id=$deduction_id;
		}
		$report_deduction->active=1;
		$report_deduction->save();
		$this->redirect(array('list','type'=>$type));

	}

	public function actiondeactivateDeduction($deduction_id,$report_id,$type)
	{
		$report_deduction=ReportDeduction::model()->findByAttributes(array('report_id'=>$report_id,'dedcution_id'=>$deduction_id));
		if($report_deduction == null)
		{
			$report_deduction= new ReportDeduction();
			$report_deduction->report_id=$report_id;
			$report_deduction->dedcution_id=$deduction_id;
		}
		$report_deduction->active=0;
		$report_deduction->save();
		$this->redirect(array('list','type'=>$type));

	}



}

?>