<?php

class StudentController extends RController
{

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}
	/**
	*Current Student List , and assign section adn roll
	*/

	public function actionStudentDetails(){
		$classTitle = $academic_year = '';
		$model = new RegistrationHistory('search');
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['RegistrationHistory']))
			$model->attributes = $_GET['RegistrationHistory'];
		$model->current_status = 1;
		$class_id = $model->class_id;
		if($class_id){
			$classInformation = ClassInformation::model()->findByPk($class_id);
			$classTitle = !empty($classInformation) ?  $classInformation->title : "null";
		}
		$academic_year = $model->nepali_year;
		$this->render('_student_list', array(
			'model' => $model,'classTitle'=>$classTitle, 'academic_year'=>$academic_year
		));
	}

}
