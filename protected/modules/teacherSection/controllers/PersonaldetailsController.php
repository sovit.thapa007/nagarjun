<?php

class PersonaldetailsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','loadGrades'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','updateSalary'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{

		$model=new Personaldetails;
		$staff=new Staff;
		$level=new Level;
		$departmentStaff=new DepartmentStaff;
		$staffSpecial=new SpecialCaseStaff;
		$staffAllowance=new StaffAllowance;
		$staffDeduction=new StaffDeduction;

		$level1=Level::model()->findAllByAttributes(['parent_id'=>null]);
		$level2=Level::model()->findAll('parent_id !=:id',[':id'=>'null']);
		$shreni=Shreni::model()->findAll();
		$specialCase=StaffSpecialCase::model()->findAll();
		$allowance=Allowances::model()->findAll();
		$deduction=Deductions::model()->findAll();
		
		$officeTime = new StaffOfficeTime;
		$selfPf = new StaffSelfPf;
		$transaction = Yii::app()->db->beginTransaction();

	
		
				   
				    $departments=Department::model()->findAll();
					$array1[] = 'Select Post';
				    foreach ($departments as $key => $value)
				    {

				        $array1[$value->department_id] = $value->department_name;
				    }

				    foreach ($level1 as $key => $value)
				    {

				        $array2[$value->id] = $value->name;
				    }
				     foreach ($level2 as $key => $value)
				    {
				        $array3[$value->id] = $value->name;
				    }
				    $array4=array();
				    foreach ($shreni as $key => $value)
				    {
				        $array4[$value->id] = $value->name;

				    }
				    	$array5[] = 'Select Post';
				    


		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Personaldetails']) && isset($_POST['Staff']))
		{

			
			$model->attributes=$_POST['Personaldetails'];
			$staff->attributes=$_POST['Staff'];
			
			$model->contact_number=$staff->contact;
			$model->email=$staff->email;
			$staffFullName=$staff->fname.' '.$staff->lname;
			$model->full_name=$staffFullName;
			
			if($model->save())
			{
			
						
						if(isset($_POST['Staff']))
						{

							//print_r($staff->join_date); exit;
							//$staff->join_date = strtotime($staff->join_date);
							//$staff->designation_id = Designation::model()->findByAttributes(array('designation'=>strtolower($_POST['designation']), 'grade'=>$_POST['grades']))->id;
							$staff->created_date = time();
							$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
							$pass = array(); //remember to declare $pass as an array
							$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache

							for ($i = 0; $i < 8; $i++) 
							{
								$n = rand(0, $alphaLength);
								$pass[] = $alphabet[$n];
							}

				    		$password = implode($pass); //turn the array into a string
							//$staff->password = hash('sha256', (hash('sha256', $staff->created_date)).$password);
				    		$staff->password=hash('sha256', 'admin123');
				    		

							$staff->detail_id=$model->id;
							$staff->username = $staff->fname.$staff->lname;
                             $staff->teacher_type=1;            
							
							if($staff->save())
							{
								$check=true;
								$departmentStaff->attributes=$_POST['DepartmentStaff'];
								$array=$_POST['DepartmentStaff']['department_id'];
								
								foreach($array as $id)
								{

									$depStaff=new DepartmentStaff;
									$depStaff->attributes=$_POST['DepartmentStaff'];
									$depStaff->staff_id=$staff->staff_id;
									$depStaff->department_id=$id;

									if($depStaff->save())
									{

									}
									else
									{
										$check=false;
									}

								}


								/*if($check)
								{

										$staffSpecial->attributes=$_POST['SpecialCaseStaff'];
										$staffSpecial->staff_id=$staff->staff_id;
										if($staffSpecial->save())
										{	
											$array=$_POST['StaffAllowance']['allowanceAmounts'];
											foreach ($array as $key => $value) 
											{

												if($value != null)
												{
												
												
												$staffAll=new StaffAllowance;
												$staffAll->staff_id=$staff->staff_id;
												$staffAll->allowance_id=$key;
												$staffAll->percentage=$value;
												$staffAll->effective_date=time();
												if($staffAll->save())
												{

												}
												else
												{
													print_r($staffAll->errors);
													exit;

												}
												}
												
											}
											
										}
										else
										{
											
											$staffSpecial->errors;

											exit;

										}


										
										$deductionArray=$_POST['StaffDeduction']['deductionAmounts'];

										foreach ($deductionArray as $key => $value) 
											{


												if($value != null)
												{

													$staffDeductionOb=new StaffDeduction;
													
													$staffDeductionOb->staff_id=$staff->staff_id;
													
													$staffDeductionOb->deduction_id=$key;
													
													$staffDeductionOb->amount=$value;
											
														if($staffDeductionOb->save())
														{

														}
														else
														{
															print_r($staffDeductionOb->errors);
															exit;

														}
												}
											}

												

								}
								else
								{

									$departmentStaff->errors;

									exit;
								}*/
								/*$officeTime->attributes = $_POST['StaffOfficeTime'];
								$officeTime->staff_id = $staff->staff_id;
								$officeTime->effective_date = date('Y-m-d H:i', time());
								$officeTime->save();*/
								$transaction->commit();
								Yii::app()->user->setFlash('success', 'Staff has been created successfully');
								$this->redirect(array('view','id'=>$model->id));	
							}

							else
							{
								$transaction->rollback();
								echo "<pre>";
								echo "Error <br />";
								echo print_r($staff->errors);
								exit;
							}
							if(empty($_POST['designation']))
							{
								$staff->addError('fname', 'Designation cannot be empty');
							}
		}



			}
			else
			{
				echo "<pre>";
				echo "Personal Details <br />";
				echo print_r($model->errors);

				exit;
			}
		}
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		

	$this->render('create',array(
				'model'=>$model,
				'staff'=>$staff,
				'level'=>$level,
				'officeTime'=>$officeTime,	
				'array1'=>$array1,
				'array2'=>$array2,
				'array3'=>$array3,
				'array4'=>$array4,
				'array5'=>$array5,
				
				'staffSpecial'=>$staffSpecial,
				'departmentStaff'=>$departmentStaff,
				
			));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */


	public function actionloadGrades(){
		$data=Designation::model()->findAllByAttributes(array('designation'=>$_POST['desig']));
 		$data=CHtml::listData($data,'grade','grade');
   		foreach($data as $value=>$city_name)
   		echo CHtml::tag('option', array('value'=>$value),CHtml::encode($city_name),true);
	}


public function actionUpdateSalary($id)
	{
		$model=$this->loadModel($id);

		//$model=new Personaldetails;
		$staff=Staff::model()->findByAttributes(['detail_id'=>$id]);
			
		$level=new Level;


		//$departmentStaff=DepartmentStaff::model()->findAllByAttributes(['staff_id'=>$staff->staff_id]);
		$level=new level;
		$departmentStaff=new DepartmentStaff;
		$staffSpecial=new SpecialCaseStaff;

		$staffAllowanceNew=new StaffAllowance;
		$staffDeductionNew=new StaffDeduction;

		$level1=Level::model()->findAllByAttributes(['parent_id'=>null]);
		$level2=Level::model()->findAll('parent_id !=:id',[':id'=>'null']);
		$shreni=Shreni::model()->findAll();
		$specialCase=StaffSpecialCase::model()->findAll();
		$allowance=Allowances::model()->findAll();
		$deduction=Deductions::model()->findAll();
		
		$officeTime = new StaffOfficeTime;
		$selfPf = new StaffSelfPf;
		$transaction = Yii::app()->db->beginTransaction();

		$designations=Designation::model()->findAll(array(
		    'select'=>'t.designation',
		    'group'=>'t.designation',
		    'distinct'=>true,
		));

		$array = array();
	
    
				    foreach ($designations as $key => $value) 
				    {
				        $array[$value->designation] = $value->designation;
				    }
				    $departments=Department::model()->findAll();
					$array1[] = 'Select Post';
				    foreach ($departments as $key => $value)
				    {

				        $array1[$value->department_id] = $value->department_name;
				    }

				    foreach ($level1 as $key => $value)
    {
				
				        $array2[$value->id] = $value->name;
				    }
				     foreach ($level2 as $key => $value)
				    {
				        $array3[$value->id] = $value->name;
				    }
				    $array4=array();
				    foreach ($shreni as $key => $value)
				    {
				        $array4[$value->id] = $value->name;

				    }
				    	$array5[] = 'Select Special Case';
				    foreach ($specialCase as $key => $value)
				    {
				        $array5[$value->id] = $value->name;

				    }
				    
				    foreach ($allowance as $key => $value)
				    {
				        $array6[$value->allowanceId] = $value->allowanceName;

				    }
				     foreach ($deduction as $key => $value)
				    {

				        $array7[$value->id] = $value->name;

				    }


		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

	
		if(isset($_POST['Staff']))
		{


			
			
			$staff->attributes=$_POST['Staff'];
			
			$model->contact_number=$staff->contact;
			$model->email=$staff->email;
			$staffFullName=$staff->fname.' '.$staff->lname;
			$model->full_name=$staffFullName;
			
			
			
						
						if(isset($_POST['Staff']))
						{

							
							
							$staff->basic_salary=$_POST['Staff']['basic_salary'];
							$staff->grade=$_POST['Staff']['grade'];
							$staff->grade_amount=$_POST['Staff']['grade_amount'];

							if($staff->update())
							{
										$check=true;
										


								if($check)
								{


									
										$staffSpecialUpdate=SpecialCaseStaff::model()->findByAttributes(array('staff_id'=>$staff->staff_id));
											if($staffSpecialUpdate == null)
											{
												$staffSpecialUpdate=new SpecialCaseStaff();
												$staffSpecialUpdate->attributes=$_POST['SpecialCaseStaff'];
												$staffSpecialUpdate->staff_id=$staff->staff_id;
												$staffSpecialUpdate->created_date=date('Y:m:d');
											} 
											else
											{
												$staffSpecialUpdate->special_case_id=$_POST['SpecialCaseStaff']['special_case_id'];
											}

										
										if($staffSpecialUpdate->save())
										{	
											$array=$_POST['StaffAllowance']['allowanceAmounts'];
											foreach ($array as $key => $value) 
											{

												if($value != null)
												{
												


												$staffAll=StaffAllowance::model()->findByAttributes(array('allowance_id'=>$key,'staff_id'=>$staff->staff_id),array('order'=>'id DESC'));

												if($staffAll == null)
												{
														$staffAll=new StaffAllowance;
														$staffAll->staff_id=$staff->staff_id;
														$staffAll->allowance_id=$key;
														$staffAll->percentage=$value;
														$staffAll->effective_date=time();
												}
												else
												{

														$staffAll->percentage=$value;
														$staffAll->effective_date=time();
												}

 
												if($staffAll->save())
												{

												}
												else
												{

													print_r($staffAll->errors);
													exit;

												}
												}
												
											}
											
										}
										else
										{
											
											$staffSpecial->errors;

											exit;

										}


										
										$deductionArray=$_POST['StaffDeduction']['deductionAmounts'];

										foreach ($deductionArray as $key => $value) 
											{


												if($value != null)
												{
                                                                       
                                                    $staffDeductionOb=StaffDeduction::model()->findByAttributes(array('deduction_id'=>$key,'staff_id'=>$staff->staff_id),array('order'=>'id DESC'));    


		                                                    if($staffDeductionOb == null)
		                                                    {                           
															$staffDeductionOb=new StaffDeduction;
															$staffDeductionOb->staff_id=$staff->staff_id;
															$staffDeductionOb->deduction_id=$key;
															$staffDeductionOb->amount=$value;
															}
															else
															{

																$staffDeductionOb->amount=$value;

															}
											
														if($staffDeductionOb->save())
														{
 
														}
														else
														{
															print_r($staffDeductionOb->errors);
															exit;

														}
												}
											}

												

								}
								else
								{

									$departmentStaff->errors;

									exit;
								}
								
								$transaction->commit();
								Yii::app()->user->setFlash('success', 'Staff has been updated successfully');
								$this->redirect(array('staff/SpecialSalarySheet','id'=>$model->id));	
							}

							else
							{
								$transaction->rollback();
								echo "<pre>";
								echo "RHis sis <br />";
								echo print_r($staff->errors);
								exit;
							}
							if(empty($_POST['designation']))
							{
								$staff->addError('fname', 'Designation cannot be empty');
							}
		}



			
			else
			{
				echo "<pre>";
				echo "Personal Details <br />";
				echo print_r($model->errors);

				exit;
			}
		}
		
$staffDeduction=array();
    $deduction=Deductions::model()->findAll();
    foreach($deduction as $name)
    {
   	

    	$staffDeduction[$name->id]=StaffDeduction::model()->findByAttributes(array('deduction_id'=>$name->id,'staff_id'=>$staff->staff_id),array('order'=>'id DESC'));
   	}



   	$staffAllowance=array();
    $allowance=Allowances::model()->findAll();
    foreach($allowance as $name)
    {
   	

    	$staffAllowance[$name->allowanceId]=StaffAllowance::model()->findByAttributes(array('allowance_id'=>$name->allowanceId,'staff_id'=>$staff->staff_id),array('order'=>'id DESC'));
   	}

	$this->render('updateSalary',array(
				'model'=>$model,
				'staff'=>$staff,
				
				'officeTime'=>$officeTime,	
				'array'=>$array,
				'array1'=>$array1,
				'array2'=>$array2,
				'array3'=>$array3,
				'array4'=>$array4,
				'array5'=>$array5,
				'array6'=>$array6,
				'array7'=>$array7,
				'staffSpecial'=>$staffSpecial,
				'departmentStaff'=>$departmentStaff,
				'staffAllowance'=>$staffAllowance,
				'staffDeduction'=>$staffDeduction,
				'staffDeductionNew'=>$staffDeductionNew,
				'staffAllowanceNew'=>$staffAllowanceNew,
			));
	}

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		//$model=new Personaldetails;
		$staff=Staff::model()->findByAttributes(['detail_id'=>$id]);
		$departmentList= DepartmentStaff::model()->findAllByAttributes(array('staff_id'=>$staff->staff_id));
		$level=new Level;


		//$departmentStaff=DepartmentStaff::model()->findAllByAttributes(['staff_id'=>$staff->staff_id]);
		$level=new level;
		$departmentStaff=new DepartmentStaff;
		$staffSpecial=new SpecialCaseStaff;

		$staffAllowance=new StaffAllowance;
		$staffDeduction=new StaffDeduction;

		$level1=Level::model()->findAllByAttributes(['parent_id'=>null]);
		$level2=Level::model()->findAll('parent_id !=:id',[':id'=>'null']);
		$shreni=Shreni::model()->findAll();
		$specialCase=StaffSpecialCase::model()->findAll();
		$allowance=Allowances::model()->findAll();
		$deduction=Deductions::model()->findAll();
		
		$officeTime = new StaffOfficeTime;
		$selfPf = new StaffSelfPf;
		$transaction = Yii::app()->db->beginTransaction();

		
		$selectedPost=array();
    
				   
				    $departments=Department::model()->findAll();
					$array1[] = 'Select Post';
				    foreach ($departments as $key => $value)
				    {

				        $array1[$value->department_id] = $value->department_name;

				        foreach ($staff->departmentList() as $keys => $depval) 
				        {
				        	if($depval->department_id == $value->department_id)
				        	{
				        		 $selectedPost[$value->department_id]=array('selected'=>true);
				        		 break;
				        	}
				        }
				       
				       
				    }

				    foreach ($level1 as $key => $value)
				    {

				        $array2[$value->id] = $value->name;
				    }
				     foreach ($level2 as $key => $value)
				    {
				        $array3[$value->id] = $value->name;
				    }
				    $array4=array();
				    foreach ($shreni as $key => $value)
				    {
				        $array4[$value->id] = $value->name;

				    }
				    
				    


		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

	
		if(isset($_POST['Personaldetails']) && isset($_POST['Staff']))
		{

			
			$model->attributes=$_POST['Personaldetails'];
			$staff->attributes=$_POST['Staff'];
			
			$model->contact_number=$staff->contact;
			$model->email=$staff->email;
			$staffFullName=$staff->fname.' '.$staff->lname;
			$model->full_name=$staffFullName;
			
			if($model->update())
			{
			
						
						if(isset($_POST['Staff']))
						{

							//$staff->join_date = strtotime($staff->join_date);
							//$staff->designation_id = Designation::model()->findByAttributes(array('designation'=>strtolower($_POST['designation']), 'grade'=>$_POST['grades']))->id;
							$staff->created_date = time();
							$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
							$pass = array(); //remember to declare $pass as an array
							$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache

							for ($i = 0; $i < 8; $i++) 
							{
								$n = rand(0, $alphaLength);
								$pass[] = $alphabet[$n];
							}

                                                        $password = implode($pass); //turn the array into a string
                                                                //$staff->password = hash('sha256', (hash('sha256', $staff->created_date)).$password);
                                                        $staff->password=hash('sha256', 'admin123');
							$staff->detail_id=$model->id;
							$staff->username = $staff->fname.$staff->lname;
							

							if($staff->update())
							{
								
								
								if(!isset($_POST['DepartmentStaff']))
								{

								}
								else 
								{	

										$deleteDepartmentStaff=DepartmentStaff::model()->findAllByAttributes(array('staff_id'=>$staff->staff_id));
										if($deleteDepartmentStaff != null)
										{
											foreach ($deleteDepartmentStaff as $key) 
											{
											
												$key->deleteRecord();	
											}
										}


										$departmentStaff->attributes=$_POST['DepartmentStaff'];
										$array=$_POST['DepartmentStaff']['department_id'];
										
										foreach($array as $id)
										{

											$depStaff=new DepartmentStaff;
											
													$depStaff->attributes=$_POST['DepartmentStaff'];
													$departCheck=DepartmentStaff::model()->findByAttributes(array('staff_id'=>$staff->staff_id,'department_id'=>$id));
													if($departCheck == null)
													{

															$depStaff->staff_id=$staff->staff_id;
															$depStaff->department_id=$id;

															if($depStaff->save())
															{

																	
															}
															else
															{
																$check=false;
															}
													}
											

										}
									}
								$transaction->commit();
								$this->redirect(array('view','id'=>$model->id));
							}

							else
							{

								$transaction->rollback();
								echo "<pre>";
								echo "RHis sis <br />";
								echo print_r($staff->errors);
								exit;
							}
							
				}

			

			}


				else
				{
					echo "<pre>";
					echo "Personal Details <br />";
					echo print_r($model->errors);

					exit;
				}
		}
		

	$this->render('update',array(
				'model'=>$model,
				'staff'=>$staff,
				'departmentList'=>$departmentList,
				'officeTime'=>$officeTime,	
				
				'array1'=>$array1,
				'array2'=>$array2,
				'array3'=>$array3,
				'array4'=>$array4,
				
				'staffSpecial'=>$staffSpecial,
				'departmentStaff'=>$departmentStaff,
				'staffAllowance'=>$staffAllowance,
				'staffDeduction'=>$staffDeduction,
				'selectedPost'=>$selectedPost
			));
	}
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
            $model=$this->loadModel($id);
            $model->status=0;
            if($model->update())
            {
                $staff=Staff::model()->findByAttributes(['detail_id'=>$model->id]);
                $staff->active=0;
                $staff->update(); 
                
            }

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Personaldetails');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Personaldetails('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Personaldetails']))
			$model->attributes=$_GET['Personaldetails'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Personaldetails the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Personaldetails::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Personaldetails $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='personaldetails-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
