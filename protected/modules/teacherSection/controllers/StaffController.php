<?php

class StaffController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	//public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			//'rights', // perform access control for CRUD operations
			'accessControl',
			'postOnly + delete', // we only allow deletion via POST request
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$superadmins = Staff::model()->findAllByAttributes(array('role'=>'superadmin'), array('select'=>'username'));
	    	foreach ($superadmins as $value) {
	    		$superadmin[] = $value->username;
	    	}
	    	$excos = Staff::model()->findAllByAttributes(array('role'=>'exco'), array('select'=>'username'));
	    	foreach ($excos as $value) {
	    		$exco[] = $value->username;
	    	}
		return array(
			
			
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('staff_view',array(
			'model'=>Staff::model()->with('officeTime')->findByPk($id),
			'id'=>$id,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Staff;
		$officeTime = new StaffOfficeTime;
		$selfPf = new StaffSelfPf;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$designations=Designation::model()->findAll(array(
		    'select'=>'t.designation',
		    'group'=>'t.designation',
		    'distinct'=>true,
		));

		$array = array();
    
	    foreach ($designations as $key => $value) {
	        $array[$value->designation] = $value->designation;
	    }
	    $departments=Department::model()->findAll();

		$array1[] = 'Select department';
    
		    foreach ($departments as $key => $value) {
		        $array1[$value->department_id] = $value->department_name;
		    }

		if(isset($_POST['Staff']) && !empty($_POST['designation']))
		{
			$model->attributes=$_POST['Staff'];
			$model->join_date = strtotime($model->join_date);
			$model->designation_id = Designation::model()->findByAttributes(array('designation'=>strtolower($_POST['designation']), 'grade'=>$_POST['grades']))->id;
			$model->created_date = time();
			$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
			$pass = array(); //remember to declare $pass as an array
			$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
			for ($i = 0; $i < 8; $i++) {
				$n = rand(0, $alphaLength);
				$pass[] = $alphabet[$n];
			}
    		$password = implode($pass); //turn the array into a string
			$model->password = hash('sha256', (hash('sha256', $model->created_date)).$password);
			if($model->save()){
				$model->username = 'TPC'.Yii::app()->db->getLastInsertId();
				$model->save();
				$officeTime->attributes = $_POST['StaffOfficeTime'];
				$officeTime->staff_id = $model->staff_id;
				$officeTime->effective_date = date('Y-m-d H:i', time());
				$officeTime->save();
				Yii::app()->user->setFlash('success', 'Staff has been created successfully');
				$this->redirect('../Password/sendPassword?email='.$model->email.'&password='.$password);
			}
			if(empty($_POST['designation'])){
				$model->addError('fname', 'Designation cannot be empty');
			}
		}
		$this->render('create',array(
			'model'=>$model,
			'officeTime'=>$officeTime,
			'array'=>$array,
			'array1'=>$array1,
		));
	}

	/**
	 * adds allowance to staff through ajax request and sends response
	 * @return [boolean] [true on success and false on failure]
	 */
	public function actionStaffAllowance($id)
	{
		$transaction = Yii::app()->db->beginTransaction();
		$model = new StaffAllowance;
		$model1 = new Allowances;

		if(isset($_POST['allowance-btn']))
		{
			foreach ($_POST as $aid => $percentage) 
					{
				if($id != 'allowance-btn'){
					$model = new StaffAllowance;
					$model->staff_id = $id;
					$model->allowance_id = $aid;
					$model->percentage = $percentage;
					$model->effective_date = date('Y-m-d H:i:m', time());
					if(!$model->save()){
						$transaction->rollback();
						Yii::app()->user->setFlash('error', 'The system encountered an error.');
						print_r($model->getErrors());
						exit;
						$this->redirect('/Staff/view/'.$id);
					}
				}
			}
			$transaction->commit();
		}
		$this->render('staffAllowance', array('model'=>$model, 'model1'=>$model1, 'id'=>$id));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{

		$model=$this->loadModel($id);
		$officeTime = StaffOfficeTime::model()->findByAttributes(array('staff_id'=>$id), array('order'=>'effective_date DESC'));
		if(empty($officeTime))
		{
			$officeTime = new StaffOfficeTime;
		}

		$designation = Designation::model()->findByPk($model->designation_id);
		$model->designation = $designation->designation;
		$model->grade = $designation->grade;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$designations=Designation::model()->findAll(array(
		    'select'=>'t.designation',
		    'group'=>'t.designation',
		    'distinct'=>true,
		));
		$array = array();
    
		    foreach ($designations as $key => $value) {
		        $array[$value->designation] = $value->designation;
		    }

		    $departments=Department::model()->findAll();

		$array1[] = 'Select department';
    
		    foreach ($departments as $key => $value) {
		        $array1[$value->department_id] = $value->department_name;
		    }
		if(isset($_POST['Staff']))
		{
			$model->attributes=$_POST['Staff'];
			if(!empty($officeTime->startTime)){
			 	if($officeTime->startTime != $_POST['StaffOfficeTime']['startTime'] || $officeTime->endTime != $_POST['StaffOfficeTime']['endTime']){
					$officeTime->attributes = $_POST['StaffOfficeTime'];
					$officeTime->staff_id = $id;
					$officeTime->effective_date = date('Y-m-d H:i', time());
					$officeTime->save();
				}
			}else{
				$officeTime->attributes = $_POST['StaffOfficeTime'];
				$officeTime->staff_id = $id;
				$officeTime->effective_date = date('Y-m-d H:i', time());
				if($officeTime->save()){

				}else{
					print_r($officeTime->getErrors());
					exit;
				}
			}
			$model->join_date = strtotime($model->join_date);
			if (empty($_POST['designation']) && empty($_POST['grade']))
			{
				Yii::app()->user->setFlash('error', 'Designation and/or grade cannot be blank');
				$this->redirect(Yii::app()->createUrl('Staff/Update/'.$id));
			}
			$model->designation_id = Designation::model()->findByAttributes(array('designation'=>strtolower($_POST['designation']), 'grade'=>$_POST['grades']))->id;
			if($model->save())
			{
				Yii::app()->user->setFlash('success', 'The staff details has been updated successfully.');
				$this->redirect(array('admin','id'=>$model->staff_id));
			}
		}
		$this->render('update',array(
			'model'=>$model,
			'array'=>$array,
			'officeTime'=>$officeTime,
			'array1' => $array1,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */

	public function actionDelete($id)
	{
		$model = new Staff;
		$model->deactivateStaff($id);
		$model->save();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Staff');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Staff('search');
		$officeTime = new StaffOfficeTime;
		$model->unsetAttributes();  // clear any default values
		$designations=Designation::model()->findAll(array(
		    'select'=>'t.designation',
		    'group'=>'t.designation',
		    'distinct'=>true,
		));
		$array = array();
    
		    foreach ($designations as $key => $value) {

		        $array[$value->designation] = $value->designation;
		    }

		    $departments=Department::model()->findAll();
		$array1[] = 'Select department';
    
		    foreach ($departments as $key => $value) {
		        $array1[$value->department_id] = $value->department_name;
		    }
		if(isset($_GET['Staff'])){
			//echo "<pre>";
			//print_r($_GET);
			if(isset($_GET['Staff']['department_id']) && $_GET['Staff']['department_id'] == 0){
				$_GET['Staff']['department_id'] = '';
			}
			$model->attributes=$_GET['Staff'];
			$model->join_date = strtotime($model->join_date);
			if (!empty($_GET['designation']) && empty($_GET['grade'])){
				$model->designation_id = Designation::model()->findByAttributes(array('designation'=>strtolower($_GET['designation']), 'grade'=>$_GET['grades']))->id;
			}
		}

		$this->render('admin',array(
			'model'=>$model,
			'array'=>$array,
			'officeTime'=>$officeTime,
			'array1' => $array1,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Staff the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Staff::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Staff $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='staff-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/**
	 * Allows users to edit their details
	 * @param  [int] $id id of the currently logged in user
	 */
	public function actionEdit($id){
		$model = Staff::model()->findByPk($id);
		if(isset($_POST['Staff'])){
			$model->attributes = $_POST['Staff'];
			$selfpf =  StaffSelfPf::model()->findByAttributes(array('staff_id'=>$id), array('order'=>'effective_date DESC'));
			if(empty($selfpf) ||  $selfpf->amount != $_POST['Staff']['selfPf']){
				$selfpf = new StaffSelfPf;
				$selfpf->staff_id = $id;
				$selfpf->amount = $_POST['Staff']['selfPf'];
				$selfpf->effective_date = date('Y-m-d H:i:m', time());
				if(!$selfpf->save()){
					print_r($selfpf->getErrors());
					exit;
				}
			} 
			$rand = rand(0,9999);
                		$uploadedFile = CUploadedFile::getInstance($model,'profile_pic');
                		$fileName = "{$rand}-{$uploadedFile}";
                		if(!empty($_FILES['Staff']['name']['profile_pic'])){
                			$model->profile_pic = $fileName;
                		}else{
                			$model1 = Staff::model()->findByPk($id); 
                			$model->profile_pic = $model1->profile_pic;
                		}
 			$model->contact = $_POST['Staff']['contact'];
                		if($model->save()){
                			if(!empty($uploadedFile)){
                				$uploadedFile->saveAs(Yii::app()->basePath.'/../images/user-pics/'.$fileName);
                				$image = new EasyImage(Yii::app()->basePath.'/../images/user-pics/'.$fileName);
                				$image->resize(500, 500);
                				$image->crop(300, 300);
					$image->save(Yii::app()->basePath.'/../images/user-pics/'.$fileName);
                			}
                			Yii::app()->user->setFlash('success', 'User details has been edited successfully.');
				$this->redirect(Yii::app()->createUrl('Site'));
			}
		}
		$this->render('edit', array('model'=>$model));
	}

	public function actionloadGrades(){
		$data=Designation::model()->findAllByAttributes(array('designation'=>$_POST['desig']));
 		$data=CHtml::listData($data,'grade','grade');
   		foreach($data as $value=>$city_name)
   		echo CHtml::tag('option', array('value'=>$value),CHtml::encode($city_name),true);
	}

	public function actionAttendanceReport(){
		$this->layout = "//layouts/column2";
		$month = array(
			'' => '--Select a month',
			'1' => 'January',
			'2' => 'Febrauary',
			'3' => 'March',
			'4' => 'April',
			'5' => 'May',
			'6' => 'June',
			'7' => 'July',
			'8' => 'August',
			'9' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December',
		);
		$year = array(''=>'--Select a year--');
		for ($i=1984; $i <= date('Y', time()); $i++) { 
			$year[$i] = $i;
		}
		$this->render('../attendance/attendanceCalendar', array('months'=>$month, 'year'=>$year));
	}

	public function actionAttendanceStatistics($id = 0){
		if($id == 0){
			$id = Yii::app()->session['uid'];
		}
		$attendance = Attendance::model()->findAllByAttributes(array('staff_id'=>$id));
		$this->render('../attendance/attendanceReports', array('attendance'=>$attendance, 'id'=>$id));
	}

	public function actionAttendanceDetail($id = 0){
		if($id == 0){
			$id = Yii::app()->session['uid'];
		}
		if(isset($_POST['Attendance'])){
			$post = $_POST['Attendance'];
			if(!empty($_POST['attendance-id'])){
				$attendance = Attendance::model()->findByPk((int) $_POST['attendance-id']);
			}else{
				$attendance = new Attendance;
				$attendance->staff_id = $id;
			}
			$attendance->attributes = $post;
			//echo "<pre>";
			//print_r($attendance);
			
			if(!empty($post['login'])){
				$attendance->login = strtotime($_POST['date'] . ' ' .$post['login']);
				if(strtotime($post['login']) <= strtotime(date('Y-m-d', time()) . ' ' . StaffOfficeTime::model()->findByAttributes(array('staff_id'=>$id), array('order'=>'effective_date DESC'))->start_time)){
					$attendance->login_status = 'On time';
				}else{
					$attendance->login_status = 'Late';
				}
			}
			if(!empty($post['logout'])){
				$attendance->logout = strtotime($_POST['date'] . ' ' . $post['logout']);
				$userLogoutTime = StaffOfficeTime::model()->findByAttributes(array('staff_id'=>$id), array('order'=>'effective_date DESC'))->end_time;
				if($attendance->logout <=  strtotime(date('Y-m-d', time()) . ' ' . $userLogoutTime)){
					$attendance->logout_status = 'Early';
				}else{
					$attendance->logout_status = 'On time';
				}
			}
			if($attendance->save()){
				Yii::app()->user->setFlash('success', 'The attendance has been updated succesfully');
			}else{
				echo "<pre>";
				print_r($attendance->getErrors());
				exit;
				Yii::app()->user->setFlash('error', 'Something went wrong. Please try again later.');
			}
		}
		$attendance = Attendance::model()->findAllByAttributes(array('staff_id'=>$id));
		$this->render('../attendance/attendanceDetail', array('attendance'=>$attendance, 'id'=>$id));
	}

	/**
	 * generates pdf of the weekly attendance report for a staff
	 * @param  integer $id id of the staff
	 */
	public function actionPrintWeekAttendance($id = 0){
		$model = new Attendance;
		$attendance = $model->thisWeekSearch1($id);
		$mPDF1 = Yii::app()->ePdf->mpdf();		
		$mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
		$mPDF1->WriteHTML($this->renderPartial('../attendance/weeklyAttendancepdf', array('id'=>$id, 'model'=>$model, 'attendances'=>$attendance), true, true));
		$mPDF1->Output();
	}

	public function actionCustomAttendancePdf(){
		$model = new Attendance;
		$id = Yii::app()->session['id'];
		$attendance = $model->thisWeekSearch1(Yii::app()->session['id'], Yii::app()->session['from'], Yii::app()->session['to']);
		$mPDF1 = Yii::app()->ePdf->mpdf();		
		$mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
		$mPDF1->WriteHTML($this->renderPartial('../attendance/weeklyAttendancepdf', array('id'=>$id, 'model'=>$model, 'attendances'=>$attendance), true, true));
		$mPDF1->Output();

	}

	/**
	 * generates pdf of the weekly attendance report of a department
	 * @param  integer $id department id
	 */
	public function actionDepartWeeklyAttendance($id = 0){
		$model = new Attendance;
		if($id == 0){
			$id = Yii::app()->session['depart'];
		}
		$attendance = $model->departWeeklyAttendance($id);
		$mPDF1 = Yii::app()->ePdf->mpdf();		
		$mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
		$mPDF1->WriteHTML($this->renderPartial('../attendance/weeklyAttendancepdf', array('id'=>$id, 'model'=>$model, 'attendances'=>$attendance), true, true));
		$mPDF1->Output();
	}

	/**
	 * Display all the allowances allocated to the staff
	 * @param  [int] $id [id of the staff]
	 */
	public function actionViewAllowance($id){
		$model = new StaffAllowance;
		$allowances = $model->getStaffAllowances($id);
		$this->render('viewAllowance', array('id'=>$id, 'allowances'=>$allowances));
	}

	public function actionAddAllowance(){
		if(isset($_GET['allowance'])){
			$model = new StaffAllowance;
			$allowance = (int) $_GET['allowance'];
			$percentage = $_GET['percentage'];
			$model->staff_id = $_GET['id'];
			$model->allowance_id = $allowance;
			$model->percentage = $percentage;
			$model->effective_date = date('Y-m-d H:i:m', time());
			if($model->save()){
				echo "true";
			}else{
				echo "false";
			}
		}
	}

public function actionTSalarySheet()
{
	$this->layout = "//layouts/column1";
		$staffs = '';
		$model = new Staff;
		$specialCase=new StaffSpecialCase;
		$unique='';
		$special = '';
		$array=array();
		if(isset($_POST['special']))
		{
			if($_POST['special'] == null)
			{
	
				$array=Staff::model()->findAllByAttributes(['active'=>1]);

			}
			else
			{
            $special=$_POST['special'];
            $staffSpecialId=SpecialCaseStaff::model()->findAllByAttributes(array('special_case_id'=>$special));
			$i=0;
			foreach($staffSpecialId as $data)
			{

				$a=Staff::model()->findByAttributes(array('staff_id'=>$data->staff_id, 'active'=>'1'));
				if($a != null)
				{
					$array[$i]=$a;
					$i++;
				}
			}
		}	
			$unique=array_map("unserialize", array_unique(array_map("serialize", $array)));

			/*echo "<pre>";
			print_r($array);
			exit;*/
			/*$staffs = Staff::model()->findAllByAttributes(array('department_id'=>$_POST['depart'], 'active'=>'1'));
			$depart = $_POST['depart'];*/
		}
			
		

		$this->render('tSalarySheet', array('staffs'=>$unique, 'model'=>$model, 'special'=>$special));
		
}




	public function actionSpecialSalarySheet()
	{
		$this->layout = "//layouts/column1";
		
		$staffs = '';	
		$model = new Staff;
		$specialCase=new StaffSpecialCase;
		$unique='';
		$special = '';
		$array=array();



		if(isset($_POST['print']))
		{
			$special=$_POST['print'];
			$this->pdfSpecialSalary($model,$special);
			
		}




		if(isset($_POST['special'] ))
		{
			if($_POST['special'] != '')
			{
									
						//$type=$_POST['type'];

						if(isset($_POST['special']))
						{
										if($_POST['special'] == null)
										{
								
												//$array=Staff::model()->findAllByAttributes(['active'=>1]);

										}
										else
										{
									            $special=$_POST['special'];
									            $staffSpecialId=SpecialCaseStaff::model()->findAllByAttributes(array('special_case_id'=>$special));
												$i=0;
												foreach($staffSpecialId as $data)
												{

													$a=Staff::model()->findByAttributes(array('staff_id'=>$data->staff_id, 'active'=>'1'));
													if($a != null)
													{
														$array[$i]=$a;
														$i++;
													}
												}
										}
										
										$unique=array_map("unserialize", array_unique(array_map("serialize", $array)));

						}

				}

			}
			
		

		$this->render('specialSalarySheet', array('staffs'=>$unique, 'model'=>$model, 'special'=>$special,'specialDropdown'=>true));
		
		
	}

	public function actionPartTimeLedger()
	{
		$staff=Staff::model()->findAllByAttributes(array('teacher_type'=>2));
		if(isset($_POST['month']))
		{
			$month=$_POST['month'];
			
		}
		else
		{
			$month=0;
		}

		
		$this->render('partTimeLedger',array(
			'staffs'=>$staff,
			'month'=>$month,
			));

	}


	



	public function actionBankSalarySheet()	
	{
		$this->layout = "//layouts/column1";
		$staffs = '';
		$model = new Staff;
		$specialCase=new StaffSpecialCase;
		$unique='';
		$special = '';
		$array=array();
		if(isset($_POST['special']))
		{
			if($_POST['special'] == null)
			{
	
				$array=Staff::model()->findAllByAttributes(['active'=>1]);

			}
			else
			{
            $special=$_POST['special'];
            $staffSpecialId=SpecialCaseStaff::model()->findAllByAttributes(array('special_case_id'=>$special));
			$i=0;
			foreach($staffSpecialId as $data)
			{

				$a=Staff::model()->findByAttributes(array('staff_id'=>$data->staff_id, 'active'=>'1'));
				if($a != null)
				{
					$array[$i]=$a;
					$i++;
				}
			}
		}
			
			$unique=array_map("unserialize", array_unique(array_map("serialize", $array)));

			/*echo "<pre>";
			print_r($array);
			exit;*/
			/*$staffs = Staff::model()->findAllByAttributes(array('department_id'=>$_POST['depart'], 'active'=>'1'));
			$depart = $_POST['depart'];*/
		}
			
		

		$this->render('bankSalarySheet', array('staffs'=>$unique, 'model'=>$model, 'special'=>$special));
		
	}

	public function actionDepartmentSalarySheet(){	

		$this->layout = "//layouts/column1";
		$staffs = '';
		$model = new Staff;
		$depart = '';

		$array=array();


		if(isset($_POST['print']))
		{
			$depart=$_POST['print'];
			$this->pdfDepartmentSalary($model,$depart);
			
		}


if(isset($_POST['type'] ))
{
		if($_POST['type'] == '')
		{
						if(isset($_POST['depart']))
						{

							if($_POST['depart'] == null)
							{

								$array=Staff::model()->findAllByAttributes(['active'=>1]);

							}
							else
							{
						            $depart=$_POST['depart'];
									$departstaff=DepartmentStaff::model()->findAllByAttributes(array('department_id'=>$_POST['depart']));


									$i=0;
									foreach($departstaff as $data)
									{
										
										$a=Staff::model()->findByAttributes(array('staff_id'=>$data->staff_id, 'active'=>'1'));

										if($a != null)
										{
											$array[$i]=$a;
											$i++;
											
										}
									}
							}
						}
			}
			else
			{

				if(isset($_POST['depart']))
						{
							$type=$_POST['type'];

							if($_POST['depart'] == null)
							{

								$array=Staff::model()->findAllByAttributes(['active'=>1,'teacher_type'=>$type]);

							}
							else
							{
						            $depart=$_POST['depart'];
									$departstaff=DepartmentStaff::model()->findAllByAttributes(array('department_id'=>$_POST['depart']));


									$i=0;
									foreach($departstaff as $data)
									{
										
										$a=Staff::model()->findByAttributes(array('staff_id'=>$data->staff_id, 'active'=>'1','teacher_type'=>$type));

										if($a != null)
										{
											$array[$i]=$a;
											$i++;
											
										}
									}
								}
						}

			}
		}
			
			/*echo "<pre>";
			print_r($array);
			exit;*/
			/*$staffs = Staff::model()->findAllByAttributes(array('department_id'=>$_POST['depart'], 'active'=>'1'));
			$depart = $_POST['depart'];*/
		
			
		

		$this->render('specialSalarySheet', array('staffs'=>$array, 'model'=>$model, 'depart'=>$depart,'departmentDropdown'=>true));
	}

	public function actionPayrollSheet($id)
	{
		$model=new Staff;

		$staff = Staff::model()->findByPk($id);
		$gtotal = 0;

		
		$this->render('payrollSheet', array('staff'=>$staff,'model'=>$model));
	}

	public function actionStaffPayroll(){
		$model = new Staff;
		if(isset($_GET['Staff'])){
			$model->attributes = $_GET['Staff'];
		}
		$this->render('staffPayrollList', array('model'=>$model));
	}



	public function actionSaveMonthlySalary()
	{
		$this->layout = "//layouts/column1";
		$staffs = '';
		$model = new Staff;
		$specialCase=new StaffSpecialCase;
		$unique='';
		$special = '';
		$array=array();
		
		if(isset($_POST['searchmonth']) && isset($_POST['searchyear']) && $_POST['searchmonth']!= null && $_POST['searchyear']!= null)
		{
			$month=$_POST['searchmonth'];
			$year=$_POST['searchyear'];
		}
		else
		{
			$month=null;
			$year=null;
		}

		if(isset($_POST['month']) && isset($_POST['year']) && isset($_POST['save-submission']) && $_POST['month']!= null && $_POST['year']!= null)
		{
			$month=$_POST['month'];
			$year=$_POST['year'];
			$special=$_POST['save-special'];
			$save=true;
		}
		else
		{
			
			$save=false;
		}

		if(isset($_POST['special']))
		{ 
				if($_POST['special'] == null)
				{
		
						$array=Staff::model()->findAllByAttributes(['active'=>1]);

				}
				else
				{
			            $special=$_POST['special'];
			            $staffSpecialId=SpecialCaseStaff::model()->findAllByAttributes(array('special_case_id'=>$special));
						$i=0;
						foreach($staffSpecialId as $data)
						{

							$a=Staff::model()->findByAttributes(array('staff_id'=>$data->staff_id, 'active'=>'1'));
							if($a != null)
							{
								$array[$i]=$a;
								$i++;
							}
						}
				}
				
				$unique=array_map("unserialize", array_unique(array_map("serialize", $array)));

		}

		$this->render('specialSalarySheet', array('save'=>$save,'month'=>$month,'year'=>$year,'staffs'=>$unique, 'model'=>$model, 'special'=>$special,'saveMonthlySalary'=>true));


	}


	public function actionUpdateSalary($id)
	{


		$model=$this->loadModel($id);

		//$model=new Personaldetails;
		$staff=Staff::model()->findByAttributes(['detail_id'=>$id]);
		
		$level=new Level;


		//$departmentStaff=DepartmentStaff::model()->findAllByAttributes(['staff_id'=>$staff->staff_id]);
		$level=new level;
		$departmentStaff=new DepartmentStaff;
		$staffSpecial=new SpecialCaseStaff;

		$staffAllowance=new StaffAllowance;
		$staffDeduction=new StaffDeduction;

		$level1=Level::model()->findAllByAttributes(['parent_id'=>null]);
		$level2=Level::model()->findAll('parent_id !=:id',[':id'=>'null']);
		$shreni=Shreni::model()->findAll();
		$specialCase=StaffSpecialCase::model()->findAll();
		$allowance=Allowances::model()->findAll();
		$deduction=Deductions::model()->findAll();
		
		$officeTime = new StaffOfficeTime;
		$selfPf = new StaffSelfPf;
		$transaction = Yii::app()->db->beginTransaction();

		$designations=Designation::model()->findAll(array(
		    'select'=>'t.designation',
		    'group'=>'t.designation',
		    'distinct'=>true,
		));

		$array = array();
	
    
				    foreach ($designations as $key => $value) 
				    {
				        $array[$value->designation] = $value->designation;
				    }
				    $departments=Department::model()->findAll();
					$array1[] = 'Select department';
				    foreach ($departments as $key => $value)
				    {

				        $array1[$value->department_id] = $value->department_name;
				    }

				    foreach ($level1 as $key => $value)
				    {

				        $array2[$value->id] = $value->name;
				    }
				     foreach ($level2 as $key => $value)
				    {
				        $array3[$value->id] = $value->name;
				    }
				    $array4=array();
				    foreach ($shreni as $key => $value)
				    {
				        $array4[$value->id] = $value->name;

				    }
				    	$array5[] = 'Select Special Case';
				    foreach ($specialCase as $key => $value)
				    {
				        $array5[$value->id] = $value->name;

				    }
				    
				    foreach ($allowance as $key => $value)
				    {
				        $array6[$value->allowanceId] = $value->allowanceName;

				    }
				     foreach ($deduction as $key => $value)
				    {

				        $array7[$value->id] = $value->name;

				    }


		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

	
		if(isset($_POST['Personaldetails']) && isset($_POST['Staff']))
		{

			
			$model->attributes=$_POST['Personaldetails'];
			$staff->attributes=$_POST['Staff'];
			
			$model->contact_number=$staff->contact;
			$model->email=$staff->email;
			$staffFullName=$staff->fname.' '.$staff->lname;
			$model->full_name=$staffFullName;
			
			if($model->update())
			{
			
						
						if(isset($_POST['Staff']))
						{

							
							$staff->join_date = strtotime($staff->join_date);
							//$staff->designation_id = Designation::model()->findByAttributes(array('designation'=>strtolower($_POST['designation']), 'grade'=>$_POST['grades']))->id;
							$staff->created_date = time();
							$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
							$pass = array(); //remember to declare $pass as an array
							$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache

							for ($i = 0; $i < 8; $i++) 
							{
								$n = rand(0, $alphaLength);
								$pass[] = $alphabet[$n];
							}

                                                        $password = implode($pass); //turn the array into a string
                                                                //$staff->password = hash('sha256', (hash('sha256', $staff->created_date)).$password);
                                                        $staff->password=hash('sha256', 'admin123');
							$staff->detail_id=$model->id;
							$staff->username = $staff->fname.$staff->lname;
							$staff->basic_salary=$_POST['Staff']['basic_salary'];
							$staff->grade=$_POST['Staff']['grade'];
							$staff->grade_amount=$_POST['Staff']['grade_amount'];

							if($staff->update())
							{
								
								$check=true;
								$departmentStaff->attributes=$_POST['DepartmentStaff'];
								$array=$_POST['DepartmentStaff']['department_id'];
								
								foreach($array as $id)
								{

									$depStaff=new DepartmentStaff;
									$depStaff->attributes=$_POST['DepartmentStaff'];
									$departCheck=DepartmentStaff::model()->findByAttributes(array('staff_id'=>$staff->staff_id,'department_id'=>$id));
									if($departCheck == null)
									{
											$depStaff->staff_id=$staff->staff_id;
											$depStaff->department_id=$id;

											if($depStaff->save())
											{

											}
											else
											{
												$check=false;
											}
									}
								}


								if($check)
								{

										$staffSpecial->attributes=$_POST['SpecialCaseStaff'];
										$staffSpecial->staff_id=$staff->staff_id;
										if($staffSpecial->save())
										{	
											$array=$_POST['StaffAllowance']['allowanceAmounts'];
											foreach ($array as $key => $value) 
											{

												if($value != null)
												{
												
												
												$staffAll=new StaffAllowance;
												$staffAll->staff_id=$staff->staff_id;
												$staffAll->allowance_id=$key;
												$staffAll->percentage=$value;
												$staffAll->effective_date=time();
												if($staffAll->save())
												{

												}
												else
												{
													print_r($staffAll->errors);
													exit;

												}
												}
												
											}
											
										}
										else
										{
											
											$staffSpecial->errors;

											exit;

										}


										
										$deductionArray=$_POST['StaffDeduction']['deductionAmounts'];

										foreach ($deductionArray as $key => $value) 
											{


												if($value != null)
												{
                                                                                                      
													$staffDeductionOb=new StaffDeduction;
													$staffDeductionOb->staff_id=$staff->staff_id;
													$staffDeductionOb->deduction_id=$key;
													$staffDeductionOb->amount=$value;
											
														if($staffDeductionOb->save())
														{

														}
														else
														{
															print_r($staffDeductionOb->errors);
															exit;

														}
												}
											}

												

								}
								else
								{

									$departmentStaff->errors;

									exit;
								}
								$officeTime->attributes = $_POST['StaffOfficeTime'];
								$officeTime->staff_id = $staff->staff_id;
								$officeTime->effective_date = date('Y-m-d H:i', time());
								$officeTime->save();
								$transaction->commit();
								Yii::app()->user->setFlash('success', 'Staff has been created successfully');
								$this->redirect(array('view','id'=>$model->id));	
							}

							else
							{
								$transaction->rollback();
								echo "<pre>";
								echo "RHis sis <br />";
								echo print_r($staff->errors);
								exit;
							}
							if(empty($_POST['designation']))
							{
								$staff->addError('fname', 'Designation cannot be empty');
							}
		}



			}
			else
			{
				echo "<pre>";
				echo "Personal Details <br />";
				echo print_r($model->errors);

				exit;
			}
		}
		

	$this->render('updateSalary',array(
				'model'=>$model,
				'staff'=>$staff,
				
				'officeTime'=>$officeTime,	
				'array'=>$array,
				'array1'=>$array1,
				'array2'=>$array2,
				'array3'=>$array3,
				'array4'=>$array4,
				'array5'=>$array5,
				'array6'=>$array6,
				'array7'=>$array7,
				'staffSpecial'=>$staffSpecial,
				'departmentStaff'=>$departmentStaff,
				'staffAllowance'=>$staffAllowance,
				'staffDeduction'=>$staffDeduction,
			));
	}

	public function actionUpdateStaffAllowance(){
		if(isset($_GET['id']) && isset($_GET['rate'])){
			$staffAllowance = StaffAllowance::model()->findByPk($_GET['id']);
			/*echo "<pre>";
			print_r($staffAllowance);
			exit;*/
			if($staffAllowance->percentage != $_GET['rate']){
				$allowance = new StaffAllowance;
				$allowance->staff_id = $staffAllowance->staff_id;
				$allowance->allowance_id = $staffAllowance->allowance_id;
				$allowance->percentage = $_GET['rate'];
				$allowance->effective_date = date('Y-m-d H:i:m', time());
				if($allowance->save()){
					echo "true";
				}else{
					echo "false";
				}
			}
		}
	}

	public function actionDeleteStaffAllowance($id){
		$allowance = new StaffAllowance;
		$staffAllowance = StaffAllowance::model()->findByPk($_GET['id']);
		$allowance->staff_id = $staffAllowance->staff_id;
		$allowance->allowance_id = $staffAllowance->allowance_id;
		$allowance->percentage = 0;
		$allowance->effective_date = date('Y-m-d H:i:m', time());
		if($allowance->save()){
			Yii::app()->user->setFlash('success', 'Allowance deleted succesfully');
			$this->redirect(Yii::app()->createUrl('/Staff/viewAllowance/'.$staffAllowance->id));
		}
	}




	public function pdfDepartmentSalary($model,$depart)
    {



			if($depart == null)
			{

				$array=Staff::model()->findAllByAttributes(['active'=>1]);

			}
			else
			{
				$departstaff=DepartmentStaff::model()->findAllByAttributes(array('department_id'=>$depart));


			$i=0;
			foreach($departstaff as $data)
			{
				
				$a=Staff::model()->findByAttributes(array('staff_id'=>$data->staff_id, 'active'=>'1'));

				if($a != null)
				{
					$array[$i]=$a;
					$i++;
					
				}
			}
		}
		
		$unique=array_map("unserialize", array_unique(array_map("serialize", $array)));
        $mPDF1 = Yii::app()->ePdf->mpdf();
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
        $mPDF1->WriteHTML($this->renderPartial('print/printDepartmentSalary', array('staffs'=>$array, 'model'=>$model, 'depart'=>$depart),true));

 
         $mPDF1->Output();
 
	
 
       
    }

    public function pdfSpecialSalary($model,$special)
    {



			if($special == null)
			{

				$array=Staff::model()->findAllByAttributes(['active'=>1]);

			}
			else
			{
				 	$staffSpecialId=SpecialCaseStaff::model()->findAllByAttributes(array('special_case_id'=>$special));
					$i=0;
					foreach($staffSpecialId as $data)
					{

						$a=Staff::model()->findByAttributes(array('staff_id'=>$data->staff_id, 'active'=>'1'));
						if($a != null)
						{
							$array[$i]=$a;
							$i++;
						}
					}
			}
		
			
		$unique=array_map("unserialize", array_unique(array_map("serialize", $array)));



        # mPDF
        $mPDF1 = Yii::app()->ePdf->mpdf();
 
        # You can easily override default constructor's params
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
 
        // # render (full page)
         $mPDF1->WriteHTML($this->renderPartial('print/printSpecialSalary', array('staffs'=>$unique, 'model'=>$model, 'special'=>$special),true));
 
        // # Load a stylesheet
        // $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/main.css');
        // $mPDF1->WriteHTML($stylesheet, 1);
 
        // # renderPartial (only 'view' of current controller)
        // $mPDF1->WriteHTML($this->renderPartial('index', array(), true));
 
        // # Renders image
        // $mPDF1->WriteHTML(CHtml::image(Yii::getPathOfAlias('webroot.css') . '/bg.gif' ));
 
        // # Outputs ready PDF
         $mPDF1->Output();
 
        // ////////////////////////////////////////////////////////////////////////////////////
 
        // # HTML2PDF has very similar syntax
        // $html2pdf = Yii::app()->ePdf->HTML2PDF();
        // $html2pdf->WriteHTML($this->renderPartial('index', array(), true));
        // $html2pdf->Output();
 
       
    }

    public function actionUpdateDetails($id)
    {
    	$special_cases=StaffSpecialCase::model()->findAll();
    	$staff=Staff::model()->findByPk($id);
    	$allowances=Allowances::model()->findAll();
    	$deductions=Deductions::model()->findAll();
    	$plus2Allowances=Plus2Allowances::model()->findAll();
    	$this->render('updateDetails',array(
				'special_cases'=>$special_cases,
				'allowances'=>$allowances,
				'deductions'=>$deductions,
				'id'=>$id,
				'staff'=>$staff,
				'plus2Allowances'=>$plus2Allowances
			));
    }

    public function actionSaveAllDepartmentDetails()
    {
    	
    	
    	$allSpecialCases=isset($_POST['allcases'])?$_POST['allcases']:null;
    	$staff_id=isset($_POST['staff_id'])?$_POST['staff_id']:null;
    	//print_r($_POST); exit;
    	if($allSpecialCases == null || $staff_id == null)
    	{
    		Yii::app()->user->setFlash('error', "No Department Selected to Save");
    		$this->redirect(array('updateDetails','id'=>$staff_id));
    	}
    	foreach ($allSpecialCases as $key => $value) 
    	{
    		$salaries=$_POST['payroll'][$value]['salaries'];
    		$allowances=$_POST['payroll'][$value]['allowances'];
    		if(isset($_POST['payroll'][$value]['pAllowances']))
    			$pAllowances=$_POST['payroll'][$value]['pAllowances']; 
    		else
    			$pAllowances=null;
    		$deductions=$_POST['payroll'][$value]['deductions'];
    		$staff=Staff::model()->findByPk($staff_id);
    		if($staff == null) throw new CHttpException(404,'The specified staff is not registered or not found.');
    		$specialCasestaff=SpecialCaseStaff::model()->findByAttributes(array('staff_id'=>$staff_id,'special_case_id'=>$value));
    		if($specialCasestaff == null)
    		{
    			$specialCasestaff=new SpecialCaseStaff();
    			$specialCasestaff->staff_id=$staff_id;
    			$specialCasestaff->special_case_id=$value;
    		}
    		if($salaries['basicSalary'] != null)
    				$specialCasestaff->basic_salary=$salaries['basicSalary'];
    		if($salaries['grade'] != null)
    				$specialCasestaff->grade=$salaries['grade'];
    		if($salaries['gradeAmount'] != null)
    				$specialCasestaff->grade_amount=$salaries['gradeAmount'];

    		$this->saveAllowances($allowances,$staff_id,$value); 
    		$this->saveDeductions($deductions,$staff_id,$value); 
    		if($pAllowances!=null)
    		{
					$this->savePlus2Allowances($pAllowances,$staff_id);	
    		}
    		$specialCasestaff->save();
    	}

    	Yii::app()->user->setFlash('success', "Information Saved");

    	$this->redirect(array('updateDetails','id'=>$staff_id));

    }

    public function saveAllowances($allowances,$staff_id,$special_case_id)
    {
    	$allowances=array_filter($allowances, function($var){return !$var == null;} );

    	if($allowances != null)
    	{
	    	foreach ($allowances as $key => $value) 
	    	{
	    		$staffAllowance=StaffAllowance::model()->findByAttributes(array('staff_id'=>$staff_id,'special_case_id'=>$special_case_id,'allowance_id'=>$key));
	    		if($staffAllowance == null)
	    		{
	    			$staffAllowance=new StaffAllowance();
	    			$staffAllowance->staff_id=$staff_id;
	    			$staffAllowance->allowance_id=$key;
	    			$staffAllowance->special_case_id=$special_case_id;
	    			$staffAllowance->effective_date=time();
	    		}
	    		$staffAllowance->percentage=$value;
	    		$staffAllowance->save();
	    	}
	    }


    }

     public function savePlus2Allowances($allowances,$staff_id)
    {
    	$allowances=array_filter($allowances, function($var){return !$var == null;} );

    	if($allowances != null)
    	{
	    	foreach ($allowances as $key => $value) 
	    	{
	    		$staffAllowance=StaffPlus2Allowances::model()->findByAttributes(array('staff_id'=>$staff_id,'allowance_id'=>$key));
	    		if($staffAllowance == null)
	    		{
	    			$staffAllowance=new StaffPlus2Allowances();
	    			$staffAllowance->staff_id=$staff_id;
	    			$staffAllowance->allowance_id=$key;
	    		}
	    		$staffAllowance->days=$value['days'];
	    		$staffAllowance->amount=$value['amount'];
	    		if(!$staffAllowance->save())
	    		{
	    				print_r($staffAllowance->errors); exit;
	    		}
	    	}
	    }


    }

    public function saveDeductions($deductions,$staff_id,$special_case_id)
    {
    	$deductions=array_filter($deductions, function($var){return !$var == null;} );

    	foreach ($deductions as $key => $value) 
    	{
    		$staffDeduction=StaffDeduction::model()->findByAttributes(array('staff_id'=>$staff_id,'special_case_id'=>$special_case_id,'deduction_id'=>$key));
    		if($staffDeduction == null)
    		{
    			$staffDeduction=new StaffDeduction();
    			$staffDeduction->staff_id=$staff_id;
    			$staffDeduction->deduction_id=$key;
    			$staffDeduction->special_case_id=$special_case_id;
    		}
    		$staffDeduction->amount=$value;
    		$staffDeduction->save();
    	}
    	

    }
    public function actionRemoveStaffFromDepartment($id,$special_case_id)
    {
    	$model=SpecialCaseStaff::model()->findByAttributes(array('staff_id'=>$id,'special_case_id'=>$special_case_id));
    	if($model->delete())
    	{
    		$allowances=StaffAllowance::model()->findAllByAttributes(array('staff_id'=>$id,'special_case_id'=>$special_case_id));
    		$deductions=StaffDeduction::model()->findAllByAttributes(array('staff_id'=>$id,'special_case_id'=>$special_case_id));
    		$p2Allowances=StaffPlus2Allowances::model()->findAllByAttributes(array('staff_id'=>$id));

    		foreach ($allowances as $key => $value) 
    		{
    			$value->delete();
    		}
    		foreach ($deductions as $key => $value) {
    			$value->delete();
    		}
    		foreach ($p2Allowances as $key => $value) {
    			$value->delete();
    		}

    		Yii::app()->user->setFlash('success', "Department Removed");
    	}
    	else
    	{
    		Yii::app()->user->setFlash('error', "Department was not Removed! Some error occured");
    	}

    	$this->redirect(array('updateDetails','id'=>$id));


    }




    
}
