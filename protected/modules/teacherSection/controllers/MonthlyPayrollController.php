<?php

class MonthlyPayrollController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','monthlySheet','library','monthlySalarySheet','accountEntry','IndividualSalarySheet','governmentGrant','privateFund','annex','higherSecondary','bankSheet','pfreport','cifreport'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MonthlyPayroll;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['MonthlyPayroll']))
		{
			$model->attributes=$_POST['MonthlyPayroll'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MonthlyPayroll']))
		{
			$model->attributes=$_POST['MonthlyPayroll'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('MonthlyPayroll');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new MonthlyPayroll('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MonthlyPayroll']))
			$model->attributes=$_GET['MonthlyPayroll'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MonthlyPayroll the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MonthlyPayroll::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MonthlyPayroll $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='monthly-payroll-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}



public function pdfMonthlySalary($model,$year,$month,$callBy,$special=null)
    {

    	if($callBy == 'ledger')
    	{
    		$viewFile='print/printMonthlyLedger';
    	}
    	elseif($callBy == 'sheet')
    	{
    		$viewFile='print/printMonthlySalarySheet';
    	}


	
		if(isset($special))
		{
		
				$month=$month;
				$year=$year;
				$special=$special;
				$monthlySheet=MonthlyPayroll::model()->findAllByAttributes(array('month'=>$month, 'year'=>$year,'department_id'=>$special));

		}
		else
		{
			$monthlySheet=MonthlyPayroll::model()->findAllByAttributes(array('month'=>$month, 'year'=>$year));
		}


        $mPDF1 = Yii::app()->ePdf->mpdf();
        $mPDF1->AddPage('L');

        
        $mPDF1->WriteHTML($this->renderPartial($viewFile, array('staffs'=>$monthlySheet, 'model'=>$model,'month'=>$month,'year'=>$year,'special'=>$special),true));

         $mPDF1->Output();
 
	
 
       
    }

	public function actionMonthlySheet()
	{
		$this->layout = "//layouts/column1";
		$model = new Staff;
		$specialCase=new StaffSpecialCase;
		$monthlySheet=null;
		$month=null;
		$year=null;
		$array=array();
		$special=null;


		if(isset($_POST['printYear']) && isset($_POST['printMonth']))
		{
			$printMonth=$_POST['printMonth'];
			$printYear=$_POST['printYear'];
			if(isset($_POST['printSpecial']))
			{
			$printSpecial=$_POST['printSpecial'];
			$this->pdfMonthlySalary($model,$printYear,$printMonth,'ledger',$printSpecial);
			}
			else
			{
				$this->pdfMonthlySalary($model,$printYear,$printMonth,'ledger');
			}
			

		}

		if(isset($_POST['month']) && isset($_POST['year']))
		{

		
				$month=$_POST['month'];
				$year=$_POST['year'];
				$monthlySheet=MonthlyPayroll::model()->findAllByAttributes(array('month'=>$month, 'year'=>$year));
		}

		if(isset($_POST['month']) && isset($_POST['year']) && isset($_POST['special']))
		{
		
			if($_POST['special'] != '')
			{
		
				$month=$_POST['month'];
				$year=$_POST['year'];
				$special=$_POST['special'];
				$monthlySheet=MonthlyPayroll::model()->findAllByAttributes(array('month'=>$month, 'year'=>$year,'department_id'=>$special));
			}
		
		}


		if(isset($_POST['month']) && isset($_POST['year']) && isset($_POST['special']) && isset($_POST['type']))
		{
		

			if($_POST['special'] != '' && $_POST['type'] != '')
			{
				$month=$_POST['month'];
				$year=$_POST['year'];
				$special=$_POST['special'];
				$type= $_POST['type'];
				$monthlySheet=MonthlyPayroll::model()->findAllByAttributes(array('month'=>$month, 'year'=>$year,'special_case_id'=>$special,'teacher_type'=>$type));
			}
			elseif($_POST['type'] != '')
			{
					$month=$_POST['month'];
				$year=$_POST['year'];
				$special=$_POST['special'];
				$type= $_POST['type'];
				$monthlySheet=MonthlyPayroll::model()->findAllByAttributes(array('month'=>$month, 'year'=>$year,'teacher_type'=>$type));

			}

		}

		$this->render('monthlySheet', array('staffs'=>$monthlySheet, 'model'=>$model,'month'=>$month,'year'=>$year,'special'=>$special));
		
	}




public function actionIndividualSalarySheet($id)
{
	$this->layout = "//layouts/column1";
		$model = new Staff;
		$specialCase=new StaffSpecialCase;
		$monthlySheet=null;
		$month=null;
		$year=null;
		$array=array();


		$individualStaff = Staff::model()->findByPk($id);

		if(isset($_POST['printYear']) && isset($_POST['printMonth']))
		{
			$printMonth=$_POST['printMonth'];
			$printYear=$_POST['printYear'];
			if(isset($_POST['printSpecial']))
			{
			$printSpecial=$_POST['printSpecial'];
			$this->pdfMonthlySalary($model,$printYear,$printMonth,'sheet',$printSpecial);
			}
			else
			{
				$this->pdfMonthlySalary($model,$printYear,$printMonth,'sheet');
			}
			

		}


	if(isset($_POST['year']))
	{

				$year=$_POST['year'];
				

	}


	if(isset($_POST['month']) && isset($_POST['year']))
		{


		
				$month=$_POST['month'];
				$year=$_POST['year'];
			
				if($_POST['month'] == '')
				{
				$monthlySheet=MonthlyPayroll::model()->findAllByAttributes(array('staff_id'=>$id,'year'=>$year));
				}
				else
				{
				$monthlySheet=MonthlyPayroll::model()->findAllByAttributes(array('staff_id'=>$id,'month'=>$month, 'year'=>$year));
				}
		}
		$this->render('individualSheet', array('individualStaff'=>$individualStaff,'staffs'=>$monthlySheet, 'model'=>$model,'month'=>$month,'year'=>$year));
		
	

}
public function actionMonthlySalarySheet()
{
	
			
		$this->layout = "//layouts/column1";
		$model = new Staff;
		$specialCase=new StaffSpecialCase;
		$monthlySheet=null;
		$month=null;
		$year=null;
		$array=array();
		$special=null;
		if(isset($_POST['printYear']) && isset($_POST['printMonth']))
		{
			$printMonth=$_POST['printMonth'];
			$printYear=$_POST['printYear'];
			if(isset($_POST['printSpecial']))
			{
			$printSpecial=$_POST['printSpecial'];
			$this->pdfMonthlySalary($model,$printYear,$printMonth,'sheet',$printSpecial);
			}
			else
			{
				$this->pdfMonthlySalary($model,$printYear,$printMonth,'sheet');
			}
			

		}



	if(isset($_POST['month']) && isset($_POST['year']))
		{

		
				$month=$_POST['month'];
				$year=$_POST['year'];
				$monthlySheet=MonthlyPayroll::model()->findAllByAttributes(array('month'=>$month, 'year'=>$year));
		}

		if(isset($_POST['month']) && isset($_POST['year']) && isset($_POST['special']))
		{
		
			if($_POST['special'] != '')
			{
		
				$month=$_POST['month'];
				$year=$_POST['year'];
				$special=$_POST['special'];
				$monthlySheet=MonthlyPayroll::model()->findAllByAttributes(array('month'=>$month, 'year'=>$year,'department_id'=>$special));
			}
		
		}


		



		$this->render('monthlySalarySheet', array('staffs'=>$monthlySheet, 'model'=>$model,'month'=>$month,'year'=>$year,'special'=>$special));
		
	}





public function pdfReportsMonthlySalary($model,$year,$month,$callBy,$special=null,$report_allowances,$report_deductions)
    {

    	$viewFile=$callBy;
    	$monthlySheet=array();

		if(isset($special))
		{
				$month=$month;
				$year=$year;
				$special=$special;
				$allMonthly=MonthlyPayroll::model()->findAllByAttributes(array('month'=>$month, 'year'=>$year));
				if($allMonthly != null)
				{
							$i=0;
							foreach ($allMonthly as $value) 
							{
									$department=StaffSpecialCase::model()->findByPk($value->department_id);
									if($department != null)
									{
											if($department->type==$special)
											{
												$monthlySheet[$i]=$value;

												$i++;
											}
									}
							}
				}
		}
		else
		{
			$monthlySheet=MonthlyPayroll::model()->findAllByAttributes(array('month'=>$month, 'year'=>$year));
		}


        $mPDF1 = Yii::app()->ePdf->mpdf();
        $mPDF1->AddPage('L');

        
        $mPDF1->WriteHTML($this->renderPartial($viewFile, array('staffs'=>$monthlySheet, 'model'=>$model,'month'=>$month,'year'=>$year,'special'=>$special,'report_allowances'=>$report_allowances,'report_deductions'=>$report_deductions),true));

         $mPDF1->Output();
       
    }

	public function actionGovernmentGrant()
	{
		$report_id=Reports::model()->findByAttributes(array('type'=>'government'))->id;
		$report_allowances=ReportAllowance::model()->findAllByAttributes(array('report_id'=>$report_id,'active'=>1));
		$report_deductions=ReportDeduction::model()->findAllByAttributes(array('report_id'=>$report_id,'active'=>1));

		$this->layout = "//layouts/column1";
		$model = new Staff;
		$specialCase=new StaffSpecialCase;
		$monthlySheet=null;
		$month=null;
		$year=null;
		$special='government';
		$monthlySheet=array();
		if(isset($_POST['printYear']) && isset($_POST['printMonth']))
		{
			$printMonth=$_POST['printMonth'];
			$printYear=$_POST['printYear'];
			
			
			$this->pdfReportsMonthlySalary($model,$printYear,$printMonth,'printFiveReports/governmentGrant','government',$report_allowances,$report_deductions);
			
		}

	if(isset($_POST['month']) && isset($_POST['year']))
		{
		
				$month=$_POST['month'];
				$year=$_POST['year'];
				$allMonthly=MonthlyPayroll::model()->findAllByAttributes(array('month'=>$month, 'year'=>$year));

				if($allMonthly != null)
				{
							$i=0;

							foreach ($allMonthly as $value) 
							{

									
									$department=StaffSpecialCase::model()->findByPk($value->department_id);

									if($department != null)
									{
											if($department->type=="government")
											{
												$monthlySheet[$i]=$value;

												$i++;
											}
									}
							}
				}
		}

		
		$this->render('fiveReports/governmentGrant', array('staffs'=>$monthlySheet, 'model'=>$model,'month'=>$month,'year'=>$year,'special'=>$special,'report_allowances'=>$report_allowances,'report_deductions'=>$report_deductions));
		


	}


	public function actionPrivateFund()
	{
		$report_id=Reports::model()->findByAttributes(array('type'=>'private'))->id;
		$report_allowances=ReportAllowance::model()->findAllByAttributes(array('report_id'=>$report_id,'active'=>1));
		$report_deductions=ReportDeduction::model()->findAllByAttributes(array('report_id'=>$report_id,'active'=>1));

		$this->layout = "//layouts/column1";
		$model = new Staff;
		$specialCase=new StaffSpecialCase;
		$monthlySheet=null;
		$month=null;
		$year=null;
		
		$special='Private Fund';
		$monthlySheet=array();
		if(isset($_POST['printYear']) && isset($_POST['printMonth']))
		{
			$printMonth=$_POST['printMonth'];
			$printYear=$_POST['printYear'];
			
			
			$this->pdfReportsMonthlySalary($model,$printYear,$printMonth,'printFiveReports/privateFund','private',$report_allowances,$report_deductions);
			
		}




	if(isset($_POST['month']) && isset($_POST['year']))
		{
		
				$month=$_POST['month'];
				$year=$_POST['year'];
				$allMonthly=MonthlyPayroll::model()->findAllByAttributes(array('month'=>$month, 'year'=>$year));

				if($allMonthly != null)
				{
							$i=0;

							foreach ($allMonthly as $value) 
							{

									
									$department=StaffSpecialCase::model()->findByPk($value->department_id);

									if($department != null)
									{
											if($department->type=="private")
											{
												$monthlySheet[$i]=$value;

												$i++;
											}
									}


							}


				}


		}

		
		$this->render('fiveReports/privateFund', array('staffs'=>$monthlySheet, 'model'=>$model,'month'=>$month,'year'=>$year,'special'=>$special,'report_allowances'=>$report_allowances,'report_deductions'=>$report_deductions));
		


	}



	





	public function actionlibrary()
	{

		$report_id=Reports::model()->findByAttributes(array('type'=>'library'))->id;
		$report_allowances=ReportAllowance::model()->findAllByAttributes(array('report_id'=>$report_id,'active'=>1));
		$report_deductions=ReportDeduction::model()->findAllByAttributes(array('report_id'=>$report_id,'active'=>1));
		$this->layout = "//layouts/column1";
		$model = new Staff;
		$specialCase=new StaffSpecialCase;
		$monthlySheet=null;
		$month=null;
		$year=null;
		
		$special='Library';
		$monthlySheet=array();


		if(isset($_POST['printYear']) && isset($_POST['printMonth']))
		{
			$printMonth=$_POST['printMonth'];
			$printYear=$_POST['printYear'];
			
			
			$this->pdfReportsMonthlySalary($model,$printYear,$printMonth,'printFiveReports/library','library',$report_allowances,$report_deductions);
			
		}



	if(isset($_POST['month']) && isset($_POST['year']))
		{
		
				$month=$_POST['month'];
				$year=$_POST['year'];
				$allMonthly=MonthlyPayroll::model()->findAllByAttributes(array('month'=>$month, 'year'=>$year));

				if($allMonthly != null)
				{
							$i=0;

							foreach ($allMonthly as $value) 
							{

									
									$department=StaffSpecialCase::model()->findByPk($value->department_id);

									if($department != null)
									{
											if($department->type=="library")
											{
												$monthlySheet[$i]=$value;

												$i++;
											}
									}


							}


				}






		}


		$report_id=Reports::model()->findByAttributes(array('type'=>'library'))->id;
		$report_allowances=ReportAllowance::model()->findAllByAttributes(array('report_id'=>$report_id,'active'=>1));
		$report_deductions=ReportDeduction::model()->findAllByAttributes(array('report_id'=>$report_id,'active'=>1));
		$this->render('fiveReports/library', array('staffs'=>$monthlySheet, 'model'=>$model,'month'=>$month,'year'=>$year,'special'=>$special,'report_allowances'=>$report_allowances,'report_deductions'=>$report_deductions));
		


	}






	public function actionAnnex()
	{
		$report_id=Reports::model()->findByAttributes(array('type'=>'annex'))->id;
		$report_allowances=ReportAllowance::model()->findAllByAttributes(array('report_id'=>$report_id,'active'=>1));
		$report_deductions=ReportDeduction::model()->findAllByAttributes(array('report_id'=>$report_id,'active'=>1));
		$this->layout = "//layouts/column1";
		$model = new Staff;
		$specialCase=new StaffSpecialCase;
		$monthlySheet=null;
		$month=null;
		$year=null;
		
		$special='Annex';
		$monthlySheet=array();


		if(isset($_POST['printYear']) && isset($_POST['printMonth']))
		{
			$printMonth=$_POST['printMonth'];
			$printYear=$_POST['printYear'];
			
			
			$this->pdfReportsMonthlySalary($model,$printYear,$printMonth,'printFiveReports/annex','annex',$report_allowances,$report_deductions);
			
		}




	if(isset($_POST['month']) && isset($_POST['year']))
		{
		
				$month=$_POST['month'];
				$year=$_POST['year'];
				$allMonthly=MonthlyPayroll::model()->findAllByAttributes(array('month'=>$month, 'year'=>$year));

				if($allMonthly != null)
				{
							$i=0;

							foreach ($allMonthly as $value) 
							{

									
									$department=StaffSpecialCase::model()->findByPk($value->department_id);

									if($department != null)
									{
											if($department->type=="annex")
											{
												$monthlySheet[$i]=$value;

												$i++;
											}
									}


							}

				}
		}

		$report_id=Reports::model()->findByAttributes(array('type'=>'annex'))->id;
		$report_allowances=ReportAllowance::model()->findAllByAttributes(array('report_id'=>$report_id,'active'=>1));
		$report_deductions=ReportDeduction::model()->findAllByAttributes(array('report_id'=>$report_id,'active'=>1));
		$this->render('fiveReports/annex', array('staffs'=>$monthlySheet, 'model'=>$model,'month'=>$month,'year'=>$year,'special'=>$special,'report_allowances'=>$report_allowances,'report_deductions'=>$report_deductions));
		


	}


	public function actionHigherSecondary()
	{
		$report_id=Reports::model()->findByAttributes(array('type'=>'plus2'))->id;
		$report_allowances=ReportAllowance::model()->findAllByAttributes(array('report_id'=>$report_id,'active'=>1));
		$report_deductions=ReportDeduction::model()->findAllByAttributes(array('report_id'=>$report_id,'active'=>1));
		$this->layout = "//layouts/column1";
		$model = new Staff;
		$specialCase=new StaffSpecialCase;
		$monthlySheet=null;
		$month=null;
		$year=null;
		
		$special='+2';
		$monthlySheet=array();


		if(isset($_POST['printYear']) && isset($_POST['printMonth']))
		{
			$printMonth=$_POST['printMonth'];
			$printYear=$_POST['printYear'];
			$this->pdfReportsMonthlySalary($model,$printYear,$printMonth,'printFiveReports/higherSecondary','plus2',$report_allowances,$report_deductions);
			
		}

		if(isset($_POST['month']) && isset($_POST['year']))
			{
					$month=$_POST['month'];
					$year=$_POST['year'];
					$allMonthly=MonthlyPayroll::model()->findAllByAttributes(array('month'=>$month, 'year'=>$year));

					if($allMonthly != null)
					{
								$i=0;
								foreach ($allMonthly as $value) 
								{	
										$department=StaffSpecialCase::model()->findByPk($value->department_id);

										if($department != null)
										{
												if($department->type=="plus2")
												{
													$monthlySheet[$i]=$value;

													$i++;
												}
										}


								}

					}
			}


			
		$this->render('fiveReports/higherSecondary', array('staffs'=>$monthlySheet, 'model'=>$model,'month'=>$month,'year'=>$year,'special'=>$special,'report_deductions'=>$report_deductions,'report_allowances'=>$report_allowances));
		




	}




	public function pdfBankReportsMonthlySalary($model,$year,$month,$callBy,$special=null)
    {
    	$viewFile=$callBy;
    	$monthlySheet=array();
		if(isset($special))
		{
		
				$month=$month;
				$year=$year;
				$special=$special;
				$allMonthly=MonthlyPayroll::model()->findAllByAttributes(array('month'=>$month, 'year'=>$year));
				if($allMonthly != null)
				{
							$i=0;

							foreach ($allMonthly as $value) 
							{

									
									$department=StaffSpecialCase::model()->findByPk($value->department_id);

									if($department != null)
									{
											if($department->type==$special)
											{
												$monthlySheet[$i]=$value;

												$i++;
											}
									}


							}


				}

		}
		else
		{
			$monthlySheet=MonthlyPayroll::model()->findAllByAttributes(array('month'=>$month, 'year'=>$year));
		}


        $mPDF1 = Yii::app()->ePdf->mpdf();
      

        
        $mPDF1->WriteHTML($this->renderPartial($viewFile, array('staffs'=>$monthlySheet, 'model'=>$model,'month'=>$month,'year'=>$year,'special'=>$special),true));

         $mPDF1->Output();
       
    }



	public function actionPfReport()
	{
		
		$this->layout = "//layouts/column1";
		$model = new Staff;
		
		$monthlySheet=null;
		$month=null;
		$year=null;
	
		$monthlySheet=array();
		if(isset($_POST['printYear']) && isset($_POST['printMonth']))
		{

				$month=$_POST['printMonth'];
				$year=$_POST['printYear'];
				$allMonthly=MonthlyPayroll::model()->findAllByAttributes(array('month'=>$month, 'year'=>$year));

				if($allMonthly != null)
				{
							$i=0;

							foreach ($allMonthly as $value) 
							{
									if($value->allowance_pf != 0 && $value->allowance_pf != null)
									{
											
												$monthlySheet[$i]=$value;

												$i++;
											
									}
							}
				}

		        $mPDF1 = Yii::app()->ePdf->mpdf();
		        $mPDF1->WriteHTML($this->renderPartial('extraReports/printPfReport', array('staffs'=>$monthlySheet, 'model'=>$model,'month'=>$month,'year'=>$year),true));
		         $mPDF1->Output();
			
		}


	if(isset($_POST['month']) && isset($_POST['year']))
		{
		
				$month=$_POST['month'];
				$year=$_POST['year'];
				$allMonthly=MonthlyPayroll::model()->findAllByAttributes(array('month'=>$month, 'year'=>$year));

				if($allMonthly != null)
				{
							$i=0;

							foreach ($allMonthly as $value) 
							{
									if($value->allowance_pf != 0 && $value->allowance_pf != null)
									{
											
												$monthlySheet[$i]=$value;

												$i++;
											
									}
							}
				}
		}

		
		$this->render('extraReports/pf', array('staffs'=>$monthlySheet, 'model'=>$model,'month'=>$month,'year'=>$year));
		


	}


	public function actionCifReport()
	{
		
		$this->layout = "//layouts/column1";
		$model = new Staff;
		
		$monthlySheet=null;
		$month=null;
		$year=null;
	
		$monthlySheet=array();
		if(isset($_POST['printYear']) && isset($_POST['printMonth']))
		{

				$month=$_POST['printMonth'];
				$year=$_POST['printYear'];
				$allMonthly=MonthlyPayroll::model()->findAllByAttributes(array('month'=>$month, 'year'=>$year));

				if($allMonthly != null)
				{
							$i=0;

							foreach ($allMonthly as $value) 
							{
									if($value->cif != 0 && $value->cif != null)
									{
											
												$monthlySheet[$i]=$value;

												$i++;
											
									}
							}
				}

		        $mPDF1 = Yii::app()->ePdf->mpdf();
		        $mPDF1->WriteHTML($this->renderPartial('extraReports/printCifReport', array('staffs'=>$monthlySheet, 'model'=>$model,'month'=>$month,'year'=>$year),true));
		         $mPDF1->Output();
			
		}


	if(isset($_POST['month']) && isset($_POST['year']))
		{
		
				$month=$_POST['month'];
				$year=$_POST['year'];
				$allMonthly=MonthlyPayroll::model()->findAllByAttributes(array('month'=>$month, 'year'=>$year));

				if($allMonthly != null)
				{
							$i=0;

							foreach ($allMonthly as $value) 
							{
									if($value->cif != 0 && $value->cif != null)
									{
											
												$monthlySheet[$i]=$value;

												$i++;
											
									}
							}
				}
		}

		
		$this->render('extraReports/CifReport', array('staffs'=>$monthlySheet, 'model'=>$model,'month'=>$month,'year'=>$year));
		


	}
	public function actionBankSheet()
	{

		
		$this->layout = "//layouts/column1";
		$model = new Staff;
		$specialCase=new StaffSpecialCase;
		$monthlySheet=null;
		$month=null;
		$year=null;
		
		$special=null;
		$monthlySheet=array();


		if(isset($_POST['printYear']) && isset($_POST['printMonth']))
		{
			$printMonth=$_POST['printMonth'];
			$printYear=$_POST['printYear'];
			
			
			$this->pdfBankReportsMonthlySalary($model,$printYear,$printMonth,'extraReports/printBankReport',$_POST['printSpecial']);
			
		}

		if(isset($_POST['month']) && isset($_POST['year']))
			{
					$month=$_POST['month'];
					$year=$_POST['year'];
					
					$special=$_POST['special'];
					
					$allMonthly=MonthlyPayroll::model()->findAllByAttributes(array('month'=>$month, 'year'=>$year));
					if($allMonthly != null)
					{
								$i=0;
								foreach ($allMonthly as $value) 
								{	
										$department=StaffSpecialCase::model()->findByPk($value->department_id);

										if($department != null)
										{
												if($department->type==$special)
												{
													$monthlySheet[$i]=$value;

													$i++;
												}
										}


								}

					}
			}


		$this->render('extraReports/BankReport', array('staffs'=>$monthlySheet, 'model'=>$model,'month'=>$month,'year'=>$year,'special'=>$special));
		




	}



	public function accountEntry()
	{
			$special=$_POST['special'];
			$amount=$_POST['amount'];
			$ledger=Ledgers::model()->findByAttributes(array('salary_type'=>$special));

			if($ledger != null)
			{

								
										$entries = new Entries;
										$entries->dr_total=$amount;
										$entries->cr_total=$amount;
										$entries->tag_id=3;
										//$entries->number=$transaction_id;
										$entries-> date = date('Y-m-d');
										//$entries-> cheq = 123;
										$entries->entrytype_id=4;
										//$entries->entrytype_id=$transaction_id;
										$entries->approval_manager=0;
										$entries->approval_admin=0;
										$entries->narration="Salary Billing Entry";
										//$entries->custom=0;

										if(!$entries->save())
										{
											exit;

										}
										else
										{

											$entryId=$entries->id;
										}
										$entryItem=new Entryitems;
										$entryItem->entry_id=$entryId;
										$entryItem->ledger_id=59;
										$entryItem->amount=$amount;
										$entryItem->dc="D";
										if(!$entryItem->save())
										{
											exit;

										}

														$entryItem=new Entryitems;
														//$ledger=Ledgers::model()->find('fee_title_id=:ID', array(':ID'=>$student_fee_info->fee_id));
														$entryItem->entry_id=$entryId;
														$entryItem->ledger_id=$ledger->id;
														$entryItem->amount=$amount;
														$entryItem->dc="C";
														if(!$entryItem->save())
														{
															exit;

														}
														else
														{
															Yii::app()->user->setFlash('success', 'Entries Saved');

														}
												

											
											
				}
				else
				{

						Yii::app()->user->setFlash('error', 'Entries Not Saved');
				}



	}








}
