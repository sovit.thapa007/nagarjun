<?php

/**
 * This is the model class for table "monthly_payroll".
 *
 * The followings are the available columns in table 'monthly_payroll':
 * @property integer $id
 * @property integer $staff_id
 * @property integer $level_id
 * @property integer $special_case_id
 * @property integer $basic_salary
 * @property integer $grade
 * @property integer $grade_amount
 * @property integer $allowance_pf
 * @property integer $assistant_teacher
 * @property integer $class_teacher
 * @property integer $coordinator
 * @property integer $private_teacher
 * @property integer $head_teacher
 * @property integer $eca_chief
 * @property integer $rahat
 * @property integer $disable_mahila
 * @property integer $disable_purush
 * @property integer $insurance_add
 * @property integer $gross_total
 * @property integer $deduction_pf
 * @property integer $deduction_insurance
 * @property integer $advance
 * @property integer $others
 * @property integer $cif
 * @property integer $net_salary
 * @property integer $yearly_total
 * @property double $monthly_tax
 * @property double $net_payment
 * @property integer $month
 * @property integer $year
 * @property string $created_date
 */
class MonthlyPayroll extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'monthly_payroll';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('staff_id, level_id, special_case_id, basic_salary, grade, grade_amount, month, year', 'required'),
			/*array('staff_id, level_id, special_case_id, basic_salary, grade, grade_amount, allowance_pf, assistant_teacher, class_teacher, coordinator, private_teacher, head_teacher, eca_chief, rahat, disable_mahila, disable_purush, insurance_add, gross_total, deduction_pf, deduction_insurance, advance, others, cit, net_salary, yearly_total, month, year', 'numerical', 'integerOnly'=>true),*/
			array('monthly_tax, net_payment', 'numerical'),
			array('created_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, staff_id, level_id, special_case_id, basic_salary, grade, grade_amount, allowance_pf, assistant_teacher, class_teacher, coordinator, private_teacher, head_teacher, eca_chief, rahat, disable_mahila, disable_purush, insurance_add, gross_total, deduction_pf, deduction_insurance, advance, others, cif, net_salary, yearly_total, monthly_tax, net_payment, month, year, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'staff' => array(self::BELONGS_TO, 'Staff', 'staff_id'),
			'specialcase'=>array(self::BELONGS_TO, 'StaffSpecialCase', 'special_case_id'),

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'staff_id' => 'Staff',
			'level_id' => 'Level',
			'special_case_id' => 'Special Case',
			'basic_salary' => 'Basic Salary',
			'grade' => 'Grade',
			'grade_amount' => 'Grade Amount',
			'allowance_pf' => 'Allowance Pf',
			'assistant_teacher' => 'Assistant Teacher',
			'class_teacher' => 'Class Teacher',
			'coordinator' => 'Coordinator',
			'private_teacher' => 'Private Teacher',
			'head_teacher' => 'Head Teacher',
			'eca_chief' => 'Eca Chief',
			'rahat' => 'Rahat',
			'disable_mahila' => 'Disable Mahila',
			'disable_purush' => 'Disable Purush',
			'insurance_add' => 'Insurance Add',
			'gross_total' => 'Gross Total',
			'deduction_pf' => 'Deduction Pf',
			'deduction_insurance' => 'Deduction Insurance',
			'advance' => 'Advance',
			'others' => 'Others',
			'cif' => 'Cif',
			'net_salary' => 'Net Salary',
			'yearly_total' => 'Yearly Total',
			'monthly_tax' => 'Monthly Tax',
			'net_payment' => 'Net Payment',
			'month' => 'Month',
			'year' => 'Year',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('staff_id',$this->staff_id);
		$criteria->compare('level_id',$this->level_id);
		$criteria->compare('special_case_id',$this->special_case_id);
		$criteria->compare('basic_salary',$this->basic_salary);
		$criteria->compare('grade',$this->grade);
		$criteria->compare('grade_amount',$this->grade_amount);
		$criteria->compare('allowance_pf',$this->allowance_pf);
		$criteria->compare('assistant_teacher',$this->assistant_teacher);
		$criteria->compare('class_teacher',$this->class_teacher);
		$criteria->compare('coordinator',$this->coordinator);
		$criteria->compare('private_teacher',$this->private_teacher);
		$criteria->compare('head_teacher',$this->head_teacher);
		$criteria->compare('eca_chief',$this->eca_chief);
		$criteria->compare('rahat',$this->rahat);
		$criteria->compare('disable_mahila',$this->disable_mahila);
		$criteria->compare('disable_purush',$this->disable_purush);
		$criteria->compare('insurance_add',$this->insurance_add);
		$criteria->compare('gross_total',$this->gross_total);
		$criteria->compare('deduction_pf',$this->deduction_pf);
		$criteria->compare('deduction_insurance',$this->deduction_insurance);
		$criteria->compare('advance',$this->advance);
		$criteria->compare('others',$this->others);
		$criteria->compare('cif',$this->cif);
		$criteria->compare('net_salary',$this->net_salary);
		$criteria->compare('yearly_total',$this->yearly_total);
		$criteria->compare('monthly_tax',$this->monthly_tax);
		$criteria->compare('net_payment',$this->net_payment);
		$criteria->compare('month',$this->month);
		$criteria->compare('year',$this->year);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MonthlyPayroll the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function checkExistingData($month,$year,$staffId,$department_id)
	{
			$model=MonthlyPayroll::model()->findByAttributes(array('staff_id'=>$staffId,'month'=>$month,'year'=>$year,'department_id'=>$department_id));
			if($model == null)
			{
				return true;
			}
			else
			{
				return false;
			}
	}


	public function addColum($name)
	{


		
	}

	 public function departmentList()
    {
    	$model=DepartmentPayroll::model()->findAllByAttributes(array('payroll_id'=>$this->id));
    	return $model;
    }
    

}
