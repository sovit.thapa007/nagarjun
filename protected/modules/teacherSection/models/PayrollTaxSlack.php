<?php

/**
 * This is the model class for table "payroll_tax_slack".
 *
 * The followings are the available columns in table 'payroll_tax_slack':
 * @property integer $id
 * @property integer $first_tax_percent
 * @property integer $first_tax_amount
 * @property integer $second_tax_percent
 * @property integer $second_tax_amount
 * @property integer $third_tax_percent
 * @property integer $third_tax_amount
 * @property string $created_date
 */
class PayrollTaxSlack extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payroll_tax_slack';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('first_tax_percent, first_tax_amount, second_tax_percent, second_tax_amount, third_tax_percent, third_tax_amount', 'required'),
			array('first_tax_percent, first_tax_amount, second_tax_percent, second_tax_amount, third_tax_percent, third_tax_amount', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, first_tax_percent, first_tax_amount, second_tax_percent, second_tax_amount, third_tax_percent, third_tax_amount, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'first_tax_percent' => 'First Tax Percent',
			'first_tax_amount' => 'First Tax Amount',
			'second_tax_percent' => 'Second Tax Percent',
			'second_tax_amount' => 'Second Tax Amount',
			'third_tax_percent' => 'Third Tax Percent',
			'third_tax_amount' => 'Third Tax Amount',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('first_tax_percent',$this->first_tax_percent);
		$criteria->compare('first_tax_amount',$this->first_tax_amount);
		$criteria->compare('second_tax_percent',$this->second_tax_percent);
		$criteria->compare('second_tax_amount',$this->second_tax_amount);
		$criteria->compare('third_tax_percent',$this->third_tax_percent);
		$criteria->compare('third_tax_amount',$this->third_tax_amount);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PayrollTaxSlack the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
