<?php

/**
 * This is the model class for table "training_info".
 *
 * The followings are the available columns in table 'training_info':
 * @property integer $id
 * @property integer $personalDetailsid
 * @property string $year
 * @property string $type
 * @property string $subject
 * @property string $duration
 * @property string $organiser
 */
class TrainingInfo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'training_info';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('personalDetailsid', 'required'),
			array('personalDetailsid', 'numerical', 'integerOnly'=>true),
			array('year', 'length', 'max'=>4),
			array('type, duration', 'length', 'max'=>50),
			array('subject', 'length', 'max'=>100),
			array('organiser', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, personalDetailsid, year, type, subject, duration, organiser', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'personalDetailsid' => 'Teacher Name',
			'year' => 'Year',
			'type' => 'Type',
			'subject' => 'Subject',
			'duration' => 'Duration',
			'organiser' => 'Organiser',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('personalDetailsid',$this->personalDetailsid);
		$criteria->compare('year',$this->year,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('duration',$this->duration,true);
		$criteria->compare('organiser',$this->organiser,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TrainingInfo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
