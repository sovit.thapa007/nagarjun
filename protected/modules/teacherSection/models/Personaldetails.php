<?php

/**
 * This is the model class for table "personaldetails".
 *
 * The followings are the available columns in table 'personaldetails':
 * @property integer $id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $type
 * @property string $gender
 * @property string $caste
 * @property string $nationality
 * @property string $date_of_birth
 * @property string $citizenship_no
 * @property string $issue_district
 * @property string $fathers_name
 * @property string $mothers_name
 * @property string $spouse_name
 * @property string $will_person
 * @property string $mother_tongue
 * @property string $disability
 * @property string $email
 * @property string $contact_number
 * @property string $current_level
 * @property string $position
 * @property string $rank
 * @property string $teaching_language
 * @property string $license_no
 * @property string $insurance_no
 * @property string $pf_ac_no
 * @property string $trk_no
 * @property string $bank_name
 * @property string $bank_ac_no
 * @property integer $status
 * @property string $cif_no
 */
class Personaldetails extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'personaldetails';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('first_name, middle_name, last_name, date_of_birth, contact_number', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('first_name, middle_name, last_name, fathers_name, mothers_name, spouse_name, will_person, email, bank_name', 'length', 'max'=>250),
			array('type', 'length', 'max'=>21),
			array('gender, date_of_birth, contact_number', 'length', 'max'=>15),
			array('caste, rank, insurance_no, pf_ac_no, trk_no, bank_ac_no', 'length', 'max'=>50),
			array('nationality, mother_tongue, current_level, position, teaching_language', 'length', 'max'=>100),
			array('citizenship_no', 'length', 'max'=>35),
			array('issue_district', 'length', 'max'=>150),
			array('disability', 'length', 'max'=>10),
			array('license_no', 'length', 'max'=>55),
			array('cif_no', 'length', 'max'=>40),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, first_name, middle_name, last_name, type, gender, caste, nationality, date_of_birth, citizenship_no, issue_district, fathers_name, mothers_name, spouse_name, will_person, mother_tongue, disability, email, contact_number, current_level, position, rank, teaching_language, license_no, insurance_no, pf_ac_no, trk_no, bank_name, bank_ac_no, status, cif_no', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'first_name' => 'First Name',
			'middle_name' => 'Middle Name',
			'last_name' => 'Last Name',
			'type' => 'Type',
			'gender' => 'Gender',
			'caste' => 'Caste',
			'nationality' => 'Nationality',
			'date_of_birth' => 'Date Of Birth',
			'citizenship_no' => 'Citizenship No',
			'issue_district' => 'Issue District',
			'fathers_name' => 'Fathers Name',
			'mothers_name' => 'Mothers Name',
			'spouse_name' => 'Spouse Name',
			'will_person' => 'Will Person',
			'mother_tongue' => 'Mother Tongue',
			'disability' => 'Disability',
			'email' => 'Email',
			'contact_number' => 'Contact Number',
			'current_level' => 'Current Level',
			'position' => 'Position',
			'rank' => 'Rank',
			'teaching_language' => 'Teaching Language',
			'license_no' => 'License No',
			'insurance_no' => 'Insurance No',
			'pf_ac_no' => 'Pf Ac No',
			'trk_no' => 'Trk No',
			'bank_name' => 'Bank Name',
			'bank_ac_no' => 'Bank Ac No',
			'status' => 'Status',
			'cif_no' => 'Cif No',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('caste',$this->caste,true);
		$criteria->compare('nationality',$this->nationality,true);
		$criteria->compare('date_of_birth',$this->date_of_birth,true);
		$criteria->compare('citizenship_no',$this->citizenship_no,true);
		$criteria->compare('issue_district',$this->issue_district,true);
		$criteria->compare('fathers_name',$this->fathers_name,true);
		$criteria->compare('mothers_name',$this->mothers_name,true);
		$criteria->compare('spouse_name',$this->spouse_name,true);
		$criteria->compare('will_person',$this->will_person,true);
		$criteria->compare('mother_tongue',$this->mother_tongue,true);
		$criteria->compare('disability',$this->disability,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('contact_number',$this->contact_number,true);
		$criteria->compare('current_level',$this->current_level,true);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('rank',$this->rank,true);
		$criteria->compare('teaching_language',$this->teaching_language,true);
		$criteria->compare('license_no',$this->license_no,true);
		$criteria->compare('insurance_no',$this->insurance_no,true);
		$criteria->compare('pf_ac_no',$this->pf_ac_no,true);
		$criteria->compare('trk_no',$this->trk_no,true);
		$criteria->compare('bank_name',$this->bank_name,true);
		$criteria->compare('bank_ac_no',$this->bank_ac_no,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('cif_no',$this->cif_no,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Personaldetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getStaff()
	{
		return Staff::model()->findByAttributes(array('detail_id'=>$this->id));
	}
}
