<?php

/**
 * This is the model class for table "staff_deduction".
 *
 * The followings are the available columns in table 'staff_deduction':
 * @property integer $id
 * @property string $created_date
 * @property string $updated_date
 * @property integer $updated_by
 * @property integer $staff_id
 * @property integer $deduction_id
 * @property integer $status
 * @property string $month
 */
class StaffDeduction extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $deductionAmounts=array();
	public function tableName()
	{
		return 'staff_deduction';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('staff_id, deduction_id', 'required'),
			array('updated_by, staff_id, deduction_id, amount,status','numerical', 'integerOnly'=>true),
			array('month', 'length', 'max'=>22),
			array('created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, created_date, updated_date, updated_by, staff_id, deduction_id, status, month', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'created_date' => 'Created Date',
			'updated_date' => 'Updated Date',
			'updated_by' => 'Updated By',
			'staff_id' => 'Staff',
			'deduction_id' => 'Deduction',
			'status' => 'Status',
			'month' => 'Month',
			'amount'=>'Deduction Amount',
		);

	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('staff_id',$this->staff_id);
		$criteria->compare('deduction_id',$this->deduction_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('month',$this->month,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StaffDeduction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
