<?php

/**
 * This is the model class for table "part_time_teacher".
 *
 * The followings are the available columns in table 'part_time_teacher':
 * @property integer $id
 * @property integer $staff_id
 * @property string $created_date
 * @property integer $created_by
 * @property integer $theory_class_attend
 * @property integer $practical_class_attend
 * @property integer $theory_class_amount
 * @property integer $practical_class_amount
 * @property integer $month
 */
class PartTimeTeacher extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'part_time_teacher';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('staff_id, theory_class_attend, theory_class_amount', 'required'),
			array('staff_id, created_by, theory_class_attend, practical_class_attend, theory_class_amount, practical_class_amount, month', 'numerical', 'integerOnly'=>true),
			array('created_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, staff_id, created_date, created_by, theory_class_attend, practical_class_attend, theory_class_amount, practical_class_amount, month', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'staff_id' => 'Staff',
			'created_date' => 'Created Date',
			'created_by' => 'Created By',
			'theory_class_attend' => 'Theory Class Attendence',
			'practical_class_attend' => 'Practical Class Attendence',
			'theory_class_amount' => 'Theory Class Amount',
			'practical_class_amount' => 'Practical Class Amount',
			'month' => 'Month',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('staff_id',$this->staff_id);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('theory_class_attend',$this->theory_class_attend);
		$criteria->compare('practical_class_attend',$this->practical_class_attend);
		$criteria->compare('theory_class_amount',$this->theory_class_amount);
		$criteria->compare('practical_class_amount',$this->practical_class_amount);
		$criteria->compare('month',$this->month);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PartTimeTeacher the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
