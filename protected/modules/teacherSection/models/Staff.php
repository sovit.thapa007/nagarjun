<?php

/**
 * This is the model class for table "staff".
 *
 * The followings are the available columns in table 'staff':
 * @property integer $staff_id
 * @property string $fname
 * @property string $lname
 * @property string $address
 * @property string $contact
 * @property string $username
 * @property string $password
 * @property integer $department_id
 * @property integer $designation_id
 * @property integer $join_date
 * @property integer $office_start_time
 * @property integer $office_end_time
 * @property string $email
 * @property string $profile_pic
 * @property integer $verified_email
 * @property string $token
 * @property integer $created_date
 */
class Staff extends CActiveRecord
{
	public $designation, $grade, $repassword, $start_time, $end_time, $selfPf;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'staff';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fname, lname, contact, teacher_type,password, join_date, created_date, marital_status', 'required'),
			array('department_id,level_id,shreni_id, verified_email, created_date', 'numerical', 'integerOnly'=>true),
			array('fname, lname, address, contact, username, password, email', 'length', 'max'=>100),
			array('profile_pic', 'length', 'max'=>200),
			//array('email', 'unique', 'message' => 'Email address already in use', 'on' => 'insert'),
			array('email', 'email'),
			array('fname', 'match', 'pattern' => '/^[a-zA-Z\s]+$/', 'message' => 'First Name can only contain alphabet characters'),
			array('lname', 'match', 'pattern' => '/^[a-zA-Z\s]+$/', 'message' => 'Last Name can only contain alphabet characters'),
			array('token', 'length', 'max'=>300),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('staff_id, fname, lname, address, marital_status, contact, username, password, department_id, designation_id, join_date, email, profile_pic, verified_email, token, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'depart' => array(self::BELONGS_TO, 'Department', 'department_id'),
			'officeTime' => array(self::HAS_MANY, 'StaffOfficeTime', 'staff_id'),
			'designation' =>array(self::BELONGS_TO, 'Designation', 'designation_id'),
			'level' =>array(self::BELONGS_TO, 'Level', 'level_id'),
			'shreni' =>array(self::BELONGS_TO, 'Shreni', 'shreni_id'),
			'details'=>array(self::BELONGS_TO, 'Personaldetails', 'detail_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'staff_id' => 'Staff',
			'fname' => 'Fname',
			'lname' => 'Lname',
			'address' => 'Address',
			'contact' => 'Contact',
			'marital_status'=>'Marital Status',
			'username' => 'Username',
			'password' => 'Password',
			'department_id' => 'Department',
			'designation_id' => 'Designation',
			'join_date' => 'Join Date',
			'email' => 'Email',
			'profile_pic' => 'Profile Pic',
			'verified_email' => 'Verified Email',
			'token' => 'Token',
			'created_date' => 'Created Date',
			'role'=>'Role',
			'level_id'=>'Level',
			'shreni_id'=>'Shreni'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('staff_id',$this->staff_id);
		$criteria->compare('fname',$this->fname,true);
		$criteria->compare('lname',$this->lname,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('contact',$this->contact,true);
		$criteria->compare('marital_status',$this->marital_status,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('department_id',$this->department_id);
		$criteria->compare('designation_id',$this->designation_id);
		$criteria->compare('join_date',$this->join_date);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('profile_pic',$this->profile_pic,true);
		$criteria->compare('verified_email',$this->verified_email);
		$criteria->compare('token',$this->token,true);
		$criteria->compare('created_date',$this->created_date);
		$criteria->compare('active','1', true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Staff the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function deactivateStaff($id){
		$model = Staff::model()->findByPk($id);
		$model->active = 0;
		
		if($model->save()){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * method to get the self pf amount of a staff in a given month
	 * @param  [int] $id   staff id
	 * @param  [string] $date [date ]
	 * @return [type]       [description]
	 */
	public function getSelfPf($id, $date){
		$selfPf = StaffSelfPf::model()->findByAttributes(array('staff_id'=>$id), array('order'=>'id DESC'), 'month(effective_date) <= "'.date('m', $date) . '" AND year(effective_date) <= "' . date('y', $date) . '"');
		if(!empty($selfPf)){
			return number_format((float)$selfPf->amount, 2, '.', '');
		}else{
			return "0";
		}
	}

	public function getPf($date){
		$pf = PayrollPf::model()->findByAttributes(array(), array('order'=>'id DESC'), 'month(effective_date) <= "'.date('m', $date) . '" AND year(effective_date) <= "' . date('y', $date) . '"');
		if(!empty($pf)){

			return $pf->pf_percent;
		}else{
			return "0";
		}
	}


	public function getCf($date){
		$cf = CitizenFund::model()->findByAttributes(array(), array('order'=>'id DESC'), 'month(effective_date) <= "'.date('m', $date) . '" AND year(effective_date) <= "' . date('y', $date) . '"');
		if(!empty($cf)){
			
			return $cf->percentage;
		}else{
			return "0";
		}
	}


	public function getPfc($date)
	{
		$pfc = PfContribution::model()->findByAttributes(array(), array('order'=>'effective_date DESC'), 'month(effective_date) <= "'.date('m', $date) . '" AND year(effective_date) <= "' . date('y', $date) . '"');
		if(!empty($pfc)){
			return $pfc->rate;
		}else{
			return "0";
		}
	}

	public function getBankAc($id)
	{
		$staff=Staff::model()->findByPk($id);
		$personalDetail=Personaldetails::model()->findByPk($staff->detail_id);

		if($personalDetail != null)
		return $personalDetail->bank_ac_no;

	return '';

	}


	public function specialCaseStaff()
	{
		$model=SpecialCaseStaff::model()->findByAttributes(array('staff_id'=>$this->staff_id));
		return $model != null ? $model->special_case_id : 0;
		
	}


	public function isPermanentGovernment($id)
	{
		$staff_special=StaffSpecialCase::model()->findByPk($id);
		if($staff_special != null)
				{
					$name=$staff_special->name;
					
					if($name == 'Permanent Government')
					{	
						return true;
					}
				}

		return false;

	}

	public static function isPartTime($id)
	{
		$specialcaseStaff=SpecialCaseStaff::model()->findAllByAttributes(array('staff_id'=>$id));
		foreach ($specialcaseStaff as $staff) 
		{
			
				$special_id=$staff->special_case_id;
				$staff_special=StaffSpecialCase::model()->findByPk($special_id);
				$type=$staff_special->type;
				
				if($type == 'parttime')
				{	
					return true;
				}
			
		}
		return false;


	}

	public function getBankName($id)
	{

		$staff=Staff::model()->findByPk($id);

		$personalDetail=Personaldetails::model()->findByPk($staff->detail_id);
		if($personalDetail != null)
		return $personalDetail->bank_name;


		return '';
	}

	public function getLevelName()
	{
	
		if($this->level_id == 0 || $this->level_id == null)
		{

			return '-';

		}
		else
		$level=Level::model()->findByPk($this->level_id);

		return $level->name;

	}

	public function getAllowanceNumber($id, $allowance,$special_case_id)
	{
            
        	$allowances = StaffAllowance::model()->findByAttributes(array('staff_id'=>$id, 'allowance_id'=>$allowance,'special_case_id'=>$special_case_id),array('order'=>'id DESC'));
		if($allowances == null) return 0;
		return $allowances->percentage;


	}

	public function getDeductionNumber($id, $deduction,$special_case_id)
	{

		$deductions = StaffDeduction::model()->findByAttributes(array('staff_id'=>$id, 'deduction_id'=>$deduction,'special_case_id'=>$special_case_id),array('order'=>'id DESC'));
		if($deductions== null) 
			{return 0;}

		return $deductions->amount;


	}



	public function getAllowances($id, $allowance, $date){
		$allowances = StaffAllowance::model()->findAllByAttributes(array('staff_id'=>$id), array('group'=>'allowance_id'), 'effective_date <= "'.date('Y-m-d H:i:m', $date) . '" AND percentage != 0');
		$year = date('Y', $date);
		$month = date('m', $date);
		//foreach ($allowances as $key => $allowance) {
			$sql = "SELECT allowance_id, percentage FROM staff_allowance WHERE allowance_id = '".$allowance."' AND staff_id = '".$id."' AND year(effective_date) <= '".$year."' AND month(effective_date) <= '".$month."' ORDER BY id DESC";
			//$sql = "SELECT allowance_id, percentage FROM staff_allowance WHERE allowance_id = '1' AND staff_id = '12' AND year(effective_date) <= '2014' AND month(effective_date) <= '11' ORDER BY effective_date DESC";
			$result = Yii::app()->db->createCommand($sql)->queryRow();
		//}
		if(!empty($result['percentage'])){
			if((time() - Staff::model()->findByPk($id)->join_date)/86400 <= 30){
				return 0;
			}else{
				return $result['percentage'];
			}
		}else{
			return "0";
		}
		
	}	

	public function getAbsentDays($id, $date){

		$attendance = Attendance::model()->findAllByAttributes(array('staff_id'=>$id), 'FROM_UNIXTIME(login, "%m") = "'.date('m', $date).'" AND FROM_UNIXTIME(login, "%Y") = "'.date('Y', $date).'"');
		$holiday = 0;
		$year = date('Y', $date);
		$month = date('m', $date);
		for ($i=1; $i <= date('t') ; $i++) {			
			$new = $year . "-" . $month . "-" . $i;
			if(date('D', strtotime($new)) == 'Sat'){
				$holiday +=1;
			}
		}

		$late = count(Attendance::model()->findAllByAttributes(array('staff_id'=>$id, 'login_status'=>'late'), 'FROM_UNIXTIME(login, "%m") = "'.date('m', $date).'" AND FROM_UNIXTIME(login, "%Y") = "'.date('Y', $date).'"'));
		$late = floor($late/3);

		$absent = date('t', $date) - count($attendance) - $holiday - $late;				
		
		$salary = Staff::model()->findByPk($id)->basic_salary;
		$salary = $absent * $salary/(date('t', $date) - $holiday);
		return round($salary, 2);
	}

	public function getTdsRate($id, $amount){
		$staff = Staff::model()->with('designation')->findByPk($id);
		$status = $staff->marital_status;
		$yearly = $amount*12;
		$tdsAmount = 0;		
		$tds = TdsRate::model()->findAllByAttributes(array('marital_status'=>$status), array('order'=>'upto_amount ASC'));
		foreach($tds as $a){
			if($yearly >=$a->upto_amount && $yearly > 0){
				$yearly = $yearly - $a->upto_amount;
				$tdsAmount += $a->upto_amount * $a->tds_rate/100;
			}elseif($yearly <= $a->upto_amount && $yearly > 0){
				$tdsAmount +=  $yearly * $a->tds_rate/100;
				$yearly = $yearly - $a->upto_amount;
			}

		}
		//$tds = TdsRate::model()->findByAttributes(array('marital_status'=>$status), 'upto_amount >=  "' . $amount*12 . '"');
		if(!empty($tdsAmount)){
			return round($tdsAmount/12, 2);
		}else{
			return '0';
		}
	}

	public function getFullName() {
    		return $this->fname . ' ' . $this->lname;
	}	

	public static function getPartTimeStaffInfo($id,$month)
    {
    	if($month == 0)
    	{
    	$info=PartTimeTeacher::model()->findByAttributes(array('staff_id'=>$id));
    	}
    	else
    	{
    	$info =PartTimeTeacher::model()->findByAttributes(array('staff_id'=>$id,'month'=>$month));
    	}
    	return $info;

    }

    public function departmentList()
    {
    	$model=DepartmentStaff::model()->findAllByAttributes(array('staff_id'=>$this->staff_id));
    	return $model;
    }
    


    public static function saveMonthlySalary()
    {
    	$allowances = Allowances::model()->findAll();
    	$staffs 	= Staff::model()->findAll();
    	

    	foreach ($staffs as $staff) 
    	{
    		$monthlyPayroll = new MonthlyPayroll();
    	 	$i=1;
    			foreach($allowances as $allowanceDb)
		    	{ 
		    		$string=$allowanceDb->allowanceName;
		    		$allowance="allowance".$i;
		    		$$allowance=strtolower(str_replace(' ', '_',$string));
		    		$allowanceAmount="allowanceAmount".$i;
		    		$$allowanceAmount=$staff->getAllowanceNumber($staff->staff_id, $allowanceDb->allowanceId);


		    		$monthlyPayroll->$$allowance=$$allowanceAmount;


		    		$i++;

		    	}


		    	$monthlyPayroll->staff_id=$staff->staff_id;
		    	$monthlyPayroll->level_id=$staff->level_id;
		    	$monthlyPayroll->special_case_id=1;
		    	$monthlyPayroll->basic_salary=$staff->basic_salary;
		    	$monthlyPayroll->grade=$staff->grade;
		    	$monthlyPayroll->grade_amount=$staff->grade_amount;
		    	$monthlyPayroll->month=1;
		    	$monthlyPayroll->year=2073;
		    	if($monthlyPayroll->save())
		    	{

		    	}
		    	else
		    	{
		    		
		    	}



    	} 

    	


    }

    public function getBasicSalary($special_case_id)
    {
    	$amount=SpecialCaseStaff::model()->findByAttributes(array('staff_id'=>$this->staff_id,'special_case_id'=>$special_case_id))->basic_salary;
    	return $amount == null ? 0:$amount;

    }
    public function getGrade($special_case_id)
    {
    	$grade=SpecialCaseStaff::model()->findByAttributes(array('staff_id'=>$this->staff_id,'special_case_id'=>$special_case_id))->grade;
    	return $grade == null ? 0: $grade;
    }
    public function getGradeAmount($special_case_id)
    {
    	$amount=SpecialCaseStaff::model()->findByAttributes(array('staff_id'=>$this->staff_id,'special_case_id'=>$special_case_id))->grade_amount;
    	return $amount == null ? 0:$amount;
    }
    public function getPartTimeAmount($special_case_id)
    {
    	$amount=SpecialCaseStaff::model()->findByAttributes(array('staff_id'=>$this->staff_id,'special_case_id'=>$special_case_id))->part_time_class_amount;
    	return $amount == null ? 0:$amount;
    }
     public function getPartTimeClass($special_case_id)
    {
    	$amount=SpecialCaseStaff::model()->findByAttributes(array('staff_id'=>$this->staff_id,'special_case_id'=>$special_case_id))->part_time_class;
    	return $amount == null ? 0:$amount;
    }

    public function getPlus2()
    {
    	$model=StaffSpecialCase::model()->findByAttributes(array('type'=>'plus2'))->id;
    	if($model == null)
    	{
    		print_r("No Plus 2"); exit;
    	}
    	return StaffSpecialCase::model()->findByAttributes(array('type'=>'plus2'))->id;
    }

	
}
