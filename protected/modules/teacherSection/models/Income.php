<?php

/**
 * This is the model class for table "income".
 *
 * The followings are the available columns in table 'income':
 * @property integer $id
 * @property integer $personalDetailsid
 * @property string $monthly_salary
 * @property integer $grade_num
 * @property string $grade_amount
 * @property string $head_teacher
 * @property string $dress
 * @property string $festival
 * @property string $insurance
 * @property string $remote
 * @property string $medicine
 * @property string $mahangi
 * @property string $provident_fund
 * @property string $citizen_investment
 */
class Income extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'income';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('personalDetailsid, monthly_salary, dress, festival', 'required'),
			array('personalDetailsid, grade_num', 'numerical', 'integerOnly'=>true),
			array('monthly_salary, grade_amount, head_teacher, dress, festival, insurance, remote, medicine, mahangi, provident_fund, citizen_investment', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, personalDetailsid, monthly_salary, grade_num, grade_amount, head_teacher, dress, festival, insurance, remote, medicine, mahangi, provident_fund, citizen_investment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'personalDetailsid' => 'Teacher Name',
			'monthly_salary' => 'Monthly Salary',
			'grade_num' => 'Grade Num',
			'grade_amount' => 'Grade Amount',
			'head_teacher' => 'Head Teacher',
			'dress' => 'Dress',
			'festival' => 'Festival',
			'insurance' => 'Insurance',
			'remote' => 'Remote',
			'medicine' => 'Medicine',
			'mahangi' => 'Mahangi',
			'provident_fund' => 'Provident Fund',
			'citizen_investment' => 'Citizen Investment',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('personalDetailsid',$this->personalDetailsid);
		$criteria->compare('monthly_salary',$this->monthly_salary,true);
		$criteria->compare('grade_num',$this->grade_num);
		$criteria->compare('grade_amount',$this->grade_amount,true);
		$criteria->compare('head_teacher',$this->head_teacher,true);
		$criteria->compare('dress',$this->dress,true);
		$criteria->compare('festival',$this->festival,true);
		$criteria->compare('insurance',$this->insurance,true);
		$criteria->compare('remote',$this->remote,true);
		$criteria->compare('medicine',$this->medicine,true);
		$criteria->compare('mahangi',$this->mahangi,true);
		$criteria->compare('provident_fund',$this->provident_fund,true);
		$criteria->compare('citizen_investment',$this->citizen_investment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Income the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
