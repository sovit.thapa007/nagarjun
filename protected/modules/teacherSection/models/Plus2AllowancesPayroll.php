<?php

/**
 * This is the model class for table "plus2_allowances_payroll".
 *
 * The followings are the available columns in table 'plus2_allowances_payroll':
 * @property integer $id
 * @property integer $payroll_id
 * @property integer $allowance_id
 * @property integer $days
 * @property integer $amount
 * @property string $created_date
 */
class Plus2AllowancesPayroll extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'plus2_allowances_payroll';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('payroll_id, allowance_id', 'required'),
			array('payroll_id, allowance_id, days, amount', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, payroll_id, allowance_id, days, amount, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'payroll_id' => 'Payroll',
			'allowance_id' => 'Allowance',
			'days' => 'Days',
			'amount' => 'Amount',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('payroll_id',$this->payroll_id);
		$criteria->compare('allowance_id',$this->allowance_id);
		$criteria->compare('days',$this->days);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Plus2AllowancesPayroll the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
