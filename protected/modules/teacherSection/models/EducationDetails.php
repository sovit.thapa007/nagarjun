<?php

/**
 * This is the model class for table "education_details".
 *
 * The followings are the available columns in table 'education_details':
 * @property integer $id
 * @property integer $personalDetailsid
 * @property string $qualification
 * @property string $board_university
 * @property string $year
 * @property string $stream
 * @property string $education_course
 * @property string $division
 */
class EducationDetails extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'education_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('personalDetailsid, qualification, board_university, year, stream, division', 'required'),
			array('personalDetailsid', 'numerical', 'integerOnly'=>true),
			array('qualification, stream', 'length', 'max'=>25),
			array('board_university', 'length', 'max'=>100),
			array('year, education_course, division', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, personalDetailsid, qualification, board_university, year, stream, education_course, division', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'personalDetailsid' => 'Teacher Name',
			'qualification' => 'Qualification',
			'board_university' => 'Board University',
			'year' => 'Year',
			'stream' => 'Stream',
			'education_course' => 'Education Course',
			'division' => 'Division',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('personalDetailsid',$this->personalDetailsid);
		$criteria->compare('qualification',$this->qualification,true);
		$criteria->compare('board_university',$this->board_university,true);
		$criteria->compare('year',$this->year,true);
		$criteria->compare('stream',$this->stream,true);
		$criteria->compare('education_course',$this->education_course,true);
		$criteria->compare('division',$this->division,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EducationDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
