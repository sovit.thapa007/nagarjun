<?php
/* @var $this MonthlyPayrollController */
/* @var $model MonthlyPayroll */

$this->breadcrumbs=array(
	'Monthly Payrolls'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MonthlyPayroll', 'url'=>array('index')),
	array('label'=>'Create MonthlyPayroll', 'url'=>array('create')),
	array('label'=>'View MonthlyPayroll', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MonthlyPayroll', 'url'=>array('admin')),
);
?>

<h1>Update MonthlyPayroll <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>