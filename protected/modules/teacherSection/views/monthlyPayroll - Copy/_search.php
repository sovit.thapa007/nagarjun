<?php
/* @var $this MonthlyPayrollController */
/* @var $model MonthlyPayroll */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'staff_id'); ?>
		<?php echo $form->textField($model,'staff_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'level_id'); ?>
		<?php echo $form->textField($model,'level_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'special_case_id'); ?>
		<?php echo $form->textField($model,'special_case_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'basic_salary'); ?>
		<?php echo $form->textField($model,'basic_salary'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'grade'); ?>
		<?php echo $form->textField($model,'grade'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'grade_amount'); ?>
		<?php echo $form->textField($model,'grade_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'allowance_pf'); ?>
		<?php echo $form->textField($model,'allowance_pf'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'assistant_teacher'); ?>
		<?php echo $form->textField($model,'assistant_teacher'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'class_teacher'); ?>
		<?php echo $form->textField($model,'class_teacher'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'coordinator'); ?>
		<?php echo $form->textField($model,'coordinator'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'private_teacher'); ?>
		<?php echo $form->textField($model,'private_teacher'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'head_teacher'); ?>
		<?php echo $form->textField($model,'head_teacher'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'eca_chief'); ?>
		<?php echo $form->textField($model,'eca_chief'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rahat'); ?>
		<?php echo $form->textField($model,'rahat'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'disable_mahila'); ?>
		<?php echo $form->textField($model,'disable_mahila'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'disable_purush'); ?>
		<?php echo $form->textField($model,'disable_purush'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'insurance_add'); ?>
		<?php echo $form->textField($model,'insurance_add'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gross_total'); ?>
		<?php echo $form->textField($model,'gross_total'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'deduction_pf'); ?>
		<?php echo $form->textField($model,'deduction_pf'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'deduction_insurance'); ?>
		<?php echo $form->textField($model,'deduction_insurance'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'advance'); ?>
		<?php echo $form->textField($model,'advance'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'others'); ?>
		<?php echo $form->textField($model,'others'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cit'); ?>
		<?php echo $form->textField($model,'cit'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'net_salary'); ?>
		<?php echo $form->textField($model,'net_salary'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'yearly_total'); ?>
		<?php echo $form->textField($model,'yearly_total'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'monthly_tax'); ?>
		<?php echo $form->textField($model,'monthly_tax'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'net_payment'); ?>
		<?php echo $form->textField($model,'net_payment'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'month'); ?>
		<?php echo $form->textField($model,'month'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'year'); ?>
		<?php echo $form->textField($model,'year'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->