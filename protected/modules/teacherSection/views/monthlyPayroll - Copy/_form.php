<?php
/* @var $this MonthlyPayrollController */
/* @var $model MonthlyPayroll */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'monthly-payroll-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'staff_id'); ?>
		<?php echo $form->textField($model,'staff_id'); ?>
		<?php echo $form->error($model,'staff_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'level_id'); ?>
		<?php echo $form->textField($model,'level_id'); ?>
		<?php echo $form->error($model,'level_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'special_case_id'); ?>
		<?php echo $form->textField($model,'special_case_id'); ?>
		<?php echo $form->error($model,'special_case_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'basic_salary'); ?>
		<?php echo $form->textField($model,'basic_salary'); ?>
		<?php echo $form->error($model,'basic_salary'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'grade'); ?>
		<?php echo $form->textField($model,'grade'); ?>
		<?php echo $form->error($model,'grade'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'grade_amount'); ?>
		<?php echo $form->textField($model,'grade_amount'); ?>
		<?php echo $form->error($model,'grade_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'allowance_pf'); ?>
		<?php echo $form->textField($model,'allowance_pf'); ?>
		<?php echo $form->error($model,'allowance_pf'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'assistant_teacher'); ?>
		<?php echo $form->textField($model,'assistant_teacher'); ?>
		<?php echo $form->error($model,'assistant_teacher'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'class_teacher'); ?>
		<?php echo $form->textField($model,'class_teacher'); ?>
		<?php echo $form->error($model,'class_teacher'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'coordinator'); ?>
		<?php echo $form->textField($model,'coordinator'); ?>
		<?php echo $form->error($model,'coordinator'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'private_teacher'); ?>
		<?php echo $form->textField($model,'private_teacher'); ?>
		<?php echo $form->error($model,'private_teacher'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'head_teacher'); ?>
		<?php echo $form->textField($model,'head_teacher'); ?>
		<?php echo $form->error($model,'head_teacher'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'eca_chief'); ?>
		<?php echo $form->textField($model,'eca_chief'); ?>
		<?php echo $form->error($model,'eca_chief'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rahat'); ?>
		<?php echo $form->textField($model,'rahat'); ?>
		<?php echo $form->error($model,'rahat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'disable_mahila'); ?>
		<?php echo $form->textField($model,'disable_mahila'); ?>
		<?php echo $form->error($model,'disable_mahila'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'disable_purush'); ?>
		<?php echo $form->textField($model,'disable_purush'); ?>
		<?php echo $form->error($model,'disable_purush'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'insurance_add'); ?>
		<?php echo $form->textField($model,'insurance_add'); ?>
		<?php echo $form->error($model,'insurance_add'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gross_total'); ?>
		<?php echo $form->textField($model,'gross_total'); ?>
		<?php echo $form->error($model,'gross_total'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'deduction_pf'); ?>
		<?php echo $form->textField($model,'deduction_pf'); ?>
		<?php echo $form->error($model,'deduction_pf'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'deduction_insurance'); ?>
		<?php echo $form->textField($model,'deduction_insurance'); ?>
		<?php echo $form->error($model,'deduction_insurance'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'advance'); ?>
		<?php echo $form->textField($model,'advance'); ?>
		<?php echo $form->error($model,'advance'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'others'); ?>
		<?php echo $form->textField($model,'others'); ?>
		<?php echo $form->error($model,'others'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cit'); ?>
		<?php echo $form->textField($model,'cit'); ?>
		<?php echo $form->error($model,'cit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'net_salary'); ?>
		<?php echo $form->textField($model,'net_salary'); ?>
		<?php echo $form->error($model,'net_salary'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'yearly_total'); ?>
		<?php echo $form->textField($model,'yearly_total'); ?>
		<?php echo $form->error($model,'yearly_total'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'monthly_tax'); ?>
		<?php echo $form->textField($model,'monthly_tax'); ?>
		<?php echo $form->error($model,'monthly_tax'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'net_payment'); ?>
		<?php echo $form->textField($model,'net_payment'); ?>
		<?php echo $form->error($model,'net_payment'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'month'); ?>
		<?php echo $form->textField($model,'month'); ?>
		<?php echo $form->error($model,'month'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'year'); ?>
		<?php echo $form->textField($model,'year'); ?>
		<?php echo $form->error($model,'year'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
		<?php echo $form->error($model,'created_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->