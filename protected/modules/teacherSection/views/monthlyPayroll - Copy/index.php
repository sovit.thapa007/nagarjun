<?php
/* @var $this MonthlyPayrollController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Monthly Payrolls',
);

$this->menu=array(
	array('label'=>'Create MonthlyPayroll', 'url'=>array('create')),
	array('label'=>'Manage MonthlyPayroll', 'url'=>array('admin')),
);
?>

<h1>Monthly Payrolls</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
