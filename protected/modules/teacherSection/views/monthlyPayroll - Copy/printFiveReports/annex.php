<style>
table#payroll tbody tr td{
 border: 1px solid black;
}

table#payroll tr th{
 border: 1px solid black;
}

table#payroll{
 border-collapse: collapse;;
}

</style>

<h4 style="text-align:center">

<?php  

    echo UtilityFunctions::SchoolInformation()->title;
    echo "<br>";
    echo UtilityFunctions::SchoolInformation()->tole;
    echo "<br>";

     ?>

<?php
if(!empty($month) && !empty($year)) 
{ 
    echo "Salary Sheet for Year ".UtilityFunctions::getYear()[$year].' '.UtilityFunctions::getMonth()[$month].' Month';
}
else
{

   echo "Select Month and Year for Salary Sheet"; 
}
?>
    

</h4> <hr>
   
<br>
<?php 


$allowances = Allowances::model()->findAll();
$deductions=Deductions::model()->findAll();


$grandBasicTotal=0;
$grandPFTotal=0;
$grandAllowance=0;
$grandInsuranceTotal=0;
$grandGrossTotal=0;
$grandPFDeductionTotal=0;
$grandDeduction=0;
$grandInsuranceDeductionTotal=0;
$grandNetTotal=0;
$grandYearlyTotal=0;
$grandMonthlyTaxTotal=0;
$grandNetPayment=0;

 ?>
<table class="table table-bordered table-striped" id="payroll">
    <thead>
    <thead>
        <tr>
            <th rowspan="3">S.N</th>
            <th rowspan="3">Full Name</th>
            <th rowspan="3">Level</th>
            <th colspan="3">Salary</th>
           
            <th rowspan="3">Allowances</th>
            
            <th rowspan="3">Gross Total</th>
             <th colspan="<?php echo count($deductions); ?>">Deductions</th>
            
           
            <th rowspan="3"> N.Total</th>    
            <th rowspan="3"> Yearly Total</th>    
            <th rowspan="3"> Monthly Tax</th>         
            <th rowspan="3"> Net payment</th>            
        </tr>
        <tr>
            <th> Basic </th>
            <th> Grade </th>
            <th> Grade Amount</th>
           
             <?php foreach($deductions as $deduction){ ?>
                <th><?php echo $deduction->name; ?></th>
            <?php } ?>
        </tr>
    </thead>
    <tbody>
     <tr><td></td></tr>
        <?php 
        $gTotalSalary=0;
        $ntotal = 0;
        $yearlyIncome=0;
         $allowanceTotal=0;
        $tax=0;

        if(!empty($staffs)){ 
         $sn=0;
         
   
        foreach ($staffs as $staff) { 
             $allowanceTotal=0;
            $isPartTime=false;   
            $deductionTotal=0;
            $gtotal = 0; 
            $sn++;
           
            ?>


        <tr>

           
        
            <td><?php echo $sn; ?></td>
            <td><?php echo $staff->staff->fname.' '.$staff->staff->lname; ?></td>
            <td><?php echo '' ?></td>
            <td><?php echo $staff->basic_salary; $grandBasicTotal+= $staff->basic_salary;?></td>
            <td><?php echo $staff->grade; ?></td>
            <td><?php echo $staff->grade_amount; ?></td>
            
 
 
             <?php foreach($allowances as $allowanceDb){ ?>
            <?php 

                    $string=$allowanceDb->allowanceName;
                   
                    $allowance=strtolower(str_replace(' ', '_',$string));
                    $allowanceTotal+= $staff->$allowance;
            ?>
           

       
        <?php } ?>
           

            <td><?php 
            
                echo $allowanceTotal; 
                $grandAllowance+= $allowanceTotal;
                ?></td>
           
          
            <td>
            <?php 
            echo $staff->gross_total;
            $grandGrossTotal += $staff->gross_total;

            ?>

            </td>
            

          

           <?php foreach($deductions as $deductionDb){ ?>
           
            <td>
            
            <?php 

                     $string=$deductionDb->name;
                   
                    $deduction=strtolower(str_replace(' ', '_',$string));
                   $deductionTotal+= $staff->$deduction;

                   echo $staff->$deduction;

            ?>
            </td>
        

           <?php } ?>
          
            <td>
            <?php 
            echo $staff->net_salary;
            $grandNetTotal += $staff->net_salary;
            ?>
            </td>
           
            <td><?php echo $staff->yearly_total; $grandYearlyTotal+= $staff->yearly_total;?></td>
            <td><?php echo $staff->monthly_tax; $grandMonthlyTaxTotal+= $staff->monthly_tax; ?></td> 
             <td><?php echo $staff->net_payment; $grandNetPayment += $staff->net_payment;?></td>

        </tr>
        <?php }  } ?>
<?php 
       
        if(!empty($staffs)){ 
        
  
        foreach ($staffs as $staff) { 
           
            
            ?>

        <tr>
         
            <td><?php echo 'Total' ?></td>
            <td><?php echo '';?></td>

            <td><?php '' ?></td>
            <td><?php echo $grandBasicTotal; ?></td>
            <td><?php echo '';?></td>
             <td><?php echo ''; ?></td>
            
            <td> <?php echo $grandAllowance; ?></td>
           
            <td><?php echo $grandGrossTotal; ?></td>
          
                  
           <?php foreach($deductions as $deduction){ ?>
            <td>
            <?php echo '';
            ?>

            </td>

            <?php } ?>

           
            <td><?php echo $grandNetTotal; ?></td>
             <td><?php echo $grandYearlyTotal; ?></td>
             <td><?php echo $grandMonthlyTaxTotal; ?></td>
           
           <!--  <td><?php //echo $nsalary = $nsalary - $tdeduction; $ntotal += $nsalary; ?></td> -->
             <td><?php echo $grandNetPayment; ?></td>
  
        </tr>
        <?php break; } }?>



           
    </tbody>
</table>

