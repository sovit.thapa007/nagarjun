<?php
/* @var $this PartTimeTeacherController */
/* @var $model PartTimeTeacher */

$this->breadcrumbs=array(
	'Part Time Teachers'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PartTimeTeacher', 'url'=>array('index')),
	array('label'=>'Create PartTimeTeacher', 'url'=>array('create')),
	array('label'=>'Update PartTimeTeacher', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PartTimeTeacher', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PartTimeTeacher', 'url'=>array('admin')),
);
?>

<h1>View PartTimeTeacher #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'staff_id',
		'created_date',
		'created_by',
		'theory_class_attend',
		'practical_class_attend',
		'theory_class_amount',
		'practical_class_amount',
		'month',
	),
)); ?>
