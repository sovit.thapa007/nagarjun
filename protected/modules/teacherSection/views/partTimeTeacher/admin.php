<?php
/* @var $this PartTimeTeacherController */
/* @var $model PartTimeTeacher */

$this->breadcrumbs=array(
	'Part Time Teachers'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List PartTimeTeacher', 'url'=>array('index')),
	array('label'=>'Create PartTimeTeacher', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#part-time-teacher-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Part Time Teachers</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'part-time-teacher-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'staff_id',
		'created_date',
		'created_by',
		'theory_class_attend',
		'practical_class_attend',
		/*
		'theory_class_amount',
		'practical_class_amount',
		'month',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
