<?php
/* @var $this PartTimeTeacherController */
/* @var $model PartTimeTeacher */

$this->breadcrumbs=array(
	'Part Time Teachers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PartTimeTeacher', 'url'=>array('index')),
	array('label'=>'Manage PartTimeTeacher', 'url'=>array('admin')),
);
?>

<h1>Create PartTimeTeacher</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'array1'=>$array1,'month'=>$month)); ?>