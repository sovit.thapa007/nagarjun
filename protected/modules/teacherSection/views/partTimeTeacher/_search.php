<?php
/* @var $this PartTimeTeacherController */
/* @var $model PartTimeTeacher */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'staff_id'); ?>
		<?php echo $form->textField($model,'staff_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'theory_class_attend'); ?>
		<?php echo $form->textField($model,'theory_class_attend'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'practical_class_attend'); ?>
		<?php echo $form->textField($model,'practical_class_attend'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'theory_class_amount'); ?>
		<?php echo $form->textField($model,'theory_class_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'practical_class_amount'); ?>
		<?php echo $form->textField($model,'practical_class_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'month'); ?>
		<?php echo $form->textField($model,'month'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->