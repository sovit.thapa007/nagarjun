<?php
/* @var $this PartTimeTeacherController */
/* @var $data PartTimeTeacher */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('staff_id')); ?>:</b>
	<?php echo CHtml::encode($data->staff_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('theory_class_attend')); ?>:</b>
	<?php echo CHtml::encode($data->theory_class_attend); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('practical_class_attend')); ?>:</b>
	<?php echo CHtml::encode($data->practical_class_attend); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('theory_class_amount')); ?>:</b>
	<?php echo CHtml::encode($data->theory_class_amount); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('practical_class_amount')); ?>:</b>
	<?php echo CHtml::encode($data->practical_class_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('month')); ?>:</b>
	<?php echo CHtml::encode($data->month); ?>
	<br />

	*/ ?>

</div>