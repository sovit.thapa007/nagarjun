<?php
/* @var $this PartTimeTeacherController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Part Time Teachers',
);

$this->menu=array(
	array('label'=>'Create PartTimeTeacher', 'url'=>array('create')),
	array('label'=>'Manage PartTimeTeacher', 'url'=>array('admin')),
);
?>

<h1>Part Time Teachers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
