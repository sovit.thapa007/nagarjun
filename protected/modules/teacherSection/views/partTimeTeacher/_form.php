<?php
/* @var $this PartTimeTeacherController */
/* @var $model PartTimeTeacher */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'part-time-teacher-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'staff_id'); ?>
		<?php echo $form->dropDownList($model, 'staff_id', $array1, array('class'=>'span7')); ?> 
		<?php echo $form->error($model,'staff_id'); ?>
	</div>

	<!-- <div class="row">
		<?php echo $form->labelEx($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
		<?php echo $form->error($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div> -->

	<div class="row">
		<?php echo $form->labelEx($model,'theory_class_attend'); ?>
		<?php echo $form->textField($model,'theory_class_attend'); ?>
		<?php echo $form->error($model,'theory_class_attend'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'theory_class_amount'); ?>
		<?php echo $form->textField($model,'theory_class_amount'); ?>
		<?php echo $form->error($model,'theory_class_amount'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'practical_class_attend'); ?>
		<?php echo $form->textField($model,'practical_class_attend'); ?>
		<?php echo $form->error($model,'practical_class_attend'); ?>
	</div>

	

	<div class="row">
		<?php echo $form->labelEx($model,'practical_class_amount'); ?>
		<?php echo $form->textField($model,'practical_class_amount'); ?>
		<?php echo $form->error($model,'practical_class_amount'); ?>
	</div>

	 <div class="row">
		<?php echo $form->labelEx($model,'month'); ?>
		<?php echo $form->dropDownList($model, 'month', $month, array('class'=>'span7')); ?> 
		<?php echo $form->error($model,'month'); ?>
	</div> 

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->