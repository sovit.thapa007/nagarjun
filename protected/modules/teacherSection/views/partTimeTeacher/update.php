<?php
/* @var $this PartTimeTeacherController */
/* @var $model PartTimeTeacher */

$this->breadcrumbs=array(
	'Part Time Teachers'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PartTimeTeacher', 'url'=>array('index')),
	array('label'=>'Create PartTimeTeacher', 'url'=>array('create')),
	array('label'=>'View PartTimeTeacher', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PartTimeTeacher', 'url'=>array('admin')),
);
?>

<h1>Update PartTimeTeacher <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>