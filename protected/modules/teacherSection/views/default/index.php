
<?php
/* @var $this DefaultController */

$this->breadcrumbs = array(
	$this->module->id,
);
?>
<div class="row-fluid quick-actions">
	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/teacherSection/personaldetails/create" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/teacher.svg" alt="school register"></span>
			<h3><strong>TEACHER DETAILS</strong></h3>
		</a>
	</div>
	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/teacherSection/trainingInfo/create" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/professor.svg" alt="school register"></span>
			<h3><strong>TEACHER TRAINING</strong></h3>
		</a>
	</div>
	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/teacherSection/teachingHistory/create" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/presentation.svg" alt="school register"></span>
			<h3><strong>TEACHER HISTORY</strong></h3>
		</a>
	</div>
	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/teacherSection/income/create" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/profit.svg" alt="school register"></span>
			<h3><strong>TEACHER INCOME</strong></h3>
		</a>
	</div>
	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/teacherSection/leaveDetails/create" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/desk.svg" alt="school register"></span>
			<h3><strong>TEACHER LEAVE</strong></h3>
		</a>
	</div>
	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/teacherSection/report/index" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/report.svg" alt="school register"></span>
			<h3><strong>TEACHER REPORT</strong></h3>
		</a>
	</div>
</div>

	