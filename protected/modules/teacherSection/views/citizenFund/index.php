<?php
/* @var $this CitizenFundController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Citizen Funds',
);

$this->menu=array(
	array('label'=>'Create CitizenFund', 'url'=>array('create')),
	array('label'=>'Manage CitizenFund', 'url'=>array('admin')),
);
?>

<h1>Citizen Funds</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
