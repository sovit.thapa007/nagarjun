<?php
/* @var $this CitizenFundController */
/* @var $model CitizenFund */

$this->breadcrumbs=array(
	'Citizen Funds'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CitizenFund', 'url'=>array('index')),
	array('label'=>'Create CitizenFund', 'url'=>array('create')),
	array('label'=>'View CitizenFund', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CitizenFund', 'url'=>array('admin')),
);
?>

<h1>Update CitizenFund <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>