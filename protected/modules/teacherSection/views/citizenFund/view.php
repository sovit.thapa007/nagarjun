<?php
/* @var $this CitizenFundController */
/* @var $model CitizenFund */

$this->breadcrumbs=array(
	'Citizen Funds'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CitizenFund', 'url'=>array('index')),
	array('label'=>'Create CitizenFund', 'url'=>array('create')),
	array('label'=>'Update CitizenFund', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CitizenFund', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CitizenFund', 'url'=>array('admin')),
);
?>

<h1>View CitizenFund #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'percentage',
		'effective_date',
		'created_date',
	),
)); ?>
