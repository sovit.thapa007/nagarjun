<?php
/* @var $this CitizenFundController */
/* @var $model CitizenFund */

$this->breadcrumbs=array(
	'Citizen Funds'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CitizenFund', 'url'=>array('index')),
	array('label'=>'Manage CitizenFund', 'url'=>array('admin')),
);
?>

<h1>Create CitizenFund</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>