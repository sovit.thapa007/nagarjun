<div class="container adduser">
<?php
	$baseUrl = Yii::app()->request->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl . '/js/registration/jquery.create.js');
	$cs->registerScriptFile($baseUrl . '/js/registration/jquery.validation.js');
	$this->breadcrumbs=array(
		'Teacher Section' => Yii::app()->getModule('teacherSection')->getBaseUrl(),
		'Teacher Report'=>array('index'),
		'Teacher Report'
	);
	?>

	<div class="row-fluid" style="margin-top:20px;">
      <div class="row-fluid">
        <?php
        $sn=1;
        $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>' TEACHER DETAILS '));
        echo $this->renderPartial('_personal_report', array('model'=>$model),true);
        $this->endWidget();
        $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>' TEACHER INCOME '));
        echo $this->renderPartial('_teacher_income', array('model'=>$model));
        $this->endWidget();
        $this->beginWidget('system.web.widgets.CClipWidget', array('id'=>' TEACHER LEAVE '));
        echo $this->renderPartial('_teacher_leave', array('model'=>$model));
        $this->endWidget();
        $tabParameters = array();
        foreach($this->clips as $key=>$clip)
          $tabParameters['tab'.(count($tabParameters)+1)] = array('title'=>$key, 'content'=>$clip);
        ?>

        <?php $this->widget('system.web.widgets.CTabView', array('tabs'=>$tabParameters)); ?>



      </div>
    </div>
</div>
	