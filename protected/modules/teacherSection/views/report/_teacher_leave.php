<div class="row-fluid">
<h4 class="text-center"> Teacher Leave Report</h4>

	<span style="float :right; margin-right:5px;">
		<form action="#" method="POST">
	        <button class="btn btn-success" name="leave_report" id="leave_report">Export(Excel)</button>
	    </form>
	</span>
</div>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
'type' => 'striped bordered',
'id'=>'leave-details-grid',
'dataProvider'=>LeaveDetails::model()->search(),
	'columns'=>array(
		array('name'=>'personalDetailsid',
		'value'=>'Personaldetails::model()->findByPk($data->personalDetailsid)->full_name',
		'filter'=>CHtml::listData(Personaldetails::model()->findAll(),'id','full_name'),
		),
		'leave_type',
		'from',
		'to',
),
)); ?>

	
<?php
    if(isset($_POST['leave_report']))
    {
        
    $this->widget('EExcelView', array(
     'dataProvider'=>LeaveDetails::model()->search(),
            'grid_mode'=>'export',
            'title'=>'Teacher Leave Report',
            'filename'=>'Teacher Leave Report ('.date('Y-m-d').')',
            'stream'=>true,
            'exportType'=>'Excel2007',
            'columns'=>array(
				array('name'=>'personalDetailsid',
					'value'=>'Personaldetails::model()->findByPk($data->personalDetailsid)->full_name',
					'filter'=>CHtml::listData(Personaldetails::model()->findAll(),'id','full_name'),
					),
					'leave_type',
					'from',
					'to',
							
			            ),
        ));
}
?>

