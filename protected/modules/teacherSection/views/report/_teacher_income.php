<div class="row-fluid">
<h4 class="text-center"> Teacher Income Report</h4>

	<span style="float :right; margin-right:5px;">
		<form action="#" method="POST">
	        <button class="btn btn-success" name="income_report" id="income_report">Export(Excel)</button>
	    </form>
	</span>
</div>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
'type' => 'striped bordered',
'id'=>'income-grid',
'dataProvider'=>Income::model()->search(),
	'columns'=>array(
		array('name'=>'personalDetailsid',
		'value'=>'Personaldetails::model()->findByPk($data->personalDetailsid)->full_name',
		'filter'=>CHtml::listData(Personaldetails::model()->findAll(),'id','full_name'),
		),
		'monthly_salary',
		'grade_num',
		'grade_amount',
		'head_teacher',
),
)); ?>

	
<?php
    if(isset($_POST['income_report']))
    {
        
    $this->widget('EExcelView', array(
     'dataProvider'=>Income::model()->search(),
            'grid_mode'=>'export',
            'title'=>'Teacher Income Report',
            'filename'=>'Teacher Income Report ('.date('Y-m-d').')',
            'stream'=>true,
            'exportType'=>'Excel2007',
            'columns'=>array(
				array('name'=>'personalDetailsid',
				'value'=>'Personaldetails::model()->findByPk($data->personalDetailsid)->full_name',
				'filter'=>CHtml::listData(Personaldetails::model()->findAll(),'id','full_name'),
				),
				'monthly_salary',
				'grade_num',
				'grade_amount',
				'head_teacher',	
				'dress',
				'festival',
				'insurance',
				'remote',
				'medicine',
				'mahangi',
				'provident_fund',
				'citizen_investment',
				
            ),
        ));
}
?>

