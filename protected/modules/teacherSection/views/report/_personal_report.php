<div class="row-fluid">
<h4 class="text-center"> Personal Details Report</h4>

	<span style="float :right; margin-right:5px;">
		<form action="#" method="POST">
	        <button class="btn btn-success" name="personal_report" id="personal_report">Export(Excel)</button>
	    </form>
	</span>
</div>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
'type' => 'striped bordered',
'id'=>'personaldetails-grid',
'dataProvider'=>Personaldetails::model()->search(),
	'columns'=>array(
		'full_name',
		'gender',
		'caste',
		'nationality',
		'date_of_birth',
),
)); ?>


<?php
    if(isset($_POST['personal_report']))
    {
        
    $this->widget('EExcelView', array(
     'dataProvider'=>Personaldetails::model()->search(),
            'grid_mode'=>'export',
            'title'=>'Teacher Personal Detail Report',
            'filename'=>'Teacher Personal Detail Report ('.date('Y-m-d').')',
            'stream'=>true,	
            'exportType'=>'Excel2007',
            'columns'=>array(
				'full_name',
				'gender',
				'caste',
				'nationality',
				'date_of_birth',				
				'citizenship_no',
				'issue_district',
				'fathers_name',
				'mothers_name',
				'spouse_name',
				'will_person',
				'mother_tongue',
				'disability',
				'email',
				'contact_number',
				'current_level',
				'position',
				'rank',
				'teaching_language',
				'license_no',
				'insurance_no',
				'pf_ac_no',
				'trk_no',
				'bank_name',
				'bank_ac_no',
				
            ),
        ));
}
?>

