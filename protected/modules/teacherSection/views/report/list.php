<h2><center><?= strtoupper($type) ?> Report Column Setting</center></h2>
<div class="row-fluid">

	<div class="span6">
		<table>
		<tr>
			<th>Allowance Name</th>
			<th>Status</th>
			<th>Action</th>

		</tr>

		<?php 
			foreach ($allowances as $key => $value) { 
			?>
			<tr>
				<td><?= $value->allowanceName ?></td>
				<td><?php 
					if(in_array($value->allowanceId, $report_allowances))
						echo "Included";
					else
						echo "Not-Included";

				?> </td>

				<td><?php
					if(in_array($value->allowanceId, $report_allowances))
						echo "<a href='".Yii::app()->request->baseUrl."/teacherSection/report/deactivateAllowance?report_id=".$report_id."&&allowance_id=".$value->allowanceId."&&type=".$type."'>EXCLUDE</a>";
					else
						echo "<a href='".Yii::app()->request->baseUrl."/teacherSection/report/activateAllowance?report_id=".$report_id."&&allowance_id=".$value->allowanceId."&&type=".$type."'>INCLUDE</a>";
					?>

				</td>
			</tr>	


			<?php }

		?>
		</table>
	</div>


<div class="span6">
	<table>
		<tr>
			<th>Deduction Name</th>
			<th>Status</th>
			<th>Action</th>

		</tr>

		<?php 
			foreach ($deductions as $key => $value) { 
			?>
			<tr>
				<td><?= $value->name ?></td>
				<td><?php 
					if(in_array($value->id, $report_deductions))
						echo "Included";
					else
						echo "Not-Included";

				?> </td>

				<td><?php
					if(in_array($value->id, $report_deductions))
						echo "<a href='".Yii::app()->request->baseUrl."/teacherSection/report/deactivateDeduction?report_id=".$report_id."&&deduction_id=".$value->id."&&type=".$type."'>EXCLUDE</a>";
					else
						echo "<a href='".Yii::app()->request->baseUrl."/teacherSection/report/activateDeduction?report_id=".$report_id."&&deduction_id=".$value->id."&&type=".$type."'>INCLUDE</a>";
					?>

				</td>
			</tr>	


			<?php }

		?>
		</table>
</div>
</div>