<?php
/* @var $this TeachingHistoryController */
/* @var $data TeachingHistory */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('personalDetailsid')); ?>:</b>
	<?php echo CHtml::encode($data->personalDetailsid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('appointment_made')); ?>:</b>
	<?php echo CHtml::encode($data->appointment_made); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('decision_made')); ?>:</b>
	<?php echo CHtml::encode($data->decision_made); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('level')); ?>:</b>
	<?php echo CHtml::encode($data->level); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rank')); ?>:</b>
	<?php echo CHtml::encode($data->rank); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('position')); ?>:</b>
	<?php echo CHtml::encode($data->position); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('district')); ?>:</b>
	<?php echo CHtml::encode($data->district); ?>
	<br />

	*/ ?>

</div>