<?php
/* @var $this TeachingHistoryController */
/* @var $model TeachingHistory */

$this->breadcrumbs=array(
	'Teaching Histories'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TeachingHistory', 'url'=>array('index')),
	array('label'=>'Create TeachingHistory', 'url'=>array('create')),
	array('label'=>'View TeachingHistory', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TeachingHistory', 'url'=>array('admin')),
);
?>

<h1>Update TeachingHistory <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>