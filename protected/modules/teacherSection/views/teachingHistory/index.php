<?php
/* @var $this TeachingHistoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Teaching Histories',
);

$this->menu=array(
	array('label'=>'Create TeachingHistory', 'url'=>array('create')),
	array('label'=>'Manage TeachingHistory', 'url'=>array('admin')),
);
?>

<h1>Teaching Histories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
