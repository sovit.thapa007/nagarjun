<?php
/* @var $this TeachingHistoryController */
/* @var $model TeachingHistory */

$this->breadcrumbs=array(
	'Teaching Histories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TeachingHistory', 'url'=>array('index')),
	array('label'=>'Manage TeachingHistory', 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>