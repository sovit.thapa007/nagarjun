<?php
/* @var $this TeachingHistoryController */
/* @var $model TeachingHistory */

$this->breadcrumbs=array(
	'Teaching Histories'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TeachingHistory', 'url'=>array('index')),
	array('label'=>'Create TeachingHistory', 'url'=>array('create')),
	array('label'=>'Update TeachingHistory', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TeachingHistory', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TeachingHistory', 'url'=>array('admin')),
);
?>

<h1>View TeachingHistory #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'personalDetailsid',
		'appointment_made',
		'decision_made',
		'level',
		'rank',
		'position',
		'district',
	),
)); ?>
