<?php
$baseUrl = Yii::app()->request->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl . '/js/registration/jquery.create.js');
$cs->registerScriptFile($baseUrl . '/js/registration/jquery.validation.js');

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'teaching-history-form',
	'type' => 'horizontal',
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
	),
	//'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
	),
	'focus' => array($model, 'personalDetailsid'),
	));
?>

<style>
	.custom-error, .span12.custom-error{
		border: 1px solid red;
	}
	.table-bordered input.custom-error,.table-bordered input.custom-error:focus,.custom-error:focus,.span12.custom-error:focus{
		border: 1px solid red;
	}
	.table-bordered input,.table-bordered select, .table-bordered textArea {
		width: 130px;
	}
</style>
<!-- form -->
<div class="row-fluid">
	<div class="span12">
		
			<?php echo $form->errorSummary($model); ?>
            <div class="box-title"><p class="help-block">Fields with <span class="required">*</span> are required.</p></div>
            <div class="box-content nopadding">
                <div class="row-fluid">
                	<div class="control-group" >
                		<div class="control-group">
							<h3 class="text-center"> Teaching History Details: </h3>
	                    </div>
		
						<?php echo $form->dropDownListRow($model, 'personalDetailsid', CHtml::listData(Personaldetails::model()->findAll(), 'id', 'full_name'), array('prompt' => 'Select Teacher', 'class' => 'span12')); ?>
		
						<?php echo $form->textFieldRow($model,'appointment_made',array('size'=>10,'maxlength'=>10, 'class' => 'span12')); ?>
		
						<?php echo $form->textFieldRow($model,'decision_made',array('size'=>10,'maxlength'=>10, 'class' => 'span12')); ?>
		
						<?php echo $form->textFieldRow($model,'level',array('size'=>35,'maxlength'=>35, 'class' => 'span12')); ?>
		
						<?php echo $form->textFieldRow($model,'rank',array('size'=>50,'maxlength'=>50, 'class' => 'span12')); ?>
							
						<?php echo $form->textFieldRow($model,'position', array('class' => 'span12')); ?>
		
						<?php echo $form->dropDownListRow($model, 'district', CHtml::listData(Location::model()->findAll(array('condition'=>'location_slug=:location_slug','params'=>array(':location_slug'=>'ds'))), 'title', 'title'), array('prompt' => 'Select District', 'class' => 'span12')); ?>
						
					</div>

					<div class="form-actions">
						<?php
						$this->widget('bootstrap.widgets.TbButton', array(
							'buttonType' => 'submit',
							'type' => 'primary',
							'label' => $model->isNewRecord ? 'Create' : 'Save',
							'htmlOptions' => array(
								'id' => "create-button",
								'name' => 'createButton',
								'onClick' => 'return validateForm();'
						)));
						$this->endWidget();
						?>
					</div>
				</div>
			</div>
	
	</div>
</div>