<?php
/* @var $this LeaveDetailsController */
/* @var $model LeaveDetails */

$this->breadcrumbs=array(
	'Leave Details'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List LeaveDetails', 'url'=>array('index')),
	array('label'=>'Manage LeaveDetails', 'url'=>array('admin')),
);
?>

<h1>Create LeaveDetails</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>