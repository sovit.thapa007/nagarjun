<?php
/* @var $this LeaveDetailsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Leave Details',
);

$this->menu=array(
	array('label'=>'Create LeaveDetails', 'url'=>array('create')),
	array('label'=>'Manage LeaveDetails', 'url'=>array('admin')),
);
?>

<h1>Leave Details</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
