<?php
/* @var $this LeaveDetailsController */
/* @var $model LeaveDetails */

$this->breadcrumbs=array(
	'Leave Details'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List LeaveDetails', 'url'=>array('index')),
	array('label'=>'Create LeaveDetails', 'url'=>array('create')),
	array('label'=>'Update LeaveDetails', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete LeaveDetails', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LeaveDetails', 'url'=>array('admin')),
);
?>

<h1>View LeaveDetails #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'personalDetailsid',
		'leave_type',
		'from',
		'to',
	),
)); ?>
