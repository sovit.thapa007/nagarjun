<?php
/* @var $this LeaveDetailsController */
/* @var $data LeaveDetails */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('personalDetailsid')); ?>:</b>
	<?php echo CHtml::encode($data->personalDetailsid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('leave_type')); ?>:</b>
	<?php echo CHtml::encode($data->leave_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('from')); ?>:</b>
	<?php echo CHtml::encode($data->from); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('to')); ?>:</b>
	<?php echo CHtml::encode($data->to); ?>
	<br />


</div>