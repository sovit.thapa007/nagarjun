<?php
/* @var $this DeductionsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Deductions',
);

$this->menu=array(
	array('label'=>'Create Deductions', 'url'=>array('create')),
	array('label'=>'Manage Deductions', 'url'=>array('admin')),
);
?>

<h1>Deductions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
