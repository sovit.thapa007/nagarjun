<?php
/* @var $this DeductionsController */
/* @var $model Deductions */

$this->breadcrumbs=array(
	'Deductions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Deductions', 'url'=>array('index')),
	array('label'=>'Manage Deductions', 'url'=>array('admin')),
);
?>

<h1>Create Deductions</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>