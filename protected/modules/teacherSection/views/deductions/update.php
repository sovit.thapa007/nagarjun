<?php
/* @var $this DeductionsController */
/* @var $model Deductions */

$this->breadcrumbs=array(
	'Deductions'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Deductions', 'url'=>array('index')),
	array('label'=>'Create Deductions', 'url'=>array('create')),
	array('label'=>'View Deductions', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Deductions', 'url'=>array('admin')),
);
?>

<h1>Update Deductions <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>