<?php
/* @var $this AllowancesController */
/* @var $model Allowances */

$this->breadcrumbs=array(
	'Allowances'=>array('index'),
	$model->allowanceId=>array('view','id'=>$model->allowanceId),
	'Update',
);

$this->menu=array(
	array('label'=>'List Allowances', 'url'=>array('index')),
	array('label'=>'Create Allowances', 'url'=>array('create')),
	array('label'=>'View Allowances', 'url'=>array('view', 'id'=>$model->allowanceId)),
	array('label'=>'Manage Allowances', 'url'=>array('admin')),
);
?>

<h1>Update Allowances <?php echo $model->allowanceId; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>