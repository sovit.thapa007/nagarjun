<?php
/* @var $this AllowancesController */
/* @var $model Allowances */

$this->breadcrumbs=array(
	'Allowances'=>array('index'),
	$model->allowanceId,
);

$this->menu=array(
	array('label'=>'List Allowances', 'url'=>array('index')),
	array('label'=>'Create Allowances', 'url'=>array('create')),
	array('label'=>'Update Allowances', 'url'=>array('update', 'id'=>$model->allowanceId)),
	array('label'=>'Delete Allowances', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->allowanceId),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Allowances', 'url'=>array('admin')),
);
?>

<h1>View Allowances #<?php echo $model->allowanceId; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'allowanceId',
		'allowanceName',
	),
)); ?>
