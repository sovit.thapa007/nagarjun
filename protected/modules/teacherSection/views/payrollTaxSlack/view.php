<?php
/* @var $this PayrollTaxSlackController */
/* @var $model PayrollTaxSlack */

$this->breadcrumbs=array(
	'Payroll Tax Slacks'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PayrollTaxSlack', 'url'=>array('index')),
	array('label'=>'Create PayrollTaxSlack', 'url'=>array('create')),
	array('label'=>'Update PayrollTaxSlack', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PayrollTaxSlack', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PayrollTaxSlack', 'url'=>array('admin')),
);
?>

<h1>View PayrollTaxSlack #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'first_tax_percent',
		'first_tax_amount',
		'second_tax_percent',
		'second_tax_amount',
		'third_tax_percent',
		'third_tax_amount',
		'created_date',
	),
)); ?>
