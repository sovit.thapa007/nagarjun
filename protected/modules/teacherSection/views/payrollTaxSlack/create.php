<?php
/* @var $this PayrollTaxSlackController */
/* @var $model PayrollTaxSlack */

$this->breadcrumbs=array(
	'Payroll Tax Slacks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PayrollTaxSlack', 'url'=>array('index')),
	array('label'=>'Manage PayrollTaxSlack', 'url'=>array('admin')),
);
?>

<h1>Create PayrollTaxSlack</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>