<?php
/* @var $this PayrollTaxSlackController */
/* @var $model PayrollTaxSlack */

$this->breadcrumbs=array(
	'Payroll Tax Slacks'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List PayrollTaxSlack', 'url'=>array('index')),
	array('label'=>'Create PayrollTaxSlack', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#payroll-tax-slack-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Payroll Tax Slabs</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'payroll-tax-slack-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'first_tax_percent',
		'first_tax_amount',
		'second_tax_percent',
		'second_tax_amount',
		'third_tax_percent',
		/*
		'third_tax_amount',
		'created_date',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
