<?php
/* @var $this PayrollTaxSlackController */
/* @var $model PayrollTaxSlack */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'first_tax_percent'); ?>
		<?php echo $form->textField($model,'first_tax_percent'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'first_tax_amount'); ?>
		<?php echo $form->textField($model,'first_tax_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'second_tax_percent'); ?>
		<?php echo $form->textField($model,'second_tax_percent'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'second_tax_amount'); ?>
		<?php echo $form->textField($model,'second_tax_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'third_tax_percent'); ?>
		<?php echo $form->textField($model,'third_tax_percent'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'third_tax_amount'); ?>
		<?php echo $form->textField($model,'third_tax_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->