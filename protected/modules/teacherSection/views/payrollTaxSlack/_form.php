<?php
/* @var $this PayrollTaxSlackController */
/* @var $model PayrollTaxSlack */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'payroll-tax-slack-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'first_tax_percent'); ?>
		<?php echo $form->textField($model,'first_tax_percent'); ?>
		<?php echo $form->error($model,'first_tax_percent'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'first_tax_amount'); ?>
		<?php echo $form->textField($model,'first_tax_amount'); ?>
		<?php echo $form->error($model,'first_tax_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'second_tax_percent'); ?>
		<?php echo $form->textField($model,'second_tax_percent'); ?>
		<?php echo $form->error($model,'second_tax_percent'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'second_tax_amount'); ?>
		<?php echo $form->textField($model,'second_tax_amount'); ?>
		<?php echo $form->error($model,'second_tax_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'third_tax_percent'); ?>
		<?php echo $form->textField($model,'third_tax_percent'); ?>
		<?php echo $form->error($model,'third_tax_percent'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'third_tax_amount'); ?>
		<?php echo $form->textField($model,'third_tax_amount'); ?>
		<?php echo $form->error($model,'third_tax_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
		<?php echo $form->error($model,'created_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->