<?php
/* @var $this PayrollTaxSlackController */
/* @var $model PayrollTaxSlack */

$this->breadcrumbs=array(
	'Payroll Tax Slacks'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PayrollTaxSlack', 'url'=>array('index')),
	array('label'=>'Create PayrollTaxSlack', 'url'=>array('create')),
	array('label'=>'View PayrollTaxSlack', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PayrollTaxSlack', 'url'=>array('admin')),
);
?>

<h1>Update PayrollTaxSlack <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>