<?php
/* @var $this PayrollTaxSlackController */
/* @var $data PayrollTaxSlack */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_tax_percent')); ?>:</b>
	<?php echo CHtml::encode($data->first_tax_percent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_tax_amount')); ?>:</b>
	<?php echo CHtml::encode($data->first_tax_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('second_tax_percent')); ?>:</b>
	<?php echo CHtml::encode($data->second_tax_percent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('second_tax_amount')); ?>:</b>
	<?php echo CHtml::encode($data->second_tax_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('third_tax_percent')); ?>:</b>
	<?php echo CHtml::encode($data->third_tax_percent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('third_tax_amount')); ?>:</b>
	<?php echo CHtml::encode($data->third_tax_amount); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	*/ ?>

</div>