<?php
/* @var $this PayrollTaxSlackController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Payroll Tax Slacks',
);

$this->menu=array(
	array('label'=>'Create PayrollTaxSlack', 'url'=>array('create')),
	array('label'=>'Manage PayrollTaxSlack', 'url'=>array('admin')),
);
?>

<h1>Payroll Tax Slacks</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
