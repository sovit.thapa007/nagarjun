<?php
/* @var $this StaffSpecialCaseController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Staff Special Cases',
);

$this->menu=array(
	array('label'=>'Create StaffSpecialCase', 'url'=>array('create')),
	array('label'=>'Manage StaffSpecialCase', 'url'=>array('admin')),
);
?>

<h1>Staff Special Cases</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
