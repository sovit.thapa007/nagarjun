<?php
/* @var $this StaffSpecialCaseController */
/* @var $model StaffSpecialCase */

$this->breadcrumbs=array(
	'Staff Special Cases'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List StaffSpecialCase', 'url'=>array('index')),
	array('label'=>'Create StaffSpecialCase', 'url'=>array('create')),
	array('label'=>'View StaffSpecialCase', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage StaffSpecialCase', 'url'=>array('admin')),
);
?>

<h1>Update StaffSpecialCase <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>