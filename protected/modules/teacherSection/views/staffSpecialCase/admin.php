<?php
/* @var $this StaffSpecialCaseController */
/* @var $model StaffSpecialCase */

$this->breadcrumbs=array(
	'Staff Special Cases'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List StaffSpecialCase', 'url'=>array('index')),
	array('label'=>'Create StaffSpecialCase', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#staff-special-case-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Staff Special Cases</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<br>
<?php echo CHtml::link('Create','create',array('class'=>'btn btn-success')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'staff-special-case-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'name',
		'created_date',
		'updated_by',
		'id',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
