<?php
/* @var $this StaffSpecialCaseController */
/* @var $model StaffSpecialCase */

$this->breadcrumbs=array(
	'Staff Special Cases'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List StaffSpecialCase', 'url'=>array('index')),
	array('label'=>'Manage StaffSpecialCase', 'url'=>array('admin')),
);
?>

<h1>Create StaffSpecialCase</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>