<?php
/* @var $this TrainingInfoController */
/* @var $model TrainingInfo */

$this->breadcrumbs=array(
	'Training Infos'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List TrainingInfo', 'url'=>array('index')),
	array('label'=>'Create TrainingInfo', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#training-info-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Training Infos</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'training-info-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
                        'header'=>'№',
                        'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                        'htmlOptions'=>array(
                                'width'=>'25',
                                'class'=>'centered'
                        )
                ),
		//'personalDetailsid',
		array(
			'name'=>'personalDetailsid',
			'value'=>'Personaldetails::model()->findByPk($data->pdetail_id)->full_name',
			'filter'=>CHtml::listData(Personaldetails::model()->findAll(),'id','full_name'),
		),
		'year',
		'type',
		'subject',
		'duration',
		/*
		'organiser',
		*/
		array(
					'header'=>'Action',
					'class' => 'bootstrap.widgets.TbButtonColumn',
				),
	),
)); ?>
