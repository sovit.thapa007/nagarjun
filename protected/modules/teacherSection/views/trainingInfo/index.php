<?php
/* @var $this TrainingInfoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Training Infos',
);

$this->menu=array(
	array('label'=>'Create TrainingInfo', 'url'=>array('create')),
	array('label'=>'Manage TrainingInfo', 'url'=>array('admin')),
);
?>

<h1>Training Infos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
