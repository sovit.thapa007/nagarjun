<?php
/* @var $this TrainingInfoController */
/* @var $model TrainingInfo */

$this->breadcrumbs=array(
	'Training Infos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TrainingInfo', 'url'=>array('index')),
	array('label'=>'Create TrainingInfo', 'url'=>array('create')),
	array('label'=>'View TrainingInfo', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TrainingInfo', 'url'=>array('admin')),
);
?>

<h1>Update TrainingInfo <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>