<?php
/* @var $this TrainingInfoController */
/* @var $model TrainingInfo */

$this->breadcrumbs=array(
	'Training Infos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TrainingInfo', 'url'=>array('index')),
	array('label'=>'Manage TrainingInfo', 'url'=>array('admin')),
);
?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>