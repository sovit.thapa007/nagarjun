<?php
/* @var $this TrainingInfoController */
/* @var $model TrainingInfo */

$this->breadcrumbs=array(
	'Training Infos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TrainingInfo', 'url'=>array('index')),
	array('label'=>'Create TrainingInfo', 'url'=>array('create')),
	array('label'=>'Update TrainingInfo', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TrainingInfo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TrainingInfo', 'url'=>array('admin')),
);
?>

<h1>View TrainingInfo #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'personalDetailsid',
		'year',
		'type',
		'subject',
		'duration',
		'organiser',
	),
)); ?>
