<?php
/* @var $this PayrollPfController */
/* @var $model PayrollPf */

$this->breadcrumbs=array(
	'Payroll Pfs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PayrollPf', 'url'=>array('index')),
	array('label'=>'Manage PayrollPf', 'url'=>array('admin')),
);
?>

<h1>Create PayrollPf</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>