<?php
/* @var $this PayrollPfController */
/* @var $model PayrollPf */

$this->breadcrumbs=array(
	'Payroll Pfs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PayrollPf', 'url'=>array('index')),
	array('label'=>'Create PayrollPf', 'url'=>array('create')),
	array('label'=>'View PayrollPf', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PayrollPf', 'url'=>array('admin')),
);
?>

<h1>Update PayrollPf <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>