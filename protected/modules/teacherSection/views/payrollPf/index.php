<?php
/* @var $this PayrollPfController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Payroll Pfs',
);

$this->menu=array(
	array('label'=>'Create PayrollPf', 'url'=>array('create')),
	array('label'=>'Manage PayrollPf', 'url'=>array('admin')),
);
?>

<h1>Payroll Pfs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
