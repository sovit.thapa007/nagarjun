<?php
/* @var $this PayrollPfController */
/* @var $model PayrollPf */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pf_percent'); ?>
		<?php echo $form->textField($model,'pf_percent'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'effective_date'); ?>
		<?php echo $form->textField($model,'effective_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->