<?php
/* @var $this PayrollPfController */
/* @var $model PayrollPf */

$this->breadcrumbs=array(
	'Payroll Pfs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PayrollPf', 'url'=>array('index')),
	array('label'=>'Create PayrollPf', 'url'=>array('create')),
	array('label'=>'Update PayrollPf', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PayrollPf', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PayrollPf', 'url'=>array('admin')),
);
?>

<h1>View PayrollPf #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'pf_percent',
		'effective_date',
	),
)); ?>
