<?php
/* @var $this PayrollPfController */
/* @var $model PayrollPf */

$this->breadcrumbs=array(
	'Payroll Pfs'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List PayrollPf', 'url'=>array('index')),
	array('label'=>'Create PayrollPf', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#payroll-pf-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Payroll Pfs</h1>


<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<br>
<br>
<?php echo CHtml::link('Create','create',array('class'=>'btn btn-success')); ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'payroll-pf-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'pf_percent',
		'effective_date',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
