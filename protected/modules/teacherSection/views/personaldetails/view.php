<?php
/* @var $this PersonaldetailsController */
/* @var $model Personaldetails */

$this->breadcrumbs=array(
	'Personaldetails'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Personaldetails', 'url'=>array('index')),
	array('label'=>'Create Personaldetails', 'url'=>array('create')),
	array('label'=>'Update Personaldetails', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Personaldetails', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Personaldetails', 'url'=>array('admin')),
);
?>

<h1>View Personaldetails #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'full_name',
		'gender',
		
		'date_of_birth',
	
		'disability',
		
		'contact_number',
		 [                      // the owner name of the model
            'label' => 'Join Date',
            'value' => $model->getStaff()->join_date,
        ],

     
        [                      // the owner name of the model
            'label' => 'Marital Status',
            'value' => $model->getStaff()->marital_status,
        ],
		
		'insurance_no',
		'pf_ac_no',
		
		'bank_name',
		'bank_ac_no',
		'cif_no'
	),
)); ?>
