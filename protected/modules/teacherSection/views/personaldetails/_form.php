<?php
$baseUrl = Yii::app()->request->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl . '/js/registration/jquery.create.js');
$cs->registerScriptFile($baseUrl . '/js/registration/jquery.validation.js');

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'personaldetails-form-form',
	'type' => 'horizontal',
	'clientOptions' => array(
		'validateOnSubmit' => true,
	),
	'focus' => array($model, 'full_name'),
));


?>
<style>
	.custom-error, .span12.custom-error{
		border: 1px solid red;
	}
	.table-bordered input.custom-error,.table-bordered input.custom-error:focus,.custom-error:focus,.span12.custom-error:focus{
		border: 1px solid red;
	}
	.table-bordered input,.table-bordered select, .table-bordered textArea {
		width: 130px;
	}
</style>
<!-- form -->
<div class="row-fluid">
	<div class="span6" >
		<?php echo $form->textFieldRow($staff, 'fname', array('class'=>'span12', 'placeholder'=>'First Name')) ?>
		<?php echo $form->textFieldRow($staff, 'lname', array('class'=>'span12', 'placeholder'=>'Last Name')) ?>
		<?php echo $form->radioButtonListRow($model, 'gender', $data = array("male" => "male", "female" => "female", "other" => "other"), $rowOptions = array('checked' => false)); ?>

<!-- <?php echo $form->dropDownListRow($model, 'caste', CHtml::listData(EthinicGroup::model()->findAll(), 'title', 'title'), array('prompt' => 'Select Ethinic Group', 'class' => 'span12')); ?>

<?php echo $form->dropDownListRow($model, 'nationality', array("nepali" => "nepali", "others" => "others"), array('class' => 'span12')); ?> -->

<?php echo $form->textFieldRow($model, 'date_of_birth', array('class' => 'span12 date', 'autocomplete' => false, 'data-mask-clearifnotmatch' => "true")); ?>
<?php echo $form->textFieldRow($staff, 'address', array('class'=>'span7', 'placeholder'=>'Address')) ?>

<!-- <?php echo $form->textFieldRow($model,'citizenship_no',array('size'=>35,'maxlength'=>35, 'style'=>'Width:332px;')); ?>

<?php //echo $form->dropDownListRow($model, 'issue_district', CHtml::listData(Location::model()->findAll(array('condition'=>'location_slug=:location_slug','params'=>array(':location_slug'=>'ds'))), 'title', 'title'), array('prompt' => 'Select Issued District', 'class' => 'span12')); ?>

<?php echo $form->textFieldRow($model,'fathers_name',array('size'=>60,'maxlength'=>250, 'style'=>'Width:332px;')); ?>

<?php echo $form->textFieldRow($model,'mothers_name',array('size'=>60,'maxlength'=>250, 'style'=>'Width:332px;')); ?>

<?php echo $form->textFieldRow($model,'spouse_name',array('size'=>60,'maxlength'=>250, 'style'=>'Width:332px;')); ?>

<?php echo $form->textFieldRow($model,'will_person',array('size'=>60,'maxlength'=>250, 'style'=>'Width:332px;')); ?>

<?php //echo $form->dropDownListRow($model, 'mother_tongue', CHtml::listData(MotherLangauge::model()->findAll(), 'id', 'title'), array('prompt' => 'Select Mother Tongue', 'class' => 'span12')); ?> -->

<?php echo $form->textFieldRow($staff, 'contact', array('class'=>'span7', 'placeholder'=>'Contact')) ?>
<?php echo $form->dropDownListRow($staff, 'marital_status', array(''=>'--Marital Status', 'Single'=>'Single', 'Married'=>'Married', 'Divorced'=>'Divorced','Unknown'=>'Unknown'), array('class'=>'span7')); ?>


<?php echo $form->textFieldRow($staff, 'join_date', array('class' => 'span12 date', 'autocomplete' => false, 'data-mask-clearifnotmatch' => "true")); ?>
<?php echo $form->textFieldRow($model,'disability',array('size'=>10,'maxlength'=>10, 'style'=>'Width:332px;')); ?>


<?php //echo $form->textFieldRow($model,'email',array('class' => 'span12', 'maxlength' => 100)); ?>	

<?php //echo $form->textFieldRow($model,'contact_number',array('size'=>15,'maxlength'=>15, 'style'=>'Width:332px;')); ?>	

</div>
<div class="span6" >
<!-- <?php echo $form->textFieldRow($model,'current_level',array('size'=>60,'maxlength'=>100)); ?>

<?php echo $form->textFieldRow($model,'position',array('size'=>60,'maxlength'=>100)); ?>

<?php echo $form->textFieldRow($model,'rank',array('size'=>50,'maxlength'=>50)); ?>

<?php echo $form->textFieldRow($model,'teaching_language',array('size'=>60,'maxlength'=>100)); ?>

<?php echo $form->textFieldRow($model,'license_no',array('size'=>55,'maxlength'=>55)); ?>	
-->


<!-- <?php echo $form->textFieldRow($model,'trk_no',array('size'=>50,'maxlength'=>50)); ?> -->




<?php echo $form->dropDownListRow($departmentStaff, 'department_id', $array1, array('multiple'=>'true','class'=>'span7','options'=>$selectedPost)); ?>
<?php echo $form->dropDownListRow($staff, 'level_id', $array3, array('class'=>'span7')); ?>
<?php echo $form->dropDownListRow($staff, 'shreni_id', $array4, array('class'=>'span7')); ?>
<?php echo $form->textFieldRow($model,'insurance_no',array('size'=>50,'maxlength'=>50)); ?>
<?php echo $form->textFieldRow($model,'pf_ac_no',array('size'=>50,'maxlength'=>50)); ?>
<?php echo $form->textFieldRow($model,'cif_no',array('size'=>50,'maxlength'=>50)); ?>
<?php echo $form->textFieldRow($model,'bank_name',array('size'=>60,'maxlength'=>250)); ?>

<?php echo $form->textFieldRow($model,'bank_ac_no',array('size'=>50,'maxlength'=>50)); ?>




</div>					
</div>



<div class="form-actions text-center">
	<?php
	$this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'type' => 'primary',
		'label' => $model->isNewRecord ? 'Create' : 'Save',
		'htmlOptions' => array(
			'id' => "create-button",
			'name' => 'createButton',
			'onClick' => 'return validateForm();'
		)));
		?>
	</div>	
	<?php $this->endWidget();?>