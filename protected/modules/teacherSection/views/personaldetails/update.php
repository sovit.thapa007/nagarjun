<?php
/* @var $this PersonaldetailsController */
/* @var $model Personaldetails */

$this->breadcrumbs=array(
	'Personaldetails'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Personaldetails', 'url'=>array('index')),
	array('label'=>'Create Personaldetails', 'url'=>array('create')),
	array('label'=>'View Personaldetails', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Personaldetails', 'url'=>array('admin')),
);
?>

<h1>Update Personal Details of <?php echo $model->full_name; ?></h1>

<?= $this->renderPartial('_form',array(
				'model'=>$model,
				'staff'=>$staff,
				'departmentList'=>$departmentList,
				'officeTime'=>$officeTime,	
				
				'array1'=>$array1,
				'array2'=>$array2,
				'array3'=>$array3,
				'array4'=>$array4,
				
				'staffSpecial'=>$staffSpecial,
				'departmentStaff'=>$departmentStaff,
				'staffAllowance'=>$staffAllowance,
				'staffDeduction'=>$staffDeduction,
				'selectedPost'=>$selectedPost
			));
?>