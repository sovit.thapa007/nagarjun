<?php
/* @var $this PersonaldetailsController */
/* @var $data Personaldetails */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('full_name')); ?>:</b>
	<?php echo CHtml::encode($data->full_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gender')); ?>:</b>
	<?php echo CHtml::encode($data->gender); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('caste')); ?>:</b>
	<?php echo CHtml::encode($data->caste); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nationality')); ?>:</b>
	<?php echo CHtml::encode($data->nationality); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_of_birth')); ?>:</b>
	<?php echo CHtml::encode($data->date_of_birth); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('citizenship_no')); ?>:</b>
	<?php echo CHtml::encode($data->citizenship_no); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('issue_district')); ?>:</b>
	<?php echo CHtml::encode($data->issue_district); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fathers_name')); ?>:</b>
	<?php echo CHtml::encode($data->fathers_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mothers_name')); ?>:</b>
	<?php echo CHtml::encode($data->mothers_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('spouse_name')); ?>:</b>
	<?php echo CHtml::encode($data->spouse_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('will_person')); ?>:</b>
	<?php echo CHtml::encode($data->will_person); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mother_tongue')); ?>:</b>
	<?php echo CHtml::encode($data->mother_tongue); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disability')); ?>:</b>
	<?php echo CHtml::encode($data->disability); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contact_number')); ?>:</b>
	<?php echo CHtml::encode($data->contact_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('current_level')); ?>:</b>
	<?php echo CHtml::encode($data->current_level); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('position')); ?>:</b>
	<?php echo CHtml::encode($data->position); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rank')); ?>:</b>
	<?php echo CHtml::encode($data->rank); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('teaching_language')); ?>:</b>
	<?php echo CHtml::encode($data->teaching_language); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('license_no')); ?>:</b>
	<?php echo CHtml::encode($data->license_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('insurance_no')); ?>:</b>
	<?php echo CHtml::encode($data->insurance_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pf_ac_no')); ?>:</b>
	<?php echo CHtml::encode($data->pf_ac_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trk_no')); ?>:</b>
	<?php echo CHtml::encode($data->trk_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bank_name')); ?>:</b>
	<?php echo CHtml::encode($data->bank_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bank_ac_no')); ?>:</b>
	<?php echo CHtml::encode($data->bank_ac_no); ?>
	<br />

	*/ ?>

</div>