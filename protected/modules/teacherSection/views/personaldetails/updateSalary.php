<?php
$baseUrl = Yii::app()->request->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl . '/js/registration/jquery.create.js');
$cs->registerScriptFile($baseUrl . '/js/registration/jquery.validation.js');

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'personaldetails-form-form',
    'type' => 'horizontal',
    'htmlOptions' => array(
        'class' => 'form-horizontal well',
    ),
    //'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'focus' => array($model, 'full_name'),
    ));


?>
<style>
    .custom-error, .span12.custom-error{
        border: 1px solid red;
    }
    .table-bordered input.custom-error,.table-bordered input.custom-error:focus,.custom-error:focus,.span12.custom-error:focus{
        border: 1px solid red;
    }
    .table-bordered input,.table-bordered select, .table-bordered textArea {
        width: 130px;
    }
</style>
<!-- form -->
<div class="row-fluid">
    <div class="span12">
        
            <?php echo $form->errorSummary($model); ?>
            <div class="box-title"><p class="help-block">Fields with <span class="required">*</span> are required.</p></div>
            <div class="box-content nopadding">
                <div class="row">
                    
                    <h1>Additional Information</h1>
                    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>true, // display a larger alert block?
        'fade'=>true, // use transitions?
        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
        'alerts'=>array( // configurations per alert type
            'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
            'info'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
            'error'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
            'warning'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
            'danger'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
        ),
    )); ?>

     <table class="table">








<td>

    <tr>
    <td><?php echo $form->labelEx($staff,'level_id'); ?></td>
    <td><?php echo $form->dropDownList($staff, 'level_id', $array2, array('class'=>'span7')); ?></td>
    <td><?php echo $form->dropDownList($staff, 'level_id', $array3, array('class'=>'span7')); ?></td>
    <td><?php echo $form->dropDownList($staff, 'shreni_id', $array4, array('class'=>'span7')); ?></td>
    </tr>

     <tr>
    <td><?php echo $form->labelEx($staffSpecial,'special_case_id'); ?></td>
    <td><?php 
     $staffSpecialId=SpecialCaseStaff::model()->findByAttributes(['staff_id'=>$staff->staff_id]);

    $id=$staffSpecialId != null ? $staffSpecialId->special_case_id : 0;
    echo $form->dropDownList($staffSpecial, 'special_case_id', $array5, array('options' => array($id=>array('selected'=>true)))); ?></td>
    
   
   
    </tr>

     <tr>
    <td><?php echo $form->labelEx($staff,'grade'); ?></td>
    <td><?php echo $form->dropDownList($staff, 'grade', array('0'=>'0','1'=>'1','2'=>'2','3'=>'3','4'=>'4'), array('class'=>'span7')); ?></td>
    <td><?php echo $form->labelEx($staff,'grade_amount'); ?></td>
     <td><?php echo $form->textField($staff, 'grade_amount', array('class'=>'span7')); ?></td>
    
   
   
    </tr>

    <tr>
    <td><?php echo $form->labelEx($staff,'basic_salary'); ?></td>
     <td><?php echo $form->textField($staff, 'basic_salary', array('class'=>'span7')); ?></td>


      <td><?php echo $form->labelEx($staff, 'teacher_type', array('class'=>'')) ?></td>
    <td><?php echo $form->dropDownList($staff,'teacher_type',array('1'=>'Full Time','2'=>'Part-Time')); ?></td>

    </tr>

    <tr>
        <td></td>
        <td></td>
        
            <td><?php echo $form->labelEx($staff,'part_time_class'); ?></td>
            <td><?php echo $form->textField($staff,'part_time_class'); ?></td>
     </tr>
      <tr>
        <td></td>
        <td></td>
            <td><?php echo $form->labelEx($staff,'part_time_class_amount'); ?></td>
            <td><?php echo $form->textField($staff,'part_time_class_amount'); ?></td>
       </tr>

     
   
    <tr>
    <tr>


  <td><h1><?php echo "Allowance Amount";?></h1><td/>
  </tr>
    <?php  
    $allowance=Allowances::model()->findAll();
    foreach($allowance as $name)
    {
    ?>
    <tr>
        <td><?php echo $name->allowanceName;?></td>
         <td><?php echo $form->textField($staffAllowanceNew, "allowanceAmounts[$name->allowanceId]"); ?></td>
         <td><?php echo $staffAllowance[$name->allowanceId] != null ?$staffAllowance[$name->allowanceId]->percentage:0; ?></td>
    </tr>

   <?php } ?>
<tr>


  <td><h1>Deduction</h1><td/>
  </tr>
    <?php  
    $deduction=Deductions::model()->findAll();
    foreach($deduction as $name)
    {
    ?>
    <tr>
    <td><?php echo $name->name;?></td>
     <td><?php echo $form->textField($staffDeductionNew, "deductionAmounts[$name->id]"); ?></td>
     <td><?php echo $staffDeduction[$name->id] != null ? $staffDeduction[$name->id]->amount:0; ?></td>

</tr>

   <?php }?>

   
</table>    


                <div class="form-actions">
                            <?php
                            $this->widget('bootstrap.widgets.TbButton', array(
                                'buttonType' => 'submit',
                                'type' => 'primary',
                                'label' => $model->isNewRecord ? 'Create' : 'Save',
                                'htmlOptions' => array(
                                    'id' => "create-button",
                                    'name' => 'createButton',
                                    'onClick' => 'return validateForm();'
                            )));
                            $this->endWidget();
                            ?>
                    </div>
            </div>
        
    </div>
</div>

