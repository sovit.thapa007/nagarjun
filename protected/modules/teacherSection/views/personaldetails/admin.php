<?php
/* @var $this PersonaldetailsController */
/* @var $model Personaldetails */

$this->breadcrumbs=array(
	'Personaldetails'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Personaldetails', 'url'=>array('index')),
	array('label'=>'Create Personaldetails', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#personaldetails-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Personaldetails</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'type' => 'striped bordered',
	'id'=>'personaldetails-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
	            'header'=>'№',
	            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
	            'htmlOptions'=>array(
	                    'width'=>'25',
	                    'class'=>'centered'
	            )
            ),
		'full_name',
		'gender',
		'caste',
		'nationality',
		'date_of_birth',
		/*
		'citizenship_no',
		'issue_district',
		'fathers_name',
		'mothers_name',
		'spouse_name',
		'will_person',
		'mother_tongue',
		'disability',
		'email',
		'contact_number',
		'current_level',
		'position',
		'rank',
		'teaching_language',
		'license_no',
		'insurance_no',
		'pf_ac_no',
		'trk_no',
		'bank_name',
		'bank_ac_no',
		*/
		 'link'=>array(
                        'header'=>'Edit Department/Amounts',
                        'type'=>'raw',
                        'value'=> 'CHtml::button("Edit Department/Amounts",array("onclick"=>"document.location.href=\'".Yii::app()->controller->createUrl("staff/updateDetails",array("id"=>$data->getStaff()->staff_id))."\'"))',
                ), 
		array(
					'header'=>'Action',
					'class' => 'bootstrap.widgets.TbButtonColumn',
				),
	),
)); ?>
