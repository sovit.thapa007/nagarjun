<?php
/* @var $this PersonaldetailsController */
/* @var $model Personaldetails */

$this->breadcrumbs=array(
	'Personaldetails'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Personaldetails', 'url'=>array('index')),
	array('label'=>'Manage Personaldetails', 'url'=>array('admin')),
);
?>
<div class="form-horizontal well">
	<h2 class="text-center"><strong>TEACHER PERSONAL DETAIL</strong></h2>



<?php $this->renderPartial('_form', array('model'=>$model,'staffSpecial'=>$staffSpecial, 'departmentStaff'=>$departmentStaff,'array5'=>$array5,'array2'=>$array2,'array3'=>$array3,'array1'=>$array1,'array4'=>$array4,'officeTime'=>$officeTime,'staff'=>$staff,'selectedPost'=>array())); ?>
</div>