<?php
/* @var $this PersonaldetailsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Personaldetails',
);

$this->menu=array(
	array('label'=>'Create Personaldetails', 'url'=>array('create')),
	array('label'=>'Manage Personaldetails', 'url'=>array('admin')),
);
?>

<h1>Personaldetails</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
