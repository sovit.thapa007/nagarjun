<style>
table#payroll tbody tr td{
 border: 1px solid black;
}

table#payroll tr th{
 border: 1px solid black;
}

table#payroll{
 border-collapse: collapse;;
}

</style>

<h4 style="text-align:center">

<?php  

    echo UtilityFunctions::SchoolInformation()->title;
    echo "<br>";
    echo UtilityFunctions::SchoolInformation()->tole;
    echo "<br>";

     ?>

<?php
if(!empty($month) && !empty($year)) 
{ 
    echo "PF Report for Year ".UtilityFunctions::getYear()[$year].' '.UtilityFunctions::getMonth()[$month].' Month';
}
else
{

   echo "Select Month and Year for PF Sheet"; 
}
?>
    

</h4> <hr>
  
<br>
<?php 

$grandTotalFundDeducted=0;
$grandDeductedFromEmployee=0;
$grandContributionByEmployeer=0;

 ?>
<table class="table table-bordered table-striped" id="payroll">
    <thead>
    <thead>
        <tr>
            <th>S.N</th>
            <th>Full Name</th>
            <th>Designation</th>
            <th>PF Number</th>
            <th>Total Fund Deducted</th>
            <th>PF Deducted from employee </th>
    
            <th> PF Contribution by Employeer</th>
           
    </thead>
    <tbody>
        <?php 

        if(!empty($staffs)){ 
         $sn=0;
         
  
        foreach ($staffs as $staff) { 
            
            $sn++;
           
            ?>

        <tr>

           
        
            <td><?php echo $sn; ?></td>
            <td><?php echo $staff->staff->fname.' '.$staff->staff->lname; ?></td>
            <td>
                <?php 
                $departments='';
                foreach ($staff->departmentList() as $key => $value) 
                {
                    $departments.=$value->department->department_name.',';   

                }
            echo rtrim($departments,','); ?>
            </td>
            <td><?php echo $staff->staff->details->pf_ac_no ?></td>
            <td><?php echo $staff->deduction_pf; $grandTotalFundDeducted+=$staff->deduction_pf ?></td>
            <td><?php echo $staff->allowance_pf; $grandDeductedFromEmployee+=$staff->allowance_pf ?></td>
            <td><?php echo $staff->allowance_pf;  $grandContributionByEmployeer+=$staff->allowance_pf ?></td>
           
            
 
 

        </tr>
        <?php }  } ?>
<?php 
       
        if(!empty($staffs)){ 
        
  
        foreach ($staffs as $staff) { 
           
            
            ?>

        <tr>
         
            <td colspan="4"><?php echo 'Total' ?></td>
           
            <td><?php echo $grandTotalFundDeducted; ?></td>
           <td><?php echo $grandDeductedFromEmployee; ?></td>
           <td><?php echo $grandContributionByEmployeer; ?></td>
  
        </tr>
        <?php break; } }?>



           
    </tbody>
</table>
