<style>table#payroll tbody tr td{
  text-align: right;
}

table#payroll tbody tr td:nth-child(1), table#payroll tbody tr td:nth-child(2), table#payroll tbody tr td:nth-child(3), table#payroll tbody tr td:nth-child(5){
  text-align: left;
}

table#payroll{
  width: 100%;
  display: block;
  
  overflow-y: scroll;
  overflow-x: scroll;
  
}

table#payroll thead tr th{
  text-align: center;
}
</style>

<h4>
<?php

  
      if(!empty($month) && !empty($year)) 
      { 
          echo " PF Ledger of Year ".UtilityFunctions::getYear()[$year].' '.UtilityFunctions::getMonth()[$month].' Month';
      }
      else
      {
         echo "Select Month and Year for PF Report"; 
      }

?>
    

</h4> <hr>
    <form action="" method="POST">

<?php echo CHtml::dropDownList('month','', UtilityFunctions::getMonth()); ?>

<?php echo CHtml::dropDownList('year', '',  UtilityFunctions::getYear()); ?>

 
       


<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'Search Monthly Salary',
    'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'small', // null, 'large', 'small' or 'mini'
    'htmlOptions'=>array('id'=>'special-submit'),
)); ?> 
</form>
<br>
<?php 

$grandTotalFundDeducted=0;
$grandDeductedFromEmployee=0;
$grandContributionByEmployeer=0;

 ?>
<table class="table table-bordered table-striped" id="payroll">
    <thead>
    <thead>
        <tr>
            <th>S.N</th>
            <th>Full Name</th>
            <th>Designation</th>
            <th>PF Number</th>
            <th>Total Fund Deducted</th>
            <th>PF Deducted from employee </th>
    
            <th> PF Contribution by Employeer</th>
           
    </thead>
    <tbody>
        <?php 

        if(!empty($staffs)){ 
         $sn=0;
         
  
        foreach ($staffs as $staff) { 
            
            $sn++;
           
            ?>

        <tr>

           
        
            <td><?php echo $sn; ?></td>
            <td><?php echo $staff->staff->fname.' '.$staff->staff->lname; ?></td>
            <td>
                <?php 
                $departments='';
                foreach ($staff->departmentList() as $key => $value) 
                {
                    $departments.=$value->department->department_name.',';   

                }
            echo rtrim($departments,','); ?>
            </td>
            <td><?php echo $staff->staff->details->pf_ac_no ?></td>
            <td><?php echo $staff->deduction_pf; $grandTotalFundDeducted+=$staff->deduction_pf ?></td>
            <td><?php echo $staff->allowance_pf; $grandDeductedFromEmployee+=$staff->allowance_pf ?></td>
            <td><?php echo $staff->allowance_pf;  $grandContributionByEmployeer+=$staff->allowance_pf ?></td>
           
            
 
 

        </tr>
        <?php }  } ?>
<?php 
       
        if(!empty($staffs)){ 
        
  
        foreach ($staffs as $staff) { 
           
            
            ?>

        <tr>
         
            <td colspan="4"><?php echo 'Total' ?></td>
           
            <td><?php echo $grandTotalFundDeducted; ?></td>
           <td><?php echo $grandDeductedFromEmployee; ?></td>
           <td><?php echo $grandContributionByEmployeer; ?></td>
  
        </tr>
        <?php break; } }?>



           
    </tbody>
</table>

<form action="" method="POST">
<?php


       
        echo CHtml::hiddenfield('printMonth',$month);
        echo CHtml::hiddenfield('printYear',$year);
        

        $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'EXPORT TO PDF',
            'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'htmlOptions'=>array('id'=>'submit'),
        )); 



?>
</form>
