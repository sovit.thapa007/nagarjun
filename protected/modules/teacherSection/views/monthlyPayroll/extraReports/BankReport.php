<style>table#payroll tbody tr td{
  text-align: right;
}

table#payroll tbody tr td:nth-child(1), table#payroll tbody tr td:nth-child(2), table#payroll tbody tr td:nth-child(3), table#payroll tbody tr td:nth-child(5){
  text-align: left;
}

table#payroll{
  width: 100%;
  display: block;
  
  overflow-y: scroll;
  overflow-x: scroll;
  
}

table#payroll thead tr th{
  text-align: center;
}
</style>

<h4>
<?php

  
      if(!empty($month) && !empty($year)) 
      { 
          echo " Bank Sheet of Year ".UtilityFunctions::getYear()[$year].' '.UtilityFunctions::getMonth()[$month].' Month';
      }
      else
      {
         echo "Select Month and Year for Salary Ledger"; 
      }

?>
    

</h4> <hr>
    <form action="" method="POST">


 <?php echo CHtml::dropDownList('special', '',  UtilityFunctions::getbankReportHeader()); ?>

<?php echo CHtml::dropDownList('month','', UtilityFunctions::getMonth()); ?>

<?php echo CHtml::dropDownList('year', '',  UtilityFunctions::getYear()); ?>

 
       


<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'Search Monthly Salary',
    'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'small', // null, 'large', 'small' or 'mini'
    'htmlOptions'=>array('id'=>'special-submit'),
)); ?> 
</form>
<br>
<?php 

$grandPayment=0;


 ?>
<table class="table table-bordered table-striped" id="payroll">
    <thead>
    <thead>
        <tr>
            <th>S.N</th>
            <th>Full Name</th>
            <th>A/c No</th>
            <th>Net Amount</th>
           
           
    </thead>
    <tbody>
        <?php 

        if(!empty($staffs)){ 
         $sn=0;
         
  
        foreach ($staffs as $staff) { 
            
            $sn++;
           
            ?>

        <tr>

           
        
            <td><?php echo $sn; ?></td>
            <td><?php echo $staff->staff->fname.' '.$staff->staff->lname; ?></td>
            <td><?php echo $staff->staff->details->bank_ac_no ?></td>
            <td><?php echo $staff->net_payment; $grandPayment+=$staff->net_payment ?></td>
           
            
 
 

        </tr>
        <?php }  } ?>
<?php 
       
        if(!empty($staffs)){ 
        
  
        foreach ($staffs as $staff) { 
           
            
            ?>

        <tr>
         
            <td><?php echo 'Total' ?></td>
            <td><?php echo '';?></td>

            <td><?php '' ?></td>
            <td><?php echo $grandPayment; ?></td>
       
  
        </tr>
        <?php break; } }?>



           
    </tbody>
</table>

<form action="" method="POST">
<?php


       
        echo CHtml::hiddenfield('printMonth',$month);
        echo CHtml::hiddenfield('printYear',$year);
        echo CHtml::hiddenfield('printSpecial',$special);
        

        $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'EXPORT TO PDF',
            'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'htmlOptions'=>array('id'=>'submit'),
        )); 



?>
</form>
