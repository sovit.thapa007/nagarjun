<style>table#payroll tbody tr td{
  text-align: right;
}

table#payroll tbody tr td:nth-child(1), table#payroll tbody tr td:nth-child(2), table#payroll tbody tr td:nth-child(3), table#payroll tbody tr td:nth-child(5){
  text-align: left;
}

table#payroll{
  width: 100%;
  display: block;
  
  overflow-y: scroll;
  overflow-x: scroll;
  
}

table#payroll thead tr th{
  text-align: center;
}
</style>
<h4>
<?php
if(!empty($month) && !empty($year)) 
{ 
    echo "Bank Sheet of Year ".UtilityFunctions::getYear()[$year].' '.UtilityFunctions::getMonth()[$month].' Month';
}
else
{

   echo "Select Month and Year for Bank Sheet"; 
}
?>
    

</h4> <hr>
    <form action="" method="POST">

<?php echo CHtml::dropDownList('month','', UtilityFunctions::getMonth()); ?>

<?php echo CHtml::dropDownList('year', '',  UtilityFunctions::getYear()); ?>



 <?php echo CHtml::dropDownList('special', '',  UtilityFunctions::getbankReportHeader()); ?>
 

       


<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'Search Monthly Salary',
    'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'small', // null, 'large', 'small' or 'mini'
    'htmlOptions'=>array('id'=>'special-submit'),
)); ?> 
</form>

<br>
<?php 

$grandBasicTotal=0;
$grandPFTotal=0;
$grandAllowance=0;
$grandInsuranceTotal=0;
$grandGrossTotal=0;
$grandPFDeductionTotal=0;
$grandDeduction=0;
$grandInsuranceDeductionTotal=0;
$grandNetTotal=0;
$grandYearlyTotal=0;
$grandMonthlyTaxTotal=0;
$grandNetPayment=0;

$allowances = Allowances::model()->findAll();
$deductions=Deductions::model()->findAll();

 ?>
<table class="table table-bordered table-striped" id="payroll">
    <thead>
       
<tr>
            <th rowspan="3">S.N</th>
            <th rowspan="3">Full Name</th>
             <th rowspan="3"> Bank Name</th> 
            <th rowspan="3"> Bank A/C</th> 
            <th rowspan="3"> Net payment</th>    
                      
        </tr>
    </thead>
      <tbody>
        <?php 
        $gTotalSalary=0;
        $ntotal = 0;
        $yearlyIncome=0;
         $allowanceTotal=0;
        $tax=0;



        if(!empty($staffs)){ 
         $sn=0;
         
  
        foreach ($staffs as $staff) { 
             $allowanceTotal=0;
            $isPartTime=false;   
            $deductionTotal=0;
            $gtotal = 0; 
            $sn++;
           
            ?>

        <tr>

           
        
            <td><?php echo $sn; ?></td>
            <td><?php echo $staff->staff->fname.' '.$staff->staff->lname; ?></td>
            <td><?php echo $model->getBankName($staff->staff_id); ?></td>
            <td><?php echo $model->getBankAc($staff->staff_id); ?></td>
            <td><?php echo $staff->net_payment; ?></td>
           
            
 
 <?php 

             
             $allowancePf=$staff->allowance_pf;
             $grandPFTotal += $allowancePf;
             ?>
             <?php foreach($allowances as $allowanceDb){ ?>
            <?php 

                    $string=$allowanceDb->allowanceName;
                   
                    $allowance=strtolower(str_replace(' ', '_',$string));
                    $allowanceTotal+= $staff->$allowance;
            ?>
           

       
        <?php } ?>
           

          <?php 
            
               
                $grandAllowance+= $allowanceTotal;
                ?>
          <?php 
            
                $grandInsuranceTotal += $staff->insurance_add; 
                ?>
            <?php 
         
            $grandGrossTotal += $staff->gross_total;

            ?>

            
            

            <?php 
           
         
            $grandPFDeductionTotal += $staff->deduction_pf;
            ?>
    
           
                <?php

                
                
                $grandInsuranceDeductionTotal += $staff->deduction_insurance;

                ?>
            


           <?php foreach($deductions as $deductionDb){ ?>
           
            
            <?php 

                     $string=$deductionDb->name;
                   
                    $deduction=strtolower(str_replace(' ', '_',$string));
                   $deductionTotal+= $staff->$deduction;

                   

            ?>
            
        

           <?php } ?>
           
           
            <?php 
          
            $grandNetTotal += $staff->net_salary;
            ?>
            
           
            <?php  $grandYearlyTotal+= $staff->yearly_total;?>
            <?php  $grandMonthlyTaxTotal+= $staff->monthly_tax; ?>
            <?php  $grandNetPayment += $staff->net_payment;?>

       
        <?php }  } ?>
<?php 
       
        if(!empty($staffs)){ 
        
  
        foreach ($staffs as $staff) { 
           
            
            ?>

        <tr>
         
            <td><?php echo 'Total' ?></td>
            <td></td>

            <td></td>
            <td></td>
            <td><?= $grandNetPayment; ?></td>
           
  
        </tr>
        <?php break; } }?>



           
    </tbody>
</table>





<form action="" method="POST">
<?php


       
        echo CHtml::hiddenfield('printMonth',$month);
        echo CHtml::hiddenfield('printYear',$year);
         echo CHtml::hiddenfield('printSpecial',$special);
         echo CHtml::hiddenfield('entry',false);
    

        $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'EXPORT TO PDF',
            'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'htmlOptions'=>array('id'=>'submit'),
        )); 



?>
</form>


<?php if($grandNetTotal != 0)
{
  ?>



<form action="" method="POST">

<?php        
        echo CHtml::hiddenfield('month',$month);
        echo CHtml::hiddenfield('year',$year);
        echo CHtml::hiddenfield('amount',$grandNetPayment);
        echo CHtml::hiddenfield('special',$special);
        echo CHtml::hiddenfield('entry',true);
        $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'Confirm Entry',
            'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'htmlOptions'=>array('id'=>'submitEntry'),
        )); 



?>

</form>


<?php } ?>


 