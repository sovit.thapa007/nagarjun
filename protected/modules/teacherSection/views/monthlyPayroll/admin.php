<?php
/* @var $this MonthlyPayrollController */
/* @var $model MonthlyPayroll */

$this->breadcrumbs=array(
	'Monthly Payrolls'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List MonthlyPayroll', 'url'=>array('index')),
	array('label'=>'Create MonthlyPayroll', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#monthly-payroll-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Monthly Payrolls</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'monthly-payroll-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'staff_id',
		'level_id',
		'special_case_id',
		'basic_salary',
		'grade',
		
		'grade_amount',
		'allowance_pf',
		'assistant_teacher',
		'class_teacher',
		'coordinator',
		'private_teacher',
		'head_teacher',
		'eca_chief',
		'rahat',
		'disable_mahila',
		'disable_purush',
		'insurance_add',
		'gross_total',
		'deduction_pf',
		'deduction_insurance',
		'advance',
		'others',
		'cif',
		'net_salary',
		'yearly_total',
		'monthly_tax',
		'net_payment',
		'month',
		'year',
		
		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
