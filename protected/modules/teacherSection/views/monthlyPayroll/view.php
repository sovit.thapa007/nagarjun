<?php
/* @var $this MonthlyPayrollController */
/* @var $model MonthlyPayroll */

$this->breadcrumbs=array(
	'Monthly Payrolls'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MonthlyPayroll', 'url'=>array('index')),
	array('label'=>'Create MonthlyPayroll', 'url'=>array('create')),
	array('label'=>'Update MonthlyPayroll', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MonthlyPayroll', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MonthlyPayroll', 'url'=>array('admin')),
);
?>

<h1>View MonthlyPayroll #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'staff_id',
		'level_id',
		'special_case_id',
		'basic_salary',
		'grade',
		'grade_amount',
		'allowance_pf',
		'assistant_teacher',
		'class_teacher',
		'coordinator',
		'private_teacher',
		'head_teacher',
		'eca_chief',
		'rahat',
		'disable_mahila',
		'disable_purush',
		'insurance_add',
		'gross_total',
		'deduction_pf',
		'deduction_insurance',
		'advance',
		'others',
		'cit',
		'net_salary',
		'yearly_total',
		'monthly_tax',
		'net_payment',
		'month',
		'year',
		'created_date',
	),
)); ?>
