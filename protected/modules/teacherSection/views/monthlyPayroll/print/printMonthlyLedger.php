

<h4 style="text-align:center">

<?php  

    echo UtilityFunctions::SchoolInformation()->title;
    echo "<br>";
    echo UtilityFunctions::SchoolInformation()->tole;
    echo "<br>";

     ?>

<?php
if(!empty($month) && !empty($year)) 
{ 
    echo "Salary Ledger of Year ".UtilityFunctions::getYear()[$year].' '.UtilityFunctions::getMonth()[$month].' Month';
}
else
{
   echo "Select Month and Year for Salary Ledger"; 
}
?>
</h4> <hr>
   

   <style>
table#payroll tbody tr td{
 border: 1px solid black;
}

table#payroll tr th{
 border: 1px solid black;
}

table#payroll{
 border-collapse: collapse;;
}

</style>
<br>
<?php 


$allowances = Allowances::model()->findAll();
$deductions=Deductions::model()->findAll();

$plus2Allowances=Plus2Allowances::model()->findALL();

$grandBasicTotal=0;
$grandPFTotal=0;
$grandInsuranceTotal=0;
$grandGrossTotal=0;
$grandPFDeductionTotal=0;
$grandInsuranceDeductionTotal=0;
$grandNetTotal=0;
$grandYearlyTotal=0;
$grandMonthlyTaxTotal=0;
$grandNetPayment=0;
 ?>
<table id="payroll">
    <thead>
        <tr>
            <th rowspan="3">S.N</th>
            <th rowspan="3">Full Name</th>
            <th rowspan="3">Level</th>
            <th colspan="3">Salary</th>
            <th colspan="<?php echo count($allowances) + 1; ?>">Allowances</th>
             <th rowspan="3">Insurance</th>
            <th rowspan="3">Gross Total</th>

            <th colspan="<?php echo count($deductions)+2; ?>">Deductions</th>
            <?php foreach ($plus2Allowances as $key => $pallowance) { ?>
                <th colspan="3"><?= $pallowance->name?></th>
            <?php } ?>
            <th rowspan="3"> N.Total</th>    
            <th rowspan="3"> Yearly Total</th>    
            <th rowspan="3"> Monthly Tax</th>         
            <th rowspan="3"> Net payment</th>            
        </tr>
        <tr>
            <th> Basic </th>
            <th> Grade </th>
            <th> Grade Amount</th>
            <th> PF/C(<?php echo $model->getPf(time()); ?>%) </th>
        
            <?php foreach($allowances as $allowance){ ?>
            <th><?php echo $allowance->allowanceName; ?></th>
            <?php } ?>
            <th> PF/C(<?php echo $model->getPf(time())*2; ?>%) </th>
          
            <th>Insurance</th>
             <?php foreach($deductions as $deduction){ ?>
                <th><?php echo $deduction->name; ?></th>
            <?php } ?>
            <?php foreach ($plus2Allowances as $key => $pallowance) { ?>
                <th>Days</th>
                <th>Amount</th>
                <th>Total</th>                

            <?php } ?>
            <!-- <th>P/F</th>
            <th>Self P/F</th>
            <th>TDS</th>
            <th>Advance</th>
            <th>Absense</th>
            <th>Others</th> -->
        </tr>
    </thead>
    <tbody>
    <tr>
    <td></td></tr>
        <?php 
        $gTotalSalary=0;
        $ntotal = 0;
        $yearlyIncome=0;
        $tax=0;

        if(!empty($staffs)){ 
         $sn=0;
         
  
        foreach ($staffs as $staff) { 
            
            $isPartTime=false;   
            $deductionTotal=0;
            $gtotal = 0; 
            $sn++;
           
            ?>

        <tr>

           
            
            <td><?php echo $sn; ?></td>
            <td><?php echo $staff->staff->fname.' '.$staff->staff->lname; ?></td>
            <td><?php echo '' ?></td>
            <td><?php echo $staff->basic_salary; $grandBasicTotal += $staff->basic_salary; ?></td>
            <td><?php echo $staff->grade; ?></td>
            <td><?php echo $staff->grade_amount; ?></td>
            
 
 <td><?php 

             
             $allowancePf=$staff->allowance_pf;
             $grandPFTotal += $allowancePf;
             echo  $allowancePf; ?></td>
             <?php foreach($allowances as $allowanceDb){ ?>
            <td><?php 

                    $string=$allowanceDb->allowanceName;
                   
                    $allowance=strtolower(str_replace(' ', '_',$string));
                    echo $staff->$allowance;
            ?>
           

        </td>
        <?php } ?>
           

            <td><?php 
            
                echo $staff->insurance_add; 
                $grandInsuranceTotal += $staff->insurance_add; 

                ?></td>
            <td>
            <?php 
            echo $staff->gross_total;
            $grandGrossTotal += $staff->gross_total;
            ?>

            </td>
            

            <td><?php 
           
            echo  $staff->deduction_pf;
            $grandPFDeductionTotal += $staff->deduction_pf;
            ?>
            </td>
            
            <td>
                <?php

                
                echo $staff->deduction_insurance;
                $grandInsuranceDeductionTotal += $staff->deduction_insurance;

                ?>
            </td>


           <?php foreach($deductions as $deductionDb){ ?>
            <td>

            <?php 

                     $string=$deductionDb->name;
                   
                    $deduction=strtolower(str_replace(' ', '_',$string));
                    echo $staff->$deduction;
            ?>

        </td>

           <?php } ?>
            
            <?php 
              $partTimeAmount=0;
             foreach ($plus2Allowances as $key => $pallowance) { 
                  $staffPlus2Model=null;
                  $partDays=null;
                  $partAmount=null;

                
                  $staffPlus2Model=Plus2AllowancesPayroll::model()->findByAttributes(array('payroll_id'=>$staff->id,'allowance_id'=>$pallowance->id));
                  if($staffPlus2Model != null)
                  {
                      $partDays=$staffPlus2Model->days;
                      $partAmount=$staffPlus2Model->amount;
                  }
            ?>

                <td><?= $partDays ?></td>
                <td><?= $partAmount ?></td>
                <td><?= $partAmount*$partDays ?></td>
            <?php 
                if($staffPlus2Model != null)
                {
                    $partTimeAmount += $partDays * $partAmount;
                }

            } 
            ?>
            <td>
            <?php 
            echo $staff->net_salary;
            $grandNetTotal += $staff->net_salary;
            ?>
            </td>
           
            <td><?php echo $staff->yearly_total; $grandYearlyTotal+= $staff->yearly_total; ?></td>
            <td><?php echo $staff->monthly_tax; $grandMonthlyTaxTotal+= $staff->monthly_tax;?></td> 
             <td><?php echo $staff->net_payment; $grandNetPayment+=  $staff->net_payment; ?></td>

        </tr>
        <?php }  } ?>


<?php 
       
        if(!empty($staffs)){ 
        
  
        foreach ($staffs as $staff) { 
           
            
            ?>

        <tr>
         
            <td><?php echo 'Total' ?></td>
            <td><?php echo '';?></td>

            <td><?php '' ?></td>
            <td><?php echo $grandBasicTotal; ?></td>
            <td><?php echo '';?></td>
             <td><?php echo ''; ?></td>
            <td><?php echo $grandPFTotal; ?></td>
             <?php foreach($allowances as $allowance){ ?>
            <td><?php /*echo $all = $model->getAllowances($staff->staff_id, $allowance->allowanceId,  time()) * $salary/100 ; 
            $gtotal += $all;*/
            ?>
            <?php echo ''; 
            
            ?>

            </td>
            <?php } ?>
            <td><?php 
            echo $grandInsuranceTotal; ?></td>
            <td><?php echo $grandGrossTotal; ?></td>
            <td><?php echo $grandPFDeductionTotal; ?></td>
             <td><?php echo $grandInsuranceDeductionTotal; ?></td>
           <?php foreach($deductions as $deduction){ ?>
            <td>
            <?php echo '';
            ?>

            </td>

            <?php } ?>
                
             <?php foreach ($plus2Allowances as $key => $pallowance) { ?>
                        <td></td>
                        <td></td>
                        <Td></Td>
                      
              <?php } ?>
           
            <td><?php echo $grandNetTotal; ?></td>
             <td><?php echo $grandYearlyTotal; ?></td>
             <td><?php echo $grandMonthlyTaxTotal; ?></td>
           
           <!--  <td><?php //echo $nsalary = $nsalary - $tdeduction; $ntotal += $nsalary; ?></td> -->
             <td><?php echo $grandNetPayment; ?></td>
  
        </tr>
        <?php break; } }?>

           
    </tbody>
</table>

