<style>
table#payroll tbody tr td{
 border: 1px solid black;
}

table#payroll tr th{
 border: 1px solid black;
}

table#payroll{
 border-collapse: collapse;;
}

</style>

<h4 style="text-align:center">

<?php  

    echo UtilityFunctions::SchoolInformation()->title;
    echo "<br>";
    echo UtilityFunctions::SchoolInformation()->tole;
    echo "<br>";

     ?>

<?php
if(!empty($month) && !empty($year)) 
{ 
    echo "Bank Sheet for Year ".UtilityFunctions::getYear()[$year].' '.UtilityFunctions::getMonth()[$month].' Month';
    echo "<br>".strtoupper($special);
}
else
{

   echo "Select Month and Year for Salary Sheet"; 
}
?>
    

</h4> <hr>
  
<br>
<?php 

$grandBasicTotal=0;
$grandPFTotal=0;
$grandAllowance=0;
$grandInsuranceTotal=0;
$grandGrossTotal=0;
$grandPFDeductionTotal=0;
$grandDeduction=0;
$grandInsuranceDeductionTotal=0;
$grandNetTotal=0;
$grandYearlyTotal=0;
$grandMonthlyTaxTotal=0;
$grandNetPayment=0;

$allowances = Allowances::model()->findAll();
$deductions=Deductions::model()->findAll();

 ?>
<table class="table table-bordered table-striped" id="payroll">
    <thead>
       
<tr>
            <th rowspan="3">S.N</th>
            <th rowspan="3">Full Name</th>
             <th rowspan="3"> Bank Name</th> 
            <th rowspan="3"> Bank A/C</th> 
            <th rowspan="3"> Net payment</th>    
                      
        </tr>
    </thead>
      <tbody>
      <tr><td></td></tr>
       <tr><td></td></tr>
        <?php 
        $gTotalSalary=0;
        $ntotal = 0;
        $yearlyIncome=0;
         $allowanceTotal=0;
        $tax=0;



        if(!empty($staffs)){ 
         $sn=0;
         
  
        foreach ($staffs as $staff) { 
             $allowanceTotal=0;
            $isPartTime=false;   
            $deductionTotal=0;
            $gtotal = 0; 
            $sn++;
           
            ?>

        <tr>

           
        
            <td><?php echo $sn; ?></td>
            <td><?php echo $staff->staff->fname.' '.$staff->staff->lname; ?></td>
            <td><?php echo $model->getBankName($staff->staff_id); ?></td>
            <td><?php echo $model->getBankAc($staff->staff_id); ?></td>
            <td><?php echo $staff->net_salary; ?></td>
           
            
 
 <?php 

             
             $allowancePf=$staff->allowance_pf;
             $grandPFTotal += $allowancePf;
             ?>
             <?php foreach($allowances as $allowanceDb){ ?>
            <?php 

                    $string=$allowanceDb->allowanceName;
                   
                    $allowance=strtolower(str_replace(' ', '_',$string));
                    $allowanceTotal+= $staff->$allowance;
            ?>
           

       
        <?php } ?>
           

          <?php 
            
               
                $grandAllowance+= $allowanceTotal;
                ?>
          <?php 
            
                $grandInsuranceTotal += $staff->insurance_add; 
                ?>
            <?php 
         
            $grandGrossTotal += $staff->gross_total;

            ?>

            
            

            <?php 
           
         
            $grandPFDeductionTotal += $staff->deduction_pf;
            ?>
    
           
                <?php

                
                
                $grandInsuranceDeductionTotal += $staff->deduction_insurance;

                ?>
            


           <?php foreach($deductions as $deductionDb){ ?>
           
            
            <?php 

                     $string=$deductionDb->name;
                   
                    $deduction=strtolower(str_replace(' ', '_',$string));
                   $deductionTotal+= $staff->$deduction;

                   

            ?>
            
        

           <?php } ?>
           
           
            <?php 
          
            $grandNetTotal += $staff->net_salary;
            ?>
            
           
            <?php  $grandYearlyTotal+= $staff->yearly_total;?>
            <?php  $grandMonthlyTaxTotal+= $staff->monthly_tax; ?>
            <?php  $grandNetPayment += $staff->net_payment;?>

       
        <?php }  } ?>
<?php 
       
        if(!empty($staffs)){ 
        
  
        foreach ($staffs as $staff) { 
           
            
            ?>

        <tr>
         
            <td><?php echo 'Total' ?></td>
            <td></td>

            <td></td>
            <td></td>
            <td><?= $grandNetTotal; ?></td>
           
  
        </tr>
        <?php break; } }?>



           
    </tbody>
</table>





