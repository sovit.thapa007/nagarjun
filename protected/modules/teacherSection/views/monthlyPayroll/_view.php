<?php
/* @var $this MonthlyPayrollController */
/* @var $data MonthlyPayroll */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('staff_id')); ?>:</b>
	<?php echo CHtml::encode($data->staff_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('level_id')); ?>:</b>
	<?php echo CHtml::encode($data->level_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('special_case_id')); ?>:</b>
	<?php echo CHtml::encode($data->special_case_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('basic_salary')); ?>:</b>
	<?php echo CHtml::encode($data->basic_salary); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('grade')); ?>:</b>
	<?php echo CHtml::encode($data->grade); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('grade_amount')); ?>:</b>
	<?php echo CHtml::encode($data->grade_amount); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('allowance_pf')); ?>:</b>
	<?php echo CHtml::encode($data->allowance_pf); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('assistant_teacher')); ?>:</b>
	<?php echo CHtml::encode($data->assistant_teacher); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('class_teacher')); ?>:</b>
	<?php echo CHtml::encode($data->class_teacher); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('coordinator')); ?>:</b>
	<?php echo CHtml::encode($data->coordinator); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('private_teacher')); ?>:</b>
	<?php echo CHtml::encode($data->private_teacher); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('head_teacher')); ?>:</b>
	<?php echo CHtml::encode($data->head_teacher); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('eca_chief')); ?>:</b>
	<?php echo CHtml::encode($data->eca_chief); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rahat')); ?>:</b>
	<?php echo CHtml::encode($data->rahat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disable_mahila')); ?>:</b>
	<?php echo CHtml::encode($data->disable_mahila); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disable_purush')); ?>:</b>
	<?php echo CHtml::encode($data->disable_purush); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('insurance_add')); ?>:</b>
	<?php echo CHtml::encode($data->insurance_add); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gross_total')); ?>:</b>
	<?php echo CHtml::encode($data->gross_total); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deduction_pf')); ?>:</b>
	<?php echo CHtml::encode($data->deduction_pf); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deduction_insurance')); ?>:</b>
	<?php echo CHtml::encode($data->deduction_insurance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('advance')); ?>:</b>
	<?php echo CHtml::encode($data->advance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('others')); ?>:</b>
	<?php echo CHtml::encode($data->others); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cit')); ?>:</b>
	<?php echo CHtml::encode($data->cit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('net_salary')); ?>:</b>
	<?php echo CHtml::encode($data->net_salary); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('yearly_total')); ?>:</b>
	<?php echo CHtml::encode($data->yearly_total); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('monthly_tax')); ?>:</b>
	<?php echo CHtml::encode($data->monthly_tax); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('net_payment')); ?>:</b>
	<?php echo CHtml::encode($data->net_payment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('month')); ?>:</b>
	<?php echo CHtml::encode($data->month); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('year')); ?>:</b>
	<?php echo CHtml::encode($data->year); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	*/ ?>

</div>