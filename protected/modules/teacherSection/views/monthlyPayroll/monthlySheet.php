<style>table#payroll tbody tr td{
  text-align: right;
}

table#payroll tbody tr td:nth-child(1), table#payroll tbody tr td:nth-child(2), table#payroll tbody tr td:nth-child(3), table#payroll tbody tr td:nth-child(5){
  text-align: left;
}

table#payroll{
  width: 100%;
  display: block;
  
  overflow-y: scroll;
  overflow-x: scroll;
  
}

table#payroll thead tr th{
  text-align: center;
}
</style>

<h4>
<?php
if(!empty($month) && !empty($year)) 
{ 
    echo "Salary Ledger of Year ".UtilityFunctions::getYear()[$year].' '.UtilityFunctions::getMonth()[$month].' Month';
}
else
{

   echo "Select Month and Year for Salary Ledger"; 
}
?>
    

</h4> <hr>
    <form action="" method="POST">

<?php echo CHtml::dropDownList('month','', UtilityFunctions::getMonth()); ?>

<?php echo CHtml::dropDownList('year', '',  UtilityFunctions::getYear()); ?>

 <?php echo CHtml::dropDownList('special', '', CHtml::listData(StaffSpecialCase::model()->findAll(), 'id', 'name'), array('prompt'=>'--Select Department--')); ?>

  
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'Search Monthly Salary',
    'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'small', // null, 'large', 'small' or 'mini'
    'htmlOptions'=>array('id'=>'special-submit'),
)); ?> 
</form>
<br>
<?php 


$allowances = Allowances::model()->findAll();
$deductions=Deductions::model()->findAll();

$plus2Allowances=Plus2Allowances::model()->findALL();

$grandBasicTotal=0;
$grandPFTotal=0;
$grandInsuranceTotal=0;
$grandGrossTotal=0;
$grandPFDeductionTotal=0;
$grandInsuranceDeductionTotal=0;
$grandNetTotal=0;
$grandYearlyTotal=0;
$grandMonthlyTaxTotal=0;
$grandNetPayment=0;
 ?>
<table class="table table-bordered table-striped" id="payroll">
    <thead>
        <tr>
            <th rowspan="3">S.N</th>
            <th rowspan="3">Full Name</th>
            <th rowspan="3">Level</th>
            <th colspan="3">Salary</th>
            <th colspan="<?php echo count($allowances) + 1; ?>">Allowances</th>
             <th rowspan="3">Insurance</th>
            <th rowspan="3">Gross Total</th>

            <th colspan="<?php echo count($deductions)+2; ?>">Deductions</th>
            <?php foreach ($plus2Allowances as $key => $pallowance) { ?>
                <th colspan="3" style="background-color: black; color:white;"><?= $pallowance->name?></th>
            <?php } ?>
            <th rowspan="3"> N.Total</th>    
            <th rowspan="3"> Yearly Total</th>    
            <th rowspan="3"> Monthly Tax</th>         
            <th rowspan="3"> Net payment</th>            
        </tr>
        <tr>
            <th style="background-color: #000055"> Basic </th>
            <th style="background-color: #000055"> Grade </th>
            <th style="background-color: #000055"> Grade Amount</th>
            <th style="background-color: #00405d"> PF/C(<?php echo $model->getPf(time()); ?>%) </th>
        
            <?php foreach($allowances as $allowance){ ?>
            <th style="background-color: #00405d"><?php echo $allowance->allowanceName; ?></th>
            <?php } ?>
            <th style="background-color: #000055"> PF/C(<?php echo $model->getPf(time())*2; ?>%) </th>
          
            <th style="background-color: #000055">Insurance</th>
             <?php foreach($deductions as $deduction){ ?>
                <th style="background-color: #000055"><?php echo $deduction->name; ?></th>
            <?php } ?>
             <?php foreach ($plus2Allowances as $key => $pallowance) { ?>
                <th style="background-color: black; color:white;">Days</th>
                <th style="background-color: black; color:white;">Amount</th>
                <th style="background-color: black; color:white;">Total</th>                

            <?php } ?>
            <!-- <th>P/F</th>
            <th>Self P/F</th>
            <th>TDS</th>
            <th>Advance</th>
            <th>Absense</th>
            <th>Others</th> -->
        </tr>
    </thead>
    <tbody>
        <?php 
        $gTotalSalary=0;
        $ntotal = 0;
        $yearlyIncome=0;
        $tax=0;

        if(!empty($staffs)){ 
         $sn=0;
         
  
        foreach ($staffs as $staff) { 
            
            $isPartTime=false;   
            $deductionTotal=0;
            $gtotal = 0; 
            $sn++;
           
            ?>

        <tr>

           
            
            <td><?php echo $sn; ?></td>
            <td><?php echo $staff->staff->fname.' '.$staff->staff->lname; ?></td>
            <td><?php echo '' ?></td>
            <td><?php echo $staff->basic_salary; $grandBasicTotal += $staff->basic_salary; ?></td>
            <td><?php echo $staff->grade; ?></td>
            <td><?php echo $staff->grade_amount; ?></td>
            
 
 <td><?php 

             
             $allowancePf=$staff->allowance_pf;
             $grandPFTotal += $allowancePf;
             echo  $allowancePf; ?></td>
             <?php foreach($allowances as $allowanceDb){ ?>
            <td><?php 

                    $string=$allowanceDb->allowanceName;
                   
                    $allowance=strtolower(str_replace(' ', '_',$string));
                    echo $staff->$allowance;
            ?>
           

        </td>
        <?php } ?>
           

            <td><?php 
            
                echo $staff->insurance_add; 
                $grandInsuranceTotal += $staff->insurance_add; 

                ?></td>
            <td>
            <?php 
            echo $staff->gross_total;
            $grandGrossTotal += $staff->gross_total;
            ?>

            </td>
            

            <td><?php 
           
            echo  $staff->deduction_pf;
            $grandPFDeductionTotal += $staff->deduction_pf;
            ?>
            </td>
            
            <td>
                <?php

                
                echo $staff->deduction_insurance;
                $grandInsuranceDeductionTotal += $staff->deduction_insurance;

                ?>
            </td>


           <?php foreach($deductions as $deductionDb){ ?>
            <td>

            <?php 

                     $string=$deductionDb->name;
                   
                    $deduction=strtolower(str_replace(' ', '_',$string));
                    echo $staff->$deduction;
            ?>

        </td>

           <?php } ?>
            
            <?php 
              $partTimeAmount=0;
             foreach ($plus2Allowances as $key => $pallowance) { 
                  $staffPlus2Model=null;
                  $partDays=null;
                  $partAmount=null;

                
                  $staffPlus2Model=Plus2AllowancesPayroll::model()->findByAttributes(array('payroll_id'=>$staff->id,'allowance_id'=>$pallowance->id));
                  if($staffPlus2Model != null)
                  {
                      $partDays=$staffPlus2Model->days;
                      $partAmount=$staffPlus2Model->amount;
                  }
            ?>

                <td><?= $partDays ?></td>
                <td><?= $partAmount ?></td>
                <td><?= $partAmount*$partDays ?></td>
            <?php 
                if($staffPlus2Model != null)
                {
                    $partTimeAmount += $partDays * $partAmount;
                }

            } 
            ?>
            <td>
            <?php 
            echo $staff->net_salary;
            $grandNetTotal += $staff->net_salary;
            ?>
            </td>
           
            <td><?php echo $staff->yearly_total; $grandYearlyTotal+= $staff->yearly_total; ?></td>
            <td><?php echo $staff->monthly_tax; $grandMonthlyTaxTotal+= $staff->monthly_tax;?></td> 
             <td><?php echo $staff->net_payment; $grandNetPayment+=  $staff->net_payment; ?></td>

        </tr>
        <?php }  } ?>


<?php 
       
        if(!empty($staffs)){ 
        
  
        foreach ($staffs as $staff) { 
           
            
            ?>

        <tr>
         
            <td><?php echo 'Total' ?></td>
            <td><?php echo '';?></td>

            <td><?php '' ?></td>
            <td><?php echo $grandBasicTotal; ?></td>
            <td><?php echo '';?></td>
             <td><?php echo ''; ?></td>
            <td><?php echo $grandPFTotal; ?></td>
             <?php foreach($allowances as $allowance){ ?>
            <td><?php /*echo $all = $model->getAllowances($staff->staff_id, $allowance->allowanceId,  time()) * $salary/100 ; 
            $gtotal += $all;*/
            ?>
            <?php echo ''; 
            
            ?>

            </td>
            <?php } ?>
            <td><?php 
            echo $grandInsuranceTotal; ?></td>  
            <td><?php echo $grandGrossTotal; ?></td>
            <td><?php echo $grandPFDeductionTotal; ?></td>
             <td><?php echo $grandInsuranceDeductionTotal; ?></td>
             
           <?php foreach($deductions as $deduction){ ?>
            <td>
            <?php echo '';
            ?>

            </td>

            <?php } ?>
             <?php foreach ($plus2Allowances as $key => $pallowance) { ?>
                        <td></td>
                        <td></td>
                        <Td></Td>
                      
              <?php } ?>
           
            <td><?php echo $grandNetTotal; ?></td>
             <td><?php echo $grandYearlyTotal; ?></td>
             <td><?php echo $grandMonthlyTaxTotal; ?></td>
           
           <!--  <td><?php //echo $nsalary = $nsalary - $tdeduction; $ntotal += $nsalary; ?></td> -->
             <td><?php echo $grandNetPayment; ?></td>
  
        </tr>
        <?php break; } }?>

           
    </tbody>
</table>

<form action="" method="POST">
<?php


       
        echo CHtml::hiddenfield('printMonth',$month);
        echo CHtml::hiddenfield('printYear',$year);
         echo CHtml::hiddenfield('printSpecial',$special);

        $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'EXPORt TO PDF',
            'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'htmlOptions'=>array('id'=>'submit'),
        )); 



?>
</form>