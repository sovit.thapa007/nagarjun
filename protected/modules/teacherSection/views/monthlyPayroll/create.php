<?php
/* @var $this MonthlyPayrollController */
/* @var $model MonthlyPayroll */

$this->breadcrumbs=array(
	'Monthly Payrolls'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MonthlyPayroll', 'url'=>array('index')),
	array('label'=>'Manage MonthlyPayroll', 'url'=>array('admin')),
);
?>

<h1>Create MonthlyPayroll</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>