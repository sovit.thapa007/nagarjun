<style>
table#payroll tbody tr td{
 border: 1px solid black;
}

table#payroll tr th{
 border: 1px solid black;
}

table#payroll{
 border-collapse: collapse;;
}

</style>

<h4 style="text-align:center">

<?php  

    echo UtilityFunctions::SchoolInformation()->title;
    echo "<br>";
    echo UtilityFunctions::SchoolInformation()->tole;
    echo "<br>";

     ?>

<?php
if(!empty($month) && !empty($year)) 
{ 
    echo "Salary Sheet for Year ".UtilityFunctions::getYear()[$year].' '.UtilityFunctions::getMonth()[$month].' Month';
}
else
{

   echo "Select Month and Year for Salary Sheet"; 
}
?>
    

</h4> <hr>
   
<br>
<?php 


$allowances = Allowances::model()->findAll();
$deductions=Deductions::model()->findAll();
$plus2Allowances=Plus2Allowances::model()->findALL();

$grandBasicTotal=0;
$grandPFTotal=0;
$grandAllowance=0;
$grandInsuranceTotal=0;
$grandGrossTotal=0;
$grandPFDeductionTotal=0;
$grandDeduction=0;
$grandInsuranceDeductionTotal=0;
$grandNetTotal=0;
$grandYearlyTotal=0;
$grandMonthlyTaxTotal=0;
$grandNetPayment=0;

 ?>
<table class="table table-bordered table-striped" id="payroll">
    <thead>
    <thead>
        <tr>
            <th rowspan="2">S.N</th>
            <th rowspan="2">Full Name</th>
            <th rowspan="2">Level</th>
            
           <?php foreach ($plus2Allowances as $key => $pallowance) { ?>
                <th colspan="3"><?= $pallowance->name?></th>
            <?php } ?>
            <th rowspan="2"> 15% Tax</th>         
            <th rowspan="2"> Net payment</th>            
        </tr>

        <tr>
         <?php foreach ($plus2Allowances as $key => $pallowance) { ?>
                    <th>DAYS</th>
                    <th>AMOUNT</th>
                     <th>TOTAL</th>
          <?php } ?>
        </tr>
       
    </thead>
    <tbody>
        <?php 
        $gTotalSalary=0;
        $ntotal = 0;
        $yearlyIncome=0;
         $allowanceTotal=0;
        $tax=0;
        if(!empty($staffs)){ 
            
         $sn=0;
         
  
        foreach ($staffs as $staff) { 
            $allowanceTotal=0;
            $isPartTime=false;   
            $deductionTotal=0;
            $gtotal = 0; 
            $sn++;
           
            ?>

        <tr>

           
        
            <td><?php echo $sn; ?></td>
            <td><?php echo $staff->staff->fname.' '.$staff->staff->lname; ?></td>
            <td><?php echo '' ?></td>

             <?php 
              $partTimeAmount=0;
             foreach ($plus2Allowances as $key => $pallowance) { 
                  $staffPlus2Model=null;
                  $partDays=null;
                  $partAmount=null;

                
                  $staffPlus2Model=Plus2AllowancesPayroll::model()->findByAttributes(array('payroll_id'=>$staff->id,'allowance_id'=>$pallowance->id));
                  if($staffPlus2Model != null)
                  {
                      $partDays=$staffPlus2Model->days;
                      $partAmount=$staffPlus2Model->amount;
                  }
            ?>

                <td><?= $partDays ?></td>
                <td><?= $partAmount ?></td>
                <td><?= $partAmount*$partDays ?></td>
            <?php 
                if($staffPlus2Model != null)
                {
                    $partTimeAmount += $partDays * $partAmount;
                }

            } 
            ?>
           
            
 
 <!-- <td><?php 
             $allowancePf=$staff->allowance_pf;
             $grandPFTotal += $allowancePf;
             echo  $allowancePf; ?>
               
      </td> -->
             <td><?php echo $staff->monthly_tax; $grandMonthlyTaxTotal+= $staff->monthly_tax; ?></td>  
             <td><?php echo $staff->net_payment; $grandNetPayment += $staff->net_payment;?></td>

        </tr>
        <?php }  } ?>
<?php 
       
        if(!empty($staffs)){ 
        
  
        foreach ($staffs as $staff) { 
           
            
            ?>

        <tr>
         
            <td><?php echo 'Total' ?></td>
            <td><?php echo '';?></td>

            <td><?php '' ?></td>

            
             <?php foreach ($plus2Allowances as $key => $pallowance) { ?>
                        <td></td>
                        <td></td>
                        <td></td>
              <?php } ?>
           
           
        
            
            <td><?php echo $grandMonthlyTaxTotal; ?></td>
           
           <!--  <td><?php //echo $nsalary = $nsalary - $tdeduction; $ntotal += $nsalary; ?></td> -->
             <td><?php echo $grandNetPayment; ?></td>
  
        </tr>
        <?php break; } }?>



           
    </tbody>
</table>