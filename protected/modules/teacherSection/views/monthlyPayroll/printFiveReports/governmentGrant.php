<style>
table#payroll tbody tr td{
 border: 1px solid black;
}

table#payroll tr th{
 border: 1px solid black;
}

table#payroll{
 border-collapse: collapse;;
}

</style>
<h4 style="text-align:center">

<?php  

    echo UtilityFunctions::SchoolInformation()->title;
    echo "<br>";
    echo UtilityFunctions::SchoolInformation()->tole;
    echo "<br>";

     ?>

<?php
if(!empty($month) && !empty($year)) 
{ 
    echo "Salary Sheet for Year ".UtilityFunctions::getYear()[$year].' '.UtilityFunctions::getMonth()[$month].' Month';
     echo "<br>".strtoupper($special);
}
else
{

   echo "Select Month and Year for Salary Sheet"; 
}
?>
    

</h4> <hr>
<br>
<?php 


$allowances = Allowances::model()->findAll();
$deductions=Deductions::model()->findAll();


$grandBasicTotal=0;
$grandPFTotal=0;
$grandAllowance=0;
$grandInsuranceTotal=0;
$grandGrossTotal=0;
$grandPFDeductionTotal=0;
$grandDeduction=0;
$grandInsuranceDeductionTotal=0;
$grandNetTotal=0;
$grandYearlyTotal=0;
$grandMonthlyTaxTotal=0;
$grandNetPayment=0;
$grandGradeAmount=0;
$grandTotalSalary=0;

 ?>
<table class="table table-bordered table-striped" id="payroll">
    <thead>
    <thead>
        <tr>
            <th rowspan="3">S.N</th>
            <th rowspan="3">Full Name</th>
            <th rowspan="3">Level</th>
            <th colspan="5">Salary</th>
            <th rowspan="3"> PF/C(<?php echo $model->getPf(time()); ?>%) </th>
            <th rowspan="3">Allowances</th>
             <?php foreach($report_allowances as $value){ ?>
                <th rowspan="3"><?php echo $value->allowance->allowanceName; ?></th>
            <?php } ?>
             <th rowspan="3">Insurance</th>
            <th rowspan="3">Gross Total</th>
             

            <th rowspan="3"> PF/C(<?php echo $model->getPf(time())*2; ?>%) </th>
            <th rowspan="3"> Insurance</th>
            <th rowspan="3">Deductions</th>
             <?php foreach($report_deductions as $value){ ?>
                <th rowspan="3"><?php echo $value->deduction->name; ?></th>
            <?php } ?>

           
            <th rowspan="3"> N.Total</th>    
            <th rowspan="3"> Yearly Total</th>    
            <th rowspan="3"> Monthly Tax</th>         
            <th rowspan="3"> Net payment</th>            
        </tr>
        <tr>
            <th> Basic </th>
            <th> Grade </th>
            <th> Grade Amount</th>
            <th> Total Grade</th>
            <th> Total Salary</th>
            
        </tr>
    </thead>

    <tbody>
    <tr><td></td></tr>
        <?php 
        $gTotalSalary=0;
        $ntotal = 0;
        $yearlyIncome=0;
         $allowanceTotal=0;
        $tax=0;
 
        if(!empty($staffs)){ 
         $sn=0;
         
  
        foreach ($staffs as $staff) { 
             $allowanceTotal=0;
            $isPartTime=false;   
            $deductionTotal=0;
            $gtotal = 0; 
            $sn++;
           
            ?>

        <tr>

           
        
            <td><?php echo $sn; ?></td>
            <td><?php echo $staff->staff->fname.' '.$staff->staff->lname; ?></td>
            <td><?php echo '' ?></td>
            <td><?php echo $staff->basic_salary; $grandBasicTotal+= $staff->basic_salary;?></td>
            <td><?php echo $staff->grade; ?></td>
            <td><?php echo $staff->grade_amount; ?></td>
            <td><?php echo $staff->grade_amount*$staff->grade; $grandGradeAmount+= $staff->grade_amount*$staff->grade;?></td>
            <td><?php echo ($staff->grade_amount*$staff->grade)+$staff->basic_salary; $grandTotalSalary+=($staff->grade_amount*$staff->grade)+$staff->basic_salary ?></td>
            
 
 <td><?php 
             $allowancePf=$staff->allowance_pf;
             $grandPFTotal += $allowancePf;
             echo  $allowancePf; ?>
               
      </td>
             <?php foreach($allowances as $allowanceDb){ ?>
            <?php 
                    if($allowanceDb->checkForReport($staff->staff_id,'government'))
                    {
                        $string=$allowanceDb->allowanceName;
                       
                        $allowance=strtolower(str_replace(' ', '_',$string));
                        $allowanceTotal+= $staff->$allowance;
                    }
            ?>
           

       
        <?php } ?>
           

            <td><?php 
            
                echo $allowanceTotal; 
                $grandAllowance+= $allowanceTotal;
                ?>
                  
                </td>
           

            <?php foreach($report_allowances as $value){ ?>
                <td><?php 

                $string=$value->allowance->allowanceName;     
                $allowance=strtolower(str_replace(' ', '_',$string));
                //$allowanceTotal+= $staff->$allowance;
                echo $staff->$allowance; 
                ?></td>
            <?php } ?>


            <td><?php 
            
                echo $staff->insurance_add; 
                $grandInsuranceTotal += $staff->insurance_add; 
                ?></td>
            <td>
            <?php 
            echo $staff->gross_total;
            $grandGrossTotal += $staff->gross_total;

            ?>

            </td>
            

            <td><?php 
           
            echo  $staff->deduction_pf;
            $grandPFDeductionTotal += $staff->deduction_pf;
            ?>
            </td>
            
            <td>
                <?php

                
                echo $staff->deduction_insurance;
                $grandInsuranceDeductionTotal += $staff->deduction_insurance;

                ?>
            </td>


          <?php foreach($deductions as $deductionDb){ ?>
            <?php 
                    if($deductionDb->checkForReport($staff->staff_id,'government'))
                    {
                        $string=$deductionDb->name;
                       
                        $deduction=strtolower(str_replace(' ', '_',$string));
                        $deductionTotal+= $staff->$deduction;
                    }
            ?>
           

       
        <?php } ?>
           

            <td><?php 
            
                echo $deductionTotal; 
                $grandDeduction+= $deductionTotal;
                ?>
                  
                </td>
           

            <?php foreach($report_deductions as $value){ ?>
                <td><?php 

                $string=$value->deduction->name;     
                $deduction=strtolower(str_replace(' ', '_',$string));
                //$allowanceTotal+= $staff->$allowance;
                echo $staff->$deduction; 
                ?></td>
            <?php } ?>

          
            <td>
            <?php 
            echo $staff->net_salary;
            $grandNetTotal += $staff->net_salary;
            ?>
            </td>
           
            <td><?php echo $staff->yearly_total; $grandYearlyTotal+= $staff->yearly_total;?></td>
            <td><?php echo $staff->monthly_tax; $grandMonthlyTaxTotal+= $staff->monthly_tax; ?></td> 
             <td><?php echo $staff->net_payment; $grandNetPayment += $staff->net_payment;?></td>

        </tr>
        <?php }  } ?>
<?php 
       
        if(!empty($staffs)){ 
        
  
        foreach ($staffs as $staff) { 
           
            
            ?>

        <tr>
         
            <td><?php echo 'Total' ?></td>
            <td><?php echo '';?></td>

            <td><?php '' ?></td>
            <td><?php echo $grandBasicTotal; ?></td>
            <td><?php echo '';?></td>
             <td><?php echo ''; ?></td>
              <td><?php echo $grandGradeAmount ?></td>
              <td><?php echo $grandTotalSalary ?></td>
            <td><?php echo $grandPFTotal; ?></td>
            <td> <?php echo $grandAllowance; ?></td>
              <?php foreach($report_allowances as $value){ ?>
                <td><?php 

            
                echo ''; 
                ?></td>
            <?php } ?>
            <td><?php 
            echo $grandInsuranceTotal; ?></td>
            <td><?php echo $grandGrossTotal; ?></td>
            <td><?php echo $grandPFDeductionTotal; ?></td>
             <td><?php echo $grandInsuranceDeductionTotal; ?></td>
              <td><?php echo $grandDeduction; ?></td>

           <?php foreach($report_deductions as $value){ ?>
                <td><?php 

            
                echo ''; 
                ?></td>
            <?php } ?>
           
            <td><?php echo $grandNetTotal; ?></td>
             <td><?php echo $grandYearlyTotal; ?></td>
             <td><?php echo $grandMonthlyTaxTotal; ?></td>
           
           <!--  <td><?php //echo $nsalary = $nsalary - $tdeduction; $ntotal += $nsalary; ?></td> -->
             <td><?php echo $grandNetPayment; ?></td>
  
        </tr>
        <?php break; } }?>



           
    </tbody>
</table>
