<style>table#payroll tbody tr td{
  text-align: right;
}

table#payroll tbody tr td:nth-child(1), table#payroll tbody tr td:nth-child(2), table#payroll tbody tr td:nth-child(3), table#payroll tbody tr td:nth-child(5){
  text-align: left;
}

table#payroll{
  width: 100%;
  display: block;
  
  overflow-y: scroll;
  overflow-x: scroll;
  
}

table#payroll thead tr th{
  text-align: center;
}
</style>

<h4>
<?php
      if(!empty($month) && !empty($year)) 
      { 
          echo strtoupper($special)." Salary Ledger of Year ".UtilityFunctions::getYear()[$year].' '.UtilityFunctions::getMonth()[$month].' Month';
      }
      else
      {
         echo "Select Month and Year for Salary Ledger"; 
      }

?>
    

</h4> <hr>
    <form action="" method="POST">

<?php echo CHtml::dropDownList('month','', UtilityFunctions::getMonth()); ?>

<?php echo CHtml::dropDownList('year', '',  UtilityFunctions::getYear()); ?>

 
       


<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'Search Monthly Salary',
    'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'small', // null, 'large', 'small' or 'mini'
    'htmlOptions'=>array('id'=>'special-submit'),
)); ?> 
</form>
<br>
<?php 


$allowances = Allowances::model()->findAll();
$deductions=Deductions::model()->findAll();


$grandBasicTotal=0;
$grandPFTotal=0;
$grandAllowance=0;
$grandInsuranceTotal=0;
$grandGrossTotal=0;
$grandPFDeductionTotal=0;
$grandDeduction=0;
$grandInsuranceDeductionTotal=0;
$grandNetTotal=0;
$grandYearlyTotal=0;
$grandMonthlyTaxTotal=0;
$grandNetPayment=0;

 ?>
<table class="table table-bordered table-striped" id="payroll">
    <thead>
    <thead>
        <tr>
            <th rowspan="3">S.N</th>
            <th rowspan="3">Full Name</th>
            <th rowspan="3">Level</th>
            <th rowspan="3">Salary</th>
            
            <th rowspan="3">Allowances</th>
             <?php foreach($report_allowances as $value){ ?>
                <th rowspan="3"><?php echo $value->allowance->allowanceName; ?></th>
            <?php } ?>
            
            <th rowspan="3">Gross Total</th>
             

           
            
            <th rowspan="3">Deductions</th>
             <?php foreach($report_deductions as $value){ ?>
                <th rowspan="3"><?php echo $value->deduction->name; ?></th>
            <?php } ?>

           
            <th rowspan="3"> N.Total</th>    
            <th rowspan="3"> Yearly Total</th>    
            <th rowspan="3"> Monthly Tax (1% Tax)</th>         
            <th rowspan="3"> Net payment</th>            
        </tr>
       
    </thead>
    <tbody>
        <?php 
        $gTotalSalary=0;
        $ntotal = 0;
        $yearlyIncome=0;
        $allowanceTotal=0;
        $tax=0;

        if(!empty($staffs)){ 
         $sn=0;
         
  
        foreach ($staffs as $staff) { 
             $allowanceTotal=0;
            $isPartTime=false;   
            $deductionTotal=0;
            $gtotal = 0; 
            $sn++;
           
            ?>
        <tr>

           
        
        <td><?php echo $sn; ?></td>
        <td><?php echo $staff->staff->fname.' '.$staff->staff->lname; ?></td>
        <td><?php echo '' ?></td>
        <td><?php echo $staff->basic_salary; $grandBasicTotal+= $staff->basic_salary;?></td>
           
            
 
 <!-- <td><?php 
             $allowancePf=$staff->allowance_pf;
             $grandPFTotal += $allowancePf;
             echo  $allowancePf; ?>
               
      </td> -->
             <?php foreach($allowances as $allowanceDb){ ?>
            <?php 
                    if($allowanceDb->checkForReport($staff->staff_id,'annex'))
                    {
                        $string=$allowanceDb->allowanceName;
                        $allowance=strtolower(str_replace(' ', '_',$string));
                        $allowanceTotal+= $staff->$allowance;
                    }
            ?>
           

       
        <?php } ?>
           

            <td><?php 
            
                echo $allowanceTotal; 
                $grandAllowance+= $allowanceTotal;
                ?>
                  
                </td>
           

            <?php foreach($report_allowances as $value){ ?>
                <td><?php 

                $string=$value->allowance->allowanceName;     
                $allowance=strtolower(str_replace(' ', '_',$string));
                //$allowanceTotal+= $staff->$allowance;
                echo $staff->$allowance; 
                ?></td>
            <?php } ?>


           
            <td>
            <?php 
            echo $staff->gross_total;
            $grandGrossTotal += $staff->gross_total;

            ?>

            </td>
            

           


          <?php foreach($deductions as $deductionDb){ ?>
            <?php 
                    if($deductionDb->checkForReport($staff->staff_id,'annex'))
                    {
                        $string=$deductionDb->name;
                       
                        $deduction=strtolower(str_replace(' ', '_',$string));
                        $deductionTotal+= $staff->$deduction;
                    }
            ?>
           

       
        <?php } ?>
           

            <td><?php 
            
                echo $deductionTotal; 
                $grandDeduction+= $deductionTotal;
                ?>
                  
                </td>
           

            <?php foreach($report_deductions as $value){ ?>
                <td><?php 

                $string=$value->deduction->name;     
                $deduction=strtolower(str_replace(' ', '_',$string));
                //$allowanceTotal+= $staff->$allowance;
                echo $staff->$deduction; 
                ?></td>
            <?php } ?>

          
            <td>
            <?php 
            echo $staff->net_salary;
            $grandNetTotal += $staff->net_salary;
            ?>
            </td>
           
            <td><?php echo $staff->yearly_total; $grandYearlyTotal+= $staff->yearly_total;?></td>
            <td><?php echo $staff->monthly_tax; $grandMonthlyTaxTotal+= $staff->monthly_tax; ?></td> 
             <td><?php echo $staff->net_payment; $grandNetPayment += $staff->net_payment;?></td>

        </tr>
        <?php }  } ?>
<?php 
       
        if(!empty($staffs)){ 
        
  
        foreach ($staffs as $staff) { 
           
            
            ?>

        <tr>
         
            <td><?php echo 'Total' ?></td>
            <td><?php echo '';?></td>

            <td><?php '' ?></td>
            <td><?php echo $grandBasicTotal; ?></td>
            
            <td> <?php echo $grandAllowance; ?></td>
              <?php foreach($report_allowances as $value){ ?>
                <td><?php 

            
                echo ''; 
                ?></td>
            <?php } ?>
            
            <td><?php echo $grandGrossTotal; ?></td>
           
              <td><?php echo $grandDeduction; ?></td>

           <?php foreach($report_deductions as $value){ ?>
                <td><?php 

            
                echo ''; 
                ?></td>
            <?php } ?>
           
            <td><?php echo $grandNetTotal; ?></td>
             <td><?php echo $grandYearlyTotal; ?></td>
             <td><?php echo $grandMonthlyTaxTotal; ?></td>
           
           <!--  <td><?php //echo $nsalary = $nsalary - $tdeduction; $ntotal += $nsalary; ?></td> -->
             <td><?php echo $grandNetPayment; ?></td>
  
        </tr>
        <?php break; } }?>



           
    </tbody>
</table>

<form action="" method="POST">
<?php


       
        echo CHtml::hiddenfield('printMonth',$month);
        echo CHtml::hiddenfield('printYear',$year);
         echo CHtml::hiddenfield('printSpecial',$special);

        $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'EXPORT TO PDF',
            'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'htmlOptions'=>array('id'=>'submit','target'=>'_blank'),
        )); 



?>
</form>
