<style>table#payroll tbody tr td{
  text-align: right;
}

table#payroll tbody tr td:nth-child(1), table#payroll tbody tr td:nth-child(2), table#payroll tbody tr td:nth-child(3), table#payroll tbody tr td:nth-child(5){
  text-align: left;
}

table#payroll{
  width: 100%;
  display: block;
  
  overflow-y: scroll;
  overflow-x: scroll;
  
}

table#payroll thead tr th{
  text-align: center;
}
</style>

<h4>
<?php

  
      if(!empty($month) && !empty($year)) 
      { 
          echo strtoupper($special)." Salary Ledger of Year ".UtilityFunctions::getYear()[$year].' '.UtilityFunctions::getMonth()[$month].' Month';
      }
      else
      {
         echo "Select Month and Year for Salary Ledger"; 
      }

?>
    

</h4> <hr>
    <form action="" method="POST">

<?php echo CHtml::dropDownList('month','', UtilityFunctions::getMonth()); ?>

<?php echo CHtml::dropDownList('year', '',  UtilityFunctions::getYear()); ?>

 
       


<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'Search Monthly Salary',
    'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'small', // null, 'large', 'small' or 'mini'
    'htmlOptions'=>array('id'=>'special-submit'),
)); ?> 
</form>
<br>
<?php 


$allowances = Allowances::model()->findAll();
$deductions=Deductions::model()->findAll();


$grandBasicTotal=0;
$grandPFTotal=0;
$grandAllowance=0;
$grandInsuranceTotal=0;
$grandGrossTotal=0;
$grandPFDeductionTotal=0;
$grandDeduction=0;
$grandInsuranceDeductionTotal=0;
$grandNetTotal=0;
$grandYearlyTotal=0;
$grandMonthlyTaxTotal=0;
$grandNetPayment=0;
$plus2Allowances=Plus2Allowances::model()->findALL();
 ?>
<table class="table table-bordered table-striped" id="payroll">
    <thead>
    <thead>
        <tr>
            <th rowspan="2">S.N</th>
            <th rowspan="2">Full Name</th>
            <th rowspan="2">Level</th>
            
           <?php foreach ($plus2Allowances as $key => $pallowance) { ?>
                <th colspan="3"><?= $pallowance->name?></th>
            <?php } ?>
            <th rowspan="2"> 15% Tax</th>         
            <th rowspan="2"> Net payment</th>            
        </tr>

        <tr>
         <?php foreach ($plus2Allowances as $key => $pallowance) { ?>
                    <th style="background-color: #000055; color:white;">DAYS</th>
                    <th style="background-color:  #000055; color:white;">AMOUNT</th>
                     <th style="background-color:  #000055; color:white;">TOTAL</th>
          <?php } ?>
        </tr>
       
    </thead>
    <tbody>
    
        <?php 
        $gTotalSalary=0;
        $ntotal = 0;
        $yearlyIncome=0;
         $allowanceTotal=0;
        $tax=0;

        if(!empty($staffs)){ 
         $sn=0;
         
  
        foreach ($staffs as $staff) { 
            $allowanceTotal=0;
            $isPartTime=false;   
            $deductionTotal=0;
            $gtotal = 0; 
            $sn++;
           
            ?>

        <tr>

           
        
            <td><?php echo $sn; ?></td>
            <td><?php echo $staff->staff->fname.' '.$staff->staff->lname; ?></td>
            <td><?php echo '' ?></td>

             <?php 
              $partTimeAmount=0;
             foreach ($plus2Allowances as $key => $pallowance) { 
                  $staffPlus2Model=null;
                  $partDays=null;
                  $partAmount=null;

                
                  $staffPlus2Model=Plus2AllowancesPayroll::model()->findByAttributes(array('payroll_id'=>$staff->id,'allowance_id'=>$pallowance->id));
                  if($staffPlus2Model != null)
                  {
                      $partDays=$staffPlus2Model->days;
                      $partAmount=$staffPlus2Model->amount;
                  }
            ?>

                <td><?= $partDays ?></td>
                <td><?= $partAmount ?></td>
                <td><?= $partAmount*$partDays ?></td>
            <?php 
                if($staffPlus2Model != null)
                {
                    $partTimeAmount += $partDays * $partAmount;
                }

            } 
            ?>
           
            
 
 <!-- <td><?php 
             $allowancePf=$staff->allowance_pf;
             $grandPFTotal += $allowancePf;
             echo  $allowancePf; ?>
               
      </td> -->
             <td><?php echo $staff->monthly_tax; $grandMonthlyTaxTotal+= $staff->monthly_tax; ?></td>  
             <td><?php echo $staff->net_payment; $grandNetPayment += $staff->net_payment;?></td>

        </tr>
        <?php }  } ?>
<?php 
       
        if(!empty($staffs)){ 
        
  
        foreach ($staffs as $staff) { 
           
            
            ?>

        <tr>
         
            <td><?php echo 'Total' ?></td>
            <td><?php echo '';?></td>

            <td><?php '' ?></td>

            
             <?php foreach ($plus2Allowances as $key => $pallowance) { ?>
                        <td></td>
                        <td></td>
                        <td></td>
              <?php } ?>
           
           
        
            
            <td><?php echo $grandMonthlyTaxTotal; ?></td>
           
           <!--  <td><?php //echo $nsalary = $nsalary - $tdeduction; $ntotal += $nsalary; ?></td> -->
             <td><?php echo $grandNetPayment; ?></td>
  
        </tr>
        <?php break; } }?>



           
    </tbody>
</table>

<form action="" method="POST">
<?php


       
        echo CHtml::hiddenfield('printMonth',$month);
        echo CHtml::hiddenfield('printYear',$year);
         echo CHtml::hiddenfield('printSpecial',$special);

        $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'EXPORT TO PDF',
            'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'htmlOptions'=>array('id'=>'submit'),
        )); 



?>
</form>
