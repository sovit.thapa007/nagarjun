<style>table#payroll tbody tr td{
  text-align: right;
}

table#payroll tbody tr td:nth-child(1), table#payroll tbody tr td:nth-child(2), table#payroll tbody tr td:nth-child(3), table#payroll tbody tr td:nth-child(5){
  text-align: left;
}

table#payroll{
  width: 100%;
  display: block;
  
  overflow-y: scroll;
  overflow-x: scroll;
  
}

table#payroll thead tr th{
  text-align: center;
}
</style>

<h4>
<?php
$displayYear= $year =='' ?'-':UtilityFunctions::getYear()[$year];
$displayMonth= $month =='' ?'-':UtilityFunctions::getMonth()[$month];



echo "Individual Salary Sheet of ". $individualStaff->getFullName();
echo "<br>";




?>
    

</h4> <hr>
    <form action="" method="POST">

<?php echo CHtml::dropDownList('month','', UtilityFunctions::getMonth()); ?>

<?php echo CHtml::dropDownList('year', '',  UtilityFunctions::getYear()); ?>

 
       


<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'Search Monthly Salary',
    'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'small', // null, 'large', 'small' or 'mini'
    'htmlOptions'=>array('id'=>'special-submit'),
)); ?> 
</form>
<br>
<?php
echo "Year:".$displayYear;
echo "<br>";
echo "Month:".$displayMonth;
?>

<?php 


$allowances = Allowances::model()->findAll();
$deductions=Deductions::model()->findAll();


$grandBasicTotal=0;
$grandPFTotal=0;
$grandAllowance=0;
$grandInsuranceTotal=0;
$grandGrossTotal=0;
$grandPFDeductionTotal=0;
$grandDeduction=0;
$grandInsuranceDeductionTotal=0;
$grandNetTotal=0;
$grandYearlyTotal=0;
$grandMonthlyTaxTotal=0;
$grandNetPayment=0;

 ?>
<table class="table table-bordered table-striped" id="payroll">
    <thead>
    <thead>
        <tr>
            <th rowspan="3">S.N</th>
            <th rowspan="3">Month</th>
            <th rowspan="3">Department</th>
            <th colspan="3">Salary</th>
            <th rowspan="3"> PF/C(<?php echo $model->getPf(time()); ?>%) </th>
            <th rowspan="3">Allowances</th>
             <th rowspan="3">Insurance</th>
            <th rowspan="3">Gross Total</th>
             <th rowspan="3">PF/C(<?php echo $model->getPf(time())*2; ?>%) </th>
              <th rowspan="3">Insurance</th>

            <th rowspan="3">Deductions</th>
            <th rowspan="3"> N.Total</th>    
            <th rowspan="3"> Yearly Total</th>    
            <th rowspan="3"> Monthly Tax</th>         
            <th rowspan="3"> Net payment</th>            
        </tr>
        <tr>
            <th style="background-color: #000055"> Basic </th>
            <th style="background-color: #000055"> Grade </th>
            <th style="background-color: #000055"> Grade Amount</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $gTotalSalary=0;
        $ntotal = 0;
        $yearlyIncome=0;
         $allowanceTotal=0;
        $tax=0;

        if(!empty($staffs)){ 
         $sn=0;
         foreach($staffs as $staff)
         {
  
    
             $allowanceTotal=0;
            $isPartTime=false;   
            $deductionTotal=0;
            $gtotal = 0; 
            $sn++;
           
            ?>

        <tr>

           
        
            <td><?php echo $sn; ?></td>
            <td><?php echo UtilityFunctions::getMonth()[$staff->month]; ?></td>
            <td><?php echo $staff->specialcase->name ?></td>
            <td><?php echo $staff->basic_salary; $grandBasicTotal+= $staff->basic_salary;?></td>
            <td><?php echo $staff->grade; ?></td>
            <td><?php echo $staff->grade_amount; ?></td>
            
 
 <td><?php 

             
             $allowancePf=$staff->allowance_pf;
             $grandPFTotal += $allowancePf;
             echo  $allowancePf; ?></td>
             <?php foreach($allowances as $allowanceDb){ ?>
            <?php 

                    $string=$allowanceDb->allowanceName;
                   
                    $allowance=strtolower(str_replace(' ', '_',$string));
                    $allowanceTotal+= $staff->$allowance;
            ?>
           

       
        <?php } ?>
           

            <td><?php 
            
                echo $allowanceTotal; 
                $grandAllowance+= $allowanceTotal;
                ?></td>
           
            <td><?php 
            
                echo $staff->insurance_add; 
                $grandInsuranceTotal += $staff->insurance_add; 
                ?></td>
            <td>
            <?php 
            echo $staff->gross_total;
            $grandGrossTotal += $staff->gross_total;

            ?>

            </td>
            

            <td><?php 
           
            echo  $staff->deduction_pf;
            $grandPFDeductionTotal += $staff->deduction_pf;
            ?>
            </td>
            
            <td>
                <?php

                
                echo $staff->deduction_insurance;
                $grandInsuranceDeductionTotal += $staff->deduction_insurance;

                ?>
            </td>


           <?php foreach($deductions as $deductionDb){ ?>
           

            <?php 

                     $string=$deductionDb->name;
                   
                    $deduction=strtolower(str_replace(' ', '_',$string));
                   $deductionTotal+= $staff->$deduction;
            ?>

        

           <?php } ?>
           <td><?php echo $deductionTotal; 

           $grandDeduction +=  $deductionTotal; 
           ?></td>
            <td>
            <?php 
            echo $staff->net_salary;
            $grandNetTotal += $staff->net_salary;
            ?>
            </td>
           
            <td><?php echo $staff->yearly_total; $grandYearlyTotal+= $staff->yearly_total;?></td>
            <td><?php echo $staff->monthly_tax; $grandMonthlyTaxTotal+= $staff->monthly_tax; ?></td> 
             <td><?php echo $staff->net_payment; $grandNetPayment += $staff->net_payment;?></td>

        </tr>
        <?php }  } ?>


<?php 
       
        if(!empty($staffs)){ 
        
  
        foreach ($staffs as $staff) { 
           
            
            ?>

        <tr>
         
            <td><?php echo 'Total' ?></td>
            <td><?php echo '';?></td>

            <td><?php '' ?></td>
            <td><?php echo $grandBasicTotal; ?></td>
            <td><?php echo '';?></td>
             <td><?php echo ''; ?></td>
            <td><?php echo $grandPFTotal; ?></td>
            <td> <?php echo $grandAllowance; ?></td>
            <td><?php 
            echo $grandInsuranceTotal; ?></td>
            <td><?php echo $grandGrossTotal; ?></td>
            <td><?php echo $grandPFDeductionTotal; ?></td>
             <td><?php echo $grandInsuranceDeductionTotal; ?></td>
               <td><?php echo $grandDeduction; ?></td>
          
           
            <td><?php echo $grandNetTotal; ?></td>
             <td><?php echo $grandYearlyTotal; ?></td>
             <td><?php echo $grandMonthlyTaxTotal; ?></td>
           
           <!--  <td><?php //echo $nsalary = $nsalary - $tdeduction; $ntotal += $nsalary; ?></td> -->
             <td><?php echo $grandNetPayment; ?></td>
  
        </tr>
        <?php break; } }?>





           
    </tbody>
</table>

