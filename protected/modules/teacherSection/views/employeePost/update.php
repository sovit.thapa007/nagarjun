<?php
$this->breadcrumbs=array(
	'Employee Posts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List EmployeePost','url'=>array('index')),
array('label'=>'Create EmployeePost','url'=>array('create')),
array('label'=>'View EmployeePost','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage EmployeePost','url'=>array('admin')),
);
?>

<h1>Update EmployeePost <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>