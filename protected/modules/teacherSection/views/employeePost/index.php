<?php
$this->breadcrumbs=array(
	'Employee Posts',
);

$this->menu=array(
array('label'=>'Create EmployeePost','url'=>array('create')),
array('label'=>'Manage EmployeePost','url'=>array('admin')),
);
?>

<h1>Employee Posts</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
