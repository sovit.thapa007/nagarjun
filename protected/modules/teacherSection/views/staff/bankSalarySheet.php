<style>table#payroll tbody tr td{
  text-align: right;
}

table#payroll tbody tr td:nth-child(1), table#payroll tbody tr td:nth-child(2), table#payroll tbody tr td:nth-child(3), table#payroll tbody tr td:nth-child(5){
  text-align: left;
}

table#payroll{
  width: 100%;
  display: block;
  
  overflow-y: scroll;
  overflow-x: scroll;
  
}

table#payroll thead tr th{
  text-align: center;
}
</style>

<h4>
    <?php 
    
    if(!empty($special)) echo StaffSpecialCase::model()->findByPk($special)->name," Ledger Sheet"; else echo "Special Case Wise Salary Sheet"; ?></h4> <hr>

<form action="" method="POST">
<?php echo CHtml::dropDownList('special', '', CHtml::listData(StaffSpecialCase::model()->findAll(), 'id', 'name'), array('prompt'=>'--Select Special Case--')); ?>&nbsp;
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'Submit',
    'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'small', // null, 'large', 'small' or 'mini'
    'htmlOptions'=>array('id'=>'special-submit'),
)); ?> 
</form>
<br>
<?php 
$allowancePf=0;
$deductionPf=0;
$gtotal=0;
$nTotalSalary=0;
$nTotalPayment=0;
$nsalary=0;
$deductionTotal=0;
$nPayment=0;

$allowances = Allowances::model()->findAll();
$deductions=Deductions::model()->findAll();

 ?>
<table class="table table-bordered table-striped" id="payroll">
    <thead>
       
<tr>
            <th rowspan="3">S.N</th>
            <th rowspan="3">Full Name</th>
             <th rowspan="3"> Bank Name</th> 
            <th rowspan="3"> Bank A/C</th> 
            <th rowspan="3"> Net payment</th>    
             <th rowspan="3"> Remarks</th>           
        </tr>
    </thead>
    <tbody>
        <?php 
        $gTotalSalary=0;
        $ntotal = 0;
        $yearlyIncome=0;
        $tax=0;

        if(!empty($staffs)){ 
         $sn=0;
         
  
        foreach ($staffs as $staff) { 
            
            $isPartTime=false;   
            $deductionTotal=0;
            $gtotal = 0; 
            $sn++;
           
            ?>

        <tr>

           
        <?php $fullName=$staff->fname." ".$staff->lname; ?>
            <td><?php echo $sn; ?></td>
            <td><?php echo $fullName; ?></td>
            <td><?php echo $model->getBankName($staff->staff_id); ?></td>
            <td><?php echo $model->getBankAc($staff->staff_id); ?></td>

 <?php 
 $salary = $staff->basic_salary;
 $grade = $staff->grade;
$gradeAmount = $staff->grade_amount; 

             
             $allowancePf=$model->isPermanentGovernment($staff->staff_id)?($model->getPf(time())/100) * $staff->basic_salary:0; 
            ?>
             <?php foreach($allowances as $allowance){ ?>
          
            <?php $all = $model->getAllowanceNumber($staff->staff_id, $allowance->allowanceId); 
            $gtotal += $all;
            ?>

        
            <?php }$gtotal+=$allowancePf; ?>

            <?php 
            $insurance=$model->isPermanentGovernment($staff->staff_id)?200:0?>
            <?php 
            $gsalary = $insurance + $gtotal + $salary + ($grade * $gradeAmount); 
            $gTotalSalary+=$gsalary;
            ?>

           <?php 
            $deductionPf=$model->isPermanentGovernment($staff->staff_id)?(2*($model->getPf(time())/100)) * $staff->basic_salary:0;
           ?>
           
                <?php

                
                 $deductInsurance=$model->isPermanentGovernment($staff->staff_id)?200*2:0;

                ?>
            


           <?php foreach($deductions as $deduction){ ?>
            
            <?php 

           $deduction = $model->getDeductionNumber($staff->staff_id, $deduction->id); 

            $deductionTotal += $deduction;
            ?>


            <?php }  $deductionTotal += $deductionPf + $deductInsurance;?>
            
            <?php 
             $nsalary = $gsalary - $deductionTotal ; $nTotalSalary+=$nsalary;
            ?>
            
            <?php $yearlyIncome=$nsalary*12 +($allowancePf*12); ?>
            <?php 
            $taxModel=PayrollTaxSlack::model()->findByPk(1);

            if($gsalary*12 < $taxModel->first_tax_amount)
            {

                 $firstTaxSlack=($taxModel->first_tax_percent/100) *($gsalary*12);
                  $tax=$firstTaxSlack;
               
            }

            else
            {
                $firstTaxSlack=($taxModel->first_tax_percent/100) *$taxModel->first_tax_amount;
            }
 
            if($yearlyIncome > $taxModel->first_tax_amount && $yearlyIncome < $taxModel->second_tax_amount)
            {
                //$firstTaxSlack=(1/100) *300000;
                $secondTaxSlack=($taxModel->second_tax_percent/100)* ($yearlyIncome - $taxModel->first_tax_amount);
                $tax=$firstTaxSlack + $secondTaxSlack;
            }

            elseif($yearlyIncome > $taxModel->third_tax_amount)
            {
                //$firstTaxSlack=(1/100) *300000;
                $secondTaxSlack=($taxModel->second_tax_percent/100)* ($taxModel->second_tax_amount - $taxModel->first_tax_amount);
                $thirdTaxSlack=($taxModel->third_tax_percent/100)* ($yearlyIncome-$taxModel->second_tax_amount);
                $tax=$firstTaxSlack +$secondTaxSlack +$thirdTaxSlack;
            }
             $tax/12; ?></td>
          
             <td><?php echo $nPayment=$nsalary-($tax/12); $nTotalPayment+=$nPayment?></td>

        </tr>
        <?php }  }?>




          <?php 
       
        if(!empty($staffs)){ 
        
  
        foreach ($staffs as $staff) { 
           
            
            ?>

        <tr>
         
            <td><?php echo 'Total' ?></td>
            <td><?php echo '';?></td>

            <td><?php echo ''; ?></td>
            
            <td><?php 
            
            echo ''; ?></td>
          
             <td><?php echo $nTotalPayment; ?></td>
  
        </tr>
        <?php break; } }?>
           
    </tbody>
</table>

<?php if($nTotalPayment != 0)
{
  ?>

<a class="btn btn-primary" href="<?php echo  Yii::app()->createUrl('teacherSection/Staff/accountEntry?amount='.$nTotalPayment)?>"><i class="icon-download-alt"></i> Confirm Entry</a><br><br>
<?php } ?>
