<style>table#payroll tbody tr td{
  text-align: right;
}

table#payroll tbody tr td:nth-child(1), table#payroll tbody tr td:nth-child(2), table#payroll tbody tr td:nth-child(3), table#payroll tbody tr td:nth-child(5){
  text-align: left;
}

table#payroll{
  width: 100%;
  display: block;
  
  overflow-y: scroll;
  overflow-x: scroll;
  
}

table#payroll thead tr th{
  text-align: center;
}
</style>

<h4>

           <br>
        
<?php 
$allowancePf=0;
$deductionPf=0;
$gtotal=0;
$nTotalSalary=0;
$nTotalPayment=0;
$nsalary=0;
$deductionTotal=0;
$nPayment=0;


$grandBasicTotal=0;
$grandPFTotal=0;
$grandInsuranceTotal=0;
$grandGrossTotal=0;
$grandPFDeductionTotal=0;
$grandInsuranceDeductionTotal=0;
$grandNetTotal=0;
$grandYearlyTotal=0;
$grandMonthlyTaxTotal=0;






$allowances = Allowances::model()->findAll();
$deductions=Deductions::model()->findAll();

 ?>
<table class="table table-bordered table-striped" id="payroll">
    <thead>
        <tr>
            <th rowspan="3">S.N</th>
            <th rowspan="3">Full Name</th>
            <th rowspan="3">Level</th>
            <th colspan="3">Salary</th>
            <th colspan="<?php echo count($allowances) + 1; ?>">Allowances</th>
             <th rowspan="3">Insurance</th>
            <th rowspan="3">Gross Total</th>

            <th colspan="<?php echo count($deductions)+2; ?>">Deductions</th>
            <th rowspan="3"> Total Class (If Part Time)</th>  
            <th rowspan="3">Class Amount (If Part Time)</th>  
            <th rowspan="3"> N.Total</th>    
            <th rowspan="3"> Yearly Total</th>    
            <th rowspan="3"> Monthly Tax</th>         
            <th rowspan="3"> Net payment</th>            
        </tr>
        <tr>
            <th style="background-color: #000055"> Basic </th>
            <th style="background-color: #000055"> Grade </th>
            <th style="background-color: #000055"> Grade Amount</th>
            <th style="background-color: #00405d"> PF/C(<?php echo $model->getPf(time()); ?>%) </th>
        
            <?php foreach($allowances as $allowance){ ?>
            <th style="background-color: #00405d"><?php echo $allowance->allowanceName; ?></th>
            <?php } ?>
            <th style="background-color: #000055"> PF/C(<?php echo $model->getPf(time())*2; ?>%) </th>
          
            <th style="background-color: #000055">Insurance</th>
             <?php foreach($deductions as $deduction){ ?>
                <th style="background-color: #000055"><?php echo $deduction->name; ?></th>
            <?php } ?>
            <!-- <th>P/F</th>
            <th>Self P/F</th>
            <th>TDS</th>
            <th>Advance</th>
            <th>Absense</th>
            <th>Others</th> -->
        </tr>
    </thead>
    <tbody>
        <?php 
        $gTotalSalary=0;
        $ntotal = 0;
        $yearlyIncome=0;
        $tax=0;
        if(!empty($staffs)){ 
         $sn=0;
         
  
        
        foreach ($staffs as $staff) { 

            $monthlyPayroll = new MonthlyPayroll();



                $monthlyPayroll->staff_id=$staff->staff_id;
                $monthlyPayroll->level_id=$staff->level_id;
                $monthlyPayroll->special_case_id=$staff->specialCaseStaff();
                $monthlyPayroll->basic_salary=$staff->basic_salary;
                $monthlyPayroll->grade=$staff->grade;
                $monthlyPayroll->grade_amount=$staff->grade_amount;
                $monthlyPayroll->shreni_id=$staff->shreni_id;

                
            
            $isPartTime=false;   
            $deductionTotal=0;
            $gtotal = 0; 
            $sn++;
           
            ?>

        <tr>
         
         <?php $fullName=$staff->fname." ".$staff->lname; ?>
            <td><?php echo $sn; ?></td>
           <td><?php echo CHtml::link($fullName,['Personaldetails/updateSalary','id'=>$staff->detail_id],array('target'=>'_blank'));?></td>
            <td><?php echo $designation = $staff->getLevelName(); ?></td>
            <td><?php echo $salary = $staff->basic_salary; $grandBasicTotal+=$salary; ?></td>
            <td><?php echo $grade = $staff->grade;?></td>
            <td><?php echo $gradeAmount = $staff->grade_amount; ?></td>
           <!--  <td><?php //echo $pfc = $model->getPfc(time()) * $salary/100; ?></td>
 -->
 <td><?php 

     $salaryIndividual=$salary + ($grade * $gradeAmount);

             
             $allowancePf=$model->isPermanentGovernment($staff->staff_id)?($model->getPf(time())/100) * $salaryIndividual:0; 
             echo  $allowancePf; 
             $grandPFTotal+= $allowancePf;

             $monthlyPayroll->allowance_pf=$allowancePf;
             ?></td>
             <?php 

             $increment=1;
             foreach($allowances as $allowanceDb){ ?>
            <td>
            <?php 

                    $string=$allowanceDb->allowanceName;
                    $allowance="allowance".$increment;
                    $$allowance=strtolower(str_replace(' ', '_',$string));
                    $allowanceAmount="allowanceAmount".$increment;
                    $$allowanceAmount=$model->getAllowanceNumber($staff->staff_id, $allowanceDb->allowanceId);
                    $monthlyPayroll->$$allowance=$$allowanceAmount;
                    $increment++;

                    echo $all = $model->getAllowanceNumber($staff->staff_id, $allowanceDb->allowanceId); 
                    $gtotal += $all;
            ?>

        </td>
            <?php }$gtotal+=$allowancePf; ?>

            <td><?php 
            if($staff->teacher_type != '2')
            {
            echo $insurance=$model->isPermanentGovernment($staff->staff_id)?200:0;
            }
            else
            {
                  echo $insurance=0;
            }
            $grandInsuranceTotal+=$insurance;
            $monthlyPayroll->insurance_add=$insurance;

            ?></td>
            <td>

            <?php 
            echo $gsalary = $insurance + $gtotal + $salary + ($grade * $gradeAmount); 
            $grandGrossTotal+=$gsalary;
            $monthlyPayroll->gross_total=$gsalary;
            $gTotalSalary+=$gsalary;
            ?>

            </td>
            

            <td><?php 
            $deductionPf=$model->isPermanentGovernment($staff->staff_id)?(2*($model->getPf(time())/100)) * $staff->basic_salary:0;
            $grandPFDeductionTotal += $deductionPf;
            echo  $deductionPf;
            $monthlyPayroll->deduction_pf=$deductionPf;
            ?>
            </td>
            
            <td>
                <?php

                if($staff->teacher_type != '2')
                {
                echo $deductInsurance=$model->isPermanentGovernment($staff->staff_id)?200*2:0;
                }
                else
                {
                     echo $deductInsurance=0;
                }
                $grandInsuranceDeductionTotal += $deductInsurance;
                $monthlyPayroll->deduction_insurance=$deductInsurance;

                ?>
            </td>


           <?php 
           $increment=1;
           foreach($deductions as $deductionDb){ ?>
            <td>

            <?php 

                        $string=$deductionDb->name;
                        $deduction="deduction".$increment;
                        $$deduction=strtolower(str_replace(' ', '_',$string));
                        $deductionAmount="deductionAmount".$increment;
                        $$deductionAmount=$model->getDeductionNumber($staff->staff_id, $deductionDb->id);
                        $monthlyPayroll->$$deduction=$$deductionAmount;
                        $increment++;

                        echo $deduction = $model->getDeductionNumber($staff->staff_id, $deductionDb->id); 

                        $deductionTotal += $deduction;
            ?>

        </td>

            <?php }  $deductionTotal += $deductionPf + $deductInsurance;?>

            <td><?php echo $totalClass=$staff->teacher_type == 2 ? $staff->part_time_class : '0'; 

            $monthlyPayroll->part_time_class=$totalClass;
            ?></td>

            <td><?php echo $classAmount=$staff->teacher_type == 2 ? $staff->part_time_class_amount : '0'; 
                    $partTimeAmount=$totalClass * $classAmount;
                     $monthlyPayroll->part_time_class_amount=$partTimeAmount;


            ?></td>
            <td>
            <?php 
            echo $nsalary = ($gsalary - $deductionTotal) + $partTimeAmount; $nTotalSalary+=$nsalary;
            $grandNetTotal += $nsalary;
            $monthlyPayroll->net_salary=$nsalary;
            ?>
            </td>
            <!-- <td><?php //echo $pfc = $model->getPf(time()) * $salary/100;?></td>
            <td><?php //echo $selfpf = $model->getSelfPf($staff->staff_id, time());?></td>
            <td><?php //echo $tds = $model->getTdsRate($staff->staff_id, $gsalary); ?></td>
            <td><?php //echo $advance = 0; ?></td>

            <td><?php //echo $absent = $model->getAbsentDays($staff->staff_id, time());?></td>
            <td><?php //echo $others = 0; ?></td>
            <td><?php //$tdeduction = $selfpf + $tds + $advance + $absent + $others; echo $tdeduction;?></td> -->
            <td><?php 

            echo $yearlyIncome=$nsalary*12 +($allowancePf*12);

            $grandYearlyTotal += $yearlyIncome; 
            $monthlyPayroll->yearly_total=$yearlyIncome;

            ?></td>
            <td><?php 
            $tax=UtilityFunctions::taxAmount($gsalary,$nsalary,$yearlyIncome,$staff);
            $grandMonthlyTaxTotal += $tax/12;
            echo $tax/12; 
            $monthlyPayroll->monthly_tax=$tax/12;
            ?></td>
           <!--  <td><?php //echo $nsalary = $nsalary - $tdeduction; $ntotal += $nsalary; ?></td> -->
             <td><?php echo $nPayment=$nsalary-($tax/12); $nTotalPayment+=$nPayment;
             $monthlyPayroll->net_payment=$nPayment;

             ?></td>

        </tr>
        <?php 


            if(isset($save))
            {
                if($save == true)
                {
                        $monthlyPayroll->created_date=date('Y-m-d');
                        $monthlyPayroll->month=$_POST['month'];
                        $monthlyPayroll->year=$_POST['year']; 
                        if(MonthlyPayroll::checkExistingData($_POST['month'],$_POST['year'],$staff->staff_id))
                        {
                                if($monthlyPayroll->save())
                                {


                                    Yii::app()->user->setFlash('success','Monthly Salary Saved.');
                                    $departments=$staff->departmentList();
                                    if($departments != null)
                                    {
                                        foreach ($departments as $key) 
                                        {
                                            $departmentPayroll=new DepartmentPayroll();
                                            $departmentPayroll->payroll_id=$monthlyPayroll->id;
                                            $departmentPayroll->department_id=$key->department_id;
                                            $departmentPayroll->save();


                                        }
                                    }
                                }
                                else
                                {
                                    print_r($monthlyPayroll->errors); exit;
                                }
                         }
                         else
                         {
                               Yii::app()->user->setFlash('error','This month salary has already been saved.');
                         }
                }
            }



        }  }?>




          <?php 
       
        if(!empty($staffs)){ 
        
  
        foreach ($staffs as $staff) { 
           
            
            ?>

        <tr>
         
            <td><?php echo 'Total' ?></td>
            <td><?php echo '';?></td>

            <td><?php '' ?></td>
            <td><?php echo $grandBasicTotal; ?></td>
            <td><?php echo '';?></td>
             <td><?php echo ''; ?></td>
            <td><?php echo $grandPFTotal; ?></td>
             <?php foreach($allowances as $allowance){ ?>
            <td><?php /*echo $all = $model->getAllowances($staff->staff_id, $allowance->allowanceId,  time()) * $salary/100 ; 
            $gtotal += $all;*/
            ?>
            <?php echo ''; 
            
            ?>

            </td>
            <?php } ?>
            <td><?php 
            echo $grandInsuranceTotal; ?></td>
            <td><?php echo $grandGrossTotal; ?></td>
            <td><?php echo $grandPFDeductionTotal; ?></td>
             <td><?php echo $grandInsuranceDeductionTotal; ?></td>
           <?php foreach($deductions as $deduction){ ?>
            <td>
            <?php echo '';
            ?>

            </td>

            <?php } ?>
            <td></td>
            <td></td>
            <td><?php echo $grandNetTotal; ?></td>
             <td><?php echo $grandYearlyTotal; ?></td>
             <td><?php echo $grandMonthlyTaxTotal; ?></td>
           
           <!--  <td><?php //echo $nsalary = $nsalary - $tdeduction; $ntotal += $nsalary; ?></td> -->
             <td><?php echo $nTotalPayment; ?></td>
  
        </tr>
        <?php break; } }?>
           
    </tbody>
</table>


