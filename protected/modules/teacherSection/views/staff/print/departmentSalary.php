

<style>table#payroll tbody tr td{
  text-align: right;
}

table#payroll tbody tr td:nth-child(1), table#payroll tbody tr td:nth-child(2), table#payroll tbody tr td:nth-child(3), table#payroll tbody tr td:nth-child(5){
  text-align: left;
}

table#payroll{
  width: 100%;
  display: block;
  
  overflow-y: scroll;
  overflow-x: scroll;
  
}

table#payroll thead tr th{
  text-align: center;
}
</style>

<br>
<?php 
$allowancePf=0;
$deductionPf=0;
$gtotal=0;
$nTotalSalary=0;
$nTotalPayment=0;
$nsalary=0;
$deductionTotal=0;
$nPayment=0;

$allowances = Allowances::model()->findAll();
$deductions=Deductions::model()->findAll();

 ?>
<table class="table table-bordered table-striped" id="payroll">
    <thead>
        <tr>
            <th rowspan="3">S.N</th>
            <th rowspan="3">Full Name</th>
            <th rowspan="3">Level</th>
            <th colspan="3">Salary</th>
            <th colspan="<?php echo count($allowances) + 1; ?>">Allowances</th>
             <th rowspan="3">Insurance</th>
            <th rowspan="3">Gross Total</th>

            <th colspan="<?php echo count($deductions)+2; ?>">Deductions</th>
            <th rowspan="3"> N.Total</th>    
            <th rowspan="3"> Yearly Total</th>    
            <th rowspan="3"> Monthly Tax</th>         
            <th rowspan="3"> Net payment</th>            
        </tr>
        <tr>
            <th style="background-color: #000055"> Basic </th>
            <th style="background-color: #000055"> Grade </th>
            <th style="background-color: #000055"> Grade Amount</th>
            <th style="background-color: #00405d"> PF/C(<?php echo $model->getPf(time()); ?>%) </th>
        
            <?php foreach($allowances as $allowance){ ?>
            <th style="background-color: #00405d"><?php echo $allowance->allowanceName; ?></th>
            <?php } ?>
            <th style="background-color: #000055"> PF/C(<?php echo $model->getPf(time())*2; ?>%) </th>
          
            <th style="background-color: #000055">Insurance</th>
             <?php foreach($deductions as $deduction){ ?>
                <th style="background-color: #000055"><?php echo $deduction->name; ?></th>
            <?php } ?>
            <!-- <th>P/F</th>
            <th>Self P/F</th>
            <th>TDS</th>
            <th>Advance</th>
            <th>Absense</th>
            <th>Others</th> -->
        </tr>
    </thead>
    <tbody>
        <?php 
        $gTotalSalary=0;
        $ntotal = 0;
        $yearlyIncome=0;
        $tax=0;

        if(!empty($staffs)){ 
         $sn=0;
         
  
        foreach ($staffs as $staff) { 
            
            $isPartTime=false;   
            $deductionTotal=0;
            $gtotal = 0; 
            $sn++;
           
            ?>

        <tr>

           
        <?php $fullName=$staff->fname." ".$staff->lname; ?>
            <td><?php echo $sn; ?></td>
            <td><?php echo CHtml::link($fullName,['Personaldetails/updateSalary','id'=>$staff->detail_id],array('target'=>'_blank'));?></td>
            <td><?php echo $designation = $staff->getLevelName(); ?></td>
            <td><?php echo $salary = $staff->basic_salary ?></td>
            <td><?php echo $grade = $staff->grade;?></td>
            <td><?php echo $gradeAmount = $staff->grade_amount; ?></td>
           <!--  <td><?php //echo $pfc = $model->getPfc(time()) * $salary/100; ?></td>
 -->
 <td><?php 

    $salaryIndividual=$salary + ($grade * $gradeAmount);

             
             $allowancePf=$model->isPermanentGovernment($staff->staff_id)?($model->getPf(time())/100) * $salaryIndividual:0; 
             echo  $allowancePf; ?></td>
             <?php foreach($allowances as $allowance){ ?>
            <td><?php /*echo $all = $model->getAllowances($staff->staff_id, $allowance->allowanceId,  time()) * $salary/100 ; 
            $gtotal += $all;*/
            ?>
            <?php echo $all = $model->getAllowanceNumber($staff->staff_id, $allowance->allowanceId); 
            $gtotal += $all;
            ?>

        </td>
            <?php }$gtotal+=$allowancePf; ?>

            <td><?php 
            echo $insurance=$model->isPermanentGovernment($staff->staff_id)?200:0?></td>
            <td>
            <?php 
            echo $gsalary = $insurance + $gtotal + $salary + ($grade * $gradeAmount); 
            $gTotalSalary+=$gsalary;
            ?>

            </td>
            

            <td><?php 
            $deductionPf=$model->isPermanentGovernment($staff->staff_id)?(2*($model->getPf(time())/100)) * $staff->basic_salary:0;
            echo  $deductionPf;?>
            </td>
            
            <td>
                <?php

                
                echo $deductInsurance=$model->isPermanentGovernment($staff->staff_id)?200*2:0;

                ?>
            </td>


           <?php foreach($deductions as $deduction){ ?>
            <td>

            <?php 

            echo $deduction = $model->getDeductionNumber($staff->staff_id, $deduction->id); 

            $deductionTotal += $deduction;
            ?>

        </td>

            <?php }  $deductionTotal += $deductionPf + $deductInsurance;?>
            <td>
            <?php 
            echo $nsalary = $gsalary - $deductionTotal ; $nTotalSalary+=$nsalary;
            ?>
            </td>
            <!-- <td><?php //echo $pfc = $model->getPf(time()) * $salary/100;?></td>
            <td><?php //echo $selfpf = $model->getSelfPf($staff->staff_id, time());?></td>
            <td><?php //echo $tds = $model->getTdsRate($staff->staff_id, $gsalary); ?></td>
            <td><?php //echo $advance = 0; ?></td>

            <td><?php //echo $absent = $model->getAbsentDays($staff->staff_id, time());?></td>
            <td><?php //echo $others = 0; ?></td>
            <td><?php //$tdeduction = $selfpf + $tds + $advance + $absent + $others; echo $tdeduction;?></td> -->
            <td><?php echo $yearlyIncome=$nsalary*12 +($allowancePf*12); ?></td>
            <td><?php 
            $taxModel=PayrollTaxSlack::model()->findByPk(1);

            if($gsalary*12 < $taxModel->first_tax_amount)
            {

                 $firstTaxSlack=($taxModel->first_tax_percent/100) *($gsalary*12);
                  $tax=$firstTaxSlack;
               
            }

            else
            {
                $firstTaxSlack=($taxModel->first_tax_percent/100) *$taxModel->first_tax_amount;
            }
 
            if($yearlyIncome > $taxModel->first_tax_amount && $yearlyIncome < $taxModel->second_tax_amount)
            {
                //$firstTaxSlack=(1/100) *300000;
                $secondTaxSlack=($taxModel->second_tax_percent/100)* ($yearlyIncome - $taxModel->first_tax_amount);
                $tax=$firstTaxSlack + $secondTaxSlack;
            }

            elseif($yearlyIncome > $taxModel->third_tax_amount)
            {
                //$firstTaxSlack=(1/100) *300000;
                $secondTaxSlack=($taxModel->second_tax_percent/100)* ($taxModel->second_tax_amount - $taxModel->first_tax_amount);
                $thirdTaxSlack=($taxModel->third_tax_percent/100)* ($yearlyIncome-$taxModel->second_tax_amount);
                $tax=$firstTaxSlack +$secondTaxSlack +$thirdTaxSlack;
            }
            echo $tax/12; ?></td>
           <!--  <td><?php //echo $nsalary = $nsalary - $tdeduction; $ntotal += $nsalary; ?></td> -->
             <td><?php echo $nPayment=$nsalary-($tax/12); $nTotalPayment+=$nPayment?></td>

        </tr>
        <?php }  }?>




          <?php 
       
        if(!empty($staffs)){ 
        
  
        foreach ($staffs as $staff) { 
           
            
            ?>

        <tr>
         
            <td><?php echo 'Total' ?></td>
            <td><?php echo '';?></td>

            <td><?php echo ''; ?></td>
            <td><?php echo '' ?></td>
            <td><?php echo '';?></td>
             <td><?php echo ''; ?></td>
            <td><?php //echo $pfc = $model->getPfc(time()) * $salary/100; ?></td>
             <?php foreach($allowances as $allowance){ ?>
            <td><?php /*echo $all = $model->getAllowances($staff->staff_id, $allowance->allowanceId,  time()) * $salary/100 ; 
            $gtotal += $all;*/
            ?>
            <?php echo ''; 
            
            ?>

        </td>
            <?php } ?>
            <td><?php 


            echo '';?></td>
            <td></td>
           <?php foreach($deductions as $deduction){ ?>
            <td>
            <?php echo '';
            ?>

        </td>

            <?php } ?>
            <td><?php 


            echo '';?></td>
            <!-- <td><?php //echo $pfc = $model->getPf(time()) * $salary/100;?></td>
            <td><?php //echo $selfpf = $model->getSelfPf($staff->staff_id, time());?></td>
            <td><?php //echo $tds = $model->getTdsRate($staff->staff_id, $gsalary); ?></td>
            <td><?php //echo $advance = 0; ?></td>

            <td><?php //echo $absent = $model->getAbsentDays($staff->staff_id, time());?></td>
            <td><?php //echo $others = 0; ?></td>
            <td><?php //$tdeduction = $selfpf + $tds + $advance + $absent + $others; echo $tdeduction;?></td> -->
            <td><?php ''; ?></td>
            <td></td>
            <td></td>
            <td><?php 
            
            echo ''; ?></td>
           <!--  <td><?php //echo $nsalary = $nsalary - $tdeduction; $ntotal += $nsalary; ?></td> -->
             <td><?php echo $nTotalPayment; ?></td>
  
        </tr>
        <?php break; } }?>
           
    </tbody>
</table>




