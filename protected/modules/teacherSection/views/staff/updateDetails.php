<style>

table#payroll{
  width: 100%;
  display: block;
  
  overflow-y: scroll;
  overflow-x: scroll;
  table-layout:fixed;

  
}

table#payroll thead tr th{
  text-align: center;
}
</style>

<center><h2>Edit/View Department and Amounts</h2></center><br>
<table class="table table-condensed">

<tr>
	<th><h4>Name</h4></th>
	<th><h4><?= $staff->fname.' '.$staff->lname ?></h4></th>	
</tr>
<tr>

	<th><h4>Address</h4></th>
	<th><h4><?= $staff->address ?></h4></th>
</tr>
<tr>
	<th><h4>Contact</h4></th>
	<th><h4><?= $staff->contact ?></h4></th>
</tr>
<tr>
	<th><h4>Level/Shreni</h4></th>
	<th><h4><?= $staff->level->name ?>/<?= $staff->shreni->name ?></h4></th>
</tr>

</table>
<hr>

<form action="<?= Yii::app()->request->baseUrl."/teacherSection/staff/saveAllDepartmentDetails" ?>" method="post">
<input name="staff_id" value="<?php echo $id ?>" hidden></input>
<table class="table table-bordered table-striped table-condensed" id="payroll">

<tr>
	<th rowspan="2">REMOVE</th>
	<th rowspan="2">EDIT</th>
	<th rowspan="2">DEPARTMENT</th>
	<th rowspan="2">BASIC SALARY</th>
	<th rowspan="2">GRADE</th>
	<th rowspan="2">GRADE AMOUNT</th>
	<?php foreach ($allowances as $key => $allowance) { ?>
		<th rowspan="2" style="background-color: #000055; color:white;"><?= $allowance->allowanceName?></th>
	<?php } ?>
	<?php foreach ($plus2Allowances as $key => $pallowance) { ?>
		<th colspan="2" style="background-color: #000055; color:white;"><?= $pallowance->name?></th>
	<?php } ?>
	<?php foreach ($deductions as $key => $deduction) { ?>
		<th rowspan="2" style="background-color: #00405d; color:white"><?= $deduction->name?></th>
	<?php } ?>
	
</tr>
<tr>
<?php foreach ($plus2Allowances as $key => $pallowance) { ?>
		<th style="background-color: #000055; color:white;">Days</th>
		<th style="background-color: #000055; color:white;">Amount</th>
	<?php } ?>

</tr>

<?php foreach ($special_cases as $key => $special_case) { 
		$special_case_staff=SpecialCaseStaff::model()->findByAttributes(array('staff_id'=>$id,'special_case_id'=>$special_case->id));
		$checked=$special_case_staff != null ? 'checked':'';
		//$currentStatus=$special_case_staff != null ? true:false;
		if($special_case_staff != null)
		{
			echo "<tr class='success'>";
			echo "<td><a href=".Yii::app()->request->baseUrl.'/teacherSection/staff/removeStaffFromDepartment?id='.$id."&special_case_id=".$special_case->id."><i class='fa fa-close'></i></a></td>";
		}
		else{
			echo "<tr>";
			echo "<td></td>";
		}
	?>
	
		<td><input type="checkbox" value="<?php echo $special_case->id ?>" name="allcases[]" <?php echo $checked ?>></input></td>
		<td><?= $special_case->name ?></td>
		<td><input type="number" value="<?= $special_case_staff!=null?$special_case_staff->basic_salary:null; ?>" name="<?php echo "payroll[".$special_case->id."]"."[salaries]"."[basicSalary]" ?>"  style="width: 90px"></input></td>
		<td><input type="number" value="<?= $special_case_staff!=null?$special_case_staff->grade:null; ?>" name="<?php echo "payroll[".$special_case->id."]"."[salaries]"."[grade]" ?>"  style="width: 90px"></input></td>
		<td><input type="number" value="<?= $special_case_staff!=null?$special_case_staff->grade_amount:null; ?>" name="<?php echo "payroll[".$special_case->id."]"."[salaries]"."[gradeAmount]" ?>"  style="width: 90px"></input></td>

		<!-- Allowances  -->
		<?php foreach ($allowances as $key => $allowance) { 
			$staffAllowance=StaffAllowance::model()->findByAttributes(array('staff_id'=>$id,'special_case_id'=>$special_case->id,'allowance_id'=>$allowance->allowanceId));
			?>
		<td><input value="<?= $staffAllowance!=null?$staffAllowance->percentage:null; ?>" type="number" name="<?php echo "payroll[".$special_case->id."]"."[allowances]"."[".$allowance->allowanceId."]" ?>"  style="width: 90px"></input></td>
		<?php } ?>

		<!-- Plus 2 Allowances  -->
		<?php 
		if($special_case->type == 'plus2')
			$isPlus2='enabled';
		else
			$isPlus2='disabled';
		foreach ($plus2Allowances as $key => $pallowance) { 
			if($isPlus2 == 'enabled')
			{
				$staffPAllowance=StaffPlus2Allowances::model()->findByAttributes(array('staff_id'=>$id,'allowance_id'=>$pallowance->id));
			}
			else
				$staffPAllowance=null;
			?>
		<td><input value="<?= $staffPAllowance!=null?$staffPAllowance->days:null; ?>" type="number" name="<?php echo "payroll[".$special_case->id."]"."[pAllowances]"."[".$pallowance->id."]"."[days]" ?>"  style="width: 90px" <?= $isPlus2 ?>></input></td>
		<td><input value="<?= $staffPAllowance!=null?$staffPAllowance->amount:null; ?>" type="number" name="<?php echo "payroll[".$special_case->id."]"."[pAllowances]"."[".$pallowance->id."]"."[amount]" ?>"  style="width: 90px"  <?= $isPlus2 ?>></input></td>
		<?php } ?>

		<!-- Deductions -->
		<?php foreach ($deductions as $key => $deduction) { 
			$staffDeduction=StaffDeduction::model()->findByAttributes(array('staff_id'=>$id,'special_case_id'=>$special_case->id,'deduction_id'=>$deduction->id));
			?>
		<td><input value="<?= $staffDeduction!=null?$staffDeduction->amount:null; ?>" type="number" name="<?php echo "payroll[".$special_case->id."]"."[deductions]"."[".$deduction->id."]" ?>"  style="width: 90px"></input></td>
		<?php } ?>
		

		</tr>
<?php } ?>

</table>
<input type="submit" value="SAVE"></input>
</form>