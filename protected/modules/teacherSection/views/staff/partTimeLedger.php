<style>table#payroll tbody tr td{
  text-align: right;
}

table#payroll tbody tr td:nth-child(1), table#payroll tbody tr td:nth-child(2), table#payroll tbody tr td:nth-child(3), table#payroll tbody tr td:nth-child(5){
  text-align: left;
}

table#payroll{
  width: 100%;
  display: block;
  
  overflow-y: scroll;
  overflow-x: scroll;
  
}

table#payroll thead tr th{
  text-align: center;
}
</style>

<h4>
    <?php 
    
    if(!empty($month)) echo "Part Time Salary Sheet"; else echo "Part Time Salary Sheet"; ?></h4> <hr>

<form action="" method="POST">
<?php echo CHtml::dropDownList('month','', UtilityFunctions::getMonth()); ?>&nbsp;
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'Submit',
    'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'small', // null, 'large', 'small' or 'mini'
    'htmlOptions'=>array('id'=>'depart-submit'),
)); ?> 
</form>
<br>

<table class="table table-bordered table-striped" id="payroll">
    <thead>
        <tr>
            <th rowspan="3">S.N</th>
            <th rowspan="3">Full Name</th>
            <th rowspan="3">Theory Attendance</th>  
            <th rowspan="3">Practical Attendance</th>
            <th rowspan="3">Theory Rate</th>  
            <th rowspan="3">Practical Rate</th>  
            <th rowspan="3">Gross Amount </th>
            <th rowspan="3">Net Amount</th>             
        </tr>
       
    </thead>
    <tbody>
        <?php 
         $totalNetAmount=0;

        if(!empty($staffs)){ 
         $sn=0;
       
  
        foreach ($staffs as $staff) {         
            $staffInfo=UtilityFunctions::getPartTimeStaffInfo($staff->staff_id,$month);               
            $sn++;
            ?>
                        <tr>
                        <td><?php echo $sn; ?></td>
                        <td><?php echo $staff->fname.' '.$staff->lname; ?></td>

                        <td>
                        <?php
                                echo $ta=$staffInfo!=null?$staffInfo->theory_class_attend:'0'; 
                        ?>
                        </td>

                        <td> 
                        <?php
                               echo $pa=$staffInfo!=null? $staffInfo->practical_class_attend:'0';
                        ?>
                            
                        </td>

                        <td>
                         <?php
                                echo $tm=$staffInfo!=null? $staffInfo->theory_class_amount:'0';
                        ?>    
                        </td>
                       
                        <td>
                        <?php
                                echo $pm=$staffInfo!=null? $staffInfo->practical_class_amount:'0';
                        ?>
                        </td>
                        <td>
                        <?php
                        echo $grossAmount=($ta*$tm)+($pa*$pm);

                        ?>
                        </td>
                        <td>
                        <?php    
                        echo $netAmount=$grossAmount-((15/100)*$grossAmount);$totalNetAmount+=$netAmount;
                        ?>
                        </td>
                        <tr>

  
<?php } }?>
<tr>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td><?php echo $totalNetAmount;?></td>

</tr>

</table>
