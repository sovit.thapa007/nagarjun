<style>table#payroll tbody tr td{
  text-align: right;
}

table#payroll tbody tr td:nth-child(1), table#payroll tbody tr td:nth-child(2), table#payroll tbody tr td:nth-child(3), table#payroll tbody tr td:nth-child(5){
  text-align: left;
}

table#payroll{
  width: 100%;
  display: block;
  
  overflow-y: scroll;
  overflow-x: scroll;
  
}

table#payroll thead tr th{
  text-align: center;
}
</style>

<h4>
    <?php 
    

    if(!empty($special)) echo StaffSpecialCase::model()->findByPk($special)->name," Ledger Sheet"; else echo "Special Case Wise Salary Sheet"; ?></h4> <hr>

<form action="" method="POST">

<?php echo CHtml::dropDownList('month','', UtilityFunctions::getMonth()); ?>
<?php echo CHtml::dropDownList('year', '',  UtilityFunctions::getYear()); ?>
<?php echo CHtml::dropDownList('special', '', CHtml::listData(StaffSpecialCase::model()->findAll(), 'id', 'name'), array('prompt'=>'--Select Special Case--')); ?>&nbsp;
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'Submit',
    'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'small', // null, 'large', 'small' or 'mini'
    'htmlOptions'=>array('id'=>'special-submit'),
)); ?> 
</form>
<br>
<?php 
$allowancePf=0;
$deductionPf=0;
$gtotal=0;
$nTotalSalary=0;
$nTotalPayment=0;
$nsalary=0;
$deductionTotal=0;
$nPayment=0;

$allowances = Allowances::model()->findAll();
$deductions=Deductions::model()->findAll();

 ?>
<table class="table table-bordered table-striped" id="payroll">
    <thead>
        <tr>
            <th rowspan="3">S.N</th>
            <th rowspan="3">Full Name</th>
            <th rowspan="3">Level</th>
            <th colspan="3">Salary</th>
            <th rowspan="3"> PF/C(<?php echo $model->getPf(time()); ?>%) </th>
            <th rowspan="3">Allowances</th>
             <th rowspan="3">Insurance</th>
            <th rowspan="3">Gross Total</th>
             <th rowspan="3">PF/C(<?php echo $model->getPf(time())*2; ?>%) </th>
              <th rowspan="3">Insurance</th>

            <th rowspan="3">Deductions</th>
            <th rowspan="3"> N.Total</th>    
            <th rowspan="3"> Yearly Total</th>    
            <th rowspan="3"> Monthly Tax</th>         
            <th rowspan="3"> Net payment</th>            
        </tr>
        <tr>
            <th style="background-color: #000055"> Basic </th>
            <th style="background-color: #000055"> Grade </th>
            <th style="background-color: #000055"> Grade Amount</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $gTotalSalary=0;
        $ntotal = 0;
        $yearlyIncome=0;
        $tax=0;

        if(!empty($staffs)){ 
         $sn=0;
         
  
        foreach ($staffs as $staff) { 
            
            $isPartTime=false;   
            $deductionTotal=0;
            $gtotal = 0; 
            $sn++;
           
            ?>

        <tr>

           
        <?php $fullName=$staff->fname." ".$staff->lname; ?>
            <td><?php echo $sn; ?></td>
            <td><?php echo $fullName; ?></td>
            <td><?php echo $designation = $staff->getLevelName(); ?></td>
            <td><?php echo $salary = $staff->basic_salary ?></td>
            <td><?php echo $grade = $staff->grade;?></td>
            <td><?php echo $gradeAmount = $staff->grade_amount; ?></td>
           <!--  <td><?php //echo $pfc = $model->getPfc(time()) * $salary/100; ?></td>
 -->
 <td><?php 

 $salaryIndividual=$salary + ($grade * $gradeAmount);
             
             $allowancePf=$model->isPermanentGovernment($staff->staff_id)?($model->getPf(time())/100) * $salaryIndividual:0; 
             echo  $allowancePf; ?></td>
             <?php foreach($allowances as $allowance){ ?>
            <?php /*echo $all = $model->getAllowances($staff->staff_id, $allowance->allowanceId,  time()) * $salary/100 ; 
            $gtotal += $all;*/
            ?>
            <?php  $all = $model->getAllowanceNumber($staff->staff_id, $allowance->allowanceId); 
            $gtotal += $all;
            ?>

            <?php } ?>
             <td>
                <?php echo $gtotal; ?>
            </td>
        <?php
            $gtotal+=$allowancePf; ?>

            <td><?php 
            echo $insurance=$model->isPermanentGovernment($staff->staff_id)?200:0?></td>
            <td>
            <?php 
            echo $gsalary = $insurance + $gtotal + $salary + ($grade * $gradeAmount); 
            $gTotalSalary+=$gsalary;
            ?>

            </td>
            

            <td><?php 
            $deductionPf=$model->isPermanentGovernment($staff->staff_id)?(2*($model->getPf(time())/100)) * $staff->basic_salary:0;
            echo  $deductionPf;?>
            </td>
            
            <td>
                <?php

                
                echo $deductInsurance=$model->isPermanentGovernment($staff->staff_id)?200*2:0;

                ?>
            </td>


           <?php foreach($deductions as $deduction){ ?>
            

            <?php 

             $deduction = $model->getDeductionNumber($staff->staff_id, $deduction->id); 

            $deductionTotal += $deduction;
            ?>  

            <?php } ?>
            <td>
                <?php echo $deductionTotal; ?>
            </td>

            <?php
             $deductionTotal += $deductionPf + $deductInsurance;?>
            <td>
            <?php 
            echo $nsalary = $gsalary - $deductionTotal ; $nTotalSalary+=$nsalary;
            ?>
            </td>
            <!-- <td><?php //echo $pfc = $model->getPf(time()) * $salary/100;?></td>
            <td><?php //echo $selfpf = $model->getSelfPf($staff->staff_id, time());?></td>
            <td><?php //echo $tds = $model->getTdsRate($staff->staff_id, $gsalary); ?></td>
            <td><?php //echo $advance = 0; ?></td>

            <td><?php //echo $absent = $model->getAbsentDays($staff->staff_id, time());?></td>
            <td><?php //echo $others = 0; ?></td>
            <td><?php //$tdeduction = $selfpf + $tds + $advance + $absent + $others; echo $tdeduction;?></td> -->
            <td><?php echo $yearlyIncome=$nsalary*12 +($allowancePf*12); ?></td>
            <td><?php 
             $tax=UtilityFunctions::taxAmount($gsalary,$yearlyIncome,$staff->level_id);
            echo $tax/12; ?></td>
           <!--  <td><?php //echo $nsalary = $nsalary - $tdeduction; $ntotal += $nsalary; ?></td> -->
             <td><?php echo $nPayment=$nsalary-($tax/12); $nTotalPayment+=$nPayment?></td>

        </tr>
        <?php }  }?>




          <?php 
       
        if(!empty($staffs)){ 
        
  
        foreach ($staffs as $staff) { 
           
            
            ?>

        <tr>
         
            <td><?php echo 'Total' ?></td>
            <td><?php echo '';?></td>

            <td><?php echo ''; ?></td>
            <td><?php echo '' ?></td>
            <td><?php echo '';?></td>
             <td><?php echo ''; ?></td>
            <td><?php //echo $pfc = $model->getPfc(time()) * $salary/100; ?></td>
             <?php foreach($allowances as $allowance){ ?>
            <td><?php /*echo $all = $model->getAllowances($staff->staff_id, $allowance->allowanceId,  time()) * $salary/100 ; 
            $gtotal += $all;*/
            ?>
            <?php echo ''; 
            
            ?>

        </td>
            <?php } ?>
           
           
          
           
            <!-- <td><?php //echo $pfc = $model->getPf(time()) * $salary/100;?></td>
            <td><?php //echo $selfpf = $model->getSelfPf($staff->staff_id, time());?></td>
            <td><?php //echo $tds = $model->getTdsRate($staff->staff_id, $gsalary); ?></td>
            <td><?php //echo $advance = 0; ?></td>

         
            
            <td><?php 
            
            echo ''; ?></td>
           <!--  <td><?php //echo $nsalary = $nsalary - $tdeduction; $ntotal += $nsalary; ?></td> -->
             <td><?php echo $nTotalPayment; ?></td>
  
        </tr>
        <?php break; } }?>
           
    </tbody>
</table>

<a class="btn btn-primary" href="#" target="_blank"><i class="icon-download-alt"></i> Download as pdf</a><br><br>
