<style>table#payroll tbody tr td{
  text-align: right;
}

table#payroll tbody tr td:nth-child(1), table#payroll tbody tr td:nth-child(2), table#payroll tbody tr td:nth-child(3), table#payroll tbody tr td:nth-child(5){
  text-align: left;
}

table#payroll{
  width: 100%;
  display: block;
  
  overflow-y: scroll;
  overflow-x: scroll;
  
}

table#payroll thead tr th{
  text-align: center;
}
</style>

<h4>

    <?php 

    if(isset($departmentDropdown))
    {
         if(!empty($depart)) echo Department::model()->findByPk($depart)->department_name," Ledger Sheet"; else echo "Department Wise Salary Sheet"; ?></h4> <hr>

        <form action="" method="POST">
        <?php echo CHtml::dropDownList('depart', '', CHtml::listData(Department::model()->findAll(), 'department_id', 'department_name'), array('prompt'=>'--Select Post--')); ?>&nbsp;

         <?php echo CHtml::dropDownList('type', '', array(''=>'---Teacher Type---','1'=>'Full Time','2'=>'Part Time')); ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'Submit',
            'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'htmlOptions'=>array('id'=>'depart-submit'),
        )); ?> 
        </form>
        <br>
<?php 
    }
    elseif(isset($specialDropdown))
    {
        if(!empty($special)) echo StaffSpecialCase::model()->findByPk($special)->name," Ledger Sheet"; else echo "Special Case Wise Salary Sheet"; ?></h4> <hr>

        <form action="" method="POST">
        <?php echo CHtml::dropDownList('special', '', CHtml::listData(StaffSpecialCase::model()->findAll(), 'id', 'name'), array('prompt'=>'--Select Department--')); ?>&nbsp;
        
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'Submit',
            'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'htmlOptions'=>array('id'=>'special-submit'),
        )); ?> 

        </form>
        <br>
        <?php
    }
    elseif(isset($saveMonthlySalary))
    { 
        ?>

        <h4>Save Monthly Salary </h4> <hr>

        <form action="" method="POST">
        <?php echo CHtml::dropDownList('searchmonth',$month, UtilityFunctions::getMonth()); ?>

        <?php echo CHtml::dropDownList('searchyear', $year,  UtilityFunctions::getYear()); ?>

         <?php echo CHtml::dropDownList('special', $special, CHtml::listData(StaffSpecialCase::model()->findAll(), 'id', 'name'), array('prompt'=>'--Select Department--')); ?>

         <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'Submit',
            'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'htmlOptions'=>array('id'=>'special-submit'),
        )); ?> 
        <br>

        <input value="<?php echo $month ?>" name="month" hidden></input>
        <input value="<?php echo $year ?>" name="year" hidden></input>
         <input value="<?php echo $special ?>" name="save-special" hidden></input>

        <?php
        if($month != null && $year != null)
        {
            echo "Month:<b>".UtilityFunctions::getMonth()[$month]."</b>";
            echo "<br>";
            echo "Year:<b>".UtilityFunctions::getYear()[$year]."</b>";
            echo "<br>";
            
        }

        ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'Save Monthly Salary',
            'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'large', // null, 'large', 'small' or 'mini'
            'htmlOptions'=>array('id'=>'save-submit','name'=>'save-submission'),
        )); ?> 
      
        <br>
        <?php 
    }
    
    ?>
<?php 
$allowancePf=0;
$deductionPf=0;
$gtotal=0;
$nTotalSalary=0;
$nTotalPayment=0;
$nsalary=0;
$deductionTotal=0;
$nPayment=0;


$grandBasicTotal=0;
$grandPFTotal=0;
$grandInsuranceTotal=0;
$grandGrossTotal=0;
$grandPFDeductionTotal=0;
$grandInsuranceDeductionTotal=0;
$grandNetTotal=0;
$grandYearlyTotal=0;
$grandMonthlyTaxTotal=0;

$grandGradeAmount=0;
$grandTotalSalary=0;





$allowances = Allowances::model()->findAll();
$deductions=Deductions::model()->findAll();

 ?>
<table class="table table-bordered table-striped" id="payroll">
    <thead>
        <tr>
    
    <?php    
    if(isset($saveMonthlySalary))
    {  ?><th rowspan ="3"></th>
<?php } ?>



            <th rowspan="3">S.N</th>

            <th rowspan="3">Full Name</th>
            <th rowspan="3">Level</th>
            <th colspan="5">Salary</th>
            <th colspan="<?php echo count($allowances) + 1; ?>">Allowances</th>
             <th rowspan="3">Insurance</th>
            <th rowspan="3">Gross Total</th>

            <th colspan="<?php echo count($deductions)+2; ?>">Deductions</th>
            <th rowspan="3"> Total Class (If Part Time)</th>  
            <th rowspan="3"> Class Amount (If Part Time)</th>  
            <th rowspan="3"> N.Total</th>    
            <th rowspan="3"> Yearly Total</th>    
            <th rowspan="3"> Monthly Tax</th>         
            <th rowspan="3"> Net payment</th>            
        </tr>
        <tr>
            <th style="background-color: #000055"> Basic </th>
            <th style="background-color: #000055"> Grade </th>
            <th style="background-color: #000055"> Grade Amount</th>
            <th style="background-color: #000055"> Total Grade</th>
            <th style="background-color: #000055"> Total Salary</th>

            <th style="background-color: #00405d"> PF/C(<?php echo $model->getPf(time()); ?>%) </th>
        
            <?php foreach($allowances as $allowance){ ?>
            <th style="background-color: #00405d"><?php echo $allowance->allowanceName; ?></th>
            <?php } ?>
            <th style="background-color: #000055"> PF/C(<?php echo $model->getPf(time())*2; ?>%) </th>
          
            <th style="background-color: #000055">Insurance</th>
             <?php foreach($deductions as $deduction){ ?>
                <th style="background-color: #000055"><?php echo $deduction->name; ?></th>
            <?php } ?>
            <!-- <th>P/F</th>
            <th>Self P/F</th>
            <th>TDS</th>
            <th>Advance</th>
            <th>Absense</th>
            <th>Others</th> -->
        </tr>
    </thead>
    <tbody>
        <?php 
        $gTotalSalary=0;
        $ntotal = 0;
        $yearlyIncome=0;
        $tax=0;
        if(!empty($staffs)){ 
         $sn=0;
         
  
        
        foreach ($staffs as $staff) { 
            
            $monthlyPayroll = new MonthlyPayroll();



                $monthlyPayroll->staff_id=$staff->staff_id;
                $monthlyPayroll->level_id=$staff->level_id;
                $monthlyPayroll->special_case_id=$special;
                $monthlyPayroll->basic_salary=$staff->getBasicSalary($special);
                $monthlyPayroll->grade=$staff->getGrade($special);;
                $monthlyPayroll->grade_amount=$staff->getGradeAmount($special);
                $monthlyPayroll->shreni_id=$staff->shreni_id;

                
            
            $isPartTime=false;   
            $deductionTotal=0;
            $gtotal = 0; 
            $sn++;
           
            ?>

        <tr>

        <?php    
            if(isset($saveMonthlySalary) && $month != null && $year != null)
            {   

                if(MonthlyPayroll::checkExistingData($month,$year,$staff->staff_id,$special))
                { 
         ?>
                     <td><input type="checkbox" name="box[]" value="<?= $staff->staff_id ?>" checked/></td>  

          <?php }else{
                        echo "<td>Already Saved</td>";

                    } 


            }?>

          </form>
         <?php $fullName=$staff->fname." ".$staff->lname; ?>
            <td><?php echo $sn; ?></td>
            <td><?php echo CHtml::link($fullName,['Staff/updateDetails','id'=>$staff->staff_id],array('target'=>'_blank'));?></td>
            <td><?php echo $designation = $staff->getLevelName(); ?></td>
            <td><?php echo $salary = $staff->getBasicSalary($special); $grandBasicTotal+=$salary; ?></td>
            <td><?php echo $grade = $staff->getGrade($special); ?></td>
            <td><?php echo $gradeAmount = $staff->getGradeAmount($special); ?></td>
             <td><?php echo$grade*$gradeAmount; $grandGradeAmount+= $grade*$gradeAmount;?></td>
            <td><?php echo ($grade*$gradeAmount)+$salary; $grandTotalSalary+=($grade*$gradeAmount)+$salary ?></td>

           <!--  <td><?php //echo $pfc = $model->getPfc(time()) * $salary/100; ?></td>

 -->
 <td><?php
     $salaryIndividual=$salary + ($grade * $gradeAmount);

        
             $allowancePf=$model->isPermanentGovernment($special)?($model->getPf(time())/100) * $salaryIndividual:0; 
             echo $allowancePf; 
             $grandPFTotal+= $allowancePf;

             $monthlyPayroll->allowance_pf=$allowancePf;
             ?></td>
             <?php 

             $increment=1;
             foreach($allowances as $allowanceDb){ ?>
            <td>
            <?php 

                    $string=$allowanceDb->allowanceName;
                    $allowance="allowance".$increment;
                    $$allowance=strtolower(str_replace(' ', '_',$string));
                    $allowanceAmount="allowanceAmount".$increment;
                    $$allowanceAmount=$model->getAllowanceNumber($staff->staff_id, $allowanceDb->allowanceId,$special);
                    $monthlyPayroll->$$allowance=$$allowanceAmount;
                    $increment++;

                    echo $all = $model->getAllowanceNumber($staff->staff_id, $allowanceDb->allowanceId,$special); 
                    $gtotal += $all;
            ?>

        </td>
            <?php }$gtotal+=$allowancePf; ?>

            <td><?php 
          
            echo $insurance=$model->isPermanentGovernment($special)?200:0;
           
            $grandInsuranceTotal+=$insurance;
            $monthlyPayroll->insurance_add=$insurance;

            ?></td>
            <td>

            <?php 
            echo $gsalary = $insurance + $gtotal + $salary + ($grade * $gradeAmount); 
            $grandGrossTotal+=$gsalary;
            $monthlyPayroll->gross_total=$gsalary;
            $gTotalSalary+=$gsalary;
            ?>

            </td>
            

            <td><?php 
            $deductionPf=$model->isPermanentGovernment($special)?(2*($model->getPf(time())/100)) *  $salaryIndividual:0;
            $grandPFDeductionTotal += $deductionPf;
            echo  $deductionPf;
            $monthlyPayroll->deduction_pf=$deductionPf;
            ?>
            </td>
            
            <td>
                <?php

                 echo $deductInsurance=$model->isPermanentGovernment($special)?200*2:0;
                
                $grandInsuranceDeductionTotal += $deductInsurance;
                $monthlyPayroll->deduction_insurance=$deductInsurance;

                ?>
            </td>


           <?php 
           $increment=1;
           foreach($deductions as $deductionDb){ ?>
            <td>

            <?php 

                        $string=$deductionDb->name;
                        $deduction="deduction".$increment;
                        $$deduction=strtolower(str_replace(' ', '_',$string));
                        $deductionAmount="deductionAmount".$increment;
                        $$deductionAmount=$model->getDeductionNumber($staff->staff_id, $deductionDb->id,$special);
                        $monthlyPayroll->$$deduction=$$deductionAmount;
                        $increment++;

                        echo $deduction = $model->getDeductionNumber($staff->staff_id, $deductionDb->id,$special); 

                        $deductionTotal += $deduction;
            ?>

        </td>

            <?php }  $deductionTotal += $deductionPf + $deductInsurance;?>

            <td><?php echo $totalClass=$staff->getPartTimeClass($special);

            $monthlyPayroll->part_time_class=$totalClass;
            ?></td>

            <td><?php echo $classAmount=$staff->getPartTimeAmount($special); 
                    $partTimeAmount=$totalClass * $classAmount;
                     $monthlyPayroll->part_time_class_amount=$staff->getPartTimeAmount($special);


            ?></td>
            <td>
            <?php 

            echo $nsalary = ($gsalary - $deductionTotal) + $partTimeAmount; $nTotalSalary+=$nsalary;
            $grandNetTotal += $nsalary;
            $monthlyPayroll->net_salary=$nsalary;
            ?>
            </td>
            <!-- <td><?php //echo $pfc = $model->getPf(time()) * $salary/100;?></td>
            <td><?php //echo $selfpf = $model->getSelfPf($staff->staff_id, time());?></td>
            <td><?php //echo $tds = $model->getTdsRate($staff->staff_id, $gsalary); ?></td>
            <td><?php //echo $advance = 0; ?></td>

            <td><?php //echo $absent = $model->getAbsentDays($staff->staff_id, time());?></td>
            <td><?php //echo $others = 0; ?></td>
            <td><?php //$tdeduction = $selfpf + $tds + $advance + $absent + $others; echo $tdeduction;?></td> -->
            <td><?php 

           // echo $yearlyIncome=$nsalary*12 +($allowancePf*12);
            echo $yearlyIncome=$nsalary*12;

            $grandYearlyTotal += $yearlyIncome; 
            $monthlyPayroll->yearly_total=$yearlyIncome;

            ?></td>
            <td><?php 
            $tax=UtilityFunctions::taxAmount($gsalary,$nsalary,$yearlyIncome,$staff,$special);
            $grandMonthlyTaxTotal += round($tax/12,3);
            echo round($tax/12,3);
            $monthlyPayroll->monthly_tax=round($tax/12,3);
            ?></td>
           <!--  <td><?php //echo $nsalary = $nsalary - $tdeduction; $ntotal += $nsalary; ?></td> -->
             <td><?php echo $nPayment=$nsalary-round($tax/12,3); $nTotalPayment+=$nPayment;
             $monthlyPayroll->net_payment=$nPayment;

             ?></td>

        </tr>
        <?php 


            if(isset($save) && $save)
            {

               
                saveMonthlySalary($staff,$save,$monthlyPayroll,$special);
             

                
            }



        }  }?>




          <?php 
       
        if(!empty($staffs)){ 
        
  
        foreach ($staffs as $staff) { 
           
            
            ?>

        <tr>
        <?php    
        if(isset($saveMonthlySalary))
        {
         ?>
         <td></td>
         <?php  } ?>
            <td><?php echo 'Total' ?></td>
            <td><?php echo '';?></td>

            <td><?php '' ?></td>
            <td><?php echo $grandBasicTotal; ?></td>
            <td><?php echo '';?></td>
             <td><?php echo ''; ?></td>
             <td><?php echo $grandGradeAmount ?></td>
              <td><?php echo $grandTotalSalary ?></td>
            <td><?php echo $grandPFTotal; ?></td>
             <?php foreach($allowances as $allowance){ ?>
            <td><?php /*echo $all = $model->getAllowances($staff->staff_id, $allowance->allowanceId,  time()) * $salary/100 ; 
            $gtotal += $all;*/
            ?>
            <?php echo ''; 
            
            ?>

            </td>
            <?php } ?>
            <td><?php 
            echo $grandInsuranceTotal; ?></td>
            <td><?php echo $grandGrossTotal; ?></td>
            <td><?php echo $grandPFDeductionTotal; ?></td>
             <td><?php echo $grandInsuranceDeductionTotal; ?></td>
           <?php foreach($deductions as $deduction){ ?>
            <td>
            <?php echo '';
            ?>

            </td>

            <?php } ?>
            <td></td>
            <td></td>
            <td><?php echo $grandNetTotal; ?></td>
             <td><?php echo $grandYearlyTotal; ?></td>
             <td><?php echo $grandMonthlyTaxTotal; ?></td>
           
           <!--  <td><?php //echo $nsalary = $nsalary - $tdeduction; $ntotal += $nsalary; ?></td> -->
             <td><?php echo $nTotalPayment; ?></td>
  
        </tr>
        <?php break; } }?>
           
    </tbody>
</table>



<form action="" method="POST">
<?php
if(isset($departmentDropdown))
{ 

         echo CHtml::hiddenfield('print',$depart);
        $this->widget('bootstrap.widgets.TbButton', array(
            'label'=>'PDF',
            'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'size'=>'small', // null, 'large', 'small' or 'mini'
            'htmlOptions'=>array('id'=>'submit'),
        )); 

}

elseif(isset($specialDropdown))
{  
/* echo CHtml::hiddenfield('print',$special);
 $this->widget('bootstrap.widgets.TbButton', array(
    'label'=>'EXPORT TO PDF',
    'buttonType'=>'submit', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'size'=>'small', // null, 'large', 'small' or 'mini'
    'htmlOptions'=>array('id'=>'submit'),
));  */

}



?>
</form>

<?php 

        function saveMonthlySalary($staff,$save,$monthlyPayroll,$special)
        {
             if(isset($_POST['box']))
                {
                    if(in_array($staff->staff_id,$_POST['box']))
                    {
                
                                if($save == true)
                                    {
                                            $monthlyPayroll->created_date=date('Y-m-d');
                                            $monthlyPayroll->month=$_POST['month'];
                                            $monthlyPayroll->year=$_POST['year']; 
                                            $monthlyPayroll->department_id=$special;
                                            if(MonthlyPayroll::checkExistingData($_POST['month'],$_POST['year'],$staff->staff_id,$special))
                                            {
                                               
                                                    if($monthlyPayroll->save())
                                                    {
                                                        Yii::app()->user->setFlash('success','Monthly Salary Saved.');
                                                        $departments=$staff->departmentList();
                                                        if($departments != null)
                                                        {
                                                            foreach ($departments as $key) 
                                                            {
                                                                $departmentPayroll=new DepartmentPayroll();
                                                                $departmentPayroll->payroll_id=$monthlyPayroll->id;
                                                                $departmentPayroll->department_id=$key->department_id;
                                                                $departmentPayroll->save();


                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        print_r($monthlyPayroll->errors); exit;
                                                    }
                                             }
                                             else
                                             {
                                                   Yii::app()->user->setFlash('error','This month salary has already been saved.');
                                             }
                                    }

                    }

            }


        }



?>
