<?php
$baseUrl = Yii::app()->request->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl . '/js/registration/jquery.create.js');
$cs->registerScriptFile($baseUrl . '/js/registration/jquery.validation.js');

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'personaldetails-form-form',
    'type' => 'horizontal',
    'htmlOptions' => array(
        'class' => 'form-horizontal well',
    ),
    //'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'focus' => array($model, 'full_name'),
    ));


?>
<style>
    .custom-error, .span12.custom-error{
        border: 1px solid red;
    }
    .table-bordered input.custom-error,.table-bordered input.custom-error:focus,.custom-error:focus,.span12.custom-error:focus{
        border: 1px solid red;
    }
    .table-bordered input,.table-bordered select, .table-bordered textArea {
        width: 130px;
    }
</style>
<!-- form -->
<div class="row-fluid">
    <div class="span12">
        
            <?php echo $form->errorSummary($model); ?>
            <div class="box-title"><p class="help-block">Fields with <span class="required">*</span> are required.</p></div>
            <div class="box-content nopadding">
                <div class="row">
                    <div class="span6 control-group" >
                        <div class="control-group">
                            <h3 class="text-center"> Personal Details: </h3>
                        </div>
                        

                        <?php echo $form->textFieldRow($staff, 'fname', array('class'=>'span12', 'placeholder'=>'First Name')) ?>

                      
                        <?php echo $form->textFieldRow($staff, 'lname', array('class'=>'span12', 'placeholder'=>'Last Name')) ?>
                        

                        <?php echo $form->radioButtonListRow($model, 'gender', $data = array("male" => "male", "female" => "female", "other" => "other"), $rowOptions = array('checked' => false)); ?>

                        <?php echo $form->dropDownListRow($model, 'caste', CHtml::listData(EthinicGroup::model()->findAll(), 'title', 'title'), array('prompt' => 'Select Ethinic Group', 'class' => 'span12')); ?>

                        <?php echo $form->dropDownListRow($model, 'nationality', array("nepali" => "nepali", "others" => "others"), array('class' => 'span12')); ?>

                        <?php echo $form->textFieldRow($model, 'date_of_birth', array('class' => 'span12 date', 'autocomplete' => false, 'data-mask-clearifnotmatch' => "true")); ?>

                        <?php echo $form->textFieldRow($model,'citizenship_no',array('size'=>35,'maxlength'=>35, 'style'=>'Width:332px;')); ?>

                        <?php echo $form->dropDownListRow($model, 'issue_district', CHtml::listData(Location::model()->findAll(array('condition'=>'location_slug=:location_slug','params'=>array(':location_slug'=>'ds'))), 'title', 'title'), array('prompt' => 'Select Issued District', 'class' => 'span12')); ?>

                        <?php echo $form->textFieldRow($model,'fathers_name',array('size'=>60,'maxlength'=>250, 'style'=>'Width:332px;')); ?>

                        <?php echo $form->textFieldRow($model,'mothers_name',array('size'=>60,'maxlength'=>250, 'style'=>'Width:332px;')); ?>
        
                        <?php echo $form->textFieldRow($model,'spouse_name',array('size'=>60,'maxlength'=>250, 'style'=>'Width:332px;')); ?>
        
                        <?php echo $form->textFieldRow($model,'will_person',array('size'=>60,'maxlength'=>250, 'style'=>'Width:332px;')); ?>
        
                        <?php echo $form->dropDownListRow($model, 'mother_tongue', CHtml::listData(MotherLangauge::model()->findAll(), 'id', 'title'), array('prompt' => 'Select Mother Tongue', 'class' => 'span12')); ?>
        
                        <?php echo $form->textFieldRow($model,'disability',array('size'=>10,'maxlength'=>10, 'style'=>'Width:332px;')); ?>
                        
                        <?php //echo $form->textFieldRow($model,'email',array('class' => 'span12', 'maxlength' => 100)); ?> 
                        
                        <?php //echo $form->textFieldRow($model,'contact_number',array('size'=>15,'maxlength'=>15, 'style'=>'Width:332px;')); ?>    
        
                    </div>
                    <div class="span6 control-group" >
                        <div class="control-group">
                            <h3 class="text-center"> Current Status Details: </h3>
                        </div>
                    
                        <?php echo $form->textFieldRow($model,'current_level',array('size'=>60,'maxlength'=>100)); ?>
                    
                        <?php echo $form->textFieldRow($model,'position',array('size'=>60,'maxlength'=>100)); ?>

                        <?php echo $form->textFieldRow($model,'rank',array('size'=>50,'maxlength'=>50)); ?>

                        <?php echo $form->textFieldRow($model,'teaching_language',array('size'=>60,'maxlength'=>100)); ?>

                        <?php echo $form->textFieldRow($model,'license_no',array('size'=>55,'maxlength'=>55)); ?>   

                        <?php echo $form->textFieldRow($model,'insurance_no',array('size'=>50,'maxlength'=>50)); ?>
        
                        <?php echo $form->textFieldRow($model,'pf_ac_no',array('size'=>50,'maxlength'=>50)); ?>
        
                        <?php echo $form->textFieldRow($model,'trk_no',array('size'=>50,'maxlength'=>50)); ?>
        
                        <?php echo $form->textFieldRow($model,'bank_name',array('size'=>60,'maxlength'=>250)); ?>
        
                        <?php echo $form->textFieldRow($model,'bank_ac_no',array('size'=>50,'maxlength'=>50)); ?>   
                    </div>                  
                    </div>
                    <h1>Additional Information</h1>
                    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>true, // display a larger alert block?
        'fade'=>true, // use transitions?
        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
        'alerts'=>array( // configurations per alert type
            'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
            'info'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
            'error'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
            'warning'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
            'danger'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
        ),
    )); ?>

     <table class="table">


<tr>
    <td><?php echo $form->labelEx($staff, 'address', array('class'=>'')) ?></td>
    <td><?php echo $form->textField($staff, 'address', array('class'=>'span7', 'placeholder'=>'Address')) ?></td>
    <td><?php echo $form->labelEx($staff, 'contact', array('class'=>'')) ?></td>
    <td><?php echo $form->textField($staff, 'contact', array('class'=>'span7', 'placeholder'=>'Contact')) ?></td>
</tr>

<tr>
     <td><?php echo $form->labelEx($staff, 'marital_status', array('class'=>'')) ?></td>
    <td><?php echo $form->dropDownList($staff, 'marital_status', array(''=>'--Marital Status', 'Single'=>'Single', 'Married'=>'Married', 'Divorced'=>'Divorced'), array('class'=>'span7')); ?></td>
    <td><?php echo $form->labelEx($staff, 'email', array('class'=>'')) ?></td>
    <td><?php echo $form->textField($staff, 'email', array('class'=>'span7',  'placeholder'=>'Email')) ?></td>
</tr>

<tr>
    <td><?php echo $form->labelEx($staff,'join_date'); ?></td>
    <td><?php
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model' => $staff,
                        'attribute' => 'join_date',
                        // additional javascript options for the date picker plugin
                        'options' => array(
                            'showAnim' => 'drop',
                            'dateFormat' => 'yy-mm-dd',
                            'changeYear' => true,
                            'changeMonth' => true,
                            'yearRange' => '1900:',
                        ),
                        'htmlOptions' => 
                        array('class' => 'span7', 'placeholder'=>'Join date', 'value'=>date('Y-m-d', time())),
                    ));
                    ?>
        
    </td>
    <td><?php echo $form->labelEx($staff, 'role'); ?></td>
   <td><?php echo $form->dropDownList($staff, 'role', array('staff'=>'Staff'), array('class'=>'span7', 'prompt'=>'--Select user role--')) ?></td>
</tr>
<tr>
<td>

        <?php  


        echo $form->labelEx($officeTime, 'start_time'); ?></td>

        <td><?php 
$this->widget( 'application.extensions.timepicker.EJuiDateTimePicker', array(
    'model'     => $officeTime,
    'attribute' => 'start_time',
    'options'   => array(
        'timeOnly'   => true,
        'showHour'   => true,
        'showMinute' => true,
        'timeFormat' => 'hh:mm'
    ),
    'htmlOptions'=>array(
                        'value'=>(isset($officeTime->start_time) ? $officeTime->start_time : date('H:i', time())),
                        'style'=>'height:20px;','placeholder'=>'Office start time', 'class'=>'span7'
                    ),
) ); ?></td>
    

    
        <td><?php echo $form->labelEx($officeTime,'end_time'); ?></td>
        <td><?php 
$this->widget( 'application.extensions.timepicker.EJuiDateTimePicker', array(
    'model'     => $officeTime,
    'attribute' => 'end_time',
    'options'   => array(
        'timeOnly'   => true,
        'showHour'   => true,
        'showMinute' => true,
        'timeFormat' => 'hh:mm'
    ),
    'htmlOptions'=>array(
                        'value'=>(isset($officeTime->end_time) ? $officeTime->end_time : date('H:i', time())),
                        'style'=>'height:20px;','placeholder'=>'Office end time', 'class'=>'span7'
                    ),
) ); ?></td></tr>


    <tr>
    <td><?php echo $form->labelEx($departmentStaff,'department_id'); ?></td>
    <td><?php echo $form->dropDownList($departmentStaff, 'department_id', $array1, array('multiple'=>'true','class'=>'span7')); ?></td>
    </tr>


    <tr>
    <td><?php echo $form->labelEx($staff,'level_id'); ?></td>
    <td><?php echo $form->dropDownList($staff, 'level_id', $array2, array('class'=>'span7')); ?></td>
    <td><?php echo $form->dropDownList($staff, 'level_id', $array3, array('class'=>'span7')); ?></td>
    <td><?php echo $form->dropDownList($staff, 'shreni_id', $array4, array('class'=>'span7')); ?></td>
    </tr>

     <tr>
    <td><?php echo $form->labelEx($staffSpecial,'special_case_id'); ?></td>
    <td><?php echo $form->dropDownList($staffSpecial, 'special_case_id', $array5, array('class'=>'span7')); ?></td>
    
   
   
    </tr>

     <tr>
    <td><?php echo $form->labelEx($staff,'grade'); ?></td>
    <td><?php echo $form->dropDownList($staff, 'grade', array('1'=>'1','2'=>'2','3'=>'3','4'=>'4'), array('class'=>'span7')); ?></td>
    <td><?php echo $form->labelEx($staff,'grade_amount'); ?></td>
     <td><?php echo $form->textField($staff, 'grade_amount', array('class'=>'span7')); ?></td>
    
   
   
    </tr>

    <tr>
    <td><?php echo $form->labelEx($staff,'basic_salary'); ?></td>
     <td><?php echo $form->textField($staff, 'basic_salary', array('class'=>'span7')); ?></td>


      <td><?php echo $form->labelEx($staff, 'teacher_type', array('class'=>'')) ?></td>
    <td><?php echo $form->dropDownList($staff,'teacher_type',array('1'=>'Full Time','2'=>'Part-Time','3'=>'Full/Part Time')); ?></td>?></td>

    </tr>
    <tr>


  <td><h1><?php echo $form->labelEx($staffAllowance,'percentage'); ?></h1><td/>
  </tr>
    <?php  
    $allowance=Allowances::model()->findAll();
    foreach($allowance as $name)
    {
    ?>
    <tr>
    <td><?php echo $name->allowanceName;?></td>
     <td><?php echo $form->textField($staffAllowance, "allowanceAmounts[$name->allowanceId]"); ?></td>
</tr>

   <?php }?>
<tr>


  <td><h1><?php echo $form->labelEx($staffDeduction,'amount'); ?></h1><td/>
  </tr>

    <?php  
    $deduction=Deductions::model()->findAll();
    foreach($deduction as $name)
    {
    ?>
    <tr>
    <td><?php echo $name->name;?></td>
     <td><?php echo $form->textField($staffDeduction, "deductionAmounts[$name->id]"); ?></td>
</tr>

   <?php }?>
</table>    


                <div class="form-actions">
                            <?php
                            $this->widget('bootstrap.widgets.TbButton', array(
                                'buttonType' => 'submit',
                                'type' => 'primary',
                                'label' => $model->isNewRecord ? 'Create' : 'Save',
                                'htmlOptions' => array(
                                    'id' => "create-button",
                                    'name' => 'createButton',
                                    'onClick' => 'return validateForm();'
                            )));
                            $this->endWidget();
                            ?>
                    </div>
            </div>
        
    </div>
</div>

