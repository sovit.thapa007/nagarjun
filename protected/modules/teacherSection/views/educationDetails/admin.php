<?php
/* @var $this EducationDetailsController */
/* @var $model EducationDetails */

$this->breadcrumbs=array(
	'Education Details'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List EducationDetails', 'url'=>array('index')),
	array('label'=>'Create EducationDetails', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#education-details-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Education Details</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'education-details-grid',
	'type' => 'striped bordered',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
                        'header'=>'№',
                        'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                        'htmlOptions'=>array(
                                'width'=>'25',
                                'class'=>'centered'
                        )
                ),
		//'personalDetailsid',
		array('name'=>'personalDetailsid',
		'value'=>'Personaldetails::model()->findByPk($data->pdetail_id)->full_name',
		'filter'=>CHtml::listData(Personaldetails::model()->findAll(),'id','full_name'),
		),
		'qualification',
		'board_university',
		'year',
		'stream',
		/*
		'education_course',
		'division',
		*/
		array(
					'header'=>'Action',
					'class' => 'bootstrap.widgets.TbButtonColumn',
				),
	),
)); ?>
