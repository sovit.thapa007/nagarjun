<?php
/* @var $this EducationDetailsController */
/* @var $model EducationDetails */

$this->breadcrumbs=array(
	'Education Details'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EducationDetails', 'url'=>array('index')),
	array('label'=>'Manage EducationDetails', 'url'=>array('admin')),
);
?>

<h1>Create EducationDetails</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>