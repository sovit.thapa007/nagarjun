<?php
$baseUrl = Yii::app()->request->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl . '/js/registration/jquery.create.js');
$cs->registerScriptFile($baseUrl . '/js/registration/jquery.validation.js');

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'education-details-form',
	'type' => 'horizontal',
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
	),
	//'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
	),
	'focus' => array($model, 'full_name'),
	));
?>

<style>
	.custom-error, .span12.custom-error{
		border: 1px solid red;
	}
	.table-bordered input.custom-error,.table-bordered input.custom-error:focus,.custom-error:focus,.span12.custom-error:focus{
		border: 1px solid red;
	}
	.table-bordered input,.table-bordered select, .table-bordered textArea {
		width: 130px;
	}
</style>
<!-- form -->
<div class="row-fluid">
	<div class="span12">
		<div class="box alternate">
			<?php echo $form->errorSummary($model); ?>
            <div class="box-title"><p class="help-block">Fields with <span class="required">*</span> are required.</p></div>
            <div class="box-content nopadding">
                <div class="row">
                	<div class="control-group" >
                		<div class="control-group">
							<h3 class="text-center"> Teacher Education Details: </h3>
	                    </div>

						<div class="row">

							<?php echo $form->dropDownListRow($model, 'personalDetailsid', CHtml::listData(PersonalDetails::model()->findAll(), 'id', 'full_name'), array('prompt' => 'Select Teacher', 'class' => 'span12')); ?>
						
							<?php echo $form->textFieldRow($model,'qualification',array('size'=>25,'maxlength'=>25,'class'=>'span12')); ?>
							
							<?php echo $form->textFieldRow($model,'board_university',array('size'=>60,'maxlength'=>100,'class'=>'span12')); ?>

							<?php echo $form->textFieldRow($model,'year',array('size'=>10,'maxlength'=>10,'class'=>'span12')); ?>
							
							<?php echo $form->textFieldRow($model,'stream',array('size'=>25,'maxlength'=>25,'class'=>'span12')); ?>
							
							<?php echo $form->radioButtonListRow($model, 'education_course', $data = array("Yes" => "Yes", "No" => "No"), $rowOptions = array('checked' => false)); ?>
							
							<?php echo $form->textFieldRow($model,'division',array('size'=>10,'maxlength'=>10,'class'=>'span12')); ?>
							
						</div>

						<div class="form-actions">
							<?php
							$this->widget('bootstrap.widgets.TbButton', array(
								'buttonType' => 'submit',
								'type' => 'primary',
								'label' => $model->isNewRecord ? 'Create' : 'Save',
								'htmlOptions' => array(
									'id' => "create-button",
									'name' => 'createButton',
									'onClick' => 'return validateForm();'
							)));
							$this->endWidget();
							?>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>				