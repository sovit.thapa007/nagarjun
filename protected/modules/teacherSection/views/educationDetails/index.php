<?php
/* @var $this EducationDetailsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Education Details',
);

$this->menu=array(
	array('label'=>'Create EducationDetails', 'url'=>array('create')),
	array('label'=>'Manage EducationDetails', 'url'=>array('admin')),
);
?>

<h1>Education Details</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
