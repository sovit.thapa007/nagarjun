<?php
/* @var $this EducationDetailsController */
/* @var $data EducationDetails */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('personalDetailsid')); ?>:</b>
	<?php echo CHtml::encode($data->personalDetailsid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qualification')); ?>:</b>
	<?php echo CHtml::encode($data->qualification); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('board_university')); ?>:</b>
	<?php echo CHtml::encode($data->board_university); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('year')); ?>:</b>
	<?php echo CHtml::encode($data->year); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stream')); ?>:</b>
	<?php echo CHtml::encode($data->stream); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('education_course')); ?>:</b>
	<?php echo CHtml::encode($data->education_course); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('division')); ?>:</b>
	<?php echo CHtml::encode($data->division); ?>
	<br />

	*/ ?>

</div>