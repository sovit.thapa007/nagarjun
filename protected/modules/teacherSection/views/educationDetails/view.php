<?php
/* @var $this EducationDetailsController */
/* @var $model EducationDetails */

$this->breadcrumbs=array(
	'Education Details'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List EducationDetails', 'url'=>array('index')),
	array('label'=>'Create EducationDetails', 'url'=>array('create')),
	array('label'=>'Update EducationDetails', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EducationDetails', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EducationDetails', 'url'=>array('admin')),
);
?>

<h1>View EducationDetails #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'personalDetailsid',
		'qualification',
		'board_university',
		'year',
		'stream',
		'education_course',
		'division',
	),
)); ?>
