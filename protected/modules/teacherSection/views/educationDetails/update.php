<?php
/* @var $this EducationDetailsController */
/* @var $model EducationDetails */

$this->breadcrumbs=array(
	'Education Details'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EducationDetails', 'url'=>array('index')),
	array('label'=>'Create EducationDetails', 'url'=>array('create')),
	array('label'=>'View EducationDetails', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EducationDetails', 'url'=>array('admin')),
);
?>

<h1>Update EducationDetails <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>