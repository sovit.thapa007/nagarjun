<?php
$this->breadcrumbs=array(
	'Billing Section' => Yii::app()->getModule('billingsection')->getBaseUrl(),
	'Manage',
);

$this->menu=array(
array('label'=>'List RegistrationHistory','url'=>array('index')),
array('label'=>'Create RegistrationHistory','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('registration-history-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'type' => 'horizontal',
)); ?>


<div class="row-fluid">
	<div class="span6">
		<?php
		echo $form->dropDownListRow($model, 'level_id', CHtml::listData(SchoolLevel::model()->findAll(), 'id', 'title'), array('prompt' => 'Select Level', 'class' => 'span12', 'id' => 'school_level', 'data-url' => Yii::app()->createUrl(ClassInformation::PRGURL)), array('label' => 'Level'));
		?>
	</div>
	<div class="span6">
		<?php echo $form->dropDownListRow($model, 'program_id', array(), ['prompt' => 'Select Program', 'id' => 'program_level', 'class' => 'span12', 'data-url' => Yii::app()->createUrl(ClassInformation::CLSURL)], ['label' => 'Program Level']); ?></div>
	
</div>
<div class="row-fluid">
	<div class="span6">
			<?php echo $form->dropDownListRow($model, 'class_id', array(), ['prompt' => 'Select Class', 'id' => 'class_id', 'class' => 'span12', 'data-url' => Yii::app()->createUrl('/result/ClassTerminal/TerminalClass')], ['label' => 'Class']); ?></div>
		<div class="span6">
			<?php echo $form->textFieldRow($model,'nepali_year',array('class'=>'span12')); ?></div>
</div>
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>


<?php
	if($model->class_id && $model->nepali_year){
		?>
<h3 class="text-center">STUDENT LIST</h3>
	<h5 class="text-center"><?= "CLASS : ".$classTitle; ?></h5>
		<div class="row-fluid">
			<form action="#" method="POST">
	        	<button class="btn btn-primary" name="student_list" id="student_list">Export(Excel)</button>
	    	</form>
		</div>
		<?php

	 $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'registration-history-grid',
	'dataProvider'=>$model->search(),
	'type' => 'bordered',
	'columns'=>array(

	       [
	       'header'=>'SN',
	       'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
	       ],
			[
				'header'=>'Student Name',
				'name'=>'reg_id',
				'value'=>'isset($data->registration) ? $data->registration->name : " "',

			],
			[
				'header'=>'Class Section',
				'name'=>'section_id',
				'value'=>'isset($data->classSection) ? $data->classSection->title : " "',

			],
			'roll_number',
			[
				'name'=>'reg_type',
				'value'=>'$data->reg_type == "registration" ? "Fresh Admission" : "Re-Admission" ',
				'filter' => array("registration"=>"Fresh Admission","re_registration"=>"Re-Admission"),
			],
	),
	)); 
}


if(isset($_POST['student_list']) && !empty($model->class_id && $model->nepali_year))
{
    $this->widget('EExcelView', array(
    		'dataProvider'=>$model->search(),
			'columns'=>array(
					[
						'name'=>'school_id',
						'value'=>'20015',
					],
					[
						'name'=>'nepali_year',
						'header'=>'Year',
					],
					'reg_id',
					[
						'header'=>'First Name',
						'value'=>'isset($data->registration) && isset($data->registration->name) && explode(" ", $data->registration->name) ? explode(" ", $data->registration->name)[0] : " "',

					],
					[
						'header'=>'Last Name',
						'value'=>'isset($data->registration) && explode(" ", $data->registration->name) ? explode(" ", $data->registration->name)[1] : " "',

					],
					[
						'header'=>'Gender',
						'name'=>'sex',

					],
					[
						'header'=>"Father's Name",
						'name'=>'father_name',

					],
					[
						'header'=>"Mother's Name",
						'name'=>'mother_name',

					],
					'nepalI_dob',
					'english_dob',
					'ethinic_id',
					'disability_id',
					'ecd',
					[
						'name'=>'photo_link',
						'header'=>'Photo',
					],
					[
						'name'=>'reg_type',
						'value'=>'$data->reg_type == "registration" ? "Fresh Admission" : "Re-Admission" ',
						'filter' => array("registration"=>"Fresh Admission","re_registration"=>"Re-Admission"),
					],
			),
            'grid_mode'=>'export',
            'title'=>'Result Legder',
            'filename'=>$classTitle.'-'.$academic_year.'-('.date('Y-m-d').')',
            'stream'=>true,
        	'autoWidth'=>false,
            'exportType'=>'Excel2007',
        ) );

}
?>


<script type="text/javascript">
    $('body').on('change', '#school_level', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        if (selected.length > 0) {
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#program_level').html(response.option);
                    } else {
                        $('#program_level').html('');
                        $('#class_id').html('');
                    }
                }
            });

            var qual_url = $('#qualification_url').val();
			if (typeof qual_url != "undefined"){
				var url_2 = qual_url + '/key/' + selected;
				$.ajax({
					type: 'POST',
					url: url_2,
					dataType: 'JSON',
					success: function(response) {
						$('#qualification_details_1').html(response.view);
					}
				});
			}
        }
    });


    $('body').on('change', '#program_level', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        if (selected.length > 0) {
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#class_id').html(response.option);
                    } else {
                        $('#class_id').html('');
                    }
                }
            });
        }
    });


    $('body').on('change', '#class_id', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        if (selected.length > 0) {
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#termnial_id').html(response.option);
                    } else {
                        $('#termnial_id').html('');
                    }
                }
            });
        }
    });
</script>
