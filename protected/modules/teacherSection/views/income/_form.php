<?php
$baseUrl = Yii::app()->request->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl . '/js/registration/jquery.create.js');
$cs->registerScriptFile($baseUrl . '/js/registration/jquery.validation.js');

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'income-form',
	'type' => 'horizontal',
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
	),
	//'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
	),
	'focus' => array($model, 'personalDetailsid'),
	));
?>

<style>
	.custom-error, .span12.custom-error{
		border: 1px solid red;
	}
	.table-bordered input.custom-error,.table-bordered input.custom-error:focus,.custom-error:focus,.span12.custom-error:focus{
		border: 1px solid red;
	}
	.table-bordered input,.table-bordered select, .table-bordered textArea {
		width: 130px;
	}
</style>
<!-- form -->
<div class="row-fluid">
	<div class="span12">
		
			<?php echo $form->errorSummary($model); ?>
            <div class="box-title"><p class="help-block">Fields with <span class="required">*</span> are required.</p></div>
            <div class="box-content nopadding">
                <div class="row">
                	<div class="control-group" >
                		<div class="control-group">
							<h3 class="text-center"> Teacher Income Details: </h3>
	                    </div>

						<div class="row-fluid">

							<?php echo $form->dropDownListRow($model, 'personalDetailsid', CHtml::listData(Personaldetails::model()->findAll(), 'id', 'full_name'), array('prompt' => 'Select Teacher', 'class' => 'span11')); ?>

							<?php echo $form->textFieldRow($model,'monthly_salary',array('size'=>10,'maxlength'=>10,'class'=>'span11')); ?>

							<?php echo $form->textFieldRow($model,'grade_num',array('class'=>'span11')); ?>

							<?php echo $form->textFieldRow($model,'grade_amount',array('size'=>10,'maxlength'=>10,'class'=>'span11')); ?>
						</div>
						<div class="row-fluid">
							<div class="control-group">
								<h3 class="text-center"> Allowances: </h3>
		                    </div>

		                    <div class="row-fluid" >

								<?php echo $form->textFieldRow($model,'head_teacher',array('size'=>10,'maxlength'=>10,'class'=>'span11')); ?>

								<?php echo $form->textFieldRow($model,'dress',array('size'=>10,'maxlength'=>10,'class'=>'span11')); ?>

								<?php echo $form->textFieldRow($model,'festival',array('size'=>10,'maxlength'=>10,'class'=>'span11')); ?>

							</div>

							<div class="row-fluid" >

								<?php echo $form->textFieldRow($model,'insurance',array('size'=>10,'maxlength'=>10,'class'=>'span11')); ?>

								<?php echo $form->textFieldRow($model,'remote',array('size'=>10,'maxlength'=>10,'class'=>'span11')); ?>

								<?php echo $form->textFieldRow($model,'medicine',array('size'=>10,'maxlength'=>10,'class'=>'span11')); ?>

								<?php echo $form->textFieldRow($model,'mahangi',array('size'=>10,'maxlength'=>10,'class'=>'span11')); ?>

							</div>
						</div>
						<div class="row">
							<div class="control-group">
								<h3 class="text-center"> Funds: </h3>
		                    </div>	
							<div class="row-fluid" >
								<?php echo $form->textFieldRow($model,'provident_fund',array('size'=>10,'maxlength'=>10,'class'=>'span11')); ?>
							</div>
							<div class="row-fluid" >							
							<?php echo $form->textFieldRow($model,'citizen_investment',array('size'=>10,'maxlength'=>10,'class'=>'span11')); ?>
							</div>	
						</div>	

						<div class="form-actions">
							<?php
							$this->widget('bootstrap.widgets.TbButton', array(
								'buttonType' => 'submit',
								'type' => 'primary',
								'label' => $model->isNewRecord ? 'Create' : 'Save',
								'htmlOptions' => array(
									'id' => "create-button",
									'name' => 'createButton',
									'onClick' => 'return validateForm();'
							)));
							$this->endWidget();
							?>
						</div>

					</div>
				</div>
			</div>
		
	</div>
</div>	