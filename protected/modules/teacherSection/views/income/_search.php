<?php
/* @var $this IncomeController */
/* @var $model Income */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'personalDetailsid'); ?>
		<?php echo $form->textField($model,'personalDetailsid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'monthly_salary'); ?>
		<?php echo $form->textField($model,'monthly_salary',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'grade_num'); ?>
		<?php echo $form->textField($model,'grade_num'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'grade_amount'); ?>
		<?php echo $form->textField($model,'grade_amount',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'head_teacher'); ?>
		<?php echo $form->textField($model,'head_teacher',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dress'); ?>
		<?php echo $form->textField($model,'dress',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'festival'); ?>
		<?php echo $form->textField($model,'festival',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'insurance'); ?>
		<?php echo $form->textField($model,'insurance',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'remote'); ?>
		<?php echo $form->textField($model,'remote',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'medicine'); ?>
		<?php echo $form->textField($model,'medicine',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mahangi'); ?>
		<?php echo $form->textField($model,'mahangi',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'provident_fund'); ?>
		<?php echo $form->textField($model,'provident_fund',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'citizen_investment'); ?>
		<?php echo $form->textField($model,'citizen_investment',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->