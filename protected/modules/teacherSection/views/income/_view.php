<?php
/* @var $this IncomeController */
/* @var $data Income */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('personalDetailsid')); ?>:</b>
	<?php echo CHtml::encode($data->personalDetailsid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('monthly_salary')); ?>:</b>
	<?php echo CHtml::encode($data->monthly_salary); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('grade_num')); ?>:</b>
	<?php echo CHtml::encode($data->grade_num); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('grade_amount')); ?>:</b>
	<?php echo CHtml::encode($data->grade_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('head_teacher')); ?>:</b>
	<?php echo CHtml::encode($data->head_teacher); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dress')); ?>:</b>
	<?php echo CHtml::encode($data->dress); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('festival')); ?>:</b>
	<?php echo CHtml::encode($data->festival); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('insurance')); ?>:</b>
	<?php echo CHtml::encode($data->insurance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('remote')); ?>:</b>
	<?php echo CHtml::encode($data->remote); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('medicine')); ?>:</b>
	<?php echo CHtml::encode($data->medicine); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mahangi')); ?>:</b>
	<?php echo CHtml::encode($data->mahangi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('provident_fund')); ?>:</b>
	<?php echo CHtml::encode($data->provident_fund); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('citizen_investment')); ?>:</b>
	<?php echo CHtml::encode($data->citizen_investment); ?>
	<br />

	*/ ?>

</div>