<?php

/**
 * This is the model class for table "_grade_scale".
 *
 * The followings are the available columns in table '_grade_scale':
 * @property integer $id
 * @property string $_employee_level
 * @property integer $grade
 * @property string $amount
 * @property integer $status
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class BasicScale extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '_basic_scale';
	}

    public function behaviors() {
        return array(
            'ECompositeUniqueKeyValidatable' => array(
                'class' => 'ECompositeUniqueKeyValidatable',
                'uniqueKeys' => array(
                    'attributes' => '_employee_level, type, sherni_grade, status',
                    'errorAttributes' => '_employee_level',
                    'errorMessage' => 'यो विवरणा उपलब्ध छ|'
                )
            ),
        );
    }


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('*', 'compositeUniqueKeysValidator'),
			array('amount, _employee_level, type', 'required'),
			array('sherni_grade, status, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('_employee_level', 'length', 'max'=>28),
			array('amount', 'length', 'max'=>8),
			array('created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, _employee_level, sherni_grade, amount, status, created_by, created_date, updated_by, updated_date, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'_employee_level' => 'कर्मचारी तह',
			'type'=>'के ?',
			'sherni_grade' => 'श्रेणी/ग्रेड',
			'amount' => 'रु : ',
			'status' => 'स्थिति',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('_employee_level',$this->_employee_level,true);
		$criteria->compare('type', $this->type, true);
		$criteria->compare('sherni_grade',$this->sherni_grade);
		$criteria->compare('amount',$this->amount,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GradeScale the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
     * Validates composite unique keys
     *
     * Validates composite unique keys declared in the
     * ECompositeUniqueKeyValidatable bahavior
     */
    public function compositeUniqueKeysValidator() {
        $this->validateCompositeUniqueKeys();
    }
 	
 	public function getTypes(){
 		if($this->type == 'sherni')
 			return 'श्रेणी';
 		return 'ग्रेड';
 	}

    public function getSherniGradeTitle(){
    	if($this->type == 'sherni'){
    		if($this->sherni_grade == 1){return "प्रथम श्रेणी"; }
    		if($this->sherni_grade == 2){return "दोस्रो श्रेणी"; }
    		if($this->sherni_grade == 3){return "त्रितिय श्रेणी"; }
    		return '';
    	}
    	if($this->type == 'grade'){ return $this->sherni_grade ? UtilityFunctions::NepaliNumber($this->sherni_grade).' ग्रेड' : ''; }
    }

}
