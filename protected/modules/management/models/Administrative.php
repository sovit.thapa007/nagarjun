<?php

/**
 * This is the model class for table "_administrative".
 *
 * The followings are the available columns in table '_administrative':
 * @property integer $id
 * @property integer $magform_id
 * @property integer $school_id
 * @property string $school_level
 * @property string $amount
 * @property string $remarks
 * @property integer $status
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class Administrative extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '_administrative';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('school_id, school_level, amount', 'required'),
			array('magform_id, school_id, status, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('school_level', 'length', 'max'=>100),
			array('amount', 'length', 'max'=>12),
			array('remarks, created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, magform_id, school_id, school_level, amount, remarks, status, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'school' => array(self::BELONGS_TO, 'BasicInformation', 'school_id'),
			'magform' => array(self::BELONGS_TO, 'Magform', 'magform_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'magform_id' => 'बिल',
			'school_id' => 'बिद्यालय',
			'school_level' => 'बिद्यालयको तह ',
			'amount' => 'रकम',
			'remarks' => 'कैफियत',
			'status' => 'Status',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('magform_id',$this->magform_id);
		if(UtilityFunctions::ShowSchool())
			$criteria->compare('school_id',$this->school_id);
		else
			$criteria->compare('school_id',UtilityFunctions::SchoolID());
		$criteria->compare('school_level',$this->school_level,true);
		$criteria->compare('amount',$this->amount,true);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Administrative the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function beforeValidate(){
		if(!$this->isNewRecord){
			$this->status = 1;
			$this->created_by = Yii::app()->user->id;
			$this->created_date = date('Y-m-d H:i:s');
		}else{
			$this->updated_by = Yii::app()->user->id;
			$this->updated_date = date('Y-m-d H:i:s');
		}
		return parent::beforeValidate();
	}
}
