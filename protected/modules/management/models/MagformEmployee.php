<?php

/**
 * This is the model class for table "_magform_employee".
 *
 * The followings are the available columns in table '_magform_employee':
 * @property integer $id
 * @property integer $magform_id
 * @property integer $employee_id
 * @property string $employee_name
 * @property string $account_number
 * @property string $employee_post
 * @property string $fiscal_year
 * @property integer $chaumasik
 * @property string $amount
 * @property integer $status
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class MagformEmployee extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '_magform_employee';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('magform_id, employee_id, employee_name, chaumasik, amount, type', 'required'),
			array('magform_id, employee_id, chaumasik, status, created_by, updated_by, employee_sherni, employee_grade', 'numerical', 'integerOnly'=>true),
			array('employee_name', 'length', 'max'=>250),
			array('account_number', 'length', 'max'=>100),
			array('employee_post', 'length', 'max'=>28),
			array('employee_type', 'length', 'max'=>21),
			array('employee_level', 'length', 'max'=>18),
			array('fiscal_year', 'length', 'max'=>9),
			array('amount', 'length', 'max'=>12),
			array('level', 'length', 'max'=>28),
			array('created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, magform_id, employee_id, employee_name, account_number, employee_post, fiscal_year, chaumasik, amount, status, created_by, created_date, updated_by, updated_date, type, employee_post, employee_type, employee_sherni, employee_grade, ', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'employee' => array(self::BELONGS_TO, 'EmployeeDetail', 'employee_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'magform_id' => 'Magform',
			'employee_id' => 'Employee',
			'type'=>'Type',
			'employee_name' => 'Employee Name',
			'account_number' => 'Account Number',
			'employee_post' => 'Employee Post',
			'employee_level' => 'Employee Level',
			'employee_type' => 'Employee Type',
			'employee_sherni' => 'Employee Sherni',
			'employee_grade' => 'Employee Grade',
			'fiscal_year' => 'Fiscal Year',
			'chaumasik' => 'Chaumasik',
			'amount' => 'Amount',
			'status' => 'Status',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('magform_id',$this->magform_id);
		$criteria->compare('employee_id',$this->employee_id);
		$criteria->compare('type',$this->type, true);
		$criteria->compare('employee_name',$this->employee_name,true);
		$criteria->compare('account_number',$this->account_number,true);
		$criteria->compare('employee_post',$this->employee_post,true);
		$criteria->compare('employee_level',$this->employee_level);
		$criteria->compare('employee_type',$this->employee_type);
		$criteria->compare('employee_sherni',$this->employee_sherni);
		$criteria->compare('employee_grade',$this->employee_grade);
		$criteria->compare('fiscal_year',$this->fiscal_year,true);
		$criteria->compare('chaumasik',$this->chaumasik);
		$criteria->compare('amount',$this->amount,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MagformEmployee the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function beforeValidate(){
		if($this->isNewRecord){
			$this->status = 1;
			$this->created_by = Yii::app()->user->id;
			$this->created_date = date('Y-m-d H:i:s');
		}else{
			$this->updated_by = Yii::app()->user->id;
			$this->updated_date = date('Y-m-d H:i:s');
		}
		return parent::beforeValidate();
	}


	public function afterSave(){
		$criteria = new CDbCriteria;
		$criteria -> select = 'sum(amount) as amount';
		$criteria -> condition = 'magform_id=:magform_id AND status=:status';
		$criteria -> params = [':magform_id'=>$this->magform_id, ':status'=>1];
		$magform_employee = $this->find($criteria);
		$total_amount = $magform_employee ? $magform_employee->amount : 0;
		$magform_detail = Magform::model()->findByPk($this->magform_id);
		if(!empty($magform_detail)){
			$magform_detail->total_amount = $total_amount;
			if(!$magform_detail->update())
				return false;
		}
		return parent::afterSave();

	}

	public function getEmpNepaliName(){
		$employee_information = EmployeeDetail::model()->findByPk($this->employee_id);
		return $employee_information ? $employee_information->first_name_nepali.' '.$employee_information->middle_name_nepali.' '.$employee_information->last_name_nepali : '';
	}

	public function getEmployeeDetails(){
		$_magform_employee_detail = MagformEmployeeDetails::model()->find('magform_employee_id=:magform_employee_id AND status=:status',[':magform_employee_id'=>$this->id,':status'=>1]);
		return $_magform_employee_detail;
	}


}
