<?php

/**
 * This is the model class for table "_magform_employee_details".
 *
 * The followings are the available columns in table '_magform_employee_details':
 * @property integer $id
 * @property integer $magform_employee_id
 * @property integer $employee_id
 * @property string $type
 * @property integer $year
 * @property integer $month
 * @property integer $total_day
 * @property integer $sherni
 * @property integer $grade
 * @property string $basic_scale
 * @property string $basic_grade_scale
 * @property string $pf_amount
 * @property string $insurance_amount
 * @property string $posak_bhatta
 * @property string $bhatta
 * @property string $pra_a_bhatta
 * @property string $monthly_total
 * @property string $state
 * @property string $state_status
 * @property integer $status
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class MagformEmployeeDetails extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '_magform_employee_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('magform_employee_id, employee_id, year, month, total_day, monthly_total', 'required'),
			array('magform_employee_id, employee_id, year, month, total_day, sherni, grade, status, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>11),
			array('basic_scale, basic_grade_scale, pf_amount, insurance_amount, bhatta, pra_a_bhatta', 'length', 'max'=>9),
			array('posak_bhatta', 'length', 'max'=>8),
			array('monthly_total, state', 'length', 'max'=>12),
			array('state_status', 'length', 'max'=>10),
			array('created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, magform_employee_id, employee_id, type, year, month, total_day, sherni, grade, basic_scale, basic_grade_scale, pf_amount, insurance_amount, posak_bhatta, bhatta, pra_a_bhatta, monthly_total, state, state_status, status, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'employee' => array(self::BELONGS_TO, 'EmployeeDetail', 'employee_id'),
			'magformEmployee' => array(self::BELONGS_TO, 'MagformEmployee', 'magform_employee_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'magform_employee_id' => 'Magform Employee',
			'employee_id' => 'Employee',
			'type' => 'Type',
			'year' => 'Year',
			'month' => 'Month',
			'total_day' => 'Total Day',
			'sherni' => 'Sherni',
			'grade' => 'Grade',
			'basic_scale' => 'Basic Scale',
			'basic_grade_scale' => 'Basic Grade Scale',
			'pf_amount' => 'Pf Amount',
			'insurance_amount' => 'Insurance Amount',
			'posak_bhatta' => 'Posak Bhatta',
			'bhatta' => 'Bhatta',
			'pra_a_bhatta' => 'Pra A Bhatta',
			'monthly_total' => 'Monthly Total',
			'state' => 'State',
			'state_status' => 'State Status',
			'status' => 'Status',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('magform_employee_id',$this->magform_employee_id);
		$criteria->compare('employee_id',$this->employee_id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('year',$this->year);
		$criteria->compare('month',$this->month);
		$criteria->compare('total_day',$this->total_day);
		$criteria->compare('sherni',$this->sherni);
		$criteria->compare('grade',$this->grade);
		$criteria->compare('basic_scale',$this->basic_scale,true);
		$criteria->compare('basic_grade_scale',$this->basic_grade_scale,true);
		$criteria->compare('pf_amount',$this->pf_amount,true);
		$criteria->compare('insurance_amount',$this->insurance_amount,true);
		$criteria->compare('posak_bhatta',$this->posak_bhatta,true);
		$criteria->compare('bhatta',$this->bhatta,true);
		$criteria->compare('pra_a_bhatta',$this->pra_a_bhatta,true);
		$criteria->compare('monthly_total',$this->monthly_total,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('state_status',$this->state_status,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MagformEmployeeDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/***/
	public function beforeValidate(){
		if($this->isNewRecord){
			$this->status = 1;
			$this->created_by = Yii::app()->user->id;
			$this->created_date = date('Y-m-d H:i:s');
		}else{
			$this->updated_by = Yii::app()->user->id;
			$this->updated_date = date('Y-m-d H:i:s');
		}
		return parent::beforeValidate();
	}

	public function getNetSalary(){
		return $this->basic_scale + $this->grade*$this->basic_grade_scale;
	}


	public static function TraimasikTotalAmount($employee_id, $type, $year, $month_array){
		$criteria = new CDbCriteria;
		$criteria->select = 'sum(monthly_total) as monthly_total';
		$criteria -> condition = 'employee_id=:employee_id AND type=:type AND year=:year AND status=:status';
		$criteria -> params = [':employee_id'=>$employee_id, ':type'=>$type, ':year'=>$year, ':status'=>1];
		$criteria -> addInCondition('month', $month_array);
		$traimasikDetail = MagformEmployeeDetails::model()->find($criteria);
		return $traimasikDetail ? $traimasikDetail->monthly_total : 0;

	}
}
