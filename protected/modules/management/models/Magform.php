<?php

/**
 * This is the model class for table "_magform".
 *
 * The followings are the available columns in table '_magform':
 * @property integer $id
 * @property integer $school_id
 * @property string $fiscal_year
 * @property integer $chaumasik
 * @property string $type
 * @property string $header
 * @property string $content
 * @property string $total_amount
 * @property integer $status
 * @property string $state
 * @property string $state_status
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class Magform extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '_magform';
	}

    public function behaviors() {
        return array(
            'ECompositeUniqueKeyValidatable' => array(
                'class' => 'ECompositeUniqueKeyValidatable',
                'uniqueKeys' => array(
                    'attributes' => 'school_id, fiscal_year, chaumasik, type, status',
                    'errorAttributes' => 'fiscal_year',
                    'errorMessage' => 'यो विवरणा पहिला नै उपलब्ध छ|'
                )
            ),
        );
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('*', 'compositeUniqueKeysValidator'),
			array('school_id, fiscal_year, chaumasik, type', 'required'),
			array('school_id, chaumasik, status, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('fiscal_year', 'length', 'max'=>9),
			array('type', 'length', 'max'=>11),
			array('header', 'length', 'max'=>250),
			array('total_amount, state', 'length', 'max'=>12),
			array('state_status', 'length', 'max'=>10),
			array('created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, school_id, fiscal_year, chaumasik, type, header, content, total_amount, status, state, state_status, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'school' => array(self::BELONGS_TO, 'BasicInformation', 'school_id'),
			'user_sec' => array(self::BELONGS_TO, 'User', 'created_by'),
			'updated_sec' => array(self::BELONGS_TO, 'User', 'updated_by'),
			'updated_sec' => array(self::BELONGS_TO, 'User', 'updated_by'),
            'employee' => array(self::HAS_MANY, 'MagformEmployee', 'magform_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'school_id' => 'बिद्यालय',
			'fiscal_year' => 'आर्थिक वर्ष',
			'chaumasik' => 'त्रैमासिक',
			'type' => 'माग फारमको प्रकार',
			'header' => 'विषय',
			'content' => 'विवरण',
			'total_amount' => 'जम्मा रु.',
			'status' => 'स्थिति',
			'state' => 'अवस्था',
			'state_status' => 'अवस्था/स्थिति',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		if(UtilityFunctions::ShowSchool())
			$criteria->compare('school_id',$this->school_id);
		else
			$criteria->compare('school_id',UtilityFunctions::SchoolID());
		$criteria->compare('fiscal_year',str_replace("-", "/", $this->fiscal_year));
		$criteria->compare('chaumasik',$this->chaumasik);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('header',$this->header,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('total_amount',$this->total_amount,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('state_status',$this->state_status,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=> [
				'pageSize'=>40
			],
		));
	}


	/**
     * Validates composite unique keys
     *
     * Validates composite unique keys declared in the
     * ECompositeUniqueKeyValidatable bahavior
     */
    public function compositeUniqueKeysValidator() {
        $this->validateCompositeUniqueKeys();
    }
 	

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Magform the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	*
	*/
	public function MagformCounting($params){
		$fiscal_year = isset($params['fiscal_year']) ? $params['fiscal_year'] : null;
		$chaumasik = isset($params['chaumasik_value']) ? $params['chaumasik_value'] : null;
		$school_id = isset($params['school_id']) && UtilityFunctions::ShowSchool() ? $params['school_id'] : UtilityFunctions::SchoolID();
		$criteria = new CDbCriteria;
		$criteria -> select = 'count(*) as id, type, count(DISTINCT school_id) as school_id, fiscal_year, chaumasik, sum(total_amount) as total_amount ';
		$criteria -> condition = 'status=:status';
		$criteria -> params = [':status'=>1];
		if($fiscal_year){
			$criteria -> condition .= ' AND fiscal_year=:fiscal_year';
			$criteria -> params = array_merge($criteria->params, [':fiscal_year'=>$fiscal_year]);
			$criteria -> group = 'type,fiscal_year';
		}
		if($chaumasik){
			$criteria -> condition .= ' AND chaumasik=:chaumasik';
			$criteria -> params = array_merge($criteria->params, [':chaumasik'=>$chaumasik]);
			$criteria -> group = 'type,fiscal_year,chaumasik';
		}
		if($school_id){
			$criteria -> condition .= ' AND school_id=:school_id';
			$criteria -> params = array_merge($criteria->params, [':school_id'=>$school_id]);
			$criteria -> group = 'type,fiscal_year,chaumasik,school_id';
		}
		return $this->findAll($criteria);
	}

	public function SchoolListing($params){
		$fiscal_year = isset($params['fiscal_year']) ? $params['fiscal_year'] : null;
		$chaumasik = isset($params['chaumasik_value']) ? $params['chaumasik_value'] : null;
		$school_id = isset($params['school_id']) ? $params['school_id'] : null;
		$type = isset($params['type']) ? $params['type'] : null;
		$criteria = new CDbCriteria;
		$criteria -> select = 'count(*) as id, type, school_id, fiscal_year, chaumasik, sum(total_amount) as total_amount ';
		$criteria -> condition = 'status=:status';
		$criteria -> params = [':status'=>1];
		if($fiscal_year){
			$criteria -> condition .= ' AND fiscal_year=:fiscal_year';
			$criteria -> params = array_merge($criteria->params, [':fiscal_year'=>$fiscal_year]);
			$criteria -> group = 'school_id,fiscal_year';
		}
		if($chaumasik){
			$criteria -> condition .= ' AND chaumasik=:chaumasik';
			$criteria -> params = array_merge($criteria->params, [':chaumasik'=>$chaumasik]);
			$criteria -> group = 'school_id,fiscal_year,chaumasik';
		}
		if($type){
			$criteria -> condition .= ' AND type=:type';
			$criteria -> params = array_merge($criteria->params, [':type'=>$type]);
			$criteria -> group = 'school_id,fiscal_year,chaumasik';
		}
		return $this->findAll($criteria);
	}


	public function getTotal($records,$colName){        
        $total = $grand_total = 0.0;

        $criteria = new CDbCriteria();
        $criteria -> select = 'sum(total_amount) as total_amount';
		if(UtilityFunctions::ShowSchool())
			$criteria->compare('school_id',$this->school_id);
		else
			$criteria->compare('school_id',UtilityFunctions::SchoolID());
		$criteria->compare('fiscal_year',str_replace("-", "/", $this->fiscal_year));
		$criteria->compare('chaumasik',$this->chaumasik);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('header',$this->header,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('total_amount',$this->total_amount,true);
		$total_information = $this->find($criteria);
		$grand_total = $total_information ? $total_information->total_amount : 0;

        if(count($records) > 0){
            foreach ($records as $record) {
                    $total += $record->$colName;
            }
        }

        return UtilityFunctions::NepaliNumber($total).' &nbsp;&nbsp;&nbsp; ['.UtilityFunctions::NepaliNumber($grand_total).']';
    }

}
