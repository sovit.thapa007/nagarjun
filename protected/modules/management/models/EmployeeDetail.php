<?php

/**
 * This is the model class for table "_employee_detail".
 *
 * The followings are the available columns in table '_employee_detail':
 * @property integer $id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $first_name_nepali
 * @property string $middle_name_nepali
 * @property string $last_name_nepali
 * @property string $gender
 * @property string $type
 * @property string $level
 * @property string $post
 * @property integer $grade
 * @property string $date_of_birth_bs
 * @property string $date_of_birth_ad
 * @property string $qualification
 * @property integer $cast_id
 * @property integer $ethnicity_id
 * @property integer $maritual_status
 * @property integer $contact_number
 * @property string $temporary_address
 * @property string $permanent_address
 * @property string $temporary_assign_date
 * @property string $permament_assign_date
 * @property string $baduwa_miti
 * @property string $insurance_number
 * @property integer $pf_number
 * @property string $bank_name
 * @property string $bank_account_number
 * @property string $cif_number
 * @property string $remarks
 * @property integer $status
 * @property integer $school_id
 * @property string $created_date
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $updated_date
 */
class EmployeeDetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '_employee_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('first_name_nepali, last_name_nepali, temporary_address, school_id', 'required'),
			array('grade, cast_id, ethnicity_id, maritual_status, pf_number, status, school_id, created_by, updated_by, sherni', 'numerical', 'integerOnly'=>true),
			array('first_name, middle_name, last_name, first_name_nepali, middle_name_nepali, last_name_nepali, temporary_address, permanent_address, bank_name', 'length', 'max'=>200),
			array('gender', 'length', 'max'=>15),
			array('type', 'length', 'max'=>21),
			array('level', 'length', 'max'=>28),
			array('post', 'length', 'max'=>18),
			array('contact_number', 'length', 'max'=>10),
			array('date_of_birth_bs, insurance_number, bank_account_number, cif_number', 'length', 'max'=>50),
			array('qualification', 'length', 'max'=>100),
			array('date_of_birth_ad, temporary_assign_date, permament_assign_date, remarks, created_date, updated_date, baduwa_miti, pan_no', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, first_name, middle_name, last_name, first_name_nepali, middle_name_nepali, last_name_nepali, gender, type, level, post, grade, sherni, date_of_birth_bs, date_of_birth_ad, qualification, cast_id, ethnicity_id, maritual_status, contact_number, temporary_address, permanent_address, temporary_assign_date, permament_assign_date, insurance_number, pf_number, bank_name, bank_account_number, cif_number, remarks, status, school_id, created_date, created_by, updated_by, updated_date,baduwa_miti, pan_no', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ethinicity_sec' => array(self::BELONGS_TO, 'EthinicGroup', 'ethnicity_id'),
			'cast_sec' => array(self::BELONGS_TO, 'EthinicGroup', 'cast_id'),
			'school_sec' => array(self::BELONGS_TO, 'BasicInformation', 'school_id'),
			'user_sec' => array(self::BELONGS_TO, 'User', 'created_by'),
			'updated_sec' => array(self::BELONGS_TO, 'User', 'updated_by'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'first_name' => 'First Name',
			'middle_name' => 'Middle Name',
			'last_name' => 'Last Name',
			'first_name_nepali' => 'First Name Nepali',
			'middle_name_nepali' => 'Middle Name Nepali',
			'last_name_nepali' => 'Last Name Nepali',
			'gender' => 'लिङ्ग',
			'type' => 'प्रकार',
			'level' => 'तह',
			'post' => 'पद',
			'sherni' => 'श्रेणी',
			'grade' => 'ग्रेड',
			'date_of_birth_bs' => 'जन्म मिती(बि. स.)',
			'date_of_birth_ad' => 'जन्म मिती(ए. डि.)',
			'qualification' => 'योग्यता',
			'cast_id' => 'जात',
			'ethnicity_id' => 'जातीय',
			'maritual_status' => 'विवाहित/अविवाहित',
			'contact_number' => 'सम्पर्क नम्बर',
			'temporary_address' => 'अस्थायी ठेगाना',
			'permanent_address' => 'स्थाई ठेगाना',
			'temporary_assign_date' => 'अस्थाई नियुक्ती  मिति',
			'permament_assign_date' => 'स्थाई नियुक्ती मिति:',
			'baduwa_miti' => 'बडुवा मिति',
			'pan_no' => 'PAN नम्बर',
			'insurance_number' => 'बीमा नं.',
			'pf_number' => 'कर्मचारी संञ्चयकोष नं.',
			'bank_name' => 'बैंक',
			'bank_account_number' => 'खाता नम्बर',
			'cif_number' => 'नागरिक लगानी कोष नं.',
			'remarks' => 'कैफियत',
			'status' => 'अवस्था',
			'school_id' => 'बिद्यालय',
			'created_date' => 'Created Date',
			'created_by' => 'Created By',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('first_name_nepali',$this->first_name_nepali,true);
		$criteria->compare('middle_name_nepali',$this->middle_name_nepali,true);
		$criteria->compare('last_name_nepali',$this->last_name_nepali,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('level',$this->level,true);
		$criteria->compare('post',$this->post,true);
		$criteria->compare('grade',$this->grade);
		$criteria->compare('sherni',$this->sherni);
		$criteria->compare('date_of_birth_bs',$this->date_of_birth_bs,true);
		$criteria->compare('date_of_birth_ad',$this->date_of_birth_ad,true);
		$criteria->compare('qualification',$this->qualification,true);
		$criteria->compare('cast_id',$this->cast_id);
		$criteria->compare('ethnicity_id',$this->ethnicity_id);
		$criteria->compare('maritual_status',$this->maritual_status);
		$criteria->compare('contact_number',$this->contact_number);
		$criteria->compare('temporary_address',$this->temporary_address,true);
		$criteria->compare('permanent_address',$this->permanent_address,true);
		$criteria->compare('temporary_assign_date',$this->temporary_assign_date,true);
		$criteria->compare('permament_assign_date',$this->permament_assign_date,true);
		$criteria->compare('baduwa_miti',$this->baduwa_miti,true);
		$criteria->compare('insurance_number',$this->insurance_number,true);
		$criteria->compare('pf_number',$this->pf_number);
		$criteria->compare('bank_name',$this->bank_name,true);
		$criteria->compare('bank_account_number',$this->bank_account_number,true);
		$criteria->compare('cif_number',$this->cif_number,true);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('status',$this->status);
		if(UtilityFunctions::ShowSchool())
			$criteria->compare('school_id',$this->school_id);
		else
			$criteria->compare('school_id',UtilityFunctions::SchoolID());
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
            	'defaultOrder'=>'level,post ASC',
        	),
			'pagination'=>[
				'pageSize'=>50,
			]
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EmployeeDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getBasicSalary(){
		$criteria = new CDbCriteria;
		$criteria -> condition = '_employee_level=:_employee_level AND status=:status';
		$criteria -> params = [':_employee_level'=>$this->level, ':status'=>1];
		if($this->sherni){
			$criteria -> condition .= ' AND sherni_grade=:sherni_grade AND type=:type';
			$criteria -> params = array_merge($criteria->params,[':sherni_grade'=>$this->sherni, ':type'=>'sherni']);
		}
		$basic_scale = BasicScale::model()->find($criteria);
		return $basic_scale ? $basic_scale->amount : 0;
	}
	public function getBasicGradeSalary(){
		$basic_scale = GradeScale::model()->find('_employee_level=:_employee_level AND sherni=:sherni AND grade=:grade',[':_employee_level'=>$this->level,':sherni'=>$this->sherni,':grade'=>$this->grade]);
		return $basic_scale ? $basic_scale->amount : 0;
	}

	public function getSalary(){
		return $this->BasicSalary + $this->BasicGradeSalary;
	}

	public function getProvidentFund(){
		$pf_percentage = Yii::app()->params['pf_percentage'];
		$total_salary = $this->BasicSalary + $this->BasicGradeSalary;
		return $this->type == 'स्थायी' && $pf_percentage > 0 ? $pf_percentage*$total_salary/100  : 0;
	}

	public function getBonus(){
		$bonus = Yii::app()->params['bhatta'];
		if($this->post=='प्र.अ.')
			$bonus += Yii::app()->params['principal_bonus'];
		return $bonus; 
	}


	public function getMonthlySalary(){
		$monthly_salary = $this->Salary + $this->ProvidentFund + Yii::app()->params['insurance_amount'] + $this->Bonus;
		return $monthly_salary;
	}

	public function getThreeMonthlySalary(){
		$four_month_salary = 3*$this->MonthlySalary;
		return $four_month_salary;
	}


	public function LevelWiseEmployeeNumber($school_id){
		$criteria = new CDbCriteria;
		$criteria -> select = 'count(*) as id, level'; 
		$criteria -> condition  = 'status=:status AND school_id=:school_id';
		$criteria -> params = [':status'=>1, ':school_id'=>$school_id];
		$criteria -> group = 'level';
		return $this->findAll($criteria);
	}
	

}
