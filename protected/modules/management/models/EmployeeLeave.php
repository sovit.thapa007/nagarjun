<?php

/**
 * This is the model class for table "_employee_leave".
 *
 * The followings are the available columns in table '_employee_leave':
 * @property integer $id
 * @property integer $employee_id
 * @property integer $year
 * @property integer $month
 * @property integer $leave_day
 * @property integer $status
 * @property string $created_date
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $updated_date
 */
class EmployeeLeave extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '_employee_leave';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('employee_id, year, month, leave_day, status', 'required'),
			array('employee_id, year, month, leave_day, status, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, employee_id, year, month, leave_day, status, created_date, created_by, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'employee_id' => 'Employee',
			'year' => 'Year',
			'month' => 'Month',
			'leave_day' => 'Leave Day',
			'status' => 'Status',
			'created_date' => 'Created Date',
			'created_by' => 'Created By',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('employee_id',$this->employee_id);
		$criteria->compare('year',$this->year);
		$criteria->compare('month',$this->month);
		$criteria->compare('leave_day',$this->leave_day);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EmployeeLeave the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeValidate(){
		if($this->isNewRecord){
			$this->status = 1;
			$this->created_by = Yii::app()->user->id;
			$this->created_date = date('Y-m-d H:i:s');
		}else{
			$this->updated_by = Yii::app()->user->id;
			$this->updated_date = date('Y-m-d H:i:s');
		}
		return parent::beforeValidate();
	}

	public function LeaveDetail($params){
		$year = isset($params['year']) ? $params['year'] : null;
		$month = isset($params['month_number']) ? $params['month_number'] : null;
		$school_id = 40;//UtilityFunctions::ShowSchool() ? $params['school_id'] : UtilityFunctions::SchoolID();
		$employee_detail = EmployeeDetail::model()->findAll('school_id=:school_id AND status=:status',[':school_id'=>$school_id, ':status'=>1]);
		$employee_leave_detail = [];
		if(!empty($employee_detail)){
		if($month){
				foreach ($employee_detail as $employee_) {
					$leave_information = EmployeeLeave::model()->find('employee_id=:employee_id AND year=:year AND month=:month AND status=:status',['employee_id'=>$employee_->id, ':year'=>$year, ':month'=>$month, ':status'=>1]);
					$employee_leave_detail[$employee_->id][$month] = $leave_information ? $leave_information->leave_day : 0;
				}
		}else{
			for ($i=1; $i <= 12 ; $i++) {
				foreach ($employee_detail as $employee_) {
					$leave_information = EmployeeLeave::model()->find('employee_id=:employee_id AND year=:year AND month=:month AND status=:status',['employee_id'=>$employee_->id, ':year'=>$year, ':month'=>$i, ':status'=>1]);
					$employee_leave_detail[$employee_->id][$i] = $leave_information ? $leave_information->leave_day : 0;
				}
			}}
		}

	return $employee_leave_detail;	
	}
}
