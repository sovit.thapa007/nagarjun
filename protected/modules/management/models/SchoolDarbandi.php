<?php

/**
 * This is the model class for table "_school_darbandi".
 *
 * The followings are the available columns in table '_school_darbandi':
 * @property integer $id
 * @property integer $school_id
 * @property integer $level_id
 * @property integer $number
 * @property integer $status
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class SchoolDarbandi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '_school_darbandi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('school_id, level_id, number', 'required'),
			array('school_id, number, status, created_by, updated_by, state', 'numerical', 'integerOnly'=>true),
			array('created_date, updated_date', 'safe'),
			array('level_id', 'length', 'max'=>28),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, school_id, level_id, number, status, created_by, created_date, updated_by, updated_date, state', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'school_id' => 'School',
			'state' => 'State',
			'level_id' => 'Level',
			'number' => 'Number',
			'status' => 'Status',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('school_id',$this->school_id);
		$criteria->compare('level_id',$this->level_id);
		$criteria->compare('number',$this->number);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SchoolDarbandi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeValidate(){
		if(!$this->isNewRecord){
			$this->status = 1;
			$this->created_by = Yii::app()->user->id;
			$this->created_date = date('Y-m-d H:i:s');
		}else{
			$this->updated_by = Yii::app()->user->id;
			$this->updated_date = date('Y-m-d H:i:s');
		}
		return parent::beforeValidate();
	}

	public function Darbandi($school_id){
		$school_array = [];
		$employee_darbandi = SchoolDarbandi::model()->findAll('school_id=:school_id AND status=:status',["school_id"=>$school_id, ':status'=>1]);
		if(!empty($employee_darbandi)){
			foreach ($employee_darbandi as $darbandi_) {
				$type = $darbandi_->state == 2 ? 'rahat' : 'total';
				$school_array[$type][$darbandi_->level_id] = $darbandi_->number;
			}
		}
		return $school_array;

	}
}
