<?php

/**
 * This is the model class for table "_scholarship".
 *
 * The followings are the available columns in table '_scholarship':
 * @property integer $id
 * @property integer $school_id
 * @property integer $magform_id
 * @property string $type
 * @property integer $year
 * @property string $class
 * @property integer $students_number
 * @property integer $rate
 * @property string $amount
 * @property string $remarks
 * @property integer $status
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class Scholarship extends CActiveRecord
{

    public function behaviors() {
        return array(
            'ECompositeUniqueKeyValidatable' => array(
                'class' => 'ECompositeUniqueKeyValidatable',
                'uniqueKeys' => array(
                    'attributes' => 'school_id, magform_id, type, year, class',
                    'errorAttributes' => 'year',
                    'errorMessage' => 'यो विवरणा पहिला नै उपलब्ध छ|'
                )
            ),
        );
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '_scholarship';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('*', 'compositeUniqueKeysValidator'),
			array('school_id, year, class, students_number, rate, amount', 'required'),
			array('school_id, magform_id, magform_id, year, students_number, rate, status, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>18),
			array('class', 'length', 'max'=>100),
			array('amount', 'length', 'max'=>8),
			array('remarks, created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, school_id, magform_id, type, year, class, students_number, rate, amount, remarks, status, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'school' => array(self::BELONGS_TO, 'BasicInformation', 'school_id'),
			'magform' => array(self::BELONGS_TO, 'Magform', 'magform_id'),
		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'school_id' => 'बिद्यालय',
			'magform_id' => 'मग फर्म',
			'type' => 'प्रकार',
			'year' => 'वर्ष',
			'class' => 'कक्षा',
			'students_number' => 'विद्यार्थी संख्या',
			'rate' => 'दर',
			'amount' => 'रकम',
			'remarks' => 'कैफियत',
			'status' => 'Status',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		if(UtilityFunctions::ShowSchool())
			$criteria->compare('school_id',$this->school_id);
		else
			$criteria->compare('school_id',UtilityFunctions::SchoolID());
		$criteria->compare('magform_id',$this->magform_id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('year',$this->year);
		$criteria->compare('class',$this->class,true);
		$criteria->compare('students_number',$this->students_number);
		$criteria->compare('rate',$this->rate);
		$criteria->compare('amount',$this->amount,true);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
     * Validates composite unique keys
     *
     * Validates composite unique keys declared in the
     * ECompositeUniqueKeyValidatable bahavior
     */
    public function compositeUniqueKeysValidator() {
        $this->validateCompositeUniqueKeys();
    }
 	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Scholarship the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function beforeValidate(){
		if(!$this->isNewRecord){
			$this->status = 1;
			$this->created_by = Yii::app()->user->id;
			$this->created_date = date('Y-m-d H:i:s');
		}else{
			$this->updated_by = Yii::app()->user->id;
			$this->updated_date = date('Y-m-d H:i:s');
		}
		return parent::beforeValidate();
	}

	/**
	*
	*/
	public static function TotalScholarShip($magform_id){
		$criteria = new CDbCriteria;
		$criteria -> select = 'sum(amount) as amount';
		$criteria -> condition = 'magform_id=:magform_id AND status=:status';
		$criteria -> params = [':magform_id'=>$magform_id, ':status'=>1];
		$scholarship = $this->find($criteria);
		return $scholarship ? $scholarship->amount : 0;
	}
}
