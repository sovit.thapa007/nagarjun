<?php

/**
 * This is the model class for table "_transaction".
 *
 * The followings are the available columns in table '_transaction':
 * @property integer $id
 * @property integer $school_id
 * @property string $type
 * @property string $fiscal_year
 * @property integer $trimasik
 * @property integer $year
 * @property integer $month
 * @property string $amount
 * @property string $remark
 * @property integer $status
 * @property string $nepali_date
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class Transaction extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '_transaction';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('school_id, year, month, amount', 'required'),
			array('school_id, trimasik, year, month, status, created_by, updated_by', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>43),
			array('fiscal_year', 'length', 'max'=>9),
			array('amount', 'length', 'max'=>12),
			array('nepali_date', 'length', 'max'=>20),
			array('remark, created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, school_id, type, fiscal_year, trimasik, year, month, amount, remark, status, nepali_date, created_by, created_date, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'school_id' => 'School',
			'type' => 'Type',
			'fiscal_year' => 'Fiscal Year',
			'trimasik' => 'Trimasik',
			'year' => 'Year',
			'month' => 'Month',
			'amount' => 'Amount',
			'remark' => 'Remark',
			'status' => 'Status',
			'nepali_date' => 'Nepali Date',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('school_id',$this->school_id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('fiscal_year',$this->fiscal_year,true);
		$criteria->compare('trimasik',$this->trimasik);
		$criteria->compare('year',$this->year);
		$criteria->compare('month',$this->month);
		$criteria->compare('amount',$this->amount,true);
		$criteria->compare('remark',$this->remark,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('nepali_date',$this->nepali_date,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Transaction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
