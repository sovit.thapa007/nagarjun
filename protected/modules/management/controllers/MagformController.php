<?php

class MagformController extends Controller
{
	/**
	* @return array action filters
	*/
	/*public function filters()
	{
		return array(
			'rights', // perform access control for CRUD operations
		);
	}
*/

	/**
	*
	*/
	public function actionDashboard(){
		$magform_count_information = [];
		$chaumasik = $fiscal_year = null;
		if(isset($_POST['chaumasik']) && isset($_POST['fiscal_year'])){
			$chaumasik = (int) $_POST['chaumasik'];
			$fiscal_year = $_POST['fiscal_year'];
			$magform_count_information = Magform::model()->MagformCounting($_POST);
		}
		return $this->render('_mag_form_dashboard',['magform_count_information'=>$magform_count_information, 'chaumasik'=>$chaumasik, 'fiscal_year'=>$fiscal_year]);
	}

	/**
	[type] => salary
    [fiscal_year] => 2075/2076
    [chaumasik] => 1
	*/
	public function actionSchoolListing(){
		$school_listing = [];
		$type = $fiscal_year = $chaumasik = null;
		if(isset($_GET['type']) && isset($_GET['fiscal_year']) && isset($_GET['chaumasik'])){
			$chaumasik = (int) $_GET['chaumasik'];
			$type = $_GET['type'];
			$school_listing = Magform::model()->SchoolListing($_GET);
		}
		return $this->render('_magform_school_list',['school_listing'=>$school_listing, 'type'=>$type, 'fiscal_year'=>$fiscal_year, 'chaumasik'=>$chaumasik]);
	}

	
	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Magform;

	// Uncomment the following line if AJAX validation is needed
	// $this->performAjaxValidation($model);

		if(isset($_POST['Magform']))
		{
			$model->attributes=$_POST['Magform'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}


	public function actionCustomizeForm($id){
		$magform_detail = $this->loadModel($id);
		if(empty($magform_detail))
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		$school_id = $magform_detail->school_id;
		$school_detail = BasicInformation::model()->findByPk($school_id);
		$employee_detail = EmployeeDetail::model()->findAll('school_id=:school_id AND status=:status ORDER BY level, type',[':school_id'=>$school_id, ':status'=>1]);
		$employee_darbandi = SchoolDarbandi::model()->Darbandi($school_id);
		if($magform_detail->type == 'salary')
			return $this->render('_salary_form',['employee_detail'=>$employee_detail, 'school_id'=>$school_id, 'magform_detail'=>$magform_detail, 'school_detail'=>$school_detail, 'employee_darbandi'=>$employee_darbandi]);
		if($magform_detail->type == 'cloth')
			return $this->render('_cloth_bonus',['employee_detail'=>$employee_detail, 'school_id'=>$school_id, 'magform_detail'=>$magform_detail, 'school_detail'=>$school_detail, 'employee_darbandi'=>$employee_darbandi]);
		if($magform_detail->type == 'dasai_bonus')
			return $this->render('_festive_bonus',['employee_detail'=>$employee_detail, 'school_id'=>$school_id, 'magform_detail'=>$magform_detail, 'school_detail'=>$school_detail, 'employee_darbandi'=>$employee_darbandi]);
	}

	/**
	* month wise salary school wise
	*/
	public function actionMonthWiseSalaray(){
		if(isset($_GET['school_id']) && isset($_GET['number']) && isset($_GET['fiscal_year'])){
			$school_id = isset($_GET['school_id']) && is_numeric($_GET['school_id']) ? $_GET['school_id'] :  null;
			$employee_darbandi = SchoolDarbandi::model()->Darbandi($school_id);
			$school_id = isset($_GET['school_id']) && is_numeric($_GET['school_id']) ? $_GET['school_id'] :  null;
			$traimasik_number = isset($_GET['number']) && is_numeric($_GET['number']) && in_array($_GET['number'], [1,2,3,4]) ? $_GET['number'] :  null;
			$fiscal_year = isset($_GET['fiscal_year']) ? $_GET['fiscal_year'] :  null;
			$month = isset($_GET['month']) && is_numeric($_GET['month']) ? $_GET['month'] :  null;
			$status = isset($_GET['status'])  ? $_GET['status'] :  null;
			$message = isset($_GET['message']) ?  $_GET['message'] : null;
			$available_month_array = isset(Yii::app()->params['chaumasik'][$traimasik_number]) ? Yii::app()->params['chaumasik'][$traimasik_number] :[];
			if(!$month || empty($month)){
				$month = isset($available_month_array[0]) ? $available_month_array[0] : null;
			}
			$year_information = explode('/', $fiscal_year);
			$year_1 = isset($year_information) ?  $year_information[0] :  null;
			$year_2 = isset($year_information) ?  $year_information[1] :  null;
			$year = $traimasik_number == 4 ? $year_2 : $year_1;
			$school_detail = BasicInformation::model()->findByPk($school_id);
			$employee_detail = EmployeeDetail::model()->findAll('school_id=:school_id AND status=:status',[':school_id'=>$school_id, ':status'=>1]);
			if(!in_array($month, $available_month_array) || empty($school_detail))
				throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
			$month_index = array_search($month, $available_month_array);
			$previous_month = isset($available_month_array[$month_index-1]) ? $available_month_array[$month_index-1] :  null;
			$next_month = isset($available_month_array[$month_index+1]) ? $available_month_array[$month_index+1] :  null;
			return $this->render('_month_wise_salary',['employee_detail'=>$employee_detail, 'school_id'=>$school_id, 'school_detail'=>$school_detail, 'fiscal_year'=>$fiscal_year, 'traimasik_number'=>$traimasik_number, 'month'=>$month, 'previous_month'=>$previous_month, 'next_month'=>$next_month, 'year'=>$year, 'status'=>$status, 'message'=>$message,'employee_darbandi'=>$employee_darbandi]);
		}
		else
			return $this->redirect(['create']);
	}

	/**
	* submit monthly salary
	*/
	public function actionSubmitmonthly(){
		if(isset($_POST['employee_id_array_'])){
			$employee_id_array = $_POST['employee_id_array_'];
			$current_month = isset($_POST['current_month']) && is_numeric($_POST['current_month']) ? $_POST['current_month'] : null;
			$school_id = isset($_POST['employee_school_id']) && is_numeric($_POST['employee_school_id']) ?  $_POST['employee_school_id'] : null;
			$total_employee_array = [];
			$total_employee_detail = EmployeeDetail::model()->findAll('school_id=:school_id AND status=:status',[':school_id'=>$school_id, ':status'=>1]);
			if(!empty($total_employee_detail)){
				foreach ($total_employee_detail as $emp_detail)
					$total_employee_array[] = $emp_detail->id;
			}

			$next_month = isset($_POST['another_month']) && is_numeric($_POST['another_month']) ? $_POST['another_month'] : null;
			$traimasik_number = isset($_POST['traimasik_number']) && is_numeric($_POST['traimasik_number']) ? $_POST['traimasik_number'] : null;
			$fiscal_year = isset($_POST['fiscal_year']) ? $_POST['fiscal_year'] : '';
			$year_information = explode('/', $fiscal_year);
			$year_1 = isset($year_information) ?  $year_information[0] :  null;
			$year_2 = isset($year_information) ?  $year_information[1] :  null;
			$year = $traimasik_number == 4 ? $year_2 : $year_1;
			$available_month_array = isset(Yii::app()->params['chaumasik'][$traimasik_number]) ? Yii::app()->params['chaumasik'][$traimasik_number] :[];
			$magform_detail = Magform::model()->find('school_id=:school_id AND fiscal_year=:fiscal_year AND chaumasik=:chaumasik AND type=:type AND status=:status',[':school_id'=>$school_id, ':fiscal_year'=>$fiscal_year, ':chaumasik'=>$traimasik_number ,':type'=>'salary', ':status'=>1]);
			if(!in_array($current_month, $available_month_array) || empty($magform_detail))
				throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
			$error_array = [];
			$transaction = Yii::app()->db->beginTransaction();
			$school_id = null;
			$deactive_employee_array = !empty($total_employee_array) ? array_diff($total_employee_array,$employee_id_array) : [];
			for ($i=0; $i < sizeof($employee_id_array) ; $i++) { 
				$employee_id = $employee_id_array[$i];
				$employee_detail = EmployeeDetail::model()->findByPk($employee_id);
				if(empty($employee_detail))
					continue;
				$days = isset($_POST['present_days_'.$employee_id]) && is_numeric($_POST['present_days_'.$employee_id]) ?  $_POST['present_days_'.$employee_id] : 0;
				$school_id = $employee_detail->school_id;
				$basic_salary = $days*$employee_detail->BasicSalary/30;
				$grade_salary = $days*$employee_detail->BasicGradeSalary/30;
				$net_salary = $basic_salary + $grade_salary;
				$pf_percentage = Yii::app()->params['pf_percentage'];
				$provident_fund = $employee_detail->type == 'स्थायी' && $pf_percentage > 0 ? $pf_percentage*$net_salary/100  : 0;
				$insurance_amount = isset(Yii::app()->params['insurance_amount']) ? Yii::app()->params['insurance_amount']*$days/30 : 0;
				$general_bhatta = isset(Yii::app()->params['bhatta']) ? Yii::app()->params['bhatta']*$days/30 : 0;
				$pra_a_bhatta = $employee_detail->post=='प्र.अ.' && isset(Yii::app()->params['principal_bonus']) ?  Yii::app()->params['principal_bonus']*$days/30 :0;

				$salary = $basic_salary + $grade_salary + $provident_fund + $insurance_amount + $general_bhatta + $pra_a_bhatta;

				$month_array = array_values(array_diff($available_month_array,[$current_month]));
				$salary_other = MagformEmployeeDetails::TraimasikTotalAmount($employee_id, 'salary', $year, $month_array);
				$traimasik_salary = $salary + $salary_other;
				$employee_trimasik_sakary_detail = MagformEmployee::model()->find('employee_id=:employee_id AND type=:type AND fiscal_year=:fiscal_year AND chaumasik=:chaumasik AND status=:status',[':employee_id'=>$employee_id, ':type'=>'salary', ':fiscal_year'=>$fiscal_year, ':chaumasik'=>$traimasik_number, ':status'=>1]);
				if(empty($employee_trimasik_sakary_detail)){
					$model = new MagformEmployee();
					$model -> type = 'salary';
					$model -> magform_id = $magform_detail->id;
					$model -> employee_id = $employee_id; 
					$model -> employee_name = $employee_detail->first_name_nepali.' '.$employee_detail->middle_name_nepali.' '.$employee_detail->last_name_nepali;
					$model -> account_number = $employee_detail->bank_account_number;
					$model -> employee_post = $employee_detail->post;
					$model -> employee_type = $employee_detail -> type;
					$model -> employee_level = $employee_detail -> level;
					$model -> employee_grade = $employee_detail -> grade;
					$model -> employee_sherni = $employee_detail -> sherni;
					$model -> fiscal_year = $magform_detail->fiscal_year;
					$model -> chaumasik = $magform_detail->chaumasik;
					$model -> amount = round($traimasik_salary);
					if(!$model->validate() || !$model->save()){
						echo "<pre> one ";
						echo print_r($model->errors);
						exit;
						$error_array[] = 'false';
					}
					$traimasik_id = $model->id;
				}else{
					$employee_trimasik_sakary_detail -> amount = round($traimasik_salary);
					if(!$employee_trimasik_sakary_detail->validate() || !$employee_trimasik_sakary_detail->update()){
						echo "<pre> update traimasik salary";
						echo print_r($model->errors);
						exit;
						$error_array[] = 'false';
					}
					$traimasik_id = $employee_trimasik_sakary_detail->id;
				}

				$employee_monthly_salary = MagformEmployeeDetails::model()->find('employee_id=:employee_id AND type=:type AND year=:year AND month=:month AND status=:status', [':employee_id'=>$employee_id , ':type'=>'salary', ':year'=>$year, ':month'=>$current_month, ':status'=>1]);
				if(empty($employee_monthly_salary)){
					$employee_monthly_salary = new MagformEmployeeDetails();
					$employee_monthly_salary->employee_id = $employee_id;
					$employee_monthly_salary->type = 'salary';
					$employee_monthly_salary->year = $year;
					$employee_monthly_salary->month = $current_month;
					$employee_monthly_salary->status = 1;
					$employee_monthly_salary->sherni = $employee_detail->sherni;
					$employee_monthly_salary->grade = $employee_detail->grade;
				}
				$employee_monthly_salary->magform_employee_id = $traimasik_id;
				$employee_monthly_salary->total_day = $days;
				$employee_monthly_salary->basic_scale = round($basic_salary,2);
				$employee_monthly_salary->basic_grade_scale = round($grade_salary,2);
				$employee_monthly_salary->pf_amount = round($provident_fund,2);
				$employee_monthly_salary->insurance_amount = round($insurance_amount,2);
				$employee_monthly_salary->bhatta = round($general_bhatta,2);
				$employee_monthly_salary->pra_a_bhatta = round($pra_a_bhatta,2);
				$employee_monthly_salary->monthly_total = round($salary,2);
				if(!$employee_monthly_salary->validate() || !$employee_monthly_salary->save()){
					echo "<pre>";
					echo print_r($employee_monthly_salary->errors);
					exit;
					$error_array[] = 'false';
				}
			}
			$employee_status_section = MagformEmployeeDetails::model()->findAllByAttributes(['type'=>'salary','year'=>$year, 'month'=>$current_month, 'status'=>1, 'employee_id'=>$deactive_employee_array]);
			if(!empty($employee_status_section)){
				$remove_employee = MagformEmployeeDetails::model()->updateAll(array( 'status' => 0 ), 'type="salary" AND year='.$year.' AND month='.$current_month.' AND status=1 AND employee_id IN ('.implode(',', $deactive_employee_array).' )');
				if(!$remove_employee)
					$error_array[] = 'false';
			}
			if(!in_array('false', $error_array)){
				$message = UtilityFunctions::NepaliNumber($fiscal_year).' '.UtilityFunctions::SherniNepali($traimasik_number).' त्रैमासिकको '.UtilityFunctions::NepaliNumber($current_month).' महिनाको माग फारम भएको छ | ';
				$transaction->commit();
				$this->redirect(['/management/magform/monthWiseSalaray/?school_id='.$school_id.'&number='.$traimasik_number.'&fiscal_year='.$fiscal_year.'&month='.$next_month.'&status=1&message='.$message]);
			}else{
				$message = UtilityFunctions::NepaliNumber($fiscal_year).' '.UtilityFunctions::SherniNepali($traimasik_number).' त्रैमासिकको '.UtilityFunctions::NepaliNumber($current_month).' महिनाको माग फारम दर्ता भएको छैन |';
				$transaction->rollback();
				$this->redirect(['/management/magform/monthWiseSalaray/?school_id='.$school_id.'&number='.$traimasik_number.'&fiscal_year='.$fiscal_year.'&month='.$next_month.'&status=0&message='.$message]);
			}

		}
		throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionSubmitSalary(){
		if(isset($_POST['school']) && is_numeric($_POST['school'])){
			$school_id = $_POST['school'];
			$magform_id = isset($_POST['magform_id']) ? $_POST['magform_id'] : null;
			$magform_detail = $this->loadModel($magform_id);
			if(empty($magform_detail))
				throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
			$year_information = explode('/', $magform_detail->fiscal_year);
			$year_1 = isset($year_information) ?  $year_information[0] :  null;
			$year_2 = isset($year_information) ?  $year_information[1] :  null;
			if($school_id == UtilityFunctions::SchoolID() || UtilityFunctions::ShowSchool()){
				$employee_detail = EmployeeDetail::model()->findAll('school_id=:school_id AND status=:status',[':school_id'=>$school_id, ':status'=>1]);
				$error_array = [];
				$transaction = Yii::app()->db->beginTransaction();
				if(!empty($employee_detail)){
					foreach ($employee_detail as $employee_) {
						$employee_id = $employee_->id;
						$employee_trimasik_sakary_detail = MagformEmployee::model()->find('employee_id=:employee_id AND type=:type AND fiscal_year=:fiscal_year AND chaumasik=:chaumasik AND status=:status',[':employee_id'=>$employee_id, ':type'=>'salary', ':fiscal_year'=>$magform_detail->fiscal_year, ':chaumasik'=>$magform_detail->chaumasik, ':status'=>1]);
						if(empty($employee_trimasik_sakary_detail)){
							$model = new MagformEmployee();
							$model -> type = 'salary';
							$model -> magform_id = $magform_id;
							$model -> employee_id = $employee_id; 
							$model -> employee_name = $employee_->first_name_nepali.' '.$employee_->middle_name_nepali.' '.$employee_->last_name_nepali;
							$model -> account_number = $employee_->bank_account_number;
							$model -> employee_post = $employee_->post;
							$model -> fiscal_year = $magform_detail->fiscal_year;
							$model -> chaumasik = $magform_detail->chaumasik;
							$model -> amount = $employee_->ThreeMonthlySalary;
							if(!$model->validate() || !$model->save()){
								echo "<pre> one ";
								echo print_r($model->errors);
								exit;
								$error_array[] = 'false';
							}
							$traimasik_id = $model->id;
						}else{
							$employee_trimasik_sakary_detail -> amount = $employee_->ThreeMonthlySalary;
							if(!$employee_trimasik_sakary_detail->validate() || !$employee_trimasik_sakary_detail->update()){
								echo "<pre> update traimasik salary";
								echo print_r($employee_trimasik_sakary_detail->errors);
								exit;
								$error_array[] = 'false';
							}
						$traimasik_id = $employee_trimasik_sakary_detail->id;
						}

						$chaumasik_month = isset(Yii::app()->params['chaumasik'][$magform_detail->chaumasik]) ? Yii::app()->params['chaumasik'][$magform_detail->chaumasik] : [];
						$nepali_year = $magform_detail->chaumasik == 4 ? $year_2 : $year_1;
						for ($i=0; $i <sizeof($chaumasik_month); $i++) {
							$month = $chaumasik_month[$i];
							$employee_monthly_salary = MagformEmployeeDetails::model()->find('employee_id=:employee_id AND type=:type AND year=:year AND month=:month AND status=:status', [':employee_id'=>$employee_id , ':type'=>'salary', ':year'=>$nepali_year, ':month'=>$month, ':status'=>1]);
							if(empty($employee_monthly_salary)){
								$employee_detail = new MagformEmployeeDetails();
								$employee_detail->magform_employee_id = $traimasik_id;
								$employee_detail->employee_id = $employee_id;
								$employee_detail->total_day = 30;
								$employee_detail->year = $nepali_year;
								$employee_detail->type = 'salary';
								$employee_detail->month = $month;
								$employee_detail->sherni = $employee_->sherni;
								$employee_detail->grade = $employee_->grade;
								$employee_detail->basic_scale = $employee_->BasicSalary;
								$employee_detail->basic_grade_scale = $employee_->BasicGradeSalary;
								$employee_detail->pf_amount = $employee_->ProvidentFund;
								$employee_detail->insurance_amount = Yii::app()->params['insurance_amount'];
								$employee_detail->bhatta = Yii::app()->params['bhatta'];
								$employee_detail->pra_a_bhatta = $employee_->post=='प्र.अ.' ? Yii::app()->params['principal_bonus'] :  0;
								$employee_detail->monthly_total = $employee_->MonthlySalary;
								$employee_detail->state = 'school';
								$employee_detail->state_status = 'send';
								if(!$employee_detail->validate() || !$employee_detail->save()){
									$error_array[] = 'false';
									echo "<pre> two ";
									echo print_r($employee_detail->errors);
									exit;
								}
							}else{
								$employee_monthly_salary->sherni = $employee_->sherni;
								$employee_monthly_salary->grade = $employee_->grade;
								$employee_monthly_salary->basic_scale = $employee_->BasicSalary;
								$employee_monthly_salary->basic_grade_scale = $employee_->BasicGradeSalary;
								$employee_monthly_salary->pf_amount = $employee_->ProvidentFund;
								$employee_monthly_salary->insurance_amount = Yii::app()->params['insurance_amount'];
								$employee_monthly_salary->bhatta = Yii::app()->params['bhatta'];
								$employee_monthly_salary->pra_a_bhatta = $employee_->post=='प्र.अ.' ? Yii::app()->params['principal_bonus'] :  0;
								$employee_monthly_salary->monthly_total = $employee_->MonthlySalary;
								$employee_monthly_salary->state = 'school';
								$employee_monthly_salary->state_status = 'send';
								if(!$employee_monthly_salary->validate() || !$employee_monthly_salary->update()){
									$error_array[] = 'false';
									echo "<pre> udpate query ";
									echo print_r($employee_monthly_salary->errors);
									exit;
								}

							}


						}

					}
				}
				if(!in_array('false', $error_array)){
					$transaction->commit();
					$this->redirect(['magform/admin']);
				}else{
					$transaction->rollback();
					die('magform cannot send to municipality');
				}

			}else
				throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}else
			$this->redirect(['create']);
	}
	/**
	*
	*/
	public function actionCalculateDayWiseSalaray(){
		/*$arr = array('a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5);
		echo json_encode($arr);*/
		if(isset($_POST['employee_id']) && isset($_POST['days'])){
			$days = is_numeric($_POST['days']) ? $_POST['days'] : 0;
			$employee_id = is_numeric($_POST['employee_id']) ? $_POST['employee_id'] : null;
			$employee_detail = EmployeeDetail::model()->findByPk($employee_id);
			$salary_detail_information = [];
			if(empty($employee_detail))
				return false;
			$basic_salary = $days*$employee_detail->BasicSalary/30;
			$grade_salary = $days*$employee_detail->BasicGradeSalary/30;
			$net_salary = $basic_salary + $grade_salary;
			$pf_percentage = Yii::app()->params['pf_percentage'];
			$provident_fund = $employee_detail->type == 'स्थायी' && $pf_percentage > 0 ? $pf_percentage*$net_salary/100  : 0;
			$insurance_amount = isset(Yii::app()->params['insurance_amount']) ? Yii::app()->params['insurance_amount']*$days/30 : 0;
			$general_bhatta = isset(Yii::app()->params['bhatta']) ? Yii::app()->params['bhatta']*$days/30 : 0;
			if($employee_detail->post=='प्र.अ.')
				$general_bhatta += isset(Yii::app()->params['principal_bonus']) ?  Yii::app()->params['principal_bonus']*$days/30 :0;

			$salary = $basic_salary + $grade_salary + $provident_fund + $insurance_amount + $general_bhatta ;

			$salary_detail_information['basic_salary_nepali'] = UtilityFunctions::NepaliNumber(number_format($basic_salary, 2));
			$salary_detail_information['grade_salary_nepali'] = UtilityFunctions::NepaliNumber(number_format($grade_salary, 2));
			$salary_detail_information['net_salary_nepali'] = UtilityFunctions::NepaliNumber(number_format($net_salary, 2));
			$salary_detail_information['provident_fund_nepali'] = UtilityFunctions::NepaliNumber(number_format($provident_fund, 2));
			$salary_detail_information['insurance_amount_nepali'] = UtilityFunctions::NepaliNumber(number_format($insurance_amount, 2));
			$salary_detail_information['general_bhatta_nepali'] = UtilityFunctions::NepaliNumber(number_format($general_bhatta, 2));
			$salary_detail_information['total_salary'] = UtilityFunctions::NepaliNumber(number_format($salary, 2));
			$salary_detail_information['days'] = UtilityFunctions::NepaliNumber($days);
			echo json_encode($salary_detail_information, JSON_UNESCAPED_UNICODE);

		}
	}

	/**
	*festive bonus
	*/
	public function actionSubmitFestiveBonus(){
		if(isset($_POST['school']) && is_numeric($_POST['school'])){
			$school_id = $_POST['school'];
			$magform_id = isset($_POST['magform_id']) ? $_POST['magform_id'] : null;
			$magform_detail = $this->loadModel($magform_id);
			if(empty($magform_detail))
				throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
			$year_information = explode('/', $magform_detail->fiscal_year);
			$year_1 = isset($year_information) ?  $year_information[0] :  null;
			$year_2 = isset($year_information) ?  $year_information[1] :  null;
			if($school_id == UtilityFunctions::SchoolID() || UtilityFunctions::ShowSchool()){
				$employee_detail = EmployeeDetail::model()->findAll('school_id=:school_id AND status=:status',[':school_id'=>$school_id, ':status'=>1]); // employee type should be fil
				$error_array = [];
				$transaction = Yii::app()->db->beginTransaction();
				if(!empty($employee_detail)){
					foreach ($employee_detail as $employee_) {
						$employee_id = $employee_->id;
						$model = new MagformEmployee();
						$model -> magform_id = $magform_id;
						$model -> type = 'dasai_bonus';
						$model -> employee_id = $employee_id; 
						$model -> employee_name = $employee_->first_name_nepali.' '.$employee_->middle_name_nepali.' '.$employee_->last_name_nepali;
						$model -> account_number = $employee_->bank_account_number;
						$model -> employee_post = $employee_->post;
						$model -> fiscal_year = $magform_detail->fiscal_year;
						$model -> chaumasik = $magform_detail->chaumasik;
						$model -> amount = $employee_->BasicSalary;
						if(!$model->validate() || !$model->save())
							$error_array[] = 'false';

						$chaumasik_month = isset(Yii::app()->params['chaumasik'][$magform_detail->chaumasik]) ? Yii::app()->params['chaumasik'][$magform_detail->chaumasik] : [];
						$nepali_year = $magform_detail->chaumasik == 4 ? $year_2 : $year_1;
						$month = end($chaumasik_month);
						$employee_detail = new MagformEmployeeDetails();
						$employee_detail->magform_employee_id = $model->id;
						$employee_detail->type = 'dasai_bonus';
						$employee_detail->year = $nepali_year;
						$employee_detail->month = $month;
						$employee_detail->sherni = $employee_->sherni;
						$employee_detail->grade = $employee_->grade;
						$employee_detail->posak_bhatta = $employee_->BasicSalary;
						$employee_detail->monthly_total = $employee_->BasicSalary;
						$employee_detail->state = 'school';
						$employee_detail->state_status = 'send';
						if(!$employee_detail->validate() || !$employee_detail->save())
							$error_array[] = 'false';
					}
				}
				if(!in_array('false', $error_array)){
					$transaction->commit();
					die('magform send to municipality');
				}else{
					$transaction->rollback();
					die('magform cannot send to municipality');
				}
			}else
				throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}else
			$this->redirect(['create']);
	}

	public function actionSubmitClotheBonus(){
		if(isset($_POST['school']) && is_numeric($_POST['school'])){
			$school_id = $_POST['school'];
			$magform_id = isset($_POST['magform_id']) ? $_POST['magform_id'] : null;
			$magform_detail = $this->loadModel($magform_id);
			if(empty($magform_detail))
				throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
			$year_information = explode('/', $magform_detail->fiscal_year);
			$year_1 = isset($year_information) ?  $year_information[0] :  null;
			$year_2 = isset($year_information) ?  $year_information[1] :  null;
			if($school_id == UtilityFunctions::SchoolID() || UtilityFunctions::ShowSchool()){
				$employee_detail = EmployeeDetail::model()->findAll('school_id=:school_id AND status=:status',[':school_id'=>$school_id, ':status'=>1]);
				$employee_id_array = isset($_POST['employee_']) ? $_POST['employee_'] : [];
				$error_array = [];
				$transaction = Yii::app()->db->beginTransaction();
				if(!empty($employee_detail)){
					foreach ($employee_detail as $employee_) {
						$employee_id = $employee_->id;
						if(!in_array($employee_id, $employee_id_array))
							continue;
						$amount = isset($_POST['posak_bhatta_'.$employee_id]) ? (int) $_POST['posak_bhatta_'.$employee_id] : 0;
						$model = new MagformEmployee();
						$model -> magform_id = $magform_id;
						$model -> type = 'cloth';
						$model -> employee_id = $employee_id; 
						$model -> employee_name = $employee_->first_name_nepali.' '.$employee_->middle_name_nepali.' '.$employee_->last_name_nepali;
						$model -> account_number = $employee_->bank_account_number;
						$model -> employee_post = $employee_->post;
						$model -> fiscal_year = $magform_detail->fiscal_year;
						$model -> chaumasik = $magform_detail->chaumasik;
						$model -> amount = $amount;
						if(!$model->validate() || !$model->save())
							$error_array[] = 'false';

						$chaumasik_month = isset(Yii::app()->params['chaumasik'][$magform_detail->chaumasik]) ? Yii::app()->params['chaumasik'][$magform_detail->chaumasik] : [];
						$nepali_year = $magform_detail->chaumasik == 4 ? $year_2 : $year_1;
						$month = end($chaumasik_month);
						$employee_detail = new MagformEmployeeDetails();
						$employee_detail->magform_employee_id = $model->id;
						$employee_detail->type = 'cloth';
						$employee_detail->year = $nepali_year;
						$employee_detail->month = $month;
						$employee_detail->sherni = $employee_->sherni;
						$employee_detail->grade = $employee_->grade;
						$employee_detail->posak_bhatta = $amount;
						$employee_detail->monthly_total = $amount;
						$employee_detail->state = 'school';
						$employee_detail->state_status = 'send';
						if(!$employee_detail->validate() || !$employee_detail->save())
							$error_array[] = 'false';
					}
				}
				if(!in_array('false', $error_array)){
					$transaction->commit();
					die('magform send to municipality');
				}else{
					$transaction->rollback();
					die('magform cannot send to municipality');
				}
			}else
				throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}else
			$this->redirect(['create']);
	}


	/**
	*
	*/
	public function actionDetail($id){
		$magform_detail = $this->loadModel($id);
		if(empty($magform_detail))
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		$school_id = $magform_detail->school_id;
		$school_detail = BasicInformation::model()->findByPk($school_id);
		$employee_info = EmployeeDetail::model()->LevelWiseEmployeeNumber($school_id);
		$this->render('_magform_detail',['magform_detail'=>$magform_detail, 'school_detail'=>$school_detail, 'employee_info'=>$employee_info]);
	}

	/**
	*
	*/
	public function actionDownloadDetail($id){
		$magform_detail = $this->loadModel($id);
		if(empty($magform_detail))
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		$school_id = $magform_detail->school_id;
		$school_detail = BasicInformation::model()->findByPk($school_id);

        # mPDF
        $mPDF1 = Yii::app()->ePdf->mpdf();
        # You can easily override default constructor's params
        $mPDF1 = Yii::app()->ePdf->mpdf('UTF-8', 'A4');
        $mPDF1->writeHTMLfooter=true;
        $mPDF1->defaultfooterline = 0;
        $mPDF1->SetHTMLFooter('<div style="text-align: center">{PAGENO} / {nbpg}</div>');
        $mPDF1->AddPage('P');

        $mPDF1->WriteHTML($this->renderPartial('_magform_detail_pdf',['magform_detail'=>$magform_detail, 'school_detail'=>$school_detail], true));
		//$mPDF1->WriteHTML($this->renderPartial('_magform_detail_pdf',['magform_detail'=>$magform_detail, 'school_detail'=>$school_detail], true));
        $mPDF1->Output($title.'.pdf', 'I');
        $mPDF1->Output('testingpdf.pdf','I');


	}

	/**
	* total salary detail of employee
	*/

	public function actionSalaryDetail($id){
		$this->layout = '_datatable_layout';
		$employee_detail = EmployeeDetail::model()->findByPk($id);
		if(empty($employee_detail))
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		$employee_salary_Detail = MagformEmployeeDetails::model()->findAll('employee_id=:employee_id ORDER BY year, month ASC',[':employee_id'=>$id]);
		return $this->render('_employee_salary_Detail',['employee_salary_Detail'=>$employee_salary_Detail, 'employee_detail'=>$employee_detail]);
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

	// Uncomment the following line if AJAX validation is needed
	// $this->performAjaxValidation($model);

		if(isset($_POST['Magform']))
		{
			$model->attributes=$_POST['Magform'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
	// we only allow deletion via POST request
			$this->loadModel($id)->delete();

	// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$model=new Magform('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Magform']))
			$model->attributes=$_GET['Magform'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new Magform('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Magform']))
			$model->attributes=$_GET['Magform'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=Magform::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='magform-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
