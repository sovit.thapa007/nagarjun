<?php

class ScholarshipController extends Controller
{
	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','index','view','admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Scholarship;

	// Uncomment the following line if AJAX validation is needed
	// $this->performAjaxValidation($model);

		if(isset($_POST['Scholarship']) && isset($_POST['Scholarship']['year']))
		{
			$year = isset($_POST['Scholarship']['year']) ? (int) $_POST['Scholarship']['year'] : null;
		   	$type = isset($_POST['Scholarship']['type']) ? $_POST['Scholarship']['type'] : null;
		   	$school_id = isset($_POST['Scholarship']['school_id']) ? $_POST['Scholarship']['school_id'] : null;
		   	$remarks = isset($_POST['Scholarship']['remarks']) ? $_POST['Scholarship']['remarks'] : '';
		   	$class = isset($_POST['Scholarship']['class']) ? $_POST['Scholarship']['class'] : [];
		   	$students_number_information = isset($_POST['Scholarship']['students_number']) ? $_POST['Scholarship']['students_number'] : [];
		   	$rate_information = isset($_POST['Scholarship']['rate']) ? $_POST['Scholarship']['rate'] : [];
		   	$error_array = [];
		   	$transaction = Yii::app()->db->beginTransaction();
		   	if(!empty($class)){
		   		$total_amount = 0;
		   		for ($j=0; $j < sizeof($students_number_information) ; $j++) { 
		   			$students_number = isset($students_number_information[$j]) ? $students_number_information[$j] : 0;
		   			$rate = isset($rate_information[$j]) ? $rate_information[$j] : 0;
		   			$amount_ = $students_number*$rate;
		   			$total_amount += $amount_;
		   		}
		   		$prv_year = $year-1;
		   		$fiscal_year = $prv_year."/".$year;
		   		$magform_detail = Magform::model()->find('school_id=:school_id AND type=:type AND fiscal_year=:fiscal_year AND chaumasik=:chaumasik', [':school_id'=>$school_id, ':type'=>'scholarship', ':fiscal_year'=>$fiscal_year, ':chaumasik'=>4]);
		   		if(empty($magform_detail)){
			   		$model_magform = new Magform();
			   		$model_magform -> school_id = $school_id;
			   		$model_magform -> type = 'scholarship';
			   		$model_magform -> fiscal_year = $fiscal_year;
			   		$model_magform -> chaumasik = 4;
			   		$model_magform -> total_amount = $total_amount;
			   		$model_magform -> content = $remarks;
			   		if(!$model_magform->validate() || !$model_magform->save())
			   			$error_array[] = 'false';
			   		$magform_id = $model_magform->id;
		   		}else{
		   			$magform_detail -> total_amount = Scholarship::TotalScholarShip($magform_detail->id);
			   		if(!$magform_detail->validate() || !$magform_detail->udpate())
			   			$error_array[] = 'false';
			   		$magform_id = $magform_detail->id;
		   		}	
		   		for ($i=0; $i < sizeof($class) ; $i++) { 
		   			$class_name = isset($class[$i]) ? $class[$i] : '';
		   			$students_number = isset($students_number_information[$i]) ? $students_number_information[$i] : 0;
		   			$rate = isset($rate_information[$i]) ? $rate_information[$i] : 0;
		   			$amount = $students_number * $rate;
		   			$scholarship_ = Scholarship::model()->find('school_id=:school_id AND type=:type AND year=:year AND class=:class AND status=:status',[':school_id'=>$school_id, ':type'=>$type, ':year'=>$year, ':class'=>$class_name, ':status'=>1]);
		   			if(empty($scholarship_)){
			   			$scholar_model = new Scholarship();
			   			$scholar_model -> magform_id = $magform_id;
			   			$scholar_model -> year = $year;
			   			$scholar_model -> type = $type;
			   			$scholar_model -> school_id = $school_id;
			   			$scholar_model->class = $class_name;
			   			$scholar_model->students_number = $students_number; 
			   			$scholar_model->rate = $rate;
			   			$scholar_model->amount = $students_number * $rate;
			   			$scholar_model->remarks = $remarks;
			   			if(!$scholar_model->validate() || !$scholar_model->save()){
			   				$error_array[] = 'false';
			   				echo "<pre>";
			   				echo print_r($scholar_model->errors);
			   				exit;
			   			}
		   			}else{
		   				$scholarship_ -> students_number = $students_number; 
			   			$scholarship_ -> rate = $rate;
			   			$scholarship_ -> amount = $students_number * $rate;
			   			$scholarship_ -> remarks = $remarks;
			   			if(!$scholarship_->validate() || !$scholarship_->update())
			   				$error_array[] = 'false';
		   			}
		   		}
		   	}
		   	if(!in_array('false', $error_array)){
		   		$transaction->commit();
		   		$this->redirect(['admin']);
		   	}else{
		   		$transaction->rollback();
				throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		   	}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

	// Uncomment the following line if AJAX validation is needed
	// $this->performAjaxValidation($model);

		if(isset($_POST['Scholarship']))
		{
			$model->attributes=$_POST['Scholarship'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
	// we only allow deletion via POST request
			$this->loadModel($id)->delete();

	// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$model=new Scholarship('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Scholarship']))
			$model->attributes=$_GET['Scholarship'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new Scholarship('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Scholarship']))
			$model->attributes=$_GET['Scholarship'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=Scholarship::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='scholarship-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
