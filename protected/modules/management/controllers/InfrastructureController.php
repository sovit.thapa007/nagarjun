<?php

class InfrastructureController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete','index','view'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Infrastructure;

	// Uncomment the following line if AJAX validation is needed
	// $this->performAjaxValidation($model);

		if(isset($_POST['Infrastructure']))
		{
		   	$school_id = isset($_POST['Infrastructure']['school_id']) ? $_POST['Infrastructure']['school_id'] : null;
		   	$remarks = isset($_POST['Infrastructure']['remarks']) ? $_POST['Infrastructure']['remarks'] : '';
		   	$title_array = isset($_POST['Infrastructure']['title']) ? $_POST['Infrastructure']['title'] : [];
		   	$amount_array = isset($_POST['Infrastructure']['amount']) ? $_POST['Infrastructure']['amount'] : [];
		   	$error_array = [];
		   	$transaction = Yii::app()->db->beginTransaction();
		   	if(!empty($title_array)){
		   		$total_amount = 0;
		   		for ($j=0; $j < sizeof($amount_array) ; $j++) { 
		   			$amount_ = isset($amount_array[$j]) ? $amount_array[$j] : 0;
		   			$total_amount += $amount_;
		   		}
		   		$model_transaction = new Transaction();
		   		$model_transaction -> school_id = $school_id;
		   		$model_transaction -> type = 'भौतिक निर्माण';
		   		$model_transaction -> fiscal_year = '2075/2076';
		   		$model_transaction -> trimasik = 2;
		   		$model_transaction -> year = '2076';
		   		$model_transaction -> month = 3;
		   		$model_transaction -> amount = $total_amount;
		   		$model_transaction -> remark = $remarks;
		   		if(!$model_transaction->validate() || !$model_transaction->save())
		   			$error_array[] = 'false';
		   		for ($i=0; $i < sizeof($title_array) ; $i++) { 
		   			$title = isset($title_array[$i]) ? $title_array[$i] : '';
		   			$amount = isset($amount_array[$i]) ? $amount_array[$i] : 0;
		   			$infrast_model = new Infrastructure();
		   			$infrast_model -> transaction_id = $model_transaction->id;
		   			$infrast_model -> school_id = $school_id;
		   			$infrast_model->title = $title;
		   			$infrast_model->amount = $amount;
		   			$infrast_model->remarks = $remarks;
		   			if(!$infrast_model->validate() || !$infrast_model->save()){
		   				$error_array[] = 'false';
		   				echo "<pre>";
		   				echo print_r($infrast_model->errors);
		   				exit;
		   			}
		   		}
		   	}
		   	
		   	if(!in_array('false', $error_array)){
		   		$transaction->commit();
		   		$this->redirect(['admin']);
		   	}else{
		   		$transaction->rollback();
				throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		   	}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

	// Uncomment the following line if AJAX validation is needed
	// $this->performAjaxValidation($model);

		if(isset($_POST['Infrastructure']))
		{
			$model->attributes=$_POST['Infrastructure'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
	// we only allow deletion via POST request
			$this->loadModel($id)->delete();

	// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$model=new Infrastructure('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Infrastructure']))
			$model->attributes=$_GET['Infrastructure'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new Infrastructure('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Infrastructure']))
			$model->attributes=$_GET['Infrastructure'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=Infrastructure::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='infrastructure-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
