<?php

class SchoolDarbandiController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	//public $layout='//layouts/column2';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow', // allow authenticated user to perform 'create' and 'update' actions
			'actions'=>array('create','update','index','view','admin','delete'),
			'users'=>array('@'),
		),
		array('deny',  // deny all users
			'users'=>array('*'),
		),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new SchoolDarbandi;

	// Uncomment the following line if AJAX validation is needed
	// $this->performAjaxValidation($model);

		if(isset($_POST['SchoolDarbandi']))
		{
		   	$school_id = isset($_POST['SchoolDarbandi']['school_id']) ? $_POST['SchoolDarbandi']['school_id'] : null;
		   	$school_level_array = isset($_POST['SchoolDarbandi']['level_id']) ? $_POST['SchoolDarbandi']['level_id'] : [];
		   	$amount_array = isset($_POST['SchoolDarbandi']['number']) ? $_POST['SchoolDarbandi']['number'] : [];
		   	$error_array = [];
		   	$transaction = Yii::app()->db->beginTransaction();
		   	if(!empty($school_level_array)){
		   		for ($i=0; $i < sizeof($school_level_array) ; $i++) { 
		   			$level = isset($school_level_array[$i]) ? $school_level_array[$i] : '';
		   			$number = isset($amount_array[$i]) ? $amount_array[$i] : 0;
		   			$darbandi_model = new SchoolDarbandi();
		   			$darbandi_model -> school_id = $school_id;
		   			$darbandi_model->level_id = $level;
		   			$darbandi_model->number = $number;
		   			if(!$darbandi_model->validate() || !$darbandi_model->save()){
		   				$error_array[] = 'false';
		   				echo "<pre>";
		   				echo print_r($darbandi_model->errors);
		   				exit;
		   			}
		   		}
		   	}
		   	
		   	if(!in_array('false', $error_array)){
		   		$transaction->commit();
		   		$this->redirect(['admin']);
		   	}else{
		   		$transaction->rollback();
				throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		   	}


			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}



	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

	// Uncomment the following line if AJAX validation is needed
	// $this->performAjaxValidation($model);

		if(isset($_POST['SchoolDarbandi']))
		{
			$model->attributes=$_POST['SchoolDarbandi'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
	// we only allow deletion via POST request
			$this->loadModel($id)->delete();

	// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$model=new SchoolDarbandi('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SchoolDarbandi']))
			$model->attributes=$_GET['SchoolDarbandi'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new SchoolDarbandi('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SchoolDarbandi']))
			$model->attributes=$_GET['SchoolDarbandi'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=SchoolDarbandi::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='school-darbandi-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
