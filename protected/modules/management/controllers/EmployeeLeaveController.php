<?php

class EmployeeLeaveController extends Controller
{

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','view', 'manage', 'submitLeave', 'admin'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$school_list = BasicInformation::model()->findAll('status=:status',[':status'=>1]);
		$this->render('_employee_leave_dashboard',['school_list'=>$school_list]);
	}

	/**
	*generage employee leave
	*/
	public function actionManage(){
		$year = isset($_POST['year']) ? $_POST['year'] : null;
		$month = isset($_POST['month_number']) ? $_POST['month_number'] : null;
		$school_id = UtilityFunctions::ShowSchool() ? $_POST['school_id'] : UtilityFunctions::SchoolID();
		$school_detail = BasicInformation::model()->findByPk($school_id);
		$employee_leave_information = EmployeeLeave::model()->LeaveDetail($_POST);
		$employee_detail = EmployeeDetail::model()->findAll('school_id=:school_id AND status=:status',[':school_id'=>$school_id, ':status'=>1]);
		return $this->render('_manage_leave',['employee_detail'=>$employee_detail, 'employee_leave_information'=>$employee_leave_information, 'month'=>$month, 'year'=>$year, 'school_detail'=>$school_detail]);
	}
	/**
	* submit leave information
	*/
	public function actionSubmitLeave(){
		if(isset($_POST['employee_id_']) && isset($_POST['month_type'])){
			$month_type = $_POST['month_type'] != 100 && $_POST['month_type'] >=1 && $_POST['month_type'] <= 12  ? $_POST['month_type'] : null;
			$employee_array = $_POST['employee_id_'];
			$year = isset($_POST['year']) ? $_POST['year'] : null;
			$error_array = [];
			$transaction = Yii::app()->db->beginTransaction();
			$loop_size = $month_type ? 1 : 12;
			for ($i=0; $i < sizeof($employee_array) ; $i++) { 
				$employee_id = $employee_array[$i];
				for ($j=1; $j <= $loop_size ; $j++) {
					$month = $month_type ? $month_type : $j;
					if(isset($_POST['leave_day_'.$employee_id.'_'.$month]) && $_POST['leave_day_'.$employee_id.'_'.$month] != 0){
						$day = $_POST['leave_day_'.$employee_id.'_'.$month];
						$model = new EmployeeLeave;
						$model -> employee_id = $employee_id;
						$model -> year = $year;
						$model -> month = $month;
						$model -> leave_day = $day;
						if(!$model -> validate() || !$model -> save()){
							echo "<pre>";
							echo print_r($model->errors);
							exit;
							$error_array[] = 'false';
						}
					} 
				}

			}
			if(!in_array('false', $error_array)){
				$transaction->commit();
				return $this->redirect(['index']);
			}else{
				$transaction->rollback();
				throw new CHttpException(404,'The requested page does not exist.');
			}
		}
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new EmployeeLeave('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['EmployeeLeave']))
			$model->attributes=$_GET['EmployeeLeave'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=EmployeeLeave::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='employee-leave-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
