<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'grade-scale-form',
	'enableAjaxValidation'=>false,
)); ?>
<!-- 
<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?> -->
	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->dropDownListRow($model,'_employee_level',array("उ.मा.बि."=>"उ.मा.बि.","मा. बि."=>"मा. बि.","नि. मा. बि."=>"नि. मा. बि.","प्रा.शि."=>"प्रा.शि.","बाल शिक्षा"=>"बाल शिक्षा","लेखापाल"=>"लेखापाल","आया"=>"आया","अन्य"=>"अन्य",),array('placeholder'=>false)); ?>

			<?php echo $form->dropDownListRow($model,'grade',array(1=>'१ ग्रेड', 2=> '२ ग्रेड',3=>'३ ग्रेड',4=>'४ ग्रेड',5=>'५ ग्रेड',6=>'६ ग्रेड',7=>'७ ग्रेड',8=>'८ ग्रेड',9=>'९ ग्रेड',10=>'१० ग्रेड',11=>'११ ग्रेड'),['placeholder'=>'ग्रेड छन्नु होस श्रेणी']); ?>
		</div>
		<div class="span6">

			<?php echo $form->dropDownListRow($model,'sherni',array(1=>"प्रथम श्रेणी",2=>"दोस्रो श्रेणी",3=>"त्रितिय श्रेणी"),['placeholder'=>false]); ?>

			<?php echo $form->textFieldRow($model,'amount',array('maxlength'=>8)); ?>
		</div>
	</div>


	<?php 
	if(!$model->isNewRecord) 
		echo $form->dropDownListRow($model,'status',array(1=>"Active",0=>"In-Active")); ?>

	<!-- 	<?php echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_date',array('class'=>'span5')); ?> -->
		<br /><br />
	<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
