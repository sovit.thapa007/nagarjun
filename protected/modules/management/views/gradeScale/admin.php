<?php
$this->breadcrumbs=array(
	'Grade Scales'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List GradeScale','url'=>array('index')),
array('label'=>'Create GradeScale','url'=>array('create')),
);

?>
<div class="form-horizontal well">
<h2 class="text-center"><strong>ग्रेड तलब स्केल लिस्ट  </strong></h2>

	<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'grade-scale-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
			//'id',
			'_employee_level',

			[
				'name'=>'sherni',
				'value' => '$data->SherniTitle',
			],
			//'grade',
			[
				'name'=>'grade',
				'value' => '$data->GradeTitle',
			],
			'amount',
			[
				'name'=>'status',
				'value' => '$data->status == 1 ? "सकृय" : "निष्कृय"',
			],
			/*
			'created_by',
			'created_date',
			'updated_by',
			'updated_date',
			*/
	array(
	'class'=>'bootstrap.widgets.TbButtonColumn',
	),
	),
	)); ?>
</div>
