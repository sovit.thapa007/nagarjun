<?php
$this->breadcrumbs=array(
	'Grade Scales'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List GradeScale','url'=>array('index')),
array('label'=>'Create GradeScale','url'=>array('create')),
array('label'=>'View GradeScale','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage GradeScale','url'=>array('admin')),
);
?>

<h1>Update GradeScale <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>