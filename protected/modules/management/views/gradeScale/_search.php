<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->dropDownListRow($model,'_employee_level',array("उ.मा.बि."=>"उ.मा.बि.","मा. बि."=>"मा. बि.","नि. मा. बि."=>"नि. मा. बि.","प्रा.शि."=>"प्रा.शि.","बाल शिक्षा"=>"बाल शिक्षा","लेखापाल"=>"लेखापाल","आया"=>"आया","अन्य"=>"अन्य",),array('class'=>'input-large')); ?>

		<?php echo $form->textFieldRow($model,'sherni',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'grade',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'amount',array('class'=>'span5','maxlength'=>8)); ?>

		<?php echo $form->textFieldRow($model,'status',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_date',array('class'=>'span5')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
</div>

<?php $this->endWidget(); ?>
