<?php
$this->breadcrumbs=array(
	'Grade Scales'=>array('index'),
	$model->id,
);

?>

<div class="form-horizontal well form-horizontal">
	<table class="items table table-bordered" style="width:100%">
		<tr><td colspan="4" style="text-align: center; font-weight: bold;">ग्रेडको तलब स्केल</td></tr>
		<tr><td><?= $model->getAttributeLabel('_employee_level'); ?></td><td> : <?= $model->_employee_level; ?></td><td><?= $model->getAttributeLabel('sherni'); ?></td><td> : <?= $model->SherniTitle; ?></td></tr>
		<tr>
			<td><?= $model->getAttributeLabel('grade'); ?> </td><td><?= $model->GradeTitle; ?></td>
			<td><?= $model->getAttributeLabel('amount'); ?> </td><td><?= UtilityFunctions::NepaliNumber($model->amount); ?></td>
		</tr>
	</table>
</div>