<?php
$this->breadcrumbs=array(
	'पाठ्यपुस्तक विवणहरु'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'सुधार गर्नुस',
);

$this->menu=array(
array('label'=>'List Stationery','url'=>array('index')),
array('label'=>'Create Stationery','url'=>array('create')),
array('label'=>'View Stationery','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage Stationery','url'=>array('admin')),
);
?>

<div class="form-horizontal well">
	<h2 class="text-center"><strong>पाठ्यपुस्तक विवण सुधार गर्नुस </strong></h2>
	<?php echo $this->renderPartial('_update_form',array('model'=>$model)); ?>
</div>