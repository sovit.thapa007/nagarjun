<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'stationery-form',
	'enableAjaxValidation'=>false,
)); ?>

<!-- <p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?> -->

		<?php //echo $form->textFieldRow($model,'transaction',array('class'=>'span5')); ?>

		<div class="row-fluid">
			<?php 
			if(UtilityFunctions::ShowSchool()){
				echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => "title")),'id','title'),array('class'=>'span12', 'prompt'=>'Select School', 'required'=>'required')); 
			}else{
				echo $form->hiddenField($model,'school_id',array('value'=> UtilityFunctions::SchoolID()));
			}
			?>
		</div>

		<div class="row-fluid">
			<?php echo $form->numberFieldRow($model,'year',array('class'=>'span12')); ?>
		</div>
		<br />
		<?php //echo $form->textFieldRow($model,'school_id',array('class'=>'span5')); ?>


	    <table class="table table-bordered table-stripped" id="customFields">
	        <thead>
	            <tr>
	                <th>CLASS</th>
	                <th>STUDENT N0.</th>
	                <th>RATE</th>
	                <th>AMOUNT</th>
	                <th>BUTTON</th>
	            </tr>
	        </thead>
	        <tbody>
	            <tr>
	                <td><?php echo CHtml::activeTextField($model, 'class_id[]', array('class'=>'span12', 'placeholder'=>'CLASS')) ?></td>
	                <td><?php echo CHtml::activeNumberField($model, 'students_number[]', array('placeholder'=>'Student Number')) ?></td>
	                <td><?php echo CHtml::activeNumberField($model, 'rate[]', array('placeholder'=>'Rate Per Student')) ?></td>
	                <td><?php echo CHtml::activeTextField($model, 'amount[]', array('placeholder'=>'Total Amount')) ?></td>
	                <td><a href="javascript:void(0);" class="addCF btn"><i class="fa fa-plus"></i></a></td>
	            </tr>
	        </tbody>
	    </table>

		<?php echo $form->textAreaRow($model,'remarks',array('rows'=>6, 'cols'=>50, 'class'=>'span9')); ?>
		<br /><br />
	<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>


<script type="text/javascript">
	$(document).ready(function(){
	$(".addCF").click(function(){
		$("#customFields").append('<tr><td><input class="span12" placeholder="CLASS" name="Stationery[class][]" id="Stationery_class" type="text"></td><td><input placeholder="Student Number" name="Stationery[students_number][]" id="Stationery_students_number" type="number"></td><td><input placeholder="Rate Per Student" name="Stationery[rate][]" id="Stationery_rate" type="number"></td><td><input placeholder="Total Amount" name="Stationery[amount][]" id="Stationery_amount" type="text"></td><td><a href="javascript:void(0);" class="remCF btn"><i class="fa fa-minus"></i></a></td></tr>');
	});
    $("#customFields").on('click','.remCF',function(){
        $(this).parent().parent().remove();
    });
});
</script>
