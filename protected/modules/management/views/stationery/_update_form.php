<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'stationery-form',
	'enableAjaxValidation'=>false,
)); ?>

<!-- <p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?> -->

		<?php //echo $form->textFieldRow($model,'transaction',array('class'=>'span5')); ?>

		<div class="row-fluid">
			<?php 
			if(UtilityFunctions::ShowSchool()){
				echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => "title")),'id','title'),array('class'=>'span12', 'prompt'=>'Select School', 'required'=>'required')); 
			}else{
				echo $form->hiddenField($model,'school_id',array('value'=> UtilityFunctions::SchoolID()));
			}
			?>
		</div>
		<div class="row-fluid">
			<div class="span6">
				<?php echo $form->textFieldRow($model, 'class', array('class'=>'span12', 'placeholder'=>'CLASS')) ?>
				<?php echo $form->numberFieldRow($model, 'students_number', array('placeholder'=>'Student Number','class'=>'span12')) ?>
			</div>
			<div class="span6">
				<?php echo $form->numberFieldRow($model, 'rate', array('placeholder'=>'Rate Per Student','class'=>'span12')) ?>
				<?php echo $form->textFieldRow($model, 'amount', array('placeholder'=>'Total Amount','class'=>'span12')) ?>
			</div>
		</div>
		<?php echo $form->textAreaRow($model,'remarks',array('rows'=>6, 'cols'=>50, 'class'=>'span9')); ?>
		
		<?php 
		if(!$model->isNewRecord) 
			echo $form->dropDownListRow($model,'status',array(1=>"सकृय",0=>"निष्कृय")); ?>
		<!-- 

		<?php echo $form->textFieldRow($model,'status',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_date',array('class'=>'span5')); ?> -->
<br /><br />
	<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>

