<?php
$this->breadcrumbs=array(
	'पाठ्यपुस्तक खर्च विवण भर्नुस'=>array('create'),
	'पाठ्यपुस्तक',
);

$this->menu=array(
array('label'=>'List Stationery','url'=>array('index')),
array('label'=>'Create Stationery','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('stationery-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<div class="form-horizontal well">
	<h2 style="text-align: center;"><strong>पाठ्यपुस्तक को सूचीहरु</strong> </h2>

	<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'stationery-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
			//'id',
			//'transaction_id',
			[
				'name'=>'school_id',
				'value'=>'$data->school ? $data->school->title_nepali : ""',
			],
			'class_id',
			[
				'name'=>'rate',
				'value'=>'UtilityFunctions::NepaliNumber($data->rate)',
			],
			[
				'name'=>'students_number',
				'value'=>'UtilityFunctions::NepaliNumber($data->students_number)',
			],
			[
				'name'=>'amount',
				'value'=>'UtilityFunctions::NepaliNumber($data->amount)',
			],
			/*
			'remarks',
			'status',
			'created_by',
			'created_date',
			'updated_by',
			'updated_date',
			*/
	array(
	'class'=>'bootstrap.widgets.TbButtonColumn',
	),
	),
	)); ?>
</div>
