<?php
$this->breadcrumbs=array(
	'पाठ्यपुस्तक खर्च विवण'=>array('index'),
	'पाठ्यपुस्तक',
);

$this->menu=array(
array('label'=>'List Stationery','url'=>array('index')),
array('label'=>'Manage Stationery','url'=>array('admin')),
);
?>


<div class="form-horizontal well">
	<h2 class="text-center"><strong>पाठ्यपुस्तक खर्च विवण भर्नुस  </strong></h2>

	<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>