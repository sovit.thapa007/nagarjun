<?php
$this->breadcrumbs=array(
	'पाठ्यपुस्तक खर्च विवण'=>array('index'),
	'पाठ्यपुस्तक',
);

$this->menu=array(
array('label'=>'List Stationery','url'=>array('index')),
array('label'=>'Create Stationery','url'=>array('create')),
array('label'=>'Update Stationery','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Stationery','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Stationery','url'=>array('admin')),
);
?>

<div class="form-horizontal well">
	<h2 style="text-align: center;"><strong>पाठ्यपुस्तक को सूचीहरु विवण</strong> </h2>
	<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
			//'id',
			[
				'name'=>'transaction_id',
				'value'=>UtilityFunctions::NepaliNumber($model->transaction_id),
			],
			[
				'name'=>'school_id',
				'value'=>$model->school ? $model->school->title_nepali : "",
			],
			'class',
			[
				'name'=>'rate',
				'value'=>UtilityFunctions::NepaliNumber($model->rate),
			],
			[
				'name'=>'students_number',
				'value'=>UtilityFunctions::NepaliNumber($model->students_number),
			],
			[
				'name'=>'amount',
				'value'=>UtilityFunctions::NepaliNumber($model->amount),
			],
			'remarks',
			[
				'name'=>'status',
				'value'=>$model->status == 1 ? "सकृय" : "निष्कृय",
			],
			//'created_by',
			'created_date',
			//'updated_by',
			'updated_date',
	),
	)); ?>
</div>
