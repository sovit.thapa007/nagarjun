<?php
$this->breadcrumbs=array(
	'Stationeries',
);

$this->menu=array(
array('label'=>'Create Stationery','url'=>array('create')),
array('label'=>'Manage Stationery','url'=>array('admin')),
);
?>

<h1>Stationeries</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
