
<?php
/* @var $this DefaultController */

$this->breadcrumbs = array(
	$this->module->id,
);
?>
<div class="row-fluid quick-actions">
	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/management/employeeDetail/create" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/teacher.svg" alt="school register"></span>
			<h3><strong>कर्मचारी विवरण (शिक्षक/बाल शिक्षा/लेखापाल/आया/अन्य)</strong></h3>
		</a>
	</div>
	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/management/employeeDetail/admin" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/professor.svg" alt="school register"></span>
			<h3><strong>कर्मचारी तालिम</strong></h3>
		</a>
	</div>
	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/management/employeeDetail/admin" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/presentation.svg" alt="school register"></span>
			<h3><strong>कर्मचारी इतिहास</strong></h3>
		</a>
	</div>
	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/management/default/magform" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/profit.svg" alt="school register"></span>
			<h3><strong>माग फारम</strong></h3>
		</a>
	</div>
	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/management/employeeLeave/admin" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/desk.svg" alt="school register"></span>
			<h3><strong>बिदा प्रबिस्टि</strong></h3>
		</a>
	</div>
	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/management/basicScale/create" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/preview_ledger.svg" alt="school register"></span>
			<h3><strong>तलब स्केल</strong></h3>
		</a>
	</div>
	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/management/basicScale/create" class="quick-action">
		<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/preview_ledger.svg" alt="school register"></span>
		<h3><strong>ग्रेड स्केल</strong></h3>
		</a>
	</div>



	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/management/scholarship/create" class="quick-action">
		<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/hands.svg" alt="school register"></span>
		<h3><strong>छात्रबृति</strong></h3>
		</a>
	</div>

	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/management/stationery/create" class="quick-action">
		<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/books-stack-of-three.svg" alt="school register"></span>
		<h3><strong>पाठ्यपुस्तक</strong></h3>
		</a>
	</div>


	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/management/administrative/create" class="quick-action">
		<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/work-team.svg" alt="school register"></span>
		<h3><strong>ब्यबस्थापन खर्च</strong></h3>
		</a>
	</div>
	

	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/management/Infrastructure/create" class="quick-action">
		<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/infrastructure.svg" alt="school register"></span>
		<h3><strong>भौतिक निर्माण</strong></h3>
		</a>
	</div>
	
	
	<div class="span4">
		<a href="<?php echo Yii::app()->request->baseUrl; ?>/management/report/index" class="quick-action">
			<span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/report.svg" alt="school register"></span>
			<h3><strong>रिपोर्ट</strong></h3>
		</a>
	</div>
</div>


	