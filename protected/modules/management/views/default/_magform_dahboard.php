
<style type="text/css">label {
  display:block;
  border:solid 1px gray;
  line-height:40px;
  /*height:40px;
  width: 250px;*/
  border-radius:40px;
  -webkit-font-smoothing: antialiased; 
  margin-top:10px;
  font-family:Arial,Helvetica,sans-serif;
  color:gray;
  text-align:center;
}

input[type=radio] {
  display: none;
}

input:checked + label {
  border: solid 1px red;
  color: #F00;
}

input:checked + label:before {
content: "✓ ";
}


/* new stuff */
.check {
  visibility: hidden;
}

input:checked + label .check {
  visibility: visible;
}

input.checkbox:checked + label:before {
content: "";
}</style>
<?php
/* @var $this DefaultController */
$subject_id = 1;
$this->breadcrumbs = array(
	$this->module->id,
);
?>
<div class="form-horizontal well">
	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'grade-scale-form',
	'enableAjaxValidation'=>false,
	'action'=>Yii::app()->request->baseUrl.'/management/magform/dashboard',
	)); ?>
	<br /><br />
	<div class="row-fluid">
		<div class="span3"> आर्थिक बर्ष :  </div>
		<div class="span9">
		<select placeholder="" name="fiscal_year" id="Magform_fiscal_year">
			<option value="2075/2076"><?= UtilityFunctions::NepaliNumber('2075/2076'); ?></option>
			<option value="2076/2077"><?= UtilityFunctions::NepaliNumber('2076/2077'); ?></option>
			<option value="2077/2078"><?= UtilityFunctions::NepaliNumber('2077/2078'); ?></option>
		</select>
		</div>
	</div>
	<br /><br />
	<div class="row-fluid quick-actions">
		<div class="span4">
              <input type="radio" id='subject_id_class_1' name="chaumasik" value="1" class='subject_id_class' > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <label for="subject_id_class_1" style='margin-top:0px !important;'><span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/teacher.svg" alt="school register" height="50" width='50'></span> <br />प्रथम त्रैमासिक निकाशा</label>
		</div>
		<div class="span4">
              <input type="radio" id='subject_id_class_2' name="chaumasik" value="2" class='subject_id_class' > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <label for="subject_id_class_2" style='margin-top:0px !important;'><span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/teacher.svg" alt="school register" height="50" width='50'></span> <br />दोस्रो त्रैमासिक निकाशा</label>
		</div>
		<div class="span4">
              <input type="radio" id='subject_id_class_3' name="chaumasik" value="3" class='subject_id_class' > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <label for="subject_id_class_3" style='margin-top:0px !important;'><span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/teacher.svg" alt="school register" height="50" width='50'></span> <br />त्रितिय त्रैमासिक निकाशा</label>
		</div>
		<div class="span4">
              <input type="radio" id='subject_id_class_4' name="chaumasik" value="4" class='subject_id_class' > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <label for="subject_id_class_4" style='margin-top:0px !important;'><span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/teacher.svg" alt="school register" height="50" width='50'></span> <br />चौथो त्रैमासिक निकाशा</label>
		</div>
	</div>
	<br /><br />
	<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'खोज्नुहोस',
		)); ?>
</div>

<?php $this->endWidget(); ?>