<?php
$this->breadcrumbs=array(
	'ब्यबस्थापन खर्च'=>array('index'),
	'ब्यबस्थापन खर्च भर्नुस',
);

$this->menu=array(
array('label'=>'List Administrative','url'=>array('index')),
array('label'=>'Manage Administrative','url'=>array('admin')),
);
?>

<div class="form-horizontal well">
	<h2 class="text-center"><strong>ब्यबस्थापन खर्च विवण भर्नुस  </strong></h2>

	<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>