<?php
$this->breadcrumbs=array(
	'ब्यबस्थापन खर्च विवण थप्नुस'=>array('create'),
	'ब्यबस्थापन खर्च विवण',
);

$this->menu=array(
array('label'=>'List Administrative','url'=>array('index')),
array('label'=>'Create Administrative','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('administrative-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<div class="form-horizontal well">
	<h2 style="text-align: center;"><strong>ब्यबस्थापन खर्च को सूचीहरु</strong> </h2>
	<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'administrative-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
			//'id',
			//'transaction_id',
			//'school_id',
			[
				'name'=>'school_id',
				'value'=>'$data->school ? $data->school->title_nepali : ""',
			],
			'school_level',
			[
				'name'=>'amount',
				'value'=>'UtilityFunctions::NepaliNumber($data->amount)',
			],
			//'amount',
			'remarks',
			/*
			'status',
			'created_by',
			'created_date',
			'updated_by',
			'updated_date',
			*/
	array(
	'class'=>'bootstrap.widgets.TbButtonColumn',
	),
	),
	)); ?>
</div>
