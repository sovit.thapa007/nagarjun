<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'administrative-form',
	'enableAjaxValidation'=>false,
)); ?>

<!-- <p class="help-block">Fields with <span class="required">*</span> are required.</p> -->
<!-- 
<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'transaction_id',array('class'=>'span5')); ?>
 -->

 		<div class="row-fluid">
		<?php 
		if(UtilityFunctions::ShowSchool()){
			echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => "title_nepali")),'id','title_nepali'),array('class'=>'span12', 'prompt'=>'बिद्यालय छान्नुस', 'required'=>'required')); 
		}else{
			echo $form->hiddenField($model,'school_id',array('value'=> UtilityFunctions::SchoolID()));
		}
		?>
		</div>
		<div class="row-fluid">
			<div class="span6">
				<?php echo $form->textFieldRow($model,'school_level',array('class'=>'span12','maxlength'=>100)); ?>
			</div>
			<div class="span6">
				<?php echo $form->textFieldRow($model,'amount',array('class'=>'span12','maxlength'=>12)); ?> 
			</div>
		</div>

		<?php echo $form->textAreaRow($model,'remarks',array('rows'=>6, 'cols'=>50, 'class'=>'span9')); ?>

		<?php 
		if(!$model->isNewRecord) 
			echo $form->dropDownListRow($model,'status',array(1=>"सकृय",0=>"निष्कृय")); ?>

		<br /><br />
<!-- 

		<?php echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_date',array('class'=>'span5')); ?> -->

	<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>


<script type="text/javascript">
	$(document).ready(function(){
	$(".addCF").click(function(){
		$("#customFields").append('<tr><td><input class="span12" placeholder="बिद्यालयको तह" name="Administrative[school_level][]" id="Administrative_school_level" type="text"></td><td><input placeholder="कुल राशि" name="Administrative[amount][]" id="Administrative_amount" type="text"></td><td><a href="javascript:void(0);" class="remCF btn"><i class="fa fa-minus"></i></a></td></tr>');
	});
    $("#customFields").on('click','.remCF',function(){
        $(this).parent().parent().remove();
    });
});
</script>
