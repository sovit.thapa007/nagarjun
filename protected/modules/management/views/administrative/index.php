<?php
$this->breadcrumbs=array(
	'Administratives',
);

$this->menu=array(
array('label'=>'Create Administrative','url'=>array('create')),
array('label'=>'Manage Administrative','url'=>array('admin')),
);
?>

<h1>Administratives</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
