<?php
$this->breadcrumbs=array(
	'ब्यबस्थापन खर्च'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'सच्याउनुस',
);

$this->menu=array(
array('label'=>'List Administrative','url'=>array('index')),
array('label'=>'Create Administrative','url'=>array('create')),
array('label'=>'View Administrative','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage Administrative','url'=>array('admin')),
);
?>

<div class="form-horizontal well">
	<h2 class="text-center"><strong>ब्यबस्थापन खर्च सच्याउनुस </strong></h2>

	<?php echo $this->renderPartial('_update_form',array('model'=>$model)); ?>

</div>