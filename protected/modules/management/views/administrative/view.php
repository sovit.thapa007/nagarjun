<?php
$this->breadcrumbs=array(
	'ब्यबस्थापन खर्च विवण'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Administrative','url'=>array('index')),
array('label'=>'Create Administrative','url'=>array('create')),
array('label'=>'Update Administrative','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Administrative','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Administrative','url'=>array('admin')),
);
?>


<div class="form-horizontal well">
	<h2 style="text-align: center;"><strong>ब्यबस्थापन खर्च को विवरण</strong> </h2>
		<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'attributes'=>array(
				//'id',

				[
					'name'=>'transaction_id',
					'value'=>UtilityFunctions::NepaliNumber($model->transaction_id),
				],
				[
					'name'=>'school_id',
					'value'=>$model->school ? $model->school->title_nepali : "",
				],
				'school_level',
				[
					'name'=>'amount',
					'value'=>UtilityFunctions::NepaliNumber($model->amount),
				],
				[
					'name'=>'status',
					'value'=>$model->status == 1 ? "सकृय" : "निष्कृय",
				],
				//'status',
				//'created_by',
				'created_date',
				//'updated_by',
				'updated_date',
		),
		)); ?>

	</div>
