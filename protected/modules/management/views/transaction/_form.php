<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'transaction-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'school_id',array('class'=>'span5')); ?>

		<?php echo $form->dropDownListRow($model,'type',array("छात्रबृति"=>"छात्रबृति","पाठ्यपुस्तक"=>"पाठ्यपुस्तक","ब्यबस्थापन खर्च"=>"ब्यबस्थापन खर्च","भौतिक निर्माण"=>"भौतिक निर्माण",),array('class'=>'input-large')); ?>

		<?php echo $form->dropDownListRow($model,'fiscal_year',array("2075/2076"=>"2075/2076","2076/2077"=>"2076/2077","2077/2078"=>"2077/2078",),array('class'=>'input-large')); ?>

		<?php echo $form->textFieldRow($model,'trimasik',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'year',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'month',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'amount',array('class'=>'span5','maxlength'=>12)); ?>

		<?php echo $form->textAreaRow($model,'remark',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

		<?php echo $form->textFieldRow($model,'status',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'nepali_date',array('class'=>'span5','maxlength'=>20)); ?>

		<?php echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_date',array('class'=>'span5')); ?>

	<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
