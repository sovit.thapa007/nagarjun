<?php
$this->breadcrumbs=array(
	'Magform Employees'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List MagformEmployee','url'=>array('index')),
array('label'=>'Manage MagformEmployee','url'=>array('admin')),
);
?>

<h1>Create MagformEmployee</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>