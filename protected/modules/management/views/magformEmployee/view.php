<?php
$this->breadcrumbs=array(
	'Magform Employees'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List MagformEmployee','url'=>array('index')),
array('label'=>'Create MagformEmployee','url'=>array('create')),
array('label'=>'Update MagformEmployee','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete MagformEmployee','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage MagformEmployee','url'=>array('admin')),
);
?>

<h1>View MagformEmployee #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'magform_id',
		'employee_id',
		'type',
		'employee_name',
		'account_number',
		'employee_post',
		'fiscal_year',
		'chaumasik',
		'amount',
		'status',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
),
)); ?>
