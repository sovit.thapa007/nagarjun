<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'magform_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'employee_id',array('class'=>'span5')); ?>

		<?php echo $form->dropDownListRow($model,'type',array("salary"=>"salary","dasai_bonus"=>"dasai_bonus","cloth"=>"cloth","other"=>"other",),array('class'=>'input-large')); ?>

		<?php echo $form->textFieldRow($model,'employee_name',array('class'=>'span5','maxlength'=>250)); ?>

		<?php echo $form->textFieldRow($model,'account_number',array('class'=>'span5','maxlength'=>100)); ?>

		<?php echo $form->dropDownListRow($model,'employee_post',array("प्र.अ."=>"प्र.अ.","शिक्षक"=>"शिक्षक","बाल शिक्षा"=>"बाल शिक्षा","लेखापाल"=>"लेखापाल","आया"=>"आया","अन्य"=>"अन्य",),array('class'=>'input-large')); ?>

		<?php echo $form->dropDownListRow($model,'fiscal_year',array("2075/2076"=>"2075/2076","2076/2077"=>"2076/2077","2077/2078"=>"2077/2078",),array('class'=>'input-large')); ?>

		<?php echo $form->textFieldRow($model,'chaumasik',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'amount',array('class'=>'span5','maxlength'=>12)); ?>

		<?php echo $form->textFieldRow($model,'status',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_date',array('class'=>'span5')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
</div>

<?php $this->endWidget(); ?>
