<?php
$this->breadcrumbs=array(
	'Magform Employees',
);

$this->menu=array(
array('label'=>'Create MagformEmployee','url'=>array('create')),
array('label'=>'Manage MagformEmployee','url'=>array('admin')),
);
?>

<h1>Magform Employees</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
