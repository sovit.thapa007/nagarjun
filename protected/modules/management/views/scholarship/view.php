<?php
$this->breadcrumbs=array(
	'छात्रबृति को सूचीहरु'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Scholarship','url'=>array('index')),
array('label'=>'Create Scholarship','url'=>array('create')),
array('label'=>'Update Scholarship','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Scholarship','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Scholarship','url'=>array('admin')),
);
?>

<div class="form-horizontal well">
	<h2 style="text-align: center;"><strong>छात्रबृति को सूचीहरु</strong> </h2>

		<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'attributes'=>array(
				[
					'name'=>'school_id',
					'value'=>$model->school ? $model->school->title_nepali : "",
				],
				[
					'name'=>'transaction_id',
					'value'=>UtilityFunctions::NepaliNumber($model->transaction_id),
				],
				//'transaction_id',
				'type',
				[
					'name'=>'year',
					'value'=>UtilityFunctions::NepaliNumber($model->year),
				],
				'class',
				
				[
					'name'=>'students_number',
					'value'=>UtilityFunctions::NepaliNumber($model->students_number),
				],
				[
					'name'=>'rate',
					'value'=>UtilityFunctions::NepaliNumber($model->rate),
				],
				[
					'name'=>'amount',
					'value'=>UtilityFunctions::NepaliNumber($model->amount),
				],
				'remarks',
				[
					'name'=>'status',
					'value'=>$model->status == 1 ? "सकृय" : "निष्कृय",
				],
				//'created_by',
				'created_date',
				//'updated_by',
				'updated_date',
		),
		)); ?>
	</div>
