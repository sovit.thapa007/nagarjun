<?php
$this->breadcrumbs=array(
	'छात्रबृति विवण थप्नुस'=>array('create'),
	'छात्रबृति को सूचीहरु',
);

$this->menu=array(
array('label'=>'List Scholarship','url'=>array('index')),
array('label'=>'Create Scholarship','url'=>array('create')),
);

?>


<div class="form-horizontal well">
	<h2 style="text-align: center;"><strong>छात्रबृति को सूचीहरु</strong> </h2>

		<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'scholarship-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
				//'id',
				[
					'name'=>'school_id',
					'value'=>'$data->school ? $data->school->title_nepali : ""',
				],
				//'transaction_id',
				'type',
				[
					'name'=>'year',
					'value'=>'UtilityFunctions::NepaliNumber($data->year)',
				],
				'class',
				
				[
					'name'=>'students_number',
					'value'=>'UtilityFunctions::NepaliNumber($data->students_number)',
				],
				[
					'name'=>'rate',
					'value'=>'UtilityFunctions::NepaliNumber($data->rate)',
				],
				[
					'name'=>'amount',
					'value'=>'UtilityFunctions::NepaliNumber($data->amount)',
				],
				/*
				'remarks',
				'status',
				'created_by',
				'created_date',
				'updated_by',
				'updated_date',
				*/
		array(
		'class'=>'bootstrap.widgets.TbButtonColumn',
		),
		),
		)); ?>
</div>