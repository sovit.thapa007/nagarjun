<?php
$this->breadcrumbs=array(
	'छात्रबृति को सूचीहरु'=>array('index'),
	'थप्नुस',
);

$this->menu=array(
array('label'=>'List Scholarship','url'=>array('index')),
array('label'=>'Manage Scholarship','url'=>array('admin')),
);
?>

<div class="form-horizontal well">
	<h2 class="text-center"><strong>छात्रबृति विवण भर्नुस  </strong></h2>

	<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>