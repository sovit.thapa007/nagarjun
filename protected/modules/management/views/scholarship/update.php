<?php
$this->breadcrumbs=array(
	'छात्रबृति विवण'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'सुधार गर्नुस',
);

$this->menu=array(
array('label'=>'List Scholarship','url'=>array('index')),
array('label'=>'Create Scholarship','url'=>array('create')),
array('label'=>'View Scholarship','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage Scholarship','url'=>array('admin')),
);
?>
<div class="form-horizontal well">
	<h2 class="text-center"><strong>छात्रबृति विवण सुधार गर्नुस </strong></h2>

	<?php echo $this->renderPartial('_update_form',array('model'=>$model)); ?>
</div>