<?php
$this->breadcrumbs=array(
	'Magform Employee Details'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List MagformEmployeeDetails','url'=>array('index')),
array('label'=>'Create MagformEmployeeDetails','url'=>array('create')),
array('label'=>'Update MagformEmployeeDetails','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete MagformEmployeeDetails','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage MagformEmployeeDetails','url'=>array('admin')),
);
?>

<h1>View MagformEmployeeDetails #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'magform_employee_id',
		'employee_id',
		'type',
		'year',
		'month',
		'total_day',
		'sherni',
		'grade',
		'basic_scale',
		'basic_grade_scale',
		'pf_amount',
		'insurance_amount',
		'posak_bhatta',
		'bhatta',
		'pra_a_bhatta',
		'monthly_total',
		'state',
		'state_status',
		'status',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
),
)); ?>
