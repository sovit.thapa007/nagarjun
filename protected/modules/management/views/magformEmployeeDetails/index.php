<?php
$this->breadcrumbs=array(
	'Magform Employee Details',
);

$this->menu=array(
array('label'=>'Create MagformEmployeeDetails','url'=>array('create')),
array('label'=>'Manage MagformEmployeeDetails','url'=>array('admin')),
);
?>

<h1>Magform Employee Details</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
