<?php
$this->breadcrumbs=array(
	'Magform Employee Details'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List MagformEmployeeDetails','url'=>array('index')),
array('label'=>'Manage MagformEmployeeDetails','url'=>array('admin')),
);
?>

<h1>Create MagformEmployeeDetails</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>