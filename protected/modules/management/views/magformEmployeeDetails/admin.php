<?php
$this->breadcrumbs=array(
	'Magform Employee Details'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List MagformEmployeeDetails','url'=>array('index')),
array('label'=>'Create MagformEmployeeDetails','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('magform-employee-details-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Manage Magform Employee Details</h1>

<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
		&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'magform-employee-details-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'magform_employee_id',
		'employee_id',
		'type',
		'year',
		'month',
		/*
		'total_day',
		'sherni',
		'grade',
		'basic_scale',
		'basic_grade_scale',
		'pf_amount',
		'insurance_amount',
		'posak_bhatta',
		'bhatta',
		'pra_a_bhatta',
		'monthly_total',
		'state',
		'state_status',
		'status',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
		*/
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
