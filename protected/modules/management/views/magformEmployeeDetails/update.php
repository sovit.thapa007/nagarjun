<?php
$this->breadcrumbs=array(
	'Magform Employee Details'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List MagformEmployeeDetails','url'=>array('index')),
array('label'=>'Create MagformEmployeeDetails','url'=>array('create')),
array('label'=>'View MagformEmployeeDetails','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage MagformEmployeeDetails','url'=>array('admin')),
);
?>

<h1>Update MagformEmployeeDetails <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>