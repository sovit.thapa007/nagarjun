<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('magform_employee_id')); ?>:</b>
	<?php echo CHtml::encode($data->magform_employee_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('employee_id')); ?>:</b>
	<?php echo CHtml::encode($data->employee_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('year')); ?>:</b>
	<?php echo CHtml::encode($data->year); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('month')); ?>:</b>
	<?php echo CHtml::encode($data->month); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_day')); ?>:</b>
	<?php echo CHtml::encode($data->total_day); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('sherni')); ?>:</b>
	<?php echo CHtml::encode($data->sherni); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('grade')); ?>:</b>
	<?php echo CHtml::encode($data->grade); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('basic_scale')); ?>:</b>
	<?php echo CHtml::encode($data->basic_scale); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('basic_grade_scale')); ?>:</b>
	<?php echo CHtml::encode($data->basic_grade_scale); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pf_amount')); ?>:</b>
	<?php echo CHtml::encode($data->pf_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('insurance_amount')); ?>:</b>
	<?php echo CHtml::encode($data->insurance_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('posak_bhatta')); ?>:</b>
	<?php echo CHtml::encode($data->posak_bhatta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bhatta')); ?>:</b>
	<?php echo CHtml::encode($data->bhatta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pra_a_bhatta')); ?>:</b>
	<?php echo CHtml::encode($data->pra_a_bhatta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('monthly_total')); ?>:</b>
	<?php echo CHtml::encode($data->monthly_total); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('state')); ?>:</b>
	<?php echo CHtml::encode($data->state); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('state_status')); ?>:</b>
	<?php echo CHtml::encode($data->state_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>