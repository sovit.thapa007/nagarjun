<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'magform-employee-details-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'magform_employee_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'employee_id',array('class'=>'span5')); ?>

		<?php echo $form->dropDownListRow($model,'type',array("salary"=>"salary","dasai_bonus"=>"dasai_bonus","cloth"=>"cloth","other"=>"other",),array('class'=>'input-large')); ?>

		<?php echo $form->textFieldRow($model,'year',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'month',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'total_day',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'sherni',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'grade',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'basic_scale',array('class'=>'span5','maxlength'=>9)); ?>

		<?php echo $form->textFieldRow($model,'basic_grade_scale',array('class'=>'span5','maxlength'=>9)); ?>

		<?php echo $form->textFieldRow($model,'pf_amount',array('class'=>'span5','maxlength'=>9)); ?>

		<?php echo $form->textFieldRow($model,'insurance_amount',array('class'=>'span5','maxlength'=>9)); ?>

		<?php echo $form->textFieldRow($model,'posak_bhatta',array('class'=>'span5','maxlength'=>8)); ?>

		<?php echo $form->textFieldRow($model,'bhatta',array('class'=>'span5','maxlength'=>9)); ?>

		<?php echo $form->textFieldRow($model,'pra_a_bhatta',array('class'=>'span5','maxlength'=>9)); ?>

		<?php echo $form->textFieldRow($model,'monthly_total',array('class'=>'span5','maxlength'=>12)); ?>

		<?php echo $form->dropDownListRow($model,'state',array("school"=>"school","municipality"=>"municipality","teacher"=>"teacher",),array('class'=>'input-large')); ?>

		<?php echo $form->dropDownListRow($model,'state_status',array("send"=>"send","processing"=>"processing","pending"=>"pending","rejected"=>"rejected","approved"=>"approved",),array('class'=>'input-large')); ?>

		<?php echo $form->textFieldRow($model,'status',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_date',array('class'=>'span5')); ?>

	<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
