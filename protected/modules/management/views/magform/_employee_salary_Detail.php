
<?php
$this->breadcrumbs=array(
	'Employee Details'=>array('index'),
	$employee_detail->id,
);
?>
<div class="form-horizontal well form-horizontal">
	<h2 class="text-center"><STRONG>कर्मचारी बिस्तारि विवरणा </STRONG></h2>
	<table class="items table table-bordered" style="width:100%">
		<tbody>
			<tr>
				<td style="width: 30%">
					<img class="image" src="" data-zoom-image="" hegiht="200" width="200"></td>
				<td>
				<td>
					<table class="items table table-bordered" style="width:100%">
						<tr>
							<td> NAME (नाम)</td>
							<td> : <?= ucwords($employee_detail->first_name." ".$employee_detail->middle_name." ".$employee_detail->last_name).' ( '.$employee_detail->first_name_nepali." ".$employee_detail->middle_name_nepali." ".$employee_detail->last_name_nepali.' )'; ?></td>
							<td> <?= $employee_detail->getAttributeLabel('bank_name'); ?> : <?= $employee_detail->bank_name; ?></td>
						</tr>
						<tr>
							<td> <?= $employee_detail->getAttributeLabel('gender'); ?> </td>
							<td> : <?= $employee_detail->gender; ?></td>
							<td> <?= $employee_detail->getAttributeLabel('bank_account_number'); ?> : <?= UtilityFunctions::NepaliNumber($employee_detail->bank_account_number); ?> </td>
						</tr>
						<tr>
							<td> <?= $employee_detail->getAttributeLabel('type').'/'.$employee_detail->getAttributeLabel('level').'/'.$employee_detail->getAttributeLabel('post').'/'.$employee_detail->getAttributeLabel('grade'); ?></td>
							<td> : <?= $employee_detail->type.'/'.$employee_detail->level.'/'.$employee_detail->post.'/'.UtilityFunctions::NepaliNumber($employee_detail->grade); ?></td>

							<td> <?= $employee_detail->getAttributeLabel('insurance_number'); ?> : <?= UtilityFunctions::NepaliNumber($employee_detail->insurance_number); ?></td>
						</tr>
						<tr>
							<td> अस्थायी/स्थाई/बडुवा पदमा नियुक्ती मिति</td>
							<td> : <?= UtilityFunctions::NepaliNumber($employee_detail->temporary_assign_date).'/'.UtilityFunctions::NepaliNumber($employee_detail->permament_assign_date).'/'.UtilityFunctions::NepaliNumber($employee_detail->baduwa_miti); ?></td>
							<td> <?= $employee_detail->getAttributeLabel('pf_number'); ?> : <?= UtilityFunctions::NepaliNumber($employee_detail->pf_number); ?></td>
						</tr>
						<tr>
							<td> <?= $employee_detail->getAttributeLabel('maritual_status'); ?></td>
							<td> : <?= $employee_detail->maritual_status == 1 ? "विवाहित" : "अविवाहित"; ?></td>
							<td> <?= $employee_detail->getAttributeLabel('cif_number'); ?> : <?= $employee_detail->cif_number; ?></td>
						</tr>
						<tr>
							<td><?= $employee_detail->getAttributeLabel('contact_number'); ?></td>
							<td> : <?= $employee_detail->contact_number; ?></td>
							<td> <?= $employee_detail->getAttributeLabel('status'); ?> : <?= $employee_detail->status == '1' ?  "सकृय" : "निसकृय";  ?></td>
						</tr>
						<tr>
							<td colspan="3" style="text-align: center;"><?= isset($employee_detail->school_sec) ? $employee_detail->school_sec->title_nepali : 'null'; ?></td>
						</tr>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div class="form-horizontal well">
	<table id='datatable' class="table table-bordered" style="width: 100%;">
		<thead>
			<tr>
				<th>सि.न.</th>
				<th>प्रकार</th>
				<th>तह</th>
				<th>श्रेणी</th>
				<th>स्थायी/अस्थायी</th>
				<th>तलब</th>
				<th>ग्रेड संख्या</th>
				<th>ग्रेड दर</th>
				<th>ग्रेड समेत जम्मा तलब</th>
				<th>संचय कोष</th>
				<th>बिमा</th>
				<th>भत्ता</th>
				<th>१ महिनाको जम्मा</th>
				<th>कैफियत</th>
			</tr>
		</thead>
		<tbody>
			<?php
			if(!empty($employee_salary_Detail)){
				$sn = 1;
				foreach ($employee_salary_Detail as $employee_) {
				?>
				<tr>
					<td><?= UtilityFunctions::NepaliNumber($sn++); ?></td>	
					<td><?= UtilityFunctions::NikasiType($employee_->type); ?></td>
					<td><?= $employee_->employee ? $employee_->employee->level : ''; ?></td>	
					<td><?= UtilityFunctions::SherniNepali($employee_->sherni); ?></td>
					<td><?= $employee_->employee ?  $employee_->employee->type : ''; ?></td>
					<td><?= UtilityFunctions::NepaliNumber($employee_->basic_scale); ?></td>
					<td><?= UtilityFunctions::NepaliNumber($employee_->grade); ?></td>	
					<td><?= UtilityFunctions::NepaliNumber($employee_->basic_grade_scale); ?></td>
					<td><?= UtilityFunctions::NepaliNumber($employee_->basic_scale + $employee_->basic_grade_scale); ?></td>
					<td><?= UtilityFunctions::NepaliNumber($employee_->pf_amount); ?></td>
					<td><?= UtilityFunctions::NepaliNumber($employee_->insurance_amount); ?></td>
					<td><?= UtilityFunctions::NepaliNumber($employee_->bhatta + $employee_->posak_bhatta + $employee_->pra_a_bhatta); ?></td>
					<td><?= UtilityFunctions::NepaliNumber($employee_->monthly_total); ?></td>
					<td></td>
				</tr>
				<?php
				}
			}
			?>
		</tbody>	
		<tfoot>
			<tr>
			<th>सि.न.</th>
			<th>प्रकार</th>
			<th>तह</th>
			<th>श्रेणी</th>
			<th>स्थायी/अस्थायी</th>
			<th>तलब</th>
			<th>ग्रेड संख्या</th>
			<th>ग्रेड दर</th>
			<th>ग्रेड समेत जम्मा तलब</th>
			<th>संचय कोष</th>
			<th>बिमा</th>
			<th>भत्ता</th>
			<th>१ महिनाको जम्मा</th>
			<th>कैफियत</th>
			</tr>
		</tfoot>
	</table>
</div>


