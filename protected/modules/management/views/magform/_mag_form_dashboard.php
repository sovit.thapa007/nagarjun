<?php
$this->breadcrumbs=array(
	'Magforms'=>array('index'),
	'Manage',
);
?>
<div class="form-horizontal well">
	<h2 class="text-center"><strong>माग फारम सुची हरु </strong><a href="<?php echo Yii::app()->request->baseUrl; ?>/management/magform/create" style="float: right;" target="_blank"><strong>माग फारम भर्नुस</strong></a></h2>
	<?php
		$to_be_listed = isset(Yii::app()->params['chaumasik_feature'][$chaumasik])  ? Yii::app()->params['chaumasik_feature'][$chaumasik] : [];
		if(!empty($magform_count_information)){
			?>
			<table class="table table-bordered table-stripped">
				<thead>
      				<th>सि. न.</th>
      				<th>शीर्षक</th>
      				<th>बिद्यालय सन्ख्य</th>
      				<th>जम्मा रकम</th>
      				<th>बाटनहरु</th>
				</thead>
				<tbody>
				<?php
				$sn = 1;
				$chaumasik_keys = array_keys($to_be_listed);
				foreach ($magform_count_information as $magforminfo) {
					$type = $magforminfo->type;
					if(!in_array($type, $chaumasik_keys))
						continue;
					$title_type = isset($to_be_listed[$type]) ? $to_be_listed[$type] : '';
					?>
					<tr>
						<td><?= UtilityFunctions::NepaliNumber($sn++); ?></td>
						<td><?= $title_type; ?></td>
						<td><?= UtilityFunctions::NepaliNumber($magforminfo->school_id); ?></td>
						<td><?= UtilityFunctions::NepaliNumber($magforminfo->total_amount); ?></td>
						<td><a class="view" title="" data-toggle="tooltip" href="<?= Yii::app()->request->baseUrl; ?>/management/magform/schoolListing/?type=<?= $type; ?>&fiscal_year=<?= $fiscal_year; ?>&chaumasik=<?= $chaumasik; ?>" data-original-title="View"><i class="fa fa-eye"></i></a></td>
					</tr>
					<?php
				}
				?>
					
				</tbody>
			</table>
				<?php
		}
	?>
</div>