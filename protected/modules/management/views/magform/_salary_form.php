<?php
$this->breadcrumbs=array(
	'Magforms'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Magform','url'=>array('index')),
array('label'=>'Manage Magform','url'=>array('admin')),
);
?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'employee-detail-form',
	'type' => 'horizontal',
	'enableAjaxValidation'=>false,
	'action' => Yii::app()->baseUrl.'/management/magform/submitSalary',
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
	),
)); ?>
	<h2 class="text-center"><strong>तलब/खर्च माग फारम</strong><a href="<?= Yii::app()->baseUrl; ?>/management/magform/monthWiseSalaray/?school_id=<?= $school_detail->id; ?>&number=<?= $magform_detail->chaumasik; ?>&fiscal_year=<?= $magform_detail->fiscal_year; ?>&month=" style='float: right;'><strong>महिना-महिना को तलब/खर्च </strong></a></h2>
	
      <?php
        if(!empty($employee_info)){
          foreach ($employee_info as $employee_) {
            ?>
              <?= $employee_->level; ?> : <?= UtilityFunctions::NepaliNumber($employee_->id); ?> <br />
            <?php
          }
        }
        $pricipal = UtilityFunctions::PrincipleAccount($school_detail->id);
      	$pricipal_number = isset($pricipal['number']) ? UtilityFunctions::NepaliNumber($pricipal['number']) : '';
      	$accountant_ = UtilityFunctions::AccountantInformation($school_detail->id);
      	$accountant_number = isset($accountant_['number']) ? UtilityFunctions::NepaliNumber($accountant_['number']) : '';
      ?> 
	<table class="table table-bordered table-stripped">
		<tr><td colspan="14" style="font-size: 14pt !important; text-align: center;">
      <?php 
      echo Yii::app()->params['municipality_nepali'].'<br />'.Yii::app()->params['address_nepali'].', काठमाडौं <br /> आ. व. '.UtilityFunctions::NepaliNumber($magform_detail->fiscal_year).' '.UtilityFunctions::SherniNepali($magform_detail->chaumasik).' चैमासिक तलब/खर्च माग फारम'; ?>
      	
      </td></tr>
		<tr><td>बिद्यालय नाम</td> <td colspan="13"><?= $school_detail->title_nepali ? $school_detail->title_nepali : strtoupper($school_detail->title);  ?></td></tr>
		<tr><td>सन्चलित कक्षा</td> <td colspan="3">० देखी १२ </td><td colspan="3">बिद्यालयको खाता</td><td colspan="5"><?= UtilityFunctions::NepaliNumber($school_detail->bank_account_number).' ( '.$school_detail->bank_name.') <br /> लेखा प्रमुखको मो.न. : '.$accountant_number; ?>	</td><td>असाधारण बिदा बसेका शिक्षकहरूको नाम</td><td></td> </tr>
		<tr><td>प्रधानाध्यापकको नाम</td> <td colspan="4"><?= isset($priciple_['name']) ? UtilityFunctions::NepaliNumber($priciple_['name']).' ( '.$pricipal_number.' )' : '';?></td><td colspan="2">तह/श्रेणी</td><td colspan="5">-----/----</td><td>अबधि</td><td>मिति</td></tr>
		<tr><td rowspan="2">जम्मा स्वीकृत दरवन्दी </td><td>प्रा.शि</td><td>नि.मा.बि.</td><td>मा.बि.</td><td>उ.मा.बि.</td> <td rowspan='2'>राहत अनुदानको कोटा </td><td>प्रा.शि</td><td>नि.मा.बि.</td><td>मा.बि.</td><td>उ.मा.बि.</td><td rowspan="2">लियन/सट्ट सख्या </td> <td>तह</td><td rowspan="2">यस आ ब मा अनिबार्य अबकाश हुने शिक्षकको</td><td>नाम</td></tr>
		<tr><td><?= isset($employee_darbandi['total']['प्रा.शि.']) ? UtilityFunctions::NepaliNumber($employee_darbandi['total']['प्रा.शि.']) : ''; ?></td><td><?= isset($employee_darbandi['total']['नि.मा.बि.']) ? UtilityFunctions::NepaliNumber($employee_darbandi['total']['नि.मा.बि.']) : ''; ?></td><td><?= isset($employee_darbandi['total']['मा.बि.']) ? UtilityFunctions::NepaliNumber($employee_darbandi['total']['मा.बि.']) : ''; ?></td><td><?= isset($employee_darbandi['total']['उ.मा.बि.']) ? UtilityFunctions::NepaliNumber($employee_darbandi['total']['उ.मा.बि.']) : ''; ?></td> <td><?= isset($employee_darbandi['total']['प्रा.शि.']) ? UtilityFunctions::NepaliNumber($employee_darbandi['rahat']['प्रा.शि.']) : ''; ?></td><td><?= isset($employee_darbandi['rahat']['नि.मा.बि.']) ? UtilityFunctions::NepaliNumber($employee_darbandi['rahat']['नि.मा.बि.']) : ''; ?></td><td><?= isset($employee_darbandi['rahat']['मा.बि.']) ? UtilityFunctions::NepaliNumber($employee_darbandi['rahat']['मा.बि.']) : ''; ?></td><td><?= isset($employee_darbandi['rahat']['उ.मा.बि.']) ? UtilityFunctions::NepaliNumber($employee_darbandi['rahat']['उ.मा.बि.']) : ''; ?><td></td><td>अवकास मिती </td></tr>
	</table>
	
	<div class="row-fulid" style="overflow: auto;">
	<table class="table table-bordered table-stripped">
		<thead>
			<th>सि. न.</th>
			<th>संकेत नं. </th>
			<th>शिक्षकको नाम</th>
			<th>तह</th>
			<th>श्रेणी</th>
			<th>स्थायी/अस्थायी</th>
			<th>तलब</th>
			<th>ग्रेड संख्या</th>
			<th>ग्रेड दर</th>
			<th>ग्रेड समेत जम्मा तलब</th>
			<th>संचय कोष</th>
			<th>बिमा</th>
			<th>भत्ता</th>
			<th>१ महिनाको जम्मा</th>
			<th>३ महिनाको जम्मा</th>
			<th>कैफियत</th>
		</thead>
		<tbody>
			<?php
			if(!empty($employee_detail)){
				$sn = 1;
				foreach ($employee_detail as $employee_) {
				?>
				<tr>
					<td><?= UtilityFunctions::NepaliNumber($sn++); ?></td>
					<td><?= UtilityFunctions::NepaliNumber($employee_->id); ?></td>	
					<td><?= $employee_->first_name_nepali.' '.$employee_->middle_name_nepali.' '.$employee_->last_name_nepali; ?></td>		
					<td><?= $employee_->level; ?></td>	
					<td><?= UtilityFunctions::SherniNepali($employee_->sherni); ?></td>
					<td><?= $employee_->type; ?></td>
					<td><?= UtilityFunctions::NepaliNumber($employee_->BasicSalary); ?></td>
					<td><?= UtilityFunctions::NepaliNumber($employee_->grade); ?></td>	
					<td><?= UtilityFunctions::NepaliNumber($employee_->BasicGradeSalary); ?></td>
					<td><?= UtilityFunctions::NepaliNumber($employee_->Salary); ?></td>
					<td><?= UtilityFunctions::NepaliNumber($employee_->ProvidentFund); ?></td>
					<td><?= UtilityFunctions::NepaliNumber(Yii::app()->params['insurance_amount']); ?></td>
					<td><?= UtilityFunctions::NepaliNumber($employee_->Bonus); ?></td>
					<td><?= UtilityFunctions::NepaliNumber($employee_->MonthlySalary); ?></td>
					<td><?= UtilityFunctions::NepaliNumber($employee_->ThreeMonthlySalary); ?></td>
					<td></td>
				</tr>
				<?php
				}
			}
			?>
		</tbody>
	</table>
</div>	
<input type="hidden" name="magform_id" value="<?= $magform_detail->id; ?>">
<input type="hidden" name="school" value="<?= $school_id; ?>">
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'मागफारम पेस गर्नुस ',
		)); ?>
</div>

<?php $this->endWidget(); ?>