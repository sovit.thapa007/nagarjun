<?php
$this->breadcrumbs=array(
	'Magforms'=>array('index'),
	'Manage',
);
?>
<div class="form-horizontal well">
	<h2 class="text-center"><strong>माग फारम सुची हरु</strong></h2>
	<?php
		$to_be_listed = isset(Yii::app()->params['chaumasik_feature'][$chaumasik])  ? Yii::app()->params['chaumasik_feature'][$chaumasik] : [];
		if(!empty($school_listing)){
			?>
			<table class="table table-bordered table-stripped">
				<thead>
      				<th>सि. न.</th>
      				<th>बिद्यालय नाम</th>
      				<th>शीर्षक</th>
      				<th>जम्मा रकम</th>
      				<th>बाटनहरु</th>
				</thead>
				<tbody>
				<?php
				$sn = 1;
				foreach ($school_listing as $magforminfo) {
					$type = $magforminfo->type;
					$title_type = isset($to_be_listed[$type]) ? $to_be_listed[$type] : '';
					?>
					<tr>
						<td><?= UtilityFunctions::NepaliNumber($sn++); ?></td>
						<td><?= $magforminfo->school ? $magforminfo->school->title_nepali.' - '.UtilityFunctions::NepaliNumber($magforminfo->school->ward_no) : ''; ?></td>
						<td><?= $title_type; ?></td>
						<td><?= UtilityFunctions::NepaliNumber($magforminfo->total_amount); ?></td>
						<td><a class="view" title="" data-toggle="tooltip" href="<?= Yii::app()->request->baseUrl; ?>/management/magform/Detail/id/<?= $magforminfo->id; ?>" data-original-title="View"><i class="fa fa-eye"></i></a></td>
					</tr>
					<?php
				}
				?>
					
				</tbody>
			</table>
				<?php
		}
	?>
</div>