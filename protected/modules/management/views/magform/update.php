<?php
$this->breadcrumbs=array(
	'Magforms'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List Magform','url'=>array('index')),
array('label'=>'Create Magform','url'=>array('create')),
array('label'=>'View Magform','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage Magform','url'=>array('admin')),
);
?>

<h1>Update Magform <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>