<?php
$this->breadcrumbs=array(
	'माग फारम'=>array('index'),
	'सिर्जना गर्नुहोस्',
);

$this->menu=array(
array('label'=>'List Magform','url'=>array('index')),
array('label'=>'Manage Magform','url'=>array('admin')),
);
?>

<div class="form-horizontal well">
	<h2 class="text-center"><strong>माग फारम</strong></h2>
	<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>