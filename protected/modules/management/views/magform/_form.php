<?php
    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
  ?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'magform-form',
	'enableAjaxValidation'=>false,
)); ?>
<!-- 
	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?> -->

	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->dropDownListRow($model,'fiscal_year',array("2075/2076"=>"२०७५/२०७६","2076/2077"=>"२०७६/२०७७","2077/2078"=>"२०७७/२०७८"),['placeholder'=>false]); ?>
		</div>
		<div class="span6">
			<?php echo $form->dropDownListRow($model,'chaumasik',array(1=>"प्रथम त्रैमासिक",2=>"दोस्रो त्रैमासिक",3=>"त्रितिय त्रैमासिक", 4=>'चौथो त्रैमासिक'),['placeholder'=>false]); ?>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->dropDownListRow($model,'type',array("salary"=>"तलब/खर्चा माग फारम","dasai_bonus"=>"चाड/पर्व बोनस माग फारम"), ['placeholder'=>false]); ?>
			<!-- ,"cloth"=>"पोशक भात्ता माग फारम","other"=>"भात्ता माग फारम", -->
		</div>
		<div class="span6">
			<?php echo $form->textFieldRow($model,'total_amount',array('class'=>'span12','maxlength'=>12)); ?>
		</div>
	</div>

	<?php echo $form->textFieldRow($model,'header',array('class'=>'span5 nepaliUnicode','maxlength'=>250)); ?>

	<?php echo $form->textAreaRow($model,'content',array('rows'=>6, 'cols'=>50, 'class'=>'span9 nepaliUnicode')); ?>

	<div class="row-fluid">
	<?php 
	if(UtilityFunctions::ShowSchool()){
		echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => "title")),'id','title'),array('class'=>'span12', 'prompt'=>'Select School', 'required'=>'required')); 
	}else{
		echo $form->hiddenField($model,'school_id',array('value'=> UtilityFunctions::SchoolID()));
	}
	?>
	</div>
	<?php 
	if(!$model->isNewRecord)
		echo $form->dropDownListRow($model,'status',array(1=>"Active",0=>"De-Active")); ?>

	<!-- 
	<?php echo $form->dropDownListRow($model,'state',array("school"=>"school","municipality"=>"municipality","final"=>"final",),array('class'=>'input-large')); ?>

	<?php echo $form->dropDownListRow($model,'state_status',array("send"=>"send","processing"=>"processing","pending"=>"pending","rejected"=>"rejected","approved"=>"approved",),array('class'=>'input-large')); ?>


	<?php echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'updated_by',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'updated_date',array('class'=>'span5')); ?>
	-->
 <br /><br />
	<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>


  <!-- load jquery files -->
  <?php
  $cs->registerScriptFile($baseUrl . '/js/unicode/pramukhime.js');
  $cs->registerScriptFile($baseUrl . '/js/unicode/pramukhindic-i18n.js');
  $cs->registerScriptFile($baseUrl . '/js/unicode/pramukhindic.js');
  ?>
<script type="text/javascript">

    $(document).ready(function(){
      
    pramukhIME.addKeyboard("PramukhIndic");
    pramukhIME.enable();
    $(".nepaliUnicode").focus(function () {
    pramukhIME.setLanguage("nepali", "pramukhindic");
    });

    $(".nepaliUnicode").focusout(function () {
    pramukhIME.setLanguage("english", "pramukhime");
    });
    var chaumasik = $('#Magform_chaumasik').val();
    ChaumasikType(chaumasik);
    $('#Magform_chaumasik').change(function(){
    	var chaumasik = $(this).val();
    	ChaumasikType(chaumasik);
    });

    function ChaumasikType(chaumasik){
    	if(chaumasik == 2){
    		$('#Magform_type').html('<option value="salary">तलब/खर्चा माग फारम</option>');
    	}
    	if(chaumasik == 3){
    		$('#Magform_type').html('<option value="salary">तलब/खर्चा माग फारम</option><option value="cloth">पोशक भात्ता माग फारम</option>');
    	}
    	if(chaumasik == 1){
    		$('#Magform_type').html('<option value="salary">तलब/खर्चा माग फारम</option><option value="dasai_bonus">चाड/पर्व बोनस माग फारम</option>');
    	}
    }

});
</script>
