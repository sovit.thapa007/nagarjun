<?php
$this->breadcrumbs=array(
  'Magforms'=>array('index'),
  'Create',
);

$this->menu=array(
array('label'=>'List Magform','url'=>array('index')),
array('label'=>'Manage Magform','url'=>array('admin')),
);
?>
<div class="form-horizontal well">
  <div class="row-fluid">
  <h2 class="text-center"><strong>तलब/खर्चा माग फारम</strong></h2>

      <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#application" data-toggle="tab" aria-expanded="true">पत्र(निकाशा सम्बन्धमा)</a></li>
              <li class=""><a href="#magformdetail" data-toggle="tab" aria-expanded="false">माग फारम</a></li>
              <li class=""><a href="#bharpai" data-toggle="tab" aria-expanded="false">बुझिलिएको भर्पाई </a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="application">
                <div class="row-fluid" style="color: #0088cc;">
                  <div class="span3">
                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo/ministry_education.jpg" height='100' width='100'>
                    <br />
                    पत्र संख्या  :- <?= UtilityFunctions::NepaliNumber($magform_detail->id); ?> 
                    <br />
                    चलानी नं. :- <?= UtilityFunctions::NepaliNumber($magform_detail->fiscal_year); ?> 
                  </div>
                  <div class="span6" style="padding: 10px !important; font-weight: bold; font-size:14pt !important; text-align: center;">
                    <?= UtilityFunctions::MagformSchoolHeading($magform_detail->school_id); ?>
                    <?= UtilityFunctions::NepaliNumber($school_detail->establish_year);?>
                  </div>
                  <div class="span3" style="text-align: right;">
                    <br /><br /><br /><br />
                    नगार्जुन न. पा. <?= UtilityFunctions::NepaliNumber($school_detail->ward_no);?> <br />
                    सुचतर, काठमाडौं | <br />
                    मिती : <?php
                    $date = date('Y-m-d', strtotime($magform_detail->created_date));
                    echo UtilityFunctions::NepaliNumber(UtilityFunctions::EnglishToNepali($date)); ?>
                  </div>
                </div>
                <br /><br />
                <div class="row-fluid">
                  <div class="span2"></div>
                  <div class="span10" style="font-weight: bold;">बिषय : <?= $magform_detail->header; ?></div>
                </div><br />
                <div class="row-fluid">
                  <div class="span1"></div>
                  <div class="span10" style="text-align: justify-all;">
                    <?= $magform_detail->content; ?>
                  </div>
                  <div class="span1"></div>
                </div>

              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="magformdetail">
                <?php
                if($magform_detail->type =='salary')
                  echo $this->renderPartial('_salary_detail',['magform_detail'=>$magform_detail,'school_detail'=>$school_detail, 'employee_info'=>$employee_info]);
                if($magform_detail->type =='cloth')
                  echo $this->renderPartial('_clothes_bonus_detail',['magform_detail'=>$magform_detail,'school_detail'=>$school_detail, 'employee_info'=>$employee_info]);
                if($magform_detail->type =='dasai_bonus')
                  echo $this->renderPartial('_festiv_bonus_detail',['magform_detail'=>$magform_detail,'school_detail'=>$school_detail, 'employee_info'=>$employee_info]);

                ?>
               
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="bharpai">
                this is bharpai
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
        </div>
        </div>