<div class="row-fluid">
    <div class="row-fluid" style="color: #0088cc;">
      <div class="span3">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo/ministry_education.jpg" height='100' width='100'>
        <br />
        पत्र संख्या  :- <?= UtilityFunctions::NepaliNumber($magform_detail->id); ?> 
        <br />
        चलानी नं. :- <?= UtilityFunctions::NepaliNumber($magform_detail->fiscal_year); ?> 
      </div>
      <div class="span6" style="padding: 10px !important; font-weight: bold; font-size:14pt !important; text-align: center;">
        <?= UtilityFunctions::MagformSchoolHeading($magform_detail->school_id); ?>
        <?= UtilityFunctions::NepaliNumber($school_detail->establish_year);?>
      </div>
      <div class="span3" style="text-align: right;">
        <br /><br /><br /><br />
        नगार्जुन न. पा. <?= UtilityFunctions::NepaliNumber($school_detail->ward_no);?> <br />
        सुचतर, काठमाडौं | <br />
        मिती : <?php
        $date = date('Y-m-d', strtotime($magform_detail->created_date));
        echo UtilityFunctions::NepaliNumber(UtilityFunctions::EnglishToNepali($date)); ?>
      </div>
    </div>
    <br /><br />
    <div class="row-fluid">
      <div class="span2"></div>
      <div class="span10" style="font-weight: bold;">बिषय : <?= $magform_detail->header; ?></div>
    </div><br />
    <div class="row-fluid">
      <div class="span1"></div>
      <div class="span10" style="text-align: justify-all;">
        <?= $magform_detail->content; ?>
      </div>
      <div class="span1"></div>
    </div>

  </div>
  <div class="row-fluid">
    <table class="table table-bordered table-stripped">
      <thead>
        <th>सि. न.</th>
        <th>संकेत नं. </th>
        <th>शिक्षकको नाम</th>
        <th>तह</th>
        <th>श्रेणी</th>
        <th>स्थायी/अस्थायी</th>
        <th>तलब</th>
        <th>ग्रेड संख्या</th>
        <th>ग्रेड दर</th>
        <th>ग्रेड समेत जम्मा तलब</th>
        <th>संचय कोष</th>
        <th>बिमा</th>
        <th>भत्ता</th>
        <th>१ महिनाको जम्मा</th>
        <th>४ महिनाको जम्मा</th>
        <th>कैफियत</th>
      </thead>
      <tbody>
        <?php
        $employee_detail = $magform_detail->employee;
        if(!empty($employee_detail)){
          $sn = 1;
          foreach ($employee_detail as $employee_) {
            $employee_detail_information = $employee_->EmployeeDetails;
          ?>
          <tr>
            <td><?= UtilityFunctions::NepaliNumber($sn++); ?></td>
            <td><?= UtilityFunctions::NepaliNumber($employee_->id); ?></td> 
            <td><?= $employee_->EmpNepaliName; ?></td>    
            <td><?= $employee_->employee ? $employee_->employee->level : ''; ?></td>  
            <td><?= UtilityFunctions::SherniNepali($employee_detail_information->sherni); ?></td>
            <td><?= $employee_->employee ? $employee_->employee->type : ''; ?></td>
            <td><?= UtilityFunctions::NepaliNumber($employee_detail_information->basic_scale); ?></td>
            <td><?= UtilityFunctions::NepaliNumber($employee_detail_information->grade); ?></td>  
            <td><?= UtilityFunctions::NepaliNumber($employee_detail_information->basic_grade_scale); ?></td>
            <td><?= UtilityFunctions::NepaliNumber($employee_detail_information->basic_grade_scale); ?></td>
            <td><?= UtilityFunctions::NepaliNumber($employee_detail_information->pf_amount); ?></td>
            <td><?= UtilityFunctions::NepaliNumber($employee_detail_information->insurance_amount); ?></td>
            <td><?= UtilityFunctions::NepaliNumber($employee_detail_information->bhatta + $employee_detail_information->pra_a_bhatta); ?></td>
            <td><?= UtilityFunctions::NepaliNumber($employee_detail_information->monthly_total); ?></td>
            <td><?= UtilityFunctions::NepaliNumber($employee_->amount); ?></td>
            <td></td>
          </tr>
          <?php
          }
        }
        ?>
      </tbody>
    </table>
  </div>