<div class="row-fluid" style="color: #0088cc;">
    <div class="span3">
      <?php
        if(!empty($employee_info)){
          foreach ($employee_info as $employee_) {
            ?>
              <?= $employee_->level; ?> : <?= UtilityFunctions::NepaliNumber($employee_->id); ?> <br />
            <?php
          }
        }
      ?> 
    </div>
    <div class="span6" style="padding: 10px !important; font-weight: bold; font-size:14pt !important; text-align: center;">
      <?php 
      echo $school_detail->title_nepali.'<br />नागर्जुन - '.UtilityFunctions::NepaliNumber($school_detail->ward_no).', काठमाडौं <br /> आ. व. '.UtilityFunctions::NepaliNumber($magform_detail->fiscal_year).' '.UtilityFunctions::SherniNepali($magform_detail->chaumasik).' चैमासिक माग फारम'; ?>
    </div>
    <div class="span3" style="text-align: left;">
      बिद्यालय खाता नं. <?= UtilityFunctions::NepaliNumber($school_detail->bank_account_number); ?><br />  
      खाता रहेको बैंक :- <?= $school_detail->bank_name; ?><br />
      १) बिद्यालयको फोन नं <?= UtilityFunctions::NepaliNumber($school_detail->contact_number); ?><br />
      २) प्र.अ.को मो.न. <?php
      $priciple_ = UtilityFunctions::PrincipleAccount($school_detail->id);
      echo isset($priciple_['number']) ? UtilityFunctions::NepaliNumber($priciple_['number']) : '';
      ?><br />
      १) लेखा प्रमुखको मो.न.<?php
      $accountant_ = UtilityFunctions::AccountantInformation($school_detail->id);
      echo isset($accountant_['number']) ? UtilityFunctions::NepaliNumber($accountant_['number']) : '';
      ?><br />  
    </div>
  </div>
<h2 class="text-center"><strong>चाड/पर्व बोनस  माग फारम</strong></h2>
<table class="table table-bordered table-stripped">
	<thead>
		<th>सि. न.</th>
		<th>संकेत नं. </th>
		<th>शिक्षकको नाम</th>
		<th>खाता नम्बर </th>
		<th>तह</th>
		<th>श्रेणी</th>
		<th>स्थायी/अस्थायी</th>
		<th>तलब</th>
		<th>कैफियत</th>
	</thead>
	<tbody>
		<?php
      	$employee_detail = $magform_detail->employee;
		if(!empty($employee_detail)){
			$sn = 1;
			foreach ($employee_detail as $employee_) {
          	$employee_detail_information = $employee_->EmployeeDetails;
			?>
			<tr>
				<td><?= UtilityFunctions::NepaliNumber($sn++); ?></td>
				<td><?= UtilityFunctions::NepaliNumber($employee_->id); ?></td>	
      			<td><?= $employee_->EmpNepaliName; ?></td> 
				<td><?= UtilityFunctions::NepaliNumber($employee_->account_number); ?></td>	
          		<td><?= $employee_->employee ? $employee_->employee->level : ''; ?></td>  
          		<td><?= UtilityFunctions::SherniNepali($employee_detail_information->sherni); ?></td>
          		<td><?= $employee_->employee ? $employee_->employee->type : ''; ?></td>
          		<td><?= UtilityFunctions::NepaliNumber($employee_detail_information->basic_scale); ?></td>
				<td></td>
			</tr>
			<?php
			}
		}
		?>
	</tbody>
</table>