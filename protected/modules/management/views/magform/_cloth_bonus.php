<?php
$this->breadcrumbs=array(
	'Magforms'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Magform','url'=>array('index')),
array('label'=>'Manage Magform','url'=>array('admin')),
);
?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'employee-detail-form',
	'type' => 'horizontal',
	'enableAjaxValidation'=>false,
	'action' => Yii::app()->baseUrl.'/management/magform/submitClotheBonus',
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
	),
)); ?>
	<div class="row-fulid" style="overflow: auto;">
	<h2 class="text-center"><strong>पोशक भात्ता माग फारम</strong></h2>
	<table class="table table-bordered table-stripped">
		<thead>
			<th>सि. न.</th>
			<th>संकेत नं. </th>
			<th>शिक्षकको नाम</th>
			<th>तह</th>
			<th>खाता नम्बर </th>
			<th>पोशक भत्ता </th>
			<th>कैफियत</th>
		</thead>
		<tbody>
			<?php
			if(!empty($employee_detail)){
				$sn = 1;
				foreach ($employee_detail as $employee_) {
				?>
				<tr>
					<td><?= UtilityFunctions::NepaliNumber($sn++); ?></td>
					<td><?= UtilityFunctions::NepaliNumber($employee_->id); ?></td>	
					<td><?= $employee_->first_name_nepali.' '.$employee_->middle_name_nepali.' '.$employee_->last_name_nepali; ?></td>		
					<td><?= $employee_->level; ?></td>	
					<td><?= UtilityFunctions::NepaliNumber($employee_->bank_account_number); ?></td>
					<td><input type="number" name="posak_bhatta_<?= $employee_->id; ?>">
						<input type="hidden" name="employee_[]" value="<?= $employee_->id; ?>">
					</td>
					<td></td>
				</tr>
				<?php
				}
			}
			?>
		</tbody>
	</table>
</div>	
<input type="hidden" name="magform_id" value="<?= $magform_detail->id; ?>">
<input type="hidden" name="school" value="<?= $school_id; ?>">
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'मागफारम पेस गर्नुस ',
		)); ?>
</div>

<?php $this->endWidget(); ?>