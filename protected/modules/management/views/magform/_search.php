<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->dropDownListRow($model,'fiscal_year',array(''=>'आर्थिक वर्ष छन्नुहोस',"2075-2076"=>"२०७५/२०७६","2076-2077"=>"२०७६/२०७७","2077-2078"=>"२०७७/२०७८"),['placeholder'=>false]); ?>
		</div>
		<div class="span6">
			<?php echo $form->dropDownListRow($model,'chaumasik',array(''=>'माग फारमको प्रकार छन्नुहोस',1=>"प्रथम त्रैमासिक",2=>"दोस्रो त्रैमासिक",3=>"त्रितिय त्रैमासिक", 4=>'चौथो त्रैमासिक'),['placeholder'=>false]); ?>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->dropDownListRow($model,'type',array(''=>'त्रैमासिक छन्नुहोस', "salary"=>"तलब/खर्चा माग फारम","dasai_bonus"=>"चाड/पर्व बोनस माग फारम"), ['placeholder'=>false]); ?>
			<!-- ,"cloth"=>"पोशक भात्ता माग फारम","other"=>"भात्ता माग फारम", -->
		</div>
		<div class="span6">
			<?php echo $form->textFieldRow($model,'total_amount',array('class'=>'span12','maxlength'=>12)); ?>
		</div>
	</div>

	<?php echo $form->textFieldRow($model,'header',array('class'=>'span5 nepaliUnicode','maxlength'=>250)); ?>

	<?php echo $form->textAreaRow($model,'content',array('rows'=>6, 'cols'=>50, 'class'=>'span9 nepaliUnicode')); ?>

	<div class="row-fluid">
	<?php 
	if(UtilityFunctions::ShowSchool()){
		echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => "title")),'id','title'),array('class'=>'span12', 'prompt'=>'Select School', 'required'=>'required')); 
	}else{
		echo $form->hiddenField($model,'school_id',array('value'=> UtilityFunctions::SchoolID()));
	}
	?>
	</div>
	<?php 
	if(!$model->isNewRecord)
		echo $form->dropDownListRow($model,'status',array(1=>"Active",0=>"De-Active")); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
</div>

<?php $this->endWidget(); ?>
