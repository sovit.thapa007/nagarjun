
  <div class="row-fluid" style="color: #0088cc;">
    <div class="span3">
      <?php
        if(!empty($employee_info)){
          foreach ($employee_info as $employee_) {
            ?>
              <?= $employee_->level; ?> : <?= UtilityFunctions::NepaliNumber($employee_->id); ?> <br />
            <?php
          }
        }
      ?> 
    </div>
    <div class="span6" style="padding: 10px !important; font-weight: bold; font-size:14pt !important; text-align: center;">
      <?php 
      echo $school_detail->title_nepali.'<br />नागर्जुन - '.UtilityFunctions::NepaliNumber($school_detail->ward_no).', काठमाडौं <br /> आ. व. '.UtilityFunctions::NepaliNumber($magform_detail->fiscal_year).' '.UtilityFunctions::SherniNepali($magform_detail->chaumasik).' चैमासिक माग फारम'; ?>
    </div>
    <div class="span3" style="text-align: left;">
      बिद्यालय खाता नं. <?= UtilityFunctions::NepaliNumber($school_detail->bank_account_number); ?><br />  
      खाता रहेको बैंक :- <?= $school_detail->bank_name; ?><br />
      १) बिद्यालयको फोन नं <?= UtilityFunctions::NepaliNumber($school_detail->contact_number); ?><br />
      २) प्र.अ.को मो.न. <?php
      $priciple_ = UtilityFunctions::PrincipleAccount($school_detail->id);
      echo isset($priciple_['number']) ? UtilityFunctions::NepaliNumber($priciple_['number']) : '';
      ?><br />
      १) लेखा प्रमुखको मो.न.<?php
      $accountant_ = UtilityFunctions::AccountantInformation($school_detail->id);
      echo isset($accountant_['number']) ? UtilityFunctions::NepaliNumber($accountant_['number']) : '';
      ?><br />  
    </div>
  </div>

 <table class="table table-bordered table-stripped">
    <thead>
      <th>सि. न.</th>
      <!-- <th>संकेत नं. </th> -->
      <th>शिक्षकको नाम</th>
      <th>तह</th>
      <th>श्रेणी</th>
      <th>स्थायी/अस्थायी</th>
      <th>तलब</th>
      <th>ग्रेड संख्या</th>
      <th>ग्रेड दर</th>
      <th>ग्रेड समेत जम्मा तलब</th>
      <th>संचय कोष</th>
      <th>बिमा</th>
      <th>भत्ता</th>
      <th>१ महिनाको जम्मा</th>
      <th>४ महिनाको जम्मा</th>
      <th>कैफियत</th>
    </thead>
    <tbody>
      <?php
      $employee_detail = $magform_detail->employee;
      if(!empty($employee_detail)){
        $sn = 1;
        foreach ($employee_detail as $employee_) {
          $employee_detail_information = $employee_->EmployeeDetails;
        ?>
        <tr>
          <td><?= UtilityFunctions::NepaliNumber($sn++); ?></td>
          <!-- <td><?= UtilityFunctions::NepaliNumber($employee_->id); ?></td>  -->
          <td><?= $employee_->EmpNepaliName; ?></td>    
          <td><?= $employee_->employee ? $employee_->employee->level : ''; ?></td>  
          <td><?= UtilityFunctions::SherniNepali($employee_detail_information->sherni); ?></td>
          <td><?= $employee_->employee ? $employee_->employee->type : ''; ?></td>
          <td><?= UtilityFunctions::NepaliNumber($employee_detail_information->basic_scale); ?></td>
          <td><?= UtilityFunctions::NepaliNumber($employee_detail_information->grade); ?></td>  
          <td><?= UtilityFunctions::NepaliNumber($employee_detail_information->basic_grade_scale); ?></td>
          <td><?= UtilityFunctions::NepaliNumber($employee_detail_information->basic_grade_scale); ?></td>
          <td><?= UtilityFunctions::NepaliNumber($employee_detail_information->pf_amount); ?></td>
          <td><?= UtilityFunctions::NepaliNumber($employee_detail_information->insurance_amount); ?></td>
          <td><?= UtilityFunctions::NepaliNumber($employee_detail_information->bhatta + $employee_detail_information->pra_a_bhatta); ?></td>
          <td><?= UtilityFunctions::NepaliNumber($employee_detail_information->monthly_total); ?></td>
          <td><?= UtilityFunctions::NepaliNumber($employee_->amount); ?></td>
          <td></td>
        </tr>
        <?php
        }
      }
      ?>
    </tbody>
  </table>