
<style type="text/css">label {
  display:block;
  border:solid 1px gray;
  line-height:40px;
  height:40px;
  width: 250px;
  border-radius:40px;
  -webkit-font-smoothing: antialiased; 
  margin-top:10px;
  font-family:Arial,Helvetica,sans-serif;
  color:gray;
  text-align:center;
}

input[type=checkbox] {
  display: none;
}

input:checked + label {
  border: solid 1px red;
  color: #F00;
}

input:checked + label:before {
content: "✓ ";
}


/* new stuff */
.check {
  visibility: hidden;
}

input:checked + label .check {
  visibility: visible;
}

input.checkbox:checked + label:before {
content: "";
}</style>

<?php
$this->breadcrumbs=array(
	'Magforms'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Magform','url'=>array('index')),
array('label'=>'Manage Magform','url'=>array('admin')),
);
?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'employee-detail-form',
	'type' => 'horizontal',
	'enableAjaxValidation'=>false,
	'action' => Yii::app()->baseUrl.'/management/magform/submitmonthly',
	'htmlOptions' => array(
		'class' => 'form-horizontal well',
	),
)); ?>
		<h2 class="text-center"><strong>तलब/खर्च माग फारम</strong></h2>
		<?php
			if($status){
				?>
				<div class="alert <?= $status == 1 ? 'alert-success' : 'alert-danger'; ?> text-center"><?= $message; ?></div>
				<?php
			}	
        $pricipal = UtilityFunctions::PrincipleAccount($school_detail->id);
      	$pricipal_number = isset($pricipal['number']) ? UtilityFunctions::NepaliNumber($pricipal['number']) : '';
      	$accountant_ = UtilityFunctions::AccountantInformation($school_detail->id);
      	$accountant_number = isset($accountant_['number']) ? UtilityFunctions::NepaliNumber($accountant_['number']) : '';
		?>

	<table class="table table-bordered table-stripped">
		<tr><td colspan="14" style="font-size: 14pt !important; text-align: center;">
      <?php 
      echo Yii::app()->params['municipality_nepali'].'<br />'.Yii::app()->params['address_nepali'].', काठमाडौं <br /> आ. व. '.UtilityFunctions::NepaliNumber($fiscal_year).' '.UtilityFunctions::SherniNepali($traimasik_number).' चैमासिक तलब/खर्च माग फारम'; ?>
      	
      </td></tr>
		<tr><td>बिद्यालय नाम</td> <td colspan="13"><?= $school_detail->title_nepali ? $school_detail->title_nepali : strtoupper($school_detail->title);  ?></td></tr>
		<tr><td>सन्चलित कक्षा</td> <td colspan="3">० देखी १२ </td><td colspan="3">बिद्यालयको खाता</td><td colspan="5"><?= UtilityFunctions::NepaliNumber($school_detail->bank_account_number).' ( '.$school_detail->bank_name.') <br /> लेखा प्रमुखको मो.न. : '.$accountant_number; ?>	</td><td>असाधारण बिदा बसेका शिक्षकहरूको नाम</td><td></td> </tr>
		<tr><td>प्रधानाध्यापकको नाम</td> <td colspan="4"><?= isset($priciple_['name']) ? UtilityFunctions::NepaliNumber($priciple_['name']).' ( '.$pricipal_number.' )' : '';?></td><td colspan="2">तह/श्रेणी</td><td colspan="5">-----/----</td><td>अबधि</td><td>मिति</td></tr>
		<tr><td rowspan="2">जम्मा स्वीकृत दरवन्दी </td><td>प्रा.शि</td><td>नि.मा.बि.</td><td>मा.बि.</td><td>उ.मा.बि.</td> <td rowspan='2'>राहत अनुदानको कोटा </td><td>प्रा.शि</td><td>नि.मा.बि.</td><td>मा.बि.</td><td>उ.मा.बि.</td><td rowspan="2">लियन/सट्ट सख्या </td> <td>तह</td><td rowspan="2">यस आ ब मा अनिबार्य अबकाश हुने शिक्षकको</td><td>नाम</td></tr>
		<tr><td><?= isset($employee_darbandi['total']['प्रा.शि.']) ? UtilityFunctions::NepaliNumber($employee_darbandi['total']['प्रा.शि.']) : ''; ?></td><td><?= isset($employee_darbandi['total']['नि.मा.बि.']) ? UtilityFunctions::NepaliNumber($employee_darbandi['total']['नि.मा.बि.']) : ''; ?></td><td><?= isset($employee_darbandi['total']['मा.बि.']) ? UtilityFunctions::NepaliNumber($employee_darbandi['total']['मा.बि.']) : ''; ?></td><td><?= isset($employee_darbandi['total']['उ.मा.बि.']) ? UtilityFunctions::NepaliNumber($employee_darbandi['total']['उ.मा.बि.']) : ''; ?></td> <td><?= isset($employee_darbandi['total']['प्रा.शि.']) ? UtilityFunctions::NepaliNumber($employee_darbandi['rahat']['प्रा.शि.']) : ''; ?></td><td><?= isset($employee_darbandi['rahat']['नि.मा.बि.']) ? UtilityFunctions::NepaliNumber($employee_darbandi['rahat']['नि.मा.बि.']) : ''; ?></td><td><?= isset($employee_darbandi['rahat']['मा.बि.']) ? UtilityFunctions::NepaliNumber($employee_darbandi['rahat']['मा.बि.']) : ''; ?></td><td><?= isset($employee_darbandi['rahat']['उ.मा.बि.']) ? UtilityFunctions::NepaliNumber($employee_darbandi['rahat']['उ.मा.बि.']) : ''; ?><td></td><td>अवकास मिती </td></tr>
	</table>
		<div class="row-fluid" style="overflow: auto;">
					<table class="table table-bordered table-stripped">
					<thead>
						<th>सि. न.</th>
						<th>शिक्षकको नाम</th>
						<th>तह</th>
						<th>श्रेणी</th>
						<th>स्थायी/अस्थायी</th>
						<th>हाजिर दिन</th>
						<th>तलब</th>
						<th>ग्रेड संख्या</th>
						<th>ग्रेड दर</th>
						<th>ग्रेड समेत जम्मा तलब</th>
						<th>संचय कोष</th>
						<th>बिमा</th>
						<th>भत्ता</th>
						<th>१ महिनाको जम्मा</th>
						<th>कैफियत</th>
						<th>Action</th>
					</thead>
					<tbody>
						<?php
						$previous_month_icon = $previous_month ? 'fa fa-arrow-circle-left' : 'fa fa-ban';
						$next_month_icon = $next_month ? 'fa fa-arrow-circle-right' : 'fa fa-ban';
						$previous_month_value = $previous_month ? $previous_month : $month;
						$previous_month_value_ = $previous_month ?  UtilityFunctions::NepaliNumber($previous_month).' महिना (अघिल्लो महिना)' : UtilityFunctions::NepaliNumber($month).' महिना (अहिलेको महिना)';
						$next_month_value = $next_month ? $next_month : $month;
						$next_month_value_ = $next_month ?  UtilityFunctions::NepaliNumber($next_month).' महिना (आउने महिना)' : UtilityFunctions::NepaliNumber($month).' महिना (अहिलेको महिना)';
						if(!empty($employee_detail)){
							$sn = 1;
							$i = 1;
							$school_id = 0;
							foreach ($employee_detail as $employee_) {
								$school_id = $employee_->school_id;
								$employee_monthly_salary = MagformEmployeeDetails::model()->find('employee_id=:employee_id AND type=:type AND year=:year AND month=:month', [':employee_id'=>$employee_->id , ':type'=>'salary', ':year'=>$year, ':month'=>$month]);
								$presentdays_ = $employee_monthly_salary && $employee_monthly_salary->status == 1 ? $employee_monthly_salary->total_day : 30;
								$display_basicsalary_ = $employee_monthly_salary && $employee_monthly_salary->status == 1 ? $employee_monthly_salary->basic_scale : $employee_->BasicSalary;
								$basic_grade_ = $employee_monthly_salary && $employee_monthly_salary->status == 1 ? $employee_monthly_salary->basic_grade_scale : $employee_->BasicGradeSalary;
								$month_net_salary_ = $display_basicsalary_ + $basic_grade_;
								$Provident_fund_ = $employee_monthly_salary && $employee_monthly_salary->status == 1 ? $employee_monthly_salary->pf_amount : $employee_->ProvidentFund; 
								$insurance_ =  $employee_monthly_salary && $employee_monthly_salary->status == 1 ? $employee_monthly_salary->insurance_amount : Yii::app()->params['insurance_amount'];
								$bonus_ = $employee_monthly_salary && $employee_monthly_salary->status == 1 ? $employee_monthly_salary->bhatta + $employee_monthly_salary->pra_a_bhatta  : $employee_->Bonus;
								$total_salary_ =  $employee_monthly_salary && $employee_monthly_salary->status == 1 ? $employee_monthly_salary->monthly_total : $employee_->MonthlySalary;

								$status = empty($employee_monthly_salary) || ($employee_monthly_salary && $employee_monthly_salary->status == 1) ? 'checked' : false;
							?>
							<tr id="employee_row_<?= $i; ?>">
								<td><?= UtilityFunctions::NepaliNumber($sn); ?></td>
								<td>
    								<input type="checkbox" name="employee_id_array_[]" value="<?= $employee_->id; ?>" id="employee_name_<?= $i; ?>" <?= $status; ?>><label for="employee_name_<?= $i; ?>"><?= $employee_->first_name_nepali.' '.$employee_->middle_name_nepali.' '.$employee_->last_name_nepali; ?></label> 
    							</td>		
								<td><?= $employee_->level; ?></td>	
								<td><?= UtilityFunctions::SherniNepali($employee_->sherni); ?></td>
								<td><?= $employee_->type; ?></td>
								<td><input type="number" class="present_days_" id="presentdays_<?= $i; ?>" name="present_days_<?= $employee_->id; ?>"  value="<?= $presentdays_; ?>" max='30' style='width: 70px;'></td>	
								<td><p  id="display_basicsalary_<?= $i; ?>"><?= UtilityFunctions::NepaliNumber($display_basicsalary_); ?></p><input type="hidden" name="basicsalary_<?= $i; ?>" value="<?= $display_basicsalary_; ?>"></td>
								<td><?= UtilityFunctions::NepaliNumber($employee_->grade); ?></td>	
								<td><p  id="basic_grade_<?= $i; ?>"><?= UtilityFunctions::NepaliNumber($basic_grade_); ?></p></td>
								<td><p  id="month_net_salary_<?= $i; ?>"><?= UtilityFunctions::NepaliNumber($month_net_salary_); ?></p></td>
								<td><p  id="Provident_fund_<?= $i; ?>"><?= UtilityFunctions::NepaliNumber($Provident_fund_); ?></p></td>
								<td><p  id="insurance_<?= $i; ?>"><?= UtilityFunctions::NepaliNumber($insurance_); ?></p></td>
								<td><p  id="bonus_<?= $i; ?>"><?= UtilityFunctions::NepaliNumber($bonus_); ?></p></td>
								<td><p  id="total_salary_<?= $i; ?>"><?= UtilityFunctions::NepaliNumber($total_salary_); ?></p></td>
								<td><input type="hidden" name="employee_id_<?= $i; ?>" id="employee_id_<?= $i; ?>" value="<?= $employee_->id; ?>">
								<p id="remarks_<?= $i ?>"></p>	
								</td>	
								<td><button class="btn btn-sm btn-primary remove_filed" id="remove_<?= $i; ?>"><i class="fa fa-trash"></button></td>
							</tr>
							<?php
							$sn++;
							$i++;
							}
						}


						?>
					</tbody>
				</table>

	</div>
	<div class="row-fluid">
	<div class="span2"></div>
	<div class="span4">
	<input type="hidden" name="employee_school_id" value="<?= $school_id; ?>">
    <input type="checkbox" name="another_month" class="another_month" value="<?= $previous_month_value; ?>" id="previous_month_value"><label for="previous_month_value"><?= $previous_month_value_; ?> <i class="<?= $previous_month_icon; ?>"> </i></label> </div>
	<div class="span4">
    <input type="checkbox" name="another_month" class="another_month" value="<?= $next_month_value; ?>" id="next_month_value"><label for="next_month_value"><?= $next_month_value_; ?> <i class="<?= $next_month_icon; ?>"> </i></label></div>
	<div class="span2"></div>
	</div>
	<input type="hidden" name="current_month" value="<?= $month; ?>">
	<input type="hidden" name="traimasik_number" value="<?= $traimasik_number; ?>">
	<input type="hidden" name="fiscal_year" value="<?= $fiscal_year; ?>">
	<div class="form-actions text-center">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Submit Information',
		)); ?>
	</div>
<?php $this->endWidget(); ?>
<script type="text/javascript">
	/*$(".remove_filed").click(function(){
        $("table tbody").find('input[name="record"]').each(function(){
            if($(this).is(":checked")){
                $(this).parents("tr").remove();
            }
        });
    });
*/
	$(document).on('click', '.remove_filed', function () {
     $(this).parents('tr').remove();
	     return false;
	 });

	$(".another_month").change(function() {
	    $(".another_month").not(this).prop('checked', false);
	});
	$('.present_days_').on('keyup', function(){
		var present_id_information = $(this).attr('id').split("_");
		var id_number = present_id_information[1];
		var employee_id = $('#employee_id_'+id_number).val();
		var presentdays_ = $('#presentdays_'+id_number).val();
        $.ajax({
	        type: 'POST',
	        url: "<?= Yii::app()->baseUrl; ?>/management/magform/calculateDayWiseSalaray",
	        dataType: 'json',
	        data : {employee_id:employee_id,days:presentdays_,YII_CSRF_TOKEN: $('input[name="YII_CSRF_TOKEN"]').val()},
	        async: false, //This is deprecated in the latest version of jquery must use now callbacks
	        success: function(response) {
            	$('#display_basicsalary_'+id_number).html(response.basic_salary_nepali);
            	$('#basic_grade_'+id_number).html(response.grade_salary_nepali);
            	$('#month_net_salary_'+id_number).html(response.net_salary_nepali);
            	$('#Provident_fund_'+id_number).html(response.provident_fund_nepali);
            	$('#insurance_'+id_number).html(response.insurance_amount_nepali);
            	$('#bonus_'+id_number).html(response.general_bhatta_nepali);
            	$('#total_salary_'+id_number).html(response.total_salary);
            	$('#remarks_'+id_number).html(response.days + " दिन");
            }
        })
	})
</script>	