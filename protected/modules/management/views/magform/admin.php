<?php
$this->breadcrumbs=array(
	'Magforms'=>array('index'),
	'Manage',
);
/*
$this->menu=array(
array('label'=>'List Magform','url'=>array('index')),
array('label'=>'Create Magform','url'=>array('create')),
);*/

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('magform-grid', {
data: $(this).serialize()
});
return false;
});
");
?>


<div class="form-horizontal well">
	<h2 class="text-center"><strong>माग फारमको लिस्ट</strong></h2>
	<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
	<div class="search-form" style="display:none">
		<?php $this->renderPartial('_search',array(
		'model'=>$model,
	)); ?>
	</div><!-- search-form -->

	<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'magform-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
			//'id',
			//'school_id',
			[
				'name'=>'fiscal_year',
				'value' => 'UtilityFunctions::NepaliNumber($data->fiscal_year)',
			],
			//'fiscal_year',

			[
				'name'=>'chaumasik',
				'value' => 'UtilityFunctions::Traimasik($data->chaumasik)',
			],
			[
				'name'=>'type',
				'value' => '$data->type = "salary" ? "तलब/खर्चा माग फारम" : "चाड/पर्व बोनस माग फारम"',
			],
			'header',
			//'content',

			[
				'name'=>'total_amount',
				'value' => 'UtilityFunctions::NepaliNumber($data->total_amount)',
	             'footer'=>'जम्मा रु : ' . $model->getTotal($model->search()->getData(), 'total_amount'),
	             'footerHtmlOptions'=>array('class'=>'grid-footer'),
			],
			/*
			'status',
			'state',
			'state_status',
			'created_by',
			'created_date',
			'updated_by',
			'updated_date',
			*/
	array(
		'header'=>'Action',
	'class'=>'bootstrap.widgets.TbButtonColumn',
	),
	),
	)); ?>
</div>
