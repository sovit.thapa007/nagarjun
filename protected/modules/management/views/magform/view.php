<?php
$this->breadcrumbs=array(
	'Magforms'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Magform','url'=>array('index')),
array('label'=>'Create Magform','url'=>array('create')),
array('label'=>'Update Magform','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Magform','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Magform','url'=>array('admin')),
);
?>
<div class="form-horizontal well form-horizontal">
	<h2><STRONG>माग फारम विवरण</STRONG></h2>
	<table class="items table table-bordered" style="width:100%">
		<tbody>
			<tr>
				<td style="width: 30%">
					<img class="image" src="" data-zoom-image="" hegiht="200" width="200"></td>
				<td>
					<table class="items table table-bordered" style="width:100%">
						<tr>
							<td><?= $model->getAttributeLabel('fiscal_year'); ?></td>
							<td> : <?= $model->fiscal_year; ?></td>
							<td> Chaumasik </td>
							<td> : <?= $model->chaumasik; ?></td>
						</tr>
						<tr>
							<td> <?= $model->getAttributeLabel('type'); ?> </td>
							<td> : <?= $model->type; ?></td>
							<td> Amount </td>
							<td> : <?= $model->total_amount; ?></td>
						</tr>
						<tr>
							<td colspan="4"> <?= $model->getAttributeLabel('header'); ?> : <?= $model->header; ?> </td>
						</tr>
						<tr>
							<td colspan="4"> <?= $model->content; ?> </td>
						</tr>

						<tr>
							<td>  <?= $model->getAttributeLabel('state'); ?> </td>
							<td> : <?= $model->state; ?></td>
							<td> <?= $model->getAttributeLabel('state_status'); ?> </td>
							<td> : <?= $model->state_status; ?></td>
						</tr>
						<tr>
							<td> <?= $model->getAttributeLabel('school_id'); ?> </td>
							<td colspan="3">: <?= isset($model->school) ? $model->school->title : 'null'; ?></td>
						</tr>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
	<div class="row-fluid">
		<a href="<?php echo Yii::app()->baseUrl; ?>/management/magform/customizeForm/id/<?= $model->id; ?>" class='btn btn-primary'>माग फारम भर्नुस</a>
	</div>
	</table>
</div>
