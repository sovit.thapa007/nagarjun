<?php
$this->breadcrumbs=array(
	'Magforms',
);

$this->menu=array(
array('label'=>'Create Magform','url'=>array('create')),
array('label'=>'Manage Magform','url'=>array('admin')),
);
?>

<h1>Magforms</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
