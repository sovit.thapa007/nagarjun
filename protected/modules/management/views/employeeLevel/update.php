<?php
$this->breadcrumbs=array(
	'Employee Levels'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List EmployeeLevel','url'=>array('index')),
array('label'=>'Create EmployeeLevel','url'=>array('create')),
array('label'=>'View EmployeeLevel','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage EmployeeLevel','url'=>array('admin')),
);
?>

<h1>Update EmployeeLevel <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>