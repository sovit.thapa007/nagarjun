<?php
$this->breadcrumbs=array(
	'Employee Levels'=>array('index'),
	$model->title,
);

$this->menu=array(
array('label'=>'List EmployeeLevel','url'=>array('index')),
array('label'=>'Create EmployeeLevel','url'=>array('create')),
array('label'=>'Update EmployeeLevel','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete EmployeeLevel','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage EmployeeLevel','url'=>array('admin')),
);
?>

<h1>View EmployeeLevel #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'title',
		'status',
		'created_by',
		'created_date',
),
)); ?>
