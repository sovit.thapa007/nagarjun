<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'first_name',array('class'=>'span5','maxlength'=>200)); ?>

		<?php echo $form->textFieldRow($model,'middle_name',array('class'=>'span5','maxlength'=>200)); ?>

		<?php echo $form->textFieldRow($model,'last_name',array('class'=>'span5','maxlength'=>200)); ?>

		<?php echo $form->textFieldRow($model,'first_name_nepali',array('class'=>'span5','maxlength'=>200)); ?>

		<?php echo $form->textFieldRow($model,'middle_name_nepali',array('class'=>'span5','maxlength'=>200)); ?>

		<?php echo $form->textFieldRow($model,'last_name_nepali',array('class'=>'span5','maxlength'=>200)); ?>

		<?php echo $form->dropDownListRow($model,'gender',array("अन्य"=>"अन्य","महिला"=>"महिला","पुरुष"=>"पुरुष",),array('class'=>'input-large')); ?>

		<?php echo $form->dropDownListRow($model,'type',array("स्थायी"=>"स्थायी","अस्थायी"=>"अस्थायी","राहत"=>"राहत",),array('class'=>'input-large')); ?>

		<?php echo $form->dropDownListRow($model,'level',array("मा. बि."=>"मा. बि.","नि. मा. बि."=>"नि. मा. बि.","बाल शिक्षा"=>"बाल शिक्षा","लेखापाल"=>"लेखापाल","आया"=>"आया","अन्य"=>"अन्य",),array('class'=>'input-large')); ?>

		<?php echo $form->dropDownListRow($model,'post',array("शिक्षक"=>"शिक्षक","प्र.अ."=>"प्र.अ.","अन्य"=>"अन्य",),array('class'=>'input-large')); ?>

		<?php echo $form->textFieldRow($model,'grade',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'cast_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'ethnicity_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'maritual_status',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'contact_number',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'temporary_address',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'permanent_address',array('class'=>'span5')); ?>

		<?php echo $form->datepickerRow($model,'temporary_assign_date',array('options'=>array(),'htmlOptions'=>array('class'=>'span5')),array('prepend'=>'<i class="icon-calendar"></i>','append'=>'Click on Month/Year at top to select a different year or type in (mm/dd/yyyy).')); ?>

		<?php echo $form->datepickerRow($model,'permament_assign_date',array('options'=>array(),'htmlOptions'=>array('class'=>'span5')),array('prepend'=>'<i class="icon-calendar"></i>','append'=>'Click on Month/Year at top to select a different year or type in (mm/dd/yyyy).')); ?>

		<?php echo $form->textFieldRow($model,'insurance_number',array('class'=>'span5','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'pf_number',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'bank_name',array('class'=>'span5','maxlength'=>200)); ?>

		<?php echo $form->textFieldRow($model,'bank_account_number',array('class'=>'span5','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'cif_number',array('class'=>'span5','maxlength'=>50)); ?>

		<?php echo $form->textAreaRow($model,'remarks',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

		<?php echo $form->textFieldRow($model,'status',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_date',array('class'=>'span5')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
</div>

<?php $this->endWidget(); ?>
