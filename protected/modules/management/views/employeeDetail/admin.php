<?php
$this->breadcrumbs=array(
	'कर्मचारी बिस्तार'=>array('index'),
	'सूची',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('employee-detail-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<div class="form-horizontal well">
	<h2 style="text-align: center;"><strong>कर्मचारीहरुको सूची</strong> </h2>

<!-- <?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div> search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'employee-detail-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
		[
			'name'=>'first_name_nepali',
			'header'=>'नाम',
			'value' => '$data->first_name_nepali." ".$data->middle_name_nepali." ".$data->last_name_nepali',
		],/*
		'first_name',
		'middle_name',
		'last_name',
		'first_name_nepali',
		'middle_name_nepali',
		'last_name_nepali',*/
		//'gender',
		//'type',
		'level',
		'post',
		[
			'name'=>'grade',
			'value' => 'UtilityFunctions::NepaliNumber($data->grade)',
		],
		//'cast_id',
		//'ethnicity_id',
		//'maritual_status',
		[
			'name'=>'contact_number',
			'value' => 'UtilityFunctions::NepaliNumber($data->contact_number)',
		],
		//'temporary_address',
		//'permanent_address',
		//'temporary_assign_date',
		//'permament_assign_date',
		//'insurance_number',
		/*[
			'name'=>'pf_number',
			'value' => 'UtilityFunctions::NepaliNumber($data->pf_number)',
		],
		'bank_name',
		[
			'name'=>'bank_account_number',
			'value' => 'UtilityFunctions::NepaliNumber($data->bank_account_number)',
		],*/
		//'cif_number',
		//'remarks',

		[
			'name'=>'status',
			'value' => '$data->status == 1 ?  "सकृय" : "निसकृय"',
		],

		/*'status',
		'created_date',
		'created_by',
		'updated_by',
		'updated_date',*/
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
</div>
