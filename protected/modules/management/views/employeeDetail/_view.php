<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_name')); ?>:</b>
	<?php echo CHtml::encode($data->first_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('middle_name')); ?>:</b>
	<?php echo CHtml::encode($data->middle_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_name')); ?>:</b>
	<?php echo CHtml::encode($data->last_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_name_nepali')); ?>:</b>
	<?php echo CHtml::encode($data->first_name_nepali); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('middle_name_nepali')); ?>:</b>
	<?php echo CHtml::encode($data->middle_name_nepali); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_name_nepali')); ?>:</b>
	<?php echo CHtml::encode($data->last_name_nepali); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('gender')); ?>:</b>
	<?php echo CHtml::encode($data->gender); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('level')); ?>:</b>
	<?php echo CHtml::encode($data->level); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('post')); ?>:</b>
	<?php echo CHtml::encode($data->post); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('grade')); ?>:</b>
	<?php echo CHtml::encode($data->grade); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cast_id')); ?>:</b>
	<?php echo CHtml::encode($data->cast_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ethnicity_id')); ?>:</b>
	<?php echo CHtml::encode($data->ethnicity_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('maritual_status')); ?>:</b>
	<?php echo CHtml::encode($data->maritual_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contact_number')); ?>:</b>
	<?php echo CHtml::encode($data->contact_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('temporary_address')); ?>:</b>
	<?php echo CHtml::encode($data->temporary_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('permanent_address')); ?>:</b>
	<?php echo CHtml::encode($data->permanent_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('temporary_assign_date')); ?>:</b>
	<?php echo CHtml::encode($data->temporary_assign_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('permament_assign_date')); ?>:</b>
	<?php echo CHtml::encode($data->permament_assign_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('insurance_number')); ?>:</b>
	<?php echo CHtml::encode($data->insurance_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pf_number')); ?>:</b>
	<?php echo CHtml::encode($data->pf_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bank_name')); ?>:</b>
	<?php echo CHtml::encode($data->bank_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bank_account_number')); ?>:</b>
	<?php echo CHtml::encode($data->bank_account_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cif_number')); ?>:</b>
	<?php echo CHtml::encode($data->cif_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('remarks')); ?>:</b>
	<?php echo CHtml::encode($data->remarks); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>