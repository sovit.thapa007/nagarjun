<?php
$this->breadcrumbs=array(
	'Employee Details'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List EmployeeDetail','url'=>array('index')),
array('label'=>'Create EmployeeDetail','url'=>array('create')),
array('label'=>'View EmployeeDetail','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage EmployeeDetail','url'=>array('admin')),
);
?>

<div class="form-horizontal well">
	<h2 class="text-center"><strong>कर्मचारी बिस्तारि विवरणा अद्यावधिक गर्नुहोस्</strong></h2>
	<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
</div>