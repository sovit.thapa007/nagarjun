<?php
$this->breadcrumbs=array(
	'कर्मचारी बिस्तारि सूची',
);

$this->menu=array(
array('label'=>'Create EmployeeDetail','url'=>array('create')),
array('label'=>'Manage EmployeeDetail','url'=>array('admin')),
);
?>

<h1>Employee Details</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
