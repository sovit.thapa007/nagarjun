<?php
    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
  ?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'employee-detail-form',
	'type' => 'horizontal',
	'enableAjaxValidation'=>false,
)); ?>
<!-- 
<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?> -->
	<div class="row-fluid">
    	<div class="span2"> <strong>NAME  : </strong>
    	</div>
    	<div class="span4"> 
   			<?php echo CHtml::activeTextField($model, 'first_name', array('class'=>'span12','placeholder'=>'First Name', 'id'=>'first_name')) ?>
            <?php echo CHtml::error($model, 'first_name');?>
   		</div>
    	<div class="span3">
   			<?php echo CHtml::activeTextField($model, 'middle_name', array('class'=>'span12','placeholder'=>'Middle Name', 'id'=>'first_name')) ?>
            <?php echo CHtml::error($model, 'middle_name');?>
   		</div>
    	<div class="span3">
   			<?php echo CHtml::activeTextField($model, 'last_name', array('class'=>'span12','placeholder'=>'Last Name', 'id'=>'last_name')) ?>
            <?php echo CHtml::error($model, 'last_name');?>
   		</div>
	</div>
	<br/>
	<div class="row-fluid">
    	<div class="span2"> <strong>नाम (देवनागरिमा)  : </strong>
    	</div>
    	<div class="span4">
   			<?php echo CHtml::activeTextField($model, 'first_name_nepali', array('class'=>'span12 nepaliUnicode', 'placeholder'=>'पहिलो नाम ', 'id'=>'first_name_nepali')) ?>
            <?php echo CHtml::error($model, 'first_name_nepali');?>
   		</div>
    	<div class="span3">
   			<?php echo CHtml::activeTextField($model, 'middle_name_nepali', array('class'=>'span12 nepaliUnicode', 'placeholder'=>'बिच्को नाम ', 'id'=>'middle_name_nepali')) ?>
            <?php echo CHtml::error($model, 'middle_name_nepali');?>
   		</div>
    	<div class="span3">
   			<?php echo CHtml::activeTextField($model, 'last_name_nepali', array('class'=>'span12 nepaliUnicode', 'placeholder'=>'थर', 'id'=>'last_name_nepali')) ?>
            <?php echo CHtml::error($model, 'last_name_nepali');?>
   		</div>
	</div>
	<br />
	<div class="row-fluid">
		<div class="span4">
			<?php echo $form->dropDownListRow($model,'gender',array("महिला"=>"महिला","पुरुष"=>"पुरुष","अन्य"=>"अन्य"),['placeholder'=>false]); ?>
		</div>
		<div class="span4">
		<?php echo $form->dropDownListRow($model,'post',array("अन्य"=>"अन्य","शिक्षक"=>"शिक्षक","प्र.अ."=>"प्र.अ.",),['placeholder'=>false]); ?>
		</div>
		<div class="span4">
		<?php echo $form->dropDownListRow($model,'level',array("अन्य"=>"अन्य",'उ.मा.बि.'=>'उ.मा.बि.',"मा.बि."=>"मा.बि.","नि.मा.बि."=>"नि.मा.बि.",'प्रा.शि.'=>'प्रा.शि.',"बाल शिक्षा"=>"बाल शिक्षा","लेखापाल"=>"लेखापाल","आया"=>"आया"),['placeholder'=>false]); ?>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span4">
			<?php echo $form->dropDownListRow($model,'type',array("स्थायी"=>"स्थायी","अस्थायी"=>"अस्थायी","राहत"=>"राहत"),['placeholder'=>false,]); ?>
		</div>
		<div class="span4">
			<?php echo $form->dropDownListRow($model,'sherni',array(1=>"प्रथम श्रेणी",2=>"दोस्रो श्रेणी",3=>"त्रितिय श्रेणी"),['placeholder'=>false]); ?>
		</div>
		<div class="span4">
			<?php echo $form->textFieldRow($model,'grade'); ?>
		</div>
	</div>
	<hr />
	<div class="row-fluid">
		<div class="span6">
			<?php
			echo $form->dropDownListRow($model, 'ethnicity_id', CHtml::listData(EthinicGroup::model()->findAllByAttributes(['parent_id'=>'0']), 'id', 'title'), array('prompt' => ' छानुहोस ', 'class' => 'span12', 'id' => 'ethinic_level', 'data-url' => Yii::app()->createUrl('basicInformation/ethnicityCast')));
			?>
			<?php echo $form->dropDownListRow($model, 'cast_id', array(), ['prompt' => 'छानुहोस', 'id' => 'cast_level',  'data-url' => Yii::app()->createUrl('basicInformation/previousethnicityCast'),'class' => 'span12']); ?>
			<?php echo $form->dropDownListRow($model,'maritual_status',array(1=>"विवाहित",0=>"अविवाहित"),['placeholder'=>false]); ?>
			<?php echo $form->textFieldRow($model,'contact_number',array('class'=>'span12' ,)); ?>
		</div>
		<div class="span6">
			<?php echo $form->textFieldRow($model,'temporary_address',array('class'=>'span12')); ?>

			<?php echo $form->textFieldRow($model,'permanent_address',array('class'=>'span12')); ?>


			<div class="control-group">
				<label class="control-label" for="EmployeeDetail_maritual_status">अस्थाई नियुक्ती  मिति</label>
				<div class="controls">
	    		<?php
	    			$this->widget("ext.maskedInput.MaskedInput", array(
	                "model" => $model,
	                "attribute" => "temporary_assign_date",
	                "mask" => "9999-99-99",
	                "clientOptions"=>array("greedy"=>false, 'class'=>'span12','placeholder'=>'yyyy-mm-dd','removeMaskOnSubmit' => false)   
	            ));    
	    		?>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="EmployeeDetail_maritual_status">स्थाई नियुक्ती मिति:</label>
				<div class="controls">
	    		<?php
	    			$this->widget("ext.maskedInput.MaskedInput", array(
	                "model" => $model,
	                "attribute" => "permament_assign_date",
	                "mask" => "9999-99-99",
	                "clientOptions"=>array("greedy"=>false, 'class'=>'span12','placeholder'=>'yyyy-mm-dd','removeMaskOnSubmit' => false)   
	            ));    
	    		?>
				</div>
			</div>
		</div>
	</div>

	<hr />
	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->textFieldRow($model,'insurance_number',array('class'=>'span12','maxlength'=>50)); ?>

			<?php echo $form->textFieldRow($model,'pf_number',array('class'=>'span12')); ?>

			<?php echo $form->textFieldRow($model,'bank_name',array('class'=>'span12','maxlength'=>200)); ?>

			<?php echo $form->textFieldRow($model,'bank_account_number',array('class'=>'span12','maxlength'=>50)); ?>

			<?php echo $form->textFieldRow($model,'cif_number',array('class'=>'span12','maxlength'=>50)); ?>
		</div>
		<div class="span6">

			<div class="control-group">
				<label class="control-label" for="EmployeeDetail_maritual_status">बडुवा मिति:</label>
				<div class="controls">
	    		<?php
	    			$this->widget("ext.maskedInput.MaskedInput", array(
	                "model" => $model,
	                "attribute" => "baduwa_miti",
	                "mask" => "9999-99-99",
	                "clientOptions"=>array("greedy"=>false, 'class'=>'span12','placeholder'=>'yyyy-mm-dd','removeMaskOnSubmit' => false)   
	            ));    
	    		?>
				</div>
			</div>
			
			<?php echo $form->textFieldRow($model,'pan_no',array('class'=>'span12','maxlength'=>50)); ?>

			<?php echo $form->textAreaRow($model,'remarks',array('rows'=>8, 'cols'=>300)); ?>
		</div>
	</div>

		<?php 
		if(!$model->isNewRecord) 
			echo $form->dropDownListRow($model,'status',array(1=>"Active",0=>"In-Active")); ?>


	<?php 
	if(UtilityFunctions::ShowSchool()){
		echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => "title")),'id','title'),array('class'=>'span12', 'prompt'=>'बिद्यालय छानुहोस', 'required'=>'required')); 
	}else{
		echo $form->hiddenField($model,'school_id',array('value'=> UtilityFunctions::SchoolID()));
	}
	?>
<!-- 
		<?php echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_date',array('class'=>'span5')); ?> -->

	<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>

  <!-- load jquery files -->
  <?php
  $cs->registerScriptFile($baseUrl . '/js/unicode/pramukhime.js');
  $cs->registerScriptFile($baseUrl . '/js/unicode/pramukhindic-i18n.js');
  $cs->registerScriptFile($baseUrl . '/js/unicode/pramukhindic.js');
  ?>
<script type="text/javascript">

    $(document).ready(function(){
      
    pramukhIME.addKeyboard("PramukhIndic");
    pramukhIME.enable();
    $(".nepaliUnicode").focus(function () {
    pramukhIME.setLanguage("nepali", "pramukhindic");
    });

    $(".nepaliUnicode").focusout(function () {
    pramukhIME.setLanguage("english", "pramukhime");
    });
    $('#ethinic_level').change(function(){
	    var this_ = $(this);
	    var selected = this_.val();
	    var url = this_.data('url') + '/key/' + selected;
	    if (selected.length > 0) 
	    {
	        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'JSON',
            data : {YII_CSRF_TOKEN: $('input[name="YII_CSRF_TOKEN"]').val()},
            success: function(response) 
            {
                if (response.success) 
                {
                    $('#cast_level').html(response.option);
                } 
                else 
                {
                    $('#cast_level').html('');
                   
                }

                }
            })
	    }
	});
	var type_ = $('#EmployeeDetail_type').val();
	if(type_=='स्थायी'){
		$('#EmployeeDetail_sherni').html('<option value="1">प्रथम श्रेणी</option><option value="2">दोस्रो श्रेणी</option><option value="3">त्रितिय श्रेणी</option>');
	}else{
		$('#EmployeeDetail_sherni').html('<option value="3">त्रितिय श्रेणी</option>');
	}

    $('#EmployeeDetail_type').change(function(){
    	var type = $(this).val();
    	if(type=='स्थायी'){
    		$('#EmployeeDetail_sherni').html('<option value="1">प्रथम श्रेणी</option><option value="2">दोस्रो श्रेणी</option><option value="3">त्रितिय श्रेणी</option>');
    	}else{
    		$('#EmployeeDetail_sherni').html('<option value="3">त्रितिय श्रेणी</option>');
    	}
    });

});
</script>
