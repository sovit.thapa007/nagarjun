
<?php
$this->breadcrumbs=array(
	'Employee Details'=>array('index'),
	$model->id,
);
?>
<div class="form-horizontal well form-horizontal">
	<h2><STRONG>कर्मचारी बिस्तारि विवरणा </STRONG></h2>
	<table class="items table table-bordered" style="width:100%">
		<tbody>
			<tr>
				<td style="width: 30%">
					<img class="image" src="" data-zoom-image="" hegiht="200" width="200"></td>
				<td>
				<td>
					<table class="items table table-bordered" style="width:100%">
						<tr>
							<td> NAME (नाम)</td>
							<td> : <?= ucwords($model->first_name." ".$model->middle_name." ".$model->last_name).' ( '.$model->first_name_nepali." ".$model->middle_name_nepali." ".$model->last_name_nepali.' )'; ?></td>
						</tr>
						<tr>
							<td> <?= $model->getAttributeLabel('gender'); ?> </td>
							<td> : <?= $model->gender; ?></td>
						</tr>
						<tr>
							<td> <?= $model->getAttributeLabel('type').'/'.$model->getAttributeLabel('level').'/'.$model->getAttributeLabel('post').'/'.$model->getAttributeLabel('grade'); ?></td>
							<td> : <?= $model->type.'/'.$model->level.'/'.$model->post.'/'.$model->grade; ?></td>
						</tr>
						<tr>
							<td> अस्थायी/स्थाई पदमा नियुक्ती मिति</td>
							<td> : <?= $model->temporary_assign_date.'/'.$model->permament_assign_date; ?></td>
						</tr>
						<tr>
							<td> <?= $model->getAttributeLabel('maritual_status'); ?></td>
							<td> : <?= $model->maritual_status == 1 ? "विवाहित" : "अविवाहित"; ?></td>
						</tr>
						<tr>
							<td><?= $model->getAttributeLabel('contact_number'); ?></td>
							<td> : <?= $model->contact_number; ?></td>
						</tr>
						<tr>
							<td> बिद्यालयको नाम </td>
							<td>: <?= isset($model->school_sec) ? $model->school_sec->title : 'null'; ?></td>
						</tr>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
	<div class="alert alert-info text-center"><STRONG>ADDITIONAL INFORMATION </STRONG></div>
	<table class="items table table-bordered" style="width:100%">
			<tr>
				<td> विशेष मामला : <?= isset($model->special_sec) ? $model->special_sec->title : ''; ?></td>
				<td> <?= $model->getAttributeLabel('ethnicity_id'); ?> : <?php
				$ethinicity = isset($model->ethinicity_sec) ? $model->ethinicity_sec->title : '';
				$cast = isset($model->cast_sec) ? $model->cast_sec->title : '';
				echo $ethinicity.'/'.$cast; ?></td>
			</tr>
			<tr>
				<td> <?= $model->getAttributeLabel('permanent_address'); ?> : <?= $model->permanent_address; ?></td>
				<td> <?= $model->getAttributeLabel('temporary_address'); ?> : <?= $model->temporary_address; ?></td>
			</tr>
			<tr>
				<td> <?= $model->getAttributeLabel('bank_name'); ?> : <?= $model->bank_name; ?></td>
				<td> <?= $model->getAttributeLabel('bank_account_number'); ?> : <?= $model->bank_account_number; ?></td>
			</tr>

			<tr>
				<td> <?= $model->getAttributeLabel('insurance_number'); ?> : <?= $model->insurance_number; ?></td>
				<td> <?= $model->getAttributeLabel('pf_number'); ?> : <?= $model->pf_number; ?></td>
			</tr>
			<tr>
				<td> <?= $model->getAttributeLabel('cif_number'); ?> : <?= $model->cif_number; ?></td>
				<td> Status : <?= $model->status == '1' ?  'Active' : 'In-Active'; ?></td>
			</tr>
			<tr>
				<td> Create By/At : <?= $model->created_by.'/'.$model->created_date; ?></td>
				<td> Change By/At : <?= $model->updated_by.'/'.$model->updated_date; ?></td>
			</tr>
			<tr><td colspan="2" class="button-column"><a class="update" title="" data-toggle="tooltip" href="/management/magform/salaryDetail/id/<?= $model->id; ?>" data-original-title="Total Salary Detail"><i class="fa fa-eye"></i></a> &nbsp;&nbsp; <a class="update" title="" data-toggle="tooltip" href="/management/employeeDetail/update/id/<?= $model->id; ?>" data-original-title="Update Employee Detail"><i class="fa fa-plus"></i></a> &nbsp;&nbsp; <a class="update" title="" data-toggle="tooltip" href="/management/employeeDetail/update/id/<?= $model->id; ?>" data-original-title="Update Employee Detail"><i class="fa fa-pencil"></i></a></td><td></td></tr>
	</table>
 

