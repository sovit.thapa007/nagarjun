<?php
$this->breadcrumbs=array(
	'Employee Details'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List EmployeeDetail','url'=>array('index')),
array('label'=>'Manage EmployeeDetail','url'=>array('admin')),
);
?>

<div class="form-horizontal well">
	<h2 class="text-center"><strong>शिक्षक तथा कर्मचारीहरूको बिबरण </strong></h2>
	<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>