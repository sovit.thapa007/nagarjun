<?php
$this->breadcrumbs=array(
	'Employee Leaves',
);

$this->menu=array(
array('label'=>'Create EmployeeLeave','url'=>array('create')),
array('label'=>'Manage EmployeeLeave','url'=>array('admin')),
);
?>

<h1>Employee Leaves</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
