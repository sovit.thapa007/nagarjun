
<style type="text/css">label {
  display:block;
  border:solid 1px gray;
  line-height:40px;
  /*height:40px;
  width: 250px;*/
  border-radius:40px;
  -webkit-font-smoothing: antialiased; 
  margin-top:10px;
  font-family:Arial,Helvetica,sans-serif;
  color:gray;
  text-align:center;
}

input[type=radio] {
  display: none;
}

input:checked + label {
  border: solid 1px red;
  color: #F00;
}

input:checked + label:before {
content: "✓ ";
}


/* new stuff */
.check {
  visibility: hidden;
}

input:checked + label .check {
  visibility: visible;
}

input.checkbox:checked + label:before {
content: "";
}</style>
<?php
/* @var $this DefaultController */
$subject_id = 1;
$this->breadcrumbs = array(
	$this->module->id,
);
?>
<div class="form-horizontal well">
	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'grade-scale-form',
	'enableAjaxValidation'=>false,
	'action'=>Yii::app()->request->baseUrl.'/management/employeeLeave/manage',
	)); ?>
	<br /><br />
	<div class="row-fluid">
		<div class="span3"> साल/बर्ष :  </div>
		<div class="span9">
		<select placeholder="" name="year">
			<option value="2076"><?= UtilityFunctions::NepaliNumber('2076'); ?></option>
			<option value="2077"><?= UtilityFunctions::NepaliNumber('2077'); ?></option>
			<option value="2078"><?= UtilityFunctions::NepaliNumber('2078'); ?></option>
			<option value="2079"><?= UtilityFunctions::NepaliNumber('2079'); ?></option>
			<option value="2080"><?= UtilityFunctions::NepaliNumber('2080'); ?></option>
		</select>
		</div>
	</div>
	<br />
	<?php
	if(UtilityFunctions::ShowSchool() && !empty($school_list)){
		?>
	<div class="row-fluid">
		<div class="span3"> बिद्यालय:  </div>
		<div class="span9">
		<select placeholder="" name="school_id">
		<?php
		foreach ($school_list as $school) {
			?>
			<option value="<?= $school->id; ?>"><?= $school->title_nepali.'-'.UtilityFunctions::NepaliNumber($school->ward_no); ?></option>
			<?php
		}
		?>
		</select>
		</div>
	</div>

		<?php
	}
	?>
	<br /><br />
	<div class="row-fluid quick-actions">
		<?php
		for ($i=1; $i <= 12; $i++) { 
			?>
			<div class="span4">
	              <input type="radio" id="month_lable_<?= $i; ?>" name="month_number" value="<?= $i; ?>" class='subject_id_class' > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <label for="month_lable_<?= $i; ?>" style='margin-top:0px !important;'><span><img src="<?php echo Yii::app()->baseUrl; ?>/img/dashboard/booking.svg" alt="monthly booked" height="50" width='50'></span> <br /><?= UtilityFunctions::NepaliMonth($i); ?></label>
			</div>
			<?php
			# code...
		}
		?>
	</div>
	<br /><br />
	<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'खोज्नुहोस',
		)); ?>
</div>

<?php $this->endWidget(); ?>