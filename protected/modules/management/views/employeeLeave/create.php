<?php
$this->breadcrumbs=array(
	'Employee Leaves'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List EmployeeLeave','url'=>array('index')),
array('label'=>'Manage EmployeeLeave','url'=>array('admin')),
);
?>

<h1>Create EmployeeLeave</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>