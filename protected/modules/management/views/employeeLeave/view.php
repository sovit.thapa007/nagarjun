<?php
$this->breadcrumbs=array(
	'Employee Leaves'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List EmployeeLeave','url'=>array('index')),
array('label'=>'Create EmployeeLeave','url'=>array('create')),
array('label'=>'Update EmployeeLeave','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete EmployeeLeave','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage EmployeeLeave','url'=>array('admin')),
);
?>

<h1>View EmployeeLeave #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'employee_id',
		'year',
		'month',
		'leave_day',
		'status',
		'created_date',
		'created_by',
		'updated_by',
		'updated_date',
),
)); ?>
