

<div class="form-horizontal well">
	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'grade-scale-form',
	'enableAjaxValidation'=>false,
	'action'=>Yii::app()->request->baseUrl.'/management/employeeLeave/submitLeave',
	)); ?>
	<br />
	<h2 class="text-center"><strong>अनुपस्थित दिन को संख्या बक्स</strong></h2>
		<div class="row-fluid" style="color: #0088cc;">
		    <div class="span3">
		      <?php
		        if(!empty($employee_info)){
		          foreach ($employee_info as $employee_) {
		            ?>
		              <?= $employee_->level; ?> : <?= UtilityFunctions::NepaliNumber($employee_->id); ?> <br />
		            <?php
		          }
		        }
		      ?> 
		    </div>
		    <div class="span6" style="padding: 10px !important; font-weight: bold; font-size:14pt !important; text-align: center;">
		      <?php 
		      echo $school_detail->title_nepali.'<br />नागर्जुन - '.UtilityFunctions::NepaliNumber($school_detail->ward_no).', काठमाडौं <br />'; ?>
		    </div>
		    <div class="span3" style="text-align: left;">
		      बिद्यालय खाता नं. <?= UtilityFunctions::NepaliNumber($school_detail->bank_account_number); ?><br />  
		      खाता रहेको बैंक :- <?= $school_detail->bank_name; ?><br />
		      १) बिद्यालयको फोन नं <?= UtilityFunctions::NepaliNumber($school_detail->contact_number); ?><br />
		      २) प्र.अ.को मो.न. <?php
		      $priciple_ = UtilityFunctions::PrincipleAccount($school_detail->id);
		      echo isset($priciple_['number']) ? UtilityFunctions::NepaliNumber($priciple_['number']) : '';
		      ?><br />
		      १) लेखा प्रमुखको मो.न.<?php
		      $accountant_ = UtilityFunctions::AccountantInformation($school_detail->id);
		      echo isset($accountant_['number']) ? UtilityFunctions::NepaliNumber($accountant_['number']) : '';
		      ?><br />  
		    </div>
		  </div>

	<br /><br />
	<div class="row-fluid" style="overflow: auto;">
	<table class="table table-bordered table-stripped">
		<thead>
	      <th>सि. न.</th>
	      <th>संकेत नं. </th>
	      <th>शिक्षकको नाम</th>
	      <?php
	      	if($month){echo "<th>".UtilityFunctions::NepaliMonth($month)."</th>"; }
	      	else{
      		for ($i=1; $i <= 12 ; $i++) { 
      			?>
      			<th><?= UtilityFunctions::NepaliMonth($i); ?> </th>
      			<?php
      		}}
	      ?>
		</thead>
		<tbody>
		<?php
		if(!empty($employee_detail)){
			$sn = 1;
			foreach ($employee_detail as $employee_) {
				$employee_id = $employee_->id;
				?>
				<tr>
					<td><?= UtilityFunctions::NepaliNumber($sn++); ?></td>
					<td><?= UtilityFunctions::NepaliNumber($employee_id); ?></td>
					<td><?= $employee_->first_name_nepali.' '.$employee_->middle_name_nepali.' '.$employee_->last_name_nepali; ?></td>
				<?php
				if($month){
					$days = isset($employee_leave_information[$employee_id][$month]) ? $employee_leave_information[$employee_id][$month] : '';
					?>
					<td><input type="number" name="leave_day_<?= $employee_id.'_'.$month; ?>" value="<?= $days; ?>">
					</td>
					<?php
				}
				else{
					for ($j=1; $j <= 12 ; $j++) { 
						$days = isset($employee_leave_information[$employee_id][$j]) ? $employee_leave_information[$employee_id][$j] : '';
						?>
						<td><input type="number" name="leave_day_<?= $employee_id.'_'.$j; ?>" value="<?= $days; ?>">
						<input type="hidden" name="month_id_[]" value="<?= $j; ?>"></td>
						<?php

					}
				}
				?>
				<input type="hidden" name="employee_id_[]" value="<?= $employee_id; ?>">
				</tr>

				<?php

			}
		}
		?>
		</tbody>
	</table>
		<input type="hidden" name="month_type" value="<?= $month ? $month : 100; ?>">
		<input type="hidden" name="year" value="<?= $year; ?>">
	</div>
	<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'खोज्नुहोस',
		)); ?>
</div>

<?php $this->endWidget(); ?>
</div>