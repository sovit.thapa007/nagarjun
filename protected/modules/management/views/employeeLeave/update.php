<?php
$this->breadcrumbs=array(
	'Employee Leaves'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List EmployeeLeave','url'=>array('index')),
array('label'=>'Create EmployeeLeave','url'=>array('create')),
array('label'=>'View EmployeeLeave','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage EmployeeLeave','url'=>array('admin')),
);
?>

<h1>Update EmployeeLeave <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>