<?php
$this->breadcrumbs=array(
	'Grade Scales'=>array('index'),
	$model->id,
);

?>

<div class="form-horizontal well form-horizontal">
	<h2><STRONG>श्रेणीको तलब स्केल विवरणा</STRONG></h2>
	<table class="items table table-bordered" style="width:100%">
		<tr><td colspan="4" style="text-align: center; font-weight: bold;"><?= $model->Types;?></td></tr>
		<tr><td><?= $model->getAttributeLabel('_employee_level'); ?></td><td> : <?= $model->_employee_level; ?></td><td><?= $model->getAttributeLabel('sherni_grade'); ?></td><td> : <?= $model->SherniGradeTitle; ?></td></tr>
		<tr><td><?= $model->getAttributeLabel('amount'); ?> </td><td><?= UtilityFunctions::NepaliNumber($model->amount); ?></td><td><?= $model->getAttributeLabel('status'); ?> </td><td><?= $model->status == 1 ? "सकृय" : "निष्कृय"; ?></td></tr>
	</table>
</div>