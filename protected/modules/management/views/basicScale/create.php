<?php
$this->breadcrumbs=array(
	'Grade Scales'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List GradeScale','url'=>array('index')),
array('label'=>'Manage GradeScale','url'=>array('admin')),
);
?>

<div class="form-horizontal well">
	<h2 class="text-center"><strong>तलब स्केल सेट गर्नुहोस </strong></h2>
	<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>