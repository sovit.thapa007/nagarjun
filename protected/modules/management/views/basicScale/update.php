<?php
$this->breadcrumbs=array(
	'Grade Scales'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
/*
$this->menu=array(
array('label'=>'List GradeScale','url'=>array('index')),
array('label'=>'Create GradeScale','url'=>array('create')),
array('label'=>'View GradeScale','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage GradeScale','url'=>array('admin')),
);*/
?>

<div class="form-horizontal well">
	<h2 class="text-center"><strong>तलब/ग्रेड स्केल सच्याउनुस  </strong></h2>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
</div>