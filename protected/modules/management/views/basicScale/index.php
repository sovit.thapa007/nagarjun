<?php
$this->breadcrumbs=array(
	'Grade Scales',
);

$this->menu=array(
array('label'=>'Create GradeScale','url'=>array('create')),
array('label'=>'Manage GradeScale','url'=>array('admin')),
);
?>

<h1>Grade Scales</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
