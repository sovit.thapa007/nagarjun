<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'grade-scale-form',
	'enableAjaxValidation'=>false,
)); ?>
<!-- 
<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?> -->
	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->dropDownListRow($model,'_employee_level',array('उ.मा.बि.'=>'उ.मा.बि.',"मा. बि."=>"मा. बि.","नि. मा. बि."=>"नि. मा. बि.",'प्रा.शि.'=>'प्रा.शि.',"बाल शिक्षा"=>"बाल शिक्षा","लेखापाल"=>"लेखापाल","आया"=>"आया","अन्य"=>"अन्य",),array('class'=>'spa12')); ?>
		</div>
		<div class="span6">
			<?php echo $form->dropDownListRow($model,'type',array('sherni'=>'श्रेणी','grade'=>'ग्रेड'),['prompt'=>' श्रेणी/ग्रेड छन्नु होस ','placeholder'=>false]); ?>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->dropDownListRow($model,'sherni_grade',array(1=>"प्रथम श्रेणी",2=>"दोस्रो श्रेणी",3=>"त्रितिय श्रेणी"),['prompt'=>'श्रेणी छन्नु होस','placeholder'=>false]); ?>
		</div>
		<div class="span6">
			<?php echo $form->textFieldRow($model,'amount',array('class'=>'span12','maxlength'=>8)); ?>
		</div>
	</div>

		<?php 
		if(!$model->isNewRecord) 
			echo $form->dropDownListRow($model,'status',array(1=>"Active",0=>"In-Active")); ?>
<!-- 
		<?php echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_date',array('class'=>'span5')); ?> -->
	<br /><br />
	<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    $(document).ready(function(){
    	$('#BasicScale_type').change(function(){
    		var sherni_grade = $(this).val();
    		if(sherni_grade == 'grade'){
    			$('#BasicScale_sherni_grade').html('<option value="">ग्रेड छन्नु होस श्रेणी</option><option value="1">१ ग्रेड</option><option value="2">२ ग्रेड</option><option value="3">३ ग्रेड</option><option value="4">४ ग्रेड</option><option value="5">५ ग्रेड</option><option value="6">६ ग्रेड</option><option value="7">७ ग्रेड</option><option value="8">८ ग्रेड</option><option value="9">९ ग्रेड</option><option value="10">१० ग्रेड</option><option value="11">११ ग्रेड</option>')
    		}
    		if(sherni_grade == 'sherni'){
    			$('#BasicScale_sherni_grade').html('<option value="">श्रेणी छन्नु होस श्रेणी</option><option value="1">प्रथम श्रेणी</option><option value="2">दोस्रो श्रेणी</option><option value="3" selected="selected">त्रितिय श्रेणी</option>')

    		}
    		if(sherni_grade == ''){
    			$('#BasicScale_sherni_grade').html('<option value=""></option>')

    		}
    	});
    });
</script>
