<?php
$this->breadcrumbs=array(
	'Grade Scales'=>array('index'),
	'Manage',
);

?>
<div class="form-horizontal well">
	<h2 class="text-center"><strong>तलब/ग्रेड स्केल लिस्ट  </strong></h2>
	<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'grade-scale-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
			//'id',
			'_employee_level',
			//'sherni_grade',
			[
				'name'=>'sherni_grade',
				'value' => '$data->SherniGradeTitle',
			],
			'amount',
			[
				'name'=>'status',
				'value' => '$data->status == 1 ? "सकृय" : "निष्कृय"',
			],
			/*
			'status',
			'created_by',
			'created_date',
			'updated_by',
			'updated_date',
			*/
	array(
	'class'=>'bootstrap.widgets.TbButtonColumn',
	),
	),
	)); ?>
</div>