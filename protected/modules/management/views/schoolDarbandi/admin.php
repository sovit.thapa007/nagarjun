<?php
$this->breadcrumbs=array(
	'School Darbandis'=>array('index'),
	'Manage',
);
?>
<div class="row-fluid well">
<h4 class="text-center"><strong>Manage School Darbandi</strong></h4>
<!-- 
<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
		&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div> --><!-- search-form -->

	<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'school-darbandi-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
			//'id',
			'school_id',
			'level_id',
			'number',
			'status',
			'created_by',
			/*
			'created_date',
			'updated_by',
			'updated_date',
			*/
	array(
	'class'=>'bootstrap.widgets.TbButtonColumn',
	),
	),
	)); ?>
</div>
