<?php
$this->breadcrumbs=array(
	'School Darbandis'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List SchoolDarbandi','url'=>array('index')),
array('label'=>'Manage SchoolDarbandi','url'=>array('admin')),
);
?>

<h1>Create SchoolDarbandi</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>