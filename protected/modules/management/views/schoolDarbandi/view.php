<?php
$this->breadcrumbs=array(
	'School Darbandis'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List SchoolDarbandi','url'=>array('index')),
array('label'=>'Create SchoolDarbandi','url'=>array('create')),
array('label'=>'Update SchoolDarbandi','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete SchoolDarbandi','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage SchoolDarbandi','url'=>array('admin')),
);
?>

<h1>View SchoolDarbandi #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'school_id',
		'level_id',
		'number',
		'status',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
),
)); ?>
