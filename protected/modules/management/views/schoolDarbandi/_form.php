<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'administrative-form',
	'enableAjaxValidation'=>false,
)); ?>

		<?php 
		if(UtilityFunctions::ShowSchool()){
			echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => "title_nepali")),'id','title_nepali'),array('class'=>'span12', 'prompt'=>'बिद्यालय छान्नुस', 'required'=>'required')); 
		}else{
			echo $form->hiddenField($model,'school_id',array('value'=> UtilityFunctions::SchoolID()));
		}
		?>
		<br />
		<?php //echo $form->textFieldRow($model,'school_id',array('class'=>'span5')); ?>


	    <table class="table table-bordered table-stripped" id="customFields">
	        <thead>
	            <tr>
	                <th><?php echo CHtml::encode($model->getAttributeLabel('level_id')); ?></th>
	                <th><?php echo CHtml::encode($model->getAttributeLabel('number')); ?></th>
	                <th>BUTTON</th>
	            </tr>
	        </thead>
	        <tbody>
	            <tr>
	                <td><?php echo CHtml::activeDropDownList($model, 'level_id[]', ['उ.मा.बि.'=>'उ.मा.बि.',"मा.बि."=>"मा.बि.","नि.मा.बि."=>"नि.मा.बि.",'प्रा.शि.'=>'प्रा.शि.',"बाल शिक्षा"=>"बाल शिक्षा","लेखापाल"=>"लेखापाल","आया"=>"आया","अन्य"=>"अन्य"], array('placeholder'=>'बिद्यालयको तह')) ?></td>
	                <td><?php echo CHtml::activeTextField($model, 'number[]', array('placeholder'=>'कुल नम्बर')) ?></td>
	                <td><a href="javascript:void(0);" class="addCF btn"><i class="fa fa-plus"></i></a></td>
	            </tr>
	        </tbody>
	    </table>

	<div class="form-actions text-center">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'स्टोर गर्नुहोस',
		)); ?>
</div>

<?php $this->endWidget(); ?>


<script type="text/javascript">
	$(document).ready(function(){
	$(".addCF").click(function(){
		$("#customFields").append('<tr><td><select placeholder="बिद्यालयको तह" name="SchoolDarbandi[level_id][]" id="SchoolDarbandi_level_id"><option value="उ.मा.बि.">उ.मा.बि.</option><option value="मा.बि.">मा.बि.</option><option value="नि.मा.बि.">नि.मा.बि.</option><option value="प्रा.शि.">प्रा.शि.</option><option value="बाल शिक्षा">बाल शिक्षा</option><option value="लेखापाल">लेखापाल</option><option value="आया">आया</option><option value="अन्य">अन्य</option></td><td><input placeholder="कुल नम्बर" name="SchoolDarbandi[number][]" id="SchoolDarbandi_number" type="text"></td><td><a href="javascript:void(0);" class="remCF btn"><i class="fa fa-minus"></i></a></td></tr>');
	});
    $("#customFields").on('click','.remCF',function(){
        $(this).parent().parent().remove();
    });
});
</script>
