<?php
$this->breadcrumbs=array(
	'School Darbandis'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
array('label'=>'List SchoolDarbandi','url'=>array('index')),
array('label'=>'Create SchoolDarbandi','url'=>array('create')),
array('label'=>'View SchoolDarbandi','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage SchoolDarbandi','url'=>array('admin')),
);
?>

<h1>Update SchoolDarbandi <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>