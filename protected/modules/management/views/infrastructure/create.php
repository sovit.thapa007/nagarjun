<?php
$this->breadcrumbs=array(
	'भौतिक निर्माण विवण'=>array('index'),
	'भौतिक निर्माण विवण भर्नुस',
);

$this->menu=array(
array('label'=>'List Infrastructure','url'=>array('index')),
array('label'=>'Manage Infrastructure','url'=>array('admin')),
);
?>

<div class="form-horizontal well">
	<h2 class="text-center"><strong>भौतिक निर्माण विवण भर्नुस  </strong></h2>

	<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>