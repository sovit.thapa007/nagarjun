<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'infrastructure-form',
	'enableAjaxValidation'=>false,
)); ?>

<!-- <p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>
 -->
		<?php //echo $form->textFieldRow($model,'tansaction_id',array('class'=>'span5')); ?>


		<?php 
		if(UtilityFunctions::ShowSchool()){
			echo $form->dropDownListRow($model,'school_id',CHtml::listData(BasicInformation::model()->findAll(array("condition" => "status = 1","order" => "title_nepali")),'id','title_nepali'),array('class'=>'span12', 'prompt'=>'बिद्यालय छान्नुस ', 'required'=>'required')); 
		}else{
			echo $form->hiddenField($model,'school_id',array('value'=> UtilityFunctions::SchoolID()));
		}
		?>
		<br />
		<div class="row-fluid">
			<?php echo $form->numberFieldRow($model,'year',array('class'=>'span12')); ?>
		</div>
		<br />
		<?php //echo $form->textFieldRow($model,'school_id',array('class'=>'span5')); ?>


	    <table class="table table-bordered table-stripped" id="customFields">
	        <thead>
	            <tr>
	                <th><?php echo CHtml::encode($model->getAttributeLabel('title')); ?></th>
	                <th><?php echo CHtml::encode($model->getAttributeLabel('amount')); ?></th>
	                <th>BUTTON</th>
	            </tr>
	        </thead>
	        <tbody>
	            <tr>
	                <td><?php echo CHtml::activeTextField($model, 'title[]', array('class'=>'span12', 'placeholder'=>'शीर्षक')) ?></td>
	                <td><?php echo CHtml::activeTextField($model, 'amount[]', array('placeholder'=>'कुल राशि')) ?></td>
	                <td><a href="javascript:void(0);" class="addCF btn"><i class="fa fa-plus"></i></a></td>
	            </tr>
	        </tbody>
	    </table>
	    <br />
<!-- 
		<?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>200)); ?>

		<?php echo $form->textFieldRow($model,'amount',array('class'=>'span5','maxlength'=>12)); ?>

		<?php echo $form->textAreaRow($model,'remarks',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

		<?php echo $form->textFieldRow($model,'status',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'created_date',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_by',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'updated_date',array('class'=>'span5')); ?> -->

	<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(document).ready(function(){
	$(".addCF").click(function(){
		$("#customFields").append('<tr><td><input class="span12" placeholder="शीर्षक" name="Infrastructure[title][]" id="Infrastructure_title" type="text"></td><td><input placeholder="कुल राशि" name="Infrastructure[amount][]" id="Infrastructure_amount" type="text"></td><td><a href="javascript:void(0);" class="remCF btn"><i class="fa fa-minus"></i></a></td></tr>');
	});
    $("#customFields").on('click','.remCF',function(){
        $(this).parent().parent().remove();
    });
});
</script>

