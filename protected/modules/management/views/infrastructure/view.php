<?php
$this->breadcrumbs=array(
	'भौतिक निर्माण विवण'=>array('index'),
	$model->title,
);

$this->menu=array(
array('label'=>'List Infrastructure','url'=>array('index')),
array('label'=>'Create Infrastructure','url'=>array('create')),
array('label'=>'Update Infrastructure','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Infrastructure','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Infrastructure','url'=>array('admin')),
);
?>

<div class="form-horizontal well">
	<h2 style="text-align: center;"><strong>भौतिक निर्माण को सूचीहरु</strong> </h2>
	<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
			//'id',
			//'tansaction_id',
			[
				'name'=>'transaction_id',
				'value'=>UtilityFunctions::NepaliNumber($model->transaction_id),
			],


			[
				'name'=>'school_id',
				'value'=>$model->school ? $model->school->title_nepali : "",
			],
			//'transaction_id',
			'title',
			[
				'name'=>'amount',
				'value'=>UtilityFunctions::NepaliNumber($model->amount),
			],
			'remarks',
			[
				'name'=>'status',
				'value'=>$model->status == 1 ? "सकृय" : "निष्कृय",
			],
			'status',
			//'created_by',
			'created_date',
			//'updated_by',
			'updated_date',
	),
	)); ?>
</div>
