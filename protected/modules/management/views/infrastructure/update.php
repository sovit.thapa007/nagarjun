<?php
$this->breadcrumbs=array(
	'भौतिक निर्माण विवण'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'सुधार गर्नुस',
);

$this->menu=array(
array('label'=>'List Infrastructure','url'=>array('index')),
array('label'=>'Create Infrastructure','url'=>array('create')),
array('label'=>'View Infrastructure','url'=>array('view','id'=>$model->id)),
array('label'=>'Manage Infrastructure','url'=>array('admin')),
);
?>

<div class="form-horizontal well">
	<h2 class="text-center"><strong>अद्यावधिक गर्नुहोस्  </strong></h2>

	<?php echo $this->renderPartial('_update_form',array('model'=>$model)); ?>

</div>