<?php
$this->breadcrumbs=array(
	'भौतिक निर्माण भर्नुस'=>array('index'),
	'भौतिक निर्माण विवण',
);

$this->menu=array(
array('label'=>'List Infrastructure','url'=>array('index')),
array('label'=>'Create Infrastructure','url'=>array('create')),
);

?>
<div class="form-horizontal well">
	<h2 style="text-align: center;"><strong>भौतिक निर्माण को सूचीहरु</strong> </h2>

	<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'infrastructure-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
			//'id',

			[
				'name'=>'school_id',
				'value'=>'$data->school ? $data->school->title_nepali : ""',
			],
			//'transaction_id',
			'title',
			[
				'name'=>'amount',
				'value'=>'UtilityFunctions::NepaliNumber($data->amount)',
			],
			//'remarks',
			/*
			'status',
			'created_by',
			'created_date',
			'updated_by',
			'updated_date',
			*/
	array(
	'class'=>'bootstrap.widgets.TbButtonColumn',
	),
	),
	)); ?>
</div>
