<?php
$this->breadcrumbs=array(
	'Infrastructures',
);

$this->menu=array(
array('label'=>'Create Infrastructure','url'=>array('create')),
array('label'=>'Manage Infrastructure','url'=>array('admin')),
);
?>

<h1>Infrastructures</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
