<?php
$this->breadcrumbs=array(
	'Employee Posts'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List EmployeePost','url'=>array('index')),
array('label'=>'Create EmployeePost','url'=>array('create')),
array('label'=>'Update EmployeePost','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete EmployeePost','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage EmployeePost','url'=>array('admin')),
);
?>

<h1>View EmployeePost #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'post',
		'status',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
),
)); ?>
