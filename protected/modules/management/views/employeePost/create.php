<?php
$this->breadcrumbs=array(
	'Employee Posts'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List EmployeePost','url'=>array('index')),
array('label'=>'Manage EmployeePost','url'=>array('admin')),
);
?>

<h1>Create EmployeePost</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>