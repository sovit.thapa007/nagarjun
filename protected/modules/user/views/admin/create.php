<?php
$this->breadcrumbs = array(
	UserModule::t('Users') => array('admin'),
	UserModule::t('Create'),
);
?>
<h3 class="text-center"><strong><?php echo UserModule::t("Create User"); ?></strong></h3>

<?php

if(UtilityFunctions::SuperUser()){
	echo $this->renderPartial('_menu', array(
		'list' => array(),
	));
}
echo $this->renderPartial('_form', array('model' => $model));
?>