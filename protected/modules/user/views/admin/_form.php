	<?php echo CHtml::beginForm('', 'post', array('enctype' => 'multipart/form-data')); ?>

	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo CHtml::errorSummary(array($model)); ?>


	<div class="row-fluid">
		<div class="span6">
			<div class="row-fluid">
				<?php echo CHtml::activeLabelEx($model, 'username'); ?>
				<?php echo CHtml::activeTextField($model, 'username', array('size' => 20, 'maxlength' => 20 , 'class'=>'span12')); ?>
				<?php echo CHtml::error($model, 'username'); ?>
			</div>

			<div class="row-fluid">
				<?php echo CHtml::activeLabelEx($model, 'password'); ?>
				<?php echo CHtml::activePasswordField($model, 'password', array('size' => 60, 'maxlength' => 128 , 'class'=>'span12')); ?>
				<?php echo CHtml::error($model, 'password'); ?>
			</div>
		</div>
		<div class="span6">

			<div class="row-fluid">
				<?php echo CHtml::activeLabelEx($model, 'email'); ?>
				<?php echo CHtml::activeTextField($model, 'email', array('size' => 60, 'maxlength' => 128 , 'class'=>'span12')); ?>
				<?php echo CHtml::error($model, 'email'); ?>
			</div>

			<div class="row-fluid">
				<?php
				if(UtilityFunctions::SuperUser()){
					?>
					<?php echo CHtml::activeLabelEx($model, 'role'); ?>
					<?php echo CHtml::activeDropDownList($model, 'role', CHtml::listData(AuthItem::model()->findAll('type=:type AND name!=:name',[':type'=>2, ':name'=>'admin']),'name','name'), array('class'=>'span12'), array('class'=>'span12')); ?>
					<?php echo CHtml::error($model, 'superuser'); ?>
					<?php
				}else{
					echo CHtml::hiddenTextField($model, 'schooldataentry');
				}
				?>
			</div>
		</div>
	</div>

		<div class="row-fluid">
		<?php 
		if(UtilityFunctions::ShowSchool()){
			?>
			<?php echo CHtml::activeLabelEx($model, 'school_id'); ?>
			<?php echo CHtml::activeDropDownList($model, 'school_id',CHtml::listData(BasicInformation::model()->findAll(),'id','title'), array('class'=>'span12')); ?>
			<?php echo CHtml::error($model, 'school_id'); ?>
			<?php
		}
		?>
		</div>

	<div class="row-fluid buttons text-center">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

	<?php echo CHtml::endForm(); ?>

</div><!-- form -->