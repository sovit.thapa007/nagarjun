<!DOCTYPE html>
<html>
<head>
 <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/login.css">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome/css/font-awesome.min.css">
  <title>login form</title>
</head>
<body>
<?php
            $this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Login");
            $this->breadcrumbs = array(
              UserModule::t("Login"),
            );
            ?>
    <!--
    you can substitue the span of reauth email for a input with the email and
    include the remember me checkbox
    -->
    <div class="container">
        <div class="card card-container">
            <img id="profile-img" class="profile-img-card" src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo/ministry_education.jpg" />
            <p style="margin-top: -2em;"><?= UtilityFunctions::MunicipalityHeader(); ?></p>
            <form class="form-signin" method="post" action="#" role="login">
             <?php echo CHtml::beginForm(); ?>
                <?php //echo CHtml::errorSummary($model); ?>

                <?php echo CHtml::activeTextField($model, 'username', array('placeholder'=>'Enter Username', 'id'=>'inputEmail', 'class'=>'form-control','autocomplete'=>"off")) ?>
                <?php echo CHtml::error($model, 'username');?>

                 <?php echo CHtml::activePasswordField($model, 'password',array('placeholder'=>'Enter Password', 'id'=>'inputPassword', 'class'=>'form-control','autocomplete'=>"off")) ?>
                <?php echo CHtml::error($model, 'password');?>
                 <br />
                <?php echo CHtml::submitButton(UserModule::t("Login"), array('class' => 'btn btn-lg btn-primary btn-block btn-signin')); ?>
             <?php echo CHtml::endForm(); ?>
            </form><!-- /form -->
        </div><!-- /card-container -->
    </div><!-- /container -->
    <div class="container-fluid footer">
    <div class="row">
    <div class="col-sm-6">
    <p class="p1"><strong>&copy 2018 <?= Yii::app()->name; ?></strong></p>
    </div>

    <div class="col-sm-6">
    <p class="p2"><strong>Powered By: Encraft Technologies</strong></p>
    </div>
    </div>
    </div>
    </body>
</html>