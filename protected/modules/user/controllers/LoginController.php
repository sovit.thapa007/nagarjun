<?php

class LoginController extends Controller
{

	public $defaultAction = 'login';

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if (Yii::app()->user->isGuest) {
			$model = new UserLogin;
			// collect user input data
			if (isset($_POST['UserLogin'])) {
				$this->beginWidget('CHtmlPurifier');
				$model->attributes = $_POST['UserLogin'];
				$this->endWidget();

				// validate user input and redirect to previous page if valid
				if ($model->validate()) {
					$this->lastViset();
					if (strpos(Yii::app()->user->returnUrl, '/index.php') !== false) {
						$this->redirect(Yii::app()->controller->module->returnUrl);
					} else {
						$this->redirect(Yii::app()->user->returnUrl);
					}
				}
			}
			// display the login form
			$this->renderPartial('/user/login', array('model' => $model));
		} else
			$this->redirect(Yii::app()->controller->module->returnUrl);
	}

	private function lastViset()
	{
		$lastVisit = User::model()->notsafe()->findByPk(Yii::app()->user->id);
		$lastVisit->lastvisit = time();
		$lastVisit->save();
	}

	/**
	 * set nepali date in session
	 */
	public function actionSetNepaliDate()
	{
		if (Yii::app()->request->isPostRequest) {
			$nepali_date = isset($_POST['neapli_date']) ? $_POST['neapli_date'] : '';
			$date_info = explode('-', $nepali_date);
			$month = $date_info[1];
			if (strlen($date_info[1]) == 1)
				$month = '0' . $date_info[1];
			if (strlen($date_info[1]) == 0)
				$month = '00';
			$day = $date_info[2];
			if (strlen($date_info[2]) == 1)
				$day = '0' . $date_info[2];
			if (strlen($date_info[2]) == 0)
				$day = '00';
			$new_date = $date_info[0] . '-' . $month . '-' . $day;
			Yii::app()->session['today_nepali_date'] = $new_date;
		}
	}

}
