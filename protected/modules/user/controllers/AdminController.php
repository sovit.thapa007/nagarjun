<?php

class AdminController extends RController
{

	public $defaultAction = 'admin';
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{	
		return array(
		'rights', // perform access control for CRUD operations
		);
		/*return CMap::mergeArray(parent::filters(), array(
				'accessControl', // perform access control for CRUD operations
		));*/
	}


	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$dataProvider = new CActiveDataProvider('User', array(
			'pagination' => array(
				'pageSize' => Yii::app()->controller->module->user_page_size,
			),
		));

		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	/**
	 * Displays a particular model.
	 */
	public function actionView()
	{
		$model = $this->loadModel();
		$this->render('view', array(
			'model' => $model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new User;
		//$profile = new Profile;
		if (isset($_POST['User'])) {
			$role = isset($_POST['User']['role']) ? $_POST['User']['role'] : null;
			$model->attributes = $_POST['User'];
			$model->activkey = Yii::app()->controller->module->encrypting(microtime() . $model->password);
			$model->createtime = time();
			$model->lastvisit = time();
			$model->status = 1;
			if(($model->superuser == 1 && in_array($role, ['municipalityDataEntry', 'municipalityOfficer', 'schooldataentry', 'schooladmin'])) || ($model->superuser == 0 && in_array($role, ['schooldataentry']))){
				if ($model->validate()) {
					$model->password = Yii::app()->controller->module->encrypting($model->password);
					if ($model->save()){
						$auth_model = new AuthAssignment;
						$auth_model -> itemname =  $role;
						$auth_model -> userid = $model->id;//user id
						$auth_model -> data = 'N;';
						if(!$auth_model->save()){
							echo "<pre>";
							echo print_r($auth_model->errors);
							$is_errors[] = 'false';
							exit;
						}
						$this->redirect(array('view', 'id' => $model->id));
					} 
					throw new CHttpException('Something is wrong during user Creation');
				}
			}else{
				throw new CHttpException('Something is wrong during user Creation');
			}
		} 
		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate()
	{
		$model = $this->loadModel();
		$profile = $model->profile;
		if (isset($_POST['User'])) {
			$model->attributes = $_POST['User'];
			$role = isset($_POST['User']['role']) ? $_POST['User']['role'] : null;
			$userInformation = User::model()->findByPk(Yii::app()->user->id);
			if(($userInformation->superuser == 1 && in_array($role, ['municipalityDataEntry', 'municipalityOfficer', 'schooldataentry', 'schooladmin'])) || ($userInformation->superuser == 0 && in_array($role, ['schooldataentry']))){
				if ($model->validate()) {
					$old_password = User::model()->notsafe()->findByPk($model->id);
					if ($old_password->password != $model->password) {
						$model->password = Yii::app()->controller->module->encrypting($model->password);
						$model->activkey = Yii::app()->controller->module->encrypting(microtime() . $model->password);
					}
					if ($model->save()){
						$authentication = AuthAssignment::model()->find('userid=:userid', ['userid'=>$model->id]);
						if(!empty($authentication)){
							$authentication -> itemname = $role;
							if(!$authentication->update())
								throw new CHttpException('Role Cannot Update');

						}else{
							$auth_model = new AuthAssignment();
							$auth_model -> itemname =  $role;
							$auth_model -> userid = $model->id;//user id
							$auth_model -> data = 'N;';
							if(!$auth_model->save())
								throw new CHttpException('Role Cannot Assing');
						}
					} 
					$this->redirect(array('view', 'id' => $model->id));
				}
			else
				throw new CHttpException('Something is wrong during user Creation');
			}
			throw new CHttpException('Your not authorized to perform to this function');
		}

		$this->render('update', array(
			'model' => $model,
			'profile' => $profile,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$model = $this->loadModel();
			$profile = Profile::model()->findByPk($model->id);
			$profile->delete();
			$model->delete();
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_POST['ajax']))
				$this->redirect(array('/user/admin'));
		} else
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if ($this->_model === null) {
			if (isset($_GET['id']))
				$this->_model = User::model()->notsafe()->findbyPk($_GET['id']);
			if ($this->_model === null)
				throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $this->_model;
	}



}
