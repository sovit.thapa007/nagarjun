<?php

class DefaultController extends Controller
{

    public function actions()
    {
        $actions=parent::actions();
        unset($actions['index']);
        return $actions;
    }
    protected function verbs()
    {
        return [
            'index'=>['GET']
        ];
    }

	public function actionIndex()
	{
		    // Check if id was submitted via GET
		    if(!isset($_GET['symbol_number']))
		        echo json_encode(['status'=>0,
		        	'message'=>'Error: Parameter <b>id</b> is missing']);
			$symbol_number = $_GET['symbol_number'];
			$studentDetail = StudentInformation::model()->find('symbol_number=:symbol_number',[':symbol_number'=>$symbol_number]);
			if(empty($studentDetail))
		        echo json_encode(['status'=>0,
		        	'message'=>'Requested student does not exist']);
		    else{
				$student_id = $studentDetail->id;
				$result = Result::model()->find('student_id=:student_id AND status=:status',[':student_id'=>$student_id, ':status'=>1]);
				$student_inforamtion['symbol_number'] = $studentDetail->symbol_number;
				$student_inforamtion['name'] = strtoupper($studentDetail->first_name.' '.$studentDetail->middle_name.' '.$studentDetail->last_name);
				$student_inforamtion['school'] = $studentDetail->school_sec ? strtoupper($studentDetail->school_sec->title) : '';
				$student_inforamtion['dob'] = $studentDetail->dob_nepali.'('.$studentDetail->don_english.' A.D.)';

				$student_inforamtion['gpa'] = $result ? $result->total_gpa : '';
				$markInformation = MarkObtained::model()->findAll('student_id=:student_id AND subject_status=:status',[':student_id'=>$student_id, ':status'=>1]);
				$mark_inforamtion = [];
				if(!empty($markInformation)){
					$sn = 0;
					foreach ($markInformation as $mark_information) {
                    	$practical_section = $mark_information->is_practical;
                        $combine_full_marks = $mark_information -> theory_full_mark + $mark_information -> practical_full_mark;
                        $credit_section = UtilityFunctions::GradeCreditHours($combine_full_marks);
                        $th_grade_information = UtilityFunctions::GradeInformation((int) $mark_information -> theory_mark, (int) $mark_information -> theory_full_mark);
                        $th_grade = isset($th_grade_information['grade']) ? $th_grade_information['grade'] : '--';
                        $pr_grade = '';
                        if($practical_section){
                            $pr_grade_information = UtilityFunctions::GradeInformation((int) $mark_information -> practical_mark, (int) $mark_information -> practical_full_mark);
                            $pr_grade = isset($pr_grade_information['grade']) ? $pr_grade_information['grade'] : '--';
                        }
                        $final_grade = $mark_information->total_grade ?  $mark_information->total_grade :'';
                        $grade_point = $mark_information->total_grade_point ?  $mark_information->total_grade_point :'';
						$mark_inforamtion['subject'][$sn] = strtoupper($mark_information->subject_name);
						$mark_inforamtion['credit'][$sn] = $credit_section;
						$mark_inforamtion['th_grade'][$sn] = htmlspecialchars($th_grade);
						$mark_inforamtion['pr_grade'][$sn] = htmlspecialchars($pr_grade);
						$mark_inforamtion['final_grade'][$sn] = htmlspecialchars($final_grade);
						$mark_inforamtion['final_gp'][$sn] = $grade_point;
						$sn++;
					}
				}
		        echo json_encode(['status'=>1,
		        	'data'=>['student'=>$student_inforamtion,'mark'=>$mark_inforamtion]]);
		    }
	}


}