
	<?php if ($model->scenario === 'update'): ?>

	<h3><?php echo Rights::getAuthItemTypeName($model->type); ?></h3>

		<?php endif; ?>

		<?php 

		$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'id' => 'update-role-form',
			'type' => 'horizontal',
			'htmlOptions' => array(
				'class' => 'form-horizontal well',
			),
			//'enableClientValidation' => true,
			'clientOptions' => array(
				'validateOnSubmit' => true,
			),
			'focus' => array($model, 'name'),
			));
		?>

		<div class="row">

			<?php echo $form->textFieldRow($model, 'name', array('class' => 'span12', 'maxlength' => 255)); ?>
		<p class="hint text-center"><?php echo Rights::t('core', 'Do not change the name unless you know what you are doing.'); ?></p>
	</div>

		<div class="row">
			<?php echo $form->textAreaRow($model, 'description', array('class' => 'span12', 'maxlength' => 255)); ?>
		<p class="hint text-center"><?php echo Rights::t('core', 'A descriptive name for this item.'); ?></p>
	</div>

		<?php if (Rights::module()->enableBizRule === true): ?>

		<div class="row">
			<?php echo $form->textFieldRow($model, 'bizRule', array('class' => 'span12', 'maxlength' => 255)); ?>
				<p class="hint text-center"><?php echo Rights::t('core', 'Code that will be executed when performing access checking.'); ?></p>
			</div>

		<?php endif; ?>

		<?php if (Rights::module()->enableBizRule === true && Rights::module()->enableBizRuleData): ?>

		<div class="row">
			<?php echo $form->textFieldRow($model, 'data', array('class' => 'span12', 'maxlength' => 255)); ?>
				<p class="hint text-center"><?php echo Rights::t('core', 'Additional data available when executing the business rule.'); ?></p>
			</div>

		<?php endif; ?>

		<div class="row buttons">
			<?php echo CHtml::submitButton(Rights::t('core', 'Save')); ?> | <?php echo CHtml::link(Rights::t('core', 'Cancel'), Yii::app()->user->rightsReturnUrl); ?>
		</div>

		<?php $this->endWidget(); ?>