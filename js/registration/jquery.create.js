/* 
 * 
 * @author TNChalise <tnchalise99@gmail.com>
 * @since 2015.07.14
 * 
 */
var isClicked = false;
var checkboxCount = 0;

$(function() {
    main();
});

var main = function() {
    $('.date').attr("placeholder", 'year-month-date');
    $('.date').mask("9999-99-99");
    $('.mobile').mask("(9999)-999999");
    $('.contact').mask('(99) 9999-9999');


    $(document).on('change', '#school_level', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        if (selected.length > 0) {
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#program_level').html(response.option);
                    } else {
                        $('#program_level').html('');
                        $('#class_id').html('');
                    }
                }
            });

            var qual_url = $('#qualification_url').val();
            var url_2 = qual_url + '/key/' + selected;
            console.log(url_2);
            $.ajax({
                type: 'POST',
                url: url_2,
                dataType: 'JSON',
                success: function(response) {
                    $('#qualification_details_1').html(response.view);
                }
            });
        }
    });

    $(document).on('change','#RegistrationHistory_reg_type', function(){
        var this_ = $(this);
        var selected = this_.val();
        if( selected != 're_registration')
            $('#registration_histroy_status').hide('slow');
        else
            $('#registration_histroy_status').show('slow');
    })
/*
    $('input[name="Registration[sex]"]').click(function() {
        isClicked = true;
    });

    $('input[name="GeneralInformation[sex]"]').click(function() {
        isClicked = true;
    });*/

    $('.checkbox').on('click', function() {
        checkboxCount = $('input[type="checkbox"]:checked').length;
        if (checkboxCount > optCount) {
            if ($(this).is(":checked")) {
                // if checkbox is already checked, then uncheck event has to be occur.
            } else {
                return false;
            }
        }
    });


    $(document).on('change', '#program_level', function() {
        var this_ = $(this);
        var selected = this_.val();
        var url = this_.data('url') + '/key/' + selected;
        if (selected.length > 0) {
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'JSON',
                success: function(response) {
                    if (response.success) {
                        $('#class_id').html(response.option);
                    } else {
                        $('#class_id').html('');
                    }
                }
            });
        }
    });


};


var fetchProgramLevel = function() {

};

var fetchClass = function() {

};

var validateSubjectForm = function() {
    if (checkboxCount != optCount) {
        $('.subject-error').html('Please select exactly ' + optCount + ' subjects, that are optional').show();
    }
    else {
        $('.subject-error').html('').hide();
        return true;
    }
    return false;
};

var validateBasicInfo = function() {
    var options = {
        create: true,
        requiredFields: [
            'GeneralInformation_name',
            //'GeneralInformation_nepalI_dob',
            'school_level',
            'program_level',
            'class_id'
        ],
        //radioButtons: [{name: 'GeneralInformation[sex]', status: isClicked}],
    };
    var formValidation = new formValidate(options);
    return formValidation.getReponse();
};

var validateAdditionalInfo = function(){
    var options = {
        create: true,
        requiredFields: [
            'GuardianInformation_guardian_name',
            'GuardianInformation_occupation',
            'Location_initail_id',
            'second_parent_id',
            'third_parent_id',
            'fourth_parent_id',
            'fifth_parent_id',
            'Registration_premanent_contact_number',
        ],
    };
    var formValidation = new formValidate(options);
    return formValidation.getReponse();
};

var validateForm = function() {
    var options = {
        create: true,
        requiredFields: [
            'Registration_student_from',
            'Registration_name',
            //'Registration_nepalI_dob',
            'GuardianInformation_guardian_name',
            'GuardianInformation_occupation',
            'Location_initail_id',
            'second_parent_id',
            'third_parent_id',
            'fourth_parent_id',
            'fifth_parent_id',
            'Registration_premanent_contact_number',
            'school_level',
            'program_level',
            'class_id',
            'AcademicInformation_board_0',
            'AcademicInformation_passed_year_0',
            'AcademicInformation_percentage_0',
            'AcademicInformation_board_1',
            'AcademicInformation_passed_year_1',
            'AcademicInformation_percentage_1',
        ],
        //radioButtons: [{name: 'Registration[sex]', status: isClicked}],
    };
    var formValidation = new formValidate(options);
    return formValidation.getReponse();
};
