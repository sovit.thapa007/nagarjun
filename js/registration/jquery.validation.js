/* 
 * 
 * @author TNChalise <tnchalise99@gmail.com>
 * @since 2015.07.14
 * 
 */

var response = true;
var firstElementInForm = false;
var settings = {
    create: true,
    edit: false,
    requiredFields: [],
    radioButtons: [{}],
};

var formValidate = function(options) {

    response = true;
    firstElementInForm = false;
    settings = $.extend(settings, options);

    // If we have required Fields, then validate not empty
    for (var keys in settings.requiredFields) {
        this.validateNotEmpty(settings.requiredFields[keys]);
    }

    // If we have radio buttons, then name and status of the radio button clicked to be sent.
    for (var keys in settings.radioButtons) {
        var buttonObject = settings.radioButtons[keys];
        this.validateRadioButton(buttonObject);
    }

};

formValidate.prototype.validateNotEmpty = function(id, errorText) {
    errorText = errorText ? errorText : "This field can't be empty";
    var element = $('#' + id);

    if ($(document).find(element).length > 0) {
        var val = element.val();
        if (val.length < 1) {
            this.showError(element, errorText);
            response = false;
        } else {
            this.hideError(element);
        }
    }
    return;
};

formValidate.prototype.validateRadioButton = function(obj) {
    if (obj.name || obj.status) {
        var name = obj.name;
        var status = obj.status;
    }

    if (false === status) {
        $('input[name="' + obj.name + '"]').focus().addClass('custom-error');
        response = false;
    }

};

formValidate.prototype.showError = function(element, errorText) {
    element.addClass('custom-error');
    if (false === firstElementInForm) {
        element.focus();
        firstElementInForm = true;
    }
};

formValidate.prototype.hideError = function(element) {
    element.removeClass('custom-error');
};

formValidate.prototype.getReponse = function() {
    return response;
};